#version 330 core

#define M_PI 3.1415926535897932384626433832795

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoords0;
layout (location = 3) in vec2 texCoords1;
layout (location = 4) in vec2 texCoords2;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform bool insideCapturedArea;

uniform mat4 surroundingSphereRotation[3];

out vec2 surroundingSphereTexCoords_u1[3];
out vec2 surroundingSphereTexCoords_u2[3];

void main()
{
	if (insideCapturedArea) {

		float r = 1.0;	// radius of sphere

		for (int i = 0; i < 3; i++) {
			mat4 inverseRotationMatrix = inverse(surroundingSphereRotation[i]);
			vec3 rotatedPosition = (inverseRotationMatrix * vec4(position, 0.0)).xyz;

			// reconstruct latitude/longitude
			float latRadians = asin(rotatedPosition.y);
			float lonRadians = atan(-rotatedPosition.x, -rotatedPosition.z);
			if (lonRadians >= M_PI) {
				lonRadians -= (2.0 * M_PI);
			}
			if (lonRadians <= -M_PI) {
				lonRadians += (2.0 * M_PI);
			}

			float u = 1.0 - (1.0 / (2.0 * M_PI) * lonRadians + 0.5);
			float v = 1.0 - (1.0 / (M_PI) * latRadians + 0.5);

			vec2 uv = vec2(u,v);

			surroundingSphereTexCoords_u1[i] = fract(uv);
			surroundingSphereTexCoords_u2[i] = fract(uv + vec2(0.5,0.5)) - vec2(0.5,0.5);
		}
	}

    gl_Position = projection * view * model * vec4(position, 1.0);
}