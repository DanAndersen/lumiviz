#version 330 core
#extension GL_OES_standard_derivatives : enable


#define MAX_NUM_SOURCE_PANOS 3

#define M_PI 3.1415926535897932384626433832795

uniform bool nearestNeighbor;

uniform bool maskHeadTexture;
uniform float maskHeadExtent;

uniform sampler2D surroundingSphereTex[MAX_NUM_SOURCE_PANOS];

in vec4 worldSpace;
in vec3 BarycentricCoords;
uniform bool drawWireframe;

uniform float barycentricCoord[MAX_NUM_SOURCE_PANOS];

uniform mat4 surroundingSphereModelMatrices[MAX_NUM_SOURCE_PANOS];

uniform int numSourcePanos;

layout(location = 0) out vec4 color;


float edgeFactor(){
    vec3 d = fwidth(BarycentricCoords);
    vec3 a3 = smoothstep(vec3(0.0), d*1.5, BarycentricCoords);
    return min(min(a3.x, a3.y), a3.z);
}



void main()
{
	float maxBary = 0.0f;
	for (int i = 0; i < numSourcePanos; i++) {
		maxBary = max(maxBary, barycentricCoord[i]);
	}


	float bary[MAX_NUM_SOURCE_PANOS];
	for (int i = 0; i < numSourcePanos; i++) {
		bary[i] = barycentricCoord[i];
		if (nearestNeighbor) {
			bary[i] = bary[i] >= maxBary ? 1.0 : 0.0;
		}
	}

	vec2 texCoords[MAX_NUM_SOURCE_PANOS];
	float mask[MAX_NUM_SOURCE_PANOS];
	for (int i = 0; i < numSourcePanos; i++) {

		vec4 cameraSpace = inverse(surroundingSphereModelMatrices[i]) * vec4(worldSpace.xyz, 1.0);

		vec3 surroundingSphereCameraSpaceVector = normalize(cameraSpace.xyz);

		vec3 pt = surroundingSphereCameraSpaceVector;

		float latRadians = asin(pt.y);
		float lonRadians = atan(-pt.x, -pt.z);
		
		
		if (lonRadians >= M_PI) {
			lonRadians -= (2.0 * M_PI);
		}
		if (lonRadians <= -M_PI) {
			lonRadians += (2.0 * M_PI);
		}
		

		float u = 1.0 - (1.0 / (2.0 * M_PI) * lonRadians + 0.5);
		float v = 1.0 - (1.0 / (M_PI) * latRadians + 0.5);

		texCoords[i] = vec2(u,v);;
		mask[i] = 1.0;
	}

	vec3 colorContributions[MAX_NUM_SOURCE_PANOS];
	float barySum = 1.0;

	for (int i = 0; i < numSourcePanos; i++) {
		colorContributions[i] = (texture(surroundingSphereTex[i], texCoords[i]).xyz) * bary[i];

		if (maskHeadTexture) {
			float verticalExtent = mod(texCoords[i].y, 1.0);

			if (verticalExtent > maskHeadExtent) {
				mask[i] = 0.0;
				barySum -= bary[i];

				colorContributions[i] = vec3(0.0, 0.0, 0.0);
			}
		}
	}

	vec3 blendedColor = vec3(0.0, 0.0, 0.0);

	for (int i = 0; i < numSourcePanos; i++) {
		blendedColor += colorContributions[i];
	}

	if (barySum > 0) {
		blendedColor /= barySum;
	} else {
		blendedColor = vec3(0.0, 0.0, 0.0);
	}
	color = vec4(blendedColor, 1.0);


	if (drawWireframe) {
		color.rgb = mix(vec3(1.0), blendedColor, edgeFactor());
	}
} 