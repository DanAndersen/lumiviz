#version 330 core

#define MAX_NUM_SOURCE_PANOS 3

in vec2 TexCoords_u1;
in vec2 TexCoords_u2;

in vec3 BarycentricCoords;

uniform bool nearestNeighbor;
uniform bool maskHeadTexture;
uniform float maskHeadExtent;

uniform sampler2D surroundingSphereTex[MAX_NUM_SOURCE_PANOS];
in vec2 surroundingSphereTexCoords_u1[MAX_NUM_SOURCE_PANOS];
in vec2 surroundingSphereTexCoords_u2[MAX_NUM_SOURCE_PANOS];
uniform float barycentricCoord[MAX_NUM_SOURCE_PANOS];

uniform int numSourcePanos; // 3 if doing 2D morph; 2 if doing 1D path morph

layout(location = 0) out vec4 color;


vec2 getWraparoundTexcoords(vec2 texcoords_u1, vec2 texcoords_u2) {
    vec2 texCoords;
    texCoords.x = ( fwidth( texcoords_u1.x ) < fwidth( texcoords_u2.x )-0.001 )? texcoords_u1.x : texcoords_u2.x;
    texCoords.y = ( fwidth( texcoords_u1.y ) < fwidth( texcoords_u2.y )-0.001 )? texcoords_u1.y : texcoords_u2.y ;
    return texCoords;
}

void main()
{
	float maxBary = 0.0f;
	for (int i = 0; i < numSourcePanos; i++) {
		maxBary = max(maxBary, barycentricCoord[i]);
	}


	float bary[MAX_NUM_SOURCE_PANOS];
	for (int i = 0; i < numSourcePanos; i++) {
		bary[i] = barycentricCoord[i];
		if (nearestNeighbor) {
			bary[i] = bary[i] >= maxBary ? 1.0 : 0.0;
		}
	}

	vec2 texCoords[MAX_NUM_SOURCE_PANOS];
	float mask[MAX_NUM_SOURCE_PANOS];
	for (int i = 0; i < numSourcePanos; i++) {
		texCoords[i] = getWraparoundTexcoords(surroundingSphereTexCoords_u1[i], surroundingSphereTexCoords_u2[i]);
		mask[i] = 1.0;
	}

	vec3 colorContributions[MAX_NUM_SOURCE_PANOS];
	float barySum = 1.0;

	for (int i = 0; i < numSourcePanos; i++) {
		colorContributions[i] = (texture(surroundingSphereTex[i], texCoords[i]).xyz) * bary[i];

		if (maskHeadTexture) {
			float verticalExtent = mod(texCoords[i].y, 1.0);

			if (verticalExtent > maskHeadExtent) {
				mask[i] = 0.0;
				barySum -= bary[i];

				colorContributions[i] = vec3(0.0, 0.0, 0.0);
			}
		}
	}



	vec3 blendedColor = vec3(0.0, 0.0, 0.0);

	for (int i = 0; i < numSourcePanos; i++) {
		blendedColor += colorContributions[i];
	}

	if (barySum > 0) {
		blendedColor /= barySum;
	} else {
		blendedColor = vec3(0.0, 0.0, 0.0);
	}
	color = vec4(blendedColor, 1.0);
} 