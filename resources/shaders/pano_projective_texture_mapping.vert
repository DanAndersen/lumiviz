#version 330 core

#define M_PI 3.1415926535897932384626433832795

//layout (location = 0) in vec3 position;
//layout (location = 1) in vec3 normal;
//layout (location = 2) in vec2 texCoords0;
//layout (location = 3) in vec2 texCoords1;
//layout (location = 4) in vec2 texCoords2;

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in uint pointId;
layout (location = 3) in vec2 texCoords0;
layout (location = 4) in vec2 texCoords1;
layout (location = 5) in vec2 texCoords2;
layout (location = 6) in uint triangleId;
layout (location = 7) in vec3 barycentricCoords; // only (1,0,0), (0,1,0), and (0,0,1) in the vertex shader

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform bool projectCurrentWorldHull;
uniform vec3 tripletCentroid;

out vec3 BarycentricCoords;
out vec4 worldSpace;

void main()
{
	worldSpace = model * vec4(position, 1.0);


	vec3 renderWorldPosition = position;

	if (projectCurrentWorldHull) {
		renderWorldPosition = normalize(position - tripletCentroid) + tripletCentroid;
	}

	gl_Position = projection * view * model * vec4(renderWorldPosition, 1.0);
    
    BarycentricCoords = barycentricCoords;
}