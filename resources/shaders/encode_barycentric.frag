#version 330 core
precision highp float;

in vec3 BarycentricCoords;
flat in uint TriangleId;

layout(location = 0) out vec4 color;

vec4 encodeTriangleData() {
    // Encode the barycentric coords and the triangle id into the RGBA color.
    // This will allow us to determine, for each pixel in the output, which triangle (and where on the triangle) the ray corresponding to that pixel intersected.

    // color_red = barycentric_alpha (between 0.0 and 1.0)
    // color_green = barycentric_beta (between 0.0 and 1.0)
    // (barycentric_gamma can be inferred: barycentric_gamma = 1.0 - barycentric_alpha - barycentric_beta)

    // color_blue and color_alpha will encode the triangleId.
    // We do this so we can have more than 256 triangles. This will let us have 256*256 triangles.
    vec4 encodedColor;

    encodedColor.r = BarycentricCoords.x;
    encodedColor.g = BarycentricCoords.y;

    uint triangleId = TriangleId;
    uint quotient = triangleId / 256u;
    uint remainder = triangleId % 256u;
    float quotientNormalized = float(quotient) / 255.0;
    float remainderNormalized = float(remainder) / 255.0;

    encodedColor.b = remainderNormalized;
    encodedColor.a = quotientNormalized;
    //encodedColor.a = 0.0;

    return encodedColor;
}

void main()
{
    color = encodeTriangleData();
} 