#version 330 core

in vec2 TexCoords0_u1;
in vec2 TexCoords0_u2;

in vec2 TexCoords1_u1;
in vec2 TexCoords1_u2;

uniform sampler2D startEndTex[2];

uniform float t;

layout(location = 0) out vec4 color;

vec2 getWraparoundTexcoords(vec2 texcoords_u1, vec2 texcoords_u2) {
    vec2 texCoords;
    texCoords.x = ( fwidth( texcoords_u1.x ) < fwidth( texcoords_u2.x )-0.001 )? texcoords_u1.x : texcoords_u2.x;
    texCoords.y = ( fwidth( texcoords_u1.y ) < fwidth( texcoords_u2.y )-0.001 )? texcoords_u1.y : texcoords_u2.y ;
    return texCoords;
}

void main()
{
    vec2 texCoords0 = getWraparoundTexcoords(TexCoords0_u1, TexCoords0_u2);
    vec2 texCoords1 = getWraparoundTexcoords(TexCoords1_u1, TexCoords1_u2);

    vec3 startTexColor = texture(startEndTex[0], texCoords0).rgb;

    vec3 endTexColor = texture(startEndTex[1], texCoords1).rgb;

    vec3 mixedColor = mix(startTexColor, endTexColor, t);

    color = vec4(mixedColor, 1.0f);
    //color = vec4(1.0f, 1.0f, 0.0f, 1.0f);
} 