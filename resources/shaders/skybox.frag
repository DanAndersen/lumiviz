#version 420 core
#extension GL_EXT_texture_array : enable

#define NUM_VIEW_DIRECTIONS 16

in vec2 TexCoords_u1;
in vec2 TexCoords_u2;
in vec3 FragPos;

in vec4 cameraSpace;
in vec4 cameraSpaceArray[NUM_VIEW_DIRECTIONS];

uniform vec3 lightPos;
uniform vec3 lightColor;
uniform vec3 objectColor;

uniform bool renderingSimulatedView;

uniform mat4 locatableCameraProjection;

uniform sampler2D tex;
uniform sampler2DArray texArray;
uniform bool usingTexArray;

layout(location = 0) out vec4 color;

vec2 getUnitTexcoord(vec4 camSpace) {
    vec4 unprojectedTex = locatableCameraProjection * vec4(camSpace.xyz, 1);

    if (unprojectedTex.w > 0.0) {
        vec2 projectedTex = (unprojectedTex.xy / unprojectedTex.w);
        vec2 unitTexcoord = ((projectedTex * 0.5) + vec2(0.5, 0.5));
        if (!renderingSimulatedView) {
            unitTexcoord.y = 1.0 - unitTexcoord.y;
        }

        return unitTexcoord;
    }

    return vec2(-1.0, -1.0);
}

vec2 getWraparoundTexcoords(vec2 texcoords_u1, vec2 texcoords_u2) {
    vec2 texCoords;
    texCoords.x = ( fwidth( texcoords_u1.x ) < fwidth( texcoords_u2.x )-0.001 )? texcoords_u1.x : texcoords_u2.x;
    texCoords.y = ( fwidth( texcoords_u1.y ) < fwidth( texcoords_u2.y )-0.001 )? texcoords_u1.y : texcoords_u2.y ;
    return texCoords;
}

void main()
{
    vec3 accumulatedColor = vec3(0.0, 0.0, 0.0);
    int numViewsCovering = 0;

    if (usingTexArray) {
        for (int i = 0; i < NUM_VIEW_DIRECTIONS; i++) {
            vec2 unitTexcoord = getUnitTexcoord(cameraSpaceArray[i]);

            if (unitTexcoord.x >= 0.0 && unitTexcoord.x <= 1.0 && unitTexcoord.y >= 0.0 && unitTexcoord.y <= 1.0) {
                accumulatedColor += texture2DArray(texArray, vec3(unitTexcoord, i)).xyz;
                //accumulatedColor += texture(tex, unitTexcoord).xyz;

                numViewsCovering++;
            }
        }
    } else {
        vec2 unitTexcoord = getUnitTexcoord(cameraSpace);

        if (unitTexcoord.x >= 0.0 && unitTexcoord.x <= 1.0 && unitTexcoord.y >= 0.0 && unitTexcoord.y <= 1.0) {
            //color = texture(tex, TexCoords);
            accumulatedColor += texture(tex, unitTexcoord).xyz;

            numViewsCovering++;
        }
    }
    
    
    if (numViewsCovering > 0) {
        color = vec4(accumulatedColor / numViewsCovering, 1.0); // average between all the valid frames
    } else {
        vec2 texCoords = getWraparoundTexcoords(TexCoords_u1, TexCoords_u2);

        color = vec4(texCoords, 0.0, 1.0);
    }
} 