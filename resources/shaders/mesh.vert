#version 330 core
  
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 pointColor;
layout (location = 3) in vec2 texCoords0;
layout (location = 4) in vec2 texCoords1;
layout (location = 5) in vec2 texCoords2;

out vec2 TexCoords_u1;
out vec2 TexCoords_u2;
out vec2 TexCoords;

out vec3 Normal;
out vec3 FragPos;
out vec3 PointColor;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform bool noWraparound;

void main()
{
    gl_Position = projection * view * model * vec4(position, 1.0);
    
	TexCoords_u1 = fract(texCoords0);
    TexCoords_u2 = fract(texCoords0 + vec2(0.5,0.5)) - vec2(0.5,0.5);
    TexCoords = texCoords0;

    Normal = normal;
    FragPos = vec3(model * vec4(position, 1.0));

    PointColor = pointColor;
}