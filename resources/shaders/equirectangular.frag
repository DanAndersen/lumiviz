#version 330 core
precision highp float;

layout(location = 0) out vec4 color;

in vec2 TexCoord;

uniform samplerCube cubemap;

void main()
{
    vec2 thetaphi = ((TexCoord * 2.0) - vec2(0.5, 1.0)) * vec2(3.1415926535897932384626433832795, 1.5707963267948966192313216916398); 
    vec3 rayDirection = vec3(cos(thetaphi.y) * cos(thetaphi.x), sin(thetaphi.y), cos(thetaphi.y) * sin(thetaphi.x));

    color = texture(cubemap, rayDirection);

    //color = vec4(TexCoord, 0.0f, 1.0f);
} 