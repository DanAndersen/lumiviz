#version 330 core

#define MAX_NUM_SOURCE_PANOS 3

#define M_PI 3.1415926535897932384626433832795

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in uint pointId;
layout (location = 3) in vec2 texCoords0;
layout (location = 4) in vec2 texCoords1;
layout (location = 5) in vec2 texCoords2;
layout (location = 6) in uint triangleId;
layout (location = 7) in vec3 barycentricCoords; // only (1,0,0), (0,1,0), and (0,0,1) in the vertex shader

out vec2 TexCoords_u1;
out vec2 TexCoords_u2;

out vec3 BarycentricCoords;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform bool insideCapturedArea;

out vec2 surroundingSphereTexCoords_u1[MAX_NUM_SOURCE_PANOS];
out vec2 surroundingSphereTexCoords_u2[MAX_NUM_SOURCE_PANOS];

uniform int numSourcePanos; // 3 if doing 2D morph; 2 if doing 1D path morph

uniform float panoSphereRadius;

void main()
{
	if (insideCapturedArea) {

		vec2 uvs[MAX_NUM_SOURCE_PANOS];
		uvs[0] = texCoords0;
		uvs[1] = texCoords1;
		uvs[2] = texCoords2;

		for (int i = 0; i < numSourcePanos; i++) {
			vec2 uv = uvs[i];
			surroundingSphereTexCoords_u1[i] = fract(uv);
			surroundingSphereTexCoords_u2[i] = fract(uv + vec2(0.5,0.5)) - vec2(0.5,0.5);
		}
	}

    gl_Position = projection * view * model * vec4(position * panoSphereRadius, 1.0);
    
    TexCoords_u1 = fract(texCoords0);
    TexCoords_u2 = fract(texCoords0 + vec2(0.5,0.5)) - vec2(0.5,0.5);

    BarycentricCoords = barycentricCoords;
}