#version 330 core
  
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 pointColor;
layout (location = 3) in vec2 texCoords0;
layout (location = 4) in vec2 texCoords1;
layout (location = 5) in vec2 texCoords2;

out vec2 TexCoords0_u1;
out vec2 TexCoords0_u2;

out vec2 TexCoords1_u1;
out vec2 TexCoords1_u2;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    gl_Position = projection * view * model * vec4(position, 1.0);
    
	TexCoords0_u1 = fract(texCoords0);
    TexCoords0_u2 = fract(texCoords0 + vec2(0.5,0.5)) - vec2(0.5,0.5);

    TexCoords1_u1 = fract(texCoords1);
    TexCoords1_u2 = fract(texCoords1 + vec2(0.5,0.5)) - vec2(0.5,0.5);
}