#version 330 core

in vec2 TexCoords_u1;
in vec2 TexCoords_u2;
in vec2 TexCoords;

in vec3 Normal;
in vec3 FragPos;
in vec3 PointColor;

uniform vec3 lightPos;
uniform vec3 lightColor;
uniform vec3 objectColor;

uniform bool texEnabled;
uniform bool pointColorEnabled;
uniform bool lightingEnabled;
uniform sampler2D tex;

uniform bool noWraparound;

layout(location = 0) out vec4 color;

vec2 getWraparoundTexcoords(vec2 texcoords_u1, vec2 texcoords_u2) {
    vec2 texCoords;
    texCoords.x = ( fwidth( texcoords_u1.x ) < fwidth( texcoords_u2.x )-0.001 )? texcoords_u1.x : texcoords_u2.x;
    texCoords.y = ( fwidth( texcoords_u1.y ) < fwidth( texcoords_u2.y )-0.001 )? texcoords_u1.y : texcoords_u2.y ;
    return texCoords;
}

void main()
{

    vec3 baseColor;
    if (pointColorEnabled) {
        baseColor = PointColor;
    } else {
        baseColor = objectColor;
    }

    vec2 texCoords = noWraparound ? TexCoords : getWraparoundTexcoords(TexCoords_u1, TexCoords_u2);

    vec3 unlitColor;

    if (texEnabled) {
        unlitColor = texture(tex, texCoords).rgb;
    } else {
        unlitColor = baseColor;
    }

    if (lightingEnabled) {
        vec3 norm = normalize(Normal);
        vec3 lightDir = normalize(lightPos - FragPos);

        float diff = max(dot(norm, lightDir), 0.0);

        vec3 diffuse = diff * lightColor;

        float ambientStrength = 0.1f;
        vec3 ambient = ambientStrength * lightColor;

        vec3 result = (ambient + diffuse) * unlitColor;

        //color = vec4(1.0f, 0.5f, 0.2f, 1.0f);
        //color = vec4(TexCoords, 0.0f, 1.0f);

        color = vec4(result, 1.0f);
    } else {
        color = vec4(unlitColor, 1.0f);
    }
} 