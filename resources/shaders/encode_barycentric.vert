#version 330 core
precision highp float;

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in uint pointId;
layout (location = 3) in vec2 texCoords0;
layout (location = 4) in vec2 texCoords1;
layout (location = 5) in vec2 texCoords2;
layout (location = 6) in uint triangleId;
layout (location = 7) in vec3 barycentricCoords;

out vec3 BarycentricCoords;
flat out uint TriangleId;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform bool useProjectedMesh;
uniform vec3 virtualCameraCenter;

void main()
{
	vec3 vertexPosition = position;
	if (useProjectedMesh) {
		vertexPosition = virtualCameraCenter + normalize(position - virtualCameraCenter);
	}

    gl_Position = projection * view * model * vec4(vertexPosition, 1.0);

    BarycentricCoords = barycentricCoords;

    TriangleId = triangleId;
}