#version 420 core

#define NUM_VIEW_DIRECTIONS 16

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in uint pointId;
layout (location = 3) in vec2 texCoords0;
layout (location = 4) in vec2 texCoords1;
layout (location = 5) in vec2 texCoords2;
layout (location = 6) in uint triangleId;

// http://vcg.isti.cnr.it/~tarini/no-seams/jgt_tarini.pdf
out vec2 TexCoords_u1;
out vec2 TexCoords_u2;
out vec3 FragPos;

out vec4 cameraSpace;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform mat4 locatableCameraWorldToLocal;

uniform bool usingTexArray;

uniform mat4 locatableCameraWorldToLocalArray[NUM_VIEW_DIRECTIONS];
out vec4 cameraSpaceArray[NUM_VIEW_DIRECTIONS];


void main()
{
	vec4 worldSpace = model * vec4(position.xyz, 1.0);

	if (usingTexArray) {
		for (int i = 0; i < NUM_VIEW_DIRECTIONS; i++) {
			cameraSpaceArray[i] = locatableCameraWorldToLocalArray[i] * vec4(worldSpace.xyz, 1.0);
		}
	} else {
		cameraSpace = locatableCameraWorldToLocal * vec4(worldSpace.xyz, 1.0);
	}
	
    gl_Position = projection * view * model * vec4(position, 1.0);

    TexCoords_u1 = fract(texCoords0);
    TexCoords_u2 = fract(texCoords0 + vec2(0.5,0.5)) - vec2(0.5,0.5);

    FragPos = vec3(model * vec4(position, 1.0));
}