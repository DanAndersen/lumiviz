#version 330 core

uniform bool insideCapturedArea;
uniform bool nearestNeighbor;

uniform sampler2D surroundingSphereTex[3];
in vec2 surroundingSphereTexCoords_u1[3];
in vec2 surroundingSphereTexCoords_u2[3];
uniform float barycentricCoord[3];

layout(location = 0) out vec4 color;


vec2 getWraparoundTexcoords(vec2 texcoords_u1, vec2 texcoords_u2) {
    vec2 texCoords;
    texCoords.x = ( fwidth( texcoords_u1.x ) < fwidth( texcoords_u2.x )-0.001 )? texcoords_u1.x : texcoords_u2.x;
    texCoords.y = ( fwidth( texcoords_u1.y ) < fwidth( texcoords_u2.y )-0.001 )? texcoords_u1.y : texcoords_u2.y ;
    return texCoords;
}

void main()
{

	if (insideCapturedArea) {

		vec3 blendedColor = vec3(0.0, 0.0, 0.0);

		for (int i = 0; i < 3; i++) {
			vec2 texCoords = getWraparoundTexcoords(surroundingSphereTexCoords_u1[i], surroundingSphereTexCoords_u2[i]);

			float bary = barycentricCoord[i];
			if (nearestNeighbor) {
				float maxBary = max( max(barycentricCoord[0], barycentricCoord[1]), barycentricCoord[2]);
				bary = bary >= maxBary ? 1.0 : 0.0;
			}

			vec3 colorContribution = (texture(surroundingSphereTex[i], texCoords).xyz) * bary;

			blendedColor += colorContribution;
		}

		color = vec4(blendedColor, 1.0);

	} else {
		color = vec4(1.0, 0.0, 1.0, 1.0);
	}
} 