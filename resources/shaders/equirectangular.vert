#version 330 core
precision highp float;
  
// takes in a fullscreen quad and just passes it through
layout (location = 0) in vec3 position;
layout (location = 3) in vec2 texCoords0;

out vec2 TexCoord;

void main()
{
    gl_Position = vec4(position, 1.0);

    TexCoord = texCoords0;
}