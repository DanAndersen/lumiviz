# -*- coding: utf-8 -*-
"""
Created on Thu Nov 09 08:55:32 2017

@author: ANDERSED
"""

# after doing cubemap bundle adjustment, the relative positions of the selected sphere points may have shifted,
# which could alter the triangulation
# here, we load in the original triangulation, and then also the cubemap camera centers from the refined data
# to see if the triangulation has changed, and if so, to alter it


import numpy as np
import cv2
import os
import glob
import matplotlib.pyplot as plt
import sys
import argparse
import ntpath
import yaml
import csv
import ntpath

from scipy.spatial import Delaunay

REFINED_TRIANGULATION_FILENAME = "sphere_pose_triangulation_refined_frame_numbers.yaml"

TRIANGULATION_FILENAME = "sphere_pose_triangulation.yaml"
POSE_SPHERE_FILENAME = "pose_sphere.csv"
SCANDATA_FILENAME = "scandata.yaml"

def saveFile(fname,data):
	fd = open(fname,"wb")
	yaml.dump(data,fd)
	fd.close()

def readFile(fname):
	fd = open(fname,"rb")
	data = yaml.load(fd)
	return data


def GetSphericalCameraFrameIndexFromPoseIndex(pose_index):
    locatableCameraSyncFrameIndex = scanData["hololens_sync_frame"] - 1 # go from 1-indexed to 0-indexed
    locatableCameraSyncFrameTimestamp = sphere_pose_timestamps[locatableCameraSyncFrameIndex]
    
    sphericalCameraSyncFrameIndex = scanData["spherical_sync_frame"] - 1 # go from 1-indexed to 0-indexed
    sphericalCameraFPS = scanData["spherical_fps"]
    
    locatableCameraTimestamp = sphere_pose_timestamps[pose_index]
    secondsSinceCalibrationTime = (locatableCameraTimestamp - locatableCameraSyncFrameTimestamp)
    sphericalCameraFrameIndex = int(round(sphericalCameraSyncFrameIndex + (secondsSinceCalibrationTime * sphericalCameraFPS)))
    return sphericalCameraFrameIndex



parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)

unrefined_triangulation_data = readFile(os.path.join(args.base_dir, TRIANGULATION_FILENAME))
unrefined_triangulation_indices = unrefined_triangulation_data["triangulation_indices"]

scanData = readFile(os.path.join(args.base_dir, SCANDATA_FILENAME))


sphere_pose_timestamps = []

with open(os.path.join(args.base_dir, POSE_SPHERE_FILENAME), 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        timestamp = float(row[0])
        
        sphere_pose_timestamps.append(timestamp)




unrefined_triangulation_frame_numbers = []
for unrefined_triplet in unrefined_triangulation_indices:
    unrefined_triplet_frame_numbers = []
    for pose_index_value in unrefined_triplet:
        frame_number = GetSphericalCameraFrameIndexFromPoseIndex(pose_index_value) + 1  # 0-indexed to 1-indexed!
        unrefined_triplet_frame_numbers.append(frame_number)
    unrefined_triangulation_frame_numbers.append(unrefined_triplet_frame_numbers)
    
    
# now, load up the refined cubemap data and make a new triangulation

refined_tri_points = []
refined_tri_frame_numbers = []

REFINED_CUBEMAP_DATA_FILENAME = "refined_cubemap_data.csv"

refined_cubemap_data_path = os.path.join(args.base_dir, REFINED_CUBEMAP_DATA_FILENAME)
with open(refined_cubemap_data_path, 'rb') as cubemap_data_file:
    reader = csv.reader(cubemap_data_file, delimiter=' ')
    for row in reader:
        print row
        frame_number = int(row[0])  # 1-indexed
        camera_center = [float(x) for x in row[1:4]]
        
        refined_tri_points.append([camera_center[0], camera_center[2]]) # x z
        refined_tri_frame_numbers.append(frame_number)
        
refined_tri_points = np.array(refined_tri_points)

refined_triangulation = Delaunay(refined_tri_points)


# get the refined triangulation in terms of the original frame numbers
refined_tri_triplets_frame_numbers = []
refined_tri_simplices = refined_triangulation.simplices
for triplet in refined_tri_simplices:
    original_frame_numbers_triplet = []
    for triplet_value in triplet:
        original_frame_numbers_triplet.append(refined_tri_frame_numbers[triplet_value])
    refined_tri_triplets_frame_numbers.append(original_frame_numbers_triplet)
    
out_refined_data = {'triangulation_indices': refined_tri_triplets_frame_numbers}

saveFile(os.path.join(args.base_dir, REFINED_TRIANGULATION_FILENAME), out_refined_data)