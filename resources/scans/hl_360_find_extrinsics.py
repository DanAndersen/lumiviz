# -*- coding: utf-8 -*-
"""
Created on Mon Oct 02 18:13:34 2017

@author: ANDERSED
"""

import numpy as np
import cv2
import os
import glob
import matplotlib.pyplot as plt
import sys
import argparse
import ntpath
import yaml
import csv

def saveFile(fname,data):
	fd = open(fname,"wb")
	yaml.dump(data,fd)
	fd.close()

def readFile(fname):
	fd = open(fname,"rb")
	data = yaml.load(fd)
	return data

SPHERE_CORNER_FILENAME = "spherical_corner_data.yaml"
HOLOLENS_CORNER_FILENAME = "hololens_corner_data.yaml"
SCANDATA_FILENAME = "scandata.yaml"

EXTRINSICS_FILENAME = "hl_sphere_extrinsics.yaml"

TRANSFORM_MATRIX_CSV_FILENAME = "hl_sphere_transform_matrix.csv"


parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")
parser.add_argument("chess_x", type=int, help="x dimension of chessboard")
parser.add_argument("chess_y", type=int, help="y dimension of chessboard")
parser.add_argument("chess_size", type=float, help="size of each chessboard square in meters")
parser.add_argument("hl_intrinsic_yaml", type=str, help="path to YAML file containing intrinsic calibration for HoloLens camera")

args = parser.parse_args()

print(args)

print("Doing stereo calibration between HoloLens camera and 360 camera...")


chessboard_dims = (args.chess_x, args.chess_y)
chessboard_square_size = args.chess_size

# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
objp = np.zeros((chessboard_dims[0]*chessboard_dims[1],3), np.float32)
objp[:,:2] = np.mgrid[0:chessboard_dims[0],0:chessboard_dims[1]].T.reshape(-1,2)
objp[:,:2] *= chessboard_square_size



sphere_corner_data = readFile(os.path.join(args.base_dir, SPHERE_CORNER_FILENAME))
hololens_corner_data = readFile(os.path.join(args.base_dir, HOLOLENS_CORNER_FILENAME))

scandata = readFile(os.path.join(args.base_dir, SCANDATA_FILENAME))


hololens_pose_csv_path = glob.glob(os.path.join(args.base_dir, "*_pose.csv"))[0]
holo_pose_timestamps = []
with open(hololens_pose_csv_path, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        holo_pose_timestamp = float(row[0])
        holo_pose_timestamps.append(holo_pose_timestamp)

hl_timestamp_at_sync_frame = holo_pose_timestamps[scandata["hololens_sync_frame"] - 1]
sphere_fps = scandata["spherical_fps"]
sphere_sync_frame = scandata["spherical_sync_frame"]

# find a mapping from each of the hololens chessboard frames to a corresponding spherical chessboard frame

frame_number_mappings_from_hl_to_spherical = []

for hl_frame_filename in hololens_corner_data["corners"]:
    hl_frame_number = int(hl_frame_filename.replace("frame","").replace(".jpg",""))
    hl_timestamp_at_input_frame = holo_pose_timestamps[hl_frame_number - 1]
    
    input_frame_seconds_since_sync_frame = hl_timestamp_at_input_frame - hl_timestamp_at_sync_frame
    
    corresponding_spherical_frame_number = int(np.round(sphere_sync_frame + (input_frame_seconds_since_sync_frame * sphere_fps)))
    
    frame_number_mappings_from_hl_to_spherical.append((hl_frame_number, corresponding_spherical_frame_number))

objpoints = []
imgpointsHL = []
imgpointsSphere = []

for mapping_pair in frame_number_mappings_from_hl_to_spherical:
    hl_frame_number = mapping_pair[0]
    spherical_frame_number = mapping_pair[1]
    
    hl_filename = "frame%05d.jpg" % (hl_frame_number,)
    spherical_filename = "pano_frame%05d_cubemap.jpg" % (spherical_frame_number,)
    
    if hl_filename in hololens_corner_data["corners"] and spherical_filename in sphere_corner_data["corners"]:
        hl_corners = hololens_corner_data["corners"][hl_filename]
        spherical_corners = sphere_corner_data["corners"][spherical_filename]
        
        objpoints.append(objp)
        imgpointsHL.append(hl_corners)
        imgpointsSphere.append(spherical_corners)


# next, load in the HoloLens intrinsic data...
hl_intrinsics = readFile(args.hl_intrinsic_yaml)

camera_matrix_HL = hl_intrinsics["camera_matrix"]
dist_coeffs_HL = hl_intrinsics["dist"]


# these shouldn't be used, since they would be only for the intrinsic guess, but we're fixing the intrinsics
w_ref = hl_intrinsics["width"]
h_ref = hl_intrinsics["height"]


# next, generate the intrinsics for the spherical camera.
# Our input for the images was the front side of a cubemap generated from an equirectangular image.
# We assume that the Gear 360's conversion has taken care of all distortion, so we assume that 
# the camera matrix is representative of a square 90-degree FOV camera with center of projection exactly in the middle


# grab one of the cubemaps to figure out the resolution
example_cubemap_img = cv2.imread(glob.glob(os.path.join(args.base_dir, "pano_frames\\cubemaps\\*.jpg"))[0])
cubemap_dims = example_cubemap_img.shape[0]

# because FOV is 90 degrees...
fx_sphere = cubemap_dims/2
fy_sphere = cubemap_dims/2

camera_matrix_Sphere = np.zeros_like(camera_matrix_HL)
camera_matrix_Sphere[0,0] = fx_sphere
camera_matrix_Sphere[1,1] = fy_sphere
camera_matrix_Sphere[0,2] = cubemap_dims/2
camera_matrix_Sphere[1,2] = cubemap_dims/2
camera_matrix_Sphere[2,2] = 1.0

dist_coeffs_Sphere = np.zeros_like(dist_coeffs_HL)





stereo_flags = 0
stereo_flags |= cv2.CALIB_FIX_INTRINSIC

stereo_criteria = (cv2.TERM_CRITERIA_MAX_ITER + cv2.TERM_CRITERIA_EPS, 100, 1e-5)


R =None
T = None
E = None
F = None
retval, camera_matrix_HL, dist_coeffs_HL, camera_matrix_Sphere, dist_coeffs_Sphere, R, T, E, F = cv2.stereoCalibrate(
        objpoints,imgpointsHL, imgpointsSphere,camera_matrix_HL, dist_coeffs_HL, camera_matrix_Sphere, dist_coeffs_Sphere,(w_ref,h_ref), R, T, E, F, flags= stereo_flags, criteria = stereo_criteria)

extrinsics_data = {
        "retval": retval,
        "camera_matrix_HL": camera_matrix_HL,
        "dist_coeffs_HL": dist_coeffs_HL,
        "camera_matrix_Sphere": camera_matrix_Sphere,
        "dist_coeffs_Sphere": dist_coeffs_Sphere,
        "R": R,
        "T": T,
        "E": E,
        "F": F        
        }

saveFile(os.path.join(args.base_dir, EXTRINSICS_FILENAME), extrinsics_data)
print("extrinsics data saved to {}".format(EXTRINSICS_FILENAME))

# also save the [R|t] portion of the extrinsics into a format that's easier to consume in C++

with open(os.path.join(args.base_dir, TRANSFORM_MATRIX_CSV_FILENAME), 'wb') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerow([R[0,0], R[0,1], R[0,2], T[0][0]])
    writer.writerow([R[1,0], R[1,1], R[1,2], T[1][0]])
    writer.writerow([R[2,0], R[2,1], R[2,2], T[2][0]])
    writer.writerow([0.0, 0.0, 0.0, 1.0])

print("4x4 matrix also saved to {}".format(TRANSFORM_MATRIX_CSV_FILENAME))