# -*- coding: utf-8 -*-
"""
Created on Mon Dec 11 13:47:09 2017

@author: ANDERSED
"""

# given the SPHORB matches between nodes in the triangulation, where the matches have been reduced to an inlier set
# by finding the essential matrix, draw a visualization of the links between each node (colored based on how many correspondences there are)

import numpy as np
import cv2
import os
import glob
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import sys
import argparse
import ntpath
import yaml
import csv
import re
import ntpath
from holoscan_common import saveFile, readFile, TRIANGULATION_FILENAME, SCANDATA_FILENAME, POSE_SPHERE_FILENAME, generateAdjacencyMap, getMappingsWithinTwoSteps


def GetSphericalCameraFrameIndexFromPoseIndex(pose_index):
    locatableCameraSyncFrameIndex = scanData["hololens_sync_frame"] - 1 # go from 1-indexed to 0-indexed
    locatableCameraSyncFrameTimestamp = sphere_pose_timestamps[locatableCameraSyncFrameIndex]
    
    sphericalCameraSyncFrameIndex = scanData["spherical_sync_frame"] - 1 # go from 1-indexed to 0-indexed
    sphericalCameraFPS = scanData["spherical_fps"]
    
    locatableCameraTimestamp = sphere_pose_timestamps[pose_index]
    secondsSinceCalibrationTime = (locatableCameraTimestamp - locatableCameraSyncFrameTimestamp)
    sphericalCameraFrameIndex = int(round(sphericalCameraSyncFrameIndex + (secondsSinceCalibrationTime * sphericalCameraFPS)))
    return sphericalCameraFrameIndex






parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)








scanData = readFile(os.path.join(args.base_dir, SCANDATA_FILENAME))


sphere_pose_timestamps = []
sphere_pose_matrices = []
sphere_pose_camera_centers = []

with open(os.path.join(args.base_dir, POSE_SPHERE_FILENAME), 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        timestamp = float(row[0])
        
        sphere_pose_timestamps.append(timestamp)
        
        sphere_pose_transformation_matrix = np.array([float(x) for x in row[1:]]).reshape(4,4)
        sphere_pose_matrices.append(sphere_pose_transformation_matrix)
        sphere_pose_camera_centers.append(sphere_pose_transformation_matrix[:3,3])
        

triangulation_data = readFile(os.path.join(args.base_dir, TRIANGULATION_FILENAME))

# this is the initial triangulation from the estimated HL poses. The values are pose indices, NOT sphere camera frame numbers
triangulation_indices = triangulation_data["triangulation_indices"]



pose_indices_to_frame_numbers = {}
frame_numbers_to_pose_indices = {}

# generate a representation of the triangulation where we are working with sphere frame indices rather than pose indices.
triangulation_sphere_frame_numbers = []
for pose_index_triangle in triangulation_indices:
    sphere_frame_number_triplet = []
    for pose_index_triangle_val in pose_index_triangle:
        frame_number = GetSphericalCameraFrameIndexFromPoseIndex(pose_index_triangle_val)
        sphere_frame_number_triplet.append(frame_number)
        
        pose_indices_to_frame_numbers[pose_index_triangle_val] = frame_number
        frame_numbers_to_pose_indices[frame_number] = pose_index_triangle_val
        
    triangulation_sphere_frame_numbers.append(sphere_frame_number_triplet)
    
    
# load in all the "essential matrix" inlier data


frame_i_to_frame_j_to_num_inliers = {}


pano_frame_dir = os.path.join(args.base_dir, "pano_frames")

input_sphorb_match_paths = glob.glob(os.path.join(pano_frame_dir, "sphorb_matches", "sphorb_match_[0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9].csv"))

for input_sphorb_match_path in input_sphorb_match_paths:
    csv_filename = os.path.split(input_sphorb_match_path)[-1]
    zero_indexed_frame_numbers = [int(x) - 1 for x in re.findall(r'\d+', csv_filename)] # go from 1-indexed to 0-indexed
    
    frame_number_i = zero_indexed_frame_numbers[0]
    frame_number_j = zero_indexed_frame_numbers[1]
                                  
    essential_filename = outdata_filename = "sphorb_match_%05d_%05d_essential.yaml" % (zero_indexed_frame_numbers[0] + 1, zero_indexed_frame_numbers[1] + 1)
    essential_filepath = os.path.join(os.path.join(pano_frame_dir, "sphorb_matches", outdata_filename))
    print essential_filepath
    
    essential_data = readFile(essential_filepath)
    
    num_inliers = len(essential_data["best_fit_inlier_indices"])
    
    if frame_number_i not in frame_i_to_frame_j_to_num_inliers:
        frame_i_to_frame_j_to_num_inliers[frame_number_i] = {}
        
    frame_i_to_frame_j_to_num_inliers[frame_number_i][frame_number_j] = num_inliers



max_num_inliers = 0
all_inlier_counts = []

for frame_number_i in frame_i_to_frame_j_to_num_inliers:
    for frame_number_j in frame_i_to_frame_j_to_num_inliers[frame_number_i]:
        num_inliers = frame_i_to_frame_j_to_num_inliers[frame_number_i][frame_number_j]
        max_num_inliers = max(max_num_inliers, num_inliers)
        all_inlier_counts.append(num_inliers)




for frame_number_i in frame_i_to_frame_j_to_num_inliers:
    
    pose_idx_i = frame_numbers_to_pose_indices[frame_number_i]
    cam_center_i = sphere_pose_camera_centers[pose_idx_i]
    
    plt.plot([cam_center_i[0]], [-cam_center_i[2]], 'bs')
    
    for frame_number_j in frame_i_to_frame_j_to_num_inliers[frame_number_i]:
        pose_idx_j = frame_numbers_to_pose_indices[frame_number_j]
        cam_center_j = sphere_pose_camera_centers[pose_idx_j]
        
        #print cam_center_i
        #print cam_center_j
        
        num_inliers = frame_i_to_frame_j_to_num_inliers[frame_number_i][frame_number_j]
        
        
        plt.plot([cam_center_j[0]], [-cam_center_j[2]], 'bs')
        
        if num_inliers > 10:
            
            color = np.array([1.0, 1.0, 1.0]) * (1.0 - (num_inliers * 1.0 / max_num_inliers))
            
            plt.plot([cam_center_i[0], cam_center_j[0]], [-cam_center_i[2], -cam_center_j[2]], color=color)

plt.axes().set_aspect('equal', 'datalim')
plt.show()