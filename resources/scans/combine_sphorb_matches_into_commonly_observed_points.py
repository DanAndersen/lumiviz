# -*- coding: utf-8 -*-
"""
Created on Mon Jan 29 14:58:19 2018

@author: ANDERSED

Given all the SPHORB matches that have been marked as "inliers" based on the essential matrices, 
create a structure to group pairwise matches together into a collection of points observed by multiple cameras.

This will be used to get better results than trying to just do 3D ray intersection 
between two rays from adjacent panos with a very small baseline.

"""

# Imports
import numpy as np
import cv2
import argparse
import os
from holoscan_common import POSE_SPHERE_FILENAME, readFile, TRIANGULATION_FILENAME, SCANDATA_FILENAME, solve_essential_matrix, sphereImgPointToXYZUnitSphere, rayDistanceToEpipolarPlane, LineLineIntersect, xyzUnitSphereToSphereImgPoint
import csv
import scipy
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import glob
import re

from scipy.sparse import lil_matrix

import time
from scipy.optimize import least_squares

# =============================================================================

# The graph nodes.
class Data(object):
    def __init__(self, name):
        self.__name  = name
        self.__links = set()

    @property
    def name(self):
        return self.__name

    @property
    def links(self):
        return set(self.__links)

    def add_link(self, other):
        self.__links.add(other)
        other.__links.add(self)
        

# The function to look for connected components.
def connected_components(nodes):
    print("starting connected_components")
    # List of connected components found. The order is random.
    result = []

    # Make a copy of the set, so we can modify it.
    nodes = set(nodes)
    print("created copy of nodes")

    # Iterate while we still have nodes to process.
    while nodes:

        # Get a random node and remove it from the global set.
        n = nodes.pop()

        # This set will contain the next group of nodes connected to each other.
        group = {n}

        # Build a queue with this node in it.
        queue = [n]

        # Iterate the queue.
        # When it's empty, we finished visiting a group of connected nodes.
        while queue:

            # Consume the next item from the queue.
            n = queue.pop(0)

            # Fetch the neighbors.
            neighbors = n.links

            # Remove the neighbors we already visited.
            neighbors.difference_update(group)

            # Remove the remaining nodes from the global set.
            nodes.difference_update(neighbors)

            # Add them to the group of connected nodes.
            group.update(neighbors)

            # Add them to the queue, so we visit them in the next iterations.
            queue.extend(neighbors)

        # Add the group to the list of groups.
        result.append(group)

    print("ending connected_components")
    # Return the list of groups.
    return result


# =============================================================================

parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)

scanData = readFile(os.path.join(args.base_dir, SCANDATA_FILENAME))

pano_frame_dir = os.path.join(args.base_dir, "pano_frames")

# Determine size of panoramas.
pano_frame_resolution = cv2.imread(glob.glob(os.path.join(pano_frame_dir, "pano_frame*.jpg"))[0]).shape[::-1][1:]

# =============================================================================

print("loading in data about essential matrices and inliers...")

input_essential_paths = glob.glob(os.path.join(pano_frame_dir, "sphorb_matches", "sphorb_match_[0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9]_essential.yaml"))

all_essential_data = []
frame_to_frame_to_essential_data = {}

for input_essential_path in input_essential_paths:
    essential_filename = os.path.split(input_essential_path)[-1]
    print("loading {}".format(essential_filename))
    essential_data = readFile(input_essential_path)
    
    all_essential_data.append(essential_data)
    
    zero_indexed_frame_numbers = [int(x) - 1 for x in re.findall(r'\d+', essential_filename)] # go from 1-indexed to 0-indexed
    frame_numbers_ij = zero_indexed_frame_numbers
    
    f_i = frame_numbers_ij[0]
    f_j = frame_numbers_ij[1]
    
    if f_i not in frame_to_frame_to_essential_data:
        frame_to_frame_to_essential_data[f_i] = {}
    if f_j not in frame_to_frame_to_essential_data:
        frame_to_frame_to_essential_data[f_j] = {}
    frame_to_frame_to_essential_data[f_i][f_j] = essential_data
    frame_to_frame_to_essential_data[f_j][f_i] = essential_data

# =============================================================================

# Load in each sphorb_match CSV.

input_sphorb_match_paths = glob.glob(os.path.join(pano_frame_dir, "sphorb_matches", "sphorb_match_[0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9].csv"))

feature_hashes_to_data = {}
features = set()
feature_hash_datas = set()
feature_hashes_to_features = {}

for input_sphorb_match_path in input_sphorb_match_paths:
    csv_filename = os.path.split(input_sphorb_match_path)[-1]
    print("reading in {}...".format(csv_filename))
    zero_indexed_frame_numbers = [int(x) - 1 for x in re.findall(r'\d+', csv_filename)] # go from 1-indexed to 0-indexed
    frame_numbers_ij = zero_indexed_frame_numbers
    fn_i = frame_numbers_ij[0]
    fn_j = frame_numbers_ij[1]
    
    inlier_indices = frame_to_frame_to_essential_data[fn_i][fn_j]['best_fit_inlier_indices']
    
    with open(input_sphorb_match_path, 'rb') as f:
        reader = csv.reader(f, delimiter=',')
        match_idx = 0
        for row in reader:
            
            if match_idx in inlier_indices:
            
                feature_i = (fn_i, row[0], row[1]) # we are using the string versions of the coordinates -- we can do this because the SPHORB matches are output to a truncated decimal position, so this uniquely identifies
                feature_j = (fn_j, row[2], row[3])
                
                feature_i_hash = hash(feature_i)
                feature_j_hash = hash(feature_j)
                
                if feature_i_hash not in feature_hashes_to_data:
                    feature_hashes_to_data[feature_i_hash] = Data(feature_i_hash)
                    feature_hashes_to_features[feature_i_hash] = feature_i
                    
                if feature_j_hash not in feature_hashes_to_data:
                    feature_hashes_to_data[feature_j_hash] = Data(feature_j_hash)
                    feature_hashes_to_features[feature_j_hash] = feature_j
                
                feature_hashes_to_data[feature_i_hash].add_link(feature_hashes_to_data[feature_j_hash])
                feature_hashes_to_data[feature_j_hash].add_link(feature_hashes_to_data[feature_i_hash])
                
                features.add(feature_i)
                features.add(feature_j)
                
                feature_hash_datas.add(feature_hashes_to_data[feature_i_hash])
                feature_hash_datas.add(feature_hashes_to_data[feature_j_hash])

print("done")

print("starting to find connected components...")
all_components = connected_components(feature_hash_datas)
print("found {} connected components".format(len(all_components)))

print("finding valid components...")
valid_components = []
for component in all_components:
    if len(component) > 2 and len(component) < len(input_sphorb_match_paths):
        valid_components.append(component)
print("found {} valid components".format(len(valid_components)))


print("writing point bundles...")
out_point_bundles_csv = os.path.join(args.base_dir, "point_bundles.csv")
with open(out_point_bundles_csv, 'wb') as out_file:
    writer = csv.writer(out_file, delimiter=',')
    
    for valid_component in valid_components:
        row = []
        for feature_hash_data in valid_component:
            feature_hash = feature_hash_data.name
            feature = feature_hashes_to_features[feature_hash]
            row.append(int(feature[0]))
            row.append(float(feature[1]))
            row.append(float(feature[2]))
            
        writer.writerow(row)
print("done writing point bundles to {}".format(out_point_bundles_csv))