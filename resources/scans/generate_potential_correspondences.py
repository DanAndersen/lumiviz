# -*- coding: utf-8 -*-
"""
Created on Thu Oct 12 16:57:41 2017

@author: ANDERSED
"""

import numpy as np
import cv2
import os
import glob
import matplotlib.pyplot as plt
import sys
import argparse
import ntpath
import yaml
import csv
import ntpath

TRIANGULATION_FILENAME = "sphere_pose_triangulation.yaml"


def saveFile(fname,data):
	fd = open(fname,"wb")
	yaml.dump(data,fd)
	fd.close()

def readFile(fname):
	fd = open(fname,"rb")
	data = yaml.load(fd)
	return data


def parseFeatureData(features_yaml):
    
    feature_sides = []
    
    for sideData in features_yaml["sides"]:
        
        des = None
        kp = []
        
        for i, kpDesData in enumerate(sideData):
            point = cv2.KeyPoint()
            (point.pt, point.size, point.angle, point.response, point.octave, point.class_id, descriptor) = kpDesData
            
            if des is None:
                des = np.zeros((len(sideData), descriptor.shape[0]), dtype=np.float32)
                
            des[i] = descriptor
            kp.append(point)
        
        feature_sides.append({
                "des": des,
                "kp": kp,
                })
    
    return feature_sides


#loaded_features = {}
#finished_matches = set()

def loadFeaturesIntoCache(index):
    global loaded_features
    if index not in loaded_features:
        features_yaml_path = os.path.join(feature_in_dir, "pano_frame{}_cubemap_features.yaml".format("%05d" % (index + 1,)))
        print("loading features for index {}".format(index))
        features_yaml = readFile(features_yaml_path)
        features_data = parseFeatureData(features_yaml)
        loaded_features[index] = features_data

parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)

bf = cv2.BFMatcher()

triangulation_data = readFile(os.path.join(args.base_dir, TRIANGULATION_FILENAME))



cubemap_in_dir = os.path.join(args.base_dir, "pano_frames\\cubemaps")
feature_in_dir = os.path.join(cubemap_in_dir, "features")

out_matches_dir = os.path.join(cubemap_in_dir, "matches")
if not os.path.exists(out_matches_dir):
    os.makedirs(out_matches_dir)

triangulation_indices = triangulation_data["triangulation_indices"]

for triplet in triangulation_indices:
    print triplet
    
    combinations = [(triplet[0], triplet[1]), (triplet[1], triplet[2]), (triplet[2], triplet[0])]
    
    for combination in combinations:
        lower_index = min(combination[0], combination[1])
        higher_index = max(combination[0], combination[1])
        if (lower_index, higher_index) not in finished_matches:
            print("comparing frame indices {} and {}".format(lower_index, higher_index))
        
            loadFeaturesIntoCache(lower_index)
            loadFeaturesIntoCache(higher_index)
            
            combination_data = {}
            
            for lower_side_index in range(6):
                
                lower_side_kp = loaded_features[lower_index][lower_side_index]["kp"]
                lower_side_des = loaded_features[lower_index][lower_side_index]["des"]
                
                combination_data[lower_side_index] = {}
                
                for higher_side_index in range(lower_side_index, 6):
                    
                    higher_side_kp = loaded_features[higher_index][higher_side_index]["kp"]
                    higher_side_des = loaded_features[higher_index][higher_side_index]["des"]
                    
                    good = []
                    
                    if lower_side_des is not None and higher_side_des is not None:
                        matches = bf.knnMatch(np.asarray(lower_side_des,np.float32), np.asarray(higher_side_des,np.float32), k=2)
                        
                        for m, n in matches:
                            if m.distance < 0.75 * n.distance:
                                good.append([m])
        
                    out_goods = []
                    for good_match in good:
                        g = good_match[0]
                        
                        queryPt = list(lower_side_kp[g.queryIdx].pt)
                        trainPt = list(higher_side_kp[g.trainIdx].pt)
                        
                        good_match_data = {"distance": g.distance, "queryIdx": g.queryIdx, "trainIdx": g.trainIdx, "queryPt": queryPt, "trainPt": trainPt}
                        out_goods.append(good_match_data)
                    
                    combination_data[lower_side_index][higher_side_index] = out_goods
            
            out_matches_yaml_path = os.path.join(out_matches_dir, "{}_{}_matches.yaml".format(lower_index, higher_index))
            saveFile(out_matches_yaml_path, combination_data)
            finished_matches.add((lower_index, higher_index))
            
    #break