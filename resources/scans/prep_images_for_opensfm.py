# -*- coding: utf-8 -*-
"""
Created on Tue Jan 30 17:49:44 2018

@author: ANDERSED

Edits the EXIF data of the selected pano_frame images to add some fake GPS data.
OpenSfM uses it to determine which images are near each other -- we can use this to limit the pairwise matches.

"""

import piexif
from PIL import Image

import numpy as np
import cv2
import argparse
import os
from holoscan_common import POSE_SPHERE_FILENAME, readFile, TRIANGULATION_FILENAME, SCANDATA_FILENAME, solve_essential_matrix, sphereImgPointToXYZUnitSphere, rayDistanceToEpipolarPlane, LineLineIntersect, xyzUnitSphereToSphereImgPoint, sphereImgPointToXYZUnitSphere
import csv
import scipy
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import glob
import re

from scipy.sparse import lil_matrix

import time
from scipy.optimize import least_squares

from fractions import Fraction

# =============================================================================

parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)

scanData = readFile(os.path.join(args.base_dir, SCANDATA_FILENAME))

pano_frame_dir = os.path.join(args.base_dir, "pano_frames")

# Determine size of panoramas.
pano_frame_resolution = cv2.imread(glob.glob(os.path.join(pano_frame_dir, "pano_frame*.jpg"))[0]).shape[::-1][1:]

# =============================================================================

# Load the refined poses for each frame number.

fn_to_pose_refined_path = os.path.join(args.base_dir, "fn_to_pose_refined.csv")

if not os.path.isfile(fn_to_pose_refined_path):
    raise Exception("Missing refined poses at {}".format(fn_to_pose_refined_path))

fns_to_poses = {}   # contains matrix [R|t] where [R|t] * X_local = X_world

with open(fn_to_pose_refined_path, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        fn = int(row[0]) # 0-indexed
        mat = np.array([float(x) for x in row[1:]]).reshape((4,4))
        fns_to_poses[fn] = mat
                    
# =============================================================================

WGS84_a = 6378137.0
WGS84_b = 6356752.314245

def ecef_from_lla(lat, lon, alt):
    '''
    Compute ECEF XYZ from latitude, longitude and altitude.
    All using the WGS84 model.
    Altitude is the distance to the WGS84 ellipsoid.
    Check results here http://www.oc.nps.edu/oc2902w/coord/llhxyz.htm
    >>> lat, lon, alt = 10, 20, 30
    >>> x, y, z = ecef_from_lla(lat, lon, alt)
    >>> np.allclose(lla_from_ecef(x,y,z), [lat, lon, alt])
    True
    '''
    a2 = WGS84_a**2
    b2 = WGS84_b**2
    lat = np.radians(lat)
    lon = np.radians(lon)
    L = 1.0 / np.sqrt(a2 * np.cos(lat)**2 + b2 * np.sin(lat)**2)
    x = (a2 * L + alt) * np.cos(lat) * np.cos(lon)
    y = (a2 * L + alt) * np.cos(lat) * np.sin(lon)
    z = (b2 * L + alt) * np.sin(lat)
    return x, y, z

def ecef_from_topocentric_transform(lat, lon, alt):
    '''
    Transformation from a topocentric frame at reference position to ECEF.
    The topocentric reference frame is a metric one with the origin
    at the given (lat, lon, alt) position, with the X axis heading east,
    the Y axis heading north and the Z axis vertical to the ellipsoid.
    >>> a = ecef_from_topocentric_transform(30, 20, 10)
    >>> b = ecef_from_topocentric_transform_finite_diff(30, 20, 10)
    >>> np.allclose(a, b)
    True
    '''
    x, y, z = ecef_from_lla(lat, lon, alt)
    sa = np.sin(np.radians(lat))
    ca = np.cos(np.radians(lat))
    so = np.sin(np.radians(lon))
    co = np.cos(np.radians(lon))
    return np.array([[- so, - sa * co, ca * co, x],
                     [  co, - sa * so, ca * so, y],
                     [   0,        ca,      sa, z],
                     [   0,         0,       0, 1]])

def lla_from_ecef(x, y, z):
    '''
    Compute latitude, longitude and altitude from ECEF XYZ.
    All using the WGS84 model.
    Altitude is the distance to the WGS84 ellipsoid.
    '''
    a = WGS84_a
    b = WGS84_b
    ea = np.sqrt((a**2 - b**2) / a**2)
    eb = np.sqrt((a**2 - b**2) / b**2)
    p = np.sqrt(x**2 + y**2)
    theta = np.arctan2(z * a, p * b)
    lon = np.arctan2(y, x)
    lat = np.arctan2(z + eb**2 * b * np.sin(theta)**3,
                     p - ea**2 * a * np.cos(theta)**3)
    N = a / np.sqrt(1 - ea**2 * np.sin(lat)**2)
    alt = p / np.cos(lat) - N
    return np.degrees(lat), np.degrees(lon), alt

# X is east, Y is north, Z is up from ellipsoid
def lla_from_topocentric(x, y, z, reflat, reflon, refalt):
    '''
    Transform from topocentric XYZ to lat, lon, alt.
    '''
    T = ecef_from_topocentric_transform(reflat, reflon, refalt)
    ex = T[0, 0] * x + T[0, 1] * y + T[0, 2] * z + T[0, 3]
    ey = T[1, 0] * x + T[1, 1] * y + T[1, 2] * z + T[1, 3]
    ez = T[2, 0] * x + T[2, 1] * y + T[2, 2] * z + T[2, 3]
    return lla_from_ecef(ex, ey, ez)

def degToDmsRational(degFloat):
    minFloat = degFloat % 1.0 * 60.0
    secFloat = minFloat % 1.0 * 60.0
    deg = np.floor(degFloat)
    minute = np.floor(minFloat)
    
    sec = Fraction(secFloat).limit_denominator()

    deg = abs(deg) * 1
    minute = abs(minute) * 1
  
    return ((deg, 1), (minute, 1), (abs(sec.numerator), sec.denominator))

# The directory where we only have the selected pano_frames
input_selected_pano_frame_images_dir = "E:\\test_pano_2\\images"

input_imgs = glob.glob(os.path.join(input_selected_pano_frame_images_dir, "pano_frame*.jpg"))

reflat = 0.0
reflon = 0.0
refalt = 0.0

for input_img_path in input_imgs:
    print("processing {}...".format(input_img_path))
    in_img = Image.open(input_img_path)
    
    one_indexed_fn = int(os.path.split(input_img_path)[-1].split("pano_frame")[1].split(".")[0])
    zero_indexed_fn = one_indexed_fn - 1
    
    pose = fns_to_poses[zero_indexed_fn]
    
    cam_position = pose[:3,3]
    
    topo_x = cam_position[0]
    topo_y = -cam_position[2]
    topo_z = cam_position[1]
    
    lat_degrees, lon_degrees, alt = lla_from_topocentric(topo_x, topo_y, topo_z, reflat, reflon, refalt)
    print("lat_degrees: {}, lon_degrees: {}, alt: {}".format(lat_degrees, lon_degrees, alt))
    
    alt_fraction = Fraction(alt).limit_denominator()
    
    gps_ifd = {
            piexif.GPSIFD.GPSLatitudeRef: 'S' if lat_degrees < 0 else 'N',
            piexif.GPSIFD.GPSLatitude: degToDmsRational(lat_degrees),
            piexif.GPSIFD.GPSLongitudeRef: 'W' if lon_degrees < 0 else 'E',
            piexif.GPSIFD.GPSLongitude: degToDmsRational(lon_degrees),
            piexif.GPSIFD.GPSAltitudeRef: 1 if alt < 0 else 0,
            piexif.GPSIFD.GPSAltitude: (abs(alt_fraction.numerator), alt_fraction.denominator)
            }
    
    exif_dict = {"0th": {}, "GPS":gps_ifd}
    
    exif_bytes = piexif.dump(exif_dict)
    piexif.insert(exif_bytes, input_img_path)
    

print("done")