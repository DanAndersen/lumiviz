# -*- coding: utf-8 -*-
"""
Created on Wed Jan 31 13:41:09 2018

@author: ANDERSED
"""
import os
os.chdir("E:\\Dev\\OpenGL\\lumiviz\\resources\\scans")

import numpy as np
from multiprocessing import Pool, freeze_support
import cv2
import time
from scipy.sparse import lil_matrix
from holoscan_common import readFile, saveFile


if __name__ == '__main__':
    freeze_support()
    
    
    def vector_to_cross_product_matrix(v):
        return np.array([
            [0.0, -v[2], v[1]],
            [v[2], 0.0, -v[0]],
            [-v[1], v[0], 0.0]
        ])
    
    
    def get_errs_for_panomatch(panomatchidx, cam_mat_i, cam_mat_j):
        errs_for_this_panomatch = []
        
        mat_i_to_j = np.matmul(np.linalg.inv(cam_mat_j), cam_mat_i)
        
        rot_i_to_j = mat_i_to_j[:3,:3]
    
        t_i_to_j = mat_i_to_j[:3,3]
        t_ij_norm = t_i_to_j / np.linalg.norm(t_i_to_j)
        #t_ij_norm = t_i_to_j
    
        t_cross_i_to_j = vector_to_cross_product_matrix(t_ij_norm)
    
        E_i_to_j = np.matmul(t_cross_i_to_j, rot_i_to_j)
        
        match_xyzs_i_inliers = panomatchidx_to_match_xyzs_i_inliers[panomatchidx]
        match_xyzs_j_inliers = panomatchidx_to_match_xyzs_j_inliers[panomatchidx]
        
        num_pairs = len(match_xyzs_i_inliers)
        
        if num_pairs > 0:
        
            E_pi = np.dot(E_i_to_j, match_xyzs_i_inliers.T).T
        
            for pairidx in range(num_pairs):
                #err = np.dot(match_xyzs_j_inliers[pairidx].T, np.dot(E_i_to_j, match_xyzs_i_inliers[pairidx]))
                #errs.append(err)
                
                #pj_E_pi = np.dot(match_xyzs_j_inliers[pairidx].T, np.dot(E_i_to_j, match_xyzs_i_inliers[pairidx]))
                pj_E_pi = np.dot(match_xyzs_j_inliers[pairidx], E_pi[pairidx])
                dist_ep_tangent = pj_E_pi / np.sqrt(1.0 - pj_E_pi*pj_E_pi) # dist_ep_tangent is defined in Pagani's work
                
                errs_for_this_panomatch.append(dist_ep_tangent)
        return errs_for_this_panomatch
    
    
    def fun(params, n_cameras, panomatchidx_to_camidxs):
        print("start fun()")
        
        errs = []
        
        cam_mats = np.zeros((n_cameras, 4, 4))
        
        for camidx in range(n_cameras):
            cam_rot_vector = params[(camidx * num_params_per_camera) : (camidx * num_params_per_camera + 3)]
            
            cam_trans_vector = params[(camidx * num_params_per_camera + 3) : (camidx * num_params_per_camera + 6)]
            
            cam_rot_mat, _ = cv2.Rodrigues(cam_rot_vector)
            
            cam_mats[camidx, :3, :3] = cam_rot_mat
            cam_mats[camidx, :3, 3] = cam_trans_vector
            cam_mats[camidx, 3, 3] = 1.0
        
        errs_for_each_panomatch = [None] * num_pano_matches
    
        
        def get_errs_for_panomatch_parameters():
            for panomatchidx in range(num_pano_matches):
                camidx_i = panomatchidx_to_camidxs[panomatchidx][0]
                camidx_j = panomatchidx_to_camidxs[panomatchidx][1]
                
                cam_mat_i = cam_mats[camidx_i]
                cam_mat_j = cam_mats[camidx_j]
                yield panomatchidx, cam_mat_i, cam_mat_j
                
        pool = Pool(2)
        
        errs_for_each_panomatch[:] = pool.map(get_errs_for_panomatch, get_errs_for_panomatch_parameters())
        
        
        """
        for panomatchidx in range(num_pano_matches):
            camidx_i = panomatchidx_to_camidxs[panomatchidx][0]
            camidx_j = panomatchidx_to_camidxs[panomatchidx][1]
            
            cam_mat_i = cam_mats[camidx_i]
            cam_mat_j = cam_mats[camidx_j]
            errs_for_each_panomatch[panomatchidx] = get_errs_for_panomatch(panomatchidx, cam_mat_i, cam_mat_j)
        """
    
        errs = [item for sublist in errs_for_each_panomatch for item in sublist] # flatten list of lists
    
        errs = np.array(errs)
        
        print("end fun(), mean abs err = {}".format(np.mean(np.abs(errs))))
    
        return errs
    
    # Make the Jacobian sparsity structure
    m = num_inlier_matches * 1 # only 1D residual from each pair
    n = num_cameras * num_params_per_camera
    A = lil_matrix((m, n), dtype=int)
    
    i = np.arange(num_inlier_matches)
    for s in range(num_params_per_camera):
        A[1 * i, cam_indices_i * num_params_per_camera + s] = 1
        A[1 * i, cam_indices_j * num_params_per_camera + s] = 1
    
    
    start_time = time.time()
    initial_errs = fun(input_params, num_cameras, panomatchidx_to_camidxs)
    elapsed_time = time.time() - start_time
    print("elapsed time for one iteration of fun(): {}".format(elapsed_time))
    
    
    
	