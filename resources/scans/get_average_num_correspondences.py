# -*- coding: utf-8 -*-
"""
Created on Tue Nov 21 20:24:08 2017

@author: ANDERSED
"""

import argparse
import re
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import yaml
import os
import csv
import functools
from scipy.spatial import ConvexHull
from scipy.spatial import Delaunay
import cv2
from holoscan_common import Camera, loadFullCubemapImgForFrameNumber, loadRefinedCubemapData, saveFile, readFile

parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)

REFINED_TRIANGULATION_FILENAME = "sphere_pose_triangulation_refined_frame_numbers.yaml"

refined_triangulation_data = readFile(os.path.join(args.base_dir, REFINED_TRIANGULATION_FILENAME))["triangulation_indices"]

num_correspondences_list = []

num_sparse_correspondences_list = []

print("reading in triplets...")
for triplet_frame_numbers in refined_triangulation_data:
    triplet_frame_numbers_sorted = np.copy(triplet_frame_numbers)
    triplet_frame_numbers_sorted.sort()
    
    input_triplet_mesh_filename = os.path.join(args.base_dir, "fixed_holoroom_and_sparse_triplet_{}_{}_{}.meshes".format(triplet_frame_numbers_sorted[0], triplet_frame_numbers_sorted[1], triplet_frame_numbers_sorted[2]))
    
    with open(input_triplet_mesh_filename, 'rb') as f:
        reader = csv.reader(f, delimiter=' ')
        num_pts = int(reader.next()[0])
        num_correspondences_list.append(num_pts)
        
    input_sparse_triplet_mesh_filename = os.path.join(args.base_dir, "fixed_triplet_{}_{}_{}.meshes".format(triplet_frame_numbers_sorted[0], triplet_frame_numbers_sorted[1], triplet_frame_numbers_sorted[2]))
    
    with open(input_sparse_triplet_mesh_filename, 'rb') as f:
        reader = csv.reader(f, delimiter=' ')
        num_sparse_pts = int(reader.next()[0])
        num_sparse_correspondences_list.append(num_sparse_pts)
print("done")
