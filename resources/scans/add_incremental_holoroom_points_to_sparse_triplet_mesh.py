# -*- coding: utf-8 -*-
"""
Created on Wed Feb 21 11:11:52 2018

@author: ANDERSED

Given a set of generated sparse triplet meshes, determine where features are too sparse.
Look for large triangles on the convex hull when projected to the triplet center.

Then, shoot a ray randomly through that triangle to break it up.
Do this until all triangles are below a threshold size.

"""

import argparse
import os
import process_scan_data_helpers
from holoscan_common import readFile, sphereImgPointToXYZUnitSphere, LineLineIntersect, GetPointsProjectedOntoTranslatedUnitSphere, writeOutputTripletMeshData, fixSimplicesNeighbors, PoissonGenerator
import csv
import numpy as np
import glob
import matplotlib.pyplot as plt
from scipy.spatial import ConvexHull
import trimesh
import time

import profile

os.chdir("E:\\Dev\\OpenGL\\lumiviz\\resources\\scans")


parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)

print("Base scan directory: {}".format(args.base_dir))

SCAN_LABEL = os.path.split(args.base_dir)[-1]

print("SCAN_LABEL: {}".format(SCAN_LABEL))

# =============================================================================

fn_to_pose_refined_path = os.path.join(args.base_dir, "fn_to_pose_refined.csv")

if not os.path.isfile(fn_to_pose_refined_path):
    raise Exception("Missing refined poses at {}".format(fn_to_pose_refined_path))

"""
refined_holoroom_file_results = glob.glob(os.path.join(args.base_dir, "*_mesh_refined.room"))
if len(refined_holoroom_file_results) == 0:
    raise Exception("no refined holoroom file found")
refined_holoroom_file_path = refined_holoroom_file_results[0]
"""

holoroom_file_results = glob.glob(os.path.join(args.base_dir, "*_mesh.room"))
if len(holoroom_file_results) == 0:
    raise Exception("no holoroom file found")
holoroom_file_path = holoroom_file_results[0]

# =============================================================================

# Load in the refined holo-mesh.

print("Reading HoloLens room geometry from {}...".format(holoroom_file_path))
holoroom_mesh_verts, holoroom_mesh_indices = process_scan_data_helpers.load_holoroom_mesh(holoroom_file_path)
print("Read HoloLens room geometry, {} verts and {} triangles".format(len(holoroom_mesh_verts), len(holoroom_mesh_indices)))

print("creating trimesh for holoroom")
holoroom_mesh = trimesh.Trimesh(vertices = holoroom_mesh_verts, faces = holoroom_mesh_indices)
print("done creating trimesh")

# setting up the ray-mesh intersector
intersector = trimesh.ray.ray_triangle.RayMeshIntersector(holoroom_mesh)

# =============================================================================

# Load the refined poses for each frame number.

fns_to_poses = {}   # contains matrix [R|t] where [R|t] * X_local = X_world

with open(fn_to_pose_refined_path, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        fn = int(row[0]) # 0-indexed
        mat = np.array([float(x) for x in row[1:]]).reshape((4,4))
        fns_to_poses[fn] = mat

# =============================================================================

# Load the triangulation.

refined_frame_number_triangulation_path = os.path.join(args.base_dir, "sphere_pose_triangulation_refined_frame_numbers.yaml")

if not os.path.isfile(refined_frame_number_triangulation_path):
    raise Exception("Missing refined sphere pose triangulation at {}".format(refined_frame_number_triangulation_path))

refined_triangulation_frame_numbers = readFile(refined_frame_number_triangulation_path)["triangulation_indices"]

# =============================================================================

input_sparse_meshes_dir = os.path.join(args.base_dir, "triplet_meshes", "sparse")
out_hl_and_sparse_meshes_dir = os.path.join(args.base_dir, "triplet_meshes", "holoroom_and_sparse")

if not os.path.exists(out_hl_and_sparse_meshes_dir):
    os.makedirs(out_hl_and_sparse_meshes_dir)

# =============================================================================

triangle_area_threshold = 0.01

fruitless_attempt_limit = 10 # if we try raycasting this many times and get absolutely nothing, we give up

# =============================================================================

def calculateVisibilityOfTriangles(simplices, projected_pts, projection_center):
    
    pts_for_each_triangle = projected_pts[simplices]
    
    a_verts = pts_for_each_triangle[:,0]
    b_verts = pts_for_each_triangle[:,1]
    c_verts = pts_for_each_triangle[:,2]
    
    b_minus_a = b_verts - a_verts
    c_minus_a = c_verts - a_verts
    
    normals = np.cross(b_minus_a, c_minus_a)
    # skipping normalization because we do not actually need to normalize to determine vector direction
    
    a_minus_proj_center = a_verts - projection_center
    
    visibilities = np.einsum('ij,ij->i', normals, a_minus_proj_center) > 0
    
    return visibilities

# =============================================================================

for triplet in refined_triangulation_frame_numbers:
    
    triplet_canonical_order = sorted(triplet) # used for labeling the triplet uniquely
    
    triplet_label = "{}_{}_{}".format(triplet_canonical_order[0], triplet_canonical_order[1], triplet_canonical_order[2])
    
    matching_input_meshes = glob.glob(os.path.join(input_sparse_meshes_dir, "*{}*.meshes".format(triplet_label)))
    if len(matching_input_meshes) == 0:
        raise Exception("No valid input mesh found for triplet label {}".format(triplet_label))

    input_mesh_path = matching_input_meshes[0]
    
    input_filename = os.path.split(input_mesh_path)[-1]
    
    out_mesh_path = os.path.join(out_hl_and_sparse_meshes_dir, input_filename)
    
    if not os.path.isfile(out_mesh_path):
        print("TODO: make combined holoroom and sparse meshes for {}".format(triplet_label))
        
        pano_a_center = fns_to_poses[triplet_canonical_order[0]][:3,3]
        pano_b_center = fns_to_poses[triplet_canonical_order[1]][:3,3]
        pano_c_center = fns_to_poses[triplet_canonical_order[2]][:3,3]
        
        triplet_cubemap_centers = [pano_a_center, pano_b_center, pano_c_center]
        
        triplet_centroid = (pano_a_center + pano_b_center + pano_c_center) / 3.0
        
        init_triplet_world_points = []
        
        with open(input_mesh_path, 'rb') as triplet_data_file:
            reader = csv.reader(triplet_data_file, delimiter=' ')
            num_points = int(reader.next()[0])
            for i in range(num_points):
                pt_row = reader.next()
                world_pt = np.array([float(x) for x in pt_row])
                init_triplet_world_points.append(world_pt)
        
        print("initial num points: {}".format(len(init_triplet_world_points)))
        
        # Make convex hull from points projected to triplet centroid, then determine which are the largest triangles
        
        # always add a couple raycasts just to ensure that the origin is inside the convex hull
        init_ray_directions = np.array([[0.0,0.0,1.0], [0.0,0.0,-1.0], [0.0,1.0,0.0], [0.0,-1.0,0.0], [1.0,0.0,0.0], [-1.0,0.0,0.0]])
        init_ray_origins = np.tile(triplet_centroid, (len(init_ray_directions),1))
        
        intersect_res = intersector.intersects_id(init_ray_origins, init_ray_directions, return_locations=True, multiple_hits=False)
        intersect_res_locations = intersect_res[2]
        
        init_triplet_world_points = init_triplet_world_points + list(intersect_res_locations)
            
            
        while len(init_triplet_world_points) < 6:
            # still not enough. shoot random points
            
            random_ray_directions = np.random.normal(size=(6, 3)) 
            random_ray_directions /= np.linalg.norm(random_ray_directions, axis=1)[:, np.newaxis]
            
            random_ray_origins = np.tile(triplet_centroid, (len(random_ray_directions),1))
            
            intersect_res = intersector.intersects_id(random_ray_origins, random_ray_directions, return_locations=True, multiple_hits=False)
            intersect_res_locations = intersect_res[2]
            
            init_triplet_world_points = init_triplet_world_points + list(intersect_res_locations)
    
            
        init_triplet_world_points = np.array(init_triplet_world_points)
        
        # project points to unit circle located at triplet centroid
        init_triplet_projected_points = init_triplet_world_points - triplet_centroid
        init_triplet_projected_points /= np.linalg.norm(init_triplet_projected_points, axis=1, keepdims=True)
        
        incremental_projected_hull = ConvexHull(init_triplet_projected_points, incremental=True)
    
        all_new_world_points = [] # additional points to add
        
                               
        is_centroid_inside_hull = False
        while not is_centroid_inside_hull:
            # we need to make sure that the convex hull contains the triplet centroid, or else this won't converge.
            
            
            centroid_of_hull = np.mean(incremental_projected_hull.points, axis=0)    # not the same as the centroid of the triplet... this is just the geometric center of the generated centroid
        
            fixed_simplices, fixed_neighbors = fixSimplicesNeighbors(incremental_projected_hull.simplices, incremental_projected_hull.neighbors, incremental_projected_hull.points, centroid_of_hull)
            
            # check to see if the convex hull actually wraps around the centroid position... if not, then we need to add a big box and recalculate
            triplet_centroid_local = np.zeros(3)
            visibilities_for_centroid = calculateVisibilityOfTriangles(fixed_simplices, incremental_projected_hull.points, triplet_centroid_local)
            
            num_visible_for_centroid = np.count_nonzero(visibilities_for_centroid)
            
            if num_visible_for_centroid < len(fixed_simplices):
                # some were not visible, even to the triplet centroid! this means that the entire convex hull is located somewhere away from the centroid
                # this happens when there are only correspondences on one side
                
                # shoot rays in some random directions and add them to the hull (and the all_new_world_points list)
                
                random_ray_directions = np.random.normal(size=(6, 3)) 
                random_ray_directions /= np.linalg.norm(random_ray_directions, axis=1)[:, np.newaxis]
                
                random_ray_origins = np.tile(triplet_centroid, (len(random_ray_directions),1))
                
                intersect_res = intersector.intersects_id(random_ray_origins, random_ray_directions, return_locations=True, multiple_hits=False)
                intersect_res_locations = intersect_res[2]
                
                print("adding in {} new locations to try to make the centroid inside the hull".format(len(intersect_res_locations)))
                new_world_points = intersect_res_locations
                
                
                if len(new_world_points) > 0:
                    new_points_relative_to_triplet_centroid = new_world_points - np.tile(triplet_centroid, (len(new_world_points),1))
                    new_projected_points = new_points_relative_to_triplet_centroid / np.linalg.norm(new_points_relative_to_triplet_centroid, axis=1, keepdims=True)
                    
                    if len(all_new_world_points) == 0:
                        all_new_world_points = new_world_points
                    else:
                        all_new_world_points = np.vstack((all_new_world_points, new_world_points)) # add new world point to the list
                        
                    incremental_projected_hull.add_points(new_projected_points) # add the new projected point to the hull
                
            else:
                is_centroid_inside_hull = True
    
        print("centroid is inside hull")
    
    
    
    
    
    
    
    
    
    
        
        converged = False
                               
        print("generating raycast points, please wait...")
        
        num_fruitless_attempts = 0
                   
        while not converged:
            #print("num points in init_triplet_projected_points+all_new_world_points: {}".format(len(init_triplet_projected_points) + len(all_new_world_points)))
            #print("num points in convex hull: {}".format(len(incremental_projected_hull.points)))
            
            
            #print("num points: {}".format(len(incremental_projected_hull.points)))
            
            #centroid_of_hull = np.mean(incremental_projected_hull.points, axis=0)    # not the same as the centroid of the triplet... this is just the geometric center of the generated centroid
            #fixed_simplices, fixed_neighbors = fixSimplicesNeighbors(incremental_projected_hull.simplices, incremental_projected_hull.neighbors, incremental_projected_hull.points, centroid_of_hull)
            
            expanded_pts = incremental_projected_hull.points[incremental_projected_hull.simplices]
            a_s = expanded_pts[:,0]
            b_s = expanded_pts[:,1]
            c_s = expanded_pts[:,2]
            
            # https://math.stackexchange.com/questions/738236/how-to-calculate-the-area-of-a-triangle-abc-when-given-three-position-vectors-a
            triangle_areas = np.linalg.norm(np.cross(b_s - a_s, c_s - a_s), axis=1) / 2.0
            # triangle_areas is an Nx1 array where each value is the area of the corresponding triangle in hull.simplices
            
            face_idx_with_largest_area = np.argmax(triangle_areas)
            largest_area = triangle_areas[face_idx_with_largest_area]
            
            #print("largest area: {}".format(largest_area))
            
            tri_points_above_threshold = expanded_pts[triangle_areas > triangle_area_threshold]
            
            num_tris_above_threshold = len(tri_points_above_threshold)
            
            #print("num_tris_above_threshold: {}".format(num_tris_above_threshold))
            
            if num_tris_above_threshold < 1:
                converged = True
                break
            
            
            alpha = np.random.uniform(0.3,0.4,num_tris_above_threshold)
            beta = np.random.uniform(0.3,0.4,num_tris_above_threshold)
            gamma = 1.0 - alpha - beta
            
            #alpha = np.ones(num_tris_above_threshold) * (1.0/3.0)
            #beta = np.ones(num_tris_above_threshold) * (1.0/3.0)
            #gamma = np.ones(num_tris_above_threshold) * (1.0/3.0)
            
            center_point_in_overlarge_tris = (alpha).reshape(num_tris_above_threshold, -1) * tri_points_above_threshold[:,0] + (beta).reshape(num_tris_above_threshold, -1) * tri_points_above_threshold[:,1] + (gamma).reshape(num_tris_above_threshold, -1) * tri_points_above_threshold[:,2]
            center_ray_through_overlarge_tris = center_point_in_overlarge_tris / np.linalg.norm(center_point_in_overlarge_tris, axis=1, keepdims=True)
            
            ray_through_overlarge_tris = center_ray_through_overlarge_tris
            
            
            center_ray_origins = np.tile(triplet_centroid, (num_tris_above_threshold,1))
            
            intersect_res = intersector.intersects_id(center_ray_origins, center_ray_through_overlarge_tris, return_locations=True, multiple_hits=False)
            intersect_res_locations = intersect_res[2]
            
            """
            # if we get nothing out of the center point intersections, try a random set of points
            if len(intersect_res_locations) == 0:
                #print("no successful intersections through centers, trying random points instead")
                
                r1_s = np.random.random(num_tris_above_threshold)
                r2_s = np.random.random(num_tris_above_threshold)
                sqrt_r1_s = np.sqrt(r1_s)
                
                random_point_in_overlarge_tris = (1.0 - sqrt_r1_s).reshape(num_tris_above_threshold, -1) * tri_points_above_threshold[:,0] + (sqrt_r1_s * (1 - r2_s)).reshape(num_tris_above_threshold, -1) * tri_points_above_threshold[:,1] + (r2_s * sqrt_r1_s).reshape(num_tris_above_threshold, -1) * tri_points_above_threshold[:,2]
                random_ray_through_overlarge_tris = random_point_in_overlarge_tris / np.linalg.norm(random_point_in_overlarge_tris, axis=1, keepdims=True)
                
                ray_through_overlarge_tris = random_ray_through_overlarge_tris
                
                random_ray_origins = np.tile(triplet_centroid, (num_tris_above_threshold,1))
                
                #center_point_in_largest_tri = np.mean(largest_tri_points, axis=0)
                #center_ray_through_largest_tri = center_point_in_largest_tri / np.linalg.norm(center_point_in_largest_tri)
                
                intersect_res = intersector.intersects_id(random_ray_origins, random_ray_through_overlarge_tris, return_locations=True, multiple_hits=False)
                intersect_res_rayidxs = intersect_res[1]
                intersect_res_locations = intersect_res[2]
            """
            
            if len(intersect_res_locations) > 0:
                num_fruitless_attempts = 0 # reset it
                
                print("adding in {} new locations".format(len(intersect_res_locations)))
                new_world_points = intersect_res_locations
                
                
                new_points_relative_to_triplet_centroid = new_world_points - np.tile(triplet_centroid, (len(new_world_points),1))
                new_projected_points = new_points_relative_to_triplet_centroid / np.linalg.norm(new_points_relative_to_triplet_centroid, axis=1, keepdims=True)
                
                if len(all_new_world_points) == 0:
                    all_new_world_points = new_world_points
                else:
                    all_new_world_points = np.vstack((all_new_world_points, new_world_points)) # add new world point to the list
                    
                incremental_projected_hull.add_points(new_projected_points) # add the new projected point to the hull
            else:
                num_fruitless_attempts += 1
                print("{} consecutive fruitless attempts".format(num_fruitless_attempts))
                if num_fruitless_attempts > fruitless_attempt_limit:
                    print("no more intersections, giving up")
                    converged = True
                    break
                
            if len(all_new_world_points) > 10000:
                print("number of points is high enough, converged.")
                converged = True
                break
        
        #print(1/0)
        
        all_new_world_points = np.array(all_new_world_points)
        combined_world_points = np.vstack((init_triplet_world_points, all_new_world_points))
        
        
        
        combined_triplet_projected_points = combined_world_points - triplet_centroid
        combined_triplet_projected_points /= np.linalg.norm(combined_triplet_projected_points, axis=1, keepdims=True)
        combined_projected_hull = ConvexHull(combined_triplet_projected_points, incremental=False)
        
        centroid_of_hull = np.mean(combined_triplet_projected_points, axis=0)    # not the same as the centroid of the triplet... this is just the geometric center of the generated centroid
        fixed_simplices, fixed_neighbors = fixSimplicesNeighbors(combined_projected_hull.simplices, combined_projected_hull.neighbors, combined_triplet_projected_points, centroid_of_hull)
        
        #combined_projected_hull_mesh = trimesh.Trimesh(vertices = combined_triplet_projected_points, faces = fixed_simplices)
        #combined_world_hull_mesh = trimesh.Trimesh(vertices = combined_world_points, faces = fixed_simplices)
        
        writeOutputTripletMeshData(out_hl_and_sparse_meshes_dir, input_filename, combined_world_points, fixed_simplices)
                                         
        print("for triplet {}, generated a total of {} points from combining sparse features with holoroom raycasts".format(triplet_canonical_order, len(combined_world_points)))
        
    else:
        print("combined holoroom and sparse meshes for {} already exist. skipping.".format(triplet_label))