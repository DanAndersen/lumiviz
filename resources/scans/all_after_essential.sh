#!/bin/bash
# in bash ubuntu for windows

if [ $# -ne 1 ]; then
    echo $0: usage: all_after_essential scanlabel
    exit 1
fi

SCAN_LABEL=$1
#   SCAN_LABEL=20170413142827

echo "scratch_test_essential_matrix..."
python scratch_test_essential_matrix.py ${SCAN_LABEL}

echo "refine_holo_sphere_poses..."
python refine_holo_sphere_poses.py ${SCAN_LABEL}

echo "process_scan_data_part_2..."
python process_scan_data_part_2.py ${SCAN_LABEL}

echo "find_sparse_features_common_to_triplet..."
python find_sparse_features_common_to_triplet.py ${SCAN_LABEL}

echo "add_incremental_holoroom_points_to_sparse_triplet_mesh..."
python add_incremental_holoroom_points_to_sparse_triplet_mesh.py ${SCAN_LABEL}

echo "make_topology_consistent_generic..."
python make_topology_consistent_generic.py ${SCAN_LABEL} ${SCAN_LABEL}/triplet_meshes/holoroom_and_sparse

echo "done with all triplet data generation."