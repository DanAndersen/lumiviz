# -*- coding: utf-8 -*-
"""
Created on Fri Jan 26 12:53:37 2018

@author: ANDERSED

Given
- a base directory for a scan,
finds the sparse feature triplet meshes and the holoroom triplet meshes, and combines the points together.

This script does not do the topological consistency. That's for the make_topology_consistent_generic.py script.

"""

import argparse
import os
import process_scan_data_helpers
from holoscan_common import readFile, sphereImgPointToXYZUnitSphere, LineLineIntersect, GetPointsProjectedOntoTranslatedUnitSphere, writeOutputTripletMeshData
import csv
import numpy as np
import cv2
import glob
import matplotlib.pyplot as plt
from scipy.spatial import ConvexHull
import random
import Queue

os.chdir("E:\\Dev\\OpenGL\\lumiviz\\resources\\scans")

parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)

# =============================================================================

# Load the refined poses for each frame number.

fn_to_pose_refined_path = os.path.join(args.base_dir, "fn_to_pose_refined.csv")

if not os.path.isfile(fn_to_pose_refined_path):
    raise Exception("Missing refined poses at {}".format(fn_to_pose_refined_path))

fns_to_poses = {}   # contains matrix [R|t] where [R|t] * X_local = X_world

with open(fn_to_pose_refined_path, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        fn = int(row[0]) # 0-indexed
        mat = np.array([float(x) for x in row[1:]]).reshape((4,4))
        fns_to_poses[fn] = mat

# =============================================================================

# Load the triangulation.

refined_frame_number_triangulation_path = os.path.join(args.base_dir, "sphere_pose_triangulation_refined_frame_numbers.yaml")

if not os.path.isfile(refined_frame_number_triangulation_path):
    raise Exception("Missing refined sphere pose triangulation at {}".format(refined_frame_number_triangulation_path))

refined_triangulation_frame_numbers = readFile(refined_frame_number_triangulation_path)["triangulation_indices"]

# =============================================================================

#in_sparse_mesh_dir = os.path.join(args.base_dir, "triplet_meshes", "sparse", "consistent_topology")
#in_holoroom_mesh_dir = os.path.join(args.base_dir, "triplet_meshes", "holoroom", "consistent_topology")
in_sparse_mesh_dir = os.path.join(args.base_dir, "triplet_meshes", "sparse")
in_holoroom_mesh_dir = os.path.join(args.base_dir, "triplet_meshes", "holoroom")

out_merged_meshes_dir = os.path.join(args.base_dir, "triplet_meshes", "holoroom_and_sparse")

if not os.path.exists(out_merged_meshes_dir):
    os.makedirs(out_merged_meshes_dir)




for triplet in refined_triangulation_frame_numbers:
    
    triplet_canonical_order = sorted(triplet) # used for labeling the triplet uniquely
    
    triplet_label = "{}_{}_{}".format(triplet_canonical_order[0], triplet_canonical_order[1], triplet_canonical_order[2])
    
    out_mesh_filename = "triplet_{}.meshes".format(triplet_label)
    
    out_merged_mesh_path = os.path.join(out_merged_meshes_dir, out_mesh_filename)
    
    if not os.path.isfile(out_merged_mesh_path):
        in_sparse_mesh_path = os.path.join(in_sparse_mesh_dir, "triplet_{}.meshes".format(triplet_label))
        in_holoroom_mesh_path = os.path.join(in_holoroom_mesh_dir, "triplet_{}.meshes".format(triplet_label))    
    
        if os.path.isfile(in_sparse_mesh_path) and os.path.isfile(in_holoroom_mesh_path):
            
            pano_a_center = fns_to_poses[triplet_canonical_order[0]][:3,3]
            pano_b_center = fns_to_poses[triplet_canonical_order[1]][:3,3]
            pano_c_center = fns_to_poses[triplet_canonical_order[2]][:3,3]
            
            triplet_cubemap_centers = [pano_a_center, pano_b_center, pano_c_center]
        
            triplet_centroid = (pano_a_center + pano_b_center + pano_c_center) / 3.0
            
            triplet_world_points = []
            
            # Load in the sparse points.
            with open(in_sparse_mesh_path, 'rb') as triplet_data_file:
                reader = csv.reader(triplet_data_file, delimiter=' ')
                num_points = int(reader.next()[0])
                for i in range(num_points):
                    pt_row = reader.next()
                    world_pt = np.array([float(x) for x in pt_row])
                    triplet_world_points.append(world_pt)
                    
            # Load in the holoroom points.
            with open(in_holoroom_mesh_path, 'rb') as triplet_data_file:
                reader = csv.reader(triplet_data_file, delimiter=' ')
                num_points = int(reader.next()[0])
                for i in range(num_points):
                    pt_row = reader.next()
                    world_pt = np.array([float(x) for x in pt_row])
                    triplet_world_points.append(world_pt)
            
            triplet_world_points = np.array(triplet_world_points)
            
            valid_pts_projected_to_pano_world = GetPointsProjectedOntoTranslatedUnitSphere(triplet_world_points, triplet_centroid)
            hull = ConvexHull(valid_pts_projected_to_pano_world)
            
            """
            centroid_of_hull = np.mean(valid_pts_projected_to_pano_world, axis=0)    # not the same as the centroid of the triplet... this is just the geometric center of the generated centroid
            fixed_simplices, fixed_neighbors = fixSimplicesNeighbors(hull.simplices, hull.neighbors, valid_pts_projected_to_pano_world, centroid_of_hull)
            
            # To visualize the mesh easily...
            #test_hull_mesh = trimesh.Trimesh(vertices = valid_pts_projected_to_pano_world, faces = fixed_simplices)
            #test_hull_mesh = trimesh.Trimesh(vertices = unoccluded_hit_vert_positions_world, faces = fixed_simplices)
            #test_hull_mesh.show()
            """
            
            unprojected_pts = triplet_world_points
            
            world_correspondence_points_projected_to_centroid_sphere = GetPointsProjectedOntoTranslatedUnitSphere(unprojected_pts, triplet_centroid)
            
            pts = world_correspondence_points_projected_to_centroid_sphere
            
            if len(pts) > 0:
                world_centroid_hull = ConvexHull(pts)
                simplices = world_centroid_hull.simplices
            else:
                simplices = np.array([])
            
            writeOutputTripletMeshData(out_merged_meshes_dir, out_mesh_filename, unprojected_pts, simplices)
                                                 
            print("for triplet {}, generated a total of {} points from holoroom raycasts".format(triplet_canonical_order, len(triplet_world_points)))
        else:
            raise Exception("missing input sparse or holoroom meshes for triplet {}".format(triplet_label))
    else:
        print("Already have merged meshes at {}. Skipping.".format(out_merged_mesh_path))
    