# -*- coding: utf-8 -*-
"""
Created on Mon Oct 09 17:59:18 2017

@author: ANDERSED
"""

# given a set of pano-cubemaps, and a YAML containing the triangulation from the sphere pose,
# generate features for each of the poses used in the triangulation

import numpy as np
import cv2
import os
import glob
import matplotlib.pyplot as plt
import sys
import argparse
import ntpath
import yaml
import csv
import ntpath

TRIANGULATION_FILENAME = "sphere_pose_triangulation.yaml"


def saveFile(fname,data):
	fd = open(fname,"wb")
	yaml.dump(data,fd)
	fd.close()

def readFile(fname):
	fd = open(fname,"rb")
	data = yaml.load(fd)
	return data

parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")
parser.add_argument("num_features", type=int, help="number of features to detect")

args = parser.parse_args()

print(args)


triangulation_data = readFile(os.path.join(args.base_dir, TRIANGULATION_FILENAME))

triangulation_indices = triangulation_data["triangulation_indices"]




cubemap_in_dir = os.path.join(args.base_dir, "pano_frames\\cubemaps")

orb = cv2.ORB_create(nfeatures=args.num_features)

print("Loading in cubemaps from {}".format(cubemap_in_dir))

out_features_dir = os.path.join(cubemap_in_dir, "features")
if not os.path.exists(out_features_dir):
    os.makedirs(out_features_dir)

for triplet in triangulation_indices:
    for index in triplet:
        cubemap_path = os.path.join(cubemap_in_dir, "pano_frame{}_cubemap.jpg".format("%05d" % (index + 1,)))
        
        basename = ntpath.basename(cubemap_path).replace(".jpg","")
        
        out_feature_filepath = os.path.join(out_features_dir, "{}_features.yaml".format(basename))
        
        if not os.path.isfile(out_feature_filepath):
            
            print("loading in {}".format(basename))
    
            cubemap = cv2.imread(cubemap_path)
            
            cubemap_side_size = cubemap.shape[0]
            
            cubemap_feature_data = {
                    "sides": []
                    }
            
            for i in range(6):
                
                cubemap_side = cubemap[:,i*cubemap_side_size:(i+1)*cubemap_side_size]
                
                kp, des = orb.detectAndCompute(cubemap_side, None)
                
                cubemap_side_with_kp = cv2.drawKeypoints(cubemap_side, kp, None, color=(0,255,0), flags=0)
                cv2.imshow("cubemap_side_with_kp_{}".format(i), cubemap_side_with_kp)
                cv2.waitKey(1)
                
                # can't just save 'kp' to file, as the data is lost. 
                # https://stackoverflow.com/questions/10045363/pickling-cv2-keypoint-causes-picklingerror
                kp_list = []
                for kp_idx in range(len(kp)):
                    point = kp[kp_idx]
                    descriptor = des[kp_idx]
                    temp = (point.pt, point.size, point.angle, point.response, point.octave, point.class_id, descriptor) 
                    kp_list.append(temp)
                
                cubemap_feature_data["sides"].append(kp_list)
            
            
            saveFile(out_feature_filepath, cubemap_feature_data) 

cv2.destroyAllWindows()