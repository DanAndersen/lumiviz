# -*- coding: utf-8 -*-
"""
Created on Thu Nov 16 15:53:18 2017

@author: ANDERSED
"""

# given a sample triplet, try to rectify the cubemap faces to do stereo maps

from holoscan_common import Camera, loadFullCubemapImgForFrameNumber, loadRefinedCubemapData, readFile, loadRefinedTriangulationData
import cv2
import argparse
import matplotlib.pyplot as plt
import numpy as np
import os

parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)

refined_triangulation_data = loadRefinedTriangulationData(args.base_dir)

test_frame_numbers = refined_triangulation_data[0]







frame_number_i = test_frame_numbers[0]
frame_number_j = test_frame_numbers[1]
side_idx_i = 0
side_idx_j = 0






cubemap_img_i = loadFullCubemapImgForFrameNumber(args.base_dir, frame_number_i)
cubemap_img_j = loadFullCubemapImgForFrameNumber(args.base_dir, frame_number_j)


cube_side_pixels = cubemap_img_i.shape[0]

side_img_i = cubemap_img_i[:,side_idx_i*cube_side_pixels:(side_idx_i+1)*cube_side_pixels]
side_img_j = cubemap_img_j[:,side_idx_j*cube_side_pixels:(side_idx_j+1)*cube_side_pixels]


(frame_numbers_to_cubemap_cameras, frame_numbers_to_transformation_matrices, frame_numbers_to_inverse_transformation_matrices) = loadRefinedCubemapData(args.base_dir)

imageSize = (cube_side_pixels,cube_side_pixels)

cubemapSideCameraMatrix = np.identity(3)
cubemapSideCameraMatrix[0][0] = cube_side_pixels/2.0
cubemapSideCameraMatrix[1][1] = cube_side_pixels/2.0
cubemapSideCameraMatrix[0][2] = cube_side_pixels/2.0
cubemapSideCameraMatrix[1][2] = cube_side_pixels/2.0

cameraMatrix1 = cubemapSideCameraMatrix
cameraMatrix2 = cubemapSideCameraMatrix

distCoeffs1 = np.zeros(5)
distCoeffs2 = np.zeros(5)

Rt_i_inv = frame_numbers_to_inverse_transformation_matrices[frame_number_i]
Rt_j = frame_numbers_to_transformation_matrices[frame_number_j]

Rt_j_inv = frame_numbers_to_inverse_transformation_matrices[frame_number_j]
Rt_i = frame_numbers_to_transformation_matrices[frame_number_i]

#Rt = np.matmul(Rt_j, Rt_i_inv)
#Rt = np.matmul(Rt_i_inv, Rt_j)

#Rt = np.matmul(Rt_i, Rt_j_inv)
Rt = np.matmul(Rt_j_inv, Rt_i)


R = Rt[:3,:3]
T = Rt[:3,3]

R1, R2, P1, P2, Q, validPixROI1, validPixROI2 = cv2.stereoRectify(cameraMatrix1, distCoeffs1, cameraMatrix2, distCoeffs2, imageSize, R, T)

map1_i, map2_i = cv2.initUndistortRectifyMap(cameraMatrix1, distCoeffs1, R1, cameraMatrix1, imageSize, cv2.CV_32FC1)
map1_j, map2_j = cv2.initUndistortRectifyMap(cameraMatrix2, distCoeffs2, R2, cameraMatrix2, imageSize, cv2.CV_32FC1)

comparison_img = np.zeros((cube_side_pixels, cube_side_pixels*2, 3), dtype=np.uint8)
comparison_img[:,:cube_side_pixels] = cv2.remap(side_img_i, map1_i, map2_i, cv2.INTER_LINEAR)
comparison_img[:,cube_side_pixels:] = cv2.remap(side_img_j, map1_j, map2_j, cv2.INTER_LINEAR)


num_lines = 10
for i in range(num_lines):
    y = int(float(cube_side_pixels) / num_lines * i)
    cv2.line(comparison_img, (0,y), (cube_side_pixels*2, y), (0,255,0))

plt.imshow(comparison_img)