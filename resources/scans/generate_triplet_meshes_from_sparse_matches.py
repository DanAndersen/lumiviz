# -*- coding: utf-8 -*-
"""
Created on Thu Jan 25 12:37:59 2018

@author: ANDERSED

Given the refined poses and the new triangulation of triplets,
use the SPHORB matches to make .meshes files for each triplet using only the
sparse matches.

"""

raise Exception("WARNING: this script is no longer being used! use find_sparse_features_common_to_triplet.py instead!")









import argparse
import os
import process_scan_data_helpers
from holoscan_common import readFile, sphereImgPointToXYZUnitSphere, LineLineIntersect, GetPointsProjectedOntoTranslatedUnitSphere, writeOutputTripletMeshData
import csv
import numpy as np
import cv2
import glob
import matplotlib.pyplot as plt
from scipy.spatial import ConvexHull
import time
import profile

os.chdir("E:\\Dev\\OpenGL\\lumiviz\\resources\\scans")


parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)

print("Base scan directory: {}".format(args.base_dir))

SCAN_LABEL = os.path.split(args.base_dir)[-1]

print("SCAN_LABEL: {}".format(SCAN_LABEL))

# =============================================================================

fn_to_pose_refined_path = os.path.join(args.base_dir, "fn_to_pose_refined.csv")

if not os.path.isfile(fn_to_pose_refined_path):
    raise Exception("Missing refined poses at {}".format(fn_to_pose_refined_path))
    
refined_frame_number_triangulation_path = os.path.join(args.base_dir, "sphere_pose_triangulation_refined_frame_numbers.yaml")

if not os.path.isfile(refined_frame_number_triangulation_path):
    raise Exception("Missing refined sphere pose triangulation at {}".format(refined_frame_number_triangulation_path))

# =============================================================================

pano_frame_dir = os.path.join(args.base_dir, "pano_frames")

# Determine size of panoramas.
pano_frame_resolution = cv2.imread(glob.glob(os.path.join(pano_frame_dir, "pano_frame*.jpg"))[0]).shape[::-1][1:]

# =============================================================================

# Load the refined poses for each frame number.

fns_to_poses = {}   # contains matrix [R|t] where [R|t] * X_local = X_world

with open(fn_to_pose_refined_path, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        fn = int(row[0]) # 0-indexed
        mat = np.array([float(x) for x in row[1:]]).reshape((4,4))
        fns_to_poses[fn] = mat
    
# =============================================================================

def drawMatchesThreshold(img_i, img_j, kp_i, kp_j, vals, threshold_val):
    img_combined = np.concatenate((img_i, img_j), axis=0)
    
    thickness = 1
    
    num_matches = len(kp_i)
    for match_idx in range(num_matches):
        val = vals[match_idx]
        color = (0,0,255) if val > threshold_val else (0,255,0)
        
        end1 = tuple(np.round(kp_i[match_idx]).astype(int))
        end2 = tuple(np.round(kp_j[match_idx]).astype(int) + np.array([0, img_i.shape[0]]))
        
        cv2.line(img_combined, end1, end2, color, thickness)
    
    return img_combined

def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)

def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))

out_triplet_sparse_dir = os.path.join(args.base_dir, "triplet_meshes", "sparse")
if not os.path.exists(out_triplet_sparse_dir):
    os.makedirs(out_triplet_sparse_dir)

refined_triangulation_frame_numbers = readFile(refined_frame_number_triangulation_path)["triangulation_indices"]





# =============================================================================

i_j_to_good_world_points = {}

def generateTripletMeshesFromSparseFeatures(triplet):
    triplet_canonical_order = sorted(triplet) # used for labeling the triplet uniquely
    
    triplet_label = "{}_{}_{}".format(triplet_canonical_order[0], triplet_canonical_order[1], triplet_canonical_order[2])
    
    out_triplet_sparse_mesh_filename = "triplet_{}.meshes".format(triplet_label)
    
    pano_a_center = fns_to_poses[triplet_canonical_order[0]][:3,3]
    pano_b_center = fns_to_poses[triplet_canonical_order[1]][:3,3]
    pano_c_center = fns_to_poses[triplet_canonical_order[2]][:3,3]
    
    triplet_centroid = (pano_a_center + pano_b_center + pano_c_center) / 3.0
    
    if not os.path.isfile(os.path.join(out_triplet_sparse_dir, out_triplet_sparse_mesh_filename)):
        start_time = time.time()
        
        num_pairs_in_triplet_with_matches = 0
        
        correspondence_points_world = []
        
        for a in range(3):
            
            b = (a+1)%3
            fn_a = triplet[a] # 0-indexed
            fn_b = triplet[b]
            
            # fn_i is always less than fn_j
            fn_i = min(fn_a, fn_b)
            fn_j = max(fn_a, fn_b)
            
            mat_i = fns_to_poses[fn_i]
            mat_j = fns_to_poses[fn_j]
            
            sphorb_match_path = os.path.join(args.base_dir, "pano_frames", "sphorb_matches", "sphorb_match_%05d_%05d.csv" % (fn_i + 1, fn_j + 1)) # go to 1-indexed
            
            if not os.path.isfile(sphorb_match_path):
                raise Exception("missing sphorb match: {}".format(sphorb_match_path))
            if (fn_i, fn_j) in i_j_to_good_world_points:
                good_world_points = i_j_to_good_world_points[(fn_i, fn_j)]
            else:
                good_world_points = []
            
                with open(sphorb_match_path, 'rb') as csvfile:
                    reader = csv.reader(csvfile, delimiter=',')
                    for row in reader:
                        # Get image points 
                        img_pt_fn_i = [float(x) for x in row[0:2]]
                        img_pt_fn_j = [float(x) for x in row[2:4]]
                        
                        # the unit sphere vectors of the matches, in the coordinate systems local to each pano
                        xyz_pt_fn_i = sphereImgPointToXYZUnitSphere(img_pt_fn_i[0], img_pt_fn_i[1], pano_frame_resolution)
                        xyz_pt_fn_j = sphereImgPointToXYZUnitSphere(img_pt_fn_j[0], img_pt_fn_j[1], pano_frame_resolution)
                        
                        ray_i_start_world = np.dot(mat_i, np.array([0.0, 0.0, 0.0, 1.0]))
                        ray_j_start_world = np.dot(mat_j, np.array([0.0, 0.0, 0.0, 1.0]))
                        
                        ray_i_end_world = np.dot(mat_i, np.append(xyz_pt_fn_i, 1.0))                    
                        ray_j_end_world = np.dot(mat_j, np.append(xyz_pt_fn_j, 1.0))
                        
                        intersect_res, pa, pb = LineLineIntersect(ray_i_start_world, ray_i_end_world, ray_j_start_world, ray_j_end_world)
                
                        if intersect_res:
                            
                            midpoint_world = (pa+pb)/2.0
                                       
                            midpoint_projected_on_i = midpoint_world - ray_i_start_world[0:3]
                            midpoint_projected_on_i = midpoint_projected_on_i / np.linalg.norm(midpoint_projected_on_i)
                            
                            midpoint_projected_on_j = midpoint_world - ray_j_start_world[0:3]
                            midpoint_projected_on_j = midpoint_projected_on_j / np.linalg.norm(midpoint_projected_on_j)
                            
                            
                            angle_between_i_and_midpoint = angle_between(ray_i_end_world[0:3] - ray_i_start_world[0:3], midpoint_projected_on_i)
                            angle_between_j_and_midpoint = angle_between(ray_j_end_world[0:3] - ray_j_start_world[0:3], midpoint_projected_on_j)
                            
                            
                            val = max(angle_between_i_and_midpoint, angle_between_j_and_midpoint)
                            
                            if val < 0.01:
                                # reprojection error is so small that the correspondence is probably good
                                correspondence_points_world.append(midpoint_world)
                                good_world_points.append(midpoint_world)
                            
                i_j_to_good_world_points[(fn_i, fn_j)] = good_world_points
            
            for good_world_point in good_world_points:
                correspondence_points_world.append(good_world_point)
            
            num_pairs_in_triplet_with_matches += 1
        
        correspondence_points_world = np.array(correspondence_points_world)
        
        unprojected_pts = correspondence_points_world
        
        world_correspondence_points_projected_to_centroid_sphere = GetPointsProjectedOntoTranslatedUnitSphere(unprojected_pts, triplet_centroid)
        
        pts = world_correspondence_points_projected_to_centroid_sphere
        
        if len(pts) > 0:
            world_centroid_hull = ConvexHull(pts)
            simplices = world_centroid_hull.simplices
        else:
            simplices = np.array([])
        
        writeOutputTripletMeshData(out_triplet_sparse_dir, out_triplet_sparse_mesh_filename, unprojected_pts, simplices)
             

        elapsed_time = time.time() - start_time                            
        print("for triplet {}, generated a total of {} sparse points ( took {} sec )".format(triplet_canonical_order, len(correspondence_points_world), elapsed_time))
        
    else:
        print("skipping generation of sparse triplet mesh for triplet {}".format(triplet_canonical_order))


# =============================================================================



for triplet in refined_triangulation_frame_numbers:
    generateTripletMeshesFromSparseFeatures(triplet)
