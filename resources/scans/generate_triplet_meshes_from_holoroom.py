# -*- coding: utf-8 -*-
"""
Created on Sun Jan 28 12:03:38 2018

@author: ANDERSED

Creates the .meshes files for each triplet in the triangulation, using only the HoloLens geometry as input.

Given the HoloLens geometry, determine which vertices are visible to all 3 triplet panos for each triplet.

The output is a series of .meshes files which may not be guaranteed to have consistent topology.

"""

import argparse
import os
import process_scan_data_helpers
from holoscan_common import readFile, sphereImgPointToXYZUnitSphere, LineLineIntersect, GetPointsProjectedOntoTranslatedUnitSphere, writeOutputTripletMeshData, fixSimplicesNeighbors
import csv
import numpy as np
import glob
import matplotlib.pyplot as plt
from scipy.spatial import ConvexHull
import trimesh
import time

import profile

os.chdir("E:\\Dev\\OpenGL\\lumiviz\\resources\\scans")


parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)

print("Base scan directory: {}".format(args.base_dir))

SCAN_LABEL = os.path.split(args.base_dir)[-1]

print("SCAN_LABEL: {}".format(SCAN_LABEL))

# =============================================================================

# Load in the HoloLens room geometry.

holoroom_file_path = glob.glob(os.path.join(args.base_dir, "*.room"))[0]

print("Reading HoloLens room geometry from {}...".format(holoroom_file_path))
holoroom_mesh_verts, holoroom_mesh_indices = process_scan_data_helpers.load_holoroom_mesh(holoroom_file_path)
print("Read HoloLens room geometry, {} verts and {} triangles".format(len(holoroom_mesh_verts), len(holoroom_mesh_indices)))

num_holoroom_verts = len(holoroom_mesh_verts)

# =============================================================================

# Create Trimesh object.

print("creating trimesh for holoroom")
holoroom_mesh = trimesh.Trimesh(vertices = holoroom_mesh_verts, faces = holoroom_mesh_indices)
print("done creating trimesh")

# can view mesh in 3d with
# holoroom_mesh.show() # slow to load

# =============================================================================

# setting up the ray-mesh intersector
print("Setting up ray-mesh intersector for holoroom...")
#intersector = trimesh.ray.ray_triangle.RayMeshIntersector(holoroom_mesh)
intersector = holoroom_mesh.ray
print("intersector created.")

# =============================================================================

# Load in the refined triplet-triangulation.

print("Loading refined sphere-camera poses and triplet triangulation...")

fn_to_pose_refined_path = os.path.join(args.base_dir, "fn_to_pose_refined.csv")

if not os.path.isfile(fn_to_pose_refined_path):
    raise Exception("Missing refined poses at {}".format(fn_to_pose_refined_path))
    
refined_frame_number_triangulation_path = os.path.join(args.base_dir, "sphere_pose_triangulation_refined_frame_numbers.yaml")

if not os.path.isfile(refined_frame_number_triangulation_path):
    raise Exception("Missing refined sphere pose triangulation at {}".format(refined_frame_number_triangulation_path))
    
# Load the refined poses for each frame number.

fns_to_poses = {}   # contains matrix [R|t] where [R|t] * X_local = X_world

with open(fn_to_pose_refined_path, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        fn = int(row[0]) # 0-indexed
        mat = np.array([float(x) for x in row[1:]]).reshape((4,4))
        fns_to_poses[fn] = mat

refined_triangulation_frame_numbers = readFile(refined_frame_number_triangulation_path)["triangulation_indices"]

print("Camera poses and triplet triangulation loaded.")

# =============================================================================

def generateHoloroomTripletMesh(triplet_label, triplet_canonical_order, out_triplet_holoroom_mesh_filename):
    print("processing {}".format(triplet_label))
        
    pano_a_center = fns_to_poses[triplet_canonical_order[0]][:3,3]
    pano_b_center = fns_to_poses[triplet_canonical_order[1]][:3,3]
    pano_c_center = fns_to_poses[triplet_canonical_order[2]][:3,3]
    
    triplet_centroid = np.mean(np.array([fns_to_poses[triplet_fn][:3,3] for triplet_fn in triplet_canonical_order]), axis=1)

    ray_origins = np.tile(triplet_centroid, (num_random_points,1))
    
    # Generate random points on the surface of a unit sphere
    ray_directions = np.random.normal(size=(num_random_points, 3)) 
    ray_directions /= np.linalg.norm(ray_directions, axis=1)[:, np.newaxis]
    
    print("Starting ray intersections...")
    start_time = time.time()
    intersect_res = intersector.intersects_id(ray_origins, ray_directions, return_locations=True, multiple_hits=False)
    elapsed_time = time.time() - start_time
    print("Did ray intersections, took {} seconds".format(elapsed_time))
    
    intersect_res_indextri = intersect_res[0]
    intersect_res_indexray = intersect_res[1]   # ray indices after decimation
    intersect_res_locations = intersect_res[2]

    unoccluded_hit_vert_positions_world = intersect_res_locations
    
    valid_pts_projected_to_pano_world = GetPointsProjectedOntoTranslatedUnitSphere(unoccluded_hit_vert_positions_world, triplet_centroid)
    hull = ConvexHull(valid_pts_projected_to_pano_world)
    
    centroid_of_hull = np.mean(valid_pts_projected_to_pano_world, axis=0)    # not the same as the centroid of the triplet... this is just the geometric center of the generated centroid
    fixed_simplices, fixed_neighbors = fixSimplicesNeighbors(hull.simplices, hull.neighbors, valid_pts_projected_to_pano_world, centroid_of_hull)
    
    # To visualize the mesh easily...
    #test_hull_mesh = trimesh.Trimesh(vertices = valid_pts_projected_to_pano_world, faces = fixed_simplices)
    #test_hull_mesh = trimesh.Trimesh(vertices = unoccluded_hit_vert_positions_world, faces = fixed_simplices)
    #test_hull_mesh.show()
    
    
    unprojected_pts = unoccluded_hit_vert_positions_world
    
    world_correspondence_points_projected_to_centroid_sphere = GetPointsProjectedOntoTranslatedUnitSphere(unprojected_pts, triplet_centroid)
    
    pts = world_correspondence_points_projected_to_centroid_sphere
    
    if len(pts) > 0:
        world_centroid_hull = ConvexHull(pts)
        simplices = world_centroid_hull.simplices
    else:
        simplices = np.array([])
    
    writeOutputTripletMeshData(out_triplet_holoroom_dir, out_triplet_holoroom_mesh_filename, unprojected_pts, simplices)
                                         
    print("for triplet {}, generated a total of {} points from holoroom raycasts".format(triplet_canonical_order, len(unoccluded_hit_vert_positions_world)))

# =============================================================================

out_triplet_holoroom_dir = os.path.join(args.base_dir, "triplet_meshes", "holoroom")
if not os.path.exists(out_triplet_holoroom_dir):
    os.makedirs(out_triplet_holoroom_dir)

num_random_points = 2000

for triplet in refined_triangulation_frame_numbers:
    
    triplet_canonical_order = sorted(triplet) # used for labeling the triplet uniquely
    
    triplet_label = "{}_{}_{}".format(triplet_canonical_order[0], triplet_canonical_order[1], triplet_canonical_order[2])
    
    out_triplet_holoroom_mesh_filename = "triplet_{}.meshes".format(triplet_label)
    
    if not os.path.isfile(os.path.join(out_triplet_holoroom_dir, out_triplet_holoroom_mesh_filename)):
        generateHoloroomTripletMesh(triplet_label, triplet_canonical_order, out_triplet_holoroom_mesh_filename)
        #foo, bar = profile.run('generateHoloroomTripletMesh(triplet_label, triplet_canonical_order, out_triplet_holoroom_mesh_filename)', sort='tottime')
    else:
        print("meshes file for triplet {} already exists. skipping.".format(triplet_label))