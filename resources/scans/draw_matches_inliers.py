# -*- coding: utf-8 -*-
"""
Created on Tue Dec 05 14:08:45 2017

@author: ANDERSED
"""

# test loading in some correspondences found via SPHORB and determining the essential matrix from them


import numpy as np
import cv2
import os
import glob
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import sys
import argparse
import ntpath
import yaml
import csv
import ntpath
import re
from holoscan_common import saveFile, readFile
import time
import profile
import scipy

pano_frame_resolution = (3840.0, 1920.0)

def loadPanoSphereImg(zero_indexed_frame_number):
    return cv2.imread(os.path.join(pano_frame_dir, "pano_frame%05d.jpg" % (zero_indexed_frame_number + 1,)))

def sphereImgPointToXYZUnitSphere(img_x, img_y):
    # returns an XYZ point on the unit sphere, where -z is center, +y is north pole
    
    w = pano_frame_resolution[0]
    h = pano_frame_resolution[1]
    
    lon_radians = (-2.0 * np.pi / w) * img_x + np.pi
    lat_radians = (-np.pi / h) * img_y + (np.pi/2.0)

    y = np.sin(lat_radians)
    
    x = - np.cos(lat_radians) * np.sin(lon_radians)
    z = - np.cos(lat_radians) * np.cos(lon_radians)
    
    return np.array([x,y,z])




def drawMatches(frame_number_i, frame_number_j, all_matches, inlier_indices = None):
    img_i = loadPanoSphereImg(frame_number_i)
    img_j = loadPanoSphereImg(frame_number_j)
    
    kp_i = [[m[0], m[1]] for m in all_matches]
    kp_j = [[m[2], m[3]] for m in all_matches]
    
    if inlier_indices is None:
        inliers = range(len(all_matches))
    else:
        inliers = inlier_indices
    
    stackedImg = np.concatenate((img_i, img_j), axis=0)
    
    for inlier_idx in inliers:
        pt_i = [int(x) for x in kp_i[inlier_idx]]
        
        
        pt_j = [int(x) for x in kp_j[inlier_idx]]
        
        pt_j = [pt_j[0], pt_j[1] + int(pano_frame_resolution[1])]
        
        
        color = (np.random.rand(3) * 256).astype(np.int)
        thickness = 2
        
        cv2.line(stackedImg, tuple(pt_i), tuple(pt_j), color, thickness)
    
    return stackedImg



parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)





pano_frame_dir = os.path.join(args.base_dir, "pano_frames")

input_sphorb_match_paths = glob.glob(os.path.join(pano_frame_dir, "sphorb_matches", "sphorb_match_[0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9].csv"))


for idx, input_sphorb_match_path in enumerate(input_sphorb_match_paths):
    csv_filename = os.path.split(input_sphorb_match_path)[-1]
    print("reading in {}...".format(csv_filename))
    
    
    matches = []
    
    with open(input_sphorb_match_path, 'rb') as f:
        reader = csv.reader(f, delimiter=',')
        for row in reader:
            match = [float(x) for x in row]
            matches.append(match)
    
    zero_indexed_frame_numbers = [int(x) - 1 for x in re.findall(r'\d+', csv_filename)] # go from 1-indexed to 0-indexed
    frame_numbers_ij = zero_indexed_frame_numbers

    input_essential_path = os.path.join(pano_frame_dir, "sphorb_matches", "sphorb_match_%05d_%05d_essential.yaml" % (frame_numbers_ij[0] + 1, frame_numbers_ij[1] + 1))


    essential_data = readFile(input_essential_path)
    
    best_fit_inlier_indices = essential_data['best_fit_inlier_indices']


    imgMatches = drawMatches(frame_numbers_ij[0], frame_numbers_ij[1], matches, best_fit_inlier_indices)
    cv2.namedWindow("imgMatches", cv2.WINDOW_NORMAL)
    cv2.imshow("imgMatches", imgMatches)
    cv2.resizeWindow("imgMatches", 600,600)
    cv2.waitKey(1)
    
    
    out_img_dir = os.path.join(pano_frame_dir, "match_imgs")
    if not os.path.exists(out_img_dir):
        os.makedirs(out_img_dir)
    
    out_img_path = os.path.join(out_img_dir, "{}_{}.jpg".format(frame_numbers_ij[0], frame_numbers_ij[1]))
    cv2.imwrite(out_img_path, imgMatches)
     
    #print(1/0)


