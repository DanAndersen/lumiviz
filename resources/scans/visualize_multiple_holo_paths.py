# -*- coding: utf-8 -*-
"""
Created on Wed Oct 04 13:07:48 2017

@author: ANDERSED
"""

import yaml
import argparse
import sys
import os
import subprocess
import glob
from pyrr import quaternion, matrix44, vector3
from pyrr import Quaternion, Matrix33, Matrix44, Vector3
import numpy as np
import csv


# %matplotlib qt
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import proj3d
import numpy as np
import matplotlib.pyplot as plt


def saveFile(fname,data):
	fd = open(fname,"wb")
	yaml.dump(data,fd)
	fd.close()

def readFile(fname):
	fd = open(fname,"rb")
	data = yaml.load(fd)
	return data


parser = argparse.ArgumentParser()
parser.add_argument("scan_dir", help="base directory for the specific scan")
args = parser.parse_args()

print("Given input pose CSV (for Holo camera), visualize it so we can see if it's complete")

print("scan_dir = {}".format(args.scan_dir))



# now load in the hololens poses
in_pose_csv_path = glob.glob(os.path.join(args.scan_dir, "*_pose.csv"))[0]

holo_positions = []

out_spherical_csv_data = []

with open(in_pose_csv_path, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        timestamp = float(row[0])
        pos_x = float(row[1])
        pos_y = float(row[2])
        pos_z = float(row[3])
        rot_x = float(row[4])
        rot_y = float(row[5])
        rot_z = float(row[6])
        
        pos = np.array([pos_x, pos_y, -pos_z])  # because the pose data comes from Unity with its left-handed coords, flip this axis
        rot = np.array([-rot_x, -rot_y, rot_z]) # flip axes as needed for the rotation to be in a right-handed coord system too
        
        holo_positions.append(pos)
        
    
holo_positions = np.array(holo_positions)





fig = plt.figure()
ax = fig.gca(projection='3d')

# switching Y and Z so Y is the vertical axis
ax.plot(holo_positions[:,0], holo_positions[:,2], holo_positions[:,1], label='holo_positions')


max_range = 10.0

# Create cubic bounding box to simulate equal aspect ratio
max_range = np.array([max_range*2, max_range*2, max_range*2]).max()
Xb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][0].flatten()
Yb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][1].flatten()
Zb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][2].flatten()
# Comment or uncomment following both lines to test the fake bounding box:
for xb, yb, zb in zip(Xb, Yb, Zb):
   ax.plot([xb], [yb], [zb], 'w')




ax.legend()

plt.show()
