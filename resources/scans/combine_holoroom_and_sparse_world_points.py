# -*- coding: utf-8 -*-
"""
Created on Tue Nov 21 12:32:01 2017

@author: ANDERSED
"""

import argparse
import re
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import yaml
import os
import csv
import functools
from scipy.spatial import ConvexHull
from scipy.spatial import Delaunay
import cv2
from holoscan_common import Camera, loadFullCubemapImgForFrameNumber, loadRefinedCubemapData, saveFile, readFile, writeOutputTripletMeshData, GetPointsProjectedOntoTranslatedUnitSphere
import glob
import struct
import Queue
import random


# when we get the simplices for the convex hull, the triangles are mixed in CW and CCW winding order.
# we know that from the perspective of the hull centroid, all the triangles should be visible, and so in a consistent order.
def fixSimplicesNeighbors(unfixed_simplices, unfixed_neighbors, world_points_projected_to_centroid_sphere, centroid_of_hull):
    fixed_simplices = []
    fixed_neighbors = []
    
    projected_pts = world_points_projected_to_centroid_sphere
    
    projection_center = centroid_of_hull
    
    for i in range(len(unfixed_simplices)):
        unfixed_simplex = unfixed_simplices[i]
        
        unfixed_neighbor = unfixed_neighbors[i]
        pts_in_triangle = projected_pts[unfixed_simplex]
        a = pts_in_triangle[0]
        b = pts_in_triangle[1]
        c = pts_in_triangle[2]
        
        normal = np.cross(b-a, c-a)
        normal = normal / np.linalg.norm(normal)
        
        is_facing_center = (np.dot(normal, a-projection_center)) > 0
        if is_facing_center:
            fixed_simplices.append(unfixed_simplex)
            fixed_neighbors.append(unfixed_neighbor)
        else:
            fixed_simplex = np.fliplr([unfixed_simplex])[0]
            fixed_simplices.append( fixed_simplex )
            fixed_neighbor = np.fliplr([unfixed_neighbor])[0]
            fixed_neighbors.append(fixed_neighbor)
    fixed_simplices = np.array(fixed_simplices)
    fixed_neighbors = np.array(fixed_neighbors)
    return (fixed_simplices, fixed_neighbors)




def calculateVisibilityOfTriangles(simplices, projected_pts, projection_center):
    
    visibilities = []
    
    for simplex in simplices:
        pts_in_triangle = projected_pts[simplex]
        a = pts_in_triangle[0]
        b = pts_in_triangle[1]
        c = pts_in_triangle[2]
        
        normal = np.cross(b-a, c-a)
        normal = normal / np.linalg.norm(normal)
        
        is_facing_center = (np.dot(normal, a-projection_center)) > 0
        visibilities.append(is_facing_center)
    return visibilities



def generateHullData(pts, triplet_centroid, triplet_cubemap_centers):
    pts_projected_to_centroid_sphere = GetPointsProjectedOntoTranslatedUnitSphere(pts, triplet_centroid)
    
    pts_projected_to_each_pano = [GetPointsProjectedOntoTranslatedUnitSphere(pts, c) for c in triplet_cubemap_centers]
    
    hull = ConvexHull(pts_projected_to_centroid_sphere)
    
    centroid_of_hull = np.mean(pts_projected_to_centroid_sphere, axis=0)    # not the same as the centroid of the triplet... this is just the geometric center of the generated centroid
    
    fixed_simplices, fixed_neighbors = fixSimplicesNeighbors(hull.simplices, hull.neighbors, pts_projected_to_centroid_sphere, centroid_of_hull)
    
    
    # check to see if the convex hull actually wraps around the centroid position... if not, then we need to add a big box and recalculate
    visibilities_for_centroid = calculateVisibilityOfTriangles(fixed_simplices, pts_projected_to_centroid_sphere, triplet_centroid)
    num_visible_for_centroid = np.count_nonzero(visibilities_for_centroid)
    if num_visible_for_centroid < len(fixed_simplices):
        # some were not visible, even to the centroid! this means that the entire convex hull is located somewhere away from the centroid
        # this happens when there are only correspondences on one side
        # to "fix" -- add vertices for a box that extends as far as the closest real point
        pts_augmented = np.copy(pts)
        
        
        extra_points = []
        
        for i in range(3):
            extra_points.append(triplet_cubemap_centers[i] + (triplet_cubemap_centers[i] - triplet_centroid) * 1.1 + np.array([0.0, 1.0, 0.0]))
            extra_points.append(triplet_cubemap_centers[i] + (triplet_cubemap_centers[i] - triplet_centroid) * 1.1 + np.array([0.0, -1.0, 0.0]))
            
        extra_points = np.array(extra_points)
        
        pts_augmented = np.vstack((pts_augmented, extra_points))
        
        print("NOTE: augmenting this triplet mesh with fake correspondences!")
        
        
        
        
        return generateHullData(pts_augmented, triplet_centroid, triplet_cubemap_centers)
        
    
    
    
    
    
    visibilities_for_each_pano = []
    for i in range(3):
        visibilities_for_each_pano.append(calculateVisibilityOfTriangles(fixed_simplices, pts_projected_to_each_pano[i], triplet_cubemap_centers[i]))
    
    return pts, pts_projected_to_centroid_sphere, pts_projected_to_each_pano, fixed_simplices, fixed_neighbors, visibilities_for_each_pano





def iteratePointFiltering(current_world_points, triplet_centroid, triplet_cubemap_centers):
    
    current_world_points, world_points_projected_to_centroid_sphere, world_points_projected_to_each_pano, fixed_simplices, fixed_neighbors, visibilities_for_each_pano = generateHullData(current_world_points, triplet_centroid, triplet_cubemap_centers)
    
    num_verts = len(current_world_points)
    num_triangles = len(fixed_simplices)
    
    # find a triangle that is good for all 3 meshes -- no folding for that particular triangle
    initial_good_triangle_index = -1
    
    start_point_order = [i for i in range(num_triangles)]
    random.shuffle(start_point_order)
    
    for i in start_point_order:
        if visibilities_for_each_pano[0][i] and visibilities_for_each_pano[1][i] and visibilities_for_each_pano[2][i]:
            initial_good_triangle_index = i
            break
    
    good_vert_indices = [True] * num_verts
    good_vert_indices = np.array(good_vert_indices)
    
    visited_triangle_set = set()
    tri_process_queue = Queue.Queue()
    tri_process_queue.put([initial_good_triangle_index, None])
    
    while not tri_process_queue.empty():
        [current_tri_index, predecessor_tri_index] = tri_process_queue.get(block=False)
        if current_tri_index not in visited_triangle_set:
            #print("processing tri {}".format(current_tri_index))
            visited_triangle_set.add(current_tri_index)
            
            tri_is_visible_to_all_panos = True
            for pano_idx in range(3):
                if not visibilities_for_each_pano[pano_idx][current_tri_index]:
                    tri_is_visible_to_all_panos = False
                    # this tri is flipped for at least one pano!
                    break
            
            if not tri_is_visible_to_all_panos:
                if predecessor_tri_index is not None:
                    current_tri_simplices = set(fixed_simplices[current_tri_index])
                    predecessor_tri_simplices = set(fixed_simplices[predecessor_tri_index])
                    
                    newly_introduced_vert = list(current_tri_simplices - predecessor_tri_simplices)[0]
                    #print("vert {} is bad".format(newly_introduced_vert))
                    good_vert_indices[newly_introduced_vert] = False
            
            neighbors_of_current_tri = fixed_neighbors[current_tri_index]
            for neighbor_of_current_tri in neighbors_of_current_tri:
                if neighbor_of_current_tri not in visited_triangle_set:
                    tri_process_queue.put([neighbor_of_current_tri, current_tri_index])
    
                    
    num_bad_points = len(current_world_points[good_vert_indices == False,:])
    print("removed {} bad points".format(num_bad_points))

    return current_world_points[good_vert_indices == True]











REFINED_TRIANGULATION_FILENAME = "sphere_pose_triangulation_refined_frame_numbers.yaml"


parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)

(frame_numbers_to_cubemap_cameras, frame_numbers_to_transformation_matrices, frame_numbers_to_inverse_transformation_matrices) = loadRefinedCubemapData(args.base_dir)

refined_triangulation_data = readFile(os.path.join(args.base_dir, REFINED_TRIANGULATION_FILENAME))["triangulation_indices"]


for triplet_frame_numbers in refined_triangulation_data:
    triplet_frame_numbers_sorted = np.copy(triplet_frame_numbers)
    triplet_frame_numbers_sorted.sort()
    
    
    input_sparse_triplet_mesh_filename = os.path.join(args.base_dir, "fixed_triplet_{}_{}_{}.meshes".format(triplet_frame_numbers_sorted[0], triplet_frame_numbers_sorted[1], triplet_frame_numbers_sorted[2]))
    
    input_holoroom_only_triplet_mesh_filename = os.path.join(args.base_dir, "holoroom_only_triplet_{}_{}_{}.meshes".format(triplet_frame_numbers_sorted[0], triplet_frame_numbers_sorted[1], triplet_frame_numbers_sorted[2]))
    
    if os.path.isfile(input_sparse_triplet_mesh_filename) and os.path.isfile(input_holoroom_only_triplet_mesh_filename):
        print("combining holoroom and sparse world points for  frame numbers {}".format(triplet_frame_numbers_sorted))
        
        all_world_points = []
        
        with open(input_sparse_triplet_mesh_filename, 'rb') as sparse_triplet_data_file:
            reader = csv.reader(sparse_triplet_data_file, delimiter=' ')
            num_sparse_points = int(reader.next()[0])
            for i in range(num_sparse_points):
                pt = [float(x) for x in reader.next()]
                all_world_points.append(pt)
        
        with open(input_holoroom_only_triplet_mesh_filename, 'rb') as sparse_triplet_data_file:
            reader = csv.reader(sparse_triplet_data_file, delimiter=' ')
            num_holoroom_only_points = int(reader.next()[0])
            for i in range(num_holoroom_only_points):
                pt = [float(x) for x in reader.next()]
                all_world_points.append(pt)
        
        print("loaded {} sparse points and {} holoroom points for a total of {} world points".format(num_sparse_points, num_holoroom_only_points, len(all_world_points)))
        
        
        
        
        # do the topology consistency here
        
        triplet_world_points = np.array(all_world_points)
        
        print("initial num points: {}".format(len(triplet_world_points)))
        
        
        
        triplet_cubemap_cams = [frame_numbers_to_cubemap_cameras[x] for x in triplet_frame_numbers_sorted]
        triplet_cubemap_centers = [cam.GetCameraCenter() for cam in triplet_cubemap_cams]
        
        triplet_centroid = np.mean(triplet_cubemap_centers, axis=0)
        
        # plot the points before fixing topology    
        
        #plotBefore(triplet_world_points, triplet_centroid, triplet_cubemap_centers)
        
        
        #new_world_points = triplet_world_points
        
        
        current_world_points = triplet_world_points
        new_world_points = iteratePointFiltering(current_world_points, triplet_centroid, triplet_cubemap_centers)
        while len(new_world_points) != len(current_world_points):
            current_world_points = new_world_points
            new_world_points = iteratePointFiltering(current_world_points, triplet_centroid, triplet_cubemap_centers)
        
        
        
        
        
        
        new_world_points, world_points_projected_to_centroid_sphere, world_points_projected_to_each_pano, fixed_simplices, fixed_neighbors, visibilities_for_each_pano = generateHullData(new_world_points, triplet_centroid, triplet_cubemap_centers)
        
        
        #plotAfter(new_world_points, triplet_centroid, triplet_cubemap_centers)
        
        
        out_filename = "fixed_holoroom_and_sparse_triplet_{}_{}_{}.meshes".format(triplet_frame_numbers_sorted[0], triplet_frame_numbers_sorted[1], triplet_frame_numbers_sorted[2])
        simplices = fixed_simplices
        
        print("result for this triplet: {} remaining world points".format(len(new_world_points)))
        
        
        writeOutputTripletMeshData(args.base_dir, out_filename, new_world_points, simplices)
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        output_holoroom_and_sparse_triplet_mesh_filename = os.path.join(args.base_dir, "output_holoroom_and_sparse_triplet_{}_{}_{}.meshes".format(triplet_frame_numbers_sorted[0], triplet_frame_numbers_sorted[1], triplet_frame_numbers_sorted[2]))
        
    else:
        print("skipping triplet {} because we don't have sparse+holoroom".format(triplet_frame_numbers_sorted))