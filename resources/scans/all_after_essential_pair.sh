#!/bin/bash
# in bash ubuntu for windows

if [ $# -ne 1 ]; then
    echo $0: usage: all_after_essential scanlabel
    exit 1
fi

SCAN_LABEL=$1
#   SCAN_LABEL=20170413142827

echo "find_sparse_features_common_to_pair..."
python find_sparse_features_common_to_pair.py ${SCAN_LABEL}

echo "add_incremental_holoroom_points_to_sparse_pair_mesh..."
python add_incremental_holoroom_points_to_sparse_pair_mesh.py ${SCAN_LABEL}

echo "make_pair_topology_consistent_generic..."
python make_pair_topology_consistent_generic.py ${SCAN_LABEL} ${SCAN_LABEL}/path_meshes/holoroom_and_sparse

echo "done with all pair data generation."