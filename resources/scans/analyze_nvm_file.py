# -*- coding: utf-8 -*-
"""
Created on Mon Oct 30 15:20:15 2017

@author: ANDERSED
"""

import numpy as np
import cv2
import os
import glob
import matplotlib.pyplot as plt
import sys
import argparse
import ntpath
import yaml
import csv
import ntpath

parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)


TRIANGULATION_FILENAME = "sphere_pose_triangulation.yaml"
SCANDATA_FILENAME = "scandata.yaml"
POSE_SPHERE_FILENAME = "pose_sphere.csv"

def saveFile(fname,data):
	fd = open(fname,"wb")
	yaml.dump(data,fd)
	fd.close()

def readFile(fname):
	fd = open(fname,"rb")
	data = yaml.load(fd)
	return data





nvm_path = os.path.join(args.base_dir, "pano_frames/cubemaps/faces/workspace_2step.nvm")



scanData = readFile(os.path.join(args.base_dir, SCANDATA_FILENAME))

sphere_pose_timestamps = []

with open(os.path.join(args.base_dir, POSE_SPHERE_FILENAME), 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        timestamp = float(row[0])
        
        sphere_pose_timestamps.append(timestamp)

def GetSphericalCameraFrameIndexFromPoseIndex(pose_index):
    locatableCameraSyncFrameIndex = scanData["hololens_sync_frame"] - 1 # go from 1-indexed to 0-indexed
    locatableCameraSyncFrameTimestamp = sphere_pose_timestamps[locatableCameraSyncFrameIndex]
    
    sphericalCameraSyncFrameIndex = scanData["spherical_sync_frame"] - 1 # go from 1-indexed to 0-indexed
    sphericalCameraFPS = scanData["spherical_fps"]
    
    locatableCameraTimestamp = sphere_pose_timestamps[pose_index]
    secondsSinceCalibrationTime = (locatableCameraTimestamp - locatableCameraSyncFrameTimestamp)
    sphericalCameraFrameIndex = int(round(sphericalCameraSyncFrameIndex + (secondsSinceCalibrationTime * sphericalCameraFPS)))
    return sphericalCameraFrameIndex
















camera_img_lines = []
observed_point_lines = []

with open(nvm_path, 'r+') as f:
    headerline = f.readline()
    f.readline()
    num_cubemap_imgs = int(f.readline())
    for i in range(num_cubemap_imgs):
        camera_img_line = f.readline()
        camera_img_lines.append(camera_img_line)
    f.readline()
    num_observed_points = int(f.readline())
    for i in range(num_observed_points):
        observed_point_line = f.readline()
        observed_point_lines.append(observed_point_line)
    # ignore the rest of the file

print("setting up image_to_points_map...")
# map from the image idx to the set of observed point indexes that were observed by that image
image_to_points_map = {}
    
for obs_idx in range(len(observed_point_lines)):
    observed_point_line = observed_point_lines[obs_idx]
    obs_point_fields = observed_point_line.split()
    
    num_observations_for_this_point = int(obs_point_fields[6])
    
    # we are only interested in points that were observed by at least 3 camera images, since that is the only hope for them to be triplet correspondences
    if num_observations_for_this_point >= 3:
    
        for i in range(num_observations_for_this_point):
            image_idx = int(obs_point_fields[7 + i * 4])
            if image_idx not in image_to_points_map:
                image_to_points_map[image_idx] = set()
            image_to_points_map[image_idx].add(obs_idx)
print("done")

asdf = 0
for foo in image_to_points_map:
    print(foo)
    print(image_to_points_map[foo])
    
    asdf += 1
    
    if asdf > 10:
        break

# now take these and map the results onto common sphere camera frames


print("setting up sphere_camera_frames_to_points_map...")
sphere_camera_frames_to_points_map = {}

for image_idx in image_to_points_map:
    camera_img_fields = camera_img_lines[image_idx].split()
    img_filename = camera_img_fields[0]
    frame_number = int(img_filename.split("_")[1].replace("frame","")) - 1  # go from 1-indexed (filenames) to 0-indexed
    
    if frame_number not in sphere_camera_frames_to_points_map:
        sphere_camera_frames_to_points_map[frame_number] = set()
        
    sphere_camera_frames_to_points_map[frame_number] = sphere_camera_frames_to_points_map[frame_number].union(image_to_points_map[image_idx])
print("done")


triangulation_data = readFile(os.path.join(args.base_dir, TRIANGULATION_FILENAME))

triangulation_indices = triangulation_data["triangulation_indices"]

triangulation_sphere_frame_indices = []
for pose_index_triangle in triangulation_indices:
    sphere_frame_index_triangle = []
    for pose_index_triangle_val in pose_index_triangle:
        sphere_frame_index_triangle.append(GetSphericalCameraFrameIndexFromPoseIndex(pose_index_triangle_val))
    triangulation_sphere_frame_indices.append(sphere_frame_index_triangle)
    
    
common_observed_points_for_each_triplet = []    

for sphere_frame_triplet in triangulation_sphere_frame_indices:
    print(sphere_frame_triplet)
    
    if sphere_frame_triplet[0] in sphere_camera_frames_to_points_map and sphere_frame_triplet[1] in sphere_camera_frames_to_points_map and sphere_frame_triplet[2] in sphere_camera_frames_to_points_map:
        observed_points_a = sphere_camera_frames_to_points_map[sphere_frame_triplet[0]]
        observed_points_b = sphere_camera_frames_to_points_map[sphere_frame_triplet[1]]
        observed_points_c = sphere_camera_frames_to_points_map[sphere_frame_triplet[2]]
        
        common_observed_point_indices = observed_points_a.intersection(observed_points_b).intersection(observed_points_c)
        
        print("common_observed_point_indices size " + str(len(common_observed_point_indices)))
        
        common_observed_points_for_each_triplet.append(common_observed_point_indices)