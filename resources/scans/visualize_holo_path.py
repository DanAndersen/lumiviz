# -*- coding: utf-8 -*-
"""
Created on Wed Oct 04 13:07:48 2017

@author: ANDERSED
"""

import yaml
import argparse
import sys
import os
import subprocess
import glob
from pyrr import quaternion, matrix44, vector3
from pyrr import Quaternion, Matrix33, Matrix44, Vector3
import numpy as np
import csv
from holoscan_common import SCANDATA_FILENAME
import process_scan_data_helpers
import matplotlib.patches as patches

# %matplotlib qt
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import proj3d
import numpy as np
import matplotlib.pyplot as plt

from scipy.spatial import Delaunay


def saveFile(fname,data):
	fd = open(fname,"wb")
	yaml.dump(data,fd)
	fd.close()

def readFile(fname):
	fd = open(fname,"rb")
	data = yaml.load(fd)
	return data


parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory for the specific scan")
args = parser.parse_args()

print("Given input pose CSV (for Holo camera), visualize it so we can see if it's complete")


print("Base scan directory: {}".format(args.base_dir))



SCAN_LABEL = os.path.split(args.base_dir)[-1]

print("SCAN_LABEL: {}".format(SCAN_LABEL))


#scan_labels = [SCAN_LABEL]
#scan_labels = ['20180212180805', '20180213164009', '20180214121658', '20180214161721', '20180215173737', '20180220151103', '20180222122208', '20180222133102', '20180222161203', '20180223111019']
#scan_labels = ['20180222153716','20180222153948','20180222154249']
#scan_labels = ['20180222133102']

#scan_labels = ['20180222153716'] # 0.5 office
#scan_labels = ['20180222153948'] # 0.25 office
scan_labels = ['20180222154249'] # 0.125 office


# values used for figure 4
"""
path_color = (0.5, 0.75, 1.0)
grid_line_color = (0.25,0.25,0.25)
points_color = 'red'
path_points_color = (0.0, 0.75, 0.0)
occlusion_edge_color = (0.5, 0.5, 0.5)
occlusion_face_color = (0.0, 0.4, 0.0)
#triangulation_color = (0.25, 0.25, 0.25)
triangulation_color = (1.0, 0.0, 0.0)
#background_color = (1.0,1.0,1.0)
background_color_enabled = True
background_color = (0.0, 0.0, 0.0)
pano_circle_size = 16
path_circle_size = 96
overriding_range = True
overriding_num_grid_cells_x = 23
overriding_num_grid_cells_y = 31
triangulation_linewidth = 1.0
path_linewidth = 1.5
override_centroid_units = np.array([1.0, 0.0, 2.0])
"""

"""
# values used for figure 5 left
path_color = (0.5, 0.75, 1.0)
grid_line_color = (0.25,0.25,0.25)
points_color = (1.0,0.25,0.2)
path_points_color = (0.0, 0.75, 0.0)
occlusion_edge_color = (0.0, 0.3, 0.0)
occlusion_face_color = (0.0, 0.4, 0.0)
#triangulation_color = (0.25, 0.25, 0.25)
triangulation_color = (0.8, 0.2, 0.15)
#background_color = (1.0,1.0,1.0)
background_color_enabled = True
background_color = (0.0, 0.0, 0.0)
pano_circle_size = 64
path_circle_size = 96
overriding_range = True
overriding_num_grid_cells_x = 10
overriding_num_grid_cells_y = 10
triangulation_linewidth = 3.0
path_linewidth = 4.0
override_centroid_units = np.array([1.0, 0.0, 2.0])
"""




# values used for figure 5 right
"""
path_color = (0.5, 0.5, 1.0)
grid_line_color = (0.25,0.25,0.25)
points_color = (1.0,0.25,0.2)
path_points_color = (0.0, 0.75, 0.0)
occlusion_edge_color = (0.0, 0.3, 0.0)
occlusion_face_color = (0.0, 0.3, 0.0)
#triangulation_color = (0.25, 0.25, 0.25)
triangulation_color = (0.6, 0.15, 0.1)
#background_color = (1.0,1.0,1.0)
background_color_enabled = True
background_color = (0.0, 0.0, 0.0)
pano_circle_size = 16
path_circle_size = 96
overriding_range = True
overriding_num_grid_cells_x = 10
overriding_num_grid_cells_y = 10
triangulation_linewidth = 1.5
path_linewidth = 4.0
override_centroid_units = np.array([1.0, 0.0, 2.0])
"""



"""
# values used for bary figure
path_color = (0.5, 0.5, 1.0)
grid_line_color = (0.25,0.25,0.25)
points_color = (1.0,0.25,0.2)
path_points_color = (0.0, 0.75, 0.0)
occlusion_edge_color = (0.0, 0.3, 0.0)
occlusion_face_color = (0.0, 0.3, 0.0)
#triangulation_color = (0.25, 0.25, 0.25)
triangulation_color = (0.6, 0.15, 0.1)
#background_color = (1.0,1.0,1.0)
background_color_enabled = False
background_color = (0.0, 0.0, 0.0)
pano_circle_size = 16
path_circle_size = 96
overriding_range = True
overriding_num_grid_cells_x = 3
overriding_num_grid_cells_y = 3
triangulation_linewidth = 3.0
path_linewidth = 4.0
override_centroid_units = np.array([1.7, 0.0, 2.0])
"""







# values used for user-study images
path_color = (0.5, 0.75, 1.0)
grid_line_color = (0.25,0.25,0.25)
points_color = 'red'
path_points_color = (0.0, 0.75, 0.0)
occlusion_edge_color = (0.5, 0.3, 0.5)
occlusion_face_color = (0.0, 0.3, 0.0)
#triangulation_color = (0.25, 0.25, 0.25)
triangulation_color = (1.0, 0.0, 0.0)
#background_color = (1.0,1.0,1.0)
background_color_enabled = True
background_color = (0.0, 0.0, 0.0)
pano_circle_size = 16
path_circle_size = 96
overriding_range = True
overriding_num_grid_cells_x = 23
overriding_num_grid_cells_y = 31
triangulation_linewidth = 1.0
path_linewidth = 1.5
override_centroid_units = np.array([1.0, 0.0, 2.0])






# override for office scans
# 0.5m
overriding_num_grid_cells_x = 11
overriding_num_grid_cells_y = 7
override_centroid_units = np.array([1.0, 0.0, -2.5])

# 0.25m
overriding_num_grid_cells_x = 22
overriding_num_grid_cells_y = 14
override_centroid_units = np.array([2.0, 0.0, -5.0])

# 0.125m
overriding_num_grid_cells_x = 44
overriding_num_grid_cells_y = 28
override_centroid_units = np.array([4.0, 0.0, -10.0])




for scan_label in scan_labels:
    current_base_dir = os.path.join('', scan_label)
    
    
    
    
    
    
    
    
    holoframes_zip, mesh_room, pose_csv, panovid = process_scan_data_helpers.get_all_input_filenames(current_base_dir, scan_label)
    #in_pose_csv_path = "E:\\andersed_dropbox\\Dropbox\\ScanRecordings\\2017-11-20\\20171120133708_pose.csv" # user 1
    #in_pose_csv_path = "E:\\andersed_dropbox\\Dropbox\\ScanRecordings\\2017-11-20\\20171120135653_pose.csv" # user 2
    in_pose_csv_path = pose_csv
    
    # now load in the hololens poses
    #in_pose_csv_path = glob.glob(os.path.join(args.scan_dir, "*_pose.csv"))[0]
    
    holo_positions = []
    
    out_spherical_csv_data = []
    
    with open(in_pose_csv_path, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            timestamp = float(row[0])
            pos_x = float(row[1])
            pos_y = float(row[2])
            pos_z = float(row[3])
            rot_x = float(row[4])
            rot_y = float(row[5])
            rot_z = float(row[6])
            
            pos = np.array([pos_x, pos_y, -pos_z])  # because the pose data comes from Unity with its left-handed coords, flip this axis
            rot = np.array([-rot_x, -rot_y, rot_z]) # flip axes as needed for the rotation to be in a right-handed coord system too
            
            holo_positions.append(pos)
            
        
    holo_positions = np.array(holo_positions)
    
    scanData = readFile(os.path.join(current_base_dir, SCANDATA_FILENAME))
    grid_size_meters = scanData["grid_size_meters"]
    override_centroid = override_centroid_units * grid_size_meters
    
    bounding_box_min = np.min(holo_positions, axis=0)
    bounding_box_max = np.max(holo_positions, axis=0)
    
    closest_pose_distances = {}
    closest_pose_indices = {}
    
    cell_ids_to_cell_center_positions = {}
    
    for i, pos in enumerate(holo_positions):
        cell_id = (int(np.round(pos[0] / grid_size_meters)), int(np.round(pos[2] / grid_size_meters)))
        
        cell_center_position = np.array([cell_id[0] * grid_size_meters, pos[1], cell_id[1] * grid_size_meters])
        
        cell_ids_to_cell_center_positions[cell_id] = cell_center_position
        
        dist = np.linalg.norm(cell_center_position - pos)
        
        if cell_id not in closest_pose_distances or dist < closest_pose_distances[cell_id]:
            closest_pose_distances[cell_id] = dist
            closest_pose_indices[cell_id] = i
    
    
    tri_points = []
    tri_original_indices = []
    
    for cell_id in closest_pose_indices:
        index = closest_pose_indices[cell_id]
        pos = holo_positions[index]
        
        tri_points.append([pos[0], pos[2]])
        tri_original_indices.append(index)
    
    tri_points = np.array(tri_points)
    
    triangulation = Delaunay(tri_points)
    
    
    
    
    
    centroid = (bounding_box_min + bounding_box_max) / 2.0
    
    
    
    
    selected_1d_pose_indices = []
    
    pidxs_1d_path = os.path.join(current_base_dir, "selected_1d_pose_indices.csv")
    with open(pidxs_1d_path, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            pidx = int(row[0])
            selected_1d_pose_indices.append(pidx)
    
    selected_1d_positions = np.array([holo_positions[pidx] for pidx in selected_1d_pose_indices])
    
    
    
    
    
    
    
    
    
    
    
    def draw_and_save(filename, grid_enabled, path_enabled, occlusion_enabled, pano_points_enabled, triangulation_enabled, path_points_enabled, path_segments_enabled):
    
        ax = plt.gca()
        ax.axes.get_xaxis().set_visible(False)
        ax.axes.get_yaxis().set_visible(False)
        
        plt.autoscale(False)
        ax.set_autoscale_on(False)
        
        from matplotlib.patches import Rectangle
        if background_color_enabled:
            ax.add_patch(Rectangle((-1000, -1000), 2000, 2000, facecolor=background_color))
        
        
        
        
        
        
        if occlusion_enabled:
            occlusion_background_color = (0.5, 0.5, 0.5)
        else:
            occlusion_background_color = (1.0, 1.0, 1.0)
        
        ax.patch.set_facecolor(occlusion_background_color)
        
        
        
        
        
        
        
        margin = 1.0
        
        x_range = np.abs(bounding_box_max[0] - bounding_box_min[0])
        y_range = np.abs(bounding_box_max[2] - bounding_box_min[2])
        
        if (x_range > y_range):
            y_range = x_range
            
        if (y_range > x_range):
            x_range = y_range
        
        plt.xlim(centroid[0] - (x_range/2.0) - margin, centroid[0] + (x_range/2.0) + margin)
        plt.ylim(centroid[2] - (y_range/2.0) - margin, centroid[2] + (y_range/2.0) + margin)
        
        if overriding_range:
            override_range_x = overriding_num_grid_cells_x * grid_size_meters
            override_range_y = overriding_num_grid_cells_y * grid_size_meters
            override_margin = 0.0125 * grid_size_meters
            plt.xlim(override_centroid[0] - (override_range_x/2.0) - override_margin, override_centroid[0] + (override_range_x/2.0) + override_margin)
            plt.ylim(override_centroid[2] - (override_range_y/2.0) - override_margin, override_centroid[2] + (override_range_y/2.0) + override_margin)
        
        
        
        #plt.xlim(-9.75, 4.75)
        #plt.ylim(-9.25, 5.25)
        plt.axes().set_aspect(1)
        #plt.axes().set_aspect('equal', 'datalim')
        
        
        
        if occlusion_enabled:
            for cell_id in cell_ids_to_cell_center_positions:
                cell_xy = cell_ids_to_cell_center_positions[cell_id]
                #xs = np.array([cell_xy[0] - grid_size_meters/2, cell_xy[0] + grid_size_meters/2, cell_xy[0] + grid_size_meters/2, cell_xy[0] - grid_size_meters/2, cell_xy[0] - grid_size_meters/2])
                #ys = np.array([cell_xy[2] + grid_size_meters/2, cell_xy[2] + grid_size_meters/2, cell_xy[2] - grid_size_meters/2, cell_xy[2] - grid_size_meters/2, cell_xy[2] + grid_size_meters/2])
                #plt.plot(xs, ys, color=line_color, zorder=0)
                
                ax.add_patch(Rectangle((cell_xy[0] - grid_size_meters/2, cell_xy[2] - grid_size_meters/2), grid_size_meters, grid_size_meters, facecolor=occlusion_face_color, edgecolor=occlusion_edge_color))
        
        
        if grid_enabled:
            
            for x in np.arange(-19 - grid_size_meters/2.0,  19 + grid_size_meters,grid_size_meters):
                plt.plot([x, x], [-20.0, 20.0], color=grid_line_color, zorder=3)
            
            for y in np.arange(-19 - grid_size_meters/2.0, 19.5 + grid_size_meters,grid_size_meters):
                plt.plot([-20.0, 20.0], [y, y], color=grid_line_color, zorder=3)
          
        if path_enabled:
            # switching Y and Z so Y is the vertical axis
            plt.plot(holo_positions[:,0], holo_positions[:,2], color=path_color, linewidth=path_linewidth, zorder=10)
        
        if pano_points_enabled:
            plt.scatter(tri_points[:,0], tri_points[:,1], color=points_color, zorder=5, s=pano_circle_size)
        
        
        if path_points_enabled:
            plt.scatter(selected_1d_positions[:,0], selected_1d_positions[:,2], color=path_points_color, zorder=5, s=path_circle_size)
        
        
        if path_segments_enabled:
            plt.plot(selected_1d_positions[:,0], selected_1d_positions[:,2], color=path_color, zorder=10)
        
        if triangulation_enabled:
            # get the triangulation in terms of the original pose indices
            tri_original_triplet_indices = []
            tri_simplices = triangulation.simplices
            for triplet in tri_simplices:
                original_indices_triplet = []
                for triplet_value in triplet:
                    original_indices_triplet.append(tri_original_indices[triplet_value])
                tri_original_triplet_indices.append(original_indices_triplet)
            
            
            plt.triplot(holo_positions[:,0], holo_positions[:,2], np.array(tri_original_triplet_indices), linewidth=triangulation_linewidth, color=triangulation_color, zorder=4) # only the poses used in the triangulation
        
        plt.savefig(filename, dpi=150, bbox_inches='tight', pad_inches=0, transparent=not background_color_enabled)
    
        #plt.show()
        
        plt.close()
    
    
    #draw_and_save("E:\\1.png", True, False, False, False, False)
    #draw_and_save("E:\\2.png", True, True, False, False, False)
    #draw_and_save("E:\\3.png", True, True, True, False, False)
    #draw_and_save("E:\\4.png", True, True, True, True, False)
    #draw_and_save("E:\\5.png", False, False, False, True, True)
    
    grid_enabled = True
    path_enabled = True
    occlusion_enabled = True
    pano_points_enabled = False
    triangulation_enabled = False
    path_points_enabled = False
    path_segments_enabled = False
    
    
    draw_and_save("E:\\map_{}.png".format(scan_label), grid_enabled, path_enabled, occlusion_enabled, pano_points_enabled, triangulation_enabled, path_points_enabled, path_segments_enabled)
    
