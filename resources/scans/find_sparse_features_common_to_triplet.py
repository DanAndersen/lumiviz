# -*- coding: utf-8 -*-
"""
Created on Wed Jan 31 15:39:04 2018

@author: ANDERSED

Given a triplet, try to find 3-way correspondences that can be used to accurately triangulate a position for sparse features.
"""


import argparse
import os
import process_scan_data_helpers
from holoscan_common import readFile, sphereImgPointToXYZUnitSphere, sphereImgPointToXYZUnitSphereMultiple, LineLineIntersect, GetPointsProjectedOntoTranslatedUnitSphere, writeOutputTripletMeshData
import csv
import numpy as np
import cv2
import glob
import matplotlib.pyplot as plt
from scipy.spatial import ConvexHull
import random
import time
import profile

from scipy.spatial import cKDTree

os.chdir("E:\\Dev\\OpenGL\\lumiviz\\resources\\scans")


parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)

print("Base scan directory: {}".format(args.base_dir))

SCAN_LABEL = os.path.split(args.base_dir)[-1]

print("SCAN_LABEL: {}".format(SCAN_LABEL))

# =============================================================================

fn_to_pose_refined_path = os.path.join(args.base_dir, "fn_to_pose_refined.csv")

if not os.path.isfile(fn_to_pose_refined_path):
    raise Exception("Missing refined poses at {}".format(fn_to_pose_refined_path))
    
refined_frame_number_triangulation_path = os.path.join(args.base_dir, "sphere_pose_triangulation_refined_frame_numbers.yaml")

if not os.path.isfile(refined_frame_number_triangulation_path):
    raise Exception("Missing refined sphere pose triangulation at {}".format(refined_frame_number_triangulation_path))

# =============================================================================

pano_frame_dir = os.path.join(args.base_dir, "pano_frames")

# Determine size of panoramas.
pano_frame_resolution = cv2.imread(glob.glob(os.path.join(pano_frame_dir, "pano_frame*.jpg"))[0]).shape[::-1][1:]

# =============================================================================

# Load the refined poses for each frame number.

fns_to_poses = {}   # contains matrix [R|t] where [R|t] * X_local = X_world

with open(fn_to_pose_refined_path, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        fn = int(row[0]) # 0-indexed
        mat = np.array([float(x) for x in row[1:]]).reshape((4,4))
        fns_to_poses[fn] = mat
                    
# =============================================================================


refined_triangulation_frame_numbers = readFile(refined_frame_number_triangulation_path)["triangulation_indices"]

# =============================================================================


fn_to_pano_img = {}

def loadPanoImg(fn):
    if fn not in fn_to_pano_img:
        img = cv2.imread(os.path.join(args.base_dir, "pano_frames", "pano_frame%05d.jpg" % (fn+1,)))
        fn_to_pano_img[fn] = img
    return fn_to_pano_img[fn]

# =============================================================================

def vector_to_cross_product_matrix(v):
        return np.array([
            [0.0, -v[2], v[1]],
            [v[2], 0.0, -v[0]],
            [-v[1], v[0], 0.0]
        ])

# =============================================================================

def drawMatchesThreshold(img_i, img_j, kp_i, kp_j, vals, threshold_val):
    img_combined = np.concatenate((img_i, img_j), axis=0)
    
    thickness = 1
    
    num_matches = len(kp_i)
    for match_idx in range(num_matches):
        val = vals[match_idx]
        color = (0,0,255) if val > threshold_val else (0,255,0)
        
        end1 = tuple(np.round(kp_i[match_idx]).astype(int))
        end2 = tuple(np.round(kp_j[match_idx]).astype(int) + np.array([0, img_i.shape[0]]))
        
        cv2.line(img_combined, end1, end2, color, thickness)
    
    return img_combined

# =============================================================================

# First determine if there are any additional SPHORB matches that need to be generated.

missing_sphorb_matches = []

for triplet in refined_triangulation_frame_numbers:
    triplet_canonical_order = sorted(triplet) # used for labeling the triplet uniquely

    for a in range(3):
            
        b = (a+1)%3
        fn_a = triplet_canonical_order[a] # 0-indexed
        fn_b = triplet_canonical_order[b]

        # fn_i is always less than fn_j
        fn_i = min(fn_a, fn_b)
        fn_j = max(fn_a, fn_b)
        
        sphorb_match_path = os.path.join(args.base_dir, "pano_frames", "sphorb_matches", "sphorb_match_%05d_%05d.csv" % (fn_i + 1, fn_j + 1)) # go to 1-indexed
        
        if not os.path.isfile(sphorb_match_path):
            missing_sphorb_matches.append([fn_i, fn_j])

if len(missing_sphorb_matches) > 0:
    print("need to generate {} more SPHORB matches due to changes in triangulation.".format(len(missing_sphorb_matches)))

    match_list_csv = os.path.join(args.base_dir, "refinement_frame_number_matches.csv")
    with open(match_list_csv, 'ab') as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        
        for missing_match in missing_sphorb_matches:
            fn_i = missing_match[0]
            fn_j = missing_match[1]        
            writer.writerow([fn_i, fn_j])

    print("Rerunning SPHORB matching to get new matches.")
    process_scan_data_helpers.create_sphorb_matches(args.base_dir)
    
    print("At this point, we should have all the pairwise matches we need now.")

# =============================================================================

def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)

def angle_between(v1, v2, prenormalized = False):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """
    v1_u = v1 if prenormalized else unit_vector(v1)
    v2_u = v2 if prenormalized else unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))

# =============================================================================


def drawFeatureClusters(pano_img, img_pts, xyzs, kd_tree, r):
    img = np.copy(pano_img)
    all_clusters_indices = kd_tree.query_ball_tree(kd_tree, r)
    for cluster_indices in all_clusters_indices:
        cluster_color = (random.randint(0,255),random.randint(0,255),random.randint(0,255))
        
        for idx in cluster_indices:
            img_pt = img_pts[idx]
            cv2.circle(img, (int(img_pt[0]), int(img_pt[1])), 4, cluster_color, -1)
    return img

# =============================================================================

# line intersection point of lines in 3D space in the least squares sense
# prenormalized = True if the distance between each corresponding PA and PB = 1
def lineIntersect3D(PA, PB, prenormalized=False):
    # PA: Nx3 matrix containing starting point of N lines
    # PB: Nx3 matrix containing end point of N lines
    
    Si = PB - PA # N lines described as vectors
    if prenormalized:
        ni = Si
    else:
        ni = Si / np.linalg.norm(Si, axis=1, keepdims=True) # normalize vectors
    nx = ni[:,0]
    ny = ni[:,1]
    nz = ni[:,2]
    
    SXX = np.sum(nx*nx - 1)
    SYY = np.sum(ny*ny - 1)
    SZZ = np.sum(nz*nz - 1)
    SXY = np.sum(nx*ny)
    SXZ = np.sum(nx*nz)
    SYZ = np.sum(ny*nz)
    S = np.array([[SXX, SXY, SXZ], [SXY, SYY, SYZ], [SXZ, SYZ, SZZ]])
    CX = np.sum(PA[:,0] * (nx*nx - 1) + PA[:,1] * (nx*ny) + PA[:,2] * (nx*nz))
    CY = np.sum(PA[:,0] * (nx*ny) + PA[:,1] * (ny*ny - 1) + PA[:,2] * (ny*nz))
    CZ = np.sum(PA[:,0] * (nx*nz) + PA[:,1] * (ny*nz) + PA[:,2] * (nz*nz - 1))
    C = np.array([[CX], [CY], [CZ]])
    P_intersect = np.linalg.solve(S, C)
    
    return P_intersect

# =============================================================================

out_triplet_sparse_dir = os.path.join(args.base_dir, "triplet_meshes", "sparse")
if not os.path.exists(out_triplet_sparse_dir):
    os.makedirs(out_triplet_sparse_dir)

# =============================================================================

def computeGoodMatchData(mat_i, mat_j, kp_i, kp_j, xyzs_i, xyzs_j):
    
    # select only high-quality matches based on our refined poses
    good_kp_i = []
    good_kp_j = []
    good_xyzs_i = []
    good_xyzs_j = []
    
    ray_i_start_world = np.dot(mat_i, np.array([0.0, 0.0, 0.0, 1.0]))
    ray_j_start_world = np.dot(mat_j, np.array([0.0, 0.0, 0.0, 1.0]))
    
    threshold_val = 0.01

    for matchidx in range(len(xyzs_i)):
        img_pt_i = kp_i[matchidx]
        img_pt_j = kp_j[matchidx]
        xyz_pt_i = xyzs_i[matchidx]
        xyz_pt_j = xyzs_j[matchidx]
    
        ray_i_end_world = np.dot(mat_i, np.append(xyz_pt_i, 1.0))                    
        ray_j_end_world = np.dot(mat_j, np.append(xyz_pt_j, 1.0))
        
        intersect_res, pa, pb = LineLineIntersect(ray_i_start_world, ray_i_end_world, ray_j_start_world, ray_j_end_world)

        angle_val = np.inf

        if intersect_res:
            
            midpoint_world = (pa+pb)/2.0
                       
            midpoint_projected_on_i = midpoint_world - ray_i_start_world[0:3]
            midpoint_projected_on_i = midpoint_projected_on_i / np.linalg.norm(midpoint_projected_on_i)
            
            midpoint_projected_on_j = midpoint_world - ray_j_start_world[0:3]
            midpoint_projected_on_j = midpoint_projected_on_j / np.linalg.norm(midpoint_projected_on_j)
            
            angle_between_i_and_midpoint = angle_between(ray_i_end_world[0:3] - ray_i_start_world[0:3], midpoint_projected_on_i, prenormalized=True)
            angle_between_j_and_midpoint = angle_between(ray_j_end_world[0:3] - ray_j_start_world[0:3], midpoint_projected_on_j, prenormalized=True)
            
            angle_val = max(angle_between_i_and_midpoint, angle_between_j_and_midpoint)
            
            if angle_val < threshold_val:
                good_kp_i.append(img_pt_i)
                good_kp_j.append(img_pt_j)
                good_xyzs_i.append(xyz_pt_i)
                good_xyzs_j.append(xyz_pt_j)
                
    print("got {} good matches".format(len(good_kp_i)))
    
    good_xyzs_i = np.array(good_xyzs_i)
    good_xyzs_j = np.array(good_xyzs_j)
    
    return {
            "good_kp_i": good_kp_i,
            "good_kp_j": good_kp_j,
            "good_xyzs_i": good_xyzs_i,
            "good_xyzs_j": good_xyzs_j,
            "kdtree_i": cKDTree(good_xyzs_i) if len(good_xyzs_i) > 0 else None,
            "kdtree_j": cKDTree(good_xyzs_j) if len(good_xyzs_j) > 0 else None
            }
    

def generateTripletMeshForCommonSparseFeatures(triplet_canonical_order, triplet_label, out_triplet_sparse_mesh_filename):
    start_time = time.time()
    print("Processing triplet {}".format(triplet_label))
    
    pano_a_center = fns_to_poses[triplet_canonical_order[0]][:3,3]
    pano_b_center = fns_to_poses[triplet_canonical_order[1]][:3,3]
    pano_c_center = fns_to_poses[triplet_canonical_order[2]][:3,3]
    
    triplet_centroid = (pano_a_center + pano_b_center + pano_c_center) / 3.0
    
    ij_to_matchdata = {}
                       
    for i in range(3):
        for j in range(i+1, 3):
            # fn_i always < fn_j
            fn_i = triplet_canonical_order[i]
            fn_j = triplet_canonical_order[j]
            
            mat_i = fns_to_poses[fn_i]
            mat_j = fns_to_poses[fn_j]
            
            sphorb_match_ij_path = os.path.join(args.base_dir, "pano_frames", "sphorb_matches", "sphorb_match_%05d_%05d.csv" % (fn_i + 1, fn_j + 1)) # go to 1-indexed
        
            if not os.path.isfile(sphorb_match_ij_path):
                raise Exception("no sphorb matches for: {}".format(sphorb_match_ij_path))
            
            kp_i = []
            kp_j = []
            xyzs_i = []
            xyzs_j = []
            
            with open(sphorb_match_ij_path, 'rb') as csvfile:
                reader = csv.reader(csvfile, delimiter=',')
                for row in reader:
                    # Get image points 
                    img_pt_i = [float(x) for x in row[0:2]]
                    img_pt_j = [float(x) for x in row[2:4]]
                    kp_i.append(img_pt_i)
                    kp_j.append(img_pt_j)
                    
            kp_i = np.array(kp_i)
            kp_j = np.array(kp_j)
            img_pt_i_xs = kp_i[:,0]
            img_pt_i_ys = kp_i[:,1]
            img_pt_j_xs = kp_j[:,0]
            img_pt_j_ys = kp_j[:,1]
            
            xyzs_i = sphereImgPointToXYZUnitSphereMultiple(img_pt_i_xs, img_pt_i_ys, pano_frame_resolution)
            xyzs_j = sphereImgPointToXYZUnitSphereMultiple(img_pt_j_xs, img_pt_j_ys, pano_frame_resolution)

            good_match_data = computeGoodMatchData(mat_i, mat_j, kp_i, kp_j, xyzs_i, xyzs_j)
            
            ij_to_matchdata[(i,j)] = good_match_data
            
            """
            pano_img_i = loadPanoImg(fn_i)
            pano_img_j = loadPanoImg(fn_j)
            matches_ij_img = drawMatchesThreshold(pano_img_i, pano_img_j, kp_i, kp_j, vals, threshold_val)
            winname = "{} {}".format(i, j)
            cv2.namedWindow(winname, cv2.WINDOW_NORMAL)
            cv2.imshow(winname, matches_ij_img)
            """
        
    cluster_radius = 0.01
    
    all_match_rays = []
    
    for i in range(3):
        for j in range(i+1,3):
            ij_data = ij_to_matchdata[(i,j)]
            i_tree_ij = ij_data["kdtree_i"] # the kD tree holding the points on pano i when matching (pano i <--> pano j)
            j_tree_ij = ij_data["kdtree_j"] # the kD tree holding the points on pano j when matching (pano i <--> pano j)
            
            for iprime in range(3):
                for jprime in range(iprime+1,3):
                    if not (i == iprime and j == jprime):
                        
                        #print("our match is {}<-->{}, and we are comparing against {}<-->{}".format(i, j, iprime, jprime))
                        
                        iprimejprime_data = ij_to_matchdata[(iprime,jprime)]
                
                        iprime_tree_iprimejprime = iprimejprime_data["kdtree_i"]
                        jprime_tree_iprimejprime = iprimejprime_data["kdtree_j"]
                        
                        if i_tree_ij is not None and j_tree_ij is not None and iprime_tree_iprimejprime is not None and jprime_tree_iprimejprime is not None:
                        
                            if i == iprime:
                                center_node = i
                                edge_nodes = [j, jprime]
                                center_ij_iprimejprime = i_tree_ij.query_ball_tree(iprime_tree_iprimejprime, cluster_radius)
                            elif i == jprime:
                                center_node = i
                                edge_nodes = [j, iprime]
                                center_ij_iprimejprime = i_tree_ij.query_ball_tree(jprime_tree_iprimejprime, cluster_radius)
                            elif j == iprime:
                                center_node = j
                                edge_nodes = [i, jprime]
                                center_ij_iprimejprime = j_tree_ij.query_ball_tree(iprime_tree_iprimejprime, cluster_radius)
                            elif j == jprime:
                                center_node = j
                                edge_nodes = [i, iprime]
                                center_ij_iprimejprime = j_tree_ij.query_ball_tree(jprime_tree_iprimejprime, cluster_radius)
                            else:
                                raise Exception("should not have gotten here")
                
                            for ij_idx in range(len(center_ij_iprimejprime)):
                                
                                match_rays = {}
                                match_rays[i] = []
                                match_rays[j] = []
                                match_rays[iprime] = []
                                match_rays[jprime] = []
                                
                                
                                ij_i_good_xyz = ij_data["good_xyzs_i"][ij_idx]
                                ij_j_good_xyz = ij_data["good_xyzs_j"][ij_idx]
                                
                                match_rays[i].append(ij_i_good_xyz)
                                match_rays[j].append(ij_j_good_xyz)
                                
                                
                                iprimejprime_idxs = center_ij_iprimejprime[ij_idx]
                                
                                if len(iprimejprime_idxs) > 0:
                                    #print("{}: {}".format(ij_idx, iprimejprime_idxs))
                                    
                                    for iprimejprime_idx in iprimejprime_idxs:
                                        iprimejprime_iprime_good_xyz = iprimejprime_data["good_xyzs_i"][iprimejprime_idx]
                                        iprimejprime_jprime_good_xyz = iprimejprime_data["good_xyzs_j"][iprimejprime_idx]
                                        
                                        match_rays[iprime].append(iprimejprime_iprime_good_xyz)
                                        match_rays[jprime].append(iprimejprime_jprime_good_xyz)
                                
                                
                                    for key in edge_nodes + [center_node]:
                                        match_rays[key] = np.array(match_rays[key])
                                        match_rays[key] = np.mean(match_rays[key], axis=0)
                                        match_rays[key] /= np.linalg.norm(match_rays[key])
                                    
                                    all_match_rays.append(match_rays)
    
    sparse_world_points = []                       
                                
    for match_rays in all_match_rays:
        
        PA = np.zeros((len(match_rays), 4))
        PB = np.zeros((len(match_rays), 4))
        
        for idx in match_rays:
            fn = triplet_canonical_order[idx]
            mat = fns_to_poses[fn]
            
            ray_start_world = np.dot(mat, np.array([0.0, 0.0, 0.0, 1.0]))
            
            xyz = match_rays[idx]
            ray_end_world = np.dot(mat, np.append(xyz, 1.0))    
            
            PA[idx] = ray_start_world
            PB[idx] = ray_end_world
            
        P_intersect = lineIntersect3D(PA, PB, prenormalized=True)
        
        sparse_world_points.append(P_intersect)
            
    sparse_world_points = np.array(sparse_world_points).reshape(-1,3)
                                
                                
    
    
    
    
    unprojected_pts = sparse_world_points
        
    world_correspondence_points_projected_to_centroid_sphere = GetPointsProjectedOntoTranslatedUnitSphere(unprojected_pts, triplet_centroid)
    
    pts = world_correspondence_points_projected_to_centroid_sphere
    
    if len(pts) > 4: # need at least 4 points for a convex hull
        try:
            world_centroid_hull = ConvexHull(pts)
            simplices = world_centroid_hull.simplices
        except:
            print("error when making convex hull")
            simplices = np.array([])
    else:
        simplices = np.array([])
    
    writeOutputTripletMeshData(out_triplet_sparse_dir, out_triplet_sparse_mesh_filename, unprojected_pts, simplices)
         

    elapsed_time = time.time() - start_time                            
    print("for triplet {}, generated a total of {} sparse points ( took {} sec )".format(triplet_canonical_order, len(unprojected_pts), elapsed_time))        

# =============================================================================

for triplet in refined_triangulation_frame_numbers:
    triplet_canonical_order = sorted(triplet) # used for labeling the triplet uniquely
    
    triplet_label = "{}_{}_{}".format(triplet_canonical_order[0], triplet_canonical_order[1], triplet_canonical_order[2])
    
    out_triplet_sparse_mesh_filename = "triplet_{}.meshes".format(triplet_label)
    
    if not os.path.isfile(os.path.join(out_triplet_sparse_dir, out_triplet_sparse_mesh_filename)):
        generateTripletMeshForCommonSparseFeatures(triplet_canonical_order, triplet_label, out_triplet_sparse_mesh_filename)
        #foo, bar = profile.run('generateTripletMeshForCommonSparseFeatures(triplet_canonical_order, triplet_label, out_triplet_sparse_mesh_filename)', sort='tottime')
    else:
        print("skipping generation of sparse triplet mesh for triplet {}".format(triplet_canonical_order))