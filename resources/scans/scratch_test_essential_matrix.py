# -*- coding: utf-8 -*-
"""
Created on Tue Dec 05 14:08:45 2017

@author: ANDERSED
"""

# test loading in some correspondences found via SPHORB and determining the essential matrix from them


import numpy as np
import cv2
import os
import glob
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import sys
import argparse
import ntpath
import yaml
import csv
import ntpath
import re
from holoscan_common import saveFile, readFile
import time
import profile
import scipy

pano_frame_resolution = (3840.0, 1920.0)

def loadPanoSphereImg(zero_indexed_frame_number):
    return cv2.imread(os.path.join(pano_frame_dir, "pano_frame%05d.jpg" % (zero_indexed_frame_number + 1,)))

def sphereImgPointToXYZUnitSphere(img_x, img_y):
    # returns an XYZ point on the unit sphere, where -z is center, +y is north pole
    
    w = pano_frame_resolution[0]
    h = pano_frame_resolution[1]
    
    lon_radians = (-2.0 * np.pi / w) * img_x + np.pi
    lat_radians = (-np.pi / h) * img_y + (np.pi/2.0)

    y = np.sin(lat_radians)
    
    x = - np.cos(lat_radians) * np.sin(lon_radians)
    z = - np.cos(lat_radians) * np.cos(lon_radians)
    
    return np.array([x,y,z])

def toHomogeneousCoords(xyz):
    return xyz * 1.0/xyz[2] # yes, we do have to actually convert to homogeneous coordinates
    #return xyz


# array form of 3x3 matrix of (yprime * y^T)
def getRowForSolver(y, yprime):
    return np.outer(yprime, y).reshape(-1)


# the projected distance epsilon_p as described in Pagini et al.
def rayDistanceToEpipolarPlaneSingle(x, xprime, E):
    e_x = np.dot(E, x)
    
    xprime_norm = np.linalg.norm(xprime)
    e_x_norm = np.linalg.norm(e_x)
    
    epsilon_p = ( np.abs(np.dot(xprime.T, e_x)) ) / ( xprime_norm * e_x_norm )
    
    return epsilon_p


# the projected distance epsilon_p as described in Pagini et al...
# but for multiple pairs of (x, x') at the same time.
# xs = Nx3 array of points
# xprimes = Nx3 array of points
# returns array of size N, each value with the corresponding distance
def rayDistanceToEpipolarPlaneMultiple(xs, xprimes, E):
    
    #print("in rayDistanceToEpipolarPlaneMultiple")
    #print("xs shape: {}".format(xs.shape))
    #print("xprimes shape: {}".format(xprimes.shape))
    
    E_x = np.dot(E, xs.T).T
                
    xprimes_norms = np.linalg.norm(xprimes, axis=1)
    
    E_x_norms = np.linalg.norm(E_x, axis=1)
    
    xprimes_E_x = np.einsum('ij,ij->i', xprimes, E_x)
    
    return np.abs(xprimes_E_x) / (xprimes_norms * E_x_norms)
    


def getAverageRayDistanceToEpipolarPlane(matches, E):    
    
    #print("in getAverageRayDistanceToEpipolarPlane")
    
    xs = matches[:,0:3]
    xprimes = matches[:,3:6]
    
    errs = rayDistanceToEpipolarPlaneMultiple(xs, xprimes, E)
    
    return np.mean(errs)


# given a list of matches (each match is a pair of 3D homogeneous coordinates),
# return the best-fit essential matrix E that maps between the points in the matches
def solve_essential_matrix(grouped_matches_homogeneous):
    # http://ece631web.groups.et.byu.net/Lectures/ECEn631%2013%20-%208%20Point%20Algorithm.pdf        
    
    ys = grouped_matches_homogeneous[:,0:3]
    yprimes = grouped_matches_homogeneous[:,3:6]
    
    #print("ys shape: {}".format(ys.shape))
    #print("yprimes shape: {}".format(yprimes.shape))
    
    n_matches = len(ys)
    
    #print("n_matches: {}".format(n_matches))
    
    # M_matrix is a matrix of size (n_matches x 9) where each row is the array form of (yprime * y^T)
    M_matrix = np.matmul(yprimes[:,:,np.newaxis], ys[:,np.newaxis, :]).reshape(n_matches, 9)
   
    u, s, vH = np.linalg.svd(M_matrix)
    v = vH.T
    E_est = v[:,8].reshape(3,3)    
    u_eest, s_eest, vH_eest = np.linalg.svd(E_est)    
    s1 = s_eest[0]
    s2 = s_eest[1]
    avg_s = (s1+s2)/2.0
    #s_eest_prime = np.array([[avg_s, 0, 0], [0, avg_s, 0], [0,0,0]])
    s_eest_prime = np.zeros((3,3))
    s_eest_prime[0,0] = avg_s
    s_eest_prime[1,1] = avg_s
    E_prime = np.matmul(np.matmul(u_eest, s_eest_prime), vH_eest)
    return E_prime




        

def drawMatches(frame_number_i, frame_number_j, all_matches, inlier_indices = None):
    img_i = loadPanoSphereImg(frame_number_i)
    img_j = loadPanoSphereImg(frame_number_j)
    
    kp_i = [[m[0], m[1]] for m in all_matches]
    kp_j = [[m[2], m[3]] for m in all_matches]
    
    if inlier_indices is None:
        inliers = range(len(all_matches))
    else:
        inliers = inlier_indices
    
    stackedImg = np.concatenate((img_i, img_j), axis=0)
    
    for inlier_idx in inliers:
        pt_i = [int(x) for x in kp_i[inlier_idx]]
        
        
        pt_j = [int(x) for x in kp_j[inlier_idx]]
        
        pt_j = [pt_j[0], pt_j[1] + int(pano_frame_resolution[1])]
        
        
        color = (np.random.rand(3) * 256).astype(np.int)
        thickness = 2
        
        cv2.line(stackedImg, tuple(pt_i), tuple(pt_j), color, thickness)
    
    return stackedImg


# matches_homogeneous: a list of pairs, where each pair is a set of two 3D homogeneous coordinates
# n: the minimum number of data points needed to fit the model (e.g. 8 for the 8-point algorithm)
# k: max number of iterations allowed in the algorithm
# t: threshold value to determine when a data point fits a model
# d: number of close data points required to assert that a model fits well to data (absolute number, not a percentage)
def ransac_essential_matrix(match_xyzs, grouped_matches_homogeneous, n, k, t, d):
    print("starting ransac_essential_matrix()")
    num_points_all = len(grouped_matches_homogeneous) # how many were input (inliers and outliers)
    
    iterations = 0
    best_fit_essential_matrix = None
    best_err = np.inf
    best_fit_inlier_indices = []
    if num_points_all >= n:
        while iterations < k:
            #print("iteration {}".format(iterations))
            maybe_inliers_indices = np.sort(np.random.choice(num_points_all, n, replace=False))
            maybe_inliers = []
            for idx in range(num_points_all):
                if idx in maybe_inliers_indices:
                    maybe_inliers.append(grouped_matches_homogeneous[idx])
            maybe_inliers = np.array(maybe_inliers)
            maybe_model = solve_essential_matrix(maybe_inliers)
            
            also_inliers = []
            also_inlier_indices = []

            additional_idxs = []
            additional_matches = []            
            additional_xs = []
            additional_xprimes = []
            
            for idx in range(num_points_all):
                if idx not in maybe_inliers_indices:
                    match = grouped_matches_homogeneous[idx]
                    
                    additional_idxs.append(idx)
                    additional_matches.append(match)
                    additional_xs.append(match[0:3])
                    additional_xprimes.append(match[3:6])
                    
            additional_xs = np.array(additional_xs)
            additional_xprimes = np.array(additional_xprimes)
            
            errs = rayDistanceToEpipolarPlaneMultiple(additional_xs, additional_xprimes, maybe_model)
            
            for additional_idx in range(len(additional_idxs)):
                if errs[additional_idx] < t:
                    all_idx = additional_idxs[additional_idx]
                    also_inlier_indices.append(all_idx)
                    also_inliers.append(grouped_matches_homogeneous[all_idx])
                        
            #print("size of also_inliers: {}".format(len(also_inliers)))
            if len(also_inliers) > d:
                #print("found good model")
                # this implies that we may have found a good model
                # now test how good it is
                all_current_inliers = np.vstack((maybe_inliers, also_inliers))
                all_current_inlier_indices = np.concatenate( (maybe_inliers_indices, also_inlier_indices), axis=0 )
                
                better_model = solve_essential_matrix(all_current_inliers)
                this_err = getAverageRayDistanceToEpipolarPlane(all_current_inliers, better_model)
                if this_err < best_err:
                    #print("found improved model")
                    best_fit_essential_matrix = better_model
                    best_err = this_err
                    best_fit_inlier_indices = all_current_inlier_indices
            iterations += 1
    return best_fit_essential_matrix, best_err, best_fit_inlier_indices












class SphericalEpipolarModel:
    """
    data is input as a matrix of size (n_matches x 6)
    where cols 0,1,2 are the homogeneous coord for match x
    and cols 3,4,5 are the homogenerous coord for match xprime
    """
    def __init__(self):
        return
    
    def fit(self, data):
        E = solve_essential_matrix(data)
        return E
    
    def get_error(self, data, model):
        xs = data[:,0:3]
        xprimes = data[:,3:6]
        E = model
        err_per_pair = rayDistanceToEpipolarPlaneMultiple(xs, xprimes, E)
        return err_per_pair





def ransac(data,model,n,k,t,d,debug=False,return_all=False):
    """fit model parameters to data using the RANSAC algorithm
    
This implementation written from pseudocode found at
http://en.wikipedia.org/w/index.php?title=RANSAC&oldid=116358182

{{{
Given:
    data - a set of observed data points
    model - a model that can be fitted to data points
    n - the minimum number of data values required to fit the model
    k - the maximum number of iterations allowed in the algorithm
    t - a threshold value for determining when a data point fits a model
    d - the number of close data values required to assert that a model fits well to data
Return:
    bestfit - model parameters which best fit the data (or nil if no good model is found)
iterations = 0
bestfit = nil
besterr = something really large
while iterations < k {
    maybeinliers = n randomly selected values from data
    maybemodel = model parameters fitted to maybeinliers
    alsoinliers = empty set
    for every point in data not in maybeinliers {
        if point fits maybemodel with an error smaller than t
             add point to alsoinliers
    }
    if the number of elements in alsoinliers is > d {
        % this implies that we may have found a good model
        % now test how good it is
        bettermodel = model parameters fitted to all points in maybeinliers and alsoinliers
        thiserr = a measure of how well model fits these points
        if thiserr < besterr {
            bestfit = bettermodel
            besterr = thiserr
        }
    }
    increment iterations
}
return bestfit
}}}
"""
    iterations = 0
    bestfit = None
    besterr = np.inf
    best_inlier_idxs = None
    while iterations < k:
        maybe_idxs, test_idxs = random_partition(n,data.shape[0])
        maybeinliers = data[maybe_idxs,:]
        test_points = data[test_idxs]
        maybemodel = model.fit(maybeinliers)
        test_err = model.get_error( test_points, maybemodel)
        also_idxs = test_idxs[test_err < t] # select indices of rows with accepted points
        alsoinliers = data[also_idxs,:]
        if debug:
            print 'test_err.min()',test_err.min()
            print 'test_err.max()',test_err.max()
            print 'np.mean(test_err)',np.mean(test_err)
            print 'iteration %d:len(alsoinliers) = %d'%(
                iterations,len(alsoinliers))
        if len(alsoinliers) > d:
            betterdata = np.concatenate( (maybeinliers, alsoinliers) )
            bettermodel = model.fit(betterdata)
            better_errs = model.get_error( betterdata, bettermodel)
            thiserr = np.mean( better_errs )
            if thiserr < besterr:
                bestfit = bettermodel
                besterr = thiserr
                best_inlier_idxs = np.concatenate( (maybe_idxs, also_idxs) )
        iterations+=1
    if bestfit is None:
        #raise ValueError("did not meet fit acceptance criteria")
        print("did not meet fit acceptance criteria")
        return None, {'inliers': np.array([])}
    if return_all:
        return bestfit, {'inliers':best_inlier_idxs}
    else:
        return bestfit

def random_partition(n,n_data):
    """return n random rows of data (and also the other len(data)-n rows)"""
    all_idxs = np.arange( n_data )
    np.random.shuffle(all_idxs)
    idxs1 = all_idxs[:n]
    idxs2 = all_idxs[n:]
    return idxs1, idxs2


parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)



ransac_params = {
    'MIN_MATCHES_NEEDED': 8,     
    'NUM_ITERATIONS': 100,
    'THRESHOLD_VAL': 0.02,
    'PERCENT_INLIERS_NEEDED': 0.2
}



model = SphericalEpipolarModel()




pano_frame_dir = os.path.join(args.base_dir, "pano_frames")

input_sphorb_match_paths = glob.glob(os.path.join(pano_frame_dir, "sphorb_matches", "sphorb_match_[0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9].csv"))


for idx, input_sphorb_match_path in enumerate(input_sphorb_match_paths):
    csv_filename = os.path.split(input_sphorb_match_path)[-1]
    print("reading in {}...".format(csv_filename))
    
    zero_indexed_frame_numbers = [int(x) - 1 for x in re.findall(r'\d+', csv_filename)] # go from 1-indexed to 0-indexed
    frame_numbers_ij = zero_indexed_frame_numbers

    outdata_filename = "sphorb_match_%05d_%05d_essential.yaml" % (frame_numbers_ij[0] + 1, frame_numbers_ij[1] + 1)
    outdata_path = os.path.join(os.path.join(pano_frame_dir, "sphorb_matches", outdata_filename))

        
    if not os.path.isfile(outdata_path):
        start_time = time.time()
        matches = []
        
        match_xyzs = []
        matches_homogeneous = []
        
        with open(input_sphorb_match_path, 'rb') as f:
            reader = csv.reader(f, delimiter=',')
            for row in reader:
                match = [float(x) for x in row]
                matches.append(match)
                #print match
                
                pt_i = sphereImgPointToXYZUnitSphere(match[0], match[1])
                pt_j = sphereImgPointToXYZUnitSphere(match[2], match[3])
                
                match_xyz = [pt_i, pt_j]
                match_xyzs.append(match_xyz)
                
                #print match_xyz
                
                match_homogeneous = [toHomogeneousCoords(xyz) for xyz in match_xyz]
                matches_homogeneous.append(match_homogeneous)
                
                #print match_homogeneous
        
        # Convert the homogeneous matches to a matrix of size (n_matches x 6)
        # where cols 0,1,2 are the coords for x and cols 3,4,5 are the coords for xprime
        xs = np.array([pair[0] for pair in matches_homogeneous])
        xprimes = np.array([pair[1] for pair in matches_homogeneous])
        
        grouped_matches_homogeneous = np.hstack((xs, xprimes))
        
        print("input matches: {}".format(len(matches)))
        
        #best_fit_essential_matrix, best_err, best_fit_inlier_indices = ransac_essential_matrix(match_xyzs, grouped_matches_homogeneous, ransac_params["MIN_MATCHES_NEEDED"], ransac_params["NUM_ITERATIONS"], ransac_params["THRESHOLD_VAL"], ransac_params["PERCENT_INLIERS_NEEDED"]*len(matches_homogeneous))
        #best_fit_essential_matrix, best_err, best_fit_inlier_indices = profile.run('ransac_essential_matrix(match_xyzs, grouped_matches_homogeneous, ransac_params["MIN_MATCHES_NEEDED"], ransac_params["NUM_ITERATIONS"], ransac_params["THRESHOLD_VAL"], ransac_params["PERCENT_INLIERS_NEEDED"]*len(matches_homogeneous))', sort='tottime')
        
        all_data = grouped_matches_homogeneous
        n = ransac_params['MIN_MATCHES_NEEDED'] # the minimum number of data values required to fit the model
        k = ransac_params['NUM_ITERATIONS'] # the maximum number of iterations allowed in the algorithm
        t = ransac_params['THRESHOLD_VAL'] # a threshold value for determining when a data point fits a model
        d = ransac_params["PERCENT_INLIERS_NEEDED"]*len(all_data) # the number of close data values required to assert that a model fits well to data
        ransac_fit, ransac_data = ransac(all_data, model, 
                                         n,k,t,d, # misc parameters
                                         debug=False, return_all=True)
        
        
        best_fit_essential_matrix = ransac_fit
        if best_fit_essential_matrix is not None:
            best_err = np.mean(model.get_error(all_data[ransac_data['inliers']], ransac_fit)) # mean error against inliers
        else:
            best_err = np.inf
        best_fit_inlier_indices = list(ransac_data['inliers'])
        
        
            
        """
        imgMatches = drawMatches(frame_numbers_ij[0], frame_numbers_ij[1], matches, best_fit_inlier_indices)
        cv2.namedWindow("imgMatches", cv2.WINDOW_NORMAL)
        cv2.imshow("imgMatches", imgMatches)
        cv2.resizeWindow("imgMatches", 600,600)
        cv2.waitKey(1)
        """
    
        #print(1/0)
    
        output_data = {
            "best_fit_essential_matrix": best_fit_essential_matrix,
            "best_err": best_err,
            "best_fit_inlier_indices": best_fit_inlier_indices,
            "ransac_params": ransac_params
        }
        
        print("done solving, found {} inliers with err {}".format(len(best_fit_inlier_indices), best_err))
        
        saveFile(outdata_path, output_data)
        
        elapsed_time = time.time() - start_time
        
        print("saved data to {}, total time: {} sec".format(outdata_filename, elapsed_time))
    else:
        print("{} already exists. skipping.".format(outdata_filename))
    
print("done with all matches")



