# -*- coding: utf-8 -*-
"""
Created on Wed Nov 29 12:51:37 2017

@author: ANDERSED
"""

# in preparation for cubemap bundle adjustment:
# load in the estimated room triangulation
# find all matches between adjacent cubemaps (1 or 2 steps away)
# determine, for each of the horizontal cubemap directions (front, back, left, right)
# which faces in cubemap i are likely to have real correspondences with which faces in cubemap j
# then, once that data is determined, generate features and find correspondences

import numpy as np
import cv2
import os
import glob
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import sys
import argparse
import ntpath
import yaml
import csv
import ntpath
from holoscan_common import saveFile, readFile, TRIANGULATION_FILENAME, SCANDATA_FILENAME, POSE_SPHERE_FILENAME, generateAdjacencyMap, getMappingsWithinTwoSteps

parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)


MATCH_LIST_FILENAME = "frame_number_matches.csv"



side_idxs_to_transformation_matrices = {
        0: np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]], dtype=np.float64),
        1: np.array([[0,0,1,0],[0,1,0,0],[-1,0,0,0],[0,0,0,1]], dtype=np.float64),
        2: np.array([[-1,0,0,0],[0,1,0,0],[0,0,-1,0],[0,0,0,1]], dtype=np.float64),
        3: np.array([[0,0,-1,0],[0,1,0,0],[1,0,0,0],[0,0,0,1]], dtype=np.float64),
        4: np.array([[0,0,-1,0],[1,0,0,0],[0,-1,0,0],[0,0,0,1]], dtype=np.float64),
        5: np.array([[0,0,-1,0],[-1,0,0,0],[0,1,0,0],[0,0,0,1]], dtype=np.float64)
        }

side_idxs_to_inverse_transformation_matrices = {}
for side_idx in side_idxs_to_transformation_matrices:
    side_idxs_to_inverse_transformation_matrices[side_idx] = np.linalg.inv(side_idxs_to_transformation_matrices[side_idx])





frustum_near = 1.0
frustum_far = 20.0

frustum_points_local = np.array([
        [-frustum_near, -frustum_near, -frustum_near, 1.0], # left bottom near
        [frustum_near, -frustum_near, -frustum_near, 1.0], # right bottom near
        [-frustum_near, frustum_near, -frustum_near, 1.0], # left top near
        [frustum_near, frustum_near, -frustum_near, 1.0], # right top near
        [-frustum_far, -frustum_far, -frustum_far, 1.0], # left bottom far
        [frustum_far, -frustum_far, -frustum_far, 1.0], # right bottom far
        [-frustum_far, frustum_far, -frustum_far, 1.0], # left top far
        [frustum_far, frustum_far, -frustum_far, 1.0], # right top far
        ])

frustum_plane_indices = [
        [1, 5, 7],    # right
        [3, 7, 6],    # top
        [2, 6, 4],    # left
        [0, 4, 5],    # bottom
        [0, 3, 2],    # near
        [4, 6, 7],    # far
        ]

frustum_plane_quad_indices = [
        [1, 5, 7, 3],    # right
        [3, 7, 6, 2],    # top
        [2, 6, 4, 0],    # left
        [0, 4, 5, 1],    # bottom
        [0, 1, 3, 2],    # near
        [5, 4, 6, 7],    # far
        ]


side_idx_to_colors = [
        'r','y','g','c','b','m'
        ]



class Frustum:
    def __init__(self, transformation_matrix, side_idx):
        side_to_front_matrix = side_idxs_to_inverse_transformation_matrices[side_idx]
        frustum_points_local_side = np.matmul(side_to_front_matrix, frustum_points_local.T).T
        frustum_points_world = np.matmul(transformation_matrix, frustum_points_local_side.T).T

        self.frustum_points_world = frustum_points_world

        self.frustum_planes = []

        # http://kitchingroup.cheme.cmu.edu/blog/2015/01/18/Equation-of-a-plane-through-three-points/
        for frustum_plane_triplet in frustum_plane_indices:
            p1 = frustum_points_world[frustum_plane_triplet[0]][:3]
            p2 = frustum_points_world[frustum_plane_triplet[1]][:3]
            p3 = frustum_points_world[frustum_plane_triplet[2]][:3]
            v1 = p3 - p1
            
            v1 = v1 / np.linalg.norm(v1)
            
            v2 = p2 - p1
            
            v2 = v2 / np.linalg.norm(v2)
            
            cp = np.cross(v1, v2)
            a, b, c = cp
            
            d = - np.dot(cp, p3)
            
            # the equation is ax + by + cz = d
            self.frustum_planes.append(np.array([a,b,c,d]))
            
            
# given a frame number, generate a set of 6 frustums representing each side of the cubemap
def GenerateFrustums(frame_number):
    pose_index = frame_numbers_to_pose_indices[frame_number]
    transformation_matrix = sphere_pose_matrices[pose_index] # goes from local (front side) cubemap space to world space
    
    side_idx_to_frustums = []
                                                
    for side_idx in range(6):
        
        frustum = Frustum(transformation_matrix, side_idx)
        
        side_idx_to_frustums.append(frustum)
    return side_idx_to_frustums
    """
    fig = plt.figure(1)
    ax = fig.add_subplot(111, projection='3d')
    
    for side_idx in range(6):
        frustum = side_idx_to_frustums[side_idx]
        
        pts = frustum.frustum_points_world
        color = side_idx_to_colors[side_idx]
        
        far_pts = pts[4:]
        
        poly_verts = [zip(far_pts[:,0], far_pts[:,1], far_pts[:,2])]
        poly = Poly3DCollection(poly_verts)
        poly.set_color(color)
        ax.add_collection3d(poly)
        
        
        ax.scatter(pts[:,0], pts[:,1], pts[:,2], c=color, marker='o')
    
    plot_center_point = np.array([0.0, 0.0, 0.0])
    plot_range = 100.0
    
    ax.auto_scale_xyz([plot_center_point[0] - plot_range, plot_center_point[0] + plot_range], [plot_center_point[1] - plot_range, plot_center_point[1] + plot_range], [plot_center_point[2] - plot_range, plot_center_point[2] + plot_range])
    plt.show()
    """



scanData = readFile(os.path.join(args.base_dir, SCANDATA_FILENAME))


sphere_pose_timestamps = []
sphere_pose_matrices = []
sphere_pose_camera_centers = []

with open(os.path.join(args.base_dir, POSE_SPHERE_FILENAME), 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        timestamp = float(row[0])
        
        sphere_pose_timestamps.append(timestamp)
        
        sphere_pose_transformation_matrix = np.array([float(x) for x in row[1:]]).reshape(4,4)
        sphere_pose_matrices.append(sphere_pose_transformation_matrix)
        sphere_pose_camera_centers.append(sphere_pose_transformation_matrix[:3,3])
        
        

def GetSphericalCameraFrameIndexFromPoseIndex(pose_index):
    locatableCameraSyncFrameIndex = scanData["hololens_sync_frame"] - 1 # go from 1-indexed to 0-indexed
    locatableCameraSyncFrameTimestamp = sphere_pose_timestamps[locatableCameraSyncFrameIndex]
    
    sphericalCameraSyncFrameIndex = scanData["spherical_sync_frame"] - 1 # go from 1-indexed to 0-indexed
    sphericalCameraFPS = scanData["spherical_fps"]
    
    locatableCameraTimestamp = sphere_pose_timestamps[pose_index]
    secondsSinceCalibrationTime = (locatableCameraTimestamp - locatableCameraSyncFrameTimestamp)
    sphericalCameraFrameIndex = int(round(sphericalCameraSyncFrameIndex + (secondsSinceCalibrationTime * sphericalCameraFPS)))
    return sphericalCameraFrameIndex










triangulation_data = readFile(os.path.join(args.base_dir, TRIANGULATION_FILENAME))

# this is the initial triangulation from the estimated HL poses. The values are pose indices, NOT sphere camera frame numbers
triangulation_indices = triangulation_data["triangulation_indices"]



pose_indices_to_frame_numbers = {}
frame_numbers_to_pose_indices = {}

# generate a representation of the triangulation where we are working with sphere frame indices rather than pose indices.
triangulation_sphere_frame_numbers = []
for pose_index_triangle in triangulation_indices:
    sphere_frame_number_triplet = []
    for pose_index_triangle_val in pose_index_triangle:
        frame_number = GetSphericalCameraFrameIndexFromPoseIndex(pose_index_triangle_val)
        sphere_frame_number_triplet.append(frame_number)
        
        pose_indices_to_frame_numbers[pose_index_triangle_val] = frame_number
        frame_numbers_to_pose_indices[frame_number] = pose_index_triangle_val
        
    triangulation_sphere_frame_numbers.append(sphere_frame_number_triplet)
    

# determine which frame numbers (i.e. which cubemaps) are next to each other in the triangulation
adjacency_map_sphere_frame_numbers = generateAdjacencyMap(triangulation_sphere_frame_numbers)

# determine which frame numbers can be reached within two hops
mappings_in_two_steps_sphere_frame_numbers = getMappingsWithinTwoSteps(adjacency_map_sphere_frame_numbers)



# contains data in the form [frame_number_i, frame_number_j]
out_match_data = []



for frame_number_i in mappings_in_two_steps_sphere_frame_numbers:
    
    pose_idx_i = frame_numbers_to_pose_indices[frame_number_i]
    cam_center_i = sphere_pose_camera_centers[pose_idx_i]
    
    for frame_number_j in mappings_in_two_steps_sphere_frame_numbers[frame_number_i]:
        
        pose_idx_j = frame_numbers_to_pose_indices[frame_number_j]
        cam_center_j = sphere_pose_camera_centers[pose_idx_j]
        
        print("{} {}".format(frame_number_i, frame_number_j))
        
        out_match_data.append([frame_number_i, frame_number_j])



out_match_list_path = os.path.join(args.base_dir, MATCH_LIST_FILENAME)

with open(out_match_list_path, 'wb') as out_file:
    writer = csv.writer(out_file, delimiter=',')
    
    for row in out_match_data:
        writer.writerow(row)






# generate frustums for each of the cubemap sides

# do a robust form of frustum intersection to determine which cubemap sides have valid overlaps
# http://www.yosoygames.com.ar/wp/2016/12/frustum-vs-pyramid-intersection-also-frustum-vs-frustum/
# http://www.iquilezles.org/www/articles/frustumcorrect/frustumcorrect.htm

"""


def testPlotFrustums(frustum):
    num_test_points = 500

    point_range = 500

    test_points = np.hstack((np.random.rand(num_test_points,3) * point_range - (point_range / 2.0) + camera_center, np.ones(num_test_points).reshape(num_test_points,1)))

    inside_points = []
    outside_points = []

    for test_point in test_points:
        
        is_outside = False
        
        for frustum_plane_idx in range(6):
            if frustum_plane_idx > 5:
                break
            
            frustum_plane = frustum.frustum_planes[frustum_plane_idx]
            if np.dot(test_point, frustum_plane) < 0:
                is_outside = True
                break
            
        
        if is_outside:
            outside_points.append(test_point)
        else:
            inside_points.append(test_point)
                
    inside_points = np.array(inside_points)
    outside_points = np.array(outside_points)

    fig = plt.figure(1)
    ax = fig.add_subplot(111, projection='3d')
    
    pts = frustum.frustum_points_world
    for plane_idx in range(6):
        quad_indices_for_this_plane = frustum_plane_quad_indices[plane_idx]
        pts_for_this_quad = pts[quad_indices_for_this_plane]
        poly_verts = [zip(pts_for_this_quad[:,0], pts_for_this_quad[:,1], pts_for_this_quad[:,2])]
        poly = Poly3DCollection(poly_verts)
        poly.set_color('g')
        poly.set_alpha(0.1)
        ax.add_collection3d(poly)
    
    if len(inside_points) > 0:
        ax.scatter(inside_points[:,0], inside_points[:,1], inside_points[:,2], c='g', marker='o')
        
    if len(outside_points) > 0:
        ax.scatter(outside_points[:,0], outside_points[:,1], outside_points[:,2], c='r', marker='o')
    
    plot_center_point = np.array([0.0, 0.0, 0.0])
    plot_range = 100.0
    
    ax.auto_scale_xyz([plot_center_point[0] - plot_range, plot_center_point[0] + plot_range], [plot_center_point[1] - plot_range, plot_center_point[1] + plot_range], [plot_center_point[2] - plot_range, plot_center_point[2] + plot_range])
    plt.show()

















print("generating frustums for each frame-number...")

frame_numbers_to_frustums = {}

for frame_number in mappings_in_two_steps_sphere_frame_numbers:
    frame_numbers_to_frustums[frame_number] = GenerateFrustums(frame_number)
    
print("done generating frustums")



def frustumsOverlap(frustum_i, frustum_j):
    frustum_i_world_points = frustum_i.frustum_points_world
    frustum_j_world_points = frustum_j.frustum_points_world
    
    frustum_i_planes = frustum_i.frustum_planes
    frustum_j_planes = frustum_j.frustum_planes
    
    intersects = True
    
    # http://www.yosoygames.com.ar/wp/2016/12/frustum-vs-pyramid-intersection-also-frustum-vs-frustum/
    # do frustum_i planes vs frustum_j verts
    for i in range(6):
        isAnyVertexInPositiveSide = False
        for j in range(8):
            dotResult = np.dot( frustum_i_planes[i], frustum_j_world_points[j])
            isAnyVertexInPositiveSide = isAnyVertexInPositiveSide or (dotResult > 0)
        
        intersects = intersects and isAnyVertexInPositiveSide
        
    # do frustum_j planes vs frustum_i verts
    for j in range(6):
        isAnyVertexInPositiveSide = False
        for i in range(8):
            dotResult = np.dot( frustum_j_planes[j], frustum_i_world_points[i])
            isAnyVertexInPositiveSide = isAnyVertexInPositiveSide or (dotResult > 0)
        
        intersects = intersects and isAnyVertexInPositiveSide

    return intersects








# maps frame_number_i --> side_idx_i --> frame_number_j --> side_idx_j where i<j
overlapping_frustums = {}

for frame_number_i in mappings_in_two_steps_sphere_frame_numbers:    
    all_frustums_i = frame_numbers_to_frustums[frame_number_i]    
    for frame_number_j in mappings_in_two_steps_sphere_frame_numbers[frame_number_i]:        
        all_frustums_j = frame_numbers_to_frustums[frame_number_j]        
        for side_idx_i in range(6):
            frustum_i = all_frustums_i[side_idx_i]
            for side_idx_j in range(6):
                frustum_j = all_frustums_j[side_idx_j]
                print("comparing frustum ({}, side {}) with ({}, side {})".format(frame_number_i, side_idx_i, frame_number_j, side_idx_j))
                if frustumsOverlap(frustum_i, frustum_j):
                    # add the overlapping frustums to our collection
                    if frame_number_i not in overlapping_frustums:
                        overlapping_frustums[frame_number_i] = {}
                    if side_idx_i not in overlapping_frustums[frame_number_i]:
                        overlapping_frustums[frame_number_i][side_idx_i] = {}
                    if frame_number_j not in overlapping_frustums[frame_number_i][side_idx_i]:
                        overlapping_frustums[frame_number_i][side_idx_i][frame_number_j] = []
                    overlapping_frustums[frame_number_i][side_idx_i][frame_number_j].append(side_idx_j)
                    
                    
                    
                    
out_overlapping_frustums_path = os.path.join(args.base_dir, "overlapping_frustums.yaml")
saveFile(out_overlapping_frustums_path, overlapping_frustums)
    
    
    
        #print("TODO: breaking early")
        #break
    #print("TODO: breaking early")
    #break

def GetCubemapPath(sphere_camera_frame_index):
    cubemapLabel = "pano_frame%05d_cubemap" % (sphere_camera_frame_index + 1,) # frame images are 1-indexed
    cubemapPath = os.path.join(args.base_dir, "pano_frames/cubemaps/" + cubemapLabel + ".jpg")
    return cubemapPath



print("generated overlapping frustum list, finding features and correspondences to use for bundle adjustment...")

orb = cv2.ORB_create()
bf = cv2.BFMatcher(cv2.NORM_HAMMING)


frame_numbers_to_full_cubemap_imgs = {}
frame_numbers_to_side_idx_to_kp = {}
frame_numbers_to_side_idx_to_des = {}



def loadKpDes(img, frame_number, side_idx):
    if frame_number not in frame_numbers_to_side_idx_to_kp:
        frame_numbers_to_side_idx_to_kp[frame_number] = {}
    
    if frame_number not in frame_numbers_to_side_idx_to_des:
        frame_numbers_to_side_idx_to_des[frame_number] = {}
    
    if side_idx not in frame_numbers_to_side_idx_to_kp[frame_number]:
        kp, des = orb.detectAndCompute(img, None)
        frame_numbers_to_side_idx_to_kp[frame_number][side_idx] = kp
        frame_numbers_to_side_idx_to_des[frame_number][side_idx] = des
    return frame_numbers_to_side_idx_to_kp[frame_number][side_idx], frame_numbers_to_side_idx_to_des[frame_number][side_idx]
    
    





cube_side_pixels = None

for frame_number_i in overlapping_frustums:
    
    if frame_number_i not in frame_numbers_to_full_cubemap_imgs:
        frame_numbers_to_full_cubemap_imgs[frame_number_i] = cv2.imread(GetCubemapPath(frame_number_i))
    full_cubemap_i_img = frame_numbers_to_full_cubemap_imgs[frame_number_i]
    
    if cube_side_pixels is None:
        cube_side_pixels = full_cubemap_i_img.shape[0]
    
    for side_idx_i in overlapping_frustums[frame_number_i]:
        if side_idx_i != 4 and side_idx_i != 5:
            
            side_img_i = full_cubemap_i_img[:,side_idx_i*cube_side_pixels:(side_idx_i+1)*cube_side_pixels]
            kp_i, des_i = loadKpDes(side_img_i, frame_number_i, side_idx_i)
            
            
            for frame_number_j in overlapping_frustums[frame_number_i][side_idx_i]:
                
                if frame_number_j not in frame_numbers_to_full_cubemap_imgs:
                    frame_numbers_to_full_cubemap_imgs[frame_number_j] = cv2.imread(GetCubemapPath(frame_number_j))
                full_cubemap_j_img = frame_numbers_to_full_cubemap_imgs[frame_number_j]
                
                for side_idx_j in overlapping_frustums[frame_number_i][side_idx_i][frame_number_j]:
                    if side_idx_j != 4 and side_idx_j != 5:
                        print("comparing frustum ({}, side {}) with ({}, side {})".format(frame_number_i, side_idx_i, frame_number_j, side_idx_j))

                        side_img_j = full_cubemap_j_img[:,side_idx_j*cube_side_pixels:(side_idx_j+1)*cube_side_pixels]
                        kp_j, des_j = loadKpDes(side_img_j, frame_number_j, side_idx_j)
                    
                        if len(kp_i) > 0 and len(kp_j) > 0:
                            matches_i_j = bf.knnMatch(des_i, des_j, k=2)
                        
                            good_matches_i_j = []
                            for m, n in good_matches_i_j:
                                if m.distance < 0.9*n.distance:
                                    good_matches_i_j.append([m])
                            print("found {} good matches".format(len(good_matches_i_j)))
                    
                            #match_img = cv2.drawMatchesKnn(side_img_i, kp_i, side_img_j, kp_j, good_matches_i_j, None, flags=2)
                            #plt.imshow(match_img)
                        
                    #break
                #break
        #break
    #break


"""


"""
fig = plt.figure(1)
ax = fig.add_subplot(111, projection='3d')

for frame_number_i in mappings_in_two_steps_sphere_frame_numbers:  
    all_frustums_i = frame_numbers_to_frustums[frame_number_i]    
    for side_idx_i in range(6):
        frustum_i = all_frustums_i[side_idx_i]
        frustum_i_pts = frustum_i.frustum_points_world
         
        for plane_idx in range(6):
            quad_indices_for_this_plane = frustum_plane_quad_indices[plane_idx]
            pts_for_this_quad = frustum_i_pts[quad_indices_for_this_plane]
            poly_verts = [zip(pts_for_this_quad[:,0], pts_for_this_quad[:,1], pts_for_this_quad[:,2])]
            poly = Poly3DCollection(poly_verts)
            
            poly.set_color(side_idx_to_colors[side_idx_i])
            poly.set_alpha(0.1)
            ax.add_collection3d(poly)
        
        
        for frame_number_j in mappings_in_two_steps_sphere_frame_numbers[frame_number_i]:        
            all_frustums_j = frame_numbers_to_frustums[frame_number_j]        
            for side_idx_j in range(6):
                frustum_j = all_frustums_j[side_idx_j]

                frustum_j_pts = frustum_j.frustum_points_world
            
                for plane_idx in range(6):
                    quad_indices_for_this_plane = frustum_plane_quad_indices[plane_idx]
                    pts_for_this_quad = frustum_j_pts[quad_indices_for_this_plane]
                    poly_verts = [zip(pts_for_this_quad[:,0], pts_for_this_quad[:,1], pts_for_this_quad[:,2])]
                    poly = Poly3DCollection(poly_verts)
                    poly.set_color(side_idx_to_colors[side_idx_j])
                    poly.set_alpha(0.1)
                    ax.add_collection3d(poly)
            
    break
    
    
    
plot_center_point = np.array([0.0, 0.0, 0.0])
plot_range = 20.0

ax.auto_scale_xyz([plot_center_point[0] - plot_range, plot_center_point[0] + plot_range], [plot_center_point[1] - plot_range, plot_center_point[1] + plot_range], [plot_center_point[2] - plot_range, plot_center_point[2] + plot_range])
plt.show()
"""

