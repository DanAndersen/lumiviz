# -*- coding: utf-8 -*-
"""
Created on Sat Feb 17 12:41:38 2018

@author: ANDERSED

Given the refined watertight mesh from the Spatial Understanding, iteratively improve the sphere poses.

Inputs are the pairwise SPHORB matches between nearby panos, as well as the list of inlier indices for each one.

The error that is to be minimized isangular reprojection error of points that are collided against the mesh.

For a match (xyz_i, xyz_j) between pano i and pano j, we use the existing translation/rotation parameters
to send out the ray xyz_i into the world, colliding against the mesh at point coll_i. We then compare the 
direction from pano j to coll_i with the direction of xyz_j.

"""


import os
os.chdir("E:\\Dev\\OpenGL\\lumiviz\\resources\\scans")

# Imports
import numpy as np
import cv2
import argparse
import os
from holoscan_common import POSE_SPHERE_FILENAME, readFile, saveFile, TRIANGULATION_FILENAME, SCANDATA_FILENAME, solve_essential_matrix, sphereImgPointToXYZUnitSphereMultiple, rayDistanceToEpipolarPlane, LineLineIntersect, xyzUnitSphereToSphereImgPoint
import csv
import scipy
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import glob
import re
import profile

import process_scan_data_helpers

from scipy.sparse import lil_matrix

import time
from scipy.optimize import least_squares

import multiprocessing

import trimesh

# =============================================================================

def getPanoFramePath(zero_indexed_frame_number):
    return os.path.join(args.base_dir, "pano_frames", "pano_frame%05d.jpg" % (zero_indexed_frame_number + 1,))

class PanoMatches:
    def __init__(self, frame_number_i, frame_number_j, img_matches, estimated_dist_meters):
        self.frame_number_i = frame_number_i
        self.frame_number_j = frame_number_j
        self.matches = img_matches # matches in image coordinates
        
        self.estimated_dist_meters = estimated_dist_meters # from HoloLens, initial estimated distance in meters
        
        num_matches = len(self.matches)
        
        match_img_pts_i = np.array([[match[0], match[1]] for match in self.matches])
        match_img_pts_j = np.array([[match[2], match[3]] for match in self.matches])
        
        match_xyz_pts_i = sphereImgPointToXYZUnitSphereMultiple(match_img_pts_i[:,0], match_img_pts_i[:,1], pano_frame_resolution)
        match_xyz_pts_j = sphereImgPointToXYZUnitSphereMultiple(match_img_pts_j[:,0], match_img_pts_j[:,1], pano_frame_resolution)
        
        self.match_xyzs = []
        for idx in range(num_matches):
            self.match_xyzs.append([match_xyz_pts_i[idx], match_xyz_pts_j[idx]])
  
    def draw_matches(self, inlier_indices_list, window_title):
        img_i = cv2.imread(getPanoFramePath(self.frame_number_i))
        img_j = cv2.imread(getPanoFramePath(self.frame_number_j))
        
        img_i_height = img_i.shape[0]
        
        inlier_color = (0,255,0)
        outlier_color = (0,0,255)
        
        line_thickness = 2
        
        match_img = np.concatenate((img_i, img_j), axis=0)
        for i in range(len(self.matches)):
            match = self.matches[i]
            pt1 = (int(match[0]), int(match[1]))
            pt2 = (int(match[2]), int(match[3] + img_i_height))
            cv2.line(match_img, pt1, pt2, inlier_color if i in inlier_indices_list else outlier_color, line_thickness)
        
        cv2.namedWindow(window_title, cv2.WINDOW_NORMAL)
        cv2.resizeWindow(window_title,1024,1024)
        cv2.imshow(window_title, match_img)


def GetSphericalCameraFrameIndexFromPoseIndex(pose_index):
    locatableCameraSyncFrameIndex = scanData["hololens_sync_frame"] - 1 # go from 1-indexed to 0-indexed
    locatableCameraSyncFrameTimestamp = sphere_pose_timestamps[locatableCameraSyncFrameIndex]
    
    sphericalCameraSyncFrameIndex = scanData["spherical_sync_frame"] - 1 # go from 1-indexed to 0-indexed
    sphericalCameraFPS = scanData["spherical_fps"]
    
    locatableCameraTimestamp = sphere_pose_timestamps[pose_index]
    secondsSinceCalibrationTime = (locatableCameraTimestamp - locatableCameraSyncFrameTimestamp)
    sphericalCameraFrameIndex = int(round(sphericalCameraSyncFrameIndex + (secondsSinceCalibrationTime * sphericalCameraFPS)))
    return sphericalCameraFrameIndex

# =============================================================================

parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)

scanData = readFile(os.path.join(args.base_dir, SCANDATA_FILENAME))

pano_frame_dir = os.path.join(args.base_dir, "pano_frames")

# Determine size of panoramas.
pano_frame_resolution = cv2.imread(glob.glob(os.path.join(pano_frame_dir, "pano_frame*.jpg"))[0]).shape[::-1][1:]

# =============================================================================

# Load in the sphere_pose data (estimated pose for every pose index, from HL) 

sphere_pose_timestamps = []
sphere_pose_matrices = []
sphere_pose_camera_centers = []

with open(os.path.join(args.base_dir, POSE_SPHERE_FILENAME), 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    
    for row in reader:
        timestamp = float(row[0])
        sphere_pose_timestamps.append(timestamp)        
        sphere_pose_transformation_matrix = np.array([float(x) for x in row[1:]]).reshape(4,4)
        sphere_pose_matrices.append(sphere_pose_transformation_matrix)
        sphere_pose_camera_centers.append(sphere_pose_transformation_matrix[:3,3])
        
sphere_pose_camera_centers = np.array(sphere_pose_camera_centers)

# Map between frame numbers and pose indices


pose_indices_to_frame_numbers = {}
frame_numbers_to_pose_indices = {}

with open(os.path.join(args.base_dir, POSE_SPHERE_FILENAME), 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    
    pidx = 0
    for row in reader:
        fn = GetSphericalCameraFrameIndexFromPoseIndex(pidx)
        pose_indices_to_frame_numbers[pidx] = fn
        frame_numbers_to_pose_indices[fn] = pidx
        pidx += 1
        
# =============================================================================

# Load in the refined holo-mesh.
refined_holoroom_file_results = glob.glob(os.path.join(args.base_dir, "*_mesh_refined.room"))
if len(refined_holoroom_file_results) == 0:
    raise Exception("no refined holoroom file found")
refined_holoroom_file_path = refined_holoroom_file_results[0]

print("Reading HoloLens room geometry from {}...".format(refined_holoroom_file_path))
holoroom_mesh_verts, holoroom_mesh_indices = process_scan_data_helpers.load_holoroom_mesh(refined_holoroom_file_path)
print("Read HoloLens room geometry, {} verts and {} triangles".format(len(holoroom_mesh_verts), len(holoroom_mesh_indices)))

# =============================================================================

# Create Trimesh object.

print("creating trimesh for holoroom")
holoroom_mesh = trimesh.Trimesh(vertices = holoroom_mesh_verts, faces = holoroom_mesh_indices)
print("done creating trimesh")

# can view mesh in 3d with
# holoroom_mesh.show() # slow to load

# =============================================================================

# setting up the ray-mesh intersector
print("Setting up ray-mesh intersector for holoroom...")
#intersector = trimesh.ray.ray_triangle.RayMeshIntersector(holoroom_mesh)
intersector = holoroom_mesh.ray
print("intersector created.")
# intersector should be of type "trimesh.ray.ray_pyembree" or else this will be VERY slow

# =============================================================================

num_params_per_camera = 6 # [rx,ry,rz,tx,ty,tz]

match_limit = 100 # maximum number of inlier matches to actually use in the refinement

# =============================================================================

# Try loading in one panomatch to see how ray intersecting works for it.

camidx_to_fn = []   # unique list of frame_numbers
all_panomatches = []
print("Loading in all panomatches...")
input_sphorb_match_paths = glob.glob(os.path.join(pano_frame_dir, "sphorb_matches", "sphorb_match_[0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9].csv"))
for input_sphorb_match_path in input_sphorb_match_paths:
    csv_filename = os.path.split(input_sphorb_match_path)[-1]
    zero_indexed_frame_numbers = [int(x) - 1 for x in re.findall(r'\d+', csv_filename)] # go from 1-indexed to 0-indexed
    
    input_essential_path = os.path.join(pano_frame_dir, "sphorb_matches", "sphorb_match_%05d_%05d_essential.yaml" % (zero_indexed_frame_numbers[0] + 1, zero_indexed_frame_numbers[1] + 1))
    if os.path.isfile(input_essential_path):
        match_inlier_indices = readFile(input_essential_path)['best_fit_inlier_indices']
        pose_indices_ij = [frame_numbers_to_pose_indices[_fn] for _fn in zero_indexed_frame_numbers]
        estimated_camera_centers = [sphere_pose_camera_centers[_pidx] for _pidx in pose_indices_ij]
        estimated_dist_meters = np.linalg.norm(estimated_camera_centers[1] - estimated_camera_centers[0])
        matches = []
        with open(input_sphorb_match_path, 'rb') as f:
            reader = csv.reader(f, delimiter=',')
            rowidx = 0
            for row in reader:
                if rowidx in match_inlier_indices: # don't both loading in matches that are not inliers
                    match = [float(x) for x in row]
                    matches.append(match)
                rowidx += 1
        
        matches = np.array(matches)
        
        if len(matches) > 0:
            num_matches_to_choose = min(len(matches), match_limit)
        
            selected_match_idxs = np.sort(np.random.choice(len(matches), num_matches_to_choose, replace=False))
            matches = matches[selected_match_idxs]
            
            p = PanoMatches(zero_indexed_frame_numbers[0], zero_indexed_frame_numbers[1], matches, estimated_dist_meters)
            all_panomatches.append(p)
            if zero_indexed_frame_numbers[0] not in camidx_to_fn:
                camidx_to_fn.append(zero_indexed_frame_numbers[0])
            if zero_indexed_frame_numbers[1] not in camidx_to_fn:
                camidx_to_fn.append(zero_indexed_frame_numbers[1])
    else: 
        print("No essential YAML data found at {}. Skipping".format(input_essential_path))

num_cameras = len(camidx_to_fn)
print("Done loading in {} panomatches, for {} unique cameras".format(len(all_panomatches), num_cameras))

# =============================================================================

fn_to_camidx = {}
for camidx in range(len(camidx_to_fn)):
    fn = camidx_to_fn[camidx]
    fn_to_camidx[fn] = camidx

# =============================================================================

# Set up the input cam params.

rot_params_mask = np.zeros(num_cameras * num_params_per_camera, dtype=np.bool)
trans_params_mask = np.zeros(num_cameras * num_params_per_camera, dtype=np.bool)

input_params = np.zeros(num_cameras * num_params_per_camera)
for camidx in range(num_cameras):
    cam_fn = camidx_to_fn[camidx]
    cam_pidx = frame_numbers_to_pose_indices[cam_fn]
    
    cam_mat = sphere_pose_matrices[cam_pidx]
    
    cam_rot_mat = cam_mat[:3,:3]
    cam_rot_vector, _ = cv2.Rodrigues(cam_rot_mat)
    cam_rot_vector = cam_rot_vector.reshape(-1)
    cam_trans_vector = sphere_pose_camera_centers[cam_pidx]
    
    input_params[(camidx * num_params_per_camera) : (camidx * num_params_per_camera + 3)] = cam_rot_vector
    input_params[(camidx * num_params_per_camera + 3) : (camidx * num_params_per_camera + 6)] = cam_trans_vector
                 
    rot_params_mask[(camidx * num_params_per_camera) : (camidx * num_params_per_camera + 3)] = True
    trans_params_mask[(camidx * num_params_per_camera + 3) : (camidx * num_params_per_camera + 6)] = True
                 
# =============================================================================

# Prep the match data from all the panomatches.
num_inlier_matches = 0
for p in all_panomatches:
    num_inlier_matches += len(p.match_xyzs)
    
print("total of {} inlier matches across all panomatches".format(num_inlier_matches))

print("organizing data...")
all_match_xyzs = np.zeros((num_inlier_matches, 6)) # [xi,yi,zi,xj,yj,zj]
all_match_camidxs = np.zeros((num_inlier_matches, 2), dtype=np.uint8) # [camidx_i, camidx_j]
match_idx = 0
for p in all_panomatches:
    num_matches_in_this_panomatch = len(p.match_xyzs)
    
    match_xyzs_i = np.array([m[0] for m in p.match_xyzs])
    match_xyzs_j = np.array([m[1] for m in p.match_xyzs])
    
    all_match_xyzs[match_idx:match_idx+num_matches_in_this_panomatch, 0:3] = match_xyzs_i
    all_match_xyzs[match_idx:match_idx+num_matches_in_this_panomatch, 3:6] = match_xyzs_j
    
    camidx_i = fn_to_camidx[p.frame_number_i]
    camidx_j = fn_to_camidx[p.frame_number_j]
                  
    all_match_camidxs[match_idx:match_idx+num_matches_in_this_panomatch, 0] = camidx_i
    all_match_camidxs[match_idx:match_idx+num_matches_in_this_panomatch, 1] = camidx_j
                  
    match_idx += num_matches_in_this_panomatch
    
print("data organized")


# =============================================================================

# try loading in the previous refined params... see if they are any good here
in_refined_params = np.zeros(num_cameras * num_params_per_camera)
FN_TO_POSE_REFINED_FILENAME = "fn_to_pose_refined.csv"
fn_pose_initial_path = os.path.join(args.base_dir, FN_TO_POSE_REFINED_FILENAME)
with open(fn_pose_initial_path, 'rb') as in_file:
    reader = csv.reader(in_file, delimiter=',')
    
    for row in reader:
        fn = int(row[0])
        mat = np.array([float(x) for x in row[1:]]).reshape(4,4)

        cam_trans_vector = mat[:3,3]
        r = mat[:3,:3]
        cam_rot_vector, _ = cv2.Rodrigues(r)
        cam_rot_vector = cam_rot_vector.reshape(-1)
        
        camidx = fn_to_camidx[fn]
        
        in_refined_params[(camidx * num_params_per_camera) : (camidx * num_params_per_camera + 3)] = cam_rot_vector
        in_refined_params[(camidx * num_params_per_camera + 3) : (camidx * num_params_per_camera + 6)] = cam_trans_vector
        
        





# =============================================================================

# Let's try to perform a single function for getting the errors
#current_params = input_params
#current_params = in_refined_params


def fun(current_params):

    current_rot_params = current_params[rot_params_mask].reshape(-1,3)
    current_trans_params = current_params[trans_params_mask].reshape(-1,3)
    
    cam_rot_mats = np.array([cv2.Rodrigues(current_rot_vec)[0] for current_rot_vec in current_rot_params])
    
    matches_start_ij_world = current_trans_params[all_match_camidxs]
    matches_start_i_world = matches_start_ij_world[:,0]
    matches_start_j_world = matches_start_ij_world[:,1]
    
    # how to figure out the matches end?
    
    matches_corresponding_ij_rot_mats = cam_rot_mats[all_match_camidxs]
    matches_corresponding_i_rot_mats = matches_corresponding_ij_rot_mats[:,0]
    matches_corresponding_j_rot_mats = matches_corresponding_ij_rot_mats[:,1]
    
    matches_end_i_local = all_match_xyzs[:,0:3]
    matches_end_j_local = all_match_xyzs[:,3:6]
    
    # the einsum is a fast way of multiplying each rot_mat with its corresponding vector.
    # the slow way is... matches_direction_i_world_slow = np.array([np.matmul(matches_corresponding_i_rot_mats[idx], matches_end_i_local[idx]) for idx in range(num_inlier_matches)])
    matches_direction_i_world_fast = np.einsum('...ij,...j->...i', matches_corresponding_i_rot_mats, matches_end_i_local)
                                        
    matches_direction_j_world_fast = np.einsum('...ij,...j->...i', matches_corresponding_j_rot_mats, matches_end_j_local)
                                        
    print("Starting ray intersections...")
    start_time = time.time()
    i_intersect_res = intersector.intersects_id(matches_start_i_world, matches_direction_i_world_fast, return_locations=True, multiple_hits=False)
    j_intersect_res = intersector.intersects_id(matches_start_j_world, matches_direction_j_world_fast, return_locations=True, multiple_hits=False)
    elapsed_time = time.time() - start_time
    print("Did ray intersections, took {} seconds".format(elapsed_time))
    
    i_intersect_res_indextri = i_intersect_res[0]
    i_intersect_res_indexray = i_intersect_res[1]   # ray indices after decimation
    i_intersect_res_locations = i_intersect_res[2]
    
    j_intersect_res_indextri = j_intersect_res[0]
    j_intersect_res_indexray = j_intersect_res[1]   # ray indices after decimation
    j_intersect_res_locations = j_intersect_res[2]
    
    
    if not np.array_equal(i_intersect_res_indexray, j_intersect_res_indexray):
        # the ray results were not equal... error due to not completely watertight mesh?
        updated_i_intersect_res_locations = np.zeros((num_inlier_matches, 3))
        updated_j_intersect_res_locations = np.zeros((num_inlier_matches, 3))
        
        updated_i_intersect_res_locations[i_intersect_res_indexray] = i_intersect_res_locations
        updated_j_intersect_res_locations[j_intersect_res_indexray] = j_intersect_res_locations
                                         
        i_intersect_res_locations = updated_i_intersect_res_locations
        j_intersect_res_locations = updated_j_intersect_res_locations
    
    intersect_dists = np.linalg.norm(i_intersect_res_locations - j_intersect_res_locations, axis=1)
    
    print("end fun(), mean abs err = {}".format(np.mean(np.abs(intersect_dists))))
    
    return intersect_dists
    """
    # Assume that i_intersect_res_indexray and j_intersect_res_indexray are identical and in order [0, 1, 2, ..., num_inlier_matches-1]
    
    # We need to compute angular errors. Each match will have 2 errors.
    # First is the angle between vector (j_start to i_intersect_loc) and vector (j_start to j_start+j_dir).
    # Second is the angle between vector (i_start to j_intersect_loc) and vector (i_start to i_start+i_dir).
    # If the intersection point was the same for each of them, i_intersect_loc would equal j_intersect_loc,
    # and the errors would be zero.
    
    i_to_i_feature = matches_direction_i_world_fast
    i_to_j_intersect = j_intersect_res_locations - matches_start_i_world
    i_to_j_intersect /= np.linalg.norm(i_to_j_intersect, axis=1, keepdims=True)
    
    j_to_j_feature = matches_direction_j_world_fast
    j_to_i_intersect = i_intersect_res_locations - matches_start_j_world
    j_to_i_intersect /= np.linalg.norm(j_to_i_intersect, axis=1, keepdims=True)
    
    cos_angles_1 = np.einsum('ij,ij->i', i_to_i_feature, i_to_j_intersect) # row-wise dot products ( because A dot B = |A| |B| cos theta)
    cos_angles_2 = np.einsum('ij,ij->i', j_to_j_feature, j_to_i_intersect)
    
    angles_1 = np.arccos(cos_angles_1)
    angles_2 = np.arccos(cos_angles_2)
    
    angles_1_and_2_interleaved = np.ravel(np.column_stack((angles_1,angles_2)))
    
    print("end fun(), mean abs err = {}".format(np.mean(np.abs(angles_1_and_2_interleaved))))
    
    return angles_1_and_2_interleaved
    """


print("generating Jacobian sparsity...")
#residual_dimension = 2
residual_dimension = 1
# Make the Jacobian sparsity structure
m = num_inlier_matches * residual_dimension # 2D residual from each match
n = num_cameras * num_params_per_camera
A = lil_matrix((m, n), dtype=int)

all_match_camidxs_i = all_match_camidxs[:,0]
all_match_camidxs_j = all_match_camidxs[:,1]

i = np.arange(num_inlier_matches)
for s in range(num_params_per_camera):
    A[1 * i, all_match_camidxs_i * num_params_per_camera + s] = 1
    A[1 * i, all_match_camidxs_j * num_params_per_camera + s] = 1
     
    A[1 * i + 0, all_match_camidxs_i * num_params_per_camera + s] = 1
    A[1 * i + 0, all_match_camidxs_j * num_params_per_camera + s] = 1
     
    #A[residual_dimension * i + 1, all_match_camidxs_i * num_params_per_camera + s] = 1
    #A[residual_dimension * i + 1, all_match_camidxs_j * num_params_per_camera + s] = 1
print("generated.")





start_time = time.time()
initial_errs = fun(input_params)
#initial_errs = profile.run('fun(input_params, num_cameras, panomatchidx_to_camidxs)', sort='tottime')
elapsed_time = time.time() - start_time
print("elapsed time for one iteration of fun(): {}".format(elapsed_time))










print("starting optimization NO PARAM BOUNDS, please wait...")
t0 = time.time()

#res = least_squares(fun, input_params, bounds=param_bounds, jac_sparsity=A, verbose=2, x_scale='jac', ftol=1e-2, method='trf', loss='soft_l1', args=(num_cameras, panomatchidx_to_camidxs))
res = least_squares(fun, input_params, jac_sparsity=A, verbose=2, x_scale='jac', ftol=1e-2, method='trf', args=())

t1 = time.time()
print("Optimization took {0:.0f} seconds".format(t1 - t0))













"""
decimation_amount = 1000

xs = []
ys = []
zs = []
#for idx in range(len(i_intersect_res_locations) / decimation_amount):
#    i = idx*decimation_amount
for idx in range(1000):
    i = idx
    
    xs.append(matches_start_i_world[i,0])
    ys.append(matches_start_i_world[i,1])
    zs.append(matches_start_i_world[i,2])
    
    xs.append(i_intersect_res_locations[i,0])
    ys.append(i_intersect_res_locations[i,1])
    zs.append(i_intersect_res_locations[i,2])
    
    xs.append(None)
    ys.append(None)
    zs.append(None)
    
    xs.append(matches_start_j_world[i,0])
    ys.append(matches_start_j_world[i,1])
    zs.append(matches_start_j_world[i,2])
    
    xs.append(j_intersect_res_locations[i,0])
    ys.append(j_intersect_res_locations[i,1])
    zs.append(j_intersect_res_locations[i,2])
    
    xs.append(None)
    ys.append(None)
    zs.append(None)
    
    
    
    xs.append(i_intersect_res_locations[i,0])
    ys.append(i_intersect_res_locations[i,1])
    zs.append(i_intersect_res_locations[i,2])
    
    xs.append(j_intersect_res_locations[i,0])
    ys.append(j_intersect_res_locations[i,1])
    zs.append(j_intersect_res_locations[i,2])
    
    xs.append(None)
    ys.append(None)
    zs.append(None)
    
    

#fig = plt.figure(1)
#ax = fig.add_subplot(111, projection='3d')
plt.plot(ys, zs)
plt.scatter(current_trans_params[:,0], current_trans_params[:,2])
"""