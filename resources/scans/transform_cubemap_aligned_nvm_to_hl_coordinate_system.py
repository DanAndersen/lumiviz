# -*- coding: utf-8 -*-
"""
Created on Wed Nov 08 11:51:20 2017

@author: ANDERSED
"""

# loads in an NVM file that has already been 'bundle adjusted' for cubemap constraints
# generates a list of cubemaps, each associated with its frame number and its original rotation/translation in the VisualSFM coordinate system
# generates a list of points, in their original rotation/translation
# generates a list of observations -- for a particular cubemap, which points were seen by which cubemaps
# generates a list of triplet observations -- for a particular triplet of cubemaps, which points were seen by all three of them
# also loads in the triangulation data for the scan session
# also loads in the original pose from the HoloLens (spherical or holo-camera here?)
# finds a transformation matrix that fits the corresponding poses to each other (changes translation/rotation/scale)
# updates the rotation/transformation of the cubemaps, as well as of the points
# saves all this data to a YAML file so I don't have to recompute it again and again


import argparse
import re
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import yaml
import os
import csv
import functools
from scipy.spatial import ConvexHull
from scipy.spatial import Delaunay
from holoscan_common import writeOutputTripletMeshData

class Camera:
    def __init__(self):
        self.name = "unknown"
        self.frame_number = -1
        self.side_idx = -1
        self.m = np.identity(3)
        self.t = np.zeros(3)

    def SetQuaternionRotation(self, q):
        qq = np.sqrt(q[0]*q[0]+q[1]*q[1]+q[2]*q[2]+q[3]*q[3])
        if qq>0:
            qw=q[0]/qq
            qx=q[1]/qq
            qy=q[2]/qq
            qz=q[3]/qq
        else:
            qw = 1
            qx = qy = qz = 0
        
        self.m[0][0]=float(qw*qw + qx*qx- qz*qz- qy*qy )
        self.m[0][1]=float(2*qx*qy -2*qz*qw )
        self.m[0][2]=float(2*qy*qw + 2*qz*qx)
        self.m[1][0]=float(2*qx*qy+ 2*qw*qz)
        self.m[1][1]=float(qy*qy+ qw*qw - qz*qz- qx*qx)
        self.m[1][2]=float(2*qz*qy- 2*qx*qw)
        self.m[2][0]=float(2*qx*qz- 2*qy*qw)
        self.m[2][1]=float(2*qy*qz + 2*qw*qx )
        self.m[2][2]=float(qz*qz+ qw*qw- qy*qy- qx*qx)
        
        #self.m = self.m.transpose()
        
    def SetCameraCenterAfterRotation(self, c):
        # t = - R * C
        for j in range(3):
            self.t[j] = -float(self.m[j][0] * c[0] + self.m[j][1] * c[1] + self.m[j][2] * c[2])

    def GetCameraCenter(self):
        c = np.zeros(3)
        # C = - R' * t
        for j in range(3):
            c[j] = -float(self.m[0][j]* self.t[0] + self.m[1][j] * self.t[1] + self.m[2][j] * self.t[2])
        return c

    def GetQuaternionRotation(self):
        q = [0.0] * 4
        q[0]= 1 + self.m[0][0] + self.m[1][1] + self.m[2][2]
        if q[0]>0.000000001:
            q[0] = np.sqrt(q[0])/2.0
            q[1]= (self.m[2][1] - self.m[1][2])/( 4.0 *q[0])
            q[2]= (self.m[0][2] - self.m[2][0])/( 4.0 *q[0])
            q[3]= (self.m[1][0] - self.m[0][1])/( 4.0 *q[0])
        else:
            if self.m[0][0] > self.m[1][1] and self.m[0][0] > self.m[2][2]:
                s = 2.0 * np.sqrt( 1.0 + self.m[0][0] - self.m[1][1] - self.m[2][2])
                q[1] = 0.25 * s
                q[2] = (self.m[0][1] + self.m[1][0] ) / s
                q[3] = (self.m[0][2] + self.m[2][0] ) / s
                q[0] = (self.m[1][2] - self.m[2][1] ) / s    
            elif self.m[1][1] > self.m[2][2]:
                s = 2.0 * np.sqrt( 1.0 + self.m[1][1] - self.m[0][0] - self.m[2][2])
                q[1] = (self.m[0][1] + self.m[1][0] ) / s
                q[2] = 0.25 * s
                q[3] = (self.m[1][2] + self.m[2][1] ) / s
                q[0] = (self.m[0][2] - self.m[2][0] ) / s    
            else:
                s = 2.0 * np.sqrt( 1.0 + self.m[2][2] - self.m[0][0] - self.m[1][1] )
                q[1] = (self.m[0][2] + self.m[2][0] ) / s
                q[2] = (self.m[1][2] + self.m[2][1] ) / s
                q[3] = 0.25 * s
                q[0] = (self.m[0][1] - self.m[1][0] ) / s
            
        return q



TRIANGULATION_FILENAME = "sphere_pose_triangulation.yaml"
REFINED_TRIANGULATION_FILENAME = "sphere_pose_triangulation_refined_frame_numbers.yaml"
POSE_SPHERE_FILENAME = "pose_sphere.csv"
SCANDATA_FILENAME = "scandata.yaml"

REFINED_CUBEMAP_DATA_FILENAME = "refined_cubemap_data.csv"


def saveFile(fname,data):
	fd = open(fname,"wb")
	yaml.dump(data,fd)
	fd.close()

def readFile(fname):
	fd = open(fname,"rb")
	data = yaml.load(fd)
	return data


def multiplyQuaternions(q, r):
    # computes t = q * r
    t0 = (r[0]*q[0] - r[1]*q[1] - r[2]*q[2] - r[3]*q[3])
    t1 = (r[0]*q[1] + r[1]*q[0] - r[2]*q[3] + r[3]*q[2])
    t2 = (r[0]*q[2] + r[1]*q[3] + r[2]*q[0] - r[3]*q[1])
    t3 = (r[0]*q[3] - r[1]*q[2] + r[2]*q[1] + r[3]*q[0])
    
    return [t0,t1,t2,t3]

def AngleAxisToQuaternion(angle_axis):
    a0 = angle_axis[0]
    a1 = angle_axis[1]
    a2 = angle_axis[2]
    theta_squared = a0 * a0 + a1 * a1 + a2 * a2
    
    q = [0.0] * 4
    
    # For points not at the origin, the full conversion is numerically stable.
    if theta_squared > 0.0:
        theta = np.sqrt(theta_squared)
        half_theta = theta * 0.5
        k = np.sin(half_theta) / theta
        q[0] = np.cos(half_theta)
        q[1] = a0 * k
        q[2] = a1 * k
        q[3] = a2 * k
    else:
        # At the origin, sqrt() will produce NaN in the derivative since
        # the argument is zero.  By approximating with a Taylor series,
        # and truncating at one term, the value and first derivatives will be
        # computed correctly when Jets are used.
        k = 0.5
        q[0] = 1.0
        q[1] = a0 * k
        q[2] = a1 * k
        q[3] = a2 * k
  
    return q


def GetSphericalCameraFrameIndexFromPoseIndex(pose_index):
    locatableCameraSyncFrameIndex = scanData["hololens_sync_frame"] - 1 # go from 1-indexed to 0-indexed
    locatableCameraSyncFrameTimestamp = sphere_pose_timestamps[locatableCameraSyncFrameIndex]
    
    sphericalCameraSyncFrameIndex = scanData["spherical_sync_frame"] - 1 # go from 1-indexed to 0-indexed
    sphericalCameraFPS = scanData["spherical_fps"]
    
    locatableCameraTimestamp = sphere_pose_timestamps[pose_index]
    secondsSinceCalibrationTime = (locatableCameraTimestamp - locatableCameraSyncFrameTimestamp)
    sphericalCameraFrameIndex = int(round(sphericalCameraSyncFrameIndex + (secondsSinceCalibrationTime * sphericalCameraFPS)))
    return sphericalCameraFrameIndex



parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)


scanData = readFile(os.path.join(args.base_dir, SCANDATA_FILENAME))


sphere_pose_timestamps = []

with open(os.path.join(args.base_dir, POSE_SPHERE_FILENAME), 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        timestamp = float(row[0])
        
        sphere_pose_timestamps.append(timestamp)







input_nvm_file = r'E:\Dev\Ceres\cubemap_bundle_adjustment\build\OUT_processed_after_solve_workspace_2step.nvm'

print("TODO! need to change location of input NVM file so it's contained with the scan directory!")
print("input_nvm_file = " + input_nvm_file)

num_raw_cameras = 0
num_points = 0

raw_cameras = []

point_xyzs = []

imported_cam_idx_to_point_idx = {}

frame_number_to_side_idx_to_imported_camera = {}

with open(input_nvm_file, 'r') as fstream:
    header_line = fstream.readline().split()
    while True:
        line = fstream.readline().split()
        if len(line) > 0:
            break
    num_raw_cameras = int(line[0])
    print("found {} raw cameras".format(num_raw_cameras))
    for i in range(num_raw_cameras):
        line = fstream.readline().split()
        raw_cam_name = line[0]
        
        regex_match_groups = re.match("pano_frame([0-9]+)_cubemap_face_([0-9]+).jpg", raw_cam_name).groups()
        
        frame_number = int(regex_match_groups[0])   # 1-indexed!
        side_idx = int(regex_match_groups[1])
        
        
        if frame_number not in frame_number_to_side_idx_to_imported_camera:
            frame_number_to_side_idx_to_imported_camera[frame_number] = {}
        
        frame_number_to_side_idx_to_imported_camera[frame_number][side_idx] = i
        
        raw_cam_quat = [float(x) for x in line[2:6]]
        raw_cam_center = [float(x) for x in line[6:9]]
        
        raw_cam = Camera()
        raw_cam.name = raw_cam_name
        raw_cam.frame_number = frame_number
        raw_cam.side_idx = side_idx
        raw_cam.SetQuaternionRotation(raw_cam_quat)
        raw_cam.SetCameraCenterAfterRotation(raw_cam_center)
        
        raw_cameras.append(raw_cam)
        
    while True:
        line = fstream.readline().split()
        if len(line) > 0:
            break
    num_points = int(line[0])
    print("found {} points".format(num_points))
    for i in range(num_points):
        line = fstream.readline().split()
        point_xyz = [float(x) for x in line[0:3]]
        
        point_xyzs.append(point_xyz)
        
        num_measurements = int(line[6])
        for j in range(num_measurements):
            raw_camera_index = int(line[7 + 4*j])
            
            if raw_camera_index not in imported_cam_idx_to_point_idx:
                imported_cam_idx_to_point_idx[raw_camera_index] = set()
            imported_cam_idx_to_point_idx[raw_camera_index].add(i)
print("done reading data")

# turn raw cameras to unified cubemaps
num_cubemaps = len(frame_number_to_side_idx_to_imported_camera)

frame_number_to_new_cubemap_idx = {}
new_cubemap_idx_to_frame_number = []


i = 0
for frame_number in frame_number_to_side_idx_to_imported_camera:
    frame_number_to_new_cubemap_idx[frame_number] = i
    new_cubemap_idx_to_frame_number.append(frame_number)
    i+=1



cubemapFaceAngleAxisRotations = []
cubemapFaceAngleAxisRotations.append([0.0, 0.0, 0.0])   # forward
cubemapFaceAngleAxisRotations.append([0.0, np.pi/2.0, 0.0])   # right
cubemapFaceAngleAxisRotations.append([0.0, np.pi, 0.0])   # back
cubemapFaceAngleAxisRotations.append([0.0, -np.pi/2.0, 0.0])   # left
cubemapFaceAngleAxisRotations.append([-np.pi/2.0, 0.0, 0.0])   # up
cubemapFaceAngleAxisRotations.append([np.pi/2.0, 0.0, 0.0])   # down



cubemap_face_quaternions = []
for cubemapFaceAngleAxisRotation in cubemapFaceAngleAxisRotations:
    cubemap_face_quaternions.append( AngleAxisToQuaternion(cubemapFaceAngleAxisRotation) )


flipAngleAxis = [ np.pi, 0.0, 0.0 ]
cameraFlipQuat = AngleAxisToQuaternion(flipAngleAxis)



cubemap_cameras = []    # cameras representing the forward-facing side of each cubemap

for i in range(num_cubemaps):
    frame_number = new_cubemap_idx_to_frame_number[i]
    
    side_idx_to_imported_camera = frame_number_to_side_idx_to_imported_camera[frame_number]
    
    num_imported_cameras_for_this_cubemap = len(side_idx_to_imported_camera)
    
    for side_idx in side_idx_to_imported_camera:
        imported_camera_index = side_idx_to_imported_camera[side_idx]
        imported_camera = raw_cameras[imported_camera_index]
        
        cubemapFaceRotation = cubemap_face_quaternions[side_idx]
        
        originForwardToCubemapSideDirQuaternion = imported_camera.GetQuaternionRotation()
        
        # transforms to front-facing rotation of cubemap, also adjusts rotation so that camera is y-up z-back rather than y-down z-forward (as in VisualSFM)
        originForwardToCubemapForwardQuaternion = multiplyQuaternions( multiplyQuaternions(cameraFlipQuat, cubemapFaceRotation), originForwardToCubemapSideDirQuaternion)
        
        imported_camera_center = imported_camera.GetCameraCenter()
        
        
        cubemap_camera = Camera()
        
        cubemap_camera.SetQuaternionRotation(originForwardToCubemapForwardQuaternion)
        cubemap_camera.SetCameraCenterAfterRotation(imported_camera_center)
        
        cubemap_cameras.append(cubemap_camera)
        
        break # as soon as we have one, we'll use that instead of averaging it with the others -- our previous step already aligned them all
        




# try plotting all the raw data we have

point_xyzs = np.array(point_xyzs)

"""
num_points_for_subset = 1000
subset_points = point_xyzs[np.random.randint(point_xyzs.shape[0], size=num_points_for_subset), :]

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(subset_points[:,0], subset_points[:,1], subset_points[:,2], color='red')
plt.show()
"""





triangulation_data = readFile(os.path.join(args.base_dir, TRIANGULATION_FILENAME))

triangulation_indices = triangulation_data["triangulation_indices"]

in_spherical_pose_csv_path = os.path.join(args.base_dir, POSE_SPHERE_FILENAME)


sphere_positions = []

with open(in_spherical_pose_csv_path, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        timestamp = float(row[0])
        
        sphere_m = np.array([float(i) for i in row[1:]]).reshape((4,4))
        
        pos = sphere_m[:3,3]
        
        sphere_positions.append(pos)
        










unique_pose_indices_in_triangulation = set()

for triplet in triangulation_indices:
    for pose_index_value in triplet:
        unique_pose_indices_in_triangulation.add(pose_index_value)

# the positions being used for the affine transformation... (need to have 1-to-1 correspondences)
cubemap_positions = []  # the camera positions as found in VisualSFM coordinate system
sphere_positions_hl = []    # the camera positions as given from our HoloLens coordinate system

                      
for pose_index_value in unique_pose_indices_in_triangulation:
    
    frame_number = GetSphericalCameraFrameIndexFromPoseIndex(pose_index_value) + 1  # 0-indexed to 1-indexed!
    cubemap_camera = cubemap_cameras[frame_number_to_new_cubemap_idx[frame_number]] # i-indexed
    
    cubemap_positions.append(cubemap_camera.GetCameraCenter())
    sphere_positions_hl.append(sphere_positions[pose_index_value])
    
    

cubemap_positions = np.array(cubemap_positions)
sphere_positions_hl = np.array(sphere_positions_hl)


def plot_A_B(A, B):
    minbound = -10
    maxbound = 10
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(A[:,0], A[:,1], A[:,2], c='red')
    ax.scatter(B[:,0], B[:,1], B[:,2], c='green')
    ax.auto_scale_xyz([minbound, maxbound], [minbound, maxbound], [minbound, maxbound])
    plt.show()

# find a scale factor between the two sets of points

A_unscaled = cubemap_positions
B = sphere_positions_hl

pairwise_dists_A_unscaled = []
pairwise_dists_B = []

for i in range(len(A_unscaled)):
    for j in range(i+1, len(A_unscaled)):
        pairwise_dists_A_unscaled.append(np.linalg.norm(A_unscaled[i] - A_unscaled[j]))
        
for i in range(len(B)):
    for j in range(i+1, len(B)):
        pairwise_dists_B.append(np.linalg.norm(B[i] - B[j]))
        
pairwise_scales = []
for i in range(len(pairwise_dists_A_unscaled)):
    pairwise_scales.append(pairwise_dists_B[i] / pairwise_dists_A_unscaled[i])
    
    
median_scale_factor = np.median(pairwise_scales)    # median scale factor to go from A_unscaled to A (where A's scale is close to B's scale)

A_scaled = A_unscaled * median_scale_factor

#plot_A_B(A_scaled, B)

# now do a rigid transform, to solve for [R,t] in the equation B = R*A + t (where A is the "A_scaled")


def rigid_transform_3D(A, B):
    assert len(A) == len(B)

    N = A.shape[0]; # total points

    centroid_A = np.mean(A, axis=0)
    centroid_B = np.mean(B, axis=0)
    
    # centre the points
    AA = A - np.tile(centroid_A, (N, 1))
    BB = B - np.tile(centroid_B, (N, 1))

    # dot is matrix multiplication for array
    H = np.matmul(np.transpose(AA), BB)

    U, S, Vt = np.linalg.svd(H)

    R = np.matmul(Vt.T, U.T)

    # special reflection case
    if np.linalg.det(R) < 0:
       print "Reflection detected"
       Vt[2,:] *= -1
       R = np.matmul(Vt.T, U.T)

    t = - np.dot(R, centroid_A).T + centroid_B.T

    print t

    return R, t

# recover the transformation
ret_R, ret_t = rigid_transform_3D(A_scaled, B)

ret_R_inverse = np.linalg.inv(ret_R)

A_scaled_2 = (np.matmul(ret_R, A_scaled.T)) + np.tile(ret_t, (num_cubemaps, 1)).T
A_scaled_2 = A_scaled_2.T

#plot_A_B(A_scaled_2, B)




# now transform the points as well
scaled_point_xyzs = point_xyzs * median_scale_factor
scaled_transformed_point_xyzs = (np.matmul(ret_R, scaled_point_xyzs.T)) + np.tile(ret_t, (num_points, 1)).T
scaled_transformed_point_xyzs = scaled_transformed_point_xyzs.T





pose_corrected_cubemap_cameras = []
pose_corrected_cubemap_camera_positions = []
for cubemap_cam in cubemap_cameras:
    pose_corrected_cubemap_cam = Camera()
    pose_corrected_cubemap_cam.m = np.matmul(cubemap_cam.m, ret_R_inverse)
    
    uncorrected_camera_center = cubemap_cam.GetCameraCenter()
    corrected_camera_center = np.matmul(ret_R, (median_scale_factor * uncorrected_camera_center)) + ret_t
    
    pose_corrected_cubemap_cam.SetCameraCenterAfterRotation(corrected_camera_center)
    pose_corrected_cubemap_cameras.append(pose_corrected_cubemap_cam)
    
    pose_corrected_cubemap_camera_positions.append(pose_corrected_cubemap_cam.GetCameraCenter())

pose_corrected_cubemap_camera_positions = np.array(pose_corrected_cubemap_camera_positions)


# determine which points were seen by which triplets

frame_number_to_observed_point_indices = {} # remember, these are 1-indexed frame numbers!

for frame_number in frame_number_to_side_idx_to_imported_camera:
    point_indices_observed_by_this_frame_number = set()
    for side_idx in frame_number_to_side_idx_to_imported_camera[frame_number]:
        imported_camera_idx = frame_number_to_side_idx_to_imported_camera[frame_number][side_idx]
        observed_point_indices = imported_cam_idx_to_point_idx[imported_camera_idx]
        point_indices_observed_by_this_frame_number = point_indices_observed_by_this_frame_number.union(observed_point_indices)
    frame_number_to_observed_point_indices[frame_number] = point_indices_observed_by_this_frame_number
    

def GetPointsProjectedOntoTranslatedUnitSphere(world_pts, center_point):
    projected_pts = []
    for pt in world_pts:
        projected_pt = center_point + ((pt - center_point) / np.linalg.norm(pt - center_point))
        projected_pts.append(projected_pt)
    projected_pts = np.array(projected_pts)
    return projected_pts






# export the updated cubemap data
refined_cubemap_data_path = os.path.join(args.base_dir, REFINED_CUBEMAP_DATA_FILENAME)
with open(refined_cubemap_data_path, 'wb') as cubemap_data_file:
    writer = csv.writer(cubemap_data_file, delimiter=' ')
    for frame_number in frame_number_to_new_cubemap_idx:    # 1-indexed
        cubemap_idx = frame_number_to_new_cubemap_idx[frame_number]
        pose_corrected_cubemap_cam = pose_corrected_cubemap_cameras[cubemap_idx]
        cam_center = pose_corrected_cubemap_cam.GetCameraCenter()
        cam_quat = pose_corrected_cubemap_cam.GetQuaternionRotation()
        
        output_line = list([frame_number]) + list(cam_center) + list(cam_quat)
        writer.writerow(output_line)
    

print("========= now that we have adjusted the cube positions, we must generate a new triangulation! ==========")

unrefined_triangulation_indices = triangulation_indices

unrefined_triangulation_frame_numbers = []
for unrefined_triplet in unrefined_triangulation_indices:
    unrefined_triplet_frame_numbers = []
    for pose_index_value in unrefined_triplet:
        frame_number = GetSphericalCameraFrameIndexFromPoseIndex(pose_index_value) + 1  # 0-indexed to 1-indexed!
        unrefined_triplet_frame_numbers.append(frame_number)
    unrefined_triangulation_frame_numbers.append(unrefined_triplet_frame_numbers)


refined_tri_points = []
refined_tri_frame_numbers = []

for frame_number in frame_number_to_new_cubemap_idx:
    cubemap_idx = frame_number_to_new_cubemap_idx[frame_number]
    pose_corrected_cubemap_cam = pose_corrected_cubemap_cameras[cubemap_idx]
    cam_center = pose_corrected_cubemap_cam.GetCameraCenter()
    
    refined_tri_points.append([cam_center[0], cam_center[2]]) # x z
    refined_tri_frame_numbers.append(frame_number)
    
refined_tri_points = np.array(refined_tri_points)

refined_triangulation = Delaunay(refined_tri_points)

# get the refined triangulation in terms of the original frame numbers
refined_tri_triplets_frame_numbers = []
refined_tri_simplices = refined_triangulation.simplices
for triplet in refined_tri_simplices:
    original_frame_numbers_triplet = []
    for triplet_value in triplet:
        original_frame_numbers_triplet.append(refined_tri_frame_numbers[triplet_value])
    refined_tri_triplets_frame_numbers.append(original_frame_numbers_triplet)
    
out_refined_data = {'triangulation_indices': refined_tri_triplets_frame_numbers}

saveFile(os.path.join(args.base_dir, REFINED_TRIANGULATION_FILENAME), out_refined_data)


































print("========= done with generating new refined triangulation, use it to make the triplet meshes ==========")

num_seen_by_at_least_two = []
num_seen_by_at_least_one = []
for triplet_frame_numbers in refined_tri_triplets_frame_numbers:
    
    triplet_frame_numbers_to_export = np.copy(triplet_frame_numbers)
    triplet_frame_numbers_to_export.sort()  # go from lowest frame number to highest frame number, for consistency
           
    frame_number_a = triplet_frame_numbers_to_export[0] # already 1-index
    frame_number_b = triplet_frame_numbers_to_export[1] # already 1-index
    frame_number_c = triplet_frame_numbers_to_export[2] # already 1-index
    print("{} {} {}".format(frame_number_a, frame_number_b, frame_number_c))
    
    
    
    points_seen_by_a = frame_number_to_observed_point_indices[frame_number_a]
    points_seen_by_b = frame_number_to_observed_point_indices[frame_number_b]
    points_seen_by_c = frame_number_to_observed_point_indices[frame_number_c]
    
    points_seen_by_a_and_b = points_seen_by_a.intersection(points_seen_by_b)
    points_seen_by_b_and_c = points_seen_by_b.intersection(points_seen_by_c)
    points_seen_by_c_and_a = points_seen_by_c.intersection(points_seen_by_a)
    
    points_observed_by_all_three = (points_seen_by_a_and_b).intersection(points_seen_by_c)
    points_observed_by_at_least_two = (points_seen_by_a_and_b.union(points_seen_by_b_and_c)).union(points_seen_by_c_and_a)
    points_observed_by_at_least_one = (points_seen_by_a.union(points_seen_by_b)).union(points_seen_by_c)
    
    num_seen_by_at_least_two.append(len(points_observed_by_at_least_two))
    num_seen_by_at_least_one.append(len(points_observed_by_at_least_one))
    
    print("{} points observed by all three".format(len(points_observed_by_all_three)))
    print("{} points observed by at least two".format(len(points_observed_by_at_least_two)))
    print("{} points observed by at least one".format(len(points_observed_by_at_least_one)))
    
    set_of_points_to_use = points_observed_by_at_least_two
    #set_of_points_to_use = points_observed_by_at_least_one
    
    world_correspondence_points_for_this_triplet = []
    
    for point_idx in set_of_points_to_use:
        world_correspondence_points_for_this_triplet.append(scaled_transformed_point_xyzs[point_idx])
        
    world_correspondence_points_for_this_triplet = np.array(world_correspondence_points_for_this_triplet)
    
    # OVERRIDE
    """
    world_correspondence_points_for_this_triplet = []
    
    y_subdiv = 10
    xz_subdiv = 10
    
    min_y = -2.0
    max_y = 3.0
    y_steps = np.linspace(min_y, max_y, y_subdiv)
    
    
    xs = [-2.0, -9.0, -3.0, 4.0]
    zs = [9.0, 2.0, -10.0, -3.0]
    
    for wall_idx in range(4):
        i = wall_idx
        j = (wall_idx+1) % 4
        xs_linspace = np.linspace(xs[i], xs[j], xz_subdiv)
        zs_linspace = np.linspace(zs[i], zs[j], xz_subdiv)
        
        for y_step in y_steps:
            for t in range(xz_subdiv - 1):
                world_correspondence_points_for_this_triplet.append([xs_linspace[t],y_step,zs_linspace[t]])        
    """
    # END OVERRIDE
    
    
    
    # first, find the world-space centroid for this triplet, then project points onto it
    
    cubemap_a = pose_corrected_cubemap_cameras[frame_number_to_new_cubemap_idx[frame_number_a]]
    cubemap_b = pose_corrected_cubemap_cameras[frame_number_to_new_cubemap_idx[frame_number_b]]
    cubemap_c = pose_corrected_cubemap_cameras[frame_number_to_new_cubemap_idx[frame_number_c]]
    
    cubemap_a_center = cubemap_a.GetCameraCenter()
    cubemap_b_center = cubemap_b.GetCameraCenter()
    cubemap_c_center = cubemap_c.GetCameraCenter()
    
    triplet_centroid = (cubemap_a_center + cubemap_b_center + cubemap_c_center) / 3.0
    
    # next, project all the world correspondence points onto a unit sphere located at this triplet centroid
    
    unprojected_pts = world_correspondence_points_for_this_triplet
    
    world_correspondence_points_projected_to_centroid_sphere = GetPointsProjectedOntoTranslatedUnitSphere(unprojected_pts, triplet_centroid)
    
    pts = world_correspondence_points_projected_to_centroid_sphere
    world_centroid_hull = ConvexHull(pts)
    
    output_triplet_mesh_filename = "triplet_{}_{}_{}.meshes".format(frame_number_a, frame_number_b, frame_number_c)
    writeOutputTripletMeshData(args.base_dir, output_triplet_mesh_filename, unprojected_pts, world_centroid_hull.simplices)
        
    
    
    """
    plot_center_point = triplet_centroid
    plot_range = 10
    
    num_points_for_subset = 1000
    subset_points = scaled_transformed_point_xyzs[np.random.randint(scaled_transformed_point_xyzs.shape[0], size=num_points_for_subset), :]
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    
    for simplex in world_centroid_hull.simplices:
        loop_simplex = np.array(list(simplex) + [simplex[0]])
        plt.plot(pts[loop_simplex, 0], pts[loop_simplex, 1], pts[loop_simplex, 2], 'k-')
        plt.plot(unprojected_pts[loop_simplex, 0], unprojected_pts[loop_simplex, 1], unprojected_pts[loop_simplex, 2], 'k-')
    
    ax.scatter(subset_points[:,0], subset_points[:,1], subset_points[:,2], color='orange')
    ax.scatter(pose_corrected_cubemap_camera_positions[:,0], pose_corrected_cubemap_camera_positions[:,1], pose_corrected_cubemap_camera_positions[:,2], color='blue')
    ax.scatter(world_correspondence_points_for_this_triplet[:,0], world_correspondence_points_for_this_triplet[:,1], world_correspondence_points_for_this_triplet[:,2], c='red')
    ax.scatter(world_correspondence_points_projected_to_centroid_sphere[:,0], world_correspondence_points_projected_to_centroid_sphere[:,1], world_correspondence_points_projected_to_centroid_sphere[:,2], c='green')
    ax.auto_scale_xyz([plot_center_point[0] - plot_range, plot_center_point[0] + plot_range], [plot_center_point[1] - plot_range, plot_center_point[1] + plot_range], [plot_center_point[2] - plot_range, plot_center_point[2] + plot_range])
    plt.show()
    """
    
    
    
    
    # QUESTION: should the projected meshes on the cubemap locations be world-locked or camera-locked?
    # do world-locked, it's easier to compute.
    
    