# -*- coding: utf-8 -*-
"""
Created on Wed Oct 04 13:07:48 2017

@author: ANDERSED
"""

import yaml
import argparse
import sys
import os
import subprocess
import glob
from pyrr import quaternion, matrix44, vector3
from pyrr import Quaternion, Matrix33, Matrix44, Vector3
import numpy as np
import csv
from holoscan_common import SCANDATA_FILENAME
import process_scan_data_helpers
import matplotlib.patches as patches

# %matplotlib qt
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import proj3d
import numpy as np
import matplotlib.pyplot as plt

from scipy.spatial import Delaunay


def saveFile(fname,data):
	fd = open(fname,"wb")
	yaml.dump(data,fd)
	fd.close()

def readFile(fname):
	fd = open(fname,"rb")
	data = yaml.load(fd)
	return data


parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory for the specific scan")
args = parser.parse_args()

print("Given input pose CSV (for Holo camera), visualize it so we can see if it's complete")


print("Base scan directory: {}".format(args.base_dir))



SCAN_LABEL = os.path.split(args.base_dir)[-1]

print("SCAN_LABEL: {}".format(SCAN_LABEL))

# =============================================================================

refined_frame_number_triangulation_path = os.path.join(args.base_dir, "sphere_pose_triangulation_refined_frame_numbers.yaml")

if not os.path.isfile(refined_frame_number_triangulation_path):
    raise Exception("Missing refined sphere pose triangulation at {}".format(refined_frame_number_triangulation_path))

refined_triangulation_frame_numbers = readFile(refined_frame_number_triangulation_path)["triangulation_indices"]

num_unique_frame_numbers = len(np.unique(np.array(refined_triangulation_frame_numbers).reshape(-1)))

print("num_unique_frame_numbers: {}".format(num_unique_frame_numbers))