#!/bin/bash
# in bash ubuntu for windows

if [ $# -ne 1 ]; then
    echo $0: usage: 01_setup_scan_data scanlabel
    exit 1
fi

SCAN_LABEL=$1
#   SCAN_LABEL=20170413142827

CUBEMAP_SCRIPT_PATH=/e/Dev/OpenGL/lumiviz/resources/scans/create_cubemaps.py
INIT_YAML_SCRIPT_PATH=/e/Dev/OpenGL/lumiviz/resources/scans/init_scan_datafile.py
BASE_SCAN_DIR=/e/Dev/OpenGL/lumiviz/resources/scans/${SCAN_LABEL}

cd ${BASE_SCAN_DIR}

FRAME_ZIP_FILE=${SCAN_LABEL}_frames.zip

echo "unzipping HoloLens frames..."
# unzip the HoloLens frames (overwriting what was already there)
unzip -o ${FRAME_ZIP_FILE} -d frames

echo "horizontally flipping HoloLens frames..."
# note that the HoloLens frames are horizontally flipped and need to be modified
ffmpeg -i frames/frame%05d.jpg -qscale:v 2 -vf "hflip" frames/frame%05d.jpg

echo "creating thumbnails for HoloLens frames..."
mkdir -p thumbs
ffmpeg -i frames/frame%05d.jpg -qscale:v 2 -vf scale=iw*0.25:ih*0.25 thumbs/thumb%05d.jpg

# We assume the stitched 360 video filename has "Stitch" in it (from ActionDirector)
SPHERICAL_VIDEO_NAME=$(find . -name '*Stitch*')

echo "creating full-size 360 frames..."
mkdir -p pano_frames
ffmpeg -i ${SPHERICAL_VIDEO_NAME} -qscale:v 2 pano_frames/pano_frame%05d.jpg

echo "creating 360 thumbs..."
mkdir -p pano_thumbs
ffmpeg -i pano_frames/pano_frame%05d.jpg -qscale:v 2 -vf scale=iw*0.25:ih*0.25 pano_thumbs/pano_thumb%05d.jpg

echo "creating full-size 360 cubemaps..."
python ${CUBEMAP_SCRIPT_PATH} ./pano_frames/

echo "creating thumbnail 360 cubemaps..."
python ${CUBEMAP_SCRIPT_PATH} ./pano_thumbs/

echo "done extracting frames"

echo "Generating initial scandata YAML file..."
python ${INIT_YAML_SCRIPT_PATH} ${BASE_SCAN_DIR}

echo "Done generating YAML file. USER INPUT REQUIRED: Find the synchronized frames in each recording, edit the YAML file, then run the next script."

: '








'