# -*- coding: utf-8 -*-
"""
Created on Mon Mar 12 11:24:18 2018

@author: ANDERSED
"""

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import numpy as np
import math
import scipy.spatial.distance
from mpl_toolkits.mplot3d import Axes3D

def random_point_sphere(num_points = 1):
    theta = np.random.random(num_points) * math.pi * 2.0
    phi = np.arccos(2.0 * np.random.random(num_points) - 1.0)
    #radius = pow(np.random.random(num_points), 1.0 / 3.0)
    radius = 1.0
    x = np.cos(theta) * np.sin(phi) * radius
    y = np.sin(theta) * np.sin(phi) * radius
    z = np.cos(phi) * radius
    return np.dstack((x,y,z))[0]

# if we only compare it doesn't matter if it's squared
def min_dist_squared(points, point):
    diff = points - np.array([point])
    return np.min(np.einsum('ij,ij->i',diff,diff))

class PoissonGenerator:
    def __init__(self):
        self.disk = True
        self.num_dim = 3
        self.repeatPattern = False
        self.num_perms = (3 ** self.num_dim) if self.repeatPattern else 1

        self.zero_point = [0,0,0]
        self.random_point = random_point_sphere
        
        self.best_dist = np.inf

    def first_point(self):
        return self.random_point(1)[0]

    def find_next_point(self, current_points, iterations_per_point):
        best_dist = 0
        best_point = []
        random_points = self.random_point(iterations_per_point)
        for new_point in random_points:
            dist = min_dist_squared(current_points, new_point)
            if dist > best_dist:
                best_dist = dist
                best_point = new_point
        self.best_dist = best_dist
        return best_point

    def permute_point(self, point):
        out_array = np.array(point,ndmin = 2)
        if not self.repeatPattern:
            return out_array

        if self.num_dim == 3:
            for z in range(-1,2):
                for y in range(-1,2):
                    for x in range(-1,2):
                        if y != 0 or x != 0 or z != 0:
                            perm_point = point+[x,y,z]
                            out_array = np.append(out_array, np.array(perm_point,ndmin = 2), axis = 0 )
        elif self.num_dim == 2:            
            for y in range(-1,2):
                for x in range(-1,2):
                    if y != 0 or x != 0:
                        perm_point = point+[x,y]
                        out_array = np.append(out_array, np.array(perm_point,ndmin = 2), axis = 0 )
        else:
            for x in range(-1,2):
                if x != 0:
                    perm_point = point+[x]
                    out_array = np.append(out_array, np.array(perm_point,ndmin = 2), axis = 0 )

        return out_array

    def find_point_set(self, num_points, num_iter, iterations_per_point, rotations, progress_notification = None):
        best_point_set = []
        best_dist_avg = 0
        self.rotations = 1
        if self.disk and self.num_dim == 2:
            rotations = max(rotations, 1)
            self.rotations = rotations

        for i in range(num_iter):
            if progress_notification != None:
                progress_notification(i / num_iter)
            points = self.permute_point(self.first_point())

            for i in range(num_points-1):
                next_point = self.find_next_point(points, iterations_per_point)
                points = np.append(points, self.permute_point(next_point), axis = 0)

            current_set_dist = 0

            if rotations > 1:
                points_permuted = np.copy(points)
                for rotation in range(1, rotations):
                    rot_angle = rotation * math.pi * 2.0 / rotations
                    s, c = math.sin(rot_angle), math.cos(rot_angle)
                    rot_matrix = np.matrix([[c, -s], [s, c]])
                    points_permuted = np.append(points_permuted, np.array(np.dot(points, rot_matrix)), axis = 0)
                current_set_dist = np.min(scipy.spatial.distance.pdist(points_permuted))
            else:
                current_set_dist = np.min(scipy.spatial.distance.pdist(points))

            if current_set_dist > best_dist_avg:
                best_dist_avg = current_set_dist
                best_point_set = points
        return best_point_set[::self.num_perms,:]

    def cache_sort(self, points, sorting_buckets):
        if sorting_buckets < 1:
            return points
        if self.num_dim == 3:
            points_discretized = np.floor(points * [sorting_buckets,-sorting_buckets, sorting_buckets])
            indices_cache_space = np.array(points_discretized[:,2] * sorting_buckets * 4 + points_discretized[:,1] * sorting_buckets * 2 + points_discretized[:,0])
            points = points[np.argsort(indices_cache_space)]
        elif self.num_dim == 2:
            points_discretized = np.floor(points * [sorting_buckets,-sorting_buckets])
            indices_cache_space = np.array(points_discretized[:,1] * sorting_buckets * 2 + points_discretized[:,0])
            points = points[np.argsort(indices_cache_space)]        
        else:
            points_discretized = np.floor(points * [sorting_buckets])
            indices_cache_space = np.array(points_discretized[:,0])
            points = points[np.argsort(indices_cache_space)]
        return points


num_init_points = 100

uniform_points = np.random.normal(size=(num_init_points, 3)) 
uniform_points /= np.linalg.norm(uniform_points, axis=1, keepdims=True)





generator = PoissonGenerator()

#current_points = foo.first_point()
current_points = uniform_points


best_dists = []

for i in range(500):
    current_points = np.vstack((current_points, generator.find_next_point(current_points, 50)))
    best_dists.append(generator.best_dist)    



current_points_added = current_points[len(uniform_points):]



fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(current_points_added[:,0], current_points_added[:,1], current_points_added[:,2], c='r')
ax.scatter(uniform_points[:,0], uniform_points[:,1], uniform_points[:,2], c='g')



"""


def findClosest(current_points, candidate_points):
    radii = []
    for candidate_point in candidate_points:
        min_radius = np.inf
        
        for current_point in current_points:
            









# generate uniformly random points on sphere

num_points = 100

uniform_points = np.random.normal(size=(num_points, 3)) 
uniform_points /= np.linalg.norm(uniform_points, axis=1, keepdims=True)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(current_points[:,0], current_points[:,1], current_points[:,2])
ax.scatter(uniform_points[:,0], uniform_points[:,1], uniform_points[:,2])



poisson_k = 50




candidate_points = np.random.normal(size=(poisson_k, 3)) 
candidate_points /= np.linalg.norm(candidate_points, axis=1, keepdims=True)

candidate_radii = findClosest(candidate_points)

best = candidate_points[np.argmax(candidate_radii)]
"""