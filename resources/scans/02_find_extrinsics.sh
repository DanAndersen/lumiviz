#!/bin/bash
# in bash ubuntu for windows

if [ $# -ne 1 ]; then
    echo $0: usage: 02_find_extrinsics scanlabel
    exit 1
fi

SCAN_LABEL=$1
#   SCAN_LABEL=20170413142827

GET_CHESSBOARD_CORNERS_SCRIPT_PATH=/e/Dev/OpenGL/lumiviz/resources/scans/get_chessboard_corners.py
BASE_SCAN_DIR=/e/Dev/OpenGL/lumiviz/resources/scans/${SCAN_LABEL}

CHESS_X=12
CHESS_Y=12
CHESS_SIZE_METERS=0.0381

cd ${BASE_SCAN_DIR}

echo "finding chessboard corners for HoloLens frames..."
python ${GET_CHESSBOARD_CORNERS_SCRIPT_PATH} ${SCAN_LABEL} ${CHESS_X} ${CHESS_Y} ${CHESS_SIZE_METERS}
echo "done getting chessboard corners for HoloLens frames."

echo "finding chessboard corners for spherical frames..."
python ${GET_CHESSBOARD_CORNERS_SCRIPT_PATH} ${SCAN_LABEL} ${CHESS_X} ${CHESS_Y} ${CHESS_SIZE_METERS} --spherical
echo "done getting chessboard corners for spherical frames."

echo "Done generating corners. USER INPUT REQUIRED: Find the drawn_chessboard subdirectories and make sure the acquired corners look reasonable before moving to the next step."

: '








'