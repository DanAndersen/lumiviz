# -*- coding: utf-8 -*-
"""
Created on Tue Dec 12 15:08:43 2017

@author: ANDERSED
"""

# This script implements the full pipeline for doing spherical SfM, based off 
# of the work by Pagini and Stricker in "Structure from Motion using full 
# spherical panoramic cameras".

# Inputs:
# - a set of selected panorama camera frames
# - a collection of matches between points on the spherical panorama images
#   which have not yet been refined (e.g. by finding the essential matrix)
# - initial rough estimates of the HoloLens camera for each panorama frame

# Outputs
# - Refined poses for each of the input cameras
# - A collection of points in 3D world space
# - A list of observations (which points were observed by which cameras)

# Pipeline steps
# - Initialization
# --- Choose 2 cameras as initial cameras
# --- Solve epipolar geometry for that pair
# --- Compute essential matrix E using 8-point algorithm and RANSAC (using e_p
#     as the error metric)
# --- Factorize E into R and t, setting a unit length for t so E can be 
#     parameterized with 5 parameters (alpha, beta, gamma, theta, phi)
# --- Optimize the epipolar error (using e_p) with the LevMar optimization
# --- Triangulate the correspondences into 3D positions
# - After initialization
# --- Select the non-calibrated camera with the most matches with the already
#     calibrated cameras.
# --- Get 2D/3D correspondences from matches with already-triangulated points.
#     (may need to bucket the feature matches to get match similarities)
# --- Compute a first estimate of the new camera pose using DLT algorithm
#     (using alpha_t for RANSAC-based outlier rejection)
#     (NOTE: consider using initial HoloLens pose here)
# --- Optimize the new camera pose (using alpha_t) with LevMar
# --- Triangulate all possible points from matches with the calibrated cameras.
# --- Do sparse bundle adjustment to globally minimize reprojection error.
# --- Loop, add the next camera.

# =============================================================================

# Imports

import argparse
import os
import glob
import re
import csv
from holoscan_common import readFile, TRIANGULATION_FILENAME, SCANDATA_FILENAME, POSE_SPHERE_FILENAME, sphereImgPointToXYZUnitSphere, toHomogeneousCoords, rayDistanceToEpipolarPlane, solve_essential_matrix, LineLineIntersect
import cv2
import numpy as np
import scipy
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection

# =============================================================================

def getPanoFramePath(zero_indexed_frame_number):
    return os.path.join(args.base_dir, "pano_frames", "pano_frame%05d.jpg" % (zero_indexed_frame_number + 1,))

def GetSphericalCameraFrameIndexFromPoseIndex(pose_index):
    locatableCameraSyncFrameIndex = scanData["hololens_sync_frame"] - 1 # go from 1-indexed to 0-indexed
    locatableCameraSyncFrameTimestamp = sphere_pose_timestamps[locatableCameraSyncFrameIndex]
    
    sphericalCameraSyncFrameIndex = scanData["spherical_sync_frame"] - 1 # go from 1-indexed to 0-indexed
    sphericalCameraFPS = scanData["spherical_fps"]
    
    locatableCameraTimestamp = sphere_pose_timestamps[pose_index]
    secondsSinceCalibrationTime = (locatableCameraTimestamp - locatableCameraSyncFrameTimestamp)
    sphericalCameraFrameIndex = int(round(sphericalCameraSyncFrameIndex + (secondsSinceCalibrationTime * sphericalCameraFPS)))
    return sphericalCameraFrameIndex

class PanoMatches:
    def __init__(self, frame_number_i, frame_number_j, img_matches, estimated_dist_meters):
        self.frame_number_i = frame_number_i
        self.frame_number_j = frame_number_j
        self.matches = img_matches # matches in image coordinates
        
        self.estimated_dist_meters = estimated_dist_meters # from HoloLens, initial estimated distance in meters
        
        self.match_xyzs = []
        #self.matches_homogeneous = []
        for match in self.matches:
            pt_i = sphereImgPointToXYZUnitSphere(match[0], match[1], pano_frame_resolution)
            pt_j = sphereImgPointToXYZUnitSphere(match[2], match[3], pano_frame_resolution)
            
            match_xyz = [pt_i, pt_j]
            self.match_xyzs.append(match_xyz)
            #match_homogeneous = [toHomogeneousCoords(xyz) for xyz in match_xyz]
            #self.matches_homogeneous.append(match_homogeneous)
            
    # matches_homogeneous: a list of pairs, where each pair is a set of two 3D homogeneous coordinates
    # n: the minimum number of data points needed to fit the model (e.g. 8 for the 8-point algorithm)
    # k: max number of iterations allowed in the algorithm
    # t: threshold value to determine when a data point fits a model
    # d: number of close data points required to assert that a model fits well to data (absolute number, not a percentage)
    def compute_essential_matrix_ransac(self, n, k, t, d):
        print("starting ransac_essential_matrix()")
        num_points_all = len(self.match_xyzs) # how many were input (inliers and outliers)
        
        iterations = 0
        best_fit_essential_matrix = None
        best_err = np.inf
        best_fit_inliers = None
        best_fit_inlier_indices = []
        if num_points_all >= n:
            while iterations < k:
                #print("iteration {}".format(iterations))
                maybe_inliers_indices = np.sort(np.random.choice(num_points_all, n, replace=False))
                maybe_inliers = []
                for idx in range(num_points_all):
                    if idx in maybe_inliers_indices:
                        maybe_inliers.append(self.match_xyzs[idx])
                maybe_model = solve_essential_matrix(maybe_inliers)
                
                also_inliers = []
                also_inlier_indices = []
                for idx in range(num_points_all):
                    if idx not in maybe_inliers_indices:
                        match = self.match_xyzs[idx]
                        x = match[0]
                        xprime = match[1]
                        if rayDistanceToEpipolarPlane(x, xprime, maybe_model) < t:
                            also_inlier_indices.append(idx)
                            also_inliers.append(match)
                            
                #print("size of also_inliers: {}".format(len(also_inliers)))
                if len(also_inliers) > d:
                    #print("found good model")
                    # this implies that we may have found a good model
                    # now test how good it is
                    all_current_inliers = maybe_inliers + also_inliers
                    all_current_inlier_indices = np.concatenate( (maybe_inliers_indices, also_inlier_indices), axis=0 )
                    
                    better_model = solve_essential_matrix(all_current_inliers)
                    
                    errs = []
                    for current_inlier in all_current_inliers:
                        x = current_inlier[0]
                        xprime = current_inlier[1]
                        err = rayDistanceToEpipolarPlane(x, xprime, better_model)
                        errs.append(err)
                    this_err = np.mean(errs)
                    
                    if this_err < best_err:
                        #print("found improved model")
                        best_fit_essential_matrix = better_model
                        best_err = this_err
                        best_fit_inliers = all_current_inliers
                        best_fit_inlier_indices = all_current_inlier_indices
                iterations += 1
        return best_fit_essential_matrix, best_err, best_fit_inlier_indices
    
    def draw_matches(self, inlier_indices_list, window_title):
        img_i = cv2.imread(getPanoFramePath(self.frame_number_i))
        img_j = cv2.imread(getPanoFramePath(self.frame_number_j))
        
        img_i_height = img_i.shape[0]
        
        inlier_color = (0,255,0)
        outlier_color = (0,0,255)
        
        line_thickness = 2
        
        match_img = np.concatenate((img_i, img_j), axis=0)
        for i in range(len(self.matches)):
            match = self.matches[i]
            pt1 = (int(match[0]), int(match[1]))
            pt2 = (int(match[2]), int(match[3] + img_i_height))
            cv2.line(match_img, pt1, pt2, inlier_color if i in inlier_indices_list else outlier_color, line_thickness)
        
        cv2.namedWindow(window_title, cv2.WINDOW_NORMAL)
        cv2.resizeWindow(window_title,1024,1024)
        cv2.imshow(window_title, match_img)


# =============================================================================

parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)

# Define directories
pano_frame_dir = os.path.join(args.base_dir, "pano_frames")

# =============================================================================

# Determine size of panoramas.
pano_frame_resolution = cv2.imread(glob.glob(os.path.join(pano_frame_dir, "pano_frame*.jpg"))[0]).shape[::-1][1:]

# =============================================================================

# Load in HL-estimated poses.

scanData = readFile(os.path.join(args.base_dir, SCANDATA_FILENAME))


sphere_pose_timestamps = []
sphere_pose_matrices = []
sphere_pose_camera_centers = []

with open(os.path.join(args.base_dir, POSE_SPHERE_FILENAME), 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        timestamp = float(row[0])
        
        sphere_pose_timestamps.append(timestamp)
        
        sphere_pose_transformation_matrix = np.array([float(x) for x in row[1:]]).reshape(4,4)
        sphere_pose_matrices.append(sphere_pose_transformation_matrix)
        sphere_pose_camera_centers.append(sphere_pose_transformation_matrix[:3,3])

# =============================================================================

# Load in original data about path triangulation.

triangulation_data = readFile(os.path.join(args.base_dir, TRIANGULATION_FILENAME))

# this is the initial triangulation from the estimated HL poses. The values are pose indices, NOT sphere camera frame numbers
triangulation_indices = triangulation_data["triangulation_indices"]



pose_indices_to_frame_numbers = {}
frame_numbers_to_pose_indices = {}

# generate a representation of the triangulation where we are working with sphere frame indices rather than pose indices.
triangulation_sphere_frame_numbers = []
for pose_index_triangle in triangulation_indices:
    sphere_frame_number_triplet = []
    for pose_index_triangle_val in pose_index_triangle:
        frame_number = GetSphericalCameraFrameIndexFromPoseIndex(pose_index_triangle_val)
        sphere_frame_number_triplet.append(frame_number)
        
        pose_indices_to_frame_numbers[pose_index_triangle_val] = frame_number
        frame_numbers_to_pose_indices[frame_number] = pose_index_triangle_val
        
    triangulation_sphere_frame_numbers.append(sphere_frame_number_triplet)

# =============================================================================

# Load in all SPHORB matches.

input_sphorb_match_paths = glob.glob(os.path.join(pano_frame_dir, "sphorb_matches", "sphorb_match_[0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9].csv"))

all_pano_matches = []

for input_sphorb_match_path in input_sphorb_match_paths:
    csv_filename = os.path.split(input_sphorb_match_path)[-1]
    print("reading in {}...".format(csv_filename))
    zero_indexed_frame_numbers = [int(x) - 1 for x in re.findall(r'\d+', csv_filename)] # go from 1-indexed to 0-indexed
    frame_numbers_ij = zero_indexed_frame_numbers
    
    pose_indices_ij = [frame_numbers_to_pose_indices[fn] for fn in frame_numbers_ij]
    estimated_camera_centers = [sphere_pose_camera_centers[pidx] for pidx in pose_indices_ij]
    
    estimated_dist_meters = np.linalg.norm(estimated_camera_centers[1] - estimated_camera_centers[0])

    matches = []
    
    with open(input_sphorb_match_path, 'rb') as f:
        reader = csv.reader(f, delimiter=',')
        for row in reader:
            match = [float(x) for x in row]
            matches.append(match)
    
    p = PanoMatches(frame_numbers_ij[0], frame_numbers_ij[1], matches, estimated_dist_meters)
    all_pano_matches.append(p)
    
print("loaded in {} sets of matches".format(len(all_pano_matches)))


frame_to_frame_to_panomatch = {}
# Make a map: (frame_number --> other_frame_number --> panomatch)
# duplicate data so we don't have to switch
for p in all_pano_matches:
    f_i = p.frame_number_i
    f_j = p.frame_number_j
    if f_i not in frame_to_frame_to_panomatch:
        frame_to_frame_to_panomatch[f_i] = {}
    if f_j not in frame_to_frame_to_panomatch:
        frame_to_frame_to_panomatch[f_j] = {}
    frame_to_frame_to_panomatch[f_i][f_j] = p
    frame_to_frame_to_panomatch[f_j][f_i] = p




# =============================================================================

# Select which match-set we will use to initialize the reconstruction

num_matches_for_each_pair = []
for p in all_pano_matches:
    num_matches_for_each_pair.append(len(p.matches))

# Choose a match-set where there are both a lot of matches and the estimated distance is as far away as possible.
# This will help avoid issues of a small baseline.

min_matches_needed = np.median(num_matches_for_each_pair)

initial_pair = None

for p in all_pano_matches:
    if len(p.matches) >= min_matches_needed:
        if initial_pair is None or p.estimated_dist_meters > initial_pair.estimated_dist_meters:
            initial_pair = p

num_matches_initial = len(initial_pair.matches)
print("chose initial pair, frame_index_i = {}, frame_index_j = {}, with {} initial matches (including outliers)".format(initial_pair.frame_number_i, initial_pair.frame_number_j, num_matches_initial))

# =============================================================================

# Compute essential matrix E using eight-point algorithm and RANSAC (with e_p error)

# Input parameters
ransac_params = {
    'MIN_MATCHES_NEEDED': 8,     
    'NUM_ITERATIONS': 500,
    'THRESHOLD_VAL': 0.25,
    'PERCENT_INLIERS_NEEDED': 0.5
}

print("finding essential matrix with RANSAC...")
best_fit_essential_matrix, best_err, best_fit_inlier_indices = initial_pair.compute_essential_matrix_ransac(ransac_params["MIN_MATCHES_NEEDED"], ransac_params["NUM_ITERATIONS"], ransac_params["THRESHOLD_VAL"], ransac_params["PERCENT_INLIERS_NEEDED"]*num_matches_initial)

print("found initial essential matrix, with mean err = {} and {}/{} inliers".format(best_err, len(best_fit_inlier_indices), num_matches_initial))

# =============================================================================

# Visualize the inliers and outliers on the original images, to determine if the error threshold for RANSAC is reasonable.

initial_pair.draw_matches(best_fit_inlier_indices, "matches_after_ransac")



# =============================================================================

# Factorize the estimated essential matrix E into rotation matrix R and translation t

E = best_fit_essential_matrix

U, Sigma, VT = np.linalg.svd(E)

diag_110 = np.array([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 0.0]])
new_E = np.matmul(np.matmul(U, diag_110), VT)
U, Sigma, VT = np.linalg.svd(new_E)

W = np.array([[0.0, -1.0, 0.0], [1.0, 0.0, 0.0], [0.0, 0.0, 1.0]])
WT = W.T

# Two possible options for t: t1, t2
t1 = U[:,2]  # norm = 1
t2 = -t1

# Two possible options for R: R1, R2
R1 = np.matmul(U, np.matmul(WT, VT))
R2 = np.matmul(U, np.matmul(W, VT))

# We define the camera coordinate system of camera i (frame_number_i) as being the "world" coordinate system
    
def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)

def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))                               
                                                   
def vector_to_cross_product_matrix(v):
    return np.array([
        [0.0, -v[2], v[1]],
        [v[2], 0.0, -v[0]],
        [-v[1], v[0], 0.0]
    ])


chosen_R = None
chosen_t = None
best_num_good_points = 0

for candidate_R in [R1, R2]:
    
    candidate_R_inverse = np.linalg.inv(candidate_R)
    
    for candidate_t in [t1, t2]:
        print("===")
        print("candidate_R: {}".format(candidate_R))
        print("candidate_t: {}".format(candidate_t))
        
        num_good_points = 0
        
        for i in range(len(initial_pair.match_xyzs)):
            if i in best_fit_inlier_indices:
                match_xyz = initial_pair.match_xyzs[i]
        
                # find the ray intersection in camera i's coord system by determining 4 points
                # ci_i = the position of camera i
                # mi_i = the position of the observed point on camera i's unit sphere
                # cj_i = the position of camera j (in camera i's coord system)
                # mj_i = the position of the observed point on camera j's unit sphere (in camera i's coord system)
                # the estimated position of observed point P (in camera i's coord system) will be the intersection of two rays defined as
                # [ci_i, mi_i] and [cj_i, mj_i]
                
                ci_i = np.zeros(3)
                cj_j = np.zeros(3)
                
                mi_i = match_xyz[0]
                mj_j = match_xyz[1]
                
                #mj_i = np.dot(selected_R_inverse, mj_j) + selected_t
                #cj_i = np.dot(selected_R_inverse, cj_j) + selected_t
                
                mj_i = np.dot(candidate_R_inverse, (mj_j - candidate_t))
                cj_i = np.dot(candidate_R_inverse, (cj_j - candidate_t))
                          
                (intersect_res, pa, pb) = LineLineIntersect(ci_i, mi_i, cj_i, mj_i)
                
                P_i = (pa + pb) / 2.0
                      
                P_j = np.dot(candidate_R, P_i) + candidate_t
                
                # to determine if the selected R and t are valid, P_i must be in the same direction as mi_i (relative to i's coord system)
                # and P_j must be in the same direction as mj_j (relative to j's coord system)
                
                if np.dot(mi_i, unit_vector(P_i)) > 0 and np.dot(mj_j, unit_vector(P_j)) > 0:
                    # the point is on the correct side!
                    num_good_points += 1

        #print("num_good_points: {}".format(num_good_points))
        # the "correct" R and t should be the one with every inlier point being properly in the right direction.
        # we just check for the [R|t] that yields the most correct inlier points.                    
        print("num_good_points: {}".format(num_good_points))
        if num_good_points > best_num_good_points:
            best_num_good_points = num_good_points
            chosen_R = candidate_R
            chosen_t = candidate_t
        
print("===")
print("found valid [R|t]:")
print(chosen_R)
print(chosen_t)
print("===")

# =============================================================================

# Convert [R | t] to 5 parameters
# R rotation matrix --> Rodrigues vector of size 3
# t vector --> theta and phi, spherical coordinates because we have set ||t|| = 1
# Note on spherical coordinates...
# We set theta as inclination (from zenith) and phi as azimuth
# y is up
# theta = arccos(y/r), phi = atan2(x,z)
# x = r * np.sin(theta) * np.sin(phi)
# y = r * np.cos(theta)
# z = r * np.sin(theta) * np.cos(phi)
def R_t_to_params(in_R, in_t):
    radius = 1.0

    rot_rodrigues, _ = cv2.Rodrigues(in_R)
    theta = np.arccos(in_t[1] / radius)
    phi = np.arctan2(in_t[0], in_t[2])
    
    # parameters are stored in the order [rx, ry, rz, theta, phi]
    out_params = np.array([rot_rodrigues[0][0], rot_rodrigues[1][0], rot_rodrigues[2][0], theta, phi])
    return out_params

def params_to_R_t(in_params):
    radius = 1.0
    
    rx = in_params[0]
    ry = in_params[1]
    rz = in_params[2]
    theta = in_params[3]
    phi = in_params[4]
    
    R_from_params, _ = cv2.Rodrigues(np.array([[rx],[ry],[rz]]))
    
    t_from_params = np.array([
        radius * np.sin(theta) * np.sin(phi), 
        radius * np.cos(theta),
        radius * np.sin(theta) * np.cos(phi)
    ])
    
    return R_from_params, t_from_params

# parameters are stored in the order [rx, ry, rz, theta, phi]
unrefined_params = R_t_to_params(chosen_R, chosen_t)

inlier_match_xyzs = []
for i in range(len(initial_pair.match_xyzs)):
    if i in best_fit_inlier_indices:
        inlier_match_xyzs.append(initial_pair.match_xyzs[i])


# Error function from Pagini paper -- uses rayDistanceToEpipolarPlane
def err_func(params):
    R_from_params, t_from_params = params_to_R_t(params)
    
    t_cross = vector_to_cross_product_matrix(t_from_params)
    
    E_from_params = np.matmul(t_cross, R_from_params)
    
    errs = []
    for current_inlier in inlier_match_xyzs:
        x = current_inlier[0]
        xprime = current_inlier[1]
        err = rayDistanceToEpipolarPlane(x, xprime, E_from_params)
        errs.append(err)

    return errs

res_lm = scipy.optimize.least_squares(err_func, unrefined_params, verbose=2, method='lm', ftol=1e-8)

refined_params = res_lm.x

refined_R, refined_t = params_to_R_t(refined_params)
refined_E = np.matmul( vector_to_cross_product_matrix(refined_t), refined_R )

# =============================================================================

# Test the refined_R and refined_t to see if they are legitimate solutions.

refined_R_inverse = np.linalg.inv(refined_R)

num_good_points = 0

# Some of the points, even if they were inliers from RANSAC, will still end up being BEHIND one of the cameras.
# This can happens when the matches are slightly off but are near the epipole (so the two rays are nearly parallel and don't intersect on the right side).
# Here we throw those points out.
refined_inlier_match_indices = []

# We need to keep track of which point is associated with which match. If no 
# point is generated (due to bad intersection, or being an outlier), we leave
# it as None.
# Points are in the coordinate system of initial pair's "frame_number_i".
initial_pair_triangulated_points = [None] * len(initial_pair.match_xyzs)

ci_i = np.zeros(3)
cj_j = np.zeros(3)
cj_i = np.dot(refined_R_inverse, (cj_j - refined_t))

for i in range(len(initial_pair.match_xyzs)):
    match_xyz = initial_pair.match_xyzs[i]
    
    mi_i = match_xyz[0]
    mj_j = match_xyz[1]
    
    if rayDistanceToEpipolarPlane(mi_i, mj_j, refined_E) < ransac_params["THRESHOLD_VAL"]:
        # find the ray intersection in camera i's coord system by determining 4 points
        # ci_i = the position of camera i
        # mi_i = the position of the observed point on camera i's unit sphere
        # cj_i = the position of camera j (in camera i's coord system)
        # mj_i = the position of the observed point on camera j's unit sphere (in camera i's coord system)
        # the estimated position of observed point P (in camera i's coord system) will be the intersection of two rays defined as
        # [ci_i, mi_i] and [cj_i, mj_i]
        
        mj_i = np.dot(refined_R_inverse, (mj_j - refined_t))
    
        #print("{}".format(angle_between(mi_i-ci_i, mj_i - cj_i)))
    
        (intersect_res, pa, pb) = LineLineIntersect(ci_i, mi_i, cj_i, mj_i)
        
        P_i = (pa + pb) / 2.0
              
        P_j = np.dot(refined_R, P_i) + refined_t
        
        #angle_i = angle_between(P_i, mi_i)
        #angle_j = angle_between(P_j, mj_j)
        #print("   {} {}".format(angle_i, angle_j))
        
        # to determine if the selected R and t are valid, P_i must be in the same direction as mi_i (relative to i's coord system)
        # and P_j must be in the same direction as mj_j (relative to j's coord system)
        
        if np.dot(mi_i, unit_vector(P_i)) > 0 and np.dot(mj_j, unit_vector(P_j)) > 0:
            # the point is on the correct side!
            num_good_points += 1
            refined_inlier_match_indices.append(i)
            initial_pair_triangulated_points[i] = P_i


initial_pair.draw_matches(refined_inlier_match_indices, "matches_after_good_point_check")

# =============================================================================

# Visualize the two initial cameras, and their points, in 3D.

valid_triangulated_points = np.array([x for x in initial_pair_triangulated_points if x is not None])


fig = plt.figure(1)
ax = fig.add_subplot(111, projection='3d')

# Draw points
ax.scatter(valid_triangulated_points[:,0], valid_triangulated_points[:,1], valid_triangulated_points[:,2], c='g', marker='o')

# Draw camera i
ax.scatter(ci_i[0], ci_i[1], ci_i[2], c='r', marker='^')

# Draw camera j
ax.scatter(cj_i[0], cj_i[1], cj_i[2], c='b', marker='^')

plot_center_point = np.array([0.0, 0.0, 0.0])
plot_range = 10.0

ax.auto_scale_xyz([plot_center_point[0] - plot_range, plot_center_point[0] + plot_range], [plot_center_point[1] - plot_range, plot_center_point[1] + plot_range], [plot_center_point[2] - plot_range, plot_center_point[2] + plot_range])
plt.show()

# =============================================================================

# We have now finished the initialization step by establishing 3D points for
# the initial pair. Now, we must iteratively add each missing panorama to the
# set of processed panoramas.

# First, choose the next unadded camera to add. We will choose the unadded 
# camera that contains the maximum number of matches with 

added_pano_matches = [initial_pair]
unadded_pano_matches = [p for p in all_pano_matches if p not in added_pano_matches]

added_cameras = set()
added_cameras.add(initial_pair.frame_number_i)
added_cameras.add(initial_pair.frame_number_j)

unadded_cameras = set()
for p in unadded_pano_matches:
    if p.frame_number_i not in added_cameras:
        unadded_cameras.add(p.frame_number_i)
    if p.frame_number_j not in added_cameras:
        unadded_cameras.add(p.frame_number_j)
        
candidate_unadded_cameras = set()
for p in unadded_pano_matches:
    if p.frame_number_i in unadded_cameras and p.frame_number_j in added_cameras:
        candidate_unadded_cameras.add(p.frame_number_i)
    if p.frame_number_i in added_cameras and p.frame_number_j in unadded_cameras:
        candidate_unadded_cameras.add(p.frame_number_j)


candidate_unadded_cameras_to_num_matches_with_added_cameras = {}
for c_unadded in candidate_unadded_cameras:
    candidate_unadded_cameras_to_num_matches_with_added_cameras[c_unadded] = 0
    for c_added in added_cameras:
        if c_added in frame_to_frame_to_panomatch[c_unadded]:
            pm = frame_to_frame_to_panomatch[c_unadded][c_added]
            candidate_unadded_cameras_to_num_matches_with_added_cameras[c_unadded] += len(pm.matches)

selected_next_cam = -1
selected_next_cam_num_matches_with_added_cams = -1
for c in candidate_unadded_cameras_to_num_matches_with_added_cameras:
    if candidate_unadded_cameras_to_num_matches_with_added_cameras[c] > selected_next_cam_num_matches_with_added_cams:
        selected_next_cam = c
        selected_next_cam_num_matches_with_added_cams = candidate_unadded_cameras_to_num_matches_with_added_cameras[c]
        
print("The next camera to be added will be frame number {}, because it has a total of {} matches with already-added cameras".format(selected_next_cam, selected_next_cam_num_matches_with_added_cams))

