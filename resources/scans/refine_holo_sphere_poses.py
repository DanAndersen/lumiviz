# -*- coding: utf-8 -*-
"""
Created on Fri Dec 15 14:04:53 2017

@author: ANDERSED
"""

# Rather than doing a full SfM reconstruction from image features only, this
# script takes the initial estimates of sphere poses from the HoloLens path,
# and wiggles them into place such that we minimize the reprojection error of
# those features.

# =============================================================================

import os
os.chdir("E:\\Dev\\OpenGL\\lumiviz\\resources\\scans")

# Imports
import numpy as np
import cv2
import argparse
import os
from holoscan_common import POSE_SPHERE_FILENAME, readFile, saveFile, TRIANGULATION_FILENAME, SCANDATA_FILENAME, solve_essential_matrix, sphereImgPointToXYZUnitSphereMultiple, rayDistanceToEpipolarPlane, LineLineIntersect, xyzUnitSphereToSphereImgPoint
import csv
import scipy
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import glob
import re
import profile

from scipy.sparse import lil_matrix

import time
from scipy.optimize import least_squares

import multiprocessing

# =============================================================================

def getPanoFramePath(zero_indexed_frame_number):
    return os.path.join(args.base_dir, "pano_frames", "pano_frame%05d.jpg" % (zero_indexed_frame_number + 1,))

class PanoMatches:
    def __init__(self, frame_number_i, frame_number_j, img_matches, estimated_dist_meters):
        self.frame_number_i = frame_number_i
        self.frame_number_j = frame_number_j
        self.matches = img_matches # matches in image coordinates
        
        self.estimated_dist_meters = estimated_dist_meters # from HoloLens, initial estimated distance in meters
        
        num_matches = len(self.matches)
        
        match_img_pts_i = np.array([[match[0], match[1]] for match in self.matches])
        match_img_pts_j = np.array([[match[2], match[3]] for match in self.matches])
        
        match_xyz_pts_i = sphereImgPointToXYZUnitSphereMultiple(match_img_pts_i[:,0], match_img_pts_i[:,1], pano_frame_resolution)
        match_xyz_pts_j = sphereImgPointToXYZUnitSphereMultiple(match_img_pts_j[:,0], match_img_pts_j[:,1], pano_frame_resolution)
        
        self.match_xyzs = []
        for idx in range(num_matches):
            self.match_xyzs.append([match_xyz_pts_i[idx], match_xyz_pts_j[idx]])
  
    def draw_matches(self, inlier_indices_list, window_title):
        img_i = cv2.imread(getPanoFramePath(self.frame_number_i))
        img_j = cv2.imread(getPanoFramePath(self.frame_number_j))
        
        img_i_height = img_i.shape[0]
        
        inlier_color = (0,255,0)
        outlier_color = (0,0,255)
        
        line_thickness = 2
        
        match_img = np.concatenate((img_i, img_j), axis=0)
        for i in range(len(self.matches)):
            match = self.matches[i]
            pt1 = (int(match[0]), int(match[1]))
            pt2 = (int(match[2]), int(match[3] + img_i_height))
            cv2.line(match_img, pt1, pt2, inlier_color if i in inlier_indices_list else outlier_color, line_thickness)
        
        cv2.namedWindow(window_title, cv2.WINDOW_NORMAL)
        cv2.resizeWindow(window_title,1024,1024)
        cv2.imshow(window_title, match_img)


def GetSphericalCameraFrameIndexFromPoseIndex(pose_index):
    locatableCameraSyncFrameIndex = scanData["hololens_sync_frame"] - 1 # go from 1-indexed to 0-indexed
    locatableCameraSyncFrameTimestamp = sphere_pose_timestamps[locatableCameraSyncFrameIndex]
    
    sphericalCameraSyncFrameIndex = scanData["spherical_sync_frame"] - 1 # go from 1-indexed to 0-indexed
    sphericalCameraFPS = scanData["spherical_fps"]
    
    locatableCameraTimestamp = sphere_pose_timestamps[pose_index]
    secondsSinceCalibrationTime = (locatableCameraTimestamp - locatableCameraSyncFrameTimestamp)
    sphericalCameraFrameIndex = int(round(sphericalCameraSyncFrameIndex + (secondsSinceCalibrationTime * sphericalCameraFPS)))
    return sphericalCameraFrameIndex

# =============================================================================

parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)

scanData = readFile(os.path.join(args.base_dir, SCANDATA_FILENAME))

pano_frame_dir = os.path.join(args.base_dir, "pano_frames")

# Determine size of panoramas.
pano_frame_resolution = cv2.imread(glob.glob(os.path.join(pano_frame_dir, "pano_frame*.jpg"))[0]).shape[::-1][1:]

# =============================================================================

# Load in the sphere_pose data (estimated pose for every pose index, from HL) 

sphere_pose_timestamps = []
sphere_pose_matrices = []
sphere_pose_camera_centers = []

with open(os.path.join(args.base_dir, POSE_SPHERE_FILENAME), 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    
    for row in reader:
        timestamp = float(row[0])
        sphere_pose_timestamps.append(timestamp)        
        sphere_pose_transformation_matrix = np.array([float(x) for x in row[1:]]).reshape(4,4)
        sphere_pose_matrices.append(sphere_pose_transformation_matrix)
        sphere_pose_camera_centers.append(sphere_pose_transformation_matrix[:3,3])
        
sphere_pose_camera_centers = np.array(sphere_pose_camera_centers)

# Map between frame numbers and pose indices


pose_indices_to_frame_numbers = {}
frame_numbers_to_pose_indices = {}

with open(os.path.join(args.base_dir, POSE_SPHERE_FILENAME), 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    
    pidx = 0
    for row in reader:
        fn = GetSphericalCameraFrameIndexFromPoseIndex(pidx)
        pose_indices_to_frame_numbers[pidx] = fn
        frame_numbers_to_pose_indices[fn] = pidx
        pidx += 1
        
# =============================================================================

# Load in from the set of matches we generated. Any frame number mentioned in that list should be part of our refinement.

print("Determining which matches will be used in refinement...")

unique_frame_numbers = []
all_matches = []

match_list_csv = os.path.join(args.base_dir, "refinement_frame_number_matches.csv")
with open(match_list_csv, 'rb') as csv_file:
    reader = csv.reader(csv_file, delimiter=',')
    
    for row in reader:
        sorted_match = sorted([int(x) for x in row])
        if sorted_match[0] not in unique_frame_numbers:
            unique_frame_numbers.append(sorted_match[0])
        if sorted_match[1] not in unique_frame_numbers:
            unique_frame_numbers.append(sorted_match[1])
        all_matches.append(sorted_match)

print("Will be refining a total of {} frames based on {} matches".format(len(unique_frame_numbers), len(all_matches)))

# =============================================================================

# Plot the camera centers.
"""
fig = plt.figure(1)
ax = fig.add_subplot(111, projection='3d')

# Draw camera centers
ax.plot(sphere_pose_camera_centers[:,0], sphere_pose_camera_centers[:,1], sphere_pose_camera_centers[:,2], c='g')

plot_center_point = np.array([0.0, 0.0, 0.0])
plot_range = 10.0

ax.auto_scale_xyz([plot_center_point[0] - plot_range, plot_center_point[0] + plot_range], [plot_center_point[1] - plot_range, plot_center_point[1] + plot_range], [plot_center_point[2] - plot_range, plot_center_point[2] + plot_range])
plt.show()
"""

# The "sphere_pose_matrices" [R | t] represent the following transformation...
# R x_c + t = x_w
# where x_c is in the coordinate system of the camera, and x_w is in the world coordinate system

# =============================================================================

# Load in the detected SPHORB matches as well as the estimated essential matrices.
# We don't directly use those essential matrices but they help to remove outliers.

print("Loading in SPHORB matches (with outliers)...")

input_sphorb_match_paths = glob.glob(os.path.join(pano_frame_dir, "sphorb_matches", "sphorb_match_[0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9].csv"))

all_pano_matches = []

print("Loading in PanoMatches...")
start_time = time.time()

for input_sphorb_match_path in input_sphorb_match_paths:
    csv_filename = os.path.split(input_sphorb_match_path)[-1]
    #print("reading in {}...".format(csv_filename))
    zero_indexed_frame_numbers = [int(x) - 1 for x in re.findall(r'\d+', csv_filename)] # go from 1-indexed to 0-indexed
    frame_numbers_ij = zero_indexed_frame_numbers
    
    pose_indices_ij = [frame_numbers_to_pose_indices[_fn] for _fn in frame_numbers_ij]
    estimated_camera_centers = [sphere_pose_camera_centers[_pidx] for _pidx in pose_indices_ij]
    
    estimated_dist_meters = np.linalg.norm(estimated_camera_centers[1] - estimated_camera_centers[0])

    matches = []
    
    with open(input_sphorb_match_path, 'rb') as f:
        reader = csv.reader(f, delimiter=',')
        for row in reader:
            match = [float(x) for x in row]
            matches.append(match)
    
    p = PanoMatches(frame_numbers_ij[0], frame_numbers_ij[1], matches, estimated_dist_meters)
    all_pano_matches.append(p)
elapsed_time = time.time() - start_time
print("loaded in all PanoMatches in {} sec".format(elapsed_time))

frame_to_frame_to_panomatch = {}
# Make a map: (frame_number --> other_frame_number --> panomatch)
# duplicate data so we don't have to switch
for p in all_pano_matches:
    f_i = p.frame_number_i
    f_j = p.frame_number_j
   
    if f_i not in frame_to_frame_to_panomatch:
        frame_to_frame_to_panomatch[f_i] = {}
    if f_j not in frame_to_frame_to_panomatch:
        frame_to_frame_to_panomatch[f_j] = {}
    
    if f_j not in frame_to_frame_to_panomatch[f_i] and f_i not in frame_to_frame_to_panomatch[f_j]:
        # skip any extra matches we might end up having (fn_i->fn_j) and (fn_j->fn_i) where we might end up having inconsistency
        frame_to_frame_to_panomatch[f_i][f_j] = p
        frame_to_frame_to_panomatch[f_j][f_i] = p

print("loaded in {} sets of matches".format(len(all_pano_matches)))


print("loading in data about essential matrices and inliers...")
start_time = time.time()
input_essential_paths = glob.glob(os.path.join(pano_frame_dir, "sphorb_matches", "sphorb_match_[0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9]_essential.yaml"))

all_essential_data = []
frame_to_frame_to_essential_data = {}

for input_essential_path in input_essential_paths:
    essential_filename = os.path.split(input_essential_path)[-1]
    #print("loading {}".format(essential_filename))
    essential_data = readFile(input_essential_path)
    
    all_essential_data.append(essential_data)
    
    zero_indexed_frame_numbers = [int(x) - 1 for x in re.findall(r'\d+', essential_filename)] # go from 1-indexed to 0-indexed
    frame_numbers_ij = zero_indexed_frame_numbers
    
    f_i = frame_numbers_ij[0]
    f_j = frame_numbers_ij[1]
    
    if f_i not in frame_to_frame_to_essential_data:
        frame_to_frame_to_essential_data[f_i] = {}
    if f_j not in frame_to_frame_to_essential_data:
        frame_to_frame_to_essential_data[f_j] = {}
    
    if f_j not in frame_to_frame_to_essential_data[f_i] and f_i not in frame_to_frame_to_essential_data[f_j]:
        # skip any extra matches we might end up having (fn_i->fn_j) and (fn_j->fn_i) where we might end up having inconsistency
        frame_to_frame_to_essential_data[f_i][f_j] = essential_data
        frame_to_frame_to_essential_data[f_j][f_i] = essential_data
                     
elapsed_time = time.time() - start_time
print("loaded in all essential data for each match in {} sec".format(elapsed_time))

frame_to_frame_to_panomatch = {}
# Make a map: (frame_number --> other_frame_number --> panomatch)
# duplicate data so we don't have to switch
for p in all_pano_matches:
    f_i = p.frame_number_i
    f_j = p.frame_number_j
    if f_i not in frame_to_frame_to_panomatch:
        frame_to_frame_to_panomatch[f_i] = {}
    if f_j not in frame_to_frame_to_panomatch:
        frame_to_frame_to_panomatch[f_j] = {}
    frame_to_frame_to_panomatch[f_i][f_j] = p
    frame_to_frame_to_panomatch[f_j][f_i] = p
                               
print("done loading in SPHORB matches, {} panomatches found".format(len(all_pano_matches)))

# =============================================================================

def vector_to_cross_product_matrix(v):
    return np.array([
        [0.0, -v[2], v[1]],
        [v[2], 0.0, -v[0]],
        [-v[1], v[0], 0.0]
    ])

# =============================================================================

unique_frame_numbers_in_refinement = np.sort(np.unique(np.array(unique_frame_numbers).reshape(-1)))

num_cameras = len(unique_frame_numbers_in_refinement)

frame_numbers_to_param_base_index = {}
for i in range(num_cameras):
    fn = unique_frame_numbers_in_refinement[i]
    frame_numbers_to_param_base_index[fn] = i


num_inlier_matches = 0
for p in all_pano_matches:
    fn_i = p.frame_number_i
    fn_j = p.frame_number_j
    essential_data = frame_to_frame_to_essential_data[fn_i][fn_j]
    num_inliers = len(essential_data["best_fit_inlier_indices"])
    num_inlier_matches += num_inliers

cam_trans_vectors = np.zeros(num_cameras * 3)


num_rot_params = 3
num_trans_params = 3

num_params_per_camera = num_rot_params + num_trans_params # 3 rotation, 3 translation

input_params = np.zeros(num_cameras * num_params_per_camera)
for camidx in range(num_cameras):
    cam_fn = unique_frame_numbers_in_refinement[camidx]
    cam_pidx = frame_numbers_to_pose_indices[cam_fn]
    
    cam_mat = sphere_pose_matrices[cam_pidx]
    
    cam_rot_mat = cam_mat[:3,:3]
    cam_rot_vector, _ = cv2.Rodrigues(cam_rot_mat)
    cam_rot_vector = cam_rot_vector.reshape(-1)
    cam_trans_vector = sphere_pose_camera_centers[cam_pidx]
    
    input_params[(camidx * num_params_per_camera) : (camidx * num_params_per_camera + 3)] = cam_rot_vector
           
    cam_trans_vectors[(camidx * num_trans_params) : (camidx * num_trans_params + 3)] = cam_trans_vector
    
    input_params[(camidx * num_params_per_camera + 3) : (camidx * num_params_per_camera + 6)] = cam_trans_vector



num_pano_matches = len(all_pano_matches)



panomatchidx_to_camidxs = np.zeros((num_pano_matches, 2), dtype=np.int)
for panomatchidx in range(num_pano_matches):
    p = all_pano_matches[panomatchidx]
    fn_i = p.frame_number_i
    fn_j = p.frame_number_j
    
    camidx_i = frame_numbers_to_param_base_index[fn_i]
    camidx_j = frame_numbers_to_param_base_index[fn_j]
    
    panomatchidx_to_camidxs[panomatchidx, 0] = camidx_i
    panomatchidx_to_camidxs[panomatchidx, 1] = camidx_j



                        

panomatchidx_to_match_xyzs_i_inliers = []
panomatchidx_to_match_xyzs_j_inliers = []

for panomatchidx in range(num_pano_matches):
    p = all_pano_matches[panomatchidx]
    fn_i = p.frame_number_i
    fn_j = p.frame_number_j

    essential_data = frame_to_frame_to_essential_data[fn_i][fn_j]
    
    inlier_indices = essential_data["best_fit_inlier_indices"]
    
    match_xyzs = p.match_xyzs # all matches, including outliers
    num_inlier_pairs = len(inlier_indices)
    
    match_xyzs_i_inliers = []
    match_xyzs_j_inliers = []
    
    
    for inlier_idx in inlier_indices:
        inlier_match_xyz = match_xyzs[inlier_idx]
        match_xyzs_i_inliers.append(inlier_match_xyz[0])
        match_xyzs_j_inliers.append(inlier_match_xyz[1])
    
    match_xyzs_i_inliers = np.array(match_xyzs_i_inliers)
    match_xyzs_j_inliers = np.array(match_xyzs_j_inliers)
    
    panomatchidx_to_match_xyzs_i_inliers.append(match_xyzs_i_inliers)
    panomatchidx_to_match_xyzs_j_inliers.append(match_xyzs_j_inliers)




cam_indices_i = np.zeros(num_inlier_matches, dtype=int) # array: for each observation, indicates which camidx it is associated with for cam i
cam_indices_j = np.zeros(num_inlier_matches, dtype=int) # array: for each observation, indicates which camidx it is associated with for cam j
observation_idx = 0
for panomatchidx in range(num_pano_matches):
    p = all_pano_matches[panomatchidx]
    fn_i = p.frame_number_i
    fn_j = p.frame_number_j

    essential_data = frame_to_frame_to_essential_data[fn_i][fn_j]
    
    inlier_indices = essential_data["best_fit_inlier_indices"]
    
    camidx_i = panomatchidx_to_camidxs[panomatchidx][0]
    camidx_j = panomatchidx_to_camidxs[panomatchidx][1]
    
    num_inlier_indices = len(inlier_indices)
    
    cam_indices_i[observation_idx:observation_idx + num_inlier_indices] = camidx_i
    cam_indices_j[observation_idx:observation_idx + num_inlier_indices] = camidx_j
    
    observation_idx += num_inlier_indices
      
    
#FOOOOBAR
 












def get_errs_for_panomatch(panomatchidx, cam_mat_i, cam_mat_j):
    errs_for_this_panomatch = []
    
    mat_i_to_j = np.matmul(np.linalg.inv(cam_mat_j), cam_mat_i)
    
    rot_i_to_j = mat_i_to_j[:3,:3]

    t_i_to_j = mat_i_to_j[:3,3]
    t_ij_norm = t_i_to_j / np.linalg.norm(t_i_to_j)
    #t_ij_norm = t_i_to_j

    t_cross_i_to_j = vector_to_cross_product_matrix(t_ij_norm)

    E_i_to_j = np.matmul(t_cross_i_to_j, rot_i_to_j)
    
    match_xyzs_i_inliers = panomatchidx_to_match_xyzs_i_inliers[panomatchidx]
    match_xyzs_j_inliers = panomatchidx_to_match_xyzs_j_inliers[panomatchidx]
    
    num_pairs = len(match_xyzs_i_inliers)
    
    if num_pairs > 0:
    
        E_pi = np.dot(E_i_to_j, match_xyzs_i_inliers.T).T
    
        pj_E_pi = np.einsum('ij,ij->i', match_xyzs_j_inliers, E_pi) # calculating row-wise dot products between the rows of match_xyzs_j_inliers and the rows of E_pi
        
        dist_ep_tangents = pj_E_pi / np.sqrt(1.0 - pj_E_pi*pj_E_pi) # dist_ep_tangent is defined in Pagani's work
    
        errs_for_this_panomatch = dist_ep_tangents
    return errs_for_this_panomatch


def fun(params, n_cameras, panomatchidx_to_camidxs):
    print("start fun()")
    
    errs = []
    
    cam_mats = np.zeros((n_cameras, 4, 4))
    
    for camidx in range(n_cameras):
        cam_rot_vector = params[(camidx * num_params_per_camera) : (camidx * num_params_per_camera + 3)]
        
        cam_trans_vector = params[(camidx * num_params_per_camera + 3) : (camidx * num_params_per_camera + 6)]
        
        cam_rot_mat, _ = cv2.Rodrigues(cam_rot_vector)
        
        cam_mats[camidx, :3, :3] = cam_rot_mat
        cam_mats[camidx, :3, 3] = cam_trans_vector
        cam_mats[camidx, 3, 3] = 1.0
    
    errs_for_each_panomatch = [None] * num_pano_matches

    for panomatchidx in range(num_pano_matches):
        camidx_i = panomatchidx_to_camidxs[panomatchidx][0]
        camidx_j = panomatchidx_to_camidxs[panomatchidx][1]
        
        cam_mat_i = cam_mats[camidx_i]
        cam_mat_j = cam_mats[camidx_j]
        errs_for_each_panomatch[panomatchidx] = get_errs_for_panomatch(panomatchidx, cam_mat_i, cam_mat_j)
    

    errs = [item for sublist in errs_for_each_panomatch for item in sublist] # flatten list of lists

    errs = np.array(errs)
    
    print("end fun(), mean abs err = {}".format(np.mean(np.abs(errs))))

    return errs



# Make the Jacobian sparsity structure
m = num_inlier_matches * 1 # only 1D residual from each pair
n = num_cameras * num_params_per_camera
A = lil_matrix((m, n), dtype=int)

i = np.arange(num_inlier_matches)
for s in range(num_params_per_camera):
    A[1 * i, cam_indices_i * num_params_per_camera + s] = 1
    A[1 * i, cam_indices_j * num_params_per_camera + s] = 1


start_time = time.time()
initial_errs = fun(input_params, num_cameras, panomatchidx_to_camidxs)
#initial_errs = profile.run('fun(input_params, num_cameras, panomatchidx_to_camidxs)', sort='tottime')
elapsed_time = time.time() - start_time
print("elapsed time for one iteration of fun(): {}".format(elapsed_time))

#plt.plot(initial_errs)


# how much deviation in X/Y/Z from the original estimated location is allowed
translation_max_distance_x = 0.1
translation_max_distance_y = 0.025
translation_max_distance_z = 0.1
translation_max_distance = np.array([translation_max_distance_x, translation_max_distance_y, translation_max_distance_z])

# Set up bounds/constraints to prevent translations from deviating too much (if translation is not fixed)
param_bounds_min = []
param_bounds_max = []

for camidx in range(num_cameras):
    # rotation should be unconstrained
    param_bounds_min += [-np.inf, -np.inf, -np.inf]
    param_bounds_max += [np.inf, np.inf, np.inf]
    # but translation should be bounded by a fixed amount
    initial_trans = input_params[(camidx * num_params_per_camera + 3) : (camidx * num_params_per_camera + 6)]
    param_bounds_min += list(initial_trans - translation_max_distance)
    param_bounds_max += list(initial_trans + translation_max_distance)
        
        

param_bounds = (param_bounds_min, param_bounds_max)






print("starting optimization, please wait...")
t0 = time.time()

#res = least_squares(fun, input_params, bounds=param_bounds, jac_sparsity=A, verbose=2, x_scale='jac', ftol=1e-2, method='trf', loss='soft_l1', args=(num_cameras, panomatchidx_to_camidxs))
res = least_squares(fun, input_params, bounds=param_bounds, jac_sparsity=A, verbose=2, x_scale='jac', ftol=1e-7, method='trf', args=(num_cameras, panomatchidx_to_camidxs))

t1 = time.time()
print("Optimization took {0:.0f} seconds".format(t1 - t0))


refined_params = res.x
refined_errs = fun(refined_params, num_cameras, panomatchidx_to_camidxs)

#plt.plot(initial_errs)
#plt.plot(refined_errs)

# =============================================================================

# Use the refined params to generate a new output file that lists the refined poses for each frame number.

FN_TO_POSE_INITIAL_FILENAME = "fn_to_pose_initial.csv"
FN_TO_POSE_REFINED_FILENAME = "fn_to_pose_refined.csv"

output_line_initial = []
output_line_refined = []

for camidx in range(num_cameras):
    cam_fn = unique_frame_numbers_in_refinement[camidx]

    input_rot_vec = input_params[(camidx * num_params_per_camera) : (camidx * num_params_per_camera + 3)]
    refined_rot_vec = refined_params[(camidx * num_params_per_camera) : (camidx * num_params_per_camera + 3)]
    
    input_rot_mat, _ = cv2.Rodrigues(input_rot_vec)
    refined_rot_mat, _ = cv2.Rodrigues(refined_rot_vec)
    
    input_cam_trans_vector = input_params[(camidx * num_params_per_camera + 3) : (camidx * num_params_per_camera + 6)]
    refined_cam_trans_vector = refined_params[(camidx * num_params_per_camera + 3) : (camidx * num_params_per_camera + 6)]
    
    input_cam_mat = np.identity(4)    
    input_cam_mat[:3, :3] = input_rot_mat
    input_cam_mat[:3, 3] = input_cam_trans_vector
    input_cam_mat[3, 3] = 1.0
    
    refined_cam_mat = np.identity(4)
    refined_cam_mat[:3, :3] = refined_rot_mat
    refined_cam_mat[:3, 3] = refined_cam_trans_vector
    refined_cam_mat[3, 3] = 1.0

    output_line_initial.append([cam_fn] + list(input_cam_mat.reshape(-1)))
    output_line_refined.append([cam_fn] + list(refined_cam_mat.reshape(-1)))

fn_pose_initial_path = os.path.join(args.base_dir, FN_TO_POSE_INITIAL_FILENAME)
fn_pose_refined_path = os.path.join(args.base_dir, FN_TO_POSE_REFINED_FILENAME)

with open(fn_pose_initial_path, 'wb') as out_file:
    writer = csv.writer(out_file, delimiter=',')
    
    for row in output_line_initial:
        writer.writerow(row)

with open(fn_pose_refined_path, 'wb') as out_file:
    writer = csv.writer(out_file, delimiter=',')
    
    for row in output_line_refined:
        writer.writerow(row)
















# =============================================================================

def params_to_cam_centers(params):
    cam_centers = []
    n_cameras = len(params) / num_params_per_camera
    for camidx in range(n_cameras):
        cam_trans_vector = params[(camidx * num_params_per_camera + 3) : (camidx * num_params_per_camera + 6)]
        cam_centers.append(cam_trans_vector)
    cam_centers = np.array(cam_centers)
    return cam_centers

initial_cam_centers = params_to_cam_centers(input_params)
refined_cam_centers = params_to_cam_centers(refined_params)

"""
fig = plt.figure(1)
ax = fig.add_subplot(111, projection='3d')

# Draw camera centers
ax.plot(initial_cam_centers[:,0], initial_cam_centers[:,1], initial_cam_centers[:,2], c='r')
ax.plot(refined_cam_centers[:,0], refined_cam_centers[:,1], refined_cam_centers[:,2], c='g')

plot_center_point = np.array([0.0, 0.0, 0.0])
plot_range = 10.0

ax.auto_scale_xyz([plot_center_point[0] - plot_range, plot_center_point[0] + plot_range], [plot_center_point[1] - plot_range, plot_center_point[1] + plot_range], [plot_center_point[2] - plot_range, plot_center_point[2] + plot_range])
plt.show()
"""


















def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)

def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))










# Plot the camera centers.
"""
fig = plt.figure(1)
ax = fig.add_subplot(111, projection='3d')

# Draw camera centers
ax.plot(sphere_pose_camera_centers[:,0], sphere_pose_camera_centers[:,1], sphere_pose_camera_centers[:,2], c='g')

input_x_color = (1.0, 0.5, 0.5)
input_y_color = (0.5, 1.0, 0.5)
input_z_color = (0.5, 0.5, 1.0)

refined_x_color = (1.0, 0.0, 0.0)
refined_y_color = (0.0, 1.0, 0.0)
refined_z_color = (0.0, 0.0, 1.0)

for camidx in range(num_cameras):
    
    input_rot_vec = input_params[(camidx * num_params_per_camera) : (camidx * num_params_per_camera + 3)]
    refined_rot_vec = refined_params[(camidx * num_params_per_camera) : (camidx * num_params_per_camera + 3)]
    
    input_rot_mat, _ = cv2.Rodrigues(input_rot_vec)
    refined_rot_mat, _ = cv2.Rodrigues(refined_rot_vec)
    
    input_cam_trans_vector = input_params[(camidx * num_params_per_camera + 3) : (camidx * num_params_per_camera + 6)]
    refined_cam_trans_vector = refined_params[(camidx * num_params_per_camera + 3) : (camidx * num_params_per_camera + 6)]
    
    input_cam_mat = np.identity(4)    
    input_cam_mat[:3, :3] = input_rot_mat
    input_cam_mat[:3, 3] = input_cam_trans_vector
    input_cam_mat[3, 3] = 1.0
    
    refined_cam_mat = np.identity(4)
    refined_cam_mat[:3, :3] = refined_rot_mat
    refined_cam_mat[:3, 3] = refined_cam_trans_vector
    refined_cam_mat[3, 3] = 1.0
    
    axis_length = 0.25

    zero_pt = np.array([0.0, 0.0, 0.0, 1.0])
    x_pt = np.array([axis_length, 0.0, 0.0, 1.0])
    y_pt = np.array([0.0, axis_length, 0.0, 1.0])
    z_pt = np.array([0.0, 0.0, axis_length, 1.0])

    input_zero_pt = np.dot(input_cam_mat, zero_pt)
    input_x_pt = np.dot(input_cam_mat, x_pt)
    input_y_pt = np.dot(input_cam_mat, y_pt)
    input_z_pt = np.dot(input_cam_mat, z_pt)

    refined_zero_pt = np.dot(refined_cam_mat, zero_pt)
    refined_x_pt = np.dot(refined_cam_mat, x_pt)
    refined_y_pt = np.dot(refined_cam_mat, y_pt)
    refined_z_pt = np.dot(refined_cam_mat, z_pt)
    
    input_xaxis = np.vstack((input_zero_pt, input_x_pt))
    input_yaxis = np.vstack((input_zero_pt, input_y_pt))
    input_zaxis = np.vstack((input_zero_pt, input_z_pt))
    
    refined_xaxis = np.vstack((refined_zero_pt, refined_x_pt))
    refined_yaxis = np.vstack((refined_zero_pt, refined_y_pt))
    refined_zaxis = np.vstack((refined_zero_pt, refined_z_pt))
    
    ax.plot(input_xaxis[:,0], input_xaxis[:,1], input_xaxis[:,2], c=input_x_color)
    ax.plot(input_yaxis[:,0], input_yaxis[:,1], input_yaxis[:,2], c=input_y_color)
    ax.plot(input_zaxis[:,0], input_zaxis[:,1], input_zaxis[:,2], c=input_z_color)
    
    ax.plot(refined_xaxis[:,0], refined_xaxis[:,1], refined_xaxis[:,2], c=refined_x_color)
    ax.plot(refined_yaxis[:,0], refined_yaxis[:,1], refined_yaxis[:,2], c=refined_y_color)
    ax.plot(refined_zaxis[:,0], refined_zaxis[:,1], refined_zaxis[:,2], c=refined_z_color)

plot_center_point = np.array([0.0, 0.0, 0.0])
plot_range = 10.0

ax.auto_scale_xyz([plot_center_point[0] - plot_range, plot_center_point[0] + plot_range], [plot_center_point[1] - plot_range, plot_center_point[1] + plot_range], [plot_center_point[2] - plot_range, plot_center_point[2] + plot_range])
plt.show()
"""
