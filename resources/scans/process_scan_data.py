# -*- coding: utf-8 -*-
"""
Created on Sat Jan 20 14:13:58 2018

@author: ANDERSED

Script that does start-to-finish processing of Holo-scan data.

Given the input raw files from a specific scan directory, generates all needed
frames, aligns poses, and generates geometry needed for visualization.

Input raw files needed are:
    {SCAN_LABEL_DIR}
        {SCAN_LABEL}_frames.zip (frames from HoloLens camera, first 10 seconds)
        {SCAN_LABEL}_mesh.room (encoded geometry of room from HoloLens)
        {SCAN_LABEL}_pose.csv (estimated poses from HoloLens camera, each pose index)
        {PANOVID_NAME}.MP4 (raw unstitched video from 360 camera)
        {PANOVID_NAME}_Stitch_XHC.MP4 (360 camera video, stitched by Gear 360 ActionDirector)


"""


import argparse
import os
import process_scan_data_helpers
from holoscan_common import readFile, SCANDATA_FILENAME

print("Starting up scan-processor...")

os.chdir("E:\\Dev\\OpenGL\\lumiviz\\resources\\scans")


parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)

print("Base scan directory: {}".format(args.base_dir))

SCAN_LABEL = os.path.split(args.base_dir)[-1]

print("SCAN_LABEL: {}".format(SCAN_LABEL))

# Get filenames for raw inputs. Also verify that the files exist.
holoframes_zip, mesh_room, pose_csv, panovid = process_scan_data_helpers.get_all_input_filenames(args.base_dir, SCAN_LABEL)
    


# Extract and flip the HoloLens frames
process_scan_data_helpers.extract_and_flip_hololens_frames(args.base_dir, holoframes_zip)
    
# Extract frames from panoramic video
process_scan_data_helpers.extract_panoramic_video_frames(args.base_dir, panovid)

# Set up scan-data file to synchronize HoloLens and panoramic video.
process_scan_data_helpers.setup_scan_data_file(args.base_dir, panovid)

# Generate "pose_sphere.csv" file based on fixed estimates of extrinsics between HoloLens and 360 cam.
process_scan_data_helpers.setup_sphere_pose_csv(args.base_dir, pose_csv)

# Load in the grid_size_meters from the scan data file.
scanData = readFile(os.path.join(args.base_dir, SCANDATA_FILENAME))
grid_size_meters = scanData["grid_size_meters"]
raycasting_enabled_during_matchlist_generation = scanData["raycasting_enabled_during_matchlist_generation"]

# Generate "sphere_pose_triangulation.yaml" based on estimated positions from pose_sphere.csv.
process_scan_data_helpers.generate_scan_triangulation(args.base_dir, grid_size_meters)

# Generate "refinement_frame_number_matches.csv" to determine which frames to generate SPHORB matches for.
process_scan_data_helpers.generate_sphorb_match_list(args.base_dir, raycasting_enabled_during_matchlist_generation)

# Generate the list of 1D path frames to use too, and append the pairwise matches to the SPHORB match list.
process_scan_data_helpers.select_1d_path(args.base_dir)

# NOTE: at this point, all in-use panoframes must be selected!

# Delete any unused panoframes to save on space.
process_scan_data_helpers.delete_unused_panoframes(args.base_dir)


# Create SPHORB matches.
process_scan_data_helpers.create_sphorb_matches(args.base_dir)

# TODO: at this point, run scratch_test_essential_matrix.py and refine_holo_sphere_poses.py