# -*- coding: utf-8 -*-
"""
Created on Sat Jan 20 14:13:58 2018

@author: ANDERSED

Continues processing, after pose refinement is done.

First run process_scan_data.py.
Then run scratch_test_essential_matrix.py.
Then run refine_holo_sphere_poses.py.

The result should be that you have a file "fn_to_pose_refined.csv".

Now you can begin this script.

"""


import argparse
import os
import process_scan_data_helpers
from holoscan_common import readFile, SCANDATA_FILENAME
import numpy as np
import csv

print("Starting up scan-processor...")

os.chdir("E:\\Dev\\OpenGL\\lumiviz\\resources\\scans")


parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)

print("Base scan directory: {}".format(args.base_dir))

SCAN_LABEL = os.path.split(args.base_dir)[-1]

print("SCAN_LABEL: {}".format(SCAN_LABEL))


# Load in the grid_size_meters from the scan data file.
scanData = readFile(os.path.join(args.base_dir, SCANDATA_FILENAME))
grid_size_meters = scanData["grid_size_meters"]


# Generate refined sphere pose triangulation, using only frame numbers and not pose indices.
process_scan_data_helpers.generate_refined_scan_triangulation(args.base_dir, grid_size_meters)

# =============================================================================

fn_to_pose_refined_path = os.path.join(args.base_dir, "fn_to_pose_refined.csv")

if not os.path.isfile(fn_to_pose_refined_path):
    raise Exception("Missing refined poses at {}".format(fn_to_pose_refined_path))

refined_frame_number_triangulation_path = os.path.join(args.base_dir, "sphere_pose_triangulation_refined_frame_numbers.yaml")

if not os.path.isfile(refined_frame_number_triangulation_path):
    raise Exception("Missing refined sphere pose triangulation at {}".format(refined_frame_number_triangulation_path))

# =============================================================================

# Load the refined poses for each frame number.

fns_to_poses = {}   # contains matrix [R|t] where [R|t] * X_local = X_world

with open(fn_to_pose_refined_path, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        fn = int(row[0]) # 0-indexed
        mat = np.array([float(x) for x in row[1:]]).reshape((4,4))
        fns_to_poses[fn] = mat
                    
# =============================================================================

refined_triangulation_frame_numbers = readFile(refined_frame_number_triangulation_path)["triangulation_indices"]

# =============================================================================

# First determine if there are any additional SPHORB matches that need to be generated.

missing_sphorb_matches = []

for triplet in refined_triangulation_frame_numbers:
    triplet_canonical_order = sorted(triplet) # used for labeling the triplet uniquely

    for a in range(3):
            
        b = (a+1)%3
        fn_a = triplet_canonical_order[a] # 0-indexed
        fn_b = triplet_canonical_order[b]

        # fn_i is always less than fn_j
        fn_i = min(fn_a, fn_b)
        fn_j = max(fn_a, fn_b)
        
        sphorb_match_path = os.path.join(args.base_dir, "pano_frames", "sphorb_matches", "sphorb_match_%05d_%05d.csv" % (fn_i + 1, fn_j + 1)) # go to 1-indexed
        
        if not os.path.isfile(sphorb_match_path):
            missing_sphorb_matches.append([fn_i, fn_j])

if len(missing_sphorb_matches) > 0:
    print("need to generate {} more SPHORB matches due to changes in triangulation.".format(len(missing_sphorb_matches)))

    match_list_csv = os.path.join(args.base_dir, "refinement_frame_number_matches.csv")
    with open(match_list_csv, 'ab') as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        
        for missing_match in missing_sphorb_matches:
            fn_i = missing_match[0]
            fn_j = missing_match[1]        
            writer.writerow([fn_i, fn_j])

    print("Rerunning SPHORB matching to get new matches.")
    process_scan_data_helpers.create_sphorb_matches(args.base_dir)
    
    print("At this point, we should have all the pairwise matches we need now.")



# For each triplet, generate meshes from sparse correspondences.

