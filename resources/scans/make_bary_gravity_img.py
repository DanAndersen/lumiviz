# -*- coding: utf-8 -*-
"""
Created on Sat Feb 17 15:19:47 2018

@author: Daniel
"""

import cv2
import numpy as np
import matplotlib.pyplot as plt
import scipy.spatial

width = 720
height = 720

bary_gravity_enabled = True
bary_gravity_min = 0.6
bary_gravity_max = 0.8

#tri_points_normalized = np.array([[0.0, 0.5], [1.0, 0.0], [1.0, 1.0]])
#tri_points = tri_points_normalized * np.array([height, width])

tri_points = np.array([[112.0, 193.0], [410.0, 531.0], [593.0, 196.0]])


tri_mat = np.array([[tri_points[0][0], tri_points[1][0], tri_points[2][0]], [tri_points[0][1], tri_points[1][1], tri_points[2][1]], [1, 1, 1]])
inv_tri_mat = np.linalg.inv(tri_mat)

img = np.zeros((height,width,3), np.uint8)

for i in range(height):
    for j in range(width):
        bary_coords = np.dot(inv_tri_mat, np.array([i,j,1]))
        if bary_coords[0] > 0 and bary_coords[1] > 0 and bary_coords[2] > 0:
            
            draw_this_pixel = False
            
            render_bary_coords = np.copy(bary_coords)
            if bary_gravity_enabled:
                max_bary_index = np.argmax(bary_coords)
                max_bary_value = bary_coords[max_bary_index]
                
                nonmax_bary_indices = list(range(3))
                nonmax_bary_indices.remove(max_bary_index)
                
                if max_bary_value > bary_gravity_max:
                    render_bary_coords[max_bary_index] = 1.0
                    render_bary_coords[nonmax_bary_indices] = 0.0
                    draw_this_pixel = True
                elif max_bary_value > bary_gravity_min:
                    f = (max_bary_value - bary_gravity_min) / (bary_gravity_max - bary_gravity_min)
                    render_bary_coords[max_bary_index] = (bary_coords[max_bary_index] + (1.0 - bary_coords[max_bary_index]) * f)
                    render_bary_coords[nonmax_bary_indices] = (bary_coords[nonmax_bary_indices] * (1.0 - f))
                    draw_this_pixel = False
            
            if draw_this_pixel:
                img[i, j] = np.asarray(render_bary_coords*255, dtype=np.uint8)
        
plt.imshow(img)

cv2.imwrite("out.png", img)