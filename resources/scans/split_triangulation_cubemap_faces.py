# -*- coding: utf-8 -*-
"""
Created on Thu Oct 26 12:01:05 2017

@author: ANDERSED
"""

# Given a triangulation of pose indices in the room, find the corresponding sphere frames,
# and split them into individual image files, 1 file per cubemap face.

import numpy as np
import cv2
import os
import glob
import matplotlib.pyplot as plt
import sys
import argparse
import ntpath
import yaml
import csv
import ntpath


TRIANGULATION_FILENAME = "sphere_pose_triangulation.yaml"
SCANDATA_FILENAME = "scandata.yaml"
POSE_SPHERE_FILENAME = "pose_sphere.csv"

MATCHLIST_FILENAME = "cubemap_face_matches.txt"

def saveFile(fname,data):
	fd = open(fname,"wb")
	yaml.dump(data,fd)
	fd.close()

def readFile(fname):
	fd = open(fname,"rb")
	data = yaml.load(fd)
	return data

parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)


scanData = readFile(os.path.join(args.base_dir, SCANDATA_FILENAME))

sphere_pose_timestamps = []

with open(os.path.join(args.base_dir, POSE_SPHERE_FILENAME), 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        timestamp = float(row[0])
        
        sphere_pose_timestamps.append(timestamp)
        


def GetSphericalCameraFrameIndexFromPoseIndex(pose_index):
    locatableCameraSyncFrameIndex = scanData["hololens_sync_frame"] - 1 # go from 1-indexed to 0-indexed
    locatableCameraSyncFrameTimestamp = sphere_pose_timestamps[locatableCameraSyncFrameIndex]
    
    sphericalCameraSyncFrameIndex = scanData["spherical_sync_frame"] - 1 # go from 1-indexed to 0-indexed
    sphericalCameraFPS = scanData["spherical_fps"]
    
    locatableCameraTimestamp = sphere_pose_timestamps[pose_index]
    secondsSinceCalibrationTime = (locatableCameraTimestamp - locatableCameraSyncFrameTimestamp)
    sphericalCameraFrameIndex = int(round(sphericalCameraSyncFrameIndex + (secondsSinceCalibrationTime * sphericalCameraFPS)))
    return sphericalCameraFrameIndex

def GetCubemapPath(sphere_camera_frame_index):
    cubemapLabel = "pano_frame%05d_cubemap" % (sphere_camera_frame_index + 1,) # frame images are 1-indexed
    cubemapPath = os.path.join(args.base_dir, "pano_frames/cubemaps/" + cubemapLabel + ".jpg")
    return cubemapPath

cubemapFaceBaseDir = os.path.join(args.base_dir, "pano_frames/cubemaps/faces/")

if not os.path.exists(cubemapFaceBaseDir):
    os.makedirs(cubemapFaceBaseDir)

def GetCubemapFaceFilename(sphere_camera_frame_index, sideIdx):
    cubemapFaceFilename = "pano_frame%05d_cubemap_face_%d.jpg" % (sphere_camera_frame_index + 1,sideIdx,) # frame images are 1-indexed
    return cubemapFaceFilename

def GetCubemapFacePath(sphere_camera_frame_index, sideIdx):
    cubemapFaceFilename =  GetCubemapFaceFilename(sphere_camera_frame_index, sideIdx)
    cubemapFacePath = os.path.join(cubemapFaceBaseDir, cubemapFaceFilename)
    return cubemapFacePath

triangulation_data = readFile(os.path.join(args.base_dir, TRIANGULATION_FILENAME))

triangulation_indices = triangulation_data["triangulation_indices"]

unique_pose_indices = set()

for triangle in triangulation_indices:
    for idx in triangle:
        unique_pose_indices.add(idx)

sphere_camera_frame_indices = set()
        
# need to convert between pose indices to sphere-frame-numbers        
for pose_index in unique_pose_indices:
    sphere_camera_frame_indices.add(GetSphericalCameraFrameIndexFromPoseIndex(pose_index))
    
for sphere_camera_frame_index in sphere_camera_frame_indices:
    cubemapPath = GetCubemapPath(sphere_camera_frame_index)
    print("reading " + cubemapPath + "...")
    
    cubemapImg = cv2.imread(cubemapPath)
    
    cubeSidePixels = cubemapImg.shape[0]
    
    # skip the bottom face
    for sideIdx in range(5):
        cubeFaceImg = cubemapImg[:,sideIdx*cubeSidePixels : (sideIdx+1)*cubeSidePixels]

        cv2.imwrite(GetCubemapFacePath(sphere_camera_frame_index, sideIdx), cubeFaceImg)
    
print "done"














# now generate a pair-list for matching because we don't want to match every face with every other face.

# generate a representation of the triangulation where we are working with sphere frame indices rather than pose indices.
triangulation_sphere_frame_indices = []
for pose_index_triangle in triangulation_indices:
    sphere_frame_index_triangle = []
    for pose_index_triangle_val in pose_index_triangle:
        sphere_frame_index_triangle.append(GetSphericalCameraFrameIndexFromPoseIndex(pose_index_triangle_val))
    triangulation_sphere_frame_indices.append(sphere_frame_index_triangle)

# for each entry (frame index), list all the frame indices that are adjacent to it in the triangulation
adjacency_map = {}

for frame_index_triangle in triangulation_sphere_frame_indices:
    print frame_index_triangle
    
    for i in range(3):
        j = (i + 1) % 3
        k = (i - 1) % 3
        
        frame_index_i = frame_index_triangle[i]
        frame_index_j = frame_index_triangle[j]
        frame_index_k = frame_index_triangle[k]
        
        if frame_index_i not in adjacency_map:
            adjacency_map[frame_index_i] = set()
        
        adjacency_map[frame_index_i].add(frame_index_j)
        adjacency_map[frame_index_i].add(frame_index_k)

print("writing match list...")
selected_mappings = {}

for frame_idx_a in adjacency_map:
    if frame_idx_a not in selected_mappings:
        selected_mappings[frame_idx_a] = set()
    
    for frame_idx_b in adjacency_map[frame_idx_a]:
        if frame_idx_a < frame_idx_b:
            selected_mappings[frame_idx_a].add(frame_idx_b)
                        
        for frame_idx_c in adjacency_map[frame_idx_b]:
            if frame_idx_c != frame_idx_a and frame_idx_a < frame_idx_c:
                selected_mappings[frame_idx_a].add(frame_idx_c)
            
            

with open(os.path.join(args.base_dir, MATCHLIST_FILENAME), 'w') as out_file:
    for src in selected_mappings:
        for dst in selected_mappings[src]:
            for sideIdx_src in range(4):
                src_filename = GetCubemapFaceFilename(src, sideIdx_src)
                for sideIdx_dst in range(4):
                    dst_filename = GetCubemapFaceFilename(dst, sideIdx_dst)
                    out_file.write("{} {}\n".format(src_filename, dst_filename))
print "done"