# -*- coding: utf-8 -*-
"""
Created on Tue Oct 10 13:34:16 2017

@author: ANDERSED
"""

# given an input set of poses from the (spherical) path walked by the user,
# generate a Delaunay triangulation to the desired density -- each cell in the
# room is marked with a pose that is closest to the center of that cell.

import numpy as np
import cv2
import os
import glob
import matplotlib.pyplot as plt
import sys
import argparse
import ntpath
import yaml
import csv
import ntpath

from scipy.spatial import Delaunay

TRIANGULATION_FILENAME = "sphere_pose_triangulation.yaml"

def saveFile(fname,data):
	fd = open(fname,"wb")
	yaml.dump(data,fd)
	fd.close()

def readFile(fname):
	fd = open(fname,"rb")
	data = yaml.load(fd)
	return data

parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")
parser.add_argument("grid_size_meters", type=float, help="the grid size of the triangulation (meters)")

args = parser.parse_args()

print(args)

in_spherical_pose_csv_path = os.path.join(args.base_dir, "pose_sphere.csv")

print("Loading in spherical pose data from {}".format(in_spherical_pose_csv_path))

sphere_positions = []

with open(in_spherical_pose_csv_path, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        timestamp = float(row[0])
        
        sphere_m = np.array([float(i) for i in row[1:]]).reshape((4,4))
        
        pos = sphere_m[:3,3]
        
        sphere_positions.append(pos)
    
sphere_positions = np.array(sphere_positions)

bounding_box_min = np.min(sphere_positions, axis=0)
bounding_box_max = np.max(sphere_positions, axis=0)

closest_pose_distances = {}
closest_pose_indices = {}

for i, pos in enumerate(sphere_positions):
    cell_id = (int(np.round(pos[0] / args.grid_size_meters)), int(np.round(pos[2] / args.grid_size_meters)))
    
    cell_center_position = np.array([cell_id[0] * args.grid_size_meters, pos[1], cell_id[1] * args.grid_size_meters])
    
    dist = np.linalg.norm(cell_center_position - pos)
    
    if cell_id not in closest_pose_distances or dist < closest_pose_distances[cell_id]:
        closest_pose_distances[cell_id] = dist
        closest_pose_indices[cell_id] = i
                      
tri_points = []
tri_original_indices = []
                            
for cell_id in closest_pose_indices:
    index = closest_pose_indices[cell_id]
    pos = sphere_positions[index]
    
    tri_points.append([pos[0], pos[2]])
    tri_original_indices.append(index)
    
tri_points = np.array(tri_points)

triangulation = Delaunay(tri_points)

# get the triangulation in terms of the original pose indices
tri_original_triplet_indices = []
tri_simplices = triangulation.simplices
for triplet in tri_simplices:
    original_indices_triplet = []
    for triplet_value in triplet:
        original_indices_triplet.append(tri_original_indices[triplet_value])
    tri_original_triplet_indices.append(original_indices_triplet)


plt.triplot(sphere_positions[:,0], sphere_positions[:,2], np.array(tri_original_triplet_indices)) # only the poses used in the triangulation
plt.plot(sphere_positions[:,0], sphere_positions[:,2], 'o') # all the original poses
plt.show()

output_tri_data = {
        "grid_size_meters": args.grid_size_meters,
        "triangulation_indices": tri_original_triplet_indices
        }

out_save_data_path = os.path.join(args.base_dir, TRIANGULATION_FILENAME)

print("saving triangulation to {}".format(out_save_data_path))
saveFile(out_save_data_path, output_tri_data)
print("done saving triangulation.")