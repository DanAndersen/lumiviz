# -*- coding: utf-8 -*-
"""
Created on Tue Nov 21 20:24:08 2017

@author: ANDERSED
"""

import argparse
import re
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import yaml
import os
import csv
import functools
from scipy.spatial import ConvexHull
from scipy.spatial import Delaunay
import cv2
from holoscan_common import Camera, loadFullCubemapImgForFrameNumber, loadRefinedCubemapData, saveFile, readFile

parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)



def get_points_visible_to_cams(input_nvm_file, frame_number_i, side_idx_i, frame_number_j, side_idx_j):
    
    cam_i = None
    cam_j = None
    
    cam_i_image_index = -1
    cam_j_image_index = -1
    
    points_seen_by_cam_i = []
    points_seen_by_cam_j = []
    points_seen_by_cam_i_and_cam_j = []
    
    with open(input_nvm_file, 'r') as fstream:
        header_line = fstream.readline().split()
        while True:
            line = fstream.readline().split()
            if len(line) > 0:
                break
        num_raw_cameras = int(line[0])
        print("found {} raw cameras".format(num_raw_cameras))
        for cam_image_index in range(num_raw_cameras):
            line = fstream.readline().split()
            raw_cam_name = line[0]
            
            regex_match_groups = re.match("pano_frame([0-9]+)_cubemap_face_([0-9]+).jpg", raw_cam_name).groups()
            
            frame_number = int(regex_match_groups[0])   # 1-indexed!
            side_idx = int(regex_match_groups[1])
            
            raw_cam_quat = [float(x) for x in line[2:6]]
            raw_cam_center = [float(x) for x in line[6:9]]
            
            raw_cam = Camera()
            raw_cam.name = raw_cam_name
            raw_cam.frame_number = frame_number
            raw_cam.side_idx = side_idx
            raw_cam.SetQuaternionRotation(raw_cam_quat)
            raw_cam.SetCameraCenterAfterRotation(raw_cam_center)
            
            if frame_number == frame_number_i and side_idx == side_idx_i:
                cam_i = raw_cam
                cam_i_image_index = cam_image_index
            
            if frame_number == frame_number_j and side_idx == side_idx_j:
                cam_j = raw_cam
                cam_j_image_index = cam_image_index
            
        while True:
            line = fstream.readline().split()
            if len(line) > 0:
                break
        num_points = int(line[0])
        print("found {} points".format(num_points))
        for point_index in range(num_points):
            line = fstream.readline().split()
            point_xyz = [float(x) for x in line[0:3]]
            
            
            pt = {"point_index": point_index, "xyz": point_xyz}
            
            seen_by_cam_i = False
            seen_by_cam_j = False
            seen_by_cam_i_and_cam_j = False
            
            
            num_measurements = int(line[6])
            for j in range(num_measurements):
                raw_camera_index = int(line[7 + 4*j])
                
                obs_u = float(line[7 + 4*j + 2]) + (cube_side_pixels / 2.0)
                obs_v = float(line[7 + 4*j + 3]) + (cube_side_pixels / 2.0)
                
                if raw_camera_index == cam_i_image_index:
                    seen_by_cam_i = True
                    pt["obs_cam_i"] = (obs_u, obs_v)
                
                if raw_camera_index == cam_j_image_index:
                    seen_by_cam_j = True
                    pt["obs_cam_j"] = (obs_u, obs_v)
                    
                if seen_by_cam_i and seen_by_cam_j:
                    seen_by_cam_i_and_cam_j = True
                    
            if seen_by_cam_i:
                points_seen_by_cam_i.append(pt)
                
            if seen_by_cam_j:
                points_seen_by_cam_j.append(pt)
            
            if seen_by_cam_i_and_cam_j:
                points_seen_by_cam_i_and_cam_j.append(pt)
            
    ret_data = {
            "cam_i" : cam_i,
            "cam_j" : cam_j,
            "points_seen_by_cam_i": points_seen_by_cam_i,
            "points_seen_by_cam_j": points_seen_by_cam_j,
            "points_seen_by_cam_i_and_cam_j": points_seen_by_cam_i_and_cam_j
            }            

    print("done reading data")
    
    return ret_data



























frame_number_i = 464
pano_i_side_idx = 0

frame_number_j = 480
pano_j_side_idx = 0



input_nvm_file_before_solve = r'E:\Dev\Ceres\cubemap_bundle_adjustment\build\OUT_processed_no_solve_workspace_2step.nvm'
input_nvm_file_after_solve = r'E:\Dev\Ceres\cubemap_bundle_adjustment\build\OUT_processed_after_solve_workspace_2step.nvm'

data_before_solve = get_points_visible_to_cams(input_nvm_file_before_solve, frame_number_i, pano_i_side_idx, frame_number_j, pano_j_side_idx)
data_after_solve = get_points_visible_to_cams(input_nvm_file_after_solve, frame_number_i, pano_i_side_idx, frame_number_j, pano_j_side_idx)

img_i = cv2.imread(os.path.join(args.base_dir, "pano_frames\\cubemaps\\faces\\pano_frame%05d_cubemap_face_%d.jpg" % (frame_number_i, pano_i_side_idx) ))
img_j = cv2.imread(os.path.join(args.base_dir, "pano_frames\\cubemaps\\faces\\pano_frame%05d_cubemap_face_%d.jpg" % (frame_number_j, pano_j_side_idx) ))

img_i_with_kp = np.copy(img_i)
img_j_with_kp = np.copy(img_j)


cube_side_pixels = 960.0



side_idxs_to_transformation_matrices = {
        0: np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]], dtype=np.float64),
        1: np.array([[0,0,1,0],[0,1,0,0],[-1,0,0,0],[0,0,0,1]], dtype=np.float64),
        2: np.array([[-1,0,0,0],[0,1,0,0],[0,0,-1,0],[0,0,0,1]], dtype=np.float64),
        3: np.array([[0,0,-1,0],[0,1,0,0],[1,0,0,0],[0,0,0,1]], dtype=np.float64),
        4: np.array([[0,0,-1,0],[1,0,0,0],[0,-1,0,0],[0,0,0,1]], dtype=np.float64),
        5: np.array([[0,0,-1,0],[-1,0,0,0],[0,1,0,0],[0,0,0,1]], dtype=np.float64)
        }

side_idxs_to_inverse_transformation_matrices = {}
for side_idx in side_idxs_to_transformation_matrices:
    side_idxs_to_inverse_transformation_matrices[side_idx] = np.linalg.inv(side_idxs_to_transformation_matrices[side_idx])


# NOTE: returns False if the point is behind the camera!
def worldPointToImagePoint(world_point, cam, cam_side_idx):
    
    pt_local_cam_forward = np.dot(cam.m , (world_point[:3] - cam.GetCameraCenter()))
    
    cam_side_idx_transformation_matrix = side_idxs_to_transformation_matrices[cam_side_idx]
    
    pt_local_cam_side = np.dot(cam_side_idx_transformation_matrix, np.append(pt_local_cam_forward,1))
    
    if pt_local_cam_side[2] > 0:    # behind the camera
        return (False, None)

    fx = cube_side_pixels / 2.0
    fy = cube_side_pixels / 2.0
    cx = cube_side_pixels / 2.0
    cy = cube_side_pixels / 2.0
    
    pt_projected_u = fx * (pt_local_cam_side[0]/pt_local_cam_side[2]) + cx
    pt_projected_v = fy * (pt_local_cam_side[1]/pt_local_cam_side[2]) + cy
    
    return (True, np.array([pt_projected_u, pt_projected_v]))










cross_line_radius = 5

def draw_keypoints_onto_img(img, points_list, obs_label):
    line_thickness = 2
    for pt in points_list:
        uv = pt[obs_label]
        uv = (int(uv[0]), int(uv[1]))
        
        cv2.line(img, (uv[0] - cross_line_radius, uv[1]), (uv[0] + cross_line_radius, uv[1]), (0,255,0), line_thickness)

        cv2.line(img, (uv[0], uv[1] - cross_line_radius), (uv[0], uv[1] + cross_line_radius), (0,255,0), line_thickness)


draw_keypoints_onto_img(img_i_with_kp, data_before_solve["points_seen_by_cam_i"], "obs_cam_i")
draw_keypoints_onto_img(img_j_with_kp, data_before_solve["points_seen_by_cam_j"], "obs_cam_j")

cv2.imwrite("E:\\img_i_with_kp.png", img_i_with_kp)
cv2.imwrite("E:\\img_j_with_kp.png", img_j_with_kp)




img_i_with_common_kp = np.copy(img_i)
img_j_with_common_kp = np.copy(img_j)

draw_keypoints_onto_img(img_i_with_common_kp, data_before_solve["points_seen_by_cam_i_and_cam_j"], "obs_cam_i")
draw_keypoints_onto_img(img_j_with_common_kp, data_before_solve["points_seen_by_cam_i_and_cam_j"], "obs_cam_j")

cv2.imwrite("E:\\img_i_with_common_kp.png", img_i_with_common_kp)
cv2.imwrite("E:\\img_j_with_common_kp.png", img_j_with_common_kp)


cam_i_before = data_before_solve["cam_i"]
cam_i_after = data_after_solve["cam_i"]

cam_j_before = data_before_solve["cam_j"]
cam_j_after = data_after_solve["cam_j"]


reproj_pts_i_before = []
reproj_pts_j_before = []

reproj_dists_i_before = []
reproj_dists_j_before = []

for pt_before in data_before_solve["points_seen_by_cam_i_and_cam_j"]:
    _, reproj_pt_before_i = worldPointToImagePoint(np.array(pt_before["xyz"]), cam_i_before, pano_i_side_idx)
    _, reproj_pt_before_j = worldPointToImagePoint(np.array(pt_before["xyz"]), cam_j_before, pano_j_side_idx)
    
    reproj_pts_i_before.append(reproj_pt_before_i)
    reproj_pts_j_before.append(reproj_pt_before_j)
    
    reproj_dist_i = np.linalg.norm(reproj_pt_before_i - np.array([pt_before["obs_cam_i"][0], pt_before["obs_cam_i"][1]]))
    reproj_dist_j = np.linalg.norm(reproj_pt_before_j - np.array([pt_before["obs_cam_j"][0], pt_before["obs_cam_j"][1]]))
    
    reproj_dists_i_before.append(reproj_dist_i)
    reproj_dists_j_before.append(reproj_dist_j)
    





reproj_pts_i_after = []
reproj_pts_j_after = []

reproj_dists_i_after = []
reproj_dists_j_after = []

for pt_after in data_after_solve["points_seen_by_cam_i_and_cam_j"]:
    _, reproj_pt_after_i = worldPointToImagePoint(np.array(pt_after["xyz"]), cam_i_after, pano_i_side_idx)
    _, reproj_pt_after_j = worldPointToImagePoint(np.array(pt_after["xyz"]), cam_j_after, pano_j_side_idx)
    
    reproj_pts_i_after.append(reproj_pt_after_i)
    reproj_pts_j_after.append(reproj_pt_after_j)
    
    reproj_dist_i = np.linalg.norm(reproj_pt_after_i - np.array([pt_after["obs_cam_i"][0], pt_after["obs_cam_i"][1]]))
    reproj_dist_j = np.linalg.norm(reproj_pt_after_j - np.array([pt_after["obs_cam_j"][0], pt_after["obs_cam_j"][1]]))
    
    reproj_dists_i_after.append(reproj_dist_i)
    reproj_dists_j_after.append(reproj_dist_j)
    


img_i_with_reproj_before = np.copy(img_i_with_common_kp)
img_j_with_reproj_before = np.copy(img_j_with_common_kp)

img_i_with_reproj_after = np.copy(img_i_with_common_kp)
img_j_with_reproj_after = np.copy(img_j_with_common_kp)

def draw_xs_onto_img(img, pts):
    for pt in pts:
        uv = (int(pt[0]), int(pt[1]))
        cv2.line(img, (uv[0] - cross_line_radius, uv[1] - cross_line_radius), (uv[0] + cross_line_radius, uv[1] + cross_line_radius), (0,0,255))
        cv2.line(img, (uv[0] + cross_line_radius, uv[1] - cross_line_radius), (uv[0] - cross_line_radius, uv[1] + cross_line_radius), (0,0,255))

draw_xs_onto_img(img_i_with_reproj_before, reproj_pts_i_before)
draw_xs_onto_img(img_j_with_reproj_before, reproj_pts_j_before)

draw_xs_onto_img(img_i_with_reproj_after, reproj_pts_i_after)
draw_xs_onto_img(img_j_with_reproj_after, reproj_pts_j_after)

cv2.imwrite("E:\\img_i_with_reproj_before.png", img_i_with_reproj_before)
cv2.imwrite("E:\\img_j_with_reproj_before.png", img_j_with_reproj_before)

cv2.imwrite("E:\\img_i_with_reproj_after.png", img_i_with_reproj_after)
cv2.imwrite("E:\\img_j_with_reproj_after.png", img_j_with_reproj_after)




triplet_frame_numbers_sorted = [464, 480, 2787]

# load in the holo data for the triplet

input_holoroom_only_triplet_mesh_filename = os.path.join(args.base_dir, "holoroom_only_triplet_{}_{}_{}.meshes".format(triplet_frame_numbers_sorted[0], triplet_frame_numbers_sorted[1], triplet_frame_numbers_sorted[2]))

holoroom_points = []

with open(input_holoroom_only_triplet_mesh_filename, 'rb') as sparse_triplet_data_file:
    reader = csv.reader(sparse_triplet_data_file, delimiter=' ')
    num_holoroom_only_points = int(reader.next()[0])
    for i in range(num_holoroom_only_points):
        pt = [float(x) for x in reader.next()]
        holoroom_points.append(pt)
        
holoroom_points = np.array(holoroom_points)



(frame_numbers_to_cubemap_cameras, frame_numbers_to_transformation_matrices, frame_numbers_to_inverse_transformation_matrices) = loadRefinedCubemapData(args.base_dir)





cam_i_refined = frame_numbers_to_cubemap_cameras[frame_number_i]
cam_j_refined = frame_numbers_to_cubemap_cameras[frame_number_j]

holopoints_projected_to_cam_i_local = []
holopoints_projected_to_cam_j_local = []

holopoints_projected_to_cam_i_world = []
holopoints_projected_to_cam_j_world = []

cam_i_center = cam_i_refined.GetCameraCenter()
cam_j_center = cam_j_refined.GetCameraCenter()

for holo_point in holoroom_points:
    proj_i_unit_sphere = (holo_point - cam_i_center) / np.linalg.norm(holo_point - cam_i_center)
    proj_j_unit_sphere = (holo_point - cam_j_center) / np.linalg.norm(holo_point - cam_j_center)
    
    holopoints_projected_to_cam_i_local.append( proj_i_unit_sphere   )
    holopoints_projected_to_cam_j_local.append( proj_j_unit_sphere   )
    
    holopoints_projected_to_cam_i_world.append( proj_i_unit_sphere + cam_i_center  )
    holopoints_projected_to_cam_j_world.append( proj_j_unit_sphere + cam_j_center   )
    

holopoints_projected_to_cam_i_local = np.array(holopoints_projected_to_cam_i_local)
holopoints_projected_to_cam_j_local = np.array(holopoints_projected_to_cam_j_local)
holopoints_projected_to_cam_i_world = np.array(holopoints_projected_to_cam_i_world)
holopoints_projected_to_cam_j_world = np.array(holopoints_projected_to_cam_j_world)

holopoints_on_image_plane_i = []
for unit_sphere_pt in holopoints_projected_to_cam_i_world:
    out_res, reproj_pt = worldPointToImagePoint(unit_sphere_pt, cam_i_refined, pano_i_side_idx)
    if out_res:
        holopoints_on_image_plane_i.append(reproj_pt)
    
holopoints_on_image_plane_j = []
for unit_sphere_pt in holopoints_projected_to_cam_j_world:
    out_res, reproj_pt = worldPointToImagePoint(unit_sphere_pt, cam_j_refined, pano_j_side_idx)
    if out_res:
        holopoints_on_image_plane_j.append(reproj_pt)





projected_i_hull = ConvexHull(holopoints_projected_to_cam_i_local)
projected_j_hull = ConvexHull(holopoints_projected_to_cam_j_local)



wireframe_color = (255,255,255)



mesh_point_radius = 6
wireframe_thickness = 2

test_img_i = np.copy(img_i_with_common_kp)

for simplex in projected_i_hull.simplices:
    for idx in range(3):
        start_idx = simplex[idx]
        end_idx = simplex[(idx+1) % 3]
                  
        start_vert_world = holopoints_projected_to_cam_i_world[start_idx]
        end_vert_world = holopoints_projected_to_cam_i_world[end_idx]
        
        res_start, start_proj_pt = worldPointToImagePoint(start_vert_world, cam_i_refined, pano_i_side_idx)
        res_end, end_proj_pt = worldPointToImagePoint(end_vert_world, cam_i_refined, pano_i_side_idx)
    
        if res_start and res_end:
            cv2.line(test_img_i, tuple([int(x) for x in start_proj_pt]), tuple([int(x) for x in end_proj_pt]) , (0,0,0) , wireframe_thickness+1  )
            cv2.line(test_img_i, tuple([int(x) for x in start_proj_pt]), tuple([int(x) for x in end_proj_pt]) , wireframe_color , wireframe_thickness  )

for imgpt in holopoints_on_image_plane_i:
    cv2.circle(test_img_i, (int(imgpt[0]), int(imgpt[1])), mesh_point_radius, (255,0,0), -1)


test_img_j = np.copy(img_j_with_common_kp)

for simplex in projected_j_hull.simplices:
    for idx in range(3):
        start_idx = simplex[idx]
        end_idx = simplex[(idx+1) % 3]
                  
        start_vert_world = holopoints_projected_to_cam_j_world[start_idx]
        end_vert_world = holopoints_projected_to_cam_j_world[end_idx]
        
        res_start, start_proj_pt = worldPointToImagePoint(start_vert_world, cam_j_refined, pano_j_side_idx)
        res_end, end_proj_pt = worldPointToImagePoint(end_vert_world, cam_j_refined, pano_j_side_idx)
    
        if res_start and res_end:
            cv2.line(test_img_j, tuple([int(x) for x in start_proj_pt]), tuple([int(x) for x in end_proj_pt]) , (0,0,0), wireframe_thickness+1   )
            cv2.line(test_img_j, tuple([int(x) for x in start_proj_pt]), tuple([int(x) for x in end_proj_pt]) , wireframe_color, wireframe_thickness   )

for imgpt in holopoints_on_image_plane_j:
    cv2.circle(test_img_j, (int(imgpt[0]), int(imgpt[1])), mesh_point_radius, (255,0,0), -1)

cv2.imwrite("E:\\test_img_i.png", test_img_i)
cv2.imwrite("E:\\test_img_j.png", test_img_j)


test_img_i_no_wireframe = np.copy(img_i_with_common_kp)
for imgpt in holopoints_on_image_plane_i:
    cv2.circle(test_img_i_no_wireframe, (int(imgpt[0]), int(imgpt[1])), 3, (0,255,0), -1)




test_img_j_no_wireframe = np.copy(img_j_with_common_kp)
for imgpt in holopoints_on_image_plane_j:
    cv2.circle(test_img_j_no_wireframe, (int(imgpt[0]), int(imgpt[1])), 3, (0,255,0), -1)

cv2.imwrite("E:\\test_img_i_no_wireframe.png", test_img_i_no_wireframe)
cv2.imwrite("E:\\test_img_j_no_wireframe.png", test_img_j_no_wireframe)









"""
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

ax.scatter(holopoints_projected_to_cam_j[:,0], holopoints_projected_to_cam_j[:,1], holopoints_projected_to_cam_j[:,2], c='b', marker='o')

plt.show()


"""

