# -*- coding: utf-8 -*-
"""
Created on Tue Nov 14 14:11:52 2017

@author: ANDERSED
"""

# Here we assume we already have run "transform_cubemap_aligned_nvm_to_hl_coordinate_system.py"
# This means we have generated:
# - a collection of correspondence points for each triplet, in HL world coordinate system
# - the transformed pose of all cubemap cameras, in HL world coordinate system
# The purpose of this script is to read in each of the generated triplet mesh files,
# rank the generated world triangulation in descending order of triangle area,
# and work on subdividing the triangulations further.
# Given a triangle to be subdivided, find a feature in a sphere that is inside the triangle.
# Then find a strong match that is inside the corresponding triangle for the other spheres.
# Add that point, redo the triangulation, then select the next triangle to subdivide.



import argparse
import re
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import yaml
import os
import csv
import functools
from scipy.spatial import ConvexHull
from scipy.spatial import Delaunay
import cv2
from holoscan_common import Camera, loadFullCubemapImgForFrameNumber, loadRefinedCubemapData

def saveFile(fname,data):
	fd = open(fname,"wb")
	yaml.dump(data,fd)
	fd.close()

def readFile(fname):
	fd = open(fname,"rb")
	data = yaml.load(fd)
	return data


def getTriangleArea(tri_pts):
    A = tri_pts[0]
    B = tri_pts[1]
    C = tri_pts[2]
    AB = B - A
    AC = C - A
    S = np.linalg.norm(np.cross(AB, AC)) / 2.0
    return S


def drawKeypoints(frame_number, side_idx):
    cubemap_face_img = frame_numbers_to_side_idx_to_cubemap_face_imgs[frame_number][side_idx]
    kp = frame_numbers_to_side_idx_to_kp[frame_number][side_idx]
    kpimg = cv2.drawKeypoints(cubemap_face_img, kp, None, color=(0,255,0), flags=0)
    plt.imshow(kpimg)
    plt.show()
    

def extend2DRayEndpointToLength(a,b,lenAC):
    # given 2D points A and B, compute a value C such that C is on the line defined by the segment AB, and such that segment AC has a desired length lenAC"
    lenAB = np.linalg.norm(b-a)
    c = np.zeros_like(a)
    
    c[0] = b[0] + (b[0] - a[0]) / lenAB * lenAC
    c[1] = b[1] + (b[1] - a[1]) / lenAB * lenAC
     
    return c

# given a pixel point on a particular side_idx, determine the origin and direction of the ray in world coordinates
# here we assume the cubemap is located at the origin, no rotation
# assumes that keypoints were all generated on an image that is only a single face (not the entire cubemap image)
def getKeypointOnSphereFromCubemapKeypoint(side_idx, pt):
    imgX = pt[0]
    imgY = pt[1]
    
    imgXLocal = imgX
    imgYLocal = imgY
    
    horizontalLocation = (2.0 / cube_side_pixels) * imgXLocal - 1.0
    verticalLocation = -((2.0 / cube_side_pixels) * imgYLocal - 1.0)    # flipping so positive points up instead of down
    
    if side_idx == 0:
        # front
        kpPositionUnnormalized = (horizontalLocation, verticalLocation, -1.0)
    elif side_idx == 1:
        # right
        kpPositionUnnormalized = (1.0, verticalLocation, horizontalLocation)
    elif side_idx == 2:
        # back
        kpPositionUnnormalized = (-horizontalLocation, verticalLocation, 1.0)
    elif side_idx == 3:
        # left
        kpPositionUnnormalized = (-1.0, verticalLocation, -horizontalLocation)
    elif side_idx == 4:
        # up
        kpPositionUnnormalized = (verticalLocation, 1.0, -horizontalLocation)
    elif side_idx == 5:
        # down
        kpPositionUnnormalized = (-verticalLocation, -1.0, -horizontalLocation)
    else:
        print("error: unexpected side_idx")
    
    kpPositionUnnormalized = np.array(kpPositionUnnormalized)
    kpPosition = kpPositionUnnormalized / np.linalg.norm(kpPositionUnnormalized)
    return kpPosition

def getDistancePointLine(pt, lineStart, lineEnd, constrainToSegment):
    lineMag = np.linalg.norm(lineEnd - lineStart)
    
    px = pt[0]
    py = pt[1]
    
    startx = lineStart[0]
    starty = lineStart[1]
    
    endx = lineEnd[0]
    endy = lineEnd[1]
    
    U = (((px - startx) * (endx - startx)) + ((py - starty) * (endy - starty))) / (lineMag * lineMag)
    
    if constrainToSegment:
        if U < 0.0 or U > 1.0:
            return (False, np.inf)    # closest point does not fall within the line segment
            
    intersection = np.zeros_like(pt)
    
    intersection[0] = startx + U * (endx - startx)
    intersection[1] = starty + U * (endy - starty)
    
    distance = np.linalg.norm(pt - intersection)
    
    return (True, distance)


# NOTE: returns False if the point is behind the camera!
def worldPointToImagePoint(world_point, cam, cam_side_idx):
    
    pt_local_cam_forward = np.dot(cam.m , (world_point[:3] - cam.GetCameraCenter()))
    
    cam_side_idx_transformation_matrix = side_idxs_to_transformation_matrices[cam_side_idx]
    
    pt_local_cam_side = np.dot(cam_side_idx_transformation_matrix, np.append(pt_local_cam_forward,1))
    
    if pt_local_cam_side[2] > 0:    # behind the camera
        return (False, None)
    
    fx = -cube_side_pixels / 2.0
    fy = cube_side_pixels / 2.0
    cx = cube_side_pixels / 2.0
    cy = cube_side_pixels / 2.0
    
    pt_projected_u = fx * (pt_local_cam_side[0]/pt_local_cam_side[2]) + cx
    pt_projected_v = fy * (pt_local_cam_side[1]/pt_local_cam_side[2]) + cy
    
    return (True, np.array([pt_projected_u, pt_projected_v]))
                                 
    
    

# Given a pixel point (x,y) on cubemap A with frame number "frame_number_a" and on side "side_idx_a",
# Determine the 2D line when projecting the ray onto cubemap B with frame number "frame_number_b" and on side "side_idx_b"
def getEpipolarLineFromKeypoint(frame_number_a, side_idx_a, pt_a, frame_number_b, side_idx_b, ray_length = 1.0):
    # get the coordinates of the ray (start point and end point) in the local coordinate system of cubemap A (where the cubemap center is "0,0,0").
    # This function already takes care of the question of cubemap side for cubemap A
    ray_start_local_a = np.zeros(3)
    ray_end_local_a = getKeypointOnSphereFromCubemapKeypoint(side_idx_a, pt_a) * ray_length
    
    cam_A = frame_numbers_to_cubemap_cameras[frame_number_a]
    cam_B = frame_numbers_to_cubemap_cameras[frame_number_b]
    
    ray_start_world_forward = np.dot(cam_A.m.T, ray_start_local_a) + cam_A.GetCameraCenter()
    ray_end_world_forward = np.dot(cam_A.m.T, ray_end_local_a) + cam_A.GetCameraCenter()
    
    ray_start_local_b_forward = np.dot(cam_B.m , (ray_start_world_forward[:3] - cam_B.GetCameraCenter()))
    ray_end_local_b_forward = np.dot(cam_B.m , (ray_end_world_forward[:3] - cam_B.GetCameraCenter()))
    
    
    side_idx_b_transformation_matrix = side_idxs_to_transformation_matrices[side_idx_b]
    
    ray_start_local_b_side = np.dot(side_idx_b_transformation_matrix, np.append(ray_start_local_b_forward,1))
    ray_end_local_b_side = np.dot(side_idx_b_transformation_matrix, np.append(ray_end_local_b_forward,1))
    
    if ray_end_local_b_side[2] > 0:
        ray_end_local_b_side = ray_start_local_b_side + -1.0 * (ray_end_local_b_side - ray_start_local_b_side)
    
    fx = -cube_side_pixels / 2.0
    fy = cube_side_pixels / 2.0
    cx = cube_side_pixels / 2.0
    cy = cube_side_pixels / 2.0
    
    ray_start_projected_u = fx * (ray_start_local_b_side[0]/ray_start_local_b_side[2]) + cx
    ray_start_projected_v = fy * (ray_start_local_b_side[1]/ray_start_local_b_side[2]) + cy
                                 
    ray_end_projected_u = fx * (ray_end_local_b_side[0]/ray_end_local_b_side[2]) + cx
    ray_end_projected_v = fy * (ray_end_local_b_side[1]/ray_end_local_b_side[2]) + cy
    
    return (np.array([ray_start_projected_u, ray_start_projected_v]), np.array([ray_end_projected_u, ray_end_projected_v]))
    



EPS = 0.00000001

# http://paulbourke.net/geometry/pointlineplane/lineline.c
"""
Calculate the line segment PaPb that is the shortest route between
two lines P1P2 and P3P4. Calculate also the values of mua and mub where
Pa = P1 + mua (P2 - P1)
Pb = P3 + mub (P4 - P3)
Return FALSE if no solution exists.
"""
def LineLineIntersect(p1, p2, p3, p4, debug = False):
    
    p13 = np.zeros(3)
    p43 = np.zeros(3)
    
    p13[0] = p1[0] - p3[0]
    p13[1] = p1[1] - p3[1]
    p13[2] = p1[2] - p3[2]
    p43[0] = p4[0] - p3[0]
    p43[1] = p4[1] - p3[1]
    p43[2] = p4[2] - p3[2]
    
    if debug:
        print("p43: {}".format(p43))
    
    if abs(p43[0]) < EPS and abs(p43[1]) < EPS and abs(p43[2]) < EPS:
        return (False, None, None)
    
    p21 = np.zeros(3)
    
    p21[0] = p2[0] - p1[0]
    p21[1] = p2[1] - p1[1]
    p21[2] = p2[2] - p1[2]
    if abs(p21[0]) < EPS and abs(p21[1]) < EPS and abs(p21[2]) < EPS:
        return (False, None, None)
    
    if debug:
        print("p21: {}".format(p21))

    d1343 = p13[0] * p43[0] + p13[1] * p43[1] + p13[2] * p43[2]
    d4321 = p43[0] * p21[0] + p43[1] * p21[1] + p43[2] * p21[2]
    d1321 = p13[0] * p21[0] + p13[1] * p21[1] + p13[2] * p21[2]
    d4343 = p43[0] * p43[0] + p43[1] * p43[1] + p43[2] * p43[2]
    d2121 = p21[0] * p21[0] + p21[1] * p21[1] + p21[2] * p21[2]

    denom = d2121 * d4343 - d4321 * d4321
    
    if debug:
        print("denom: {}".format(denom))
    
    if abs(denom) < EPS:
        return (False, None, None)
    
    numer = d1343 * d4321 - d1321 * d4343

    mua = numer / denom
    mub = (d1343 + d4321 * (mua)) / d4343

    pa = np.zeros(3)
    pb = np.zeros(3)
    
    pa[0] = p1[0] + mua * p21[0]
    pa[1] = p1[1] + mua * p21[1]
    pa[2] = p1[2] + mua * p21[2]
    pb[0] = p3[0] + mub * p43[0]
    pb[1] = p3[1] + mub * p43[1]
    pb[2] = p3[2] + mub * p43[2]
    
    return (True, pa, pb)








# given a potential match of pt_i and pt_j on their respective cubemaps and cube sides,
# do a 3D intersection and see if this is even a plausible correspondence
# (e.g., is it visible to both of the cubemap side frustums?)
def intersection_is_plausible(frame_number_i, pano_i_side_idx, pt_i, frame_number_j, pano_j_side_idx, pt_j):
    
    debug = False
    if frame_number_i == 3250 and pano_i_side_idx == 0 and frame_number_j == 3552 and pano_j_side_idx == 3:
        debug = True
    
    
    cam_i = frame_numbers_to_cubemap_cameras[frame_number_i]
    cam_j = frame_numbers_to_cubemap_cameras[frame_number_j]
    
    cam_i_center = cam_i.GetCameraCenter()
    cam_j_center = cam_j.GetCameraCenter()
    
    # gets the image point as a point on the unit sphere in cubemap i's local forward-facing coordinate system
    pt_i_local_i = getKeypointOnSphereFromCubemapKeypoint(pano_i_side_idx, pt_i)
    
    # gets the image point as a point on the unit sphere in cubemap j's local forward-facing coordinate system
    pt_j_local_j = getKeypointOnSphereFromCubemapKeypoint(pano_j_side_idx, pt_j)
    
    # move the points into the world coordinate system
    pt_i_world_forward = np.dot(cam_i.m.T, pt_i_local_i) + cam_i_center
    pt_j_world_forward = np.dot(cam_j.m.T, pt_j_local_j) + cam_j_center
    
    (intersect_success, pa, pb) = LineLineIntersect(cam_i_center, pt_i_world_forward, cam_j_center, pt_j_world_forward)
    

    if not intersect_success:
        return False
    
    intersect_point_world = (pa + pb) / 2.0
                                    
    project_onto_i_success, pt_projected_onto_cam_i = worldPointToImagePoint(intersect_point_world, cam_i, pano_i_side_idx)
    
    if not project_onto_i_success:
        return False
    
    if pt_projected_onto_cam_i[0] < 0 or pt_projected_onto_cam_i[1] < 0 or pt_projected_onto_cam_i[0] > cube_side_pixels or pt_projected_onto_cam_i[1] > cube_side_pixels:
        return False
    
    project_onto_j_success, pt_projected_onto_cam_j = worldPointToImagePoint(intersect_point_world, cam_j, pano_j_side_idx)
    
    if not project_onto_j_success:
        return False
    
    if pt_projected_onto_cam_j[0] < 0 or pt_projected_onto_cam_j[1] < 0 or pt_projected_onto_cam_j[0] > cube_side_pixels or pt_projected_onto_cam_j[1] > cube_side_pixels:
        return False

    
    reprojection_error_on_cam_i = np.linalg.norm(pt_i - pt_projected_onto_cam_i)
    reprojection_error_on_cam_j = np.linalg.norm(pt_j - pt_projected_onto_cam_j)
    
    pa_pb_dist = np.linalg.norm(pa-pb)


    pixel_reprojection_limit = 5
    
    if reprojection_error_on_cam_i > pixel_reprojection_limit or reprojection_error_on_cam_j > pixel_reprojection_limit:
        return False

    if debug:
        print("=== intersection_is_plausible between pt_i {} ad pt_j {}".format(pt_i, pt_j))
        print("cam_i_center: {}".format(cam_i_center))
        print("cam_j_center: {}".format(cam_j_center))
        print("pt_i_local_i: {}".format(pt_i_local_i))
        print("pt_j_local_j: {}".format(pt_j_local_j))
        print("pt_i_world_forward: {}".format(pt_i_world_forward))
        print("pt_j_world_forward: {}".format(pt_j_world_forward))
        (intersect_success, pa, pb) = LineLineIntersect(cam_i_center, pt_i_world_forward, cam_j_center, pt_j_world_forward, debug)
        print("intersect_success: {}, pa: {}, pb: {}".format(intersect_success, pa, pb))
        print("intersection was plausible! projected onto {} of cam_i and {} of cam_j".format(pt_projected_onto_cam_i, pt_projected_onto_cam_j))
        print("original pt_i: {} and pt_j: {}".format(pt_i, pt_j))
        print("reprojection_error_on_cam_i: {}".format(reprojection_error_on_cam_i))
        print("reprojection_error_on_cam_j: {}".format(reprojection_error_on_cam_j))
        print("dist between pa and pb: {}".format(pa_pb_dist))
        print("returning true")

    return True








side_idxs_to_transformation_matrices = {
        0: np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]], dtype=np.float64),
        1: np.array([[0,0,1,0],[0,1,0,0],[-1,0,0,0],[0,0,0,1]], dtype=np.float64),
        2: np.array([[-1,0,0,0],[0,1,0,0],[0,0,-1,0],[0,0,0,1]], dtype=np.float64),
        3: np.array([[0,0,-1,0],[0,1,0,0],[1,0,0,0],[0,0,0,1]], dtype=np.float64),
        4: np.array([[0,0,-1,0],[1,0,0,0],[0,-1,0,0],[0,0,0,1]], dtype=np.float64),
        5: np.array([[0,0,-1,0],[-1,0,0,0],[0,1,0,0],[0,0,0,1]], dtype=np.float64)
        }

side_idxs_to_inverse_transformation_matrices = {}
for side_idx in side_idxs_to_transformation_matrices:
    side_idxs_to_inverse_transformation_matrices[side_idx] = np.linalg.inv(side_idxs_to_transformation_matrices[side_idx])

    
    

REFINED_TRIANGULATION_FILENAME = "sphere_pose_triangulation_refined_frame_numbers.yaml"


parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)


(frame_numbers_to_cubemap_cameras, frame_numbers_to_transformation_matrices, frame_numbers_to_inverse_transformation_matrices) = loadRefinedCubemapData(args.base_dir)

brisk = cv2.BRISK_create()
fast = cv2.FastFeatureDetector_create()
orb = cv2.ORB_create(nfeatures=4000)
bf = cv2.BFMatcher()



possible_side_connections = {
        0: [0,1,2,3],
        1: [0,1,2,3],
        2: [0,1,2,3],
        3: [0,1,2,3]#,
        #4: [0,1,2,3,4],
        #5: [0,1,2,3,5]
        }


pano_idxs_to_colors = {
        0: (0,0,255), 1: (0,255,0), 2: (255,0,0)
        }

side_idxs_to_colors = {
        0: (0,0,255), 1: (0, 165, 255), 2: (0,255,255), 3: (0,255,0), 4: (255,0,0), 5: (255,0,255)
        }



refined_triangulation_data = readFile(os.path.join(args.base_dir, REFINED_TRIANGULATION_FILENAME))["triangulation_indices"]

frame_numbers_to_side_idx_to_cubemap_face_imgs = {}
frame_numbers_to_side_idx_to_kp = {}
frame_numbers_to_side_idx_to_des = {}

cube_side_pixels = None
bottom_face_mask = None


"""
def cubemap_side_img_a_callback(event, x, y, flags, param):
    if event == cv2.EVENT_MOUSEMOVE:
        pt_a = (x,y)
        (ray_a_start, ray_a_end) = getEpipolarLineFromKeypoint(test_frame_number_a, test_side_idx_a, pt_a, test_frame_number_b, test_side_idx_b)
        #print("ray_a_start: {}".format(ray_a_start))
        #print("ray_a_end: {}".format(ray_a_end))
        start_pt = np.array(ray_a_start)
        initial_end_pt = np.array(ray_a_end)
        extended_end_pt = extend2DRayEndpointToLength(start_pt, initial_end_pt, 2000.0)
        
        cubemap_side_img_b_current = np.copy(cubemap_side_img_b)
        cv2.line(cubemap_side_img_b_current, tuple([long(c) for c in start_pt]), tuple([long(c) for c in extended_end_pt]), (0,0,0), 2)
        cv2.line(cubemap_side_img_b_current, tuple([long(c) for c in start_pt]), tuple([long(c) for c in initial_end_pt]), pano_idxs_to_colors[0], 1)
        cv2.imshow("cubemap_side_img_b", cubemap_side_img_b_current)
        
    
def cubemap_side_img_b_callback(event, x, y, flags, param):
    if event == cv2.EVENT_MOUSEMOVE:
        pt_b = (x,y)
        (ray_b_start, ray_b_end) = getEpipolarLineFromKeypoint(test_frame_number_b, test_side_idx_b, pt_b, test_frame_number_a, test_side_idx_a)
        #print("ray_b_start: {}".format(ray_b_start))
        #print("ray_b_end: {}".format(ray_b_end))
        start_pt = np.array(ray_b_start)
        initial_end_pt = np.array(ray_b_end)
        extended_end_pt = extend2DRayEndpointToLength(start_pt, initial_end_pt, 2000.0)
        
        cubemap_side_img_a_current = np.copy(cubemap_side_img_a)
        cv2.line(cubemap_side_img_a_current, tuple([long(c) for c in start_pt]), tuple([long(c) for c in extended_end_pt]), (0,0,0), 2)
        cv2.line(cubemap_side_img_a_current, tuple([long(c) for c in start_pt]), tuple([long(c) for c in initial_end_pt]), pano_idxs_to_colors[1], 1)
        cv2.imshow("cubemap_side_img_a", cubemap_side_img_a_current)



test_frame_number_a = 4983
test_frame_number_b = 6495

test_side_idx_a = 0
test_side_idx_b = 2

test_cubemap_img_a = loadFullCubemapImgForFrameNumber(args.base_dir, test_frame_number_a)
test_cubemap_img_b = loadFullCubemapImgForFrameNumber(args.base_dir, test_frame_number_b)



if cube_side_pixels is None:
    cube_side_pixels = test_cubemap_img_a.shape[0]


cubemap_side_img_a = np.copy(test_cubemap_img_a[:,cube_side_pixels*test_side_idx_a : cube_side_pixels*(test_side_idx_a+1)])
cubemap_side_img_b = np.copy(test_cubemap_img_b[:,cube_side_pixels*test_side_idx_b : cube_side_pixels*(test_side_idx_b+1)])

cv2.namedWindow("cubemap_side_img_a")
cv2.namedWindow("cubemap_side_img_b")

cv2.setMouseCallback("cubemap_side_img_a", cubemap_side_img_a_callback)
cv2.setMouseCallback("cubemap_side_img_b", cubemap_side_img_b_callback)

cubemap_side_img_a_current = np.copy(cubemap_side_img_a)
cubemap_side_img_b_current = np.copy(cubemap_side_img_b)

cv2.imshow("cubemap_side_img_a", cubemap_side_img_a_current)
cv2.imshow("cubemap_side_img_b", cubemap_side_img_b_current)

while (1):
    k = cv2.waitKey(1) & 0xFF
    if k == 27:
        break

cv2.destroyWindow("cubemap_side_img_a")
cv2.destroyWindow("cubemap_side_img_b")
cv2.waitKey(1)
"""

max_correspondence_distance_meters = 30.0



output_refined_matches_dir = os.path.join(args.base_dir, "pano_frames\\cubemaps\\refined_matches")
if not os.path.exists(output_refined_matches_dir):
    os.makedirs(output_refined_matches_dir)





for triplet_frame_numbers in refined_triangulation_data:
    triplet_frame_numbers_sorted = np.copy(triplet_frame_numbers)
    triplet_frame_numbers_sorted.sort()
    print("augmenting frame numbers {}".format(triplet_frame_numbers_sorted))
    
    for frame_number in triplet_frame_numbers_sorted:
        
        if frame_number not in frame_numbers_to_side_idx_to_cubemap_face_imgs:
            frame_numbers_to_side_idx_to_cubemap_face_imgs[frame_number] = {}
            frame_numbers_to_side_idx_to_kp[frame_number] = {}
            frame_numbers_to_side_idx_to_des[frame_number] = {}
        
            cubemap_img = loadFullCubemapImgForFrameNumber(args.base_dir, frame_number)
            
            if cube_side_pixels is None:            
                cube_side_pixels = cubemap_img.shape[0]
                bottom_face_mask = np.ones([cube_side_pixels,cube_side_pixels], dtype=np.uint8) * 255
                cv2.circle(bottom_face_mask, (cube_side_pixels/2,cube_side_pixels/2), cube_side_pixels/2, 0, -1)   # mask out my head from the bottom mask
            
            
            for side_idx in range(6):
                cubemap_side_img = cubemap_img[:,cube_side_pixels*side_idx : cube_side_pixels*(side_idx+1)]
                
                frame_numbers_to_side_idx_to_cubemap_face_imgs[frame_number][side_idx] = cubemap_side_img
                                                       
                if side_idx == 5:
                    #kp, des = orb.detectAndCompute(cubemap_side_img, bottom_face_mask)
                    kp, des = brisk.detectAndCompute(cubemap_side_img, bottom_face_mask)
                else:
                    #kp, des = orb.detectAndCompute(cubemap_side_img, None)
                    kp, des = brisk.detectAndCompute(cubemap_side_img, None)
                
                frame_numbers_to_side_idx_to_kp[frame_number][side_idx] = kp
                frame_numbers_to_side_idx_to_des[frame_number][side_idx] = des
        
    
    for pano_idx_i in range(3):        
        frame_number_i = triplet_frame_numbers_sorted[pano_idx_i]        
        for pano_i_side_idx in possible_side_connections:            
            
            kp_i = frame_numbers_to_side_idx_to_kp[frame_number_i][pano_i_side_idx]
            des_i = frame_numbers_to_side_idx_to_des[frame_number_i][pano_i_side_idx]
            
            for other_pano_idx_offset in range(1,3):
                pano_idx_j = (pano_idx_i+other_pano_idx_offset) % 3
                frame_number_j = triplet_frame_numbers_sorted[pano_idx_j]        
                
                for pano_j_side_idx in possible_side_connections[pano_i_side_idx]:
                                        
                    output_match_data_filename = os.path.join(output_refined_matches_dir, "matches_{}_{}_{}_{}.yaml".format(frame_number_i, pano_i_side_idx, frame_number_j, pano_j_side_idx))
                    
                    if not os.path.isfile(output_match_data_filename):
                        
                    
                        kp_j = frame_numbers_to_side_idx_to_kp[frame_number_j][pano_j_side_idx]
                        des_j = frame_numbers_to_side_idx_to_des[frame_number_j][pano_j_side_idx]
                        
                        good_kp_i = []
                        good_kp_j = []
                        
                        print("matching keypoints between frame {} side {} and frame {} side {}...".format(frame_number_i, pano_i_side_idx, frame_number_j, pano_j_side_idx))
                        
                        if des_i is not None and des_j is not None:
                            all_matches = bf.knnMatch(des_i, des_j, k=2)
                        else:
                            all_matches = []
                    
                        good_matches = []
                        min_epipole_dist_for_good_matches = []
                        
                        for potential_matches_for_this_feature in all_matches:
                            
                            min_epipole_distance = 10
                            best_match_for_this_feature = None
                            
                            
                            for potential_match_for_this_feature in potential_matches_for_this_feature:
                                pt_i = kp_i[potential_match_for_this_feature.queryIdx].pt
                                pt_j = kp_j[potential_match_for_this_feature.trainIdx].pt
                                           
                                # find the epipolar line when putting the ray from pt_j onto the cubemap img for i
                                (ray_j_start, ray_j_end) = getEpipolarLineFromKeypoint(frame_number_j, pano_j_side_idx, pt_j, frame_number_i, pano_i_side_idx)
                                start_pt = np.array(ray_j_start)
                                initial_end_pt = np.array(ray_j_end)
                                extended_end_pt = extend2DRayEndpointToLength(start_pt, initial_end_pt, 2000.0)
                                
                                (res_ij, distance_ij) = getDistancePointLine(pt_i, start_pt, extended_end_pt, True)
                                #print("distance from epipolar line: {}".format(distance_ij))
                                
                                (ray_i_start, ray_i_end) = getEpipolarLineFromKeypoint(frame_number_i, pano_i_side_idx, pt_i, frame_number_j, pano_j_side_idx)
                                start_pt = np.array(ray_i_start)
                                initial_end_pt = np.array(ray_i_end)
                                extended_end_pt = extend2DRayEndpointToLength(start_pt, initial_end_pt, 2000.0)
                                
                                (res_ji, distance_ji) = getDistancePointLine(pt_j, start_pt, extended_end_pt, True)
                                #print("distance from epipolar line: {}".format(distance_ji))
                                
                                if intersection_is_plausible(frame_number_i, pano_i_side_idx, pt_i, frame_number_j, pano_j_side_idx, pt_j):
                                    if distance_ij < min_epipole_distance or distance_ji < min_epipole_distance:
                                        min_epipole_distance = min(distance_ij, distance_ji)
                                        best_match_for_this_feature = potential_match_for_this_feature
                                    
                            if best_match_for_this_feature is not None:
                                good_matches.append([best_match_for_this_feature])
                                min_epipole_dist_for_good_matches.append(min_epipole_distance)
                          
                        good_match_i_pts = []
                        good_match_j_pts = []
                        good_match_epipole_dists = [float(x) for x in min_epipole_dist_for_good_matches]
                        
                        for matches in good_matches:
                            match = matches[0]
                            pt_i = kp_i[match.queryIdx].pt
                            pt_j = kp_j[match.trainIdx].pt
                            good_match_i_pts.append([pt_i[0],pt_i[1]])
                            good_match_j_pts.append([pt_j[0],pt_j[1]])
                            

                        output_data = {
                                "num_good_matches": len(good_matches),
                                "frame_number_i": int(frame_number_i),
                                "pano_i_side_idx": int(pano_i_side_idx),
                                "frame_number_j": int(frame_number_j),
                                "pano_j_side_idx": int(pano_j_side_idx),
                                "good_match_i_pts": good_match_i_pts,
                                "good_match_j_pts": good_match_j_pts,
                                "good_match_epipole_dists": good_match_epipole_dists
                                }
                        
                        
                        saveFile(output_match_data_filename, output_data)
                        
                        """
                        if len(good_matches) > 0:
                            output_img = np.zeros((cube_side_pixels, cube_side_pixels*2, 3), dtype=np.uint8)
                            output_img[:,:cube_side_pixels] = np.copy(frame_numbers_to_side_idx_to_cubemap_face_imgs[frame_number_i][pano_i_side_idx])
                            output_img[:,cube_side_pixels:] = np.copy(frame_numbers_to_side_idx_to_cubemap_face_imgs[frame_number_j][pano_j_side_idx])
                            
                            cubemap_i_face_i_img_with_epipolar_lines = output_img[:,:cube_side_pixels]
                            cubemap_j_face_j_img_with_epipolar_lines = output_img[:,cube_side_pixels:]
                            
                            
                            for matches in good_matches:
                                match = matches[0]
                                
                                good_kp_i.append(kp_i[match.queryIdx])
                                good_kp_j.append(kp_j[match.trainIdx])
                                
                                pt_i = kp_i[match.queryIdx].pt
                                pt_j = kp_j[match.trainIdx].pt
                                
                                # find the epipolar line when putting the ray from pt_j onto the cubemap img for i
                                (ray_j_start, ray_j_end) = getEpipolarLineFromKeypoint(frame_number_j, pano_j_side_idx, pt_j, frame_number_i, pano_i_side_idx)
                                
                                # extend the ray end so it covers the image
                                start_pt = np.array(ray_j_start)
                                initial_end_pt = np.array(ray_j_end)
                                extended_end_pt = extend2DRayEndpointToLength(start_pt, initial_end_pt, 2000.0)
                                
                                cv2.line(cubemap_i_face_i_img_with_epipolar_lines, tuple([long(x) for x in start_pt]), tuple([long(x) for x in extended_end_pt]), pano_idxs_to_colors[pano_idx_j], 1)
                                
                                (ray_i_start, ray_i_end) = getEpipolarLineFromKeypoint(frame_number_i, pano_i_side_idx, pt_i, frame_number_j, pano_j_side_idx)
                                
                                # extend the ray end so it covers the image
                                start_pt = np.array(ray_i_start)
                                initial_end_pt = np.array(ray_i_end)
                                extended_end_pt = extend2DRayEndpointToLength(start_pt, initial_end_pt, 2000.0)
                                
                                cv2.line(cubemap_j_face_j_img_with_epipolar_lines, tuple([long(x) for x in start_pt]), tuple([long(x) for x in extended_end_pt]), pano_idxs_to_colors[pano_idx_i], 1)
                            
                            output_img[:,:cube_side_pixels] = cv2.drawKeypoints(cubemap_i_face_i_img_with_epipolar_lines, good_kp_i, None, pano_idxs_to_colors[pano_idx_i])
                            cv2.imshow("frame_i".format(frame_number_i, pano_i_side_idx), cubemap_i_face_i_img_with_epipolar_lines)
                                
                            output_img[:,cube_side_pixels:] = cv2.drawKeypoints(cubemap_j_face_j_img_with_epipolar_lines, good_kp_j, None, pano_idxs_to_colors[pano_idx_j])
                            cv2.imshow("frame_j".format(frame_number_j, pano_j_side_idx), cubemap_j_face_j_img_with_epipolar_lines)
                            
                            cv2.waitKey(1)
                            
                            output_img_filename = os.path.join(output_refined_matches_dir, "{}_{}_{}_{}.jpg".format(frame_number_i, pano_i_side_idx, frame_number_j, pano_j_side_idx))
                            cv2.imwrite(output_img_filename, output_img)
                        """
                            
                        print("processed matches between frame {} side {} and frame {} side {}, found {} matches".format(frame_number_i, pano_i_side_idx, frame_number_j, pano_j_side_idx, len(good_matches)))
                        
                
                #break
                
        
            
            #break
        #break
    
    
       
       
    
    
    
    
    
    
    
    
    
    
    
    """
    pts = []
    triangle_indices = []
    
    input_triplet_mesh_data_path = os.path.join(args.base_dir, "triplet_{}_{}_{}.meshes".format(triplet_frame_numbers_sorted[0], triplet_frame_numbers_sorted[1], triplet_frame_numbers_sorted[2]))
    with open(input_triplet_mesh_data_path, 'rb') as triplet_data_file:
        reader = csv.reader(triplet_data_file, delimiter=' ')
        num_points = int(reader.next()[0])
        for i in range(num_points):
            pt = [float(x) for x in reader.next()]
            pts.append(pt)
        num_triangles = int(reader.next()[0])        
        for i in range(num_triangles):
            tri = [int(x) for x in reader.next()]
            triangle_indices.append(tri)
            
    pts = np.array(pts)
            
    triangle_areas = []
    
    # determine the area of each triangle
    for tri in triangle_indices:
        area = getTriangleArea([pts[idx] for idx in tri])
        triangle_areas.append(area)
    
    break
    """