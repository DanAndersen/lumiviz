# -*- coding: utf-8 -*-
"""
Created on Mon Feb 19 19:21:09 2018

@author: ANDERSED
"""

import os
import csv
import numpy as np
import cv2
import matplotlib.pyplot as plt

filenames = [
r"E:\andersed_dropbox\Dropbox\lumiviz_scan_backup\user_study_scans\01_incomplete\20180209110102_pose.csv",
r"E:\andersed_dropbox\Dropbox\lumiviz_scan_backup\user_study_scans\02_incomplete\20180212134440_pose.csv",
r"E:\andersed_dropbox\Dropbox\lumiviz_scan_backup\user_study_scans\03\20180212180805_pose.csv",
r"E:\andersed_dropbox\Dropbox\lumiviz_scan_backup\user_study_scans\04\20180213164009_pose.csv",
r"E:\andersed_dropbox\Dropbox\lumiviz_scan_backup\user_study_scans\05\20180214121658_pose.csv",
r"E:\andersed_dropbox\Dropbox\lumiviz_scan_backup\user_study_scans\06\20180214161721_pose.csv",
r"E:\andersed_dropbox\Dropbox\lumiviz_scan_backup\user_study_scans\07_incomplete\20180215120308_pose.csv",
r"E:\andersed_dropbox\Dropbox\lumiviz_scan_backup\user_study_scans\08\20180215173737_pose.csv",
r"E:\andersed_dropbox\Dropbox\lumiviz_scan_backup\user_study_scans\09\20180220151103_pose.csv",
r"E:\andersed_dropbox\Dropbox\lumiviz_scan_backup\user_study_scans\10\20180222122208_pose.csv",
r"E:\andersed_dropbox\Dropbox\lumiviz_scan_backup\user_study_scans\11\20180222133102_pose.csv",
r"E:\andersed_dropbox\Dropbox\lumiviz_scan_backup\user_study_scans\12\20180222153716_pose.csv",
r"E:\andersed_dropbox\Dropbox\lumiviz_scan_backup\user_study_scans\13\20180222153948_pose.csv",
r"E:\andersed_dropbox\Dropbox\lumiviz_scan_backup\user_study_scans\14\20180222154249_pose.csv",
r"E:\andersed_dropbox\Dropbox\lumiviz_scan_backup\user_study_scans\15\20180222161203_pose.csv",
r"E:\andersed_dropbox\Dropbox\lumiviz_scan_backup\user_study_scans\16\20180223111019_pose.csv",
r"E:\andersed_dropbox\Dropbox\lumiviz_scan_backup\user_study_scans\17\20180226123849_pose.csv",
r"E:\Dev\OpenGL\lumiviz\resources\scans\20170927140341\20170927140341_pose.csv",
]

all_timestamps = {}
all_positions = {}

for i, filename in enumerate(filenames):
    
    positions = []
    timestamps = []
    
    with open(filename, 'rb') as in_file:
        reader = csv.reader(in_file, delimiter=',')
        for row in reader:
            timestamp = float(row[0])
            timestamps.append(timestamp)
            pos = np.array([float(x) for x in row[1:4]])
            positions.append(pos)
            
    positions = np.array(positions)
    timestamps = np.array(timestamps)
    
    
    plt.plot(positions[:,0],positions[:,2])

    plt.axes().set_ylim([-6,6])
    plt.axes().set_ylim([-8,8])
    #plt.axes().set_aspect('equal', 'datalim')
    #plt.show()
    
    plt.savefig("E:\\path_{}.png".format(i + 1), bbox_inches='tight')
    plt.cla()
    
    
    all_positions[i+1] = positions    
    all_timestamps[i+1] = timestamps

all_velocities = {}

all_total_distances = {}

for scan_id in all_positions:
    positions = all_positions[scan_id]
    timestamps = all_timestamps[scan_id]
    
    total_distance = 0.0
    
    velocities = np.zeros(len(positions))
    for i in range(1, len(positions)):
        dx = np.linalg.norm(positions[i] - positions[i-1])
        dt = timestamps[i] - timestamps[i - 1]
        vel = dx/dt
        
        if vel < 2.5: # noise if above this
           total_distance += dx 
           velocities[i] = vel
                  
    all_velocities[scan_id] = velocities
    all_total_distances[scan_id] = total_distance
                       
for scan_id in all_velocities:
    vels = np.array(all_velocities[scan_id])
    mean_vel = np.mean(vels)
    median_vel = np.median(vels)
    print("{}".format(mean_vel))