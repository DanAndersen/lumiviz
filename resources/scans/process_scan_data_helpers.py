# -*- coding: utf-8 -*-
"""
Created on Sat Jan 20 14:30:08 2018

@author: ANDERSED

Helper functions for process_scan_data.py.

"""

import os
import glob
import zipfile
import subprocess
from holoscan_common import saveFile, readFile, POSE_SPHERE_FILENAME, TRIANGULATION_FILENAME, REFINED_FRAME_NUMBER_TRIANGULATION_FILENAME, REFINED_POSE_SPHERE_FILENAME, SCANDATA_FILENAME
import sys
import json
import numpy as np
import csv
from pyrr import quaternion, matrix44, vector3
from pyrr import Quaternion, Matrix33, Matrix44, Vector3
from scipy.spatial import Delaunay
import struct

def get_frame_rate(filename):
    if not os.path.exists(filename):
        sys.stderr.write("ERROR: filename %r was not found!" % (filename,))
        return -1         
    out = subprocess.check_output(["ffprobe",filename,"-v","0","-select_streams","v","-print_format","flat","-show_entries","stream=r_frame_rate"])
    rate = out.split('=')[1].strip()[1:-1].split('/')
    if len(rate)==1:
        return float(rate[0])
    if len(rate)==2:
        return float(rate[0])/float(rate[1])
    return -1

def get_all_input_filenames(base_dir, scan_label):
    holoframes_zip = os.path.join(base_dir, "{}_frames.zip".format(scan_label))
    
    if not os.path.isfile(holoframes_zip):
        raise Exception("Cannot find HoloLens frames at {}".format(holoframes_zip))
    
    mesh_room = os.path.join(base_dir, "{}_mesh.room".format(scan_label))
    
    if not os.path.isfile(mesh_room):
        raise Exception("Cannot find HoloLens room mesh at {}".format(mesh_room))
        
    pose_csv = os.path.join(base_dir, "{}_pose.csv".format(scan_label))
    
    if not os.path.isfile(pose_csv):
        raise Exception("Cannot find HoloLens pose CSV at {}".format(pose_csv))
        
    panovids = glob.glob(os.path.join(base_dir, "*_Stitch_XHC.MP4"))
    
    if len(panovids) == 0:
        raise Exception("Cannot find stitched panoramic video")
        
    panovid = panovids[0]
    
    return holoframes_zip, mesh_room, pose_csv, panovid

def extract_and_flip_hololens_frames(base_dir, holoframes_zip):
    import cv2
    output_frame_dir = os.path.join(base_dir, "frames")
    
    num_existing_extracted_frames = len(glob.glob(os.path.join(output_frame_dir, "*.jpg")))
    
    if num_existing_extracted_frames == 0:
        print("Extracting HoloLens frames...")
        with zipfile.ZipFile(holoframes_zip, 'r') as zip_ref:
            zip_ref.extractall(output_frame_dir)
            
        extracted_frames = glob.glob(os.path.join(output_frame_dir, "*.jpg"))
        
        if len(extracted_frames) == 0:
            raise Exception("No HoloLens frames extracted")
        
        print("Flipping HoloLens frames...")
        for extracted_frame in extracted_frames:
            img = cv2.imread(extracted_frame)
            flipped_img = cv2.flip( img, 1 ) # flip image horizontally
            cv2.imwrite(extracted_frame, flipped_img)
            
        print("Extracted/flipped {} HoloLens frames to {}".format(len(extracted_frames), output_frame_dir))
    else:
        print("HoloLens frames already extracted, skipping")
        
def extract_panoramic_video_frames(base_dir, panovid):
    output_frame_dir = os.path.join(base_dir, "pano_frames")
    
    num_existing_extracted_frames = len(glob.glob(os.path.join(output_frame_dir, "*.jpg")))
    
    if num_existing_extracted_frames == 0:
        print("Extracting panoramic frames...")
        
        if not os.path.exists(output_frame_dir):
            os.makedirs(output_frame_dir)
        
        ffmpeg_command = ["ffmpeg","-i", panovid, "-qscale:v", "2", os.path.join(output_frame_dir, "pano_frame%05d.jpg")]
        cmd_result = subprocess.call(ffmpeg_command)
        print("Done extracting panoramic frames.")        
    else:
        print("Some panoramic frames already exist. Skipping.")
        
def setup_scan_data_file(base_dir, panovid):
    scandata_json_filename = "scandata.json"
    scandata_yaml_filename = "scandata.yaml"
    
    out_scandata_json_path = os.path.join(base_dir, scandata_json_filename)
    out_scandata_yaml_path = os.path.join(base_dir, scandata_yaml_filename)
    
    if not os.path.isfile(out_scandata_json_path) or not os.path.isfile(out_scandata_yaml_path):
        framerate = get_frame_rate(panovid)
        
        hololens_sync_frame_str = raw_input("Enter the number of the HoloLens sync frame: ")
        
        hololens_sync_frame = int(hololens_sync_frame_str)
        
        spherical_sync_frame_str = raw_input("Enter the number of the panoramic sync frame: ")
        
        spherical_sync_frame = int(spherical_sync_frame_str)
        
        grid_size_meters_str = raw_input("Enter the grid size meters (e.g. '0.5'): ")
        
        grid_size_meters = float(grid_size_meters_str)
        
        raycasting_enabled_during_matchlist_generation_str = raw_input("Do (slow) raycasting when computing matchlist? 1 = yes, 0 = no: ")
        
        raycasting_enabled_during_matchlist_generation = int(raycasting_enabled_during_matchlist_generation_str)
        
        enable_1d_path_str = raw_input("Process the 1D path for this scan? 1 = yes, 0 = no: ")
        enable_1d_path = int(enable_1d_path_str)
        
        
        start_valid_frame_number_str = raw_input("Enter the first panoframe that is valid: ")
        start_valid_frame_number = int(start_valid_frame_number_str)
        
        end_valid_frame_number_str = raw_input("Enter the last panoframe that is valid: ")
        end_valid_frame_number = int(end_valid_frame_number_str)
        
        scan_data = { 
            'hololens_sync_frame': hololens_sync_frame,
            'spherical_sync_frame': spherical_sync_frame,
            'spherical_fps': framerate,
            'grid_size_meters': grid_size_meters,
            'raycasting_enabled_during_matchlist_generation': raycasting_enabled_during_matchlist_generation,
            'enable_1d_path': enable_1d_path,
            'start_valid_frame_number': start_valid_frame_number,
            'end_valid_frame_number': end_valid_frame_number}
        
        
        with open(out_scandata_json_path, 'w') as outfile:
            json.dump(scan_data, outfile)

        saveFile(out_scandata_yaml_path, scan_data)
        
        print("Saved scan data to {} and {}.".format(out_scandata_json_path, out_scandata_yaml_path))
    else:
        print("Scan data files already exist. Skipping.")
    
    
    
# Given the input Holo-poses,use fixed estimated extrinsics to create pose_sphere.csv
def setup_sphere_pose_csv(base_dir, pose_csv):
    out_pose_sphere_path = os.path.join(base_dir, POSE_SPHERE_FILENAME)
    
    if not os.path.isfile(out_pose_sphere_path):
        default_hl_sphere_extrinsics_path = os.path.join(base_dir, "..", "default_hl_sphere_extrinsics.yaml")
        extrinsics_data = readFile(default_hl_sphere_extrinsics_path)
        
        hl_to_sphere_transform = np.eye(4)
        hl_to_sphere_transform[:3,:3] = extrinsics_data["R"]
        hl_to_sphere_transform[:3,3] = extrinsics_data["T"].reshape(-1)
        
        # now load in the hololens poses
        in_pose_csv_path = pose_csv
        holo_poses = []
        
        holo_positions = []
        sphere_positions = []
        
        out_spherical_csv_data = []
        
        with open(in_pose_csv_path, 'rb') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            for row in reader:
                timestamp = float(row[0])
                pos_x = float(row[1])
                pos_y = float(row[2])
                pos_z = float(row[3])
                rot_x = float(row[4])
                rot_y = float(row[5])
                rot_z = float(row[6])
                
                pos = np.array([pos_x, pos_y, -pos_z])  # because the pose data comes from Unity with its left-handed coords, flip this axis
                rot = np.array([-rot_x, -rot_y, rot_z]) # flip axes as needed for the rotation to be in a right-handed coord system too
                
                holo_positions.append(pos)
                              
                # rotation of the camera/hololens, representing ZXY euler angles in degrees 
                # (so z degrees about z axis, then x degrees about x axis, then y degrees about y axis, in that order)
                
                m = Matrix44.identity()
                m = Matrix44.from_z_rotation(np.radians(-rot[2])) * m
                m = Matrix44.from_x_rotation(np.radians(-rot[0])) * m
                m = Matrix44.from_y_rotation(np.radians(-rot[1])) * m
                m = Matrix44.from_translation(pos) * m
                m = m.T
                #m = ~m
                
                sphere_m = np.matmul(m, hl_to_sphere_transform)
                sphere_positions.append(sphere_m[:3,3])
                
                out_spherical_csv_data.append([timestamp,  sphere_m[0,0], sphere_m[0,1], sphere_m[0,2], sphere_m[0,3],
                                                           sphere_m[1,0], sphere_m[1,1], sphere_m[1,2], sphere_m[1,3],
                                                           sphere_m[2,0], sphere_m[2,1], sphere_m[2,2], sphere_m[2,3],
                                                           sphere_m[3,0], sphere_m[3,1], sphere_m[3,2], sphere_m[3,3]])
                
                holo_poses.append({
                        "timestamp": timestamp,
                        "pos_x": pos_x,
                        "pos_y": pos_y,
                        "pos_z": pos_z,
                        "rot_x": rot_x,
                        "rot_y": rot_y,
                        "rot_z": rot_z,
                        "mat": m
                        })
            
        holo_positions = np.array(holo_positions)
        sphere_positions = np.array(sphere_positions)
        
        # for each pose, write the (Holo) timestamp, then the transformation matrix (1D, row-major)
        with open(out_pose_sphere_path, 'wb') as csvfile:
            writer = csv.writer(csvfile, delimiter=',')
            for row in out_spherical_csv_data:
                writer.writerow(row)
            
        print("Done transforming poses, saved to {}".format(out_pose_sphere_path))
        
    else:
        print("pose_sphere.csv already exists. Skipping.")
    



# http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToAngle/index.htm
def rotMatToAngleAxis(m):
    
    angle = 0.0
    x = 0.0
    y = 0.0
    z = 0.0
    
    epsilon = 0.0001
    epsilon2 = 0.001
    
    if abs(m[0][1]-m[1][0]) < epsilon and abs(m[0][2]-m[2][0])< epsilon and abs(m[1][2]-m[2][1])< epsilon:
        # singularity found
        if abs(m[0][1]+m[1][0]) < epsilon2 and abs(m[0][2]+m[2][0]) < epsilon2 and abs(m[1][2]+m[2][1]) < epsilon2 and abs(m[0][0]+m[1][1]+m[2][2]-3) < epsilon2:
            return (0.0, np.array([1.0, 0.0, 0.0]))
        
        angle = np.pi
        xx = (m[0][0]+1)/2
        yy = (m[1][1]+1)/2
        zz = (m[2][2]+1)/2
        xy = (m[0][1]+m[1][0])/4
        xz = (m[0][2]+m[2][0])/4
        yz = (m[1][2]+m[2][1])/4
        
        if xx > yy and xx > zz: # m[0][0] is the largest diagonal term
            if xx < epsilon:
                x = 0
                y = 0.7071
                z = 0.7071
            else:
                x = np.sqrt(xx)
                y = xy/x
                z = xz/x
        elif yy > zz: # m[1][1] is the largest diagonal term
            if yy< epsilon:
                x = 0.7071
                y = 0
                z = 0.7071
            else:
                y = np.sqrt(yy)
                x = xy/y
                z = yz/y
        else: # m[2][2] is the largest diagonal term so base result on this
            if zz< epsilon:
                x = 0.7071
                y = 0.7071
                z = 0
            else:
                z = np.sqrt(zz)
                x = xz/z
                y = yz/z
			
		return (angle, np.array([x,y,z])) # return 180 deg rotation
    # no singularities, can handle normally
    s = np.sqrt((m[2][1] - m[1][2])*(m[2][1] - m[1][2]) +(m[0][2] - m[2][0])*(m[0][2] - m[2][0]) +(m[1][0] - m[0][1])*(m[1][0] - m[0][1])) # used to normalise
    if abs(s) < 0.001:
        s = 1
    angle = np.arccos(( m[0][0] + m[1][1] + m[2][2] - 1)/2)
    x = (m[2][1] - m[1][2])/s
    y = (m[0][2] - m[2][0])/s
    z = (m[1][0] - m[0][1])/s
    return (angle, np.array([x,y,z]))



    
def generate_scan_triangulation(base_dir, grid_size_meters):
    out_triangulation_path = os.path.join(base_dir, TRIANGULATION_FILENAME)
    
    if not os.path.isfile(out_triangulation_path):
        
        scanData = readFile(os.path.join(base_dir, SCANDATA_FILENAME))
        
        start_valid_frame_number = scanData['start_valid_frame_number']
        end_valid_frame_number = scanData['end_valid_frame_number']
        
        in_spherical_pose_csv_path = os.path.join(base_dir, POSE_SPHERE_FILENAME)
        
        sphere_positions = []
        sphere_pose_timestamps = []
        sphere_mats = []

        with open(in_spherical_pose_csv_path, 'rb') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            for row in reader:
                sphere_m = np.array([float(i) for i in row[1:]]).reshape((4,4))
                
                pos = sphere_m[:3,3]
                
                sphere_positions.append(pos)
                
                timestamp = float(row[0])
                sphere_mats.append(sphere_m)
                sphere_pose_timestamps.append(timestamp)
            
        sphere_positions = np.array(sphere_positions)
        sphere_mats = np.array(sphere_mats)
        sphere_pose_timestamps = np.array(sphere_pose_timestamps)
        
        def GetSphericalCameraFrameIndexFromPoseIndex(pose_index):
            locatableCameraSyncFrameIndex = scanData["hololens_sync_frame"] - 1 # go from 1-indexed to 0-indexed
            locatableCameraSyncFrameTimestamp = sphere_pose_timestamps[locatableCameraSyncFrameIndex]
            
            sphericalCameraSyncFrameIndex = scanData["spherical_sync_frame"] - 1 # go from 1-indexed to 0-indexed
            sphericalCameraFPS = scanData["spherical_fps"]
            
            locatableCameraTimestamp = sphere_pose_timestamps[pose_index]
            secondsSinceCalibrationTime = (locatableCameraTimestamp - locatableCameraSyncFrameTimestamp)
            sphericalCameraFrameIndex = int(round(sphericalCameraSyncFrameIndex + (secondsSinceCalibrationTime * sphericalCameraFPS)))
            return sphericalCameraFrameIndex
        
        # Track the velocities -- if there are any sharp discontinuities, make sure we don't choose them
        num_poses = len(sphere_pose_timestamps)
        rotational_velocities = np.zeros(num_poses) # radians per second
        
        sphere_velocities = np.zeros(len(sphere_positions))
        for i in range(1, len(sphere_positions)):
            dx = np.linalg.norm(sphere_positions[i] - sphere_positions[i-1])
            dt = sphere_pose_timestamps[i] - sphere_pose_timestamps[i - 1]
            sphere_velocities[i] = dx/dt
                             
                             
            m_prev = sphere_mats[i-1]
            m_current = sphere_mats[i]
             
            rot_m_prev = m_prev[:3,:3]
            rot_m_current = m_current[:3,:3]       
            
            (radians, axis) = rotMatToAngleAxis(np.dot(rot_m_current, np.linalg.inv(rot_m_prev)))                 
                             
            rotational_velocity = radians / dt                
            rotational_velocities[i] = rotational_velocity
        
        best_pose_distances = {}
        #best_pose_rot_vels = {}
        best_pose_indices = {}
        
        velocity_threshold = 2.5 # meters per second, if above that, it's noise
        
        rot_vel_threshold_degrees_per_second = 20.0
        
        for i, pos in enumerate(sphere_positions):
            
            fn = GetSphericalCameraFrameIndexFromPoseIndex(i)
            if fn >= start_valid_frame_number and fn <= end_valid_frame_number:
            
                prev_velocity = sphere_velocities[max(0, i-1)]
                curr_velocity = sphere_velocities[i]
                next_velocity = sphere_velocities[min(i+1, len(sphere_velocities) - 1)]
                
                if prev_velocity < velocity_threshold and curr_velocity < velocity_threshold and next_velocity < velocity_threshold:
                
                    prev_rot_vel = np.degrees(rotational_velocities[max(0, i-1)])
                    curr_rot_vel = np.degrees(rotational_velocities[i])
                    next_rot_vel = np.degrees(rotational_velocities[min(i+1, len(sphere_velocities) - 1)])
                    
                    if prev_rot_vel < rot_vel_threshold_degrees_per_second and curr_rot_vel < rot_vel_threshold_degrees_per_second and next_rot_vel < rot_vel_threshold_degrees_per_second:
                            
                        cell_id = (int(np.round(pos[0] / grid_size_meters)), int(np.round(pos[2] / grid_size_meters)))
                        
                        cell_center_position = np.array([cell_id[0] * grid_size_meters, pos[1], cell_id[1] * grid_size_meters])
                        
                        dist = np.linalg.norm(cell_center_position - pos)
                        
                        if cell_id not in best_pose_distances or dist < best_pose_distances[cell_id]:
                            best_pose_distances[cell_id] = dist
                            best_pose_indices[cell_id] = i
                              
        tri_points = []
        tri_original_indices = []
                                    
        for cell_id in best_pose_indices:
            index = best_pose_indices[cell_id]
            pos = sphere_positions[index]
            
            tri_points.append([pos[0], pos[2]])
            tri_original_indices.append(index)
            
        tri_points = np.array(tri_points)
        
        triangulation = Delaunay(tri_points)
        
        # get the triangulation in terms of the original pose indices
        tri_original_triplet_indices = []
        tri_simplices = triangulation.simplices
        for triplet in tri_simplices:
            original_indices_triplet = []
            for triplet_value in triplet:
                original_indices_triplet.append(tri_original_indices[triplet_value])
            tri_original_triplet_indices.append(original_indices_triplet)
        
        output_tri_data = {
                "grid_size_meters": grid_size_meters,
                "triangulation_indices": tri_original_triplet_indices
                }
        
        print("saving triangulation to {}".format(out_triangulation_path))
        saveFile(out_triangulation_path, output_tri_data)
        print("done saving triangulation.")
    else:
        print("Initial scan triangulation already exists. Skipping.")







def generate_refined_scan_triangulation(base_dir, grid_size_meters):        
    out_triangulation_path = os.path.join(base_dir, REFINED_FRAME_NUMBER_TRIANGULATION_FILENAME)
    
    if not os.path.isfile(out_triangulation_path):
        # load in scandata because we need it for mapping pose indices to frame numbers
        
        scanData = readFile(os.path.join(base_dir, SCANDATA_FILENAME))
        
        sphere_pose_timestamps = []
        
        with open(os.path.join(base_dir, POSE_SPHERE_FILENAME), 'rb') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            
            for row in reader:
                timestamp = float(row[0])
                sphere_pose_timestamps.append(timestamp)        
        
        def GetSphericalCameraFrameIndexFromPoseIndex(pose_index):
            locatableCameraSyncFrameIndex = scanData["hololens_sync_frame"] - 1 # go from 1-indexed to 0-indexed
            locatableCameraSyncFrameTimestamp = sphere_pose_timestamps[locatableCameraSyncFrameIndex]
            
            sphericalCameraSyncFrameIndex = scanData["spherical_sync_frame"] - 1 # go from 1-indexed to 0-indexed
            sphericalCameraFPS = scanData["spherical_fps"]
            
            locatableCameraTimestamp = sphere_pose_timestamps[pose_index]
            secondsSinceCalibrationTime = (locatableCameraTimestamp - locatableCameraSyncFrameTimestamp)
            sphericalCameraFrameIndex = int(round(sphericalCameraSyncFrameIndex + (secondsSinceCalibrationTime * sphericalCameraFPS)))
            return sphericalCameraFrameIndex
        
        # first load in the original unrefined triangulation. we only want to use these frames, not any others that might have been refined.
        unrefined_triangulation_data = readFile(os.path.join(base_dir, TRIANGULATION_FILENAME))

        unrefined_triangulation_indices = unrefined_triangulation_data["triangulation_indices"]

        frame_numbers_in_unrefined_triangulation = []

        for pose_index_triangle in unrefined_triangulation_indices:
            for pose_index_triangle_val in pose_index_triangle:
                frame_number = GetSphericalCameraFrameIndexFromPoseIndex(pose_index_triangle_val)
                if frame_number not in frame_numbers_in_unrefined_triangulation:
                    frame_numbers_in_unrefined_triangulation.append(frame_number)
        
        
        
        
        in_refined_pose_sphere_path = os.path.join(base_dir, REFINED_POSE_SPHERE_FILENAME)
        
        sphere_fns = []
        sphere_positions = []
        
        with open(in_refined_pose_sphere_path, 'rb') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            for row in reader:
                sphere_fn = int(row[0])
                if sphere_fn in frame_numbers_in_unrefined_triangulation:
                    sphere_m = np.array([float(i) for i in row[1:]]).reshape((4,4))
                    
                    pos = sphere_m[:3,3]
                    
                    sphere_positions.append(pos)
                    sphere_fns.append(sphere_fn)
            
        sphere_positions = np.array(sphere_positions)
        
        closest_pose_distances = {}
        closest_pose_frame_numbers = {}
        closest_pose_positions = {}
        
        for i, pos in enumerate(sphere_positions):
            fn = sphere_fns[i]
            cell_id = (int(np.round(pos[0] / grid_size_meters)), int(np.round(pos[2] / grid_size_meters)))
            
            cell_center_position = np.array([cell_id[0] * grid_size_meters, pos[1], cell_id[1] * grid_size_meters])
            
            dist = np.linalg.norm(cell_center_position - pos)
            
            if cell_id not in closest_pose_distances or dist < closest_pose_distances[cell_id]:
                closest_pose_distances[cell_id] = dist
                closest_pose_frame_numbers[cell_id] = fn
                closest_pose_positions[cell_id] = pos
                              
        tri_points = []
        tri_original_frame_numbers = []
                                    
        for cell_id in closest_pose_frame_numbers:
            fn = closest_pose_frame_numbers[cell_id]
            pos = closest_pose_positions[cell_id]
            
            tri_points.append([pos[0], pos[2]])
            tri_original_frame_numbers.append(fn)
            
        tri_points = np.array(tri_points)
        
        triangulation = Delaunay(tri_points)
        print(triangulation)
        
        # get the triangulation in terms of the original frame numbers
        tri_original_triplet_frame_numbers = []
        tri_simplices = triangulation.simplices
        for triplet in tri_simplices:
            original_frame_numbers_triplet = []
            for triplet_value in triplet:
                original_frame_numbers_triplet.append(tri_original_frame_numbers[triplet_value])
            tri_original_triplet_frame_numbers.append(original_frame_numbers_triplet)
        
        output_tri_data = {
                "grid_size_meters": grid_size_meters,
                "triangulation_indices": tri_original_triplet_frame_numbers
                }
        
        print("saving refined triangulation to {}".format(out_triangulation_path))
        saveFile(out_triangulation_path, output_tri_data)
        print("done saving refined triangulation.")
    else:
        print("Refined scan triangulation already exists. Skipping.")









def delete_unused_panoframes(base_dir):
    print("Deleting any unused panoframes...")
    command = ["python", "delete_unused_panoframes.py", base_dir]
    cmd_result = subprocess.call(command)
    print("Done deleting any unused panoframe.")



def select_1d_path(base_dir):
    # runs the existing select_1d_path.py script
    
    out_1d_path = os.path.join(base_dir, "selected_1d_frame_numbers.csv")
    if not os.path.isfile(out_1d_path):
        print("Selecting 1D path frames...")
        filter_scan_triangulation_command = ["python", "select_1d_path.py", base_dir]
        cmd_result = subprocess.call(filter_scan_triangulation_command)
        print("Done selecting 1D path frames.")
    else:
        print("1D path frames at {} have already been selected. Skipping.".format(out_1d_path) )
    


    
    
def generate_sphorb_match_list(base_dir, raycasting_enabled_during_matchlist_generation):
    # runs the existing filter_scan_triangulation_with_geometry.py script
    
    out_match_list_path = os.path.join(base_dir, "refinement_frame_number_matches.csv")
    if not os.path.isfile(out_match_list_path):
        print("Generating SPHORB match list...")
        filter_scan_triangulation_command = ["python", "filter_scan_triangulation_with_geometry.py", base_dir, str(raycasting_enabled_during_matchlist_generation)]
        cmd_result = subprocess.call(filter_scan_triangulation_command)
        print("Done generating SPHORB match list.")
    else:
        print("Match list input for SPHORB ({}) already exists. Skipping.".format(out_match_list_path) )
        
        
def create_sphorb_matches(base_dir):
    scan_label = os.path.split(base_dir)[-1]
    
    print("Starting SPHORB matching...")
    wd = os.getcwd()
    os.chdir("E:\\Dev\\cpp\\panorama_correspondence_finder")
    filter_scan_triangulation_command = ["./build/Release/PanoramaCorrespondenceFinder.exe", scan_label]
    cmd_result = subprocess.call(filter_scan_triangulation_command)
    os.chdir(wd)
    print("Done SPHORB matching.")





# Returns (verts, indices) from the mesh -- indices changed so everything is combined into a single mesh rather than split into multiple meshes
def load_holoroom_mesh(holoroom_file_path):
    # HoloRoom meshes are saved in the format...
    # File header: vertex count (32 bit integer), triangle count (32 bit integer)
    # Vertex list: vertex.x, vertex.y, vertex.z (all 32 bit float)
    # Triangle index list: 32 bit integers
    with open(holoroom_file_path, "rb") as f:
        combined_verts = []
        combined_indices = []
    
        vert_index_offset = 0
        while True:
            #print("loading mesh in model, vert_index_offset = {}".format(vert_index_offset))        
            start_four_bytes = f.read(4)
            if len(start_four_bytes) < 4:
                break
            else:
                vertex_count = struct.unpack('i', start_four_bytes)[0]
                triangle_count = struct.unpack('i', f.read(4))[0]
                
                for i in range(vertex_count):
                    vx = struct.unpack('f', f.read(4))[0]
                    vy = struct.unpack('f', f.read(4))[0]
                    vz = struct.unpack('f', f.read(4))[0]
                    
                    vz *= -1.0 # because Unity uses a left-handed coord system, we want to flip this axis so it is in right-handed coords
                    
                    vert = [vx,vy,vz]
                    combined_verts.append(vert)
    
                for i in range(triangle_count/3):
                    
                    indices_in_tri = []
                    
                    for j in range(3):
                        base_index = struct.unpack('i', f.read(4))[0]
                        offset_index = base_index + vert_index_offset
                        indices_in_tri.append(offset_index)
                    
                    combined_indices.append(indices_in_tri[0])
                    combined_indices.append(indices_in_tri[2])  # switching order of indices in triangle 
                    combined_indices.append(indices_in_tri[1])
                
                
                vert_index_offset += vertex_count
                
        combined_verts = np.array(combined_verts).reshape(-1,3)
        combined_indices = np.array(combined_indices, dtype=np.int64).reshape(-1,3)
    return (combined_verts, combined_indices)

