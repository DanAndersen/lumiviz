# -*- coding: utf-8 -*-
"""
Created on Thu Nov 16 16:02:44 2017

@author: ANDERSED
"""

import numpy as np
import os
import csv
import yaml
import math
import scipy.spatial.distance

# the initial triangulation (described with pose indices) before any cubemap bundle adjustment of poses
TRIANGULATION_FILENAME = "sphere_pose_triangulation.yaml"

# data describing the synchronization frame and the FPS of the cameras
SCANDATA_FILENAME = "scandata.yaml"

# initial unrefined list of poses of the sphere (estimated by transforming the initial HL poses with a fixed extrinsic value)
POSE_SPHERE_FILENAME = "pose_sphere.csv"

# the refined triangulation (described with frame numbers) after the bundle adjustment is done
REFINED_FRAME_NUMBER_TRIANGULATION_FILENAME = "sphere_pose_triangulation_refined_frame_numbers.yaml"

# refined list of poses. Data is in the format...
# frame_number, m11, m12, m13, m14, m21, ..., m44
REFINED_POSE_SPHERE_FILENAME = "fn_to_pose_refined.csv"

EPS = 0.00000001

# http://paulbourke.net/geometry/pointlineplane/lineline.c
"""
Calculate the line segment PaPb that is the shortest route between
two lines P1P2 and P3P4. Calculate also the values of mua and mub where
Pa = P1 + mua (P2 - P1)
Pb = P3 + mub (P4 - P3)
Return FALSE if no solution exists.
"""
def LineLineIntersect(p1, p2, p3, p4, debug = False):
    
    p13 = np.zeros(3)
    p43 = np.zeros(3)
    
    p13[0] = p1[0] - p3[0]
    p13[1] = p1[1] - p3[1]
    p13[2] = p1[2] - p3[2]
    p43[0] = p4[0] - p3[0]
    p43[1] = p4[1] - p3[1]
    p43[2] = p4[2] - p3[2]
    
    if debug:
        print("p43: {}".format(p43))
    
    if abs(p43[0]) < EPS and abs(p43[1]) < EPS and abs(p43[2]) < EPS:
        return (False, None, None)
    
    p21 = np.zeros(3)
    
    p21[0] = p2[0] - p1[0]
    p21[1] = p2[1] - p1[1]
    p21[2] = p2[2] - p1[2]
    if abs(p21[0]) < EPS and abs(p21[1]) < EPS and abs(p21[2]) < EPS:
        return (False, None, None)
    
    if debug:
        print("p21: {}".format(p21))

    d1343 = p13[0] * p43[0] + p13[1] * p43[1] + p13[2] * p43[2]
    d4321 = p43[0] * p21[0] + p43[1] * p21[1] + p43[2] * p21[2]
    d1321 = p13[0] * p21[0] + p13[1] * p21[1] + p13[2] * p21[2]
    d4343 = p43[0] * p43[0] + p43[1] * p43[1] + p43[2] * p43[2]
    d2121 = p21[0] * p21[0] + p21[1] * p21[1] + p21[2] * p21[2]

    denom = d2121 * d4343 - d4321 * d4321
    
    if debug:
        print("denom: {}".format(denom))
    
    if abs(denom) < EPS:
        return (False, None, None)
    
    numer = d1343 * d4321 - d1321 * d4343

    mua = numer / denom
    mub = (d1343 + d4321 * (mua)) / d4343

    pa = np.zeros(3)
    pb = np.zeros(3)
    
    pa[0] = p1[0] + mua * p21[0]
    pa[1] = p1[1] + mua * p21[1]
    pa[2] = p1[2] + mua * p21[2]
    pb[0] = p3[0] + mub * p43[0]
    pb[1] = p3[1] + mub * p43[1]
    pb[2] = p3[2] + mub * p43[2]
    
    return (True, pa, pb)











def random_point_sphere(num_points = 1):
    theta = np.random.random(num_points) * math.pi * 2.0
    phi = np.arccos(2.0 * np.random.random(num_points) - 1.0)
    #radius = pow(np.random.random(num_points), 1.0 / 3.0)
    radius = 1.0
    x = np.cos(theta) * np.sin(phi) * radius
    y = np.sin(theta) * np.sin(phi) * radius
    z = np.cos(phi) * radius
    return np.dstack((x,y,z))[0]

# if we only compare it doesn't matter if it's squared
def min_dist_squared(points, point):
    diff = points - np.array([point])
    return np.min(np.einsum('ij,ij->i',diff,diff))

class PoissonGenerator:
    def __init__(self):
        self.disk = True
        self.num_dim = 3
        self.repeatPattern = False
        self.num_perms = (3 ** self.num_dim) if self.repeatPattern else 1

        self.zero_point = [0,0,0]
        self.random_point = random_point_sphere
        
        self.best_dist = np.inf

    def first_point(self):
        return self.random_point(1)[0]

    def find_next_point(self, current_points, iterations_per_point):
        best_dist = 0
        best_point = []
        random_points = self.random_point(iterations_per_point)
        for new_point in random_points:
            dist = min_dist_squared(current_points, new_point)
            if dist > best_dist:
                best_dist = dist
                best_point = new_point
        self.best_dist = best_dist
        return best_point

    def permute_point(self, point):
        out_array = np.array(point,ndmin = 2)
        if not self.repeatPattern:
            return out_array

        if self.num_dim == 3:
            for z in range(-1,2):
                for y in range(-1,2):
                    for x in range(-1,2):
                        if y != 0 or x != 0 or z != 0:
                            perm_point = point+[x,y,z]
                            out_array = np.append(out_array, np.array(perm_point,ndmin = 2), axis = 0 )
        elif self.num_dim == 2:            
            for y in range(-1,2):
                for x in range(-1,2):
                    if y != 0 or x != 0:
                        perm_point = point+[x,y]
                        out_array = np.append(out_array, np.array(perm_point,ndmin = 2), axis = 0 )
        else:
            for x in range(-1,2):
                if x != 0:
                    perm_point = point+[x]
                    out_array = np.append(out_array, np.array(perm_point,ndmin = 2), axis = 0 )

        return out_array

    def find_point_set(self, num_points, num_iter, iterations_per_point, rotations, progress_notification = None):
        best_point_set = []
        best_dist_avg = 0
        self.rotations = 1
        if self.disk and self.num_dim == 2:
            rotations = max(rotations, 1)
            self.rotations = rotations

        for i in range(num_iter):
            if progress_notification != None:
                progress_notification(i / num_iter)
            points = self.permute_point(self.first_point())

            for i in range(num_points-1):
                next_point = self.find_next_point(points, iterations_per_point)
                points = np.append(points, self.permute_point(next_point), axis = 0)

            current_set_dist = 0

            if rotations > 1:
                points_permuted = np.copy(points)
                for rotation in range(1, rotations):
                    rot_angle = rotation * math.pi * 2.0 / rotations
                    s, c = math.sin(rot_angle), math.cos(rot_angle)
                    rot_matrix = np.matrix([[c, -s], [s, c]])
                    points_permuted = np.append(points_permuted, np.array(np.dot(points, rot_matrix)), axis = 0)
                current_set_dist = np.min(scipy.spatial.distance.pdist(points_permuted))
            else:
                current_set_dist = np.min(scipy.spatial.distance.pdist(points))

            if current_set_dist > best_dist_avg:
                best_dist_avg = current_set_dist
                best_point_set = points
        return best_point_set[::self.num_perms,:]

    def cache_sort(self, points, sorting_buckets):
        if sorting_buckets < 1:
            return points
        if self.num_dim == 3:
            points_discretized = np.floor(points * [sorting_buckets,-sorting_buckets, sorting_buckets])
            indices_cache_space = np.array(points_discretized[:,2] * sorting_buckets * 4 + points_discretized[:,1] * sorting_buckets * 2 + points_discretized[:,0])
            points = points[np.argsort(indices_cache_space)]
        elif self.num_dim == 2:
            points_discretized = np.floor(points * [sorting_buckets,-sorting_buckets])
            indices_cache_space = np.array(points_discretized[:,1] * sorting_buckets * 2 + points_discretized[:,0])
            points = points[np.argsort(indices_cache_space)]        
        else:
            points_discretized = np.floor(points * [sorting_buckets])
            indices_cache_space = np.array(points_discretized[:,0])
            points = points[np.argsort(indices_cache_space)]
        return points





















# array form of 3x3 matrix of (yprime * y^T)
def getRowForSolver(y, yprime):
    return np.outer(yprime, y).reshape(-1)

# given a list of matches (each match is a pair of 3D homogeneous coordinates),
# return the best-fit essential matrix E that maps between the points in the matches
def solve_essential_matrix(matches_homogeneous):
    # http://ece631web.groups.et.byu.net/Lectures/ECEn631%2013%20-%208%20Point%20Algorithm.pdf    
    M_matrix = None
    for match_homogeneous in matches_homogeneous:
        y = match_homogeneous[0]
        yprime = match_homogeneous[1]        
        row = getRowForSolver(y, yprime)        
        if M_matrix is None:
            M_matrix = row
        else:
            M_matrix = np.vstack((M_matrix, row))            
    u, s, vH = np.linalg.svd(M_matrix)
    v = vH.T
    E_est = v[:,8].reshape(3,3)    
    u_eest, s_eest, vH_eest = np.linalg.svd(E_est)    
    s1 = s_eest[0]
    s2 = s_eest[1]
    s_eest_prime = np.array([[(s1+s2)/2.0, 0, 0], [0, (s1+s2)/2.0, 0], [0,0,0]])
    E_prime = np.matmul(np.matmul(u_eest, s_eest_prime), vH_eest)
    return E_prime

# the projected distance epsilon_p as described in Pagini et al.
def rayDistanceToEpipolarPlane(x, xprime, E):
    e_x = np.dot(E, x)
    
    xprime_norm = np.linalg.norm(xprime)
    e_x_norm = np.linalg.norm(e_x)
    
    epsilon_p = ( np.abs(np.dot(xprime.T, e_x)) ) / ( xprime_norm * e_x_norm )
    
    return epsilon_p

def toHomogeneousCoords(xyz):
    return xyz * 1.0/xyz[2] # yes, we do have to actually convert to homogeneous coordinates
    #return xyz

def xyzUnitSphereToSphereImgPoint(unit_xyz, pano_frame_resolution):
    # reverses the function "sphereImgPointToXYZUnitSphere"
    w = pano_frame_resolution[0]
    h = pano_frame_resolution[1]
    
    x = unit_xyz[0]
    y = unit_xyz[1]
    z = unit_xyz[2]
    
    lat_radians = np.arcsin(y)
    
    projected_onto_xz_plane = np.array([x, 0.0, z])
    projected_onto_xz_plane = projected_onto_xz_plane / np.linalg.norm(projected_onto_xz_plane)
    
    lon_radians = np.arctan2(-projected_onto_xz_plane[0], -projected_onto_xz_plane[2])
    
    img_x = (lon_radians - np.pi) / (-2.0 * np.pi / w)
    img_y = (lat_radians - (np.pi/2.0)) / (-np.pi / h)
    
    img_x = img_x % w
    
    return np.array([img_x, img_y])
    

def sphereImgPointToXYZUnitSphereMultiple(img_xs, img_ys, pano_frame_resolution):
    w = pano_frame_resolution[0]
    h = pano_frame_resolution[1]
    
    lon_radians = (-2.0 * np.pi / w) * img_xs + np.pi
    lat_radians = (-np.pi / h) * img_ys + (np.pi/2.0)
    
    ys = np.sin(lat_radians)
    xs = - np.cos(lat_radians) * np.sin(lon_radians)
    zs = - np.cos(lat_radians) * np.cos(lon_radians)
    
    return np.column_stack((xs,ys,zs))

def sphereImgPointToXYZUnitSphere(img_x, img_y, pano_frame_resolution):
    # returns an XYZ point on the unit sphere, where -z is center, +y is north pole
    
    w = pano_frame_resolution[0]
    h = pano_frame_resolution[1]
    
    lon_radians = (-2.0 * np.pi / w) * img_x + np.pi
    lat_radians = (-np.pi / h) * img_y + (np.pi/2.0)

    y = np.sin(lat_radians)
    
    x = - np.cos(lat_radians) * np.sin(lon_radians)
    z = - np.cos(lat_radians) * np.cos(lon_radians)
    
    return np.array([x,y,z])

def saveFile(fname,data):
	fd = open(fname,"wb")
	yaml.dump(data,fd)
	fd.close()

def readFile(fname):
	fd = open(fname,"rb")
	data = yaml.load(fd)
	return data

class Camera:
    def __init__(self):
        self.name = "unknown"
        self.frame_number = -1
        self.side_idx = -1
        self.m = np.identity(3)
        self.t = np.zeros(3)

    def SetQuaternionRotation(self, q):
        qq = np.sqrt(q[0]*q[0]+q[1]*q[1]+q[2]*q[2]+q[3]*q[3])
        if qq>0:
            qw=q[0]/qq
            qx=q[1]/qq
            qy=q[2]/qq
            qz=q[3]/qq
        else:
            qw = 1
            qx = qy = qz = 0
        
        self.m[0][0]=float(qw*qw + qx*qx- qz*qz- qy*qy )
        self.m[0][1]=float(2*qx*qy -2*qz*qw )
        self.m[0][2]=float(2*qy*qw + 2*qz*qx)
        self.m[1][0]=float(2*qx*qy+ 2*qw*qz)
        self.m[1][1]=float(qy*qy+ qw*qw - qz*qz- qx*qx)
        self.m[1][2]=float(2*qz*qy- 2*qx*qw)
        self.m[2][0]=float(2*qx*qz- 2*qy*qw)
        self.m[2][1]=float(2*qy*qz + 2*qw*qx )
        self.m[2][2]=float(qz*qz+ qw*qw- qy*qy- qx*qx)
        
        #self.m = self.m.transpose()
        
    def SetCameraCenterAfterRotation(self, c):
        # t = - R * C
        for j in range(3):
            self.t[j] = -float(self.m[j][0] * c[0] + self.m[j][1] * c[1] + self.m[j][2] * c[2])

    def GetCameraCenter(self):
        c = np.zeros(3)
        # C = - R' * t
        for j in range(3):
            c[j] = -float(self.m[0][j]* self.t[0] + self.m[1][j] * self.t[1] + self.m[2][j] * self.t[2])
        return c

    def GetQuaternionRotation(self):
        q = [0.0] * 4
        q[0]= 1 + self.m[0][0] + self.m[1][1] + self.m[2][2]
        if q[0]>0.000000001:
            q[0] = np.sqrt(q[0])/2.0
            q[1]= (self.m[2][1] - self.m[1][2])/( 4.0 *q[0])
            q[2]= (self.m[0][2] - self.m[2][0])/( 4.0 *q[0])
            q[3]= (self.m[1][0] - self.m[0][1])/( 4.0 *q[0])
        else:
            if self.m[0][0] > self.m[1][1] and self.m[0][0] > self.m[2][2]:
                s = 2.0 * np.sqrt( 1.0 + self.m[0][0] - self.m[1][1] - self.m[2][2])
                q[1] = 0.25 * s
                q[2] = (self.m[0][1] + self.m[1][0] ) / s
                q[3] = (self.m[0][2] + self.m[2][0] ) / s
                q[0] = (self.m[1][2] - self.m[2][1] ) / s    
            elif self.m[1][1] > self.m[2][2]:
                s = 2.0 * np.sqrt( 1.0 + self.m[1][1] - self.m[0][0] - self.m[2][2])
                q[1] = (self.m[0][1] + self.m[1][0] ) / s
                q[2] = 0.25 * s
                q[3] = (self.m[1][2] + self.m[2][1] ) / s
                q[0] = (self.m[0][2] - self.m[2][0] ) / s    
            else:
                s = 2.0 * np.sqrt( 1.0 + self.m[2][2] - self.m[0][0] - self.m[1][1] )
                q[1] = (self.m[0][2] + self.m[2][0] ) / s
                q[2] = (self.m[1][2] + self.m[2][1] ) / s
                q[3] = 0.25 * s
                q[0] = (self.m[0][1] - self.m[1][0] ) / s
            
        return q

# 1-indexed frame number!
def loadFullCubemapImgForFrameNumber(base_dir, frame_number):
    import cv2
    cubemap_img_path = os.path.join(base_dir, "pano_frames\\cubemaps\\pano_frame%05d_cubemap.jpg" % (frame_number,))
    cubemap_img = cv2.imread(cubemap_img_path)
    return cubemap_img


# given a list of triplets (where triplets represent frame numbers, pose indices, etc),
# create an adjacency map
def generateAdjacencyMap(triangulation_list):
    # for each entry, list all the entries that are adjacent to it in the triangulation
    adjacency_map = {}
    
    for triplet in triangulation_list:
        
        for i in range(3):
            j = (i + 1) % 3
            k = (i - 1) % 3
            
            triplet_val_i = triplet[i]
            triplet_val_j = triplet[j]
            triplet_val_k = triplet[k]
            
            if triplet_val_i not in adjacency_map:
                adjacency_map[triplet_val_i] = set()
            
            adjacency_map[triplet_val_i].add(triplet_val_j)
            adjacency_map[triplet_val_i].add(triplet_val_k)
    return adjacency_map


def writeOutputTripletMeshData(base_dir, filename, unprojected_pts, simplices):
    output_triplet_mesh_data_path = os.path.join(base_dir, filename)
    print("writing triplet data to " + output_triplet_mesh_data_path)
    with open(output_triplet_mesh_data_path, 'wb') as triplet_data_file:
        writer = csv.writer(triplet_data_file, delimiter=' ')
        writer.writerow([len(unprojected_pts)]) # num points
        for unprojected_pt in unprojected_pts:
            writer.writerow(list(unprojected_pt))
        writer.writerow([len(simplices)])   # num triangles
        for triangle in simplices:
            writer.writerow(list(triangle))


# given an adjacency map of values (e.g. frame numbers), generates a collection of mappings between a value and any other value that is within two hops
# mappings are only (lower_value --> higher_value), so redundant mappings in the other direction are not listed
def getMappingsWithinTwoSteps(adjacency_map):
    selected_mappings = {}
    
    for val_a in adjacency_map:
        if val_a not in selected_mappings:
            selected_mappings[val_a] = set()
        
        for val_b in adjacency_map[val_a]:
            if val_a < val_b:
                selected_mappings[val_a].add(val_b)
                            
            for val_c in adjacency_map[val_b]:
                if val_c != val_a and val_a < val_c:
                    selected_mappings[val_a].add(val_c)
    return selected_mappings

def GetPointsProjectedOntoTranslatedUnitSphere(world_pts, center_point):
    
    world_pts_minus_center_point = world_pts - center_point
    world_pts_minus_center_point = world_pts_minus_center_point / np.linalg.norm(world_pts_minus_center_point, axis=1, keepdims=True)
    
    projected_pts = center_point + world_pts_minus_center_point
    
    return projected_pts

def loadRefinedTriangulationData(base_dir):
    REFINED_TRIANGULATION_FILENAME = "sphere_pose_triangulation_refined_frame_numbers.yaml"
    return readFile(os.path.join(base_dir, REFINED_TRIANGULATION_FILENAME))["triangulation_indices"]

def loadRefinedCubemapData(base_dir):
    REFINED_CUBEMAP_DATA_FILENAME = "refined_cubemap_data.csv"
    frame_numbers_to_cubemap_cameras = {}   # 1-indexed
    
    frame_numbers_to_transformation_matrices = {}   # represents the [R t] matrix that goes from world-space to local-camera-space
    frame_numbers_to_inverse_transformation_matrices = {}   # represents the [R t]^-1 matrix that goes from local-camera-space to world-space
    
    refined_cubemap_data_path = os.path.join(base_dir, REFINED_CUBEMAP_DATA_FILENAME)
    
    with open(refined_cubemap_data_path, 'rb') as cubemap_data_file:
        reader = csv.reader(cubemap_data_file, delimiter=' ')
        for row in reader:
            frame_number = int(row[0])
            cam_center = [float(x) for x in row[1:4]]
            cam_quat = [float(x) for x in row[4:8]]
            
            cubemap_cam = Camera()
            cubemap_cam.SetQuaternionRotation(cam_quat)
            cubemap_cam.SetCameraCenterAfterRotation(cam_center)
            
            frame_numbers_to_cubemap_cameras[frame_number] = cubemap_cam
    
            transformation_matrix = np.identity(4)
            transformation_matrix[:3,:3] = cubemap_cam.m
            transformation_matrix[:3,3] = cubemap_cam.t
    
            frame_numbers_to_transformation_matrices[frame_number] = transformation_matrix
            frame_numbers_to_inverse_transformation_matrices[frame_number] = np.linalg.inv(transformation_matrix)
    
    return (frame_numbers_to_cubemap_cameras, frame_numbers_to_transformation_matrices, frame_numbers_to_transformation_matrices)


def fixSimplicesNeighbors(unfixed_simplices, unfixed_neighbors, world_points_projected_to_centroid_sphere, centroid_of_hull):
    fixed_simplices = []
    fixed_neighbors = []
    
    projected_pts = world_points_projected_to_centroid_sphere
    
    projection_center = centroid_of_hull
    
    for i in range(len(unfixed_simplices)):
        unfixed_simplex = unfixed_simplices[i]
        
        unfixed_neighbor = unfixed_neighbors[i]
        pts_in_triangle = projected_pts[unfixed_simplex]
        a = pts_in_triangle[0]
        b = pts_in_triangle[1]
        c = pts_in_triangle[2]
        
        normal = np.cross(b-a, c-a)
        normal = normal / np.linalg.norm(normal)
        
        is_facing_center = (np.dot(normal, a-projection_center)) > 0
        if is_facing_center:
            fixed_simplices.append(unfixed_simplex)
            fixed_neighbors.append(unfixed_neighbor)
        else:
            fixed_simplex = np.fliplr([unfixed_simplex])[0]
            fixed_simplices.append( fixed_simplex )
            fixed_neighbor = np.fliplr([unfixed_neighbor])[0]
            fixed_neighbors.append(fixed_neighbor)
    fixed_simplices = np.array(fixed_simplices)
    fixed_neighbors = np.array(fixed_neighbors)
    return (fixed_simplices, fixed_neighbors)