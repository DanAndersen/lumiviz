# -*- coding: utf-8 -*-
"""
Created on Sun Oct 01 15:55:47 2017

@author: ANDERSED
"""

# creates a YAML file for the user to fill in basic data about the scan.

import yaml
import argparse
import sys
import os
import subprocess
import glob

SCANDATA_FILENAME = "scandata.yaml"

def get_frame_rate(filename):
    if not os.path.exists(filename):
        sys.stderr.write("ERROR: filename %r was not found!" % (filename,))
        return -1         
    out = subprocess.check_output(["ffprobe",filename,"-v","0","-select_streams","v","-print_format","flat","-show_entries","stream=r_frame_rate"])
    rate = out.split('=')[1].strip()[1:-1].split('/')
    if len(rate)==1:
        return float(rate[0])
    if len(rate)==2:
        return float(rate[0])/float(rate[1])
    return -1

def saveFile(fname,data):
	fd = open(fname,"wb")
	yaml.dump(data,fd)
	fd.close()

def readFile(fname):
	fd = open(fname,"rb")
	data = yaml.load(fd)
	return data





parser = argparse.ArgumentParser()
parser.add_argument("scan_dir", help="base directory for the specific scan")
args = parser.parse_args()


spherical_video_filenames = glob.glob(os.path.join(args.scan_dir, "*Stitch*"))
if len(spherical_video_filenames) == 0:
    print("ERROR: no spherical video found in {}".format(args.scan_dir))

spherical_video_filename = spherical_video_filenames[0]

framerate = get_frame_rate(spherical_video_filename)



scan_data = { 
        'hololens_sync_frame': 0,
        'spherical_sync_frame': 0,
        'spherical_fps': framerate}


output_filename = os.path.join(args.scan_dir, SCANDATA_FILENAME)

print("saving scan data to {}...".format(output_filename))

saveFile(output_filename, scan_data)

print("saved. NOTE: You must open the file and manually edit the values before proceeding!")
