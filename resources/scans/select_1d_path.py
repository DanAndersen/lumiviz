# -*- coding: utf-8 -*-
"""
Created on Tue Feb 06 16:53:31 2018

@author: ANDERSED

Given an input scan, determine the frames to choose that form a continuous 1D sequence.

We should choose them so they are more or less evenly spaced in distance (not in time).

We should also choose them to exclude frames that are likely to have a lot of rotational velocity (blur).

"""

import argparse
import os
import process_scan_data_helpers
from holoscan_common import readFile, sphereImgPointToXYZUnitSphere, LineLineIntersect, GetPointsProjectedOntoTranslatedUnitSphere, writeOutputTripletMeshData, saveFile, TRIANGULATION_FILENAME
import csv
import numpy as np
import cv2
import glob
import matplotlib.pyplot as plt
from scipy.spatial import ConvexHull
import random
import Queue
import profile
from scipy.spatial import cKDTree

# =============================================================================

os.chdir("E:\\Dev\\OpenGL\\lumiviz\\resources\\scans")

parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)

print("Base scan directory: {}".format(args.base_dir))

SCAN_LABEL = os.path.split(args.base_dir)[-1]

print("SCAN_LABEL: {}".format(SCAN_LABEL))

# =============================================================================

in_spherical_pose_csv_path = os.path.join(args.base_dir, "pose_sphere.csv")


print("Loading in spherical pose data from {}".format(in_spherical_pose_csv_path))


# Indexed by pose index
sphere_pose_timestamps = []
sphere_positions = []
sphere_mats = []

with open(in_spherical_pose_csv_path, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        timestamp = float(row[0])
        
        sphere_m = np.array([float(i) for i in row[1:]]).reshape((4,4))
        
        pos = sphere_m[:3,3]
        
        sphere_positions.append(pos)
        sphere_mats.append(sphere_m)
        sphere_pose_timestamps.append(timestamp)
    
sphere_positions = np.array(sphere_positions)
sphere_mats = np.array(sphere_mats)
sphere_pose_timestamps = np.array(sphere_pose_timestamps)

num_poses = len(sphere_pose_timestamps)

# =============================================================================

SCANDATA_FILENAME = "scandata.yaml"
scanData = readFile(os.path.join(args.base_dir, SCANDATA_FILENAME))


enable_1d_path = (scanData['enable_1d_path'] == 1)


# =============================================================================

def GetSphericalCameraFrameIndexFromPoseIndex(pose_index):
    locatableCameraSyncFrameIndex = scanData["hololens_sync_frame"] - 1 # go from 1-indexed to 0-indexed
    locatableCameraSyncFrameTimestamp = sphere_pose_timestamps[locatableCameraSyncFrameIndex]
    
    sphericalCameraSyncFrameIndex = scanData["spherical_sync_frame"] - 1 # go from 1-indexed to 0-indexed
    sphericalCameraFPS = scanData["spherical_fps"]
    
    locatableCameraTimestamp = sphere_pose_timestamps[pose_index]
    secondsSinceCalibrationTime = (locatableCameraTimestamp - locatableCameraSyncFrameTimestamp)
    sphericalCameraFrameIndex = int(round(sphericalCameraSyncFrameIndex + (secondsSinceCalibrationTime * sphericalCameraFPS)))
    return sphericalCameraFrameIndex

# =============================================================================


# http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToAngle/index.htm
def rotMatToAngleAxis(m):
    
    angle = 0.0
    x = 0.0
    y = 0.0
    z = 0.0
    
    epsilon = 0.0001
    epsilon2 = 0.001
    
    if abs(m[0][1]-m[1][0]) < epsilon and abs(m[0][2]-m[2][0])< epsilon and abs(m[1][2]-m[2][1])< epsilon:
        # singularity found
        if abs(m[0][1]+m[1][0]) < epsilon2 and abs(m[0][2]+m[2][0]) < epsilon2 and abs(m[1][2]+m[2][1]) < epsilon2 and abs(m[0][0]+m[1][1]+m[2][2]-3) < epsilon2:
            return (0.0, np.array([1.0, 0.0, 0.0]))
        
        angle = np.pi
        xx = (m[0][0]+1)/2
        yy = (m[1][1]+1)/2
        zz = (m[2][2]+1)/2
        xy = (m[0][1]+m[1][0])/4
        xz = (m[0][2]+m[2][0])/4
        yz = (m[1][2]+m[2][1])/4
        
        if xx > yy and xx > zz: # m[0][0] is the largest diagonal term
            if xx < epsilon:
                x = 0
                y = 0.7071
                z = 0.7071
            else:
                x = np.sqrt(xx)
                y = xy/x
                z = xz/x
        elif yy > zz: # m[1][1] is the largest diagonal term
            if yy< epsilon:
                x = 0.7071
                y = 0
                z = 0.7071
            else:
                y = np.sqrt(yy)
                x = xy/y
                z = yz/y
        else: # m[2][2] is the largest diagonal term so base result on this
            if zz< epsilon:
                x = 0.7071
                y = 0.7071
                z = 0
            else:
                z = np.sqrt(zz)
                x = xz/z
                y = yz/z
			
		return (angle, np.array([x,y,z])) # return 180 deg rotation
    # no singularities, can handle normally
    s = np.sqrt((m[2][1] - m[1][2])*(m[2][1] - m[1][2]) +(m[0][2] - m[2][0])*(m[0][2] - m[2][0]) +(m[1][0] - m[0][1])*(m[1][0] - m[0][1])) # used to normalise
    if abs(s) < 0.001:
        s = 1
    angle = np.arccos(( m[0][0] + m[1][1] + m[2][2] - 1)/2)
    x = (m[2][1] - m[1][2])/s
    y = (m[0][2] - m[2][0])/s
    z = (m[1][0] - m[0][1])/s
    return (angle, np.array([x,y,z]))


# =============================================================================

# Compute the rotational and translational velocities.

rotational_velocities = np.zeros(num_poses) # radians per second
translational_velocities = np.zeros(num_poses) # meters per second

accumulated_distances = np.zeros(num_poses)
                                   
total_meters_traveled = 0.0
                                   
for i in range(num_poses):
    if i > 0:
        m_prev = sphere_mats[i-1]
        m_current = sphere_mats[i]
        
        rot_m_prev = m_prev[:3,:3]
        rot_m_current = m_current[:3,:3]
        
        pos_prev = sphere_positions[i-1]
        pos_current = sphere_positions[i]
        
        timestamp_prev = sphere_pose_timestamps[i-1]
        timestamp_current = sphere_pose_timestamps[i]
        
        elapsed_time = timestamp_current - timestamp_prev # seconds
        
        translation_dist = np.linalg.norm(pos_current - pos_prev)
        
        total_meters_traveled += translation_dist
        accumulated_distances[i] = total_meters_traveled
        
        translational_velocity = translation_dist / elapsed_time
        translational_velocities[i] = translational_velocity
                                
        (radians, axis) = rotMatToAngleAxis(np.dot(rot_m_current, np.linalg.inv(rot_m_prev)))
        
        rotational_velocity = radians / elapsed_time
        rotational_velocities[i] = rotational_velocity
                             
#plt.plot(translational_velocities)
#plt.plot(rotational_velocities)

print("TODO: replace hardcoded values with ones selected from individual scans")
distance_spacing = 0.25 # distance to space out the 1D path in meters

# we allow for +/- (1/max_adjustment_factor) percent of the distance spacing to move them around
# for example: if it's 4, then we can search +/- 25% distancewise
max_adjustment_factor = 4

distance_spaced_pose_indices = [] # sequential list of pose indices such that each consecutive pose index is as equally spaced out in distance as possible

adjustment_distance_spaced_pose_indices = []
                               
next_accumulated_distance = 0.0
next_accumulated_adjustment_distance = 0.0
for i in range(num_poses):
    if accumulated_distances[i] >= next_accumulated_distance:
        next_accumulated_distance += distance_spacing
        distance_spaced_pose_indices.append(i)
    if accumulated_distances[i] >= next_accumulated_adjustment_distance:
        next_accumulated_adjustment_distance += (distance_spacing/max_adjustment_factor)
        adjustment_distance_spaced_pose_indices.append(i)
    


    
spaced_stable_pose_indices = []
# now that we have evenly spaced positions, wiggle them forward/backward a bit to try to move away from any large rotation velocities
# we allow for +/- 25% of the distance spacing to move them around
for i in range(len(distance_spaced_pose_indices)):
    center_pidx = distance_spaced_pose_indices[i]
    center_accumulated_distance = i*distance_spacing
    min_accumulated_distance = center_accumulated_distance - (distance_spacing/max_adjustment_factor)
    max_accumulated_distance = center_accumulated_distance + (distance_spacing/max_adjustment_factor)
    
    min_pidx_to_check = adjustment_distance_spaced_pose_indices[max(0, i*max_adjustment_factor - 1)]
    max_pidx_to_check = adjustment_distance_spaced_pose_indices[min(len(adjustment_distance_spaced_pose_indices)-1, i*max_adjustment_factor + 1)]
    
    #print("for center pidx {}, can check pidxs between {} and {}".format(center_pidx, min_pidx_to_check, max_pidx_to_check))
    
    pidx_in_range_with_min_rot_vel = min_pidx_to_check + np.argmin(rotational_velocities[min_pidx_to_check:max_pidx_to_check])
    spaced_stable_pose_indices.append(pidx_in_range_with_min_rot_vel)



# =============================================================================

# Convert the selected pose indices to frame numbers.

spaced_stable_frame_numbers = [GetSphericalCameraFrameIndexFromPoseIndex(_pidx) for _pidx in spaced_stable_pose_indices]

# If any of the frame numbers are negative, then they are invalid selections.
# Remove them from the list of selected pose indices and selected frame numbers.
for first_valid_selection_idx in range(len(spaced_stable_frame_numbers)):
    if spaced_stable_frame_numbers[first_valid_selection_idx] > 0:
        break
    
# the value of first_valid_selection_idx is the first index of the spaced_stable lists that is a valid frame.
spaced_stable_pose_indices = spaced_stable_pose_indices[first_valid_selection_idx:]
spaced_stable_frame_numbers = spaced_stable_frame_numbers[first_valid_selection_idx:]

# =============================================================================


"""
# Graph the rotational velocities of the chosen points -- after adjustment should be strictly lower than before adjustment
rot_vels_before_adjustment = rotational_velocities[distance_spaced_pose_indices]
rot_vels_after_adjustment = rotational_velocities[spaced_stable_pose_indices]
deg_rot_vels_before_adjustment = np.degrees(rot_vels_before_adjustment)
deg_rot_vels_after_adjustment = np.degrees(rot_vels_after_adjustment)

after = True

fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_xlabel("Path frame index")
ax.set_ylabel("Rotational velocity (degrees per second)")
if after:
    ax.set_title("After rotational velocity reduction")
    ax.plot(deg_rot_vels_after_adjustment)
else:
    ax.set_title("Before rotational velocity reduction")
    ax.plot(deg_rot_vels_before_adjustment)
ax.set_ylim([0,60])
plt.show()
"""



spaced_positions = np.zeros((len(distance_spaced_pose_indices), 3))
for i in range(len(distance_spaced_pose_indices)):
    pidx = distance_spaced_pose_indices[i]
    spaced_positions[i] = sphere_positions[pidx]
    
spaced_stable_positions = np.zeros((len(spaced_stable_pose_indices), 3))
for i in range(len(spaced_stable_pose_indices)):
    pidx = spaced_stable_pose_indices[i]
    spaced_stable_positions[i] = sphere_positions[pidx]

"""
plt.plot(spaced_positions[:,0], spaced_positions[:,2])
plt.scatter(spaced_positions[:,0], spaced_positions[:,2])

plt.plot(spaced_stable_positions[:,0], spaced_stable_positions[:,2])
plt.scatter(spaced_stable_positions[:,0], spaced_stable_positions[:,2])

plt.plot(sphere_positions[:,0], sphere_positions[:,2])
"""


# =============================================================================

# Output the selected pose indices.

out_selected_1d_pose_indices_path = os.path.join(args.base_dir, "selected_1d_pose_indices.csv")

with open(out_selected_1d_pose_indices_path, 'wb') as out_file:
    writer = csv.writer(out_file, delimiter=',')
    
    for pidx in spaced_stable_pose_indices:
        writer.writerow([pidx])
    
print("saved pose indices to {}".format(out_selected_1d_pose_indices_path))

# =============================================================================

# Also convert the selected pose indices to frame numbers and output that list too.

out_selected_1d_frame_numbers_path = os.path.join(args.base_dir, "selected_1d_frame_numbers.csv")

with open(out_selected_1d_frame_numbers_path, 'wb') as out_file:
    writer = csv.writer(out_file, delimiter=',')
    
    for fn in spaced_stable_frame_numbers:
        writer.writerow([fn])
        
print("saved frame numbers to {}".format(out_selected_1d_frame_numbers_path))

# =============================================================================

# Generate the pairwise matches we want for the 1D path.
# For frame number i, we want a pair (i, i+1)

selected_1d_pairwise_matches = []

if enable_1d_path:

    # =============================================================================
    
    for i in range(len(spaced_stable_frame_numbers) - 1):
        selected_1d_pairwise_matches.append([spaced_stable_frame_numbers[i], spaced_stable_frame_numbers[i+1]])
    
    # =============================================================================
    
    # We also want matches connecting each frame in the 1D path to at least one frame that is only in the 2D triangulation.
    # Load in the raw, unrefined triangulation. The numbers in this are pose indices, not frame numbers
    triangulation_data = readFile(os.path.join(args.base_dir, TRIANGULATION_FILENAME))
    
    triangulation_indices = triangulation_data["triangulation_indices"]
    
    unique_triangulation_pose_indices = []
    for pose_index_triangle in triangulation_indices:
        for pose_index_triangle_val in pose_index_triangle:        
            if pose_index_triangle_val not in unique_triangulation_pose_indices:
                unique_triangulation_pose_indices.append(pose_index_triangle_val)
    
    # Find the pose indices that are only in the 2D triangulation and are not in the 1D path at all
    pose_indices_only_in_triangulation = [x for x in unique_triangulation_pose_indices if x not in spaced_stable_pose_indices]
    pose_indices_only_in_triangulation = sorted(pose_indices_only_in_triangulation)
    
    positions_only_in_triangulation = np.array([sphere_positions[pidx] for pidx in pose_indices_only_in_triangulation])
    
    # Make a kD tree to find the nearest neighbor among the indices only in the triangulation
    trionly_pidx_tree = cKDTree(positions_only_in_triangulation)
    
    
    # Now go through each of the 1D pose indices and find the nearest neighbord among the indices only in the triangulation
    for pidx_1D in spaced_stable_pose_indices:
        position_1D = sphere_positions[pidx_1D]
        nn_dist, nn_treeidx = trionly_pidx_tree.query(position_1D)
        nn_pidx_2D = pose_indices_only_in_triangulation[nn_treeidx]
    
        fn_1D = GetSphericalCameraFrameIndexFromPoseIndex(pidx_1D)
        nn_fn_2D = GetSphericalCameraFrameIndexFromPoseIndex(nn_pidx_2D)
        
        new_match = sorted([fn_1D, nn_fn_2D])
        
        selected_1d_pairwise_matches.append(new_match)
    
    # =============================================================================
    
    # Now read in the current match list from the file and determine which of the new matches are not already on the list.
    # We want to avoid adding duplicates if we can.
    
    existing_matches_in_list = []
    match_list_csv = os.path.join(args.base_dir, "refinement_frame_number_matches.csv")
    with open(match_list_csv, 'rb') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        
        for row in reader:
            existing_matches_in_list.append(sorted([int(fn) for fn in row]))
    
    
    matches_to_add = [x for x in selected_1d_pairwise_matches if x not in existing_matches_in_list]
    print("we have {} additional matches to add".format(len(matches_to_add)))
    
    # Append these matches onto our list of sphorb matches to make.
    
    with open(match_list_csv, 'ab') as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        
        for missing_match in matches_to_add:
            fn_i = missing_match[0]
            fn_j = missing_match[1]        
            writer.writerow([fn_i, fn_j])
            
    print("appended additional 1D pairwise matches to {}".format(match_list_csv))
else:
    print("'enable_1d_path' is disabled for this scan. Not adding these matches to the list of matches for this scan.")
