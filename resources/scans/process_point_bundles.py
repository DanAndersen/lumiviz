# -*- coding: utf-8 -*-
"""
Created on Mon Jan 29 18:52:26 2018

@author: ANDERSED

Given the "point_bundles.csv" file, where each row is a list of all observations of what should be the same real-world point,
(in the format frame_number,img_x,img_y,...)
determine the 3D position of these points by 3D ray intersections.

"""

import numpy as np
import cv2
import argparse
import os
from holoscan_common import POSE_SPHERE_FILENAME, readFile, TRIANGULATION_FILENAME, SCANDATA_FILENAME, solve_essential_matrix, sphereImgPointToXYZUnitSphere, rayDistanceToEpipolarPlane, LineLineIntersect, xyzUnitSphereToSphereImgPoint, sphereImgPointToXYZUnitSphere
import csv
import scipy
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import glob
import re

from scipy.sparse import lil_matrix

import time
from scipy.optimize import least_squares

# =============================================================================

parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)

scanData = readFile(os.path.join(args.base_dir, SCANDATA_FILENAME))

pano_frame_dir = os.path.join(args.base_dir, "pano_frames")

# Determine size of panoramas.
pano_frame_resolution = cv2.imread(glob.glob(os.path.join(pano_frame_dir, "pano_frame*.jpg"))[0]).shape[::-1][1:]

# =============================================================================

# Load the refined poses for each frame number.

fn_to_pose_refined_path = os.path.join(args.base_dir, "fn_to_pose_refined.csv")

if not os.path.isfile(fn_to_pose_refined_path):
    raise Exception("Missing refined poses at {}".format(fn_to_pose_refined_path))

fns_to_poses = {}   # contains matrix [R|t] where [R|t] * X_local = X_world

with open(fn_to_pose_refined_path, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        fn = int(row[0]) # 0-indexed
        mat = np.array([float(x) for x in row[1:]]).reshape((4,4))
        fns_to_poses[fn] = mat
                    
# =============================================================================

# line intersection point of lines in 3D space in the least squares sense
def lineIntersect3D(PA, PB):
    # PA: Nx3 matrix containing starting point of N lines
    # PB: Nx3 matrix containing end point of N lines
    
    Si = PB - PA # N lines described as vectors
    ni = Si / np.linalg.norm(Si, axis=1, keepdims=True) # normalize vectors
    nx = ni[:,0]
    ny = ni[:,1]
    nz = ni[:,2]
    
    SXX = np.sum(nx*nx - 1)
    SYY = np.sum(ny*ny - 1)
    SZZ = np.sum(nz*nz - 1)
    SXY = np.sum(nx*ny)
    SXZ = np.sum(nx*nz)
    SYZ = np.sum(ny*nz)
    S = np.array([[SXX, SXY, SXZ], [SXY, SYY, SYZ], [SXZ, SYZ, SZZ]])
    CX = np.sum(PA[:,0] * (nx*nx - 1) + PA[:,1] * (nx*ny) + PA[:,2] * (nx*nz))
    CY = np.sum(PA[:,0] * (nx*ny) + PA[:,1] * (ny*ny - 1) + PA[:,2] * (ny*nz))
    CZ = np.sum(PA[:,0] * (nx*nz) + PA[:,1] * (ny*nz) + PA[:,2] * (nz*nz - 1))
    C = np.array([[CX], [CY], [CZ]])
    P_intersect = np.linalg.solve(S, C)
    
    return P_intersect

# =============================================================================

points = []

in_point_bundles_path = os.path.join(args.base_dir, "point_bundles.csv")

with open(in_point_bundles_path, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        print("processing bundle {}".format(len(points)))
        num_observations = len(row) / 3
        
        if num_observations > 10:
                              
            PA = []
            PB = []
        
            fns_in_this_observation = set()
        
            for i in range(num_observations):
                frame_number = int(row[3*i + 0])
                
                if frame_number not in fns_in_this_observation:
                
                    img_x = float(row[3*i + 1])
                    img_y = float(row[3*i + 2])
                    
                    R = fns_to_poses[frame_number][:3,:3]
                    t = fns_to_poses[frame_number][:3,3]
                    
                    pa_world = t
                    
                    pb_local = sphereImgPointToXYZUnitSphere(img_x, img_y, pano_frame_resolution)
                    
                    pb_world = np.dot(R, pb_local) + t
                                    
                    PA.append(pa_world)
                    PB.append(pb_world)
                    
                    fns_in_this_observation.add(frame_number)
            
            PA = np.array(PA)
            PB = np.array(PB)
            
            P_intersect = lineIntersect3D(PA, PB)
            
            points.append(P_intersect)
            
            """
            if num_observations > 10:
            
                print("PA")
                print(PA)
                print("PB")
                print(PB)
                print("P_intersect")
                print(P_intersect)
                
                fig = plt.figure()
                ax = fig.add_subplot(111, projection='3d')
                
                for i in range(num_observations):
                    ax.plot(
                            np.array([PA[i][0],PB[i][0]]), 
                            np.array([PA[i][1],PB[i][1]]), 
                            np.array([PA[i][2],PB[i][2]]))
                    
                ax.scatter(P_intersect[0], P_intersect[1], P_intersect[2])
                    
                plt.show()
                
                break
                """
    
points = np.array(points).reshape(-1,3)

decimation = 1

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

ax.scatter(points[::decimation,0], points[::decimation,1], points[::decimation,2])
    
plt.show()