import numpy as np
import cv2
import os
import glob
import matplotlib.pyplot as plt
import sys
import argparse
import ntpath
import yaml
import csv


SPHERE_CORNER_FILENAME = "spherical_corner_data.yaml"
HOLOLENS_CORNER_FILENAME = "hololens_corner_data.yaml"

SCANDATA_FILENAME = "scandata.yaml"



# when working with square chessboards, we might need to rotate the found corners 
# so they are all consistent. We define the 'ground truth' here as:
# ---------/
# /--------
# --------->
# where corners[0] should be in the top left, go horizontal before going vertical
def makeCornersConsistent(corners, chessboard_dims):
    x = chessboard_dims[0]
    y = chessboard_dims[1]
    
    while True:
        presumed_top_left = corners[0][0]
        presumed_top_right = corners[x - 1][0]
        presumed_bottom_left = corners[(x-1)*(y)][0]
        presumed_bottom_right = corners[(x*y)-1][0]
        
        in_proper_rotation = (presumed_top_left[0] < presumed_top_right[0] and presumed_top_left[1] < presumed_bottom_left[1] and presumed_top_left[0] < presumed_bottom_right[0] and presumed_top_left[1] < presumed_bottom_right[1])
        if in_proper_rotation:
            break
        
        cornersgrid = corners.reshape(x, y, 1, 2)
        
        rotatedcornersgrid = np.rot90(cornersgrid)
        
        corners = rotatedcornersgrid.reshape(corners.shape)
    return corners
    
    


def saveFile(fname,data):
	fd = open(fname,"wb")
	yaml.dump(data,fd)
	fd.close()

def readFile(fname):
	fd = open(fname,"rb")
	data = yaml.load(fd)
	return data



parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")
parser.add_argument("chess_x", type=int, help="x dimension of chessboard")
parser.add_argument("chess_y", type=int, help="y dimension of chessboard")
parser.add_argument("chess_size", type=float, help="size of each chessboard square in meters")
parser.add_argument("--spherical", action="store_true")

args = parser.parse_args()

print(args)

print("Finding chessboard corners...")

# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

chessboard_dims = (args.chess_x, args.chess_y)

# if spherical condition, we use this to determine the start/end frames to find chessboards in
sphere_first_chessboard_frame = None
sphere_last_chessboard_frame = None

if args.spherical:
    print("spherical frames, determining start/end frames based on HoloLens data")
    frames_dir = os.path.join(args.base_dir, "pano_frames\\cubemaps")
    
    # we must also use the previously generated hololens corner data to 
    # automatically determine the range of input 360 frames we should be looking at.
    
    # load in the hololens corner data to find out the first/last HL frames that had chessboards in them
    hololens_frame_numbers_with_chessboards = []
    hololens_in_corner_data = readFile(os.path.join(args.base_dir, HOLOLENS_CORNER_FILENAME))
    for hololens_frame_name in hololens_in_corner_data["corners"]:
        hl_frame_number = int(hololens_frame_name.replace("frame","").replace(".jpg",""))
        hololens_frame_numbers_with_chessboards.append(hl_frame_number)
    
    first_hl_chess_frame = min(hololens_frame_numbers_with_chessboards)
    last_hl_chess_frame = max(hololens_frame_numbers_with_chessboards)
    
    # next, load in the scandata YAML to figure out what frame is the "sync" frame
    scandata = readFile(os.path.join(args.base_dir, SCANDATA_FILENAME))
    
    # now read in the HL pose data from the CSV file to find the HL timestamps at our start/end/sync points
    hololens_pose_csv_path = glob.glob(os.path.join(args.base_dir, "*_pose.csv"))[0]
    holo_pose_timestamps = []
    with open(hololens_pose_csv_path, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            holo_pose_timestamp = float(row[0])
            holo_pose_timestamps.append(holo_pose_timestamp)
    
    # NOTE: frame images are 1-indexed while the poses are 0-indexed (so poses[0] corresponds to frame00001.jpg)
    hl_timestamp_at_sync_frame = holo_pose_timestamps[scandata["hololens_sync_frame"] - 1]
    hl_timestamp_at_first_chessboard = holo_pose_timestamps[first_hl_chess_frame - 1]
    hl_timestamp_at_last_chessboard = holo_pose_timestamps[last_hl_chess_frame - 1]
    
    # find the timestamps... relative to the sync point
    first_chessboard_seconds_since_sync = hl_timestamp_at_first_chessboard - hl_timestamp_at_sync_frame
    last_chessboard_seconds_since_sync = hl_timestamp_at_last_chessboard - hl_timestamp_at_sync_frame
    
    # given the 360 framerate (and the 360 sync frame), find the start and end 360 frames that cover the chessboard
    sphere_fps = scandata["spherical_fps"]
    sphere_sync_frame = scandata["spherical_sync_frame"]
    sphere_first_chessboard_frame = int(np.floor(sphere_sync_frame + (first_chessboard_seconds_since_sync * sphere_fps)))
    sphere_last_chessboard_frame = int(np.ceil(sphere_sync_frame + (last_chessboard_seconds_since_sync * sphere_fps)))
    print("will only be reading in spherical frames between {} and {}".format(sphere_first_chessboard_frame, sphere_last_chessboard_frame))
    
else:
    frames_dir = os.path.join(args.base_dir, "frames")

frame_search_path = os.path.join(frames_dir, "*.jpg")

in_frames = glob.glob(frame_search_path)

out_dir = os.path.join(frames_dir, "drawn_chessboards")
if not os.path.exists(out_dir):
    os.makedirs(out_dir)


corner_data = { 
        "chessboard_dims": chessboard_dims,
        "chessboard_size": args.chess_size,
        "corners": {
                }
        }

for in_frame in in_frames:
    basename = ntpath.basename(in_frame)
    
    if args.spherical:
        # decide if we should skip or not based on the sphere_first_chessboard_frame and sphere_last_chessboard_frame
        sphere_frame_number = int(basename.replace("pano_frame","").replace("_cubemap.jpg",""))
        if sphere_frame_number < sphere_first_chessboard_frame or sphere_frame_number > sphere_last_chessboard_frame:
            continue
    
    print("reading in {}".format(basename))
    in_img = cv2.imread(in_frame)
    
    if args.spherical:
        # resize the image we're working with to only the first square image of the cubemap (= front image)
        in_img_height = int(in_img.shape[0])
        in_img = in_img[:in_img_height, :in_img_height]
        
        
        # try sharpening the image
        #kernel = np.array([[-0.5,-0.5,-0.5], [-0.5,5,-0.5], [-0.5,-0.5,-0.5]])
        #in_img = cv2.filter2D(in_img, -1, kernel)
    
    gray = cv2.cvtColor(in_img,cv2.COLOR_BGR2GRAY)
    
    # Find the chess board corners
    ret, corners = cv2.findChessboardCorners(gray, chessboard_dims,cv2.CALIB_CB_FAST_CHECK | cv2.CALIB_CB_ADAPTIVE_THRESH | cv2.CALIB_CB_NORMALIZE_IMAGE)
    
    if ret:        
        #corners2 = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)
        corners2 = cv2.cornerSubPix(gray,corners,(5,5),(-1,-1),criteria) # reducing the window size from 11x11 to 5x5 seems to help w/ spherical images: https://joelgranados.com/2011/02/07/about-opencv-cornersubpix/
        #corners2 = corners
        
        corners2 = makeCornersConsistent(corners2, chessboard_dims)
        
        img_copy = in_img.copy()
        img_drawn = cv2.drawChessboardCorners(img_copy, chessboard_dims, corners2,ret)
        cv2.imshow('img',img_drawn)
        cv2.waitKey(1)
        
        out_drawn_path = os.path.join(out_dir, basename)
        cv2.imwrite(out_drawn_path, img_drawn)
        
        corner_data["corners"][basename] = corners2
        

cv2.destroyAllWindows()

if args.spherical:
    corner_data_out_filename = os.path.join(args.base_dir, SPHERE_CORNER_FILENAME)
else:
    corner_data_out_filename = os.path.join(args.base_dir, HOLOLENS_CORNER_FILENAME)

print("saving corner data to {}".format(corner_data_out_filename))
saveFile(corner_data_out_filename, corner_data)
print("done reading in frames.")