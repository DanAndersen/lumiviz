# -*- coding: utf-8 -*-
"""
Created on Sun Nov 19 16:29:08 2017

@author: ANDERSED
"""

import argparse
import re
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import matplotlib.colors as colors
import yaml
import os
import csv
import functools
from scipy.spatial import ConvexHull
from scipy.spatial import Delaunay
import cv2
from holoscan_common import Camera, loadFullCubemapImgForFrameNumber, loadRefinedCubemapData, saveFile, readFile, GetPointsProjectedOntoTranslatedUnitSphere, writeOutputTripletMeshData

REFINED_TRIANGULATION_FILENAME = "sphere_pose_triangulation_refined_frame_numbers.yaml"


side_idxs_to_transformation_matrices = {
        0: np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]], dtype=np.float64),
        1: np.array([[0,0,1,0],[0,1,0,0],[-1,0,0,0],[0,0,0,1]], dtype=np.float64),
        2: np.array([[-1,0,0,0],[0,1,0,0],[0,0,-1,0],[0,0,0,1]], dtype=np.float64),
        3: np.array([[0,0,-1,0],[0,1,0,0],[1,0,0,0],[0,0,0,1]], dtype=np.float64),
        4: np.array([[0,0,-1,0],[1,0,0,0],[0,-1,0,0],[0,0,0,1]], dtype=np.float64),
        5: np.array([[0,0,-1,0],[-1,0,0,0],[0,1,0,0],[0,0,0,1]], dtype=np.float64)
        }

side_idxs_to_inverse_transformation_matrices = {}
for side_idx in side_idxs_to_transformation_matrices:
    side_idxs_to_inverse_transformation_matrices[side_idx] = np.linalg.inv(side_idxs_to_transformation_matrices[side_idx])

    

def ptsLocalFrontToWorld(cam, pts):
    if len(pts.shape) == 1:
        pts = np.array([pts])
    return np.dot(cam.m.T, pts[:,:3].T).T + cam.GetCameraCenter()

def ptsLocalSideToFront(side_idx, pts):
    side_idx_inverse_transformation_matrix = side_idxs_to_inverse_transformation_matrices[side_idx]
    return np.dot(side_idx_inverse_transformation_matrix, pts.T).T

def scatter_pts(ax, pts, c, marker):
    ax.scatter(pts[:,0], pts[:,1], pts[:,2], c=c, marker=marker)


def plot_mesh(ax, pts, simplices, colorarray):
    for simplex in simplices:
        xs = pts[simplex, 0]
        ys = pts[simplex, 1]
        zs = pts[simplex, 2]
        tri = Poly3DCollection([zip(xs, ys, zs)])
        tri.set_alpha(0.5)
        
        tri.set_facecolor(colorarray)
        
        ax.add_collection3d(tri)

frame_numbers_to_side_idxs_to_culling_planes = {}


def ptsLocalSideToWorld(cam, side_idx, pts):
    pts_local_front = ptsLocalSideToFront(side_idx, pts)
    pts_world = ptsLocalFrontToWorld(cam, pts_local_front)
    return pts_world

def GetCullingPlanesForCubemapSide(frame_number, side_idx):
    if frame_number not in frame_numbers_to_side_idxs_to_culling_planes:
        frame_numbers_to_side_idxs_to_culling_planes[frame_number] = {}
        
    if side_idx not in frame_numbers_to_side_idxs_to_culling_planes[frame_number]:
        cam = frame_numbers_to_cubemap_cameras[frame_number]
        
        near_z = 1.0
        far_z = 15.0
        
        nbl_local = np.array([-near_z,-near_z,-near_z, 1.0])
        nbr_local = np.array([near_z,-near_z,-near_z, 1.0])
        ntl_local = np.array([-near_z,near_z,-near_z, 1.0])
        ntr_local = np.array([near_z,near_z,-near_z, 1.0])
        
        fbl_local = np.array([-far_z,-far_z,-far_z, 1.0])
        fbr_local = np.array([far_z,-far_z,-far_z, 1.0])
        ftl_local = np.array([-far_z,far_z,-far_z, 1.0])
        ftr_local = np.array([far_z,far_z,-far_z, 1.0])
        
        nbl_world = ptsLocalSideToWorld(cam, side_idx, np.array([nbl_local]))
        nbr_world = ptsLocalSideToWorld(cam, side_idx, np.array([nbr_local]))
        ntl_world = ptsLocalSideToWorld(cam, side_idx, np.array([ntl_local]))
        ntr_world = ptsLocalSideToWorld(cam, side_idx, np.array([ntr_local]))
        
        fbl_world = ptsLocalSideToWorld(cam, side_idx, np.array([fbl_local]))
        fbr_world = ptsLocalSideToWorld(cam, side_idx, np.array([fbr_local]))
        ftl_world = ptsLocalSideToWorld(cam, side_idx, np.array([ftl_local]))
        ftr_world = ptsLocalSideToWorld(cam, side_idx, np.array([ftr_local]))
        
        culling_planes = []
        culling_planes.append([nbr_world, fbr_world, ftr_world])    # right plane
        culling_planes.append([ntr_world, ftr_world, ftl_world])    # top plane
        culling_planes.append([ntl_world, ftl_world, fbl_world])    # left plane
        culling_planes.append([nbl_world, fbl_world, fbr_world])    # bottom_plane
        culling_planes.append([nbl_world, nbr_world, ntr_world])    # near plane
        culling_planes.append([fbl_world, ftl_world, ftr_world])    # far plane
        
        frame_numbers_to_side_idxs_to_culling_planes[frame_number][side_idx] = culling_planes
    return frame_numbers_to_side_idxs_to_culling_planes[frame_number][side_idx]
        
        

def worldPointVisible(world_pt, frame_number, side_idx):
    culling_planes = GetCullingPlanesForCubemapSide(frame_number, side_idx)
    
    for culling_plane in culling_planes:
        A = culling_plane[0]
        B = culling_plane[1]
        C = culling_plane[2]
        
        BA = B - A
        CA = C - A
        
        norm = np.cross(BA, CA)
        norm /= np.linalg.norm(norm)
        
        PA = world_pt - A
        PA /= np.linalg.norm(PA)
        
        PA = PA[0]
        norm = norm[0]
        
        #print("norm: {}".format(norm))
        #print("PA: {}".format(PA))
        
        if np.dot(PA, norm) > 0:
            return False
    
    return True



EPS = 0.00000001

# http://paulbourke.net/geometry/pointlineplane/lineline.c
"""
Calculate the line segment PaPb that is the shortest route between
two lines P1P2 and P3P4. Calculate also the values of mua and mub where
Pa = P1 + mua (P2 - P1)
Pb = P3 + mub (P4 - P3)
Return FALSE if no solution exists.
"""
def LineLineIntersect(p1, p2, p3, p4, debug = False):
    
    p13 = np.zeros(3)
    p43 = np.zeros(3)
    
    p13[0] = p1[0] - p3[0]
    p13[1] = p1[1] - p3[1]
    p13[2] = p1[2] - p3[2]
    p43[0] = p4[0] - p3[0]
    p43[1] = p4[1] - p3[1]
    p43[2] = p4[2] - p3[2]
    
    if debug:
        print("p43: {}".format(p43))
    
    if abs(p43[0]) < EPS and abs(p43[1]) < EPS and abs(p43[2]) < EPS:
        return (False, None, None)
    
    p21 = np.zeros(3)
    
    p21[0] = p2[0] - p1[0]
    p21[1] = p2[1] - p1[1]
    p21[2] = p2[2] - p1[2]
    if abs(p21[0]) < EPS and abs(p21[1]) < EPS and abs(p21[2]) < EPS:
        return (False, None, None)
    
    if debug:
        print("p21: {}".format(p21))

    d1343 = p13[0] * p43[0] + p13[1] * p43[1] + p13[2] * p43[2]
    d4321 = p43[0] * p21[0] + p43[1] * p21[1] + p43[2] * p21[2]
    d1321 = p13[0] * p21[0] + p13[1] * p21[1] + p13[2] * p21[2]
    d4343 = p43[0] * p43[0] + p43[1] * p43[1] + p43[2] * p43[2]
    d2121 = p21[0] * p21[0] + p21[1] * p21[1] + p21[2] * p21[2]

    denom = d2121 * d4343 - d4321 * d4321
    
    if debug:
        print("denom: {}".format(denom))
    
    if abs(denom) < EPS:
        return (False, None, None)
    
    numer = d1343 * d4321 - d1321 * d4343

    mua = numer / denom
    mub = (d1343 + d4321 * (mua)) / d4343

    pa = np.zeros(3)
    pb = np.zeros(3)
    
    pa[0] = p1[0] + mua * p21[0]
    pa[1] = p1[1] + mua * p21[1]
    pa[2] = p1[2] + mua * p21[2]
    pb[0] = p3[0] + mub * p43[0]
    pb[1] = p3[1] + mub * p43[1]
    pb[2] = p3[2] + mub * p43[2]
    
    return (True, pa, pb)





def plotCams(frame_number_i, frame_number_j):
    
    cam_i = frame_numbers_to_cubemap_cameras[frame_number_i]
    cam_j = frame_numbers_to_cubemap_cameras[frame_number_j]
    
    front_frustum_pts_local = np.array([[0,0,0,1],[-1,-1,-1,1],[1,-1,-1,1],[1,1,-1,1],[-1,1,-1,1]], dtype=np.float)
    
    front_frustum_pts_cam_i = ptsLocalFrontToWorld(cam_i, front_frustum_pts_local)
    front_frustum_pts_cam_j = ptsLocalFrontToWorld(cam_j, front_frustum_pts_local)
    
    frustum_indices = np.array([[0,1,2], [0,2,3], [0,3,4], [0,4,1]])
    
    
    cam_i_center = cam_i.GetCameraCenter()
    cam_j_center = cam_j.GetCameraCenter()
    
    
    cam_i_color = [1,0,0]
    cam_j_color = [0,0,1]

    plot_center_point = (cam_i_center + cam_j_center) / 2.0
    
    plot_range = 1
    
    fig = plt.figure(1)
    ax = fig.add_subplot(111, projection='3d')
    
    
    
    
    
    """
    num_random_points = 100
    random_points = (5.0 * (np.random.rand(num_random_points, 3) - 0.5)) + cam_i_center
                                  
    visible_points = []
    hidden_points = []
                                  
    for i in range(num_random_points):
        pt = random_points[i]
        if worldPointVisible(pt, frame_number_i, 0):
            visible_points.append(pt)
        else:
            hidden_points.append(pt)
    
    visible_points = np.array(visible_points)
    hidden_points = np.array(hidden_points)
    
    print("visible_points: {}".format(len(visible_points)))
    print("hidden_points: {}".format(len(hidden_points)))
    
    scatter_pts(ax, visible_points, 'g', 'o')
    scatter_pts(ax, hidden_points, 'r', 'o')
    """
    
    scatter_pts(ax, np.array([cam_i_center]), 'r', 'o')
    scatter_pts(ax, np.array([cam_j_center]), 'b', '^')
    
    #scatter_pts(ax, front_frustum_pts_cam_i, 'r', 'o')
    #scatter_pts(ax, front_frustum_pts_cam_j, 'b', '^')

    side_frustum_points = np.copy(front_frustum_pts_local)
    side_frustum_points[:,:3] *= 0.5

    for side_idx in range(6):
        side_frustum_pts_local = ptsLocalSideToFront(side_idx, side_frustum_points)
        side_frustum_cam_i_world = ptsLocalFrontToWorld(cam_i, side_frustum_pts_local)
        side_frustum_cam_j_world = ptsLocalFrontToWorld(cam_j, side_frustum_pts_local)
        
        plot_mesh(ax, side_frustum_cam_i_world, frustum_indices, cam_i_color)
        plot_mesh(ax, side_frustum_cam_j_world, frustum_indices, cam_j_color)


    
    plot_mesh(ax, front_frustum_pts_cam_i, frustum_indices, cam_i_color)
    plot_mesh(ax, front_frustum_pts_cam_j, frustum_indices, cam_j_color)
    
    """
    for i, simplex in enumerate(hull_simplices):
        
        projected_xs = projected_pts[simplex, 0]
        projected_ys = projected_pts[simplex, 1]
        projected_zs = projected_pts[simplex, 2]
        
        tri = Poly3DCollection([zip(projected_xs, projected_ys, projected_zs)])
        tri.set_alpha(0.5)
        
        tri_is_visible = visibilities[i]
        
        if tri_is_visible:
            tri.set_facecolor([0,1,0])
        else:
            tri.set_facecolor([1,0,0])
            
        
        ax.add_collection3d(tri)
        
        loop_simplex = np.array(list(simplex) + [simplex[0]])
        plt.plot(projected_pts[loop_simplex, 0], projected_pts[loop_simplex, 1], projected_pts[loop_simplex, 2], 'k-')
        plt.plot(unprojected_pts[loop_simplex, 0], unprojected_pts[loop_simplex, 1], unprojected_pts[loop_simplex, 2], 'k-')
      """  
    ax.auto_scale_xyz([plot_center_point[0] - plot_range, plot_center_point[0] + plot_range], [plot_center_point[1] - plot_range, plot_center_point[1] + plot_range], [plot_center_point[2] - plot_range, plot_center_point[2] + plot_range])
    plt.show()

cube_side_pixels = 960

def imageSpaceToEyeSpace(imagePt):
    u = imagePt[0]
    v = imagePt[1]    
    z = -1.0    
    x = - (z * (u / (cube_side_pixels/2.0) - 1.0))
    y = z * (v / (cube_side_pixels/2.0) - 1.0)
    return np.array([x,y,z,1])



parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)





input_refined_matches_dir = os.path.join(args.base_dir, "pano_frames\\cubemaps\\refined_matches")

possible_side_connections = {
        0: [0,1,2,3],
        1: [0,1,2,3],
        2: [0,1,2,3],
        3: [0,1,2,3]#,
        #4: [0,1,2,3,4],
        #5: [0,1,2,3,5]
        }


print("Loading refined cubemap data")
(frame_numbers_to_cubemap_cameras, frame_numbers_to_transformation_matrices, frame_numbers_to_inverse_transformation_matrices) = loadRefinedCubemapData(args.base_dir)

refined_triangulation_data = readFile(os.path.join(args.base_dir, REFINED_TRIANGULATION_FILENAME))["triangulation_indices"]


for triplet_frame_numbers in refined_triangulation_data:
    triplet_frame_numbers_sorted = np.copy(triplet_frame_numbers)
    triplet_frame_numbers_sorted.sort()
    print("augmenting frame numbers {}".format(triplet_frame_numbers_sorted))
    
    good_world_points_for_this_triplet = []
    
    for pano_idx_i in range(3):
        frame_number_i = triplet_frame_numbers_sorted[pano_idx_i]
        
        cam_i = frame_numbers_to_cubemap_cameras[frame_number_i]
        cam_i_center = cam_i.GetCameraCenter()
        
        for side_idx_i in possible_side_connections:
        
            for other_pano_idx_offset in range(1,3):
                pano_idx_j = (pano_idx_i+other_pano_idx_offset) % 3
                frame_number_j = triplet_frame_numbers_sorted[pano_idx_j]
                
                cam_j = frame_numbers_to_cubemap_cameras[frame_number_j]
                cam_j_center = cam_j.GetCameraCenter()
                
                for side_idx_j in possible_side_connections[side_idx_i]:
                
                    print("going to compare {} {} and {} {}".format(frame_number_i, side_idx_i, frame_number_j, side_idx_j))
                    
                    #plotCams(frame_number_i, frame_number_j)
                    
                    input_match_data_filename = os.path.join(input_refined_matches_dir, "matches_{}_{}_{}_{}.yaml".format(frame_number_i, side_idx_i, frame_number_j, side_idx_j))
                    print("loading {}".format(input_match_data_filename))
                    
                    
                    num_good_points_for_these_imgs = 0
                    
                    
                    input_match_data = readFile(input_match_data_filename)
                    num_good_matches = input_match_data["num_good_matches"]
                    for good_match_idx in range(num_good_matches):
                        
                        epipolar_dist = input_match_data["good_match_epipole_dists"][good_match_idx]
                        if epipolar_dist < 10:
                            good_match_i_pt = input_match_data["good_match_i_pts"][good_match_idx]
                            good_match_j_pt = input_match_data["good_match_j_pts"][good_match_idx]
                            
                            i_pt_eye_space = imageSpaceToEyeSpace(good_match_i_pt)
                            j_pt_eye_space = imageSpaceToEyeSpace(good_match_j_pt)
                        
                            i_pt_world_space = ptsLocalSideToWorld(cam_i, side_idx_i, i_pt_eye_space)[0]
                            j_pt_world_space = ptsLocalSideToWorld(cam_j, side_idx_j, j_pt_eye_space)[0]
                        
                            intersect_res, pa, pb = LineLineIntersect(cam_i_center, i_pt_world_space, cam_j_center, j_pt_world_space)
                            if intersect_res:
                                intersection_point_world = (pa + pb) / 2.0
                                                           
                                if worldPointVisible(intersection_point_world, frame_number_i, side_idx_i) and worldPointVisible(intersection_point_world, frame_number_j, side_idx_j):
                                    good_world_points_for_this_triplet.append(intersection_point_world)
                                    num_good_points_for_these_imgs += 1
                    
                    print("out of {} initial matches, selected {}".format(num_good_matches, num_good_points_for_these_imgs))
                    
    
            
    # once we have the new good world points for the triplet, let's add in the world points we get from the sparse reconstruction
    
    
    input_sparse_triplet_mesh_data_path = os.path.join(args.base_dir, "triplet_{}_{}_{}.meshes".format(triplet_frame_numbers_sorted[0], triplet_frame_numbers_sorted[1], triplet_frame_numbers_sorted[2]))
    
    print("importing sparse points from {}".format(input_sparse_triplet_mesh_data_path))
    with open(input_sparse_triplet_mesh_data_path, 'rb') as triplet_data_file:
        reader = csv.reader(triplet_data_file, delimiter=' ')
        num_sparse_points = int(reader.next()[0])
        for i in range(num_sparse_points):
            pt = [float(x) for x in reader.next()]
            good_world_points_for_this_triplet.append(pt)
    
    good_world_points_for_this_triplet = np.array(good_world_points_for_this_triplet)            
    
    print("total num world points: {}".format(len(good_world_points_for_this_triplet)))
    
    
    world_correspondence_points_for_this_triplet = good_world_points_for_this_triplet
    
    
    triplet_centroid = (triplet_frame_numbers_sorted[0] + triplet_frame_numbers_sorted[1] + triplet_frame_numbers_sorted[2]) / 3.0
    
    # next, project all the world correspondence points onto a unit sphere located at this triplet centroid
    
    unprojected_pts = world_correspondence_points_for_this_triplet
    
    world_correspondence_points_projected_to_centroid_sphere = GetPointsProjectedOntoTranslatedUnitSphere(unprojected_pts, triplet_centroid)
    
    pts = world_correspondence_points_projected_to_centroid_sphere
    world_centroid_hull = ConvexHull(pts)
    
    
    
    
    cubemap_a_center = frame_numbers_to_cubemap_cameras[triplet_frame_numbers_sorted[0]].GetCameraCenter()
    cubemap_b_center = frame_numbers_to_cubemap_cameras[triplet_frame_numbers_sorted[1]].GetCameraCenter()
    cubemap_c_center = frame_numbers_to_cubemap_cameras[triplet_frame_numbers_sorted[2]].GetCameraCenter()
    
    
    output_triplet_mesh_filename = "dense_triplet_{}_{}_{}.meshes".format(triplet_frame_numbers_sorted[0], triplet_frame_numbers_sorted[1], triplet_frame_numbers_sorted[2])
    writeOutputTripletMeshData(args.base_dir, output_triplet_mesh_filename, unprojected_pts, world_centroid_hull.simplices)
    
    
    

