# -*- coding: utf-8 -*-
"""
Created on Sun Feb 25 11:06:01 2018

@author: ANDERSED

Given a scan session, this script looks up the list of frames that are actually going to be used in the visualization.
It then deletes all frames that are not needed.
"""


import argparse
import os
import process_scan_data_helpers
from holoscan_common import readFile, sphereImgPointToXYZUnitSphere, LineLineIntersect, GetPointsProjectedOntoTranslatedUnitSphere, writeOutputTripletMeshData, saveFile, TRIANGULATION_FILENAME
import csv
import numpy as np
import cv2
import glob
import matplotlib.pyplot as plt
from scipy.spatial import ConvexHull
import random
import Queue
import profile
from scipy.spatial import cKDTree
import re

# =============================================================================

os.chdir("E:\\Dev\\OpenGL\\lumiviz\\resources\\scans")

parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)

print("Base scan directory: {}".format(args.base_dir))

SCAN_LABEL = os.path.split(args.base_dir)[-1]

print("SCAN_LABEL: {}".format(SCAN_LABEL))

# =============================================================================

selected_zero_indexed_frame_numbers = set()

match_list_csv = os.path.join(args.base_dir, "refinement_frame_number_matches.csv")

if not os.path.isfile(match_list_csv):
    raise Exception("Cannot delete unused panoframes. No existing file listing in-use panoframes at {}".format(match_list_csv))

print("Loading list of in-use panoframes...")
with open(match_list_csv, 'rb') as csv_file:
    reader = csv.reader(csv_file, delimiter=',')
    
    for row in reader:
        fn_i = int(row[0])
        fn_j = int(row[1])
        selected_zero_indexed_frame_numbers.add(fn_i)
        selected_zero_indexed_frame_numbers.add(fn_j)
print("Loaded. Number of in-use panoframes: {}".format(len(selected_zero_indexed_frame_numbers)))

# =============================================================================

all_panoframes_list = glob.glob(os.path.join(args.base_dir, "pano_frames", "pano_frame*.jpg"))

print("Total number of panoframes: {}".format(len(all_panoframes_list)))

used_panoframes = []
unused_panoframes = []

print("Determining used and unused panoframes...")
for panoframe_path in all_panoframes_list:
    filename = os.path.split(panoframe_path)[-1]
    zero_indexed_frame_number = [int(x) - 1 for x in re.findall(r'\d+', filename)][0] # go from 1-indexed to 0-indexed
    if zero_indexed_frame_number in selected_zero_indexed_frame_numbers:
        used_panoframes.append(panoframe_path)
    else:
        unused_panoframes.append(panoframe_path)
print("Done. {} used panoframes and {} unused panoframes".format(len(used_panoframes), len(unused_panoframes)))

# =============================================================================

print("Deleting any unused panoframes...")

for unused_panoframe_path in unused_panoframes:
    os.remove(unused_panoframe_path)
    
print("Done.")