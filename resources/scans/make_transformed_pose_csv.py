# -*- coding: utf-8 -*-
"""
Created on Wed Oct 04 13:07:48 2017

@author: ANDERSED
"""

import yaml
import argparse
import sys
import os
import subprocess
import glob
from pyrr import quaternion, matrix44, vector3
from pyrr import Quaternion, Matrix33, Matrix44, Vector3
import numpy as np
import csv


# %matplotlib qt
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import proj3d
import numpy as np
import matplotlib.pyplot as plt


EXTRINSICS_FILENAME = "hl_sphere_extrinsics.yaml"

OUT_POSE_SPHERE_CSV_FILENAME = "pose_sphere.csv"


def saveFile(fname,data):
	fd = open(fname,"wb")
	yaml.dump(data,fd)
	fd.close()

def readFile(fname):
	fd = open(fname,"rb")
	data = yaml.load(fd)
	return data


parser = argparse.ArgumentParser()
parser.add_argument("scan_dir", help="base directory for the specific scan")
args = parser.parse_args()

print("Given input pose CSV (for Holo camera), generating a new pose CSV for the 360 camera")

print("scan_dir = {}".format(args.scan_dir))

extrinsics_data = readFile(os.path.join(args.scan_dir, EXTRINSICS_FILENAME))


hl_to_sphere_transform = np.eye(4)
hl_to_sphere_transform[:3,:3] = extrinsics_data["R"]
hl_to_sphere_transform[:3,3] = extrinsics_data["T"].reshape(-1)



# now load in the hololens poses
in_pose_csv_path = glob.glob(os.path.join(args.scan_dir, "*_pose.csv"))[0]
holo_poses = []

holo_positions = []
sphere_positions = []

out_spherical_csv_data = []

with open(in_pose_csv_path, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        timestamp = float(row[0])
        pos_x = float(row[1])
        pos_y = float(row[2])
        pos_z = float(row[3])
        rot_x = float(row[4])
        rot_y = float(row[5])
        rot_z = float(row[6])
        
        pos = np.array([pos_x, pos_y, -pos_z])  # because the pose data comes from Unity with its left-handed coords, flip this axis
        rot = np.array([-rot_x, -rot_y, rot_z]) # flip axes as needed for the rotation to be in a right-handed coord system too
        
        holo_positions.append(pos)
                      
        # rotation of the camera/hololens, representing ZXY euler angles in degrees 
        # (so z degrees about z axis, then x degrees about x axis, then y degrees about y axis, in that order)
        
        m = Matrix44.identity()
        m = Matrix44.from_z_rotation(np.radians(-rot[2])) * m
        m = Matrix44.from_x_rotation(np.radians(-rot[0])) * m
        m = Matrix44.from_y_rotation(np.radians(-rot[1])) * m
        m = Matrix44.from_translation(pos) * m
        m = m.T
        #m = ~m
        
        sphere_m = np.matmul(m, hl_to_sphere_transform)
        sphere_positions.append(sphere_m[:3,3])
        
        out_spherical_csv_data.append([timestamp,  sphere_m[0,0], sphere_m[0,1], sphere_m[0,2], sphere_m[0,3],
                                                   sphere_m[1,0], sphere_m[1,1], sphere_m[1,2], sphere_m[1,3],
                                                   sphere_m[2,0], sphere_m[2,1], sphere_m[2,2], sphere_m[2,3],
                                                   sphere_m[3,0], sphere_m[3,1], sphere_m[3,2], sphere_m[3,3]])
        
        holo_poses.append({
                "timestamp": timestamp,
                "pos_x": pos_x,
                "pos_y": pos_y,
                "pos_z": pos_z,
                "rot_x": rot_x,
                "rot_y": rot_y,
                "rot_z": rot_z,
                "mat": m
                })
    
holo_positions = np.array(holo_positions)
sphere_positions = np.array(sphere_positions)




# for each pose, write the (Holo) timestamp, then the transformation matrix (1D, row-major)
with open(os.path.join(args.scan_dir, OUT_POSE_SPHERE_CSV_FILENAME), 'wb') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    for row in out_spherical_csv_data:
        writer.writerow(row)
    

print("Done transforming poses, saved to {}".format(OUT_POSE_SPHERE_CSV_FILENAME))

"""

fig = plt.figure()
ax = fig.gca(projection='3d')

# switching Y and Z so Y is the vertical axis
ax.plot(holo_positions[:,0], holo_positions[:,2], holo_positions[:,1], label='holo_positions')
ax.plot(sphere_positions[:,0], sphere_positions[:,2], sphere_positions[:,1], label='sphere_positions')


max_range = 10.0

# Create cubic bounding box to simulate equal aspect ratio
max_range = np.array([max_range*2, max_range*2, max_range*2]).max()
Xb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][0].flatten()
Yb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][1].flatten()
Zb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][2].flatten()
# Comment or uncomment following both lines to test the fake bounding box:
for xb, yb, zb in zip(Xb, Yb, Zb):
   ax.plot([xb], [yb], [zb], 'w')




ax.legend()

plt.show()
"""