# -*- coding: utf-8 -*-
"""
Created on Wed Oct 04 16:52:52 2017

@author: ANDERSED
"""

import yaml
import json
import argparse
import os

SCANDATA_FILENAME_YAML = "scandata.yaml"
SCANDATA_FILENAME_JSON = "scandata.json"

def saveFile(fname,data):
	fd = open(fname,"wb")
	yaml.dump(data,fd)
	fd.close()

def readFile(fname):
	fd = open(fname,"rb")
	data = yaml.load(fd)
	return data

parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)

scandata = readFile(os.path.join(args.base_dir, SCANDATA_FILENAME_YAML))

jsontext = json.dumps(scandata)

with open(os.path.join(args.base_dir, SCANDATA_FILENAME_JSON), "wb") as out_json_file:
    out_json_file.write(jsontext)
    
print("saved JSON scan data to {}".format(SCANDATA_FILENAME_JSON))