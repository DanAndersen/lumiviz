# -*- coding: utf-8 -*-
"""
Created on Thu Sep 28 17:16:47 2017

@author: ANDERSED
"""

import numpy as np
import cv2
import os
import glob
import matplotlib.pyplot as plt
import sys

def main():
    if len(sys.argv) < 2:
        print("need input directory, example: create_cubemaps.py path/to/input/imgs/")
    else:
    
        in_dir = sys.argv[1]
        
            
        #in_dir = r"E:\Dev\OpenGL\lumiviz\resources\scans\20170927140341\pano_thumbs"
        
        
        
        out_dir = os.path.join(in_dir, "cubemaps")
        
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)
        
        
        
        # http://stackoverflow.com/questions/29678510/convert-21-equirectangular-panorama-to-cube-map
        # Define our six cube faces. 
        # 0 - 3 are side faces, clockwise order
        # 4 and 5 are top and bottom, respectively
        faceTransform = [
                [0.0,           0.0],
                [np.pi / 2.0,   0.0],
                [np.pi,         0.0],
                [-np.pi/2.0,    0.0],
                [0.0,           -np.pi/2.0],
                [0.0,           np.pi/2.0]
                ]
        
        mapxs = {}
        mapys = {}
        
        # Map a part of the equirectangular panorama (in) to a cube face
        # (face). The ID of the face is given by faceId. The desired
        # width and height are given by width and height. 
        def createCubeMapFace(inMat, allFacesImg, faceId, width, height):
            inWidth = inMat.shape[1]
            inHeight = inMat.shape[0]
            
            if faceId in mapxs and faceId in mapys:
                mapx = mapxs[faceId]
                mapy = mapys[faceId]
            else:
                # allocate map
                mapx = np.zeros((height, width), dtype=np.float32)
                mapy = np.zeros((height, width), dtype=np.float32)
                
                # Calculate adjacent (ak) and opposite (an) of the
                # triangle that is spanned from the sphere center 
                # to our cube face.
                an = np.sin(np.pi / 4.0)
                ak = np.cos(np.pi / 4.0)
                
                ftu = faceTransform[faceId][0]
                ftv = faceTransform[faceId][1]
                
                # For each point in the target image, 
                # calculate the corresponding source coordinates. 
                for y in range(height):
                    for x in range(width):
                        nx = float(y) / float(height) - 0.5
                        ny = float(x) / float(width) - 0.5
                        
                        nx *= 2
                        ny *= 2
                        
                        # Map [-1, 1] plane coords to [-an, an]
                        # that's the coordinates in respect to a unit sphere 
                        # that contains our box. 
                        nx *= an
                        ny *= an
                        
                        u = 0.0
                        v = 0.0
                        
                        # Project from plane to sphere surface.
                        if (ftv == 0):
                            # Center faces
                            u = np.arctan2(nx, ak)
                            v = np.arctan2(ny * np.cos(u), ak)
                            u += ftu
                        elif (ftv > 0):
                            # Bottom face 
                            d = np.sqrt(nx * nx + ny * ny)
                            v = np.pi / 2.0 - np.arctan2(d, ak)
                            u = np.arctan2(ny, nx)
                        else:
                            # Top face
                            d = np.sqrt(nx * nx + ny * ny)
                            v = -np.pi / 2.0 + np.arctan2(d, ak)
                            u = np.arctan2(-ny, nx)
                            
                        # Map from angular coordinates to [-1, 1], respectively.
                        u = u / (np.pi)
                        v = v / (np.pi / 2.0)
                        
                        # Warp around, if our coordinates are out of bounds. 
                        while (v < -1):
                            v += 2
                            u += 1
                            
                        while (v > 1):
                            v -= 2
                            u += 1
            
                        while (u < -1):
                            u += 2
                            
                        while (u > 1):
                            u -= 2
                        
                        # Map from [-1, 1] to in texture space
                        u = u / 2.0 + 0.5
                        v = v / 2.0 + 0.5
            
                        u = u * (inWidth - 1)
                        v = v * (inHeight - 1)
                        
                        # Save the result for this pixel in map
                        mapx[x,y] = u
                        mapy[x,y] = v
                mapxs[faceId] = mapx
                mapys[faceId] = mapy
            
            allFacesImg[:, faceId*width : (faceId+1)*width] = cv2.remap(inMat, mapx, mapy, cv2.INTER_LINEAR)
                        
        in_img_search_path = os.path.join(in_dir, "*.jpg")
        
        in_img_paths = glob.glob(in_img_search_path)
        
        for in_img_path in in_img_paths:
            filename = in_img_path.split("\\")[-1]
            print("loading {}".format(filename))
            
            label = filename.split('.')[0]
            extension = filename.split('.')[1]
            
            in_img = cv2.imread(in_img_path)
            
            cubemapSize = in_img.shape[1] / 4
            
            allFacesImg = np.zeros((cubemapSize, cubemapSize*6, 3), dtype=in_img.dtype)
                                      
            for faceId in range(6):
                createCubeMapFace(in_img, allFacesImg, faceId, cubemapSize, cubemapSize)
                #plt.imshow(allFacesImg)
                #plt.show()
            
            out_filename = "{}_cubemap.{}".format(label, extension)
            out_full_path = os.path.join(out_dir, out_filename)
            
            cv2.imwrite(out_full_path, allFacesImg)
            
        print("done creating cubemaps")
    
if __name__ == "__main__":
    main()