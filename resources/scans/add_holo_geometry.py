# -*- coding: utf-8 -*-
"""
Created on Tue Nov 21 09:59:33 2017

@author: ANDERSED
"""

# augments the sparse correspondences with points from the HoloLens geometry



import argparse
import re
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import yaml
import os
import csv
import functools
from scipy.spatial import ConvexHull
from scipy.spatial import Delaunay
import cv2
from holoscan_common import Camera, loadFullCubemapImgForFrameNumber, loadRefinedCubemapData, saveFile, readFile
import glob
import struct

parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)

holo_room_path = glob.glob(os.path.join(args.base_dir, "*.room"))[0]

with open(holo_room_path, mode='rb') as file:
    fileContent = file.read()
    
#struct.unpack("i", fileContent[])
