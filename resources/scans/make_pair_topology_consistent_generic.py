# -*- coding: utf-8 -*-
"""
Created on Fri Jan 26 12:53:37 2018

@author: ANDERSED

Given
- a base directory for a scan, and
- the directory containing all the .meshes files that may have inconsistent topology...

Create a subdirectory inside the directory containing the .meshes file.

In that subdirectory, put the new consistent .meshes.
"""

import argparse
import os
import process_scan_data_helpers
from holoscan_common import readFile, sphereImgPointToXYZUnitSphere, LineLineIntersect, GetPointsProjectedOntoTranslatedUnitSphere, writeOutputTripletMeshData
import csv
import numpy as np
import cv2
import glob
import matplotlib.pyplot as plt
from scipy.spatial import ConvexHull
import random
import Queue
import profile

def calculateVisibilityOfTriangles(simplices, projected_pts, projection_center):
    
    pts_for_each_triangle = projected_pts[simplices]
    
    a_verts = pts_for_each_triangle[:,0]
    b_verts = pts_for_each_triangle[:,1]
    c_verts = pts_for_each_triangle[:,2]
    
    b_minus_a = b_verts - a_verts
    c_minus_a = c_verts - a_verts
    
    normals = np.cross(b_minus_a, c_minus_a)
    # skipping normalization because we do not actually need to normalize to determine vector direction
    
    a_minus_proj_center = a_verts - projection_center
    
    visibilities = np.einsum('ij,ij->i', normals, a_minus_proj_center) > 0
    
    return visibilities

def fixSimplicesNeighbors(unfixed_simplices, unfixed_neighbors, world_points_projected_to_centroid_sphere, centroid_of_hull):
    fixed_simplices = []
    fixed_neighbors = []
    
    projected_pts = world_points_projected_to_centroid_sphere
    
    projection_center = centroid_of_hull
    
    unfixed_visibilities = calculateVisibilityOfTriangles(unfixed_simplices, projected_pts, projection_center)
    
    for i in range(len(unfixed_visibilities)):
        
        unfixed_simplex = unfixed_simplices[i]
        unfixed_neighbor = unfixed_neighbors[i]
        
        is_facing_center = unfixed_visibilities[i]
        
        if is_facing_center:
            fixed_simplices.append(unfixed_simplex)
            fixed_neighbors.append(unfixed_neighbor)
        else:
            fixed_simplex = np.fliplr([unfixed_simplex])[0]
            fixed_simplices.append( fixed_simplex )
            fixed_neighbor = np.fliplr([unfixed_neighbor])[0]
            fixed_neighbors.append(fixed_neighbor)
    
    fixed_simplices = np.array(fixed_simplices)
    fixed_neighbors = np.array(fixed_neighbors)
    return (fixed_simplices, fixed_neighbors)

def generateHullData(pts, pair_centroid, pair_cubemap_centers):
    
    if len(pts) <= 6:
        # Not enough original points. Put in some fake correspondences.
        extra_points = []
        
        for i in range(2):
            extra_points.append(pair_cubemap_centers[i] + (pair_cubemap_centers[i] - pair_centroid) * 1.1 + np.array([0.0, 1.0, 0.0]))
            extra_points.append(pair_cubemap_centers[i] + (pair_cubemap_centers[i] - pair_centroid) * 1.1 + np.array([0.0, -1.0, 0.0]))
            
        extra_points = np.array(extra_points)
        
        pts = extra_points
        
    
    
    pts_projected_to_centroid_sphere = GetPointsProjectedOntoTranslatedUnitSphere(pts, pair_centroid)
    
    pts_projected_to_each_pano = [GetPointsProjectedOntoTranslatedUnitSphere(pts, c) for c in pair_cubemap_centers]
    
    hull = ConvexHull(pts_projected_to_centroid_sphere)
    
    centroid_of_hull = np.mean(pts_projected_to_centroid_sphere, axis=0)    # not the same as the centroid of the triplet... this is just the geometric center of the generated centroid

    fixed_simplices, fixed_neighbors = fixSimplicesNeighbors(hull.simplices, hull.neighbors, pts_projected_to_centroid_sphere, centroid_of_hull)
    
    # check to see if the convex hull actually wraps around the centroid position... if not, then we need to add a big box and recalculate
    visibilities_for_centroid = calculateVisibilityOfTriangles(fixed_simplices, pts_projected_to_centroid_sphere, pair_centroid)
    
    num_visible_for_centroid = np.count_nonzero(visibilities_for_centroid)
    
    if num_visible_for_centroid < len(fixed_simplices):
        print("entire convex hull is away from the centroid")
        # some were not visible, even to the centroid! this means that the entire convex hull is located somewhere away from the centroid
        # this happens when there are only correspondences on one side
        # to "fix" -- add vertices for a box that extends as far as the closest real point
        pts_augmented = np.copy(pts)
        
        
        extra_points = []
        
        for i in range(2):
            extra_points.append(pair_cubemap_centers[i] + (pair_cubemap_centers[i] - pair_centroid) * 1.1 + np.array([0.0, 1.0, 0.0]))
            extra_points.append(pair_cubemap_centers[i] + (pair_cubemap_centers[i] - pair_centroid) * 1.1 + np.array([0.0, -1.0, 0.0]))
            extra_points.append(pair_cubemap_centers[i] + (pair_cubemap_centers[i] - pair_centroid) * 1.1 + np.array([-1.0, 0.0, 0.0]))
            extra_points.append(pair_cubemap_centers[i] + (pair_cubemap_centers[i] - pair_centroid) * 1.1 + np.array([1.0, 0.0, 0.0]))
            extra_points.append(pair_cubemap_centers[i] + (pair_cubemap_centers[i] - pair_centroid) * 1.1 + np.array([0.0, 0.0, -1.0]))
            extra_points.append(pair_cubemap_centers[i] + (pair_cubemap_centers[i] - pair_centroid) * 1.1 + np.array([0.0, 0.0, 1.0]))
            
        extra_points = np.array(extra_points)
        
        pts_augmented = np.vstack((pts_augmented, extra_points))
        
        print("NOTE: augmenting this pair mesh with fake correspondences!")
        
        return generateHullData(pts_augmented, pair_centroid, pair_cubemap_centers)
        
    visibilities_for_each_pano = []
    for i in range(2):
        visibilities_for_each_pano.append(calculateVisibilityOfTriangles(fixed_simplices, pts_projected_to_each_pano[i], pair_cubemap_centers[i]))
    
    return pts, pts_projected_to_centroid_sphere, pts_projected_to_each_pano, fixed_simplices, fixed_neighbors, visibilities_for_each_pano


def iteratePointFiltering(current_world_points, pair_centroid, pair_cubemap_centers):
    
    current_world_points, world_points_projected_to_centroid_sphere, world_points_projected_to_each_pano, fixed_simplices, fixed_neighbors, visibilities_for_each_pano = generateHullData(current_world_points, pair_centroid, pair_cubemap_centers)
    
    num_verts = len(current_world_points)
    num_triangles = len(fixed_simplices)
    
    # find a triangle that is good for all 3 meshes -- no folding for that particular triangle
    initial_good_triangle_index = -1
    
    start_point_order = [i for i in range(num_triangles)]
    random.shuffle(start_point_order)
    
    for i in start_point_order:
        if visibilities_for_each_pano[0][i] and visibilities_for_each_pano[1][i]:
            initial_good_triangle_index = i
            break
    
    good_vert_indices = [True] * num_verts
    good_vert_indices = np.array(good_vert_indices)
    
    visited_triangle_set = set()
    tri_process_queue = Queue.Queue()
    tri_process_queue.put([initial_good_triangle_index, None])
    
    while not tri_process_queue.empty():
        [current_tri_index, predecessor_tri_index] = tri_process_queue.get(block=False)
        if current_tri_index not in visited_triangle_set:
            #print("processing tri {}".format(current_tri_index))
            visited_triangle_set.add(current_tri_index)
            
            tri_is_visible_to_all_panos = True
            for pano_idx in range(2):
                if not visibilities_for_each_pano[pano_idx][current_tri_index]:
                    tri_is_visible_to_all_panos = False
                    # this tri is flipped for at least one pano!
                    break
            
            if not tri_is_visible_to_all_panos:
                if predecessor_tri_index is not None:
                    current_tri_simplices = set(fixed_simplices[current_tri_index])
                    predecessor_tri_simplices = set(fixed_simplices[predecessor_tri_index])
                    
                    newly_introduced_vert = list(current_tri_simplices - predecessor_tri_simplices)[0]
                    #print("vert {} is bad".format(newly_introduced_vert))
                    good_vert_indices[newly_introduced_vert] = False
            
            neighbors_of_current_tri = fixed_neighbors[current_tri_index]
            for neighbor_of_current_tri in neighbors_of_current_tri:
                if neighbor_of_current_tri not in visited_triangle_set:
                    tri_process_queue.put([neighbor_of_current_tri, current_tri_index])
    
                    
    num_bad_points = len(current_world_points[good_vert_indices == False,:])
    print("removed {} bad points".format(num_bad_points))

    return current_world_points[good_vert_indices == True]



os.chdir("E:\\Dev\\OpenGL\\lumiviz\\resources\\scans")

parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")
parser.add_argument("input_meshes_dir", help="directory containing the .meshes files to be processed")

args = parser.parse_args()

print(args)


# =============================================================================

fn_to_pose_refined_path = os.path.join(args.base_dir, "fn_to_pose_refined.csv")

if not os.path.isfile(fn_to_pose_refined_path):
    raise Exception("Missing refined poses at {}".format(fn_to_pose_refined_path))

in_selected_1d_frame_numbers_path = os.path.join(args.base_dir, "selected_1d_frame_numbers.csv")

if not os.path.isfile(in_selected_1d_frame_numbers_path):
    raise Exception("Missing selected 1D frame numbers at {}".format(in_selected_1d_frame_numbers_path))

# =============================================================================

# Load the refined poses for each frame number.

fns_to_poses = {}   # contains matrix [R|t] where [R|t] * X_local = X_world

with open(fn_to_pose_refined_path, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        fn = int(row[0]) # 0-indexed
        mat = np.array([float(x) for x in row[1:]]).reshape((4,4))
        fns_to_poses[fn] = mat
                    
# =============================================================================

# Read in the selected 1D frame numbers.

selected_fns = []
with open(in_selected_1d_frame_numbers_path, 'rb') as in_file:
    reader = csv.reader(in_file, delimiter=',')
    for row in reader:
        fn = int(row[0])
        selected_fns.append(fn)
        
print("Loaded {} selected 1D frame numbers.".format(len(selected_fns)))

# =============================================================================

def makeTopologyConsistent(pair_world_points, pair_centroid, pair_cubemap_centers):
    current_world_points = pair_world_points
    new_world_points = iteratePointFiltering(current_world_points, pair_centroid, pair_cubemap_centers)
    while len(new_world_points) != len(current_world_points):
        current_world_points = new_world_points
        new_world_points = iteratePointFiltering(current_world_points, pair_centroid, pair_cubemap_centers)
    
    return new_world_points


# =============================================================================
    
# Create subdirectory to contain the consistent topology meshes.
out_consistent_topology_dir = os.path.join(args.input_meshes_dir, "consistent_topology")

if not os.path.exists(out_consistent_topology_dir):
    os.makedirs(out_consistent_topology_dir)

for idx in range(len(selected_fns) - 1):
    pair = [selected_fns[idx], selected_fns[idx+1]]
    
    pair_canonical_order = sorted(pair) # used for labeling the pair uniquely

    pair_label = "{}_{}".format(pair_canonical_order[0], pair_canonical_order[1])
    
    matching_input_meshes = glob.glob(os.path.join(args.input_meshes_dir, "*{}*.meshes".format(pair_label)))
    if len(matching_input_meshes) == 0:
        raise Exception("No valid input mesh found for pair label {}".format(pair_label))
        
    for input_mesh_path in matching_input_meshes:
    
        input_filename = os.path.split(input_mesh_path)[-1]
        
        out_mesh_path = os.path.join(out_consistent_topology_dir, input_filename)
        
        if not os.path.isfile(out_mesh_path):
            print("processing {}".format(pair_label))
            
            pano_a_center = fns_to_poses[pair_canonical_order[0]][:3,3]
            pano_b_center = fns_to_poses[pair_canonical_order[1]][:3,3]
            
            pair_cubemap_centers = [pano_a_center, pano_b_center]
            
            pair_centroid = (pano_a_center + pano_b_center) / 2.0
            
            pair_world_points = []
            
            with open(input_mesh_path, 'rb') as pair_data_file:
                reader = csv.reader(pair_data_file, delimiter=' ')
                num_points = int(reader.next()[0])
                for i in range(num_points):
                    pt_row = reader.next()
                    world_pt = np.array([float(x) for x in pt_row])
                    pair_world_points.append(world_pt)
            
            pair_world_points = np.array(pair_world_points)
            
            print("initial num points: {}".format(len(pair_world_points)))
            
            
            new_world_points = makeTopologyConsistent(pair_world_points, pair_centroid, pair_cubemap_centers)
            
            new_world_points, world_points_projected_to_centroid_sphere, world_points_projected_to_each_pano, fixed_simplices, fixed_neighbors, visibilities_for_each_pano = generateHullData(new_world_points, pair_centroid, pair_cubemap_centers)
            
            simplices = fixed_simplices
            
            print("result for this pair: {} remaining world points".format(len(new_world_points)))
            
            writeOutputTripletMeshData(out_consistent_topology_dir, input_filename, new_world_points, simplices)
            
            
        else:
            print("Consistent mesh already exists for input file {}. Skipping.".format(input_filename))
    