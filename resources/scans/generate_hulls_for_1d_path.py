# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 14:19:19 2018

@author: ANDERSED

Given the selected frames for 1D path morphing, create convex hulls for each pair.
For a pair (i, i+1), where we have already computed SPHORB matches (and inliers)
between them, determine the topology of the points on a unit sphere convex hull.
"""

import argparse
import os
import process_scan_data_helpers
from holoscan_common import readFile, sphereImgPointToXYZUnitSphere, sphereImgPointToXYZUnitSphereMultiple, LineLineIntersect, GetPointsProjectedOntoTranslatedUnitSphere, writeOutputTripletMeshData, saveFile
import csv
import numpy as np
import cv2
import glob
import matplotlib.pyplot as plt
from scipy.spatial import ConvexHull
import random
import Queue
import profile

# =============================================================================

os.chdir("E:\\Dev\\OpenGL\\lumiviz\\resources\\scans")

parser = argparse.ArgumentParser()
parser.add_argument("base_dir", help="base directory of the scan")

args = parser.parse_args()

print(args)

print("Base scan directory: {}".format(args.base_dir))

SCAN_LABEL = os.path.split(args.base_dir)[-1]

print("SCAN_LABEL: {}".format(SCAN_LABEL))

# =============================================================================

pano_frame_dir = os.path.join(args.base_dir, "pano_frames")

# Determine size of panoramas.
pano_frame_resolution = cv2.imread(glob.glob(os.path.join(pano_frame_dir, "pano_frame*.jpg"))[0]).shape[::-1][1:]

# =============================================================================

# Load in the selected 1D path frame numbers.

in_selected_1d_frame_numbers_path = os.path.join(args.base_dir, "selected_1d_frame_numbers.csv")

selected_fns = []
with open(in_selected_1d_frame_numbers_path, 'rb') as in_file:
    reader = csv.reader(in_file, delimiter=',')
    for row in reader:
        fn = int(row[0])
        selected_fns.append(fn)
        
print("Loaded {} selected 1D frame numbers.".format(len(selected_fns)))

# =============================================================================

out_path_meshes_dir = os.path.join(args.base_dir, "path_meshes", "sparse")

if not os.path.exists(out_path_meshes_dir):
    os.makedirs(out_path_meshes_dir)

# =============================================================================

for i in range(len(selected_fns) - 1):
    fn_i = selected_fns[i]
    fn_j = selected_fns[i+1] # fn_i is implicitly less than fn_j in 1D path case, no need to adjust the order
    
    out_start_meshes_filename = "path_{}_{}_hullstart.meshes".format(fn_i, fn_j) # convex hull initialized with fn_i's points
    out_end_meshes_filename = "path_{}_{}_hullend.meshes".format(fn_i, fn_j) # convex hull initialized with fn_j's points
    
    if not os.path.isfile(os.path.join(out_path_meshes_dir, out_start_meshes_filename)) or not os.path.isfile(os.path.join(out_path_meshes_dir, out_end_meshes_filename)):
                       
        print("processing morph between {} and {}".format(fn_i, fn_j))
        
        essential_data_path = os.path.join(args.base_dir, "pano_frames", "sphorb_matches", "sphorb_match_%05d_%05d_essential.yaml" % (fn_i + 1, fn_j + 1)) # go to 1-indexed
        
        if not os.path.isfile(essential_data_path):
            raise Exception("Missing essential data at {}".format(essential_data_path))
            
        essential_data = readFile(essential_data_path)
        
        inlier_indices = essential_data["best_fit_inlier_indices"]
                                          
        # Load in the SPHORB matches and the inlier list
        sphorb_match_path = os.path.join(args.base_dir, "pano_frames", "sphorb_matches", "sphorb_match_%05d_%05d.csv" % (fn_i + 1, fn_j + 1)) # go to 1-indexed
        
        if not os.path.isfile(sphorb_match_path):
            raise Exception("Missing SPHORB matches at {}".format(sphorb_match_path))
        
        kp_i = []
        kp_j = []
        xyzs_i = []
        xyzs_j = []
        
        with open(sphorb_match_path, 'rb') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            
            row_index = 0
            for row in reader:
                if row_index in inlier_indices:
                    # Get image points 
                    img_pt_i = [float(x) for x in row[0:2]]
                    img_pt_j = [float(x) for x in row[2:4]]
                    kp_i.append(img_pt_i)
                    kp_j.append(img_pt_j)
                
                row_index += 1
                
        kp_i = np.array(kp_i)
        kp_j = np.array(kp_j)
        img_pt_i_xs = kp_i[:,0]
        img_pt_i_ys = kp_i[:,1]
        img_pt_j_xs = kp_j[:,0]
        img_pt_j_ys = kp_j[:,1]
        
        xyzs_i = sphereImgPointToXYZUnitSphereMultiple(img_pt_i_xs, img_pt_i_ys, pano_frame_resolution)
        xyzs_j = sphereImgPointToXYZUnitSphereMultiple(img_pt_j_xs, img_pt_j_ys, pano_frame_resolution)
        
        hull_i = ConvexHull(xyzs_i)
        hull_j = ConvexHull(xyzs_j)
        
        
        writeOutputTripletMeshData(out_path_meshes_dir, out_start_meshes_filename, xyzs_i, hull_i.simplices)
        writeOutputTripletMeshData(out_path_meshes_dir, out_end_meshes_filename, xyzs_j, hull_j.simplices)
        print("wrote to {} and {}".format(out_start_meshes_filename, out_end_meshes_filename))
    else:
        print("skipping generation of path hulls for {}-->{} (already exists)".format(fn_i, fn_j))

print("Done.")