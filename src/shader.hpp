#pragma once

#include "common.hpp"

#include <glm/gtc/type_ptr.hpp>

// Standard Headers
#include <string>

// Define Namespace
class Shader
{
public:

    // Implement Custom Constructor and Destructor
	Shader(std::string const & vertFilename, std::string const & fragFilename) { init(vertFilename, fragFilename);  }
    ~Shader() { glDeleteProgram(this->Program); }

    // Public Member Functions
	
    void use();
    Shader & attach(std::string const & filename);
    GLuint   create(std::string const & filename);
    GLuint   get() { return this->Program; }
    Shader & link();

    // Wrap Calls to glUniform
    void bind(unsigned int location, float value);
    void bind(unsigned int location, glm::mat4 const & matrix);
    template<typename T> Shader & bind(std::string const & name, T&& value)
    {
        int location = glGetUniformLocation(mProgram, name.c_str());
        if (location == -1) fprintf(stderr, "Missing Uniform: %s\n", name.c_str());
        else bind(location, std::forward<T>(value));
        return *this;
    }

	GLuint Program;

private:
	void Shader::init(std::string const & vertFilename, std::string const & fragFilename);

    // Private Member Variables
    GLint  mStatus;
    GLint  mLength;

};