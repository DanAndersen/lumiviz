#include "cubemap.hpp"

Cubemap::Cubemap(int size, ImageFormat imageFormat, GLenum filter = GL_NEAREST)
{
	this->size = size;

	// initing framebuffer
	GL_CHECK(glGenFramebuffers(1, &framebufferName));
	glBindFramebuffer(GL_FRAMEBUFFER, framebufferName);

	// =====
	// create cube texture https://gamedev.stackexchange.com/questions/19461/opengl-glsl-render-to-cube-map

	glGenTextures(1, &renderedTextureCubemap);
	glBindTexture(GL_TEXTURE_CUBE_MAP, renderedTextureCubemap);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_BASE_LEVEL, 0);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAX_LEVEL, 0);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, filter);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, filter);

	const GLvoid * testDataVoid;

	if (imageFormat.Type == GL_UNSIGNED_BYTE) {
		std::vector<GLubyte> testDataUnsignedBytes(this->size * this->size * 256, 128);
		testDataVoid = &testDataUnsignedBytes[0];


		for (int loop = 0; loop < 6; ++loop) {
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + loop, 0, imageFormat.InternalFormat,
				this->size, this->size, 0, imageFormat.Format, imageFormat.Type, testDataVoid);
		}
	}
	else if (imageFormat.Type == GL_FLOAT) {
		std::vector<GLfloat> testDataFloat(this->size * this->size * imageFormat.NumChannels, 0.5f);
		testDataVoid = &testDataFloat[0];


		for (int loop = 0; loop < 6; ++loop) {
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + loop, 0, imageFormat.InternalFormat,
				this->size, this->size, 0, imageFormat.Format, imageFormat.Type, testDataVoid);
		}
	}
	else {
		std::cout << "ERROR: unknown image format for cubemap" << std::endl;
	}

	

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	// =====

	// set up depth buffer
	GL_CHECK(glGenRenderbuffers(1, &depthRenderbuffer));
	glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, this->size, this->size);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);

	GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, 0));
}

void Cubemap::preRender()
{
	GL_CHECK(glGetIntegerv(GL_VIEWPORT, previousViewport));

	GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, framebufferName));

	GL_CHECK(glViewport(0, 0, this->size, this->size));

	GL_CHECK(glClearColor(0.0f, 0.0f, 0.0f, 1.0f));
	GL_CHECK(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

}

void Cubemap::postRender()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(previousViewport[0], previousViewport[1], previousViewport[2], previousViewport[3]);
}

void Cubemap::PreFace(int faceIdx)
{
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + faceIdx, renderedTextureCubemap, 0);

	// check framebuffer status
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		std::cout << "ERROR: cubemap framebuffer was not successfully created" << std::endl;
	}

	GL_CHECK(glViewport(0, 0, this->size, this->size));
	GL_CHECK(glClearColor(0.0f, 0.0f, 0.0f, 1.0f));
	GL_CHECK(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
}

void Cubemap::PostFace(int faceIdx)
{
}

GLuint Cubemap::textureCubemap()
{
	return renderedTextureCubemap;
}

int Cubemap::GetCubemapSize()
{
	return this->size;
}
