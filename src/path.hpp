#pragma once

#include "pose.hpp"
#include "camera_parameters.hpp"

#include "common.hpp"
#include "shader.hpp"

#include <vector>
#include <unordered_map>

#include <nanoflann.hpp>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <CGAL/Barycentric_coordinates_2/Triangle_coordinates_2.h>



typedef CGAL::Exact_predicates_inexact_constructions_kernel            Kernel;
typedef CGAL::Triangulation_vertex_base_with_info_2<unsigned int, Kernel> Vb;
typedef CGAL::Triangulation_data_structure_2<Vb>                       Tds;
typedef CGAL::Delaunay_triangulation_2<Kernel, Tds>                    Delaunay;
typedef Kernel::Point_2                                                Point2D;
typedef CGAL::Triangulation_2<Kernel, Tds>::Locate_type				   Locate_type;
typedef CGAL::Barycentric_coordinates::Triangle_coordinates_2<Kernel>  Triangle_coordinates;


using namespace std;

typedef nanoflann::KDTreeSingleIndexAdaptor <
	nanoflann::L2_Simple_Adaptor<float, PointCloud<float> >,
	PointCloud<float>,
	3 /* dim */
> my_kd_tree_t;

class Path {
public:
	vector<Pose> poses;
	
	Path(vector<Pose> poses);

	void Draw(std::shared_ptr<Shader> shader, int startIndex, int endIndex);

	static std::shared_ptr<Path> LoadHoloPath(CameraParameters camera, string const & filepath, float gridSizeMeters, int limitNumFrames = -1);

	static std::shared_ptr<Path> LoadSpherePath(string const & filepath);

	size_t findNearestNeighborIndex(glm::vec3 queryPosition);

	size_t findMostSimilarNeighborIndex(glm::vec3 queryPosition, glm::vec3 queryForwardVector, float directionWeight, float searchRadius);

	glm::vec3 boundingBoxMin;
	glm::vec3 boundingBoxMax;

	// maps from a cell_id to the pose index that is closest to the center of that cell.
	// if there is no pose inside that cell, the cell is not in the map.
	std::unordered_map<glm::ivec2, size_t> closestPoseIndices;	

	Delaunay cellCenterTriangulation;

private:

	GLuint VAO, VBO;

	void setupPath();

	void generateKDTree();

	std::shared_ptr<my_kd_tree_t> kdTreeIndex;

	PointCloud<float> cloud;

	
};