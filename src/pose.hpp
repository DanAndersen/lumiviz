#pragma once

#include "glm_includes.hpp"
#include "camera_parameters.hpp"
#include <vector>
#include <string>
#include <unordered_map>

struct Pose {
	glm::vec3 Position; // position of the camera/hololens

	glm::mat4 RotationMatrix; // generated at runtime from Rotation
	glm::mat4 InverseRotationMatrix;
	glm::quat RotationQuaternion;

	glm::mat4 ModelMatrix; // both translation and rotation

	Pose()
	{

	}

	Pose(glm::vec3 pos, glm::quat rotQuat, glm::mat4 rotMat) :
		Position(pos),
		RotationMatrix(rotMat),
		RotationQuaternion(rotQuat)
	{
		ModelMatrix = glm::translate(ModelMatrix, Position);
		ModelMatrix = ModelMatrix * RotationMatrix;

		InverseRotationMatrix = glm::inverse(RotationMatrix);
	}

	static std::unordered_map<int, Pose> LoadSpherePoses(CameraParameters camera, std::string const & filepath);
};