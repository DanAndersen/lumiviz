#pragma once

struct CameraParameters {
	// contains data about the camera being used for the poses

	float HfovDegrees;
	float AspectRatio;
	float NearZMeters;
	float FarZMeters;

	CameraParameters(float hfovDegrees, float aspectRatio, float nearZMeters, float farZMeters) :
		HfovDegrees(hfovDegrees),
		AspectRatio(aspectRatio),
		NearZMeters(nearZMeters),
		FarZMeters(farZMeters)
	{

	}
};