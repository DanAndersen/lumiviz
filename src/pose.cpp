#include "pose.hpp"
#include <iostream>
#include <fstream>
#include "glm_includes.hpp"
#include <unordered_map>

#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtc/matrix_access.hpp>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/string_cast.hpp>

std::unordered_map<int, Pose> Pose::LoadSpherePoses(CameraParameters camera, std::string const & filepath) {
	// loading in a CSV file representing the spherical camera path.
	// the data is in the format:
	// frame_number,m_00,m_01,m_02,m_03,m_10,m_11,...,m_33

	std::unordered_map<int, Pose> frame_numbers_to_poses;

	std::ifstream ifs;

	ifs.open(filepath, std::ifstream::in | std::ifstream::binary);

	std::string frame_number_str;
	std::string m00_str, m01_str, m02_str, m03_str;
	std::string m10_str, m11_str, m12_str, m13_str;
	std::string m20_str, m21_str, m22_str, m23_str;
	std::string m30_str, m31_str, m32_str, m33_str;

	while (getline(ifs, frame_number_str, ',')) {

		glm::mat4 m;

		getline(ifs, m00_str, ',');
		getline(ifs, m01_str, ',');
		getline(ifs, m02_str, ',');
		getline(ifs, m03_str, ',');

		getline(ifs, m10_str, ',');
		getline(ifs, m11_str, ',');
		getline(ifs, m12_str, ',');
		getline(ifs, m13_str, ',');

		getline(ifs, m20_str, ',');
		getline(ifs, m21_str, ',');
		getline(ifs, m22_str, ',');
		getline(ifs, m23_str, ',');

		getline(ifs, m30_str, ',');
		getline(ifs, m31_str, ',');
		getline(ifs, m32_str, ',');
		getline(ifs, m33_str);

		m[0][0] = stof(m00_str);
		m[1][0] = stof(m01_str);
		m[2][0] = stof(m02_str);
		m[3][0] = stof(m03_str);

		m[0][1] = stof(m10_str);
		m[1][1] = stof(m11_str);
		m[2][1] = stof(m12_str);
		m[3][1] = stof(m13_str);

		m[0][2] = stof(m20_str);
		m[1][2] = stof(m21_str);
		m[2][2] = stof(m22_str);
		m[3][2] = stof(m23_str);

		m[0][3] = stof(m30_str);
		m[1][3] = stof(m31_str);
		m[2][3] = stof(m32_str);
		m[3][3] = stof(m33_str);

		//cout << timestamp_str << "\t" << pos_x_str << "\t" << pos_y_str << "\t" << pos_z_str << "\t" << rot_x_str << "\t" << rot_y_str << "\t" << rot_z_str << endl;

		glm::mat4 ModelMatrix = m;

		glm::mat3 RotationMatrix(ModelMatrix);

		glm::vec3 pos = glm::column(ModelMatrix, 3);
		glm::quat RotationQuat(RotationMatrix);

		int frame_number = stof(frame_number_str);

		Pose pose(pos, RotationQuat, RotationMatrix);

		// see http://stackoverflow.com/questions/31191752/right-handed-euler-angles-xyz-to-left-handed-euler-angles-xyz

		//cout << pose.Position.x << "\t" << pose.Position.y << "\t" << pose.Position.z << endl;

		frame_numbers_to_poses.insert({ frame_number, pose });

	}

	ifs.close();

	return frame_numbers_to_poses;
}