#pragma once
#include <CGAL/Cartesian.h>
#include <CGAL/Plane_3.h>

#define CGAL_NO_ASSERTIONS 1

typedef CGAL::Cartesian<float> K;

typedef K::FT K_FT;
typedef K::Ray_3 K_Ray;
typedef K::Line_3 K_Line;
typedef K::Point_3 K_Point;
typedef K::Triangle_3 K_Triangle;
typedef K::Plane_3 K_Plane;
typedef K::Vector_3 K_Vector;

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/point_generators_3.h>
#include <CGAL/algorithm.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Polyhedron_items_with_id_3.h> 
#include <CGAL/convex_hull_3.h>
#include <vector>
#include <CGAL/AABB_tree.h>
#include <CGAL/AABB_traits.h>
#include <CGAL/AABB_triangle_primitive.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel  Hull_K;
typedef CGAL::Polyhedron_3<Hull_K, CGAL::Polyhedron_items_with_id_3> Polyhedron_3;
typedef Hull_K::Segment_3                              Segment_3;
typedef Hull_K::Point_3                                Point_3;
typedef Hull_K::Ray_3                                Ray_3;
typedef Hull_K::Triangle_3                                Triangle_3;
typedef CGAL::Creator_uniform_3<double, Point_3>  PointCreator;
typedef std::list<Triangle_3>::iterator Hull_Iterator;
typedef CGAL::AABB_triangle_primitive<Hull_K, Hull_Iterator> Hull_Primitive;
typedef CGAL::AABB_traits<Hull_K, Hull_Primitive> Hull_AABB_triangle_traits;
typedef CGAL::AABB_tree<Hull_AABB_triangle_traits> Hull_Tree;