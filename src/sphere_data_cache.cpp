#include "sphere_data_cache.hpp"
#include "filesystem.h"
#include <Windows.h>
#include <fstream>
#include "globals.hpp"
#include "utils.hpp"

SphereDataCache::SphereDataCache(std::string label, int maxCapacity, int numFeatures)
	: sphereDataCacheCapacity(maxCapacity)
	, scanLabel(label)
	, renderedFrameNumber(0)
	, maxNumFeatures(numFeatures)
{
	for (int i = 0; i < sphereDataCacheCapacity; i++) {
		cachedSphericalTextures.push_back(std::make_shared<GLTexture>());
		lastFrameUsed.push_back(-1);
		cachedCubemapImgs.push_back(cv::Mat());

		cachedCubemapKeypoints.push_back(std::vector<std::vector<cv::KeyPoint>>());
		cachedCubemapDescriptors.push_back(std::vector<cv::Mat>());

		orb = cv::ORB::create();
		orb->setMaxFeatures(maxNumFeatures);
	}
}

std::string SphereDataCache::GetPanoFramePath(int frameIndex) {
	auto frameLabel = string_format("pano_frame%05d", frameIndex + 1); // frame images are 1-indexed
	auto framePath = FileSystem::getPath(string_format("resources/scans/%s/pano_frames/%s.jpg", scanLabel.c_str(), frameLabel.c_str()));
	return framePath;
}

std::string SphereDataCache::GetPanoThumbPath(int thumbIndex) {
	auto thumbLabel = string_format("pano_thumb%05d", thumbIndex + 1); // frame images are 1-indexed
	auto thumbPath = FileSystem::getPath(string_format("resources/scans/%s/pano_thumbs/%s.jpg", scanLabel.c_str(), thumbLabel.c_str()));
	return thumbPath;
}

std::string SphereDataCache::GetCubemapPath(int frameIndex) {
	auto cubemapLabel = string_format("pano_frame%05d_cubemap", frameIndex + 1); // frame images are 1-indexed
	auto cubemapPath = FileSystem::getPath(string_format("resources/scans/%s/pano_frames/cubemaps/%s.jpg", scanLabel.c_str(), cubemapLabel.c_str()));
	return cubemapPath;
}

void SphereDataCache::IncrementFrameNumber() 
{
	renderedFrameNumber++;
}

cv::Mat SphereDataCache::GetCubemapImgForSphericalCameraFrame(int sphereCameraFrame)
{
	CacheSphereDataIfNeededForSphericalCameraFrame(sphereCameraFrame);

	// at this point, assume that the cubemap img has been loaded correctly

	int sphereCacheIndex = sphereFrameToActiveSphereData[sphereCameraFrame];
	lastFrameUsed[sphereCacheIndex] = renderedFrameNumber;

	cv::Mat cachedCubemapImg = cachedCubemapImgs[sphereCacheIndex];

	return cachedCubemapImg;
}

std::vector<cv::Mat> SphereDataCache::GetCubemapDescriptorsForSphericalCameraFrame(int sphereCameraFrame) 
{
	CacheSphereDataIfNeededForSphericalCameraFrame(sphereCameraFrame);

	int sphereCacheIndex = sphereFrameToActiveSphereData[sphereCameraFrame];
	lastFrameUsed[sphereCacheIndex] = renderedFrameNumber;

	return cachedCubemapDescriptors[sphereCacheIndex];
}

std::vector<std::vector<cv::KeyPoint>> SphereDataCache::GetCubemapKeypointsForSphericalCameraFrame(int sphereCameraFrame)
{
	CacheSphereDataIfNeededForSphericalCameraFrame(sphereCameraFrame);

	int sphereCacheIndex = sphereFrameToActiveSphereData[sphereCameraFrame];
	lastFrameUsed[sphereCacheIndex] = renderedFrameNumber;

	return cachedCubemapKeypoints[sphereCacheIndex];
}

std::shared_ptr<GLTexture> SphereDataCache::GetSphericalTextureForSphericalCameraFrame(int sphereCameraFrame)
{
	CacheSphereDataIfNeededForSphericalCameraFrame(sphereCameraFrame);

	// at this point, assume that the texture for this frame has been loaded correctly

	int sphereCacheIndex = sphereFrameToActiveSphereData[sphereCameraFrame];
	lastFrameUsed[sphereCacheIndex] = renderedFrameNumber;

	std::shared_ptr<GLTexture> cachedSphericalTexture = cachedSphericalTextures[sphereCacheIndex];

	return cachedSphericalTexture;
}

// https://stackoverflow.com/questions/12774207/fastest-way-to-check-if-a-file-exist-using-standard-c-c11-c
inline bool does_file_exist(const std::string& name) {
	struct stat buffer;
	return (stat(name.c_str(), &buffer) == 0);
}

void SphereDataCache::LoadOrGenerateKeypoints(int sphereCameraFrame, std::vector<std::vector<cv::KeyPoint>> & kp_for_each_side, std::vector<cv::Mat> & desc_for_each_side, cv::Mat & cubemapImg) {
	// first try to load keypoint and descriptor data from file.
	// if the data does not exist, generate it by using the provided cubemapImg.
	// then save it to file before returning.

	std::string kpDescBaseDirectory = FileSystem::getPath(string_format("resources/scans/%s/pano_frames/cubemaps/cached_features", scanLabel.c_str()));

	kp_for_each_side.clear();
	desc_for_each_side.clear();

	for (int sideIdx = 0; sideIdx < 6; sideIdx++) {

		std::vector<cv::KeyPoint> kp_for_this_side;
		cv::Mat desc_for_this_side;	// descriptors

		// frame images are 1-indexed
		std::string featuresFilePath = string_format("%s/pano_frame%05d_orb%d_side%d.yml", kpDescBaseDirectory.c_str(), sphereCameraFrame + 1, maxNumFeatures, sideIdx);

		if (!does_file_exist(featuresFilePath)) {

			cubeSidePixels = cubemapImg.rows;

			cv::Mat cubeSideImg = cubemapImg(cv::Rect(cubeSidePixels*sideIdx, 0, cubeSidePixels, cubeSidePixels));

			
			orb->detectAndCompute(cubeSideImg, cv::noArray(), kp_for_this_side, desc_for_this_side);
			std::cout << "detected " << kp_for_this_side.size() << " keypoints" << std::endl;

			// create directory for cached_features if it doesn't exist
			if (CreateDirectory(kpDescBaseDirectory.c_str(), NULL) ||
				ERROR_ALREADY_EXISTS == GetLastError())
			{
				
				// write features to file
				cv::FileStorage features_fs(featuresFilePath, cv::FileStorage::WRITE);

				features_fs << "kp" << kp_for_this_side;
				features_fs << "desc" << desc_for_this_side;

				features_fs.release();

				std::cout << "saved feature data to " << featuresFilePath << std::endl;
			}
			else
			{
				std::cerr << "ERROR: failed to create directory " << kpDescBaseDirectory << std::endl;
			}
		}
		else {
			cv::FileStorage features_fs(featuresFilePath, cv::FileStorage::READ);
			
			features_fs["kp"] >> kp_for_this_side;
			features_fs["desc"] >> desc_for_this_side;

			features_fs.release();

			std::cout << "loaded feature data from " << featuresFilePath << ", got " << kp_for_this_side.size() << " keypoints" << std::endl;
		}

		kp_for_each_side.push_back(kp_for_this_side);
		desc_for_each_side.push_back(desc_for_this_side);
	}
}

void SphereDataCache::CacheSphereDataIfNeededForSphericalCameraFrame(int sphereCameraFrame)
{
	if (sphereFrameToActiveSphereData.find(sphereCameraFrame) == sphereFrameToActiveSphereData.end()) {
		// active sphere data not found for frame "sphereCameraFrame"... load it in.

		int sphereCacheIndexToReplace = std::distance(lastFrameUsed.begin(), std::min_element(lastFrameUsed.begin(), lastFrameUsed.end()));

		std::cout << "loading..." << std::endl;

		// load spherical texture data
		cachedSphericalTextures[sphereCacheIndexToReplace]->load(GetPanoFramePath(sphereCameraFrame));

		// load cubemap image
		cv::Mat loadedCubemapImg = cv::imread(GetCubemapPath(sphereCameraFrame));
		cachedCubemapImgs[sphereCacheIndexToReplace] = loadedCubemapImg;

		// load or generate keypoints/descriptors
		std::vector<std::vector<cv::KeyPoint>> kp_for_each_side;
		std::vector<cv::Mat> desc_for_each_side;	// descriptors
		LoadOrGenerateKeypoints(sphereCameraFrame, kp_for_each_side, desc_for_each_side, loadedCubemapImg);
		cachedCubemapKeypoints[sphereCacheIndexToReplace] = kp_for_each_side;
		cachedCubemapDescriptors[sphereCacheIndexToReplace] = desc_for_each_side;





		std::cout << "loaded, placed in cache index " << sphereCacheIndexToReplace << std::endl;

		sphereFrameToActiveSphereData[sphereCameraFrame] = sphereCacheIndexToReplace;
		std::cout << "updated record: frame " << sphereCameraFrame << " --> texture " << sphereCacheIndexToReplace << std::endl;

		if (activeSphereDataToFrames.find(sphereCacheIndexToReplace) != activeSphereDataToFrames.end()) {
			int oldFrameForThisSphereData = activeSphereDataToFrames[sphereCacheIndexToReplace];	// the sphere frame that used to be stored at this texture location

			std::cout << "this cache index (" << sphereCacheIndexToReplace << ") was previously used for frame " << oldFrameForThisSphereData << std::endl;
			std::cout << "erasing this record" << std::endl;

			sphereFrameToActiveSphereData.erase(oldFrameForThisSphereData);
		}

		activeSphereDataToFrames[sphereCacheIndexToReplace] = sphereCameraFrame;
		std::cout << "updated record: cache index " << sphereCacheIndexToReplace << " --> frame " << sphereCameraFrame << std::endl;
	}
}
