#include "framebuffer.hpp"

Framebuffer::Framebuffer(int w, int h, ImageFormat imageFormat, GLenum target, GLenum filter, bool textureArray, int numLayers)
{
	std::cout << "creating framebuffer of size " << w << " x " << h << " and " << numLayers << " layers" << std::endl;

	this->usingTextureArray = textureArray;
	this->numLayers = numLayers;

	this->width = w;
	this->height = h;

	// initing framebuffer
	GL_CHECK(glGenFramebuffers(1, &framebufferName));
	GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, framebufferName));

	// texture to render to
	GL_CHECK(glGenTextures(1, &renderedTexture2D));

	std::cout << "renderedTexture2D: " << renderedTexture2D << std::endl;

	// bind texture
	GL_CHECK(glBindTexture(target, renderedTexture2D));

	// give empty image to opengl
	GL_CHECK(glTexImage2D(target, 0, imageFormat.InternalFormat, width, height, 0, imageFormat.Format, imageFormat.Type, 0));

	GL_CHECK(glTexParameteri(target, GL_TEXTURE_MAG_FILTER, filter));
	GL_CHECK(glTexParameteri(target, GL_TEXTURE_MIN_FILTER, filter));

	if (this->usingTextureArray) {
		GL_CHECK(glGenTextures(1, &renderedTexture2DArray));
		// bind texture
		GL_CHECK(glBindTexture(GL_TEXTURE_2D_ARRAY, renderedTexture2DArray));

		// allocate storage
		GLsizei mipLevelCount = 1;
		GL_CHECK(glTexStorage3D(GL_TEXTURE_2D_ARRAY, mipLevelCount, imageFormat.InternalFormat, width, height, numLayers));

		int numValues = width*height* imageFormat.NumChannels;
		GLubyte* pixels = new GLubyte[numValues];

		// give empty image to opengl
		for (int layerIdx = 0; layerIdx < numLayers; layerIdx++) {
			GL_CHECK(glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, layerIdx, width, height, 1, imageFormat.Format, imageFormat.Type, pixels));
		}

		delete[] pixels;
		
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, filter);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, filter);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	}

	// set up depth buffer
	GL_CHECK(glGenRenderbuffers(1, &depthRenderbuffer));
	glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);

	if (this->usingTextureArray) {
		glBindTexture(GL_TEXTURE_2D_ARRAY, renderedTexture2DArray);
		int layerIdx = 0;
		glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderedTexture2DArray, 0, layerIdx);
	}

	glBindTexture(target, renderedTexture2D);
	// set up color attachment
	GL_CHECK(glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderedTexture2D, 0));

	// set up draw buffers
	GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, DrawBuffers);

	// check framebuffer status
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		std::cout << "ERROR: framebuffer was not successfully created" << std::endl;
	}

	GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, 0));
}

GLuint Framebuffer::texture2D()
{
	return renderedTexture2D;
}

GLuint Framebuffer::texture2DArray()
{
	return renderedTexture2DArray;
}

GLuint Framebuffer::framebufferId()
{
	return framebufferName;
}

void Framebuffer::preRender(bool usingTextureArray, int layerIdx)
{
	if (usingTextureArray) {
		//GL_CHECK(std::cout << "starting preRender for texture array, layer " << layerIdx << std::endl);
	}
	else {
		//GL_CHECK(std::cout << "starting preRender (no texture array)" << std::endl);
	}
	

	GL_CHECK(glGetIntegerv(GL_VIEWPORT, previousViewport));

	GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, framebufferName));

	if (usingTextureArray) {
		GL_CHECK(glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderedTexture2DArray, 0, layerIdx));
	}
	else {
		GL_CHECK(glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderedTexture2D, 0));
	}

	GL_CHECK(glViewport(0, 0, width, height));

	GL_CHECK(glClearColor(0.0f, 0.0f, 0.0f, 1.0f));
	GL_CHECK(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

	//GL_CHECK(std::cout << "ending preRender" << std::endl);
}
void Framebuffer::postRender()
{
	//GL_CHECK(std::cout << "starting postRender" << std::endl);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(previousViewport[0], previousViewport[1], previousViewport[2], previousViewport[3]);
	//GL_CHECK(std::cout << "ending postRender" << std::endl);
}
bool Framebuffer::isUsingTextureArray()
{
	return usingTextureArray;
}
int Framebuffer::getNumLayers()
{
	return numLayers;
}
int Framebuffer::getWidth()
{
	return width;
}
int Framebuffer::getHeight()
{
	return height;
}
;
