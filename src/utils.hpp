#pragma once

#include "glm_includes.hpp"
#include <opencv2/opencv.hpp>
#include "pose.hpp"
#include "globals.hpp"
#include "DataInterface.h"

glm::vec2 spherePointToUV(glm::vec3 pt);

glm::vec3 getKeypointOnSphereFromCubemapKeypoint(float x, float y, int cubeSidePixels, int cubeFaceNumber);

bool LoadNVM(ifstream& in, vector<CameraD>& camera_data, vector<Point3D>& point_data,
	vector<MeasurementPoint2D>& measurements, vector<int>& ptidx, vector<int>& camidx,
	vector<string>& names, vector<int>& ptc, vector<float>& fixedK, vector<int>& featureidx);


void SaveNVM(const char* filename, vector<CameraD>& camera_data, vector<Point3D>& point_data,
	vector<MeasurementPoint2D>& measurements, vector<int>& ptidx, vector<int>& camidx,
	vector<string>& names, vector<int>& ptc, vector<float>& fixedK, vector<int>& featureidx);

std::string getPanoFramePath(std::string scanSessionLabel, int frameIndex);

extern float EPS;


bool LineLineIntersect(
	glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec3 p4, glm::vec3 *pa, glm::vec3 *pb,
	double *mua, double *mub);