#include "path.hpp"

#include <fstream>
#include <iostream>


Path::Path(vector<Pose> poses)
{
	this->poses = poses;

	/*
	this->boundingBoxMin = glm::vec3(poses[0].Position);
	this->boundingBoxMax = glm::vec3(poses[0].Position);


	std::unordered_map<glm::ivec2, float> closestPoseDistances;

	

	for (size_t i = 0; i < poses.size(); i++) {
		auto p = poses[i].Position;	// position of raw pose

		// determine the bounding box of all these poses
		if (p.x > boundingBoxMax.x) {
			boundingBoxMax.x = p.x;
		}
		if (p.y > boundingBoxMax.y) {
			boundingBoxMax.y = p.y;
		}
		if (p.z > boundingBoxMax.z) {
			boundingBoxMax.z = p.z;
		}
		if (p.x < boundingBoxMin.x) {
			boundingBoxMin.x = p.x;
		}
		if (p.y < boundingBoxMin.y) {
			boundingBoxMin.y = p.y;
		}
		if (p.z < boundingBoxMin.z) {
			boundingBoxMin.z = p.z;
		}

		glm::ivec2 cell_id((int)glm::round(p.x / gridSizeMeters), (int)glm::round(p.z/gridSizeMeters));

		glm::vec3 cell_center_position(cell_id.x * gridSizeMeters, p.y, cell_id.y * gridSizeMeters);

		float dist = glm::distance(cell_center_position, p);

		auto it = closestPoseDistances.find(cell_id);
		if (it != closestPoseDistances.end() || dist < closestPoseDistances[cell_id]) {
			closestPoseDistances[cell_id] = dist;
			closestPoseIndices[cell_id] = i;
		}
	}

	// each pair contains a 2D representation of the points in world space (not cell id), and the associated index of the pose itself
	std::vector<std::pair<Point2D, size_t>> triangulation_points;

	for (auto &closestPoseEntry : closestPoseIndices) {
		auto &cell_id = closestPoseEntry.first;
		auto &index = closestPoseEntry.second;

		auto &p = this->poses[index].Position;

		triangulation_points.push_back(std::make_pair(Point2D(p.x, p.z), index));

		//std::cout << "(" << cell_id.x << ", " << cell_id.y << ") --> " << index << std::endl;
	}

	cellCenterTriangulation.insert(triangulation_points.begin(), triangulation_points.end());

	std::cout << "boundingBoxMin: " << boundingBoxMin.x << " " << boundingBoxMin.y << " " << boundingBoxMin.z << std::endl;
	std::cout << "boundingBoxMax: " << boundingBoxMax.x << " " << boundingBoxMax.y << " " << boundingBoxMax.z << std::endl;
	*/


	this->setupPath();
}

void Path::Draw(std::shared_ptr<Shader> shader, int startIndex, int endIndex)
{
	shader->use();

	glBindVertexArray(this->VAO);
	glDrawArrays(GL_LINE_STRIP, startIndex, endIndex);
	glBindVertexArray(0);
}



void Path::setupPath()
{
	//generateKDTree();

	glGenVertexArrays(1, &this->VAO);
	glGenBuffers(1, &this->VBO);

	glBindVertexArray(this->VAO);

	glBindBuffer(GL_ARRAY_BUFFER, this->VBO);

	glBufferData(GL_ARRAY_BUFFER, this->poses.size() * sizeof(Pose), &this->poses[0], GL_STATIC_DRAW);

	GLuint vertexAttribPosition = 0;

	// positions
	glEnableVertexAttribArray(vertexAttribPosition);
	glVertexAttribPointer(vertexAttribPosition, 3, GL_FLOAT, GL_FALSE, sizeof(Pose), (GLvoid*)offsetof(Pose, Position));

	glBindVertexArray(0);
}




void Path::generateKDTree()
{
	size_t N = this->poses.size();

	cloud.pts.resize(N);
	for (size_t i = 0; i < N; i++) {
		cloud.pts[i].x = this->poses[i].Position.x;
		cloud.pts[i].y = this->poses[i].Position.y;
		cloud.pts[i].z = this->poses[i].Position.z;
	}
	
	
	kdTreeIndex = std::make_shared<my_kd_tree_t>(3 /* dim */, cloud, nanoflann::KDTreeSingleIndexAdaptorParams(10));
	kdTreeIndex->buildIndex();
	//my_kd_tree_t index(3 /* dim */, cloud, nanoflann::KDTreeSingleIndexAdaptorParams(10));
	//index.buildIndex();
}

std::shared_ptr<Path> Path::LoadSpherePath(string const & filepath)
{
	std::vector<Pose> spherePoses;

	std::ifstream ifs;

	ifs.open(filepath, std::ifstream::in | std::ifstream::binary);

	std::string timestamp_str;
	std::string m00_str, m01_str, m02_str, m03_str;
	std::string m10_str, m11_str, m12_str, m13_str;
	std::string m20_str, m21_str, m22_str, m23_str;
	std::string m30_str, m31_str, m32_str, m33_str;

	while (getline(ifs, timestamp_str, ',')) {

		glm::mat4 m;

		getline(ifs, m00_str, ',');
		getline(ifs, m01_str, ',');
		getline(ifs, m02_str, ',');
		getline(ifs, m03_str, ',');

		getline(ifs, m10_str, ',');
		getline(ifs, m11_str, ',');
		getline(ifs, m12_str, ',');
		getline(ifs, m13_str, ',');

		getline(ifs, m20_str, ',');
		getline(ifs, m21_str, ',');
		getline(ifs, m22_str, ',');
		getline(ifs, m23_str, ',');

		getline(ifs, m30_str, ',');
		getline(ifs, m31_str, ',');
		getline(ifs, m32_str, ',');
		getline(ifs, m33_str);

		m[0][0] = stof(m00_str);
		m[1][0] = stof(m01_str);
		m[2][0] = stof(m02_str);
		m[3][0] = stof(m03_str);

		m[0][1] = stof(m10_str);
		m[1][1] = stof(m11_str);
		m[2][1] = stof(m12_str);
		m[3][1] = stof(m13_str);

		m[0][2] = stof(m20_str);
		m[1][2] = stof(m21_str);
		m[2][2] = stof(m22_str);
		m[3][2] = stof(m23_str);

		m[0][3] = stof(m30_str);
		m[1][3] = stof(m31_str);
		m[2][3] = stof(m32_str);
		m[3][3] = stof(m33_str);

		glm::mat4 ModelMatrix = m;

		glm::mat3 RotationMatrix(ModelMatrix);

		glm::vec3 pos = glm::column(ModelMatrix, 3);
		glm::quat RotationQuat(RotationMatrix);

		Pose pose(pos, RotationQuat, RotationMatrix);

		spherePoses.push_back(pose);
	}

	ifs.close();

	return std::make_shared<Path>(spherePoses);
}

size_t Path::findNearestNeighborIndex(glm::vec3 queryPosition)
{
	float query_pt[3] = { queryPosition.x, queryPosition.y, queryPosition.z };

	const size_t num_results = 1;
	size_t ret_index;
	float out_dist_sqr;
	nanoflann::KNNResultSet<float> resultSet(num_results);
	resultSet.init(&ret_index, &out_dist_sqr);
	kdTreeIndex->findNeighbors(resultSet, &query_pt[0], nanoflann::SearchParams(10));

	return ret_index;
}

size_t Path::findMostSimilarNeighborIndex(glm::vec3 queryPosition, glm::vec3 queryForwardVector, float directionWeight, float scoreGridSizeMeters)
{
	float positionWeight = 1.0f - directionWeight;

	float query_pt[3] = { queryPosition.x, queryPosition.y, queryPosition.z };

	const float search_radius = scoreGridSizeMeters; // meters

	std::vector<std::pair<size_t, float>> ret_matches;

	nanoflann::SearchParams params;

	const size_t nMatches = kdTreeIndex->radiusSearch(&query_pt[0], search_radius, ret_matches, params);

	//std::cout << "radius search, radius = " << search_radius << ", " << nMatches << " matches" << std::endl;

	if (nMatches == 0) {
		return findNearestNeighborIndex(queryPosition);
	}

	std::vector<std::pair<size_t, float>> scores; // each pair is <ret_index, score>

	for (size_t i = 0; i < nMatches; i++) {
		size_t ret_index = ret_matches[i].first;
		float ret_distance = ret_matches[i].second;

		Pose ret_pose = this->poses[ret_index];

		glm::mat4 poseRotationMatrix = ret_pose.RotationMatrix;

		glm::vec4 rotatedForwardVector = poseRotationMatrix * glm::vec4(0.0f, 0.0f, -1.0f, 0.0f);

		auto dotProduct = glm::dot(glm::vec4(glm::normalize(queryForwardVector), 0), glm::normalize(rotatedForwardVector));

		float distanceScore = 1.0f - (ret_distance / search_radius);
		float directionScore = dotProduct;

		float score = positionWeight * distanceScore + directionWeight * directionScore;

		scores.emplace_back(ret_index, score);
	}

	// sort by score, ascending
	sort(scores.begin(), scores.end(), [](std::pair<size_t, float> item1, std::pair<size_t, float> item2) { return item1.second < item2.second; });

	auto highestScoringPair = scores.back();
	auto highestScoringIndex = highestScoringPair.first;

	return highestScoringIndex;
}
