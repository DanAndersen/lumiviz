#// GLFW
//
#if defined(NANOGUI_GLAD)
#if defined(NANOGUI_SHARED) && !defined(GLAD_GLAPI_EXPORT)
#define GLAD_GLAPI_EXPORT
#endif

#include <glad/glad.h>
#else
#if defined(__APPLE__)
#define GLFW_INCLUDE_GLCOREARB
#else
#define GL_GLEXT_PROTOTYPES
#endif
#endif

#include <GLFW/glfw3.h>

#include "globals.hpp"
#include "utils.hpp"

#include <nanogui/nanogui.h>
#include <iostream>
#include <memory>

#include "common.hpp"

#include "shader.hpp"
#include "mesh.hpp"
#include "model.hpp"

#include "filesystem.h"

#include "camera.hpp"

#include "path.hpp"

#include <opencv2/opencv.hpp>

#include "gltexture.hpp"

#include "framebuffer.hpp"
#include "cubemap.hpp"

#include <stb_image.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/calib3d.hpp>

#include "yaml-cpp/yaml.h"

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/point_generators_3.h>
#include <CGAL/algorithm.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Polyhedron_items_with_id_3.h> 
#include <CGAL/convex_hull_3.h>
#include <vector>
#include <CGAL/AABB_tree.h>
#include <CGAL/AABB_traits.h>
#include <CGAL/AABB_triangle_primitive.h>

#include <unordered_set>

typedef CGAL::Exact_predicates_inexact_constructions_kernel  Hull_K;
typedef CGAL::Polyhedron_3<Hull_K, CGAL::Polyhedron_items_with_id_3> Polyhedron_3;
typedef Hull_K::Segment_3                              Segment_3;
typedef Hull_K::Point_3                                Point_3;
typedef Hull_K::Ray_3                                Ray_3;
typedef Hull_K::Triangle_3                                Triangle_3;
typedef CGAL::Creator_uniform_3<double, Point_3>  PointCreator;
typedef std::list<Triangle_3>::iterator Hull_Iterator;
typedef CGAL::AABB_triangle_primitive<Hull_K, Hull_Iterator> Hull_Primitive;
typedef CGAL::AABB_traits<Hull_K, Hull_Primitive> Hull_AABB_triangle_traits;
typedef CGAL::AABB_tree<Hull_AABB_triangle_traits> Hull_Tree;


Camera thirdPersonCamera(glm::vec3(0.0f, 0.0f, 0.0f)); // camera for moving around the scene

using namespace nanogui;


int screenWidth = 800;
int screenHeight = 800;

GLFWwindow* window;

Screen *screen = nullptr;

bool cameraMouseControlsActive = false;
GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;
bool keys[1024];
GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;

std::shared_ptr<Model> testHoloRoom;
std::shared_ptr<Model> arrowModel;
std::shared_ptr<Model> cubeModel;
std::shared_ptr<Model> sphericalCameraFrustumModel;

std::string currentTripletLabel;
std::vector<int> currentTripletFrameNumbers;

std::vector<float> bary;

Delaunay cellCenterTriangulation;

std::shared_ptr<Shader> meshShader;

std::unordered_map<int, CameraD> frameNumbersToCubemapCameras;	// 0-indexed
std::unordered_map<int, glm::vec3> frameNumbersToCubemapCameraCenters;	// 0-indexed
std::unordered_map<int, glm::mat4> frameNumbersToCameraModelMatrices;	// 0-indexed
std::vector<std::vector<int>> triangulationFrameNumbers;	// 0-indexed
std::unordered_set<int> uniqueFrameNumbers;	// 0-indexed



glm::mat4 hlToSphereTransformMatrix;
glm::mat4 sphereToHlTransformMatrix;

std::vector<glm::vec3> intersectionPts;

glm::mat4 testExtraFixRotationHlSphereMat;



void DrawThirdPersonVisualization() {
	//==================
	// set up the third person camera
	glm::mat4 modelIdentity;
	glm::mat4 thirdPersonCameraProjection = glm::perspective(thirdPersonCamera.Zoom, (float)screenWidth / (float)screenHeight, 0.1f, 100.0f);
	glm::mat4 thirdPersonCameraView = thirdPersonCamera.GetViewMatrix();

	//==================
	// initialize common uniforms
	meshShader->use();

	glUniform3f(glGetUniformLocation(meshShader->Program, "lightPos"), thirdPersonCamera.Position.x, thirdPersonCamera.Position.y, thirdPersonCamera.Position.z);

	glUniform3f(glGetUniformLocation(meshShader->Program, "lightColor"), 1.0f, 1.0f, 1.0f);
	glUniform1i(glGetUniformLocation(meshShader->Program, "lightingEnabled"), true);
	glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.75f, 0.75f, 0.75f);

	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(thirdPersonCameraProjection));
	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(thirdPersonCameraView));

	//==================

	
	

	for (auto & entry : frameNumbersToCameraModelMatrices) {
		// draw the raw sphere cameras
		glm::mat4 & raw_cam_model = entry.second;
		glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 1.0f, 0.5f);
		glm::mat4 scaledRawCamModel = glm::scale(raw_cam_model, glm::vec3(0.1f, 0.1f, 0.1f));
		glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(scaledRawCamModel));
		sphericalCameraFrustumModel->Draw(meshShader);



		// draw the holo cameras relative to it
		glm::mat4 holoCamModel = raw_cam_model * glm::inverse(hlToSphereTransformMatrix);
		glm::mat4 scaledHoloCamModel = glm::scale(holoCamModel, glm::vec3(0.1f, 0.1f, 0.1f));
		glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 0.0f, 0.0f);
		glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(scaledHoloCamModel));
		sphericalCameraFrustumModel->Draw(meshShader);
	}


	//==================

	
	glUniform1i(glGetUniformLocation(meshShader->Program, "lightingEnabled"), true);

	// draw the room
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);

	glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.0f, 1.0f, 1.0f);

	glm::mat4 holoModelMat = modelIdentity * sphereToHlTransformMatrix;

	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(holoModelMat));
	testHoloRoom->Draw(meshShader);

	glDisable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	// ==========

	// draw intersection rays

	glm::vec3 currentCamCenter = frameNumbersToCubemapCameraCenters[currentTripletFrameNumbers[0], currentTripletFrameNumbers[1], currentTripletFrameNumbers[2]];
	glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 1.0f, 1.0f);
	for (auto & intersectionPt : intersectionPts) {
		glm::mat4 intersectionRayModelMat = glm::inverse(glm::lookAt(currentCamCenter, intersectionPt, glm::vec3(0, 1, 0)));

		float dist = glm::distance(currentCamCenter, intersectionPt);

		intersectionRayModelMat = glm::scale(intersectionRayModelMat, glm::vec3(0.1f, 0.1f, dist));
		glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(intersectionRayModelMat));
		arrowModel->Draw(meshShader);
	}
}





int initWindow() {
	glfwInit();

	glfwSetTime(0);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glfwWindowHint(GLFW_SAMPLES, 0);
	glfwWindowHint(GLFW_RED_BITS, 8);
	glfwWindowHint(GLFW_GREEN_BITS, 8);
	glfwWindowHint(GLFW_BLUE_BITS, 8);
	glfwWindowHint(GLFW_ALPHA_BITS, 8);
	glfwWindowHint(GLFW_STENCIL_BITS, 8);
	glfwWindowHint(GLFW_DEPTH_BITS, 24);
	glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

	// Create a GLFWwindow object
	window = glfwCreateWindow(screenWidth, screenHeight, "Visualization", nullptr, nullptr);
	if (window == nullptr) {
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

#if defined(NANOGUI_GLAD)
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
		throw std::runtime_error("Could not initialize GLAD!");
	glGetError(); // pull and ignore unhandled errors like GL_INVALID_ENUM
#endif

	glClearColor(0.2f, 0.25f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Create a nanogui screen and pass the glfw pointer to initialize
	screen = new Screen();
	screen->initialize(window, true);

	glfwGetFramebufferSize(window, &screenWidth, &screenHeight);
	glViewport(0, 0, screenWidth, screenHeight);
	glfwSwapInterval(0);
	glfwSwapBuffers(window);

	// Create nanogui gui
	bool enabled = true;
	FormHelper *gui = new FormHelper(screen);
	nanogui::ref<Window> nanoguiWindow = gui->addWindow(Eigen::Vector2i(10, 10), "Form helper example");




	screen->setVisible(true);
	screen->performLayout();



	glfwSetCursorPosCallback(window,
		[](GLFWwindow *, double x, double y) {
		bool cursorPosConsumedByUI = screen->cursorPosCallbackEvent(x, y);
		if (!cursorPosConsumedByUI) {
			// use mouse movement for camera movement

			if (firstMouse)
			{
				lastX = x;
				lastY = y;
				firstMouse = false;
			}

			GLfloat xoffset = x - lastX;
			GLfloat yoffset = lastY - y;

			lastX = x;
			lastY = y;

			if (cameraMouseControlsActive) {
				thirdPersonCamera.ProcessMouseMovement(xoffset, yoffset);
			}
		}
	}
	);

	glfwSetMouseButtonCallback(window,
		[](GLFWwindow *, int button, int action, int modifiers) {
		bool mouseButtonConsumedByUI = screen->mouseButtonCallbackEvent(button, action, modifiers);
		if (!mouseButtonConsumedByUI) {

			if (button == GLFW_MOUSE_BUTTON_RIGHT) {
				if (action == GLFW_PRESS) {
					cameraMouseControlsActive = true;
				}
				else if (action == GLFW_RELEASE) {
					cameraMouseControlsActive = false;
				}
			}
		}
	}
	);

	glfwSetKeyCallback(window,
		[](GLFWwindow *, int key, int scancode, int action, int mods) {
		bool keyConsumedByUI = screen->keyCallbackEvent(key, scancode, action, mods);
		if (!keyConsumedByUI) {
			// treat the key input as camera controls

			if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
				glfwSetWindowShouldClose(window, GL_TRUE);

			if (action == GLFW_PRESS)
				keys[key] = true;
			else if (action == GLFW_RELEASE) {
				keys[key] = false;


			}
		}
	}
	);

	glfwSetCharCallback(window,
		[](GLFWwindow *, unsigned int codepoint) {
		screen->charCallbackEvent(codepoint);
	}
	);

	glfwSetDropCallback(window,
		[](GLFWwindow *, int count, const char **filenames) {
		screen->dropCallbackEvent(count, filenames);
	}
	);

	glfwSetScrollCallback(window,
		[](GLFWwindow *, double x, double y) {
		screen->scrollCallbackEvent(x, y);
	}
	);

	glfwSetFramebufferSizeCallback(window,
		[](GLFWwindow *, int width, int height) {
		screenWidth = width;
		screenHeight = height;
		screen->resizeCallbackEvent(screenWidth, screenHeight);

		glViewport(0, 0, screenWidth, screenHeight);
	}
	);
}


void draw() {
	glViewport(0, 0, screenWidth, screenHeight);

	DrawThirdPersonVisualization();
}


// Moves/alters the camera positions based on user input
void doMovement()
{
	Camera* camera = &thirdPersonCamera;

	bool forceHorizontalMotion = false;

	// Camera controls
	if (keys[GLFW_KEY_W])
		camera->ProcessKeyboard(FORWARD, deltaTime, forceHorizontalMotion);
	if (keys[GLFW_KEY_S])
		camera->ProcessKeyboard(BACKWARD, deltaTime, forceHorizontalMotion);
	if (keys[GLFW_KEY_A])
		camera->ProcessKeyboard(LEFT, deltaTime, forceHorizontalMotion);
	if (keys[GLFW_KEY_D])
		camera->ProcessKeyboard(RIGHT, deltaTime, forceHorizontalMotion);
	if (keys[GLFW_KEY_Q])
		camera->ProcessKeyboard(DOWN, deltaTime, forceHorizontalMotion);
	if (keys[GLFW_KEY_E])
		camera->ProcessKeyboard(UP, deltaTime, forceHorizontalMotion);



}



void gameloop() {
	// Game loop
	while (!glfwWindowShouldClose(window)) {
		// Set frame time
		GLfloat currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		// note that we have to enable the depth test per-frame; it looks like nanogui disables it in order to draw the UI
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);

		glClearColor(0.2f, 0.25f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Check if any events have been activated (key pressed, mouse moved etc.) and call corresponding response functions
		glfwPollEvents();
		doMovement();
		

		draw();

		// Draw nanogui
		screen->drawContents();
		screen->drawWidgets();


		glfwSwapBuffers(window);

	}
}



void loadCubemapData() {
	// load in triangulation data, get refs to each triplet both by pose index and by frame numbe
	std::string refinedSphereFrameNumberTriangulationPath = FileSystem::getPath(string_format("resources/scans/%s/sphere_pose_triangulation_refined_frame_numbers.yaml", SCAN_SESSION_LABEL.c_str()));

	YAML::Node refinedSphereFrameNumberTriangulation = YAML::LoadFile(refinedSphereFrameNumberTriangulationPath);

	triangulationFrameNumbers.clear();
	uniqueFrameNumbers.clear();

	auto triangulationSphereFrameNumbersNode = refinedSphereFrameNumberTriangulation["triangulation_indices"];
	for (int i = 0; i < triangulationSphereFrameNumbersNode.size(); i++) {
		auto tripletSphereFrameNumbersNode = triangulationSphereFrameNumbersNode[i];

		std::vector<int> tripletFrameNumbers;
		for (int j = 0; j < 3; j++) {
			auto frameNumber = tripletSphereFrameNumbersNode[j].as<int>() - 1;	// go from 1-indexed to 0-indexed
			tripletFrameNumbers.push_back(frameNumber);
			uniqueFrameNumbers.insert(frameNumber);
		}
		std::sort(tripletFrameNumbers.begin(), tripletFrameNumbers.end());

		triangulationFrameNumbers.push_back(tripletFrameNumbers);
	}

	frameNumbersToCubemapCameras.clear();

	std::string refinedCubemapDataPath = FileSystem::getPath(string_format("resources/scans/%s/refined_cubemap_data.csv", SCAN_SESSION_LABEL.c_str()));

	std::ifstream ifs;

	ifs.open(refinedCubemapDataPath, std::ifstream::in | std::ifstream::binary);

	int frame_number;	// NOTE: this is 1-indexed!
	double cam_center_x, cam_center_y, cam_center_z;
	double quat_w, quat_x, quat_y, quat_z;

	std::vector<std::pair<Point2D, size_t>> triangulation_points;

	while (ifs >> frame_number >> cam_center_x >> cam_center_y >> cam_center_z >> quat_w >> quat_x >> quat_y >> quat_z) {
		int zero_indexed_frame_number = frame_number - 1;

		double cam_center[3]{ cam_center_x, cam_center_y, cam_center_z };
		double q[4]{ quat_w, quat_x, quat_y, quat_z };

		CameraD cubemapCam;
		cubemapCam.SetFocalLength(cubeSidePixels / 2.0f);
		cubemapCam.SetQuaternionRotation(q);
		cubemapCam.SetCameraCenterAfterRotation(cam_center);

		frameNumbersToCubemapCameras.emplace(zero_indexed_frame_number, cubemapCam);
		frameNumbersToCubemapCameraCenters.emplace(zero_indexed_frame_number, glm::vec3(cam_center_x, cam_center_y, cam_center_z));

		// generate the model matrix representing this camera
		double raw_cam_rotation[16];
		cubemapCam.GetMatrixRotation(raw_cam_rotation);
		float raw_cam_rotation_float[16];
		std::copy(raw_cam_rotation, raw_cam_rotation + 16, raw_cam_rotation_float);
		glm::mat3 rot_matrix = glm::make_mat3(raw_cam_rotation_float);
		double raw_t[3];
		cubemapCam.GetTranslation(raw_t);
		float raw_t_float[3];
		std::copy(raw_t, raw_t + 3, raw_t_float);
		glm::mat4 raw_cam_model(rot_matrix);
		raw_cam_model = glm::translate(raw_cam_model, -glm::make_vec3(raw_t_float));

		glm::mat3 foo(raw_cam_model);

		frameNumbersToCameraModelMatrices.emplace(zero_indexed_frame_number, raw_cam_model);

		// add to triangulation
		triangulation_points.push_back(std::make_pair(Point2D(cam_center[0], cam_center[2]), zero_indexed_frame_number));
	}

	// set up triangulation to determine what triplet we are currently in
	cellCenterTriangulation.insert(triangulation_points.begin(), triangulation_points.end());

	ifs.close();
}


inline std::string GetFrameNumberTripletLabel(int frameNumber0, int frameNumber1, int frameNumber2)
{
	std::vector<int> sortedFrameNumbers = { frameNumber0, frameNumber1, frameNumber2 };
	std::sort(sortedFrameNumbers.begin(), sortedFrameNumbers.end());

	return string_format("%d_%d_%d", sortedFrameNumbers[0], sortedFrameNumbers[1], sortedFrameNumbers[2]);
}

void initShaders() {
	meshShader = std::make_shared<Shader>("mesh.vert", "mesh.frag");
}

void initModels() {
	arrowModel = std::make_shared<Model>(FileSystem::getPath("resources/meshes/arrow.obj").c_str());
	cubeModel = std::make_shared<Model>(FileSystem::getPath("resources/meshes/cube.obj").c_str());
	sphericalCameraFrustumModel = Model::CreateFrustum(90.0f, 1.0f, 0.25f);
}


glm::mat4 LoadRowMajorMat4FromCsv(string const & filepath) {
	glm::mat4 m;

	std::ifstream ifs;
	ifs.open(filepath, std::ifstream::in | std::ifstream::binary);

	string m00_str, m01_str, m02_str, m03_str;
	string m10_str, m11_str, m12_str, m13_str;
	string m20_str, m21_str, m22_str, m23_str;
	string m30_str, m31_str, m32_str, m33_str;

	getline(ifs, m00_str, ',');
	getline(ifs, m01_str, ',');
	getline(ifs, m02_str, ',');
	getline(ifs, m03_str);

	getline(ifs, m10_str, ',');
	getline(ifs, m11_str, ',');
	getline(ifs, m12_str, ',');
	getline(ifs, m13_str);

	getline(ifs, m20_str, ',');
	getline(ifs, m21_str, ',');
	getline(ifs, m22_str, ',');
	getline(ifs, m23_str);

	getline(ifs, m30_str, ',');
	getline(ifs, m31_str, ',');
	getline(ifs, m32_str, ',');
	getline(ifs, m33_str);

	m[0][0] = stof(m00_str);
	m[1][0] = stof(m01_str);
	m[2][0] = stof(m02_str);
	m[3][0] = stof(m03_str);

	m[0][1] = stof(m10_str);
	m[1][1] = stof(m11_str);
	m[2][1] = stof(m12_str);
	m[3][1] = stof(m13_str);

	m[0][2] = stof(m20_str);
	m[1][2] = stof(m21_str);
	m[2][2] = stof(m22_str);
	m[3][2] = stof(m23_str);

	m[0][3] = stof(m30_str);
	m[1][3] = stof(m31_str);
	m[2][3] = stof(m32_str);
	m[3][3] = stof(m33_str);

	return m;
}

int main(int /* argc */, char ** /* argv */) {

	initWindow(); // needed for opengl stuff

	initModels();
	initShaders();

	std::cout << "this script loads in the holo room and uses it for correspondences" << std::endl;

	ScanSession currentScan(SCAN_SESSION_LABEL);

	string scanLabel = currentScan.Label;



	hlToSphereTransformMatrix = LoadRowMajorMat4FromCsv(FileSystem::getPath(string_format("resources/scans/%s/hl_sphere_transform_matrix.csv", scanLabel.c_str())).c_str());
	cout << "hlToSphereTransformMatrix: " << glm::to_string(hlToSphereTransformMatrix) << endl;

	float testSphereRotationX = 8.0f;
	float testSphereRotationY = 0.0f;
	float testSphereRotationZ = -1.0f;


	// rotation of the camera/hololens, representing ZXY euler angles in degrees 
	// (so z degrees about z axis, then x degrees about x axis, then y degrees about y axis, in that order)
	float testZRad = glm::radians(testSphereRotationZ);
	float testXRad = glm::radians(testSphereRotationX);
	float testYRad = glm::radians(testSphereRotationY);

	testExtraFixRotationHlSphereMat = glm::mat4();	// generated at runtime from user-set rotations
	testExtraFixRotationHlSphereMat = glm::rotate(testExtraFixRotationHlSphereMat, testZRad, glm::vec3(0.0f, 0.0f, 1.0f));
	testExtraFixRotationHlSphereMat = glm::rotate(testExtraFixRotationHlSphereMat, testXRad, glm::vec3(1.0f, 0.0f, 0.0f));
	testExtraFixRotationHlSphereMat = glm::rotate(testExtraFixRotationHlSphereMat, testYRad, glm::vec3(0.0f, 1.0f, 0.0f));

	
	// update the hlToSphereTransformMatrix
	//hlToSphereTransformMatrix = hlToSphereTransformMatrix * glm::inverse(testExtraFixRotationHlSphereMat);
	//hlToSphereTransformMatrix = hlToSphereTransformMatrix * testExtraFixRotationHlSphereMat;
	hlToSphereTransformMatrix = glm::mat4();
	hlToSphereTransformMatrix = glm::translate(hlToSphereTransformMatrix, glm::vec3(0, 0.160f, 0));





	sphereToHlTransformMatrix = glm::inverse(hlToSphereTransformMatrix);

	std::string roomFilepath = string_format("resources/scans/%s/%s_mesh.room", scanLabel.c_str(), scanLabel.c_str());
	std::cout << "roomFilepath: " << roomFilepath << std::endl;

	std::cout << "loading in holo room..." << std::endl;
	testHoloRoom = Model::LoadHoloRoom(FileSystem::getPath(roomFilepath).c_str());

	std::cout << "generating AABB tree..." << std::endl;
	testHoloRoom->GenerateAABBTree();

	std::cout << "done generating AABB tree" << std::endl;



	// now load in the triangulation data, the data about each spherical camera. we want to choose the single triplet that is at the 0,0 location to work on for our test
	std::cout << "loading in cubemap data..." << std::endl;
	loadCubemapData();
	std::cout << "cubemap data loaded" << std::endl;




	/*
	std::cout << "select TEST location at 0,0" << std::endl;

	std::vector<Kernel::FT> unsorted_barycentric_coords;
	unsorted_barycentric_coords.reserve(3);	// alpha, beta, gamma


	Locate_type lt;
	int li;
	CGAL::internal::CC_iterator<CGAL::Compact_container<CGAL::Triangulation_ds_face_base_2<Tds>>, false> face_handle;

	// do the triangulation lookup
	Point2D query_point(0.0f, 0.0f);
	face_handle = cellCenterTriangulation.locate(query_point, lt, li);



	switch (lt)
	{
	case Locate_type::EDGE:
		std::cout << "edge" << std::endl;
		break;
	case Locate_type::FACE:

		{
			// determine the barycentric coordinates of the query point
			Triangle_coordinates triangle_coordinates(face_handle->vertex(0)->point(), face_handle->vertex(1)->point(), face_handle->vertex(2)->point());

			triangle_coordinates(query_point, std::inserter(unsorted_barycentric_coords, unsorted_barycentric_coords.end()));	// calculates the barycentric coords and saves them in the preallocated list



			auto unsorted_frameNumber0 = face_handle->vertex(0)->info();	// 0-indexed
			auto unsorted_frameNumber1 = face_handle->vertex(1)->info();
			auto unsorted_frameNumber2 = face_handle->vertex(2)->info();

			std::vector<std::pair<unsigned int, double>> sortedFrameNumbersToBaryCoords;
			sortedFrameNumbersToBaryCoords.push_back(std::make_pair(unsorted_frameNumber0, unsorted_barycentric_coords[0]));
			sortedFrameNumbersToBaryCoords.push_back(std::make_pair(unsorted_frameNumber1, unsorted_barycentric_coords[1]));
			sortedFrameNumbersToBaryCoords.push_back(std::make_pair(unsorted_frameNumber2, unsorted_barycentric_coords[2]));
			std::sort(sortedFrameNumbersToBaryCoords.begin(), sortedFrameNumbersToBaryCoords.end()); // we refer to triplets by their frame numbers in ascending order

			currentTripletFrameNumbers.clear();
			bary.clear();

			for (auto & pair : sortedFrameNumbersToBaryCoords) {
				currentTripletFrameNumbers.push_back(pair.first);
				bary.push_back(pair.second);
			}

			std::string tripletLabelForThisFrame = GetFrameNumberTripletLabel(currentTripletFrameNumbers[0], currentTripletFrameNumbers[1], currentTripletFrameNumbers[2]);

			currentTripletLabel = tripletLabelForThisFrame;
		}

		break;
	case Locate_type::VERTEX:
		std::cout << "vertex" << std::endl;
		break;
	case Locate_type::OUTSIDE_AFFINE_HULL:
	case Locate_type::OUTSIDE_CONVEX_HULL:
		break;
	default:
		break;
	}



	std::cout << "currentTripletLabel: " << currentTripletLabel << std::endl;
	std::cout << "currentTripletFrameNumbers: " << currentTripletFrameNumbers[0] << " " << currentTripletFrameNumbers[1] << " " << currentTripletFrameNumbers[2] << std::endl;
	*/

	int numTripletsToProcess = triangulationFrameNumbers.size();
	int processedTripletIndex = 0;

	for (auto & tripletFrameNumbers : triangulationFrameNumbers) {
		processedTripletIndex += 1;

		std::cout << "processing triplet " << processedTripletIndex << " / " << numTripletsToProcess << std::endl;

		currentTripletFrameNumbers = tripletFrameNumbers;
		std::sort(currentTripletFrameNumbers.begin(), currentTripletFrameNumbers.end());

		std::string tripletLabelForThisFrame = GetFrameNumberTripletLabel(currentTripletFrameNumbers[0], currentTripletFrameNumbers[1], currentTripletFrameNumbers[2]);
		currentTripletLabel = tripletLabelForThisFrame;

		std::cout << "currentTripletLabel: " << currentTripletLabel << std::endl;
		std::cout << "currentTripletFrameNumbers: " << currentTripletFrameNumbers[0] << " " << currentTripletFrameNumbers[1] << " " << currentTripletFrameNumbers[2] << std::endl;

		intersectionPts.clear();

		// now, let's do a simple raycast from the centroid position to each of the vertices in the holo room and see what the intersection is.
		// if the intersection is very close to the target vertex position, then it hit!
		// otherwise, it was hit by something in front of it
		// in that case... we could just use the position of what was hit in front of it as an additional correspondence world point!

		glm::vec3 tripletCameraCenter0 = frameNumbersToCubemapCameraCenters[currentTripletFrameNumbers[0]];
		glm::vec3 tripletCameraCenter1 = frameNumbersToCubemapCameraCenters[currentTripletFrameNumbers[1]];
		glm::vec3 tripletCameraCenter2 = frameNumbersToCubemapCameraCenters[currentTripletFrameNumbers[2]];

		glm::vec3 tripletCentroid_360coords = (tripletCameraCenter0 + tripletCameraCenter1 + tripletCameraCenter2) / 3.0f;


		glm::vec3 tripletCentroid_hlcoords = hlToSphereTransformMatrix * glm::vec4(tripletCentroid_360coords, 1.0f);


		K_Point rayPointA_hlcoords(tripletCentroid_hlcoords.x, tripletCentroid_hlcoords.y, tripletCentroid_hlcoords.z);





		//int numMeshes = 3;
		// std::cout << "WARNING: only doing a few meshes for testing!" << std::endl;
		int numMeshes = testHoloRoom->meshes.size();

		int decimateFactor = 10;
		int decimateCounter = 0;

		for (int meshIdx = 0; meshIdx < numMeshes; meshIdx++) {
			std::cout << "intersecting with mesh " << meshIdx << " / " << numMeshes << std::endl;

			auto & holoVertices = testHoloRoom->meshes[meshIdx].vertices;

			try {

				for (auto & holoVert_hlcoords : holoVertices) {
					if (decimateCounter < decimateFactor) {
						decimateCounter++;
					}
					else {
						decimateCounter = 0;


						glm::vec3 & vertPos = holoVert_hlcoords.Position;

						K_Point rayPointB_hlcoords(vertPos.x, vertPos.y, vertPos.z);


						K_Ray queryRay(rayPointA_hlcoords, rayPointB_hlcoords);

						auto intersection = testHoloRoom->aabb_tree->first_intersection(queryRay);
						if (intersection) {
							const K_Point* inter_pt_hlcoords = boost::get<K_Point>(&(intersection->first));



							auto poseToVertSqDist = CGAL::squared_distance(rayPointA_hlcoords, rayPointB_hlcoords);
							auto poseToIntersectionSqDist = CGAL::squared_distance(rayPointA_hlcoords, *inter_pt_hlcoords);

							//std::cout << "vertPos: " << glm::to_string(vertPos) << ", intersection_pt: " << (*intersection_pt) << ", dist between pt and intersection: " << distBetweenIntersectionAndVert << std::endl;
							if (std::fabs(poseToVertSqDist - poseToIntersectionSqDist) < 0.001f) {

								glm::vec3 intersection_pt_hlcoords((*inter_pt_hlcoords).x(), (*inter_pt_hlcoords).y(), (*inter_pt_hlcoords).z());

								glm::vec3 intersection_pt_360coords = sphereToHlTransformMatrix * glm::vec4(intersection_pt_hlcoords, 1.0f);

								intersectionPts.push_back(intersection_pt_360coords);
							}

						}
						else {
							//std::cout << "no intersection!" << std::endl;
						}
					}
				}

			}
			catch (...) {
				std::cout << "got some kind of exception, continuing..." << std::endl;
			}



		}

		int frameNumber0 = currentTripletFrameNumbers[0];
		int frameNumber1 = currentTripletFrameNumbers[1];
		int frameNumber2 = currentTripletFrameNumbers[2];



		// now that we have the intersection points, save them to a file that I can use in a python script
		std::string outHoloTripletMeshDataPath = FileSystem::getPath(string_format("resources/scans/%s/holoroom_only_triplet_%d_%d_%d.meshes", SCAN_SESSION_LABEL.c_str(),
			frameNumber0 + 1, frameNumber1 + 1, frameNumber2 + 1));	// going from 0-indexed to 1-indexed

		std::cout << "saving to " << outHoloTripletMeshDataPath << std::endl;

		std::ofstream tripletMesh_ofs;

		tripletMesh_ofs.open(outHoloTripletMeshDataPath, std::ofstream::out | std::ofstream::binary);

		int num_verts = intersectionPts.size();

		tripletMesh_ofs << num_verts;

		tripletMesh_ofs << std::endl;

		for (int i = 0; i < num_verts; i++) {
			auto & vert = intersectionPts[i];

			tripletMesh_ofs << vert.x << " " << vert.y << " " << vert.z << std::endl;
		}




		tripletMesh_ofs.close();









	}


	





	gameloop();

	// Terminate GLFW, clearing any resources allocated by GLFW.
	glfwTerminate();

	return 0;
}