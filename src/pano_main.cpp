// GLFW
//
#if defined(NANOGUI_GLAD)
#if defined(NANOGUI_SHARED) && !defined(GLAD_GLAPI_EXPORT)
#define GLAD_GLAPI_EXPORT
#endif

#include <glad/glad.h>
#else
#if defined(__APPLE__)
#define GLFW_INCLUDE_GLCOREARB
#else
#define GL_GLEXT_PROTOTYPES
#endif
#endif

#include <GLFW/glfw3.h>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/string_cast.hpp>

#include <nanogui/nanogui.h>
#include <iostream>
#include <memory>

#include "common.hpp"

#include "shader.hpp"
#include "mesh.hpp"
#include "model.hpp"

#include "filesystem.h"

#include "camera.hpp"

#include "path.hpp"

#include <opencv2/opencv.hpp>
#include <glm/gtx/vector_angle.hpp>

#include "gltexture.hpp"

#include "framebuffer.hpp"
#include "cubemap.hpp"

#include <stb_image.h>

#include "sphere_data_cache.hpp"
#include "triplet_data_cache.hpp"
#include "triplet_data.hpp"
#include "utils.hpp"

#include "global_caches.hpp"

#include "globals.hpp"

using namespace nanogui;

int screenWidth = 800;
int screenHeight = 800;

enum test_enum {
	Item1 = 0,
	Item2,
	Item3
};

bool bvar = true;
int ivar = 12345678;
double dvar = 3.1415926;
float fvar = (float)dvar;
std::string strval = "A string";
test_enum enumval = Item2;
Color colval(0.5f, 0.5f, 0.7f, 1.f);

Screen *screen = nullptr;

std::shared_ptr<Shader> meshShader;
std::shared_ptr<Shader> pathShader;
std::shared_ptr<Shader> spherePanoShader;
std::shared_ptr<Shader> panoProjectiveTextureMappingShader;
std::shared_ptr<Shader> encodeBarycentricShader;
std::shared_ptr<Shader> equirectangularShader;

std::shared_ptr<Shader> skyboxShader;

std::shared_ptr<Model> locatableCameraFrustumModel;
std::shared_ptr<Model> sphericalCameraFrustumModel;
std::shared_ptr<Model> cubeModel;
std::shared_ptr<Model> quadModel;
std::shared_ptr<Model> testHoloRoom;
std::shared_ptr<Model> testHoloRoomWalkableSpace;

std::shared_ptr<Model> arrowModel;

std::shared_ptr<Model> fullscreenQuadModel;

std::shared_ptr<Model> skyboxModel;

std::shared_ptr<Model> panoSphereModel;
std::shared_ptr<Model> sphereModel;
std::shared_ptr<GLTexture> panoSphereTexture;

Camera thirdPersonCamera(glm::vec3(0.0f, 0.0f, 0.0f)); // camera for moving around the scene
Camera visualizationCamera(glm::vec3(0.0f, 0.0f, 0.0f)); // camera for viewing the scene from predetermined locations
bool controllingVisualizationCamera = false;

std::vector<glm::mat4> corrected_sphere_rotation_matrices;
std::vector<glm::mat4> corrected_inverse_sphere_rotation_matrices;

glm::vec3 lightPos(1.0f, 2.0f, 3.0f);

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

GLFWwindow* window;
bool keys[1024];

GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;

std::vector<std::string> rayDisplayOptions = { "None", "Matches Only", "All" };
std::shared_ptr<nanogui::ComboBox> rayDisplayComboBox;

ImageFormat RGBA8(GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE, 4);
ImageFormat RGBA32(GL_RGBA32F, GL_RGBA, GL_FLOAT, 4);

ImageFormat equirectangularImageFormat = RGBA32;
ImageFormat cubemapImageFormat = RGBA32;
GLfloat *equirectangularPixels;
GLfloat *cubemapFacePixels;

std::shared_ptr<Framebuffer> equirectangularFramebuffer;

std::vector<std::string> visualizationOptions = { "Third Person", "Encoded Mesh", "Pano" };
std::shared_ptr<nanogui::ComboBox> visualizationComboBox;


bool morphingPano = true;

bool cameraMouseControlsActive = false;
bool nearestNeighbor = false;

bool rotationAdjustmentDirty = true;

CameraProfile locatableCameraProfile(896, 504, 48.0f);	// Low power / Low resolution mode for image processing tasks

float locatableCameraAspectRatio = (float)locatableCameraProfile.Width / (float)locatableCameraProfile.Height;
float locatableCameraFovYRadians = glm::radians(locatableCameraProfile.HfovDegrees / locatableCameraAspectRatio);

float testSphereRotationX = 8.0f;
float testSphereRotationY = 0.0f;
float testSphereRotationZ = -1.0f;

glm::mat4 locatableCameraProjection = glm::perspective(locatableCameraFovYRadians, locatableCameraAspectRatio, 0.1f, 100.0f);
glm::mat4 inverseLocatableCameraProjection = glm::inverse(locatableCameraProjection);

int currentSurroundingSphereCameraFrames[3];

ScanSession currentScan(SCAN_SESSION_LABEL);

CameraParameters locatableCamera(locatableCameraProfile.HfovDegrees, locatableCameraAspectRatio, 0.01f, 100.0f);


Slider* cameraPathSlider;

bool renderingSimulatedView = false;
bool printingInfo = false;
float panoramaFOV = 60.0f;
bool projectiveTextureMapping = false;

size_t sphere_pose_indices[3];


std::shared_ptr<TripletDataCache> tripletDataCache;
const int sphereDataCacheSize = 256;
const int tripletDataCacheSize = 256;
const int maxNumFeatures = 4000;

bool showSelectedSpherePoses = true;
bool showFloorTiles = true;

bool showingHullModel = true;

glm::mat4 cubemapProjectionMat;
std::vector<glm::mat4> openglCubemapViewMats;	// view matrices for each side of the cubemap, organized in the order used by OpenGL

int panoSphereNumSegments = 128;
int panoSphereNumRings = panoSphereNumSegments / 2;

std::shared_ptr<Cubemap> interpolatedHullCubemap;
int cubemapRenderTextureSize = 512;


std::shared_ptr<TripletData> currentTripletData;


using imagesDataType = vector<pair<GLTexture, GLTexture::handleType>>;

float initialHeadY;



glm::mat4 LoadRowMajorMat4FromCsv(string const & filepath) {
	glm::mat4 m;

	std::ifstream ifs;
	ifs.open(filepath, std::ifstream::in | std::ifstream::binary);

	string m00_str, m01_str, m02_str, m03_str;
	string m10_str, m11_str, m12_str, m13_str;
	string m20_str, m21_str, m22_str, m23_str;
	string m30_str, m31_str, m32_str, m33_str;

	getline(ifs, m00_str, ',');
	getline(ifs, m01_str, ',');
	getline(ifs, m02_str, ',');
	getline(ifs, m03_str);

	getline(ifs, m10_str, ',');
	getline(ifs, m11_str, ',');
	getline(ifs, m12_str, ',');
	getline(ifs, m13_str);

	getline(ifs, m20_str, ',');
	getline(ifs, m21_str, ',');
	getline(ifs, m22_str, ',');
	getline(ifs, m23_str);

	getline(ifs, m30_str, ',');
	getline(ifs, m31_str, ',');
	getline(ifs, m32_str, ',');
	getline(ifs, m33_str);

	m[0][0] = stof(m00_str);
	m[1][0] = stof(m01_str);
	m[2][0] = stof(m02_str);
	m[3][0] = stof(m03_str);

	m[0][1] = stof(m10_str);
	m[1][1] = stof(m11_str);
	m[2][1] = stof(m12_str);
	m[3][1] = stof(m13_str);

	m[0][2] = stof(m20_str);
	m[1][2] = stof(m21_str);
	m[2][2] = stof(m22_str);
	m[3][2] = stof(m23_str);

	m[0][3] = stof(m30_str);
	m[1][3] = stof(m31_str);
	m[2][3] = stof(m32_str);
	m[3][3] = stof(m33_str);

	return m;
}

glm::mat4 hlToSphereTransformMatrix;

void initShader() {

	string scanLabel = currentScan.Label;

	sphereDataCache = std::make_shared<SphereDataCache>(scanLabel, sphereDataCacheSize, maxNumFeatures);
	tripletDataCache = std::make_shared<TripletDataCache>(tripletDataCacheSize, sphereDataCache);

	
	// we need GL_NEAREST because we don't want blending of the data that we are encoding in the cubemap
	interpolatedHullCubemap = std::make_shared<Cubemap>(cubemapRenderTextureSize, cubemapImageFormat, GL_NEAREST);

	// the equirectangular texture has the same resolution as the pano-sphere because each pixel will correspond to particular vertex on the sphere
	equirectangularFramebuffer = std::make_shared<Framebuffer>(panoSphereNumSegments, panoSphereNumRings, equirectangularImageFormat, GL_TEXTURE_2D, GL_NEAREST);
	equirectangularPixels = new GLfloat[equirectangularFramebuffer->getWidth() * equirectangularFramebuffer->getHeight() * equirectangularImageFormat.NumChannels];
	cubemapFacePixels = new GLfloat[interpolatedHullCubemap->GetCubemapSize() * interpolatedHullCubemap->GetCubemapSize() * cubemapImageFormat.NumChannels];

	openglCubemapViewMats.push_back(glm::lookAt(glm::vec3(0, 0, 0), glm::vec3(1, 0, 0), glm::vec3(0, 1, 0))); // pos x = right
	openglCubemapViewMats.push_back(glm::lookAt(glm::vec3(0, 0, 0), glm::vec3(-1, 0, 0), glm::vec3(0, 1, 0))); // neg x = left
	openglCubemapViewMats.push_back(glm::lookAt(glm::vec3(0, 0, 0), glm::vec3(0, -1, 0), glm::vec3(0, 0, -1))); // pos y = up
	openglCubemapViewMats.push_back(glm::lookAt(glm::vec3(0, 0, 0), glm::vec3(0, 1, 0), glm::vec3(0, 0, 1))); // neg y = dowm
	openglCubemapViewMats.push_back(glm::lookAt(glm::vec3(0, 0, 0), glm::vec3(0, 0, -1), glm::vec3(0, 1, 0))); // pos z = back
	openglCubemapViewMats.push_back(glm::lookAt(glm::vec3(0, 0, 0), glm::vec3(0, 0, 1), glm::vec3(0, 1, 0))); // neg z = forward

	meshShader = std::make_shared<Shader>("mesh.vert", "mesh.frag");
	pathShader = std::make_shared<Shader>("pathline.vert", "pathline.frag");
	encodeBarycentricShader = std::make_shared<Shader>("encode_barycentric.vert", "encode_barycentric.frag");
	spherePanoShader = std::make_shared<Shader>("spherepano_blend.vert", "spherepano_blend.frag");
	panoProjectiveTextureMappingShader = std::make_shared<Shader>("pano_projective_texture_mapping.vert", "pano_projective_texture_mapping.frag");
	equirectangularShader = std::make_shared<Shader>("equirectangular.vert", "equirectangular.frag");

	skyboxShader = std::make_shared<Shader>("skybox.vert", "skybox.frag");

	cubeModel = std::make_shared<Model>(FileSystem::getPath("resources/meshes/cube.obj").c_str());
	quadModel = std::make_shared<Model>(FileSystem::getPath("resources/meshes/quad.obj").c_str());
	arrowModel = std::make_shared<Model>(FileSystem::getPath("resources/meshes/arrow.obj").c_str());

	panoSphereModel = Model::CreatePanoramaSphere(1.0f, 128, 64);
	sphereModel = Model::CreatePanoramaSphere(1.0f, 8, 4, true);
	panoSphereTexture = std::make_shared<GLTexture>("panoSphereTexture");
	panoSphereTexture->load(FileSystem::getPath("resources/panos/SAM_Sample_0002.jpg"));

	skyboxModel = std::make_shared<Model>(FileSystem::getPath("resources/meshes/cube.obj").c_str());

	locatableCameraFrustumModel = Model::CreateFrustum(locatableCameraProfile.HfovDegrees, sphericalCamera.AspectRatio, 0.25f);
	sphericalCameraFrustumModel = Model::CreateFrustum(sphericalCamera.HfovDegrees, sphericalCamera.AspectRatio, 0.25f);

	

	

	hlToSphereTransformMatrix = LoadRowMajorMat4FromCsv(FileSystem::getPath(string_format("resources/scans/%s/hl_sphere_transform_matrix.csv", scanLabel.c_str())).c_str());
	cout << "hlToSphereTransformMatrix: " << glm::to_string(hlToSphereTransformMatrix) << endl;

	std::string roomFilepath = string_format("resources/scans/%s/%s_mesh.room", scanLabel.c_str(), scanLabel.c_str());
	cout << "roomFilepath: " << roomFilepath << endl;

	testHoloRoom = Model::LoadHoloRoom(FileSystem::getPath(roomFilepath).c_str());
	//testHoloRoom->GenerateAABBTree();

	std::string pathFilepath = string_format("resources/scans/%s/%s_pose.csv", scanLabel.c_str(), scanLabel.c_str());
	cout << "pathFilepath: " << pathFilepath << endl;

	locatableCameraPath = Path::LoadHoloPath(locatableCamera, FileSystem::getPath(pathFilepath).c_str(), gridSizeMeters);

	cubemapProjectionMat = glm::perspective(glm::radians(90.0f), 1.0f, 0.001f, 100.0f);	// 90 degree FOV, square image, all imagery is at most 1 unit away from the origin

	initialHeadY = 0.0f;
	for (auto &p : sphericalCameraPoses) {
		initialHeadY += p.Position.y;
	}
	initialHeadY /= sphericalCameraPoses.size();

	fullscreenQuadModel = Model::CreateFullscreenQuad();

	GL_CHECK(std::cout << "done initing" << std::endl);
}

void drawAxes(glm::mat4x4 &model, float size, glm::vec3 centerObjColor) {
	meshShader->use();

	float longSize = 1.0f * size;
	float narrowSize = 0.1f * size;

	glm::mat4 xModelMatrix(model);
	xModelMatrix = glm::translate(xModelMatrix, glm::vec3(longSize / 2, 0, 0));
	xModelMatrix = glm::scale(xModelMatrix, glm::vec3(longSize, narrowSize, narrowSize));

	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(xModelMatrix));
	glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 0.0f, 0.0f);
	cubeModel->Draw(meshShader);

	glm::mat4 yModelMatrix(model);
	yModelMatrix = glm::translate(yModelMatrix, glm::vec3(0, longSize / 2, 0));
	yModelMatrix = glm::scale(yModelMatrix, glm::vec3(narrowSize, longSize, narrowSize));

	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(yModelMatrix));
	glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.0f, 1.0f, 0.0f);
	cubeModel->Draw(meshShader);

	glm::mat4 zModelMatrix(model);
	zModelMatrix = glm::translate(zModelMatrix, glm::vec3(0, 0, longSize / 2));
	zModelMatrix = glm::scale(zModelMatrix, glm::vec3(narrowSize, narrowSize, longSize));

	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(zModelMatrix));
	glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.0f, 0.0f, 1.0f);
	cubeModel->Draw(meshShader);

	glm::mat4 centerModelMatrix(model);
	centerModelMatrix = glm::scale(centerModelMatrix, glm::vec3(narrowSize * 2, narrowSize * 2, narrowSize * 2));
	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(centerModelMatrix));
	glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), centerObjColor.x, centerObjColor.y, centerObjColor.z);
	cubeModel->Draw(meshShader);
}




void drawRoom(glm::mat4x4 &view, glm::mat4x4 &proj) {

	meshShader->use();

	glUniform3f(glGetUniformLocation(meshShader->Program, "lightPos"), thirdPersonCamera.Position.x, thirdPersonCamera.Position.y, thirdPersonCamera.Position.z);

	glUniform3f(glGetUniformLocation(meshShader->Program, "lightColor"), 1.0f, 1.0f, 1.0f);

	glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.75f, 0.75f, 0.75f);

	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(proj));
	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(view));

	//==================

	// draw the room
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);

	glm::mat4 modelIdentity;
	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(modelIdentity));
	testHoloRoom->Draw(meshShader);

	glDisable(GL_CULL_FACE);
	glCullFace(GL_BACK);
}

void renderHullModel(std::shared_ptr<Model> hullModel, glm::mat4 modelMat, glm::mat4 viewMat, glm::mat4 projectionMat) {

	encodeBarycentricShader->use();

	glUniformMatrix4fv(glGetUniformLocation(encodeBarycentricShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(modelMat));
	glUniformMatrix4fv(glGetUniformLocation(encodeBarycentricShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(projectionMat));
	glUniformMatrix4fv(glGetUniformLocation(encodeBarycentricShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(viewMat));

	hullModel->Draw(encodeBarycentricShader);
}


void renderInterpolatedHullModel(glm::mat4 modelMat, glm::mat4 viewMat, glm::mat4 projectionMat) {
	std::shared_ptr<Model> worldTriangulationModel = currentTripletData->GetWorldTriangulationModel();

	renderHullModel(worldTriangulationModel, modelMat, viewMat, projectionMat);
}




void renderInterpolatedHullSphericalTexture(bool preview) {
	// first, render the interpolated hull model from the inside, as a cubemap


	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ZERO);
	glBlendEquation(GL_FUNC_ADD);

	glClearDepth(0.0f);
	glDepthFunc(GL_GREATER);	// Here we are specifically prioritizing fragments that are FURTHER from the camera.
								// This is so that if we have bad topology that occludes a bunch, it isn't as bad.

	interpolatedHullCubemap->preRender();

	glm::mat4 &cubeProjMat = cubemapProjectionMat;
	glm::mat4 cubeModelMat; // identity

	for (int faceIdx = 0; faceIdx < 6; faceIdx++) {
		interpolatedHullCubemap->PreFace(faceIdx);

		glm::mat4 cubeViewMat = openglCubemapViewMats[faceIdx];

		// in this approach, we are actually going to render the encoded mesh from a world position that matches the translation (but not rotation!) of the panorama visualization camera

		glm::mat4 translatedCubeViewMat = glm::translate(cubeViewMat, visualizationCamera.Position);


		renderInterpolatedHullModel(cubeModelMat, translatedCubeViewMat, cubeProjMat);

		interpolatedHullCubemap->PostFace(faceIdx);
	}

	interpolatedHullCubemap->postRender();

	glClearDepth(1.0f);
	glDepthFunc(GL_LESS);		// Restore the default depth function

								// =================

								// now that the cubemap is rendered, convert it to an equirectangular projection
	if (!preview) {
		equirectangularFramebuffer->preRender();
	}

	equirectangularShader->use();

	int textureUnit = 0;
	glActiveTexture(GL_TEXTURE0 + textureUnit);
	glBindTexture(GL_TEXTURE_CUBE_MAP, interpolatedHullCubemap->textureCubemap());

	glUniform1i(glGetUniformLocation(spherePanoShader->Program, "cubemap"), textureUnit);

	fullscreenQuadModel->Draw(equirectangularShader);

	if (!preview) {
		equirectangularFramebuffer->postRender();
	}
}



int getLocatableCameraFrameIndexFromPose(Pose p) {
	return (int)round(p.PoseIndex);
}

Pose makeInterpolatedPose(Pose poseA, Pose poseB, float desiredTimestamp) {
	float t = (desiredTimestamp - poseA.Timestamp) / (poseB.Timestamp - poseA.Timestamp);
	//std::cout << "t: " << t << std::endl;

	glm::vec3 interpolatedPos = glm::mix(poseA.Position, poseB.Position, t);
	glm::quat interpolatedRotQuat = glm::slerp(poseA.RotationQuaternion, poseB.RotationQuaternion, t);
	glm::mat4 interpolatedRotMat(interpolatedRotQuat);

	float interpolatedPoseIndex = glm::mix(poseA.PoseIndex, poseB.PoseIndex, t);

	Pose interpolatedPose(locatableCamera, desiredTimestamp, interpolatedPos, interpolatedRotQuat, interpolatedRotMat, interpolatedPoseIndex);
	return interpolatedPose;
}

// given a particular index in the pose list, and a desired timestamp offset...
// select an adjacent pose and do interpolation and return the generated pose
Pose getPoseFromIndex(size_t index, float timestampOffset) {
	//std::cout << "getPoseFromIndex " << index << " with offset " << timestampOffset << std::endl;
	if (timestampOffset == 0.0f) {
		return sphericalCameraPoses[index];	// no interpolation
	}

	if (index == 0 && timestampOffset < 0) {
		// trying to go before the beginning
		return sphericalCameraPoses[index];
	}

	if (index == sphericalCameraPoses.size() - 1 && timestampOffset > 0) {
		// trying to go after the end
		return sphericalCameraPoses[index];
	}

	Pose currentPose = sphericalCameraPoses[index];

	float desiredTimestamp = currentPose.Timestamp + timestampOffset;

	if (timestampOffset < 0) {
		//std::cout << "before" << std::endl;
		size_t prevPoseIndex = index - 1;

		bool prevPoseFound = false;
		while (!prevPoseFound) {
			if (prevPoseIndex < 0) {
				return sphericalCameraPoses[index];
			}

			if (sphericalCameraPoses[prevPoseIndex].Timestamp < desiredTimestamp) {
				prevPoseFound = true;
			}
			else {
				prevPoseIndex--;
			}
		}

		Pose prevPose = sphericalCameraPoses[prevPoseIndex];

		return makeInterpolatedPose(prevPose, currentPose, desiredTimestamp);
	}
	else {
		//std::cout << "after" << std::endl;
		// timestamp > 0

		size_t nextPoseIndex = index + 1;

		bool nextPoseFound = false;
		while (!nextPoseFound) {
			if (nextPoseIndex > sphericalCameraPoses.size() - 1) {
				return sphericalCameraPoses[index];
			}

			if (sphericalCameraPoses[nextPoseIndex].Timestamp > desiredTimestamp) {
				nextPoseFound = true;
			}
			else {
				nextPoseIndex++;
			}
		}

		Pose nextPose = sphericalCameraPoses[nextPoseIndex];

		return makeInterpolatedPose(currentPose, nextPose, desiredTimestamp);
	}
}

bool insideCapturedArea = false;
std::vector<size_t> poseIndicesForTriangleVerts;
std::vector<std::shared_ptr<GLTexture>> texturesForTriangleVerts;
Locate_type lt;
int li;
CGAL::internal::CC_iterator<CGAL::Compact_container<CGAL::Triangulation_ds_face_base_2<Tds>>, false> face_handle;


void DrawRoom(glm::mat4 & cameraProjection, glm::mat4 & cameraView) {
	meshShader->use();

	//==================

	// draw the room
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);

	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(cameraProjection));
	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(cameraView));

	glm::mat4 modelIdentity;
	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(modelIdentity));
	testHoloRoom->Draw(meshShader);

	glDisable(GL_CULL_FACE);
	glCullFace(GL_BACK);
}




void DrawThirdPersonVisualization() {
	float cameraPathProgress = cameraPathSlider->value();
	int cameraPoseIndex = glm::clamp((int)(sphericalCameraPoses.size() * cameraPathProgress), 0, (int)(sphericalCameraPath->poses.size() - 1));
	auto cameraPose = sphericalCameraPoses[cameraPoseIndex];

	//==================
	// set up the third person camera
	glm::mat4 modelIdentity;
	glm::mat4 thirdPersonCameraProjection = glm::perspective(thirdPersonCamera.Zoom, (float)screenWidth / (float)screenHeight, 0.1f, 100.0f);
	glm::mat4 thirdPersonCameraView = thirdPersonCamera.GetViewMatrix();

	//==================
	// initialize common uniforms
	meshShader->use();

	glUniform3f(glGetUniformLocation(meshShader->Program, "lightPos"), thirdPersonCamera.Position.x, thirdPersonCamera.Position.y, thirdPersonCamera.Position.z);

	glUniform3f(glGetUniformLocation(meshShader->Program, "lightColor"), 1.0f, 1.0f, 1.0f);
	glUniform1i(glGetUniformLocation(meshShader->Program, "lightingEnabled"), true);
	glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.75f, 0.75f, 0.75f);

	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(thirdPersonCameraProjection));
	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(thirdPersonCameraView));

	//==================

	DrawRoom(thirdPersonCameraProjection, thirdPersonCameraView);

	//==================
	 
	// draw the frustum in the place that matches the current pose

	glm::mat4 frustumModelMatrix = sphericalCameraPoses[cameraPoseIndex].ModelMatrix;

	glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 1.0f, 0.0f);
	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(frustumModelMatrix));
	sphericalCameraFrustumModel->Draw(meshShader);

	//==================

	// draw where the visualization camera is

	glm::mat4 visualizationCameraModelMatrix = glm::inverse(visualizationCamera.GetViewMatrix());

	glm::mat4 visualizationCameraScaledMatrix = glm::scale(visualizationCameraModelMatrix, glm::vec3(0.5, 0.5, 0.5));

	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(visualizationCameraScaledMatrix));
	glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 1.0f, 0.5f);
	arrowModel->Draw(meshShader);

	//==================

	// draw the path (HoloLens)

	pathShader->use();

	glUniformMatrix4fv(glGetUniformLocation(pathShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(thirdPersonCameraProjection));
	glUniformMatrix4fv(glGetUniformLocation(pathShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(thirdPersonCameraView));
	glUniformMatrix4fv(glGetUniformLocation(pathShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(modelIdentity));

	glUniform3f(glGetUniformLocation(pathShader->Program, "lineColor"), 1.0f, 0.0f, 0.0f);

	locatableCameraPath->Draw(pathShader, 0, cameraPoseIndex);

	//==================

	// draw the path (360 camera)

	glUniformMatrix4fv(glGetUniformLocation(pathShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(thirdPersonCameraProjection));
	glUniformMatrix4fv(glGetUniformLocation(pathShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(thirdPersonCameraView));
	glUniformMatrix4fv(glGetUniformLocation(pathShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(modelIdentity));

	glUniform3f(glGetUniformLocation(pathShader->Program, "lineColor"), 0.0f, 1.0f, 0.0f);

	sphericalCameraPath->Draw(pathShader, 0, cameraPoseIndex);

	//===================

	meshShader->use();

	// draw the closest cell center points
	for (auto &closestPoseEntry : sphericalCameraPath->closestPoseIndices) {
		auto &cell_id = closestPoseEntry.first;
		auto &index = closestPoseEntry.second;

		if (showSelectedSpherePoses) {
			// draw the cubes representing each of the selected sphere poses

			auto &p = sphericalCameraPoses[index];
			glm::mat4 frontPoseModelMatrix = p.ModelMatrix * testFrontToSphereCameraRotation;
			frontPoseModelMatrix = glm::scale(frontPoseModelMatrix, glm::vec3(0.1f, 0.1f, 0.1f));
			glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(frontPoseModelMatrix));
			glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.5f, 0.5f, 0.5f);
			cubeModel->Draw(meshShader);
		}
		
		if (showFloorTiles) {
			// draw the tiles on the floor

			glm::vec3 cell_center_ground_position(cell_id.x * gridSizeMeters, -1.5f, cell_id.y * gridSizeMeters);

			glm::mat4 cellCenterModelMatrix;
			cellCenterModelMatrix = glm::translate(cellCenterModelMatrix, cell_center_ground_position);
			cellCenterModelMatrix = glm::scale(cellCenterModelMatrix, glm::vec3(gridSizeMeters*.9f, 0.1f, gridSizeMeters*.9f));
			glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(cellCenterModelMatrix));
			glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.25f, 0.25f, 0.25f);
			cubeModel->Draw(meshShader);
		}
		

	}

	//=============

	// draw the three nearest surrounding points

	switch (lt)
	{
	case Locate_type::EDGE:
		std::cout << "edge" << std::endl;
		break;
	case Locate_type::FACE:

		for (int i = 0; i < 3; i++) {
			auto v = face_handle->vertex(i);
			auto index = v->info();

			auto &p = sphericalCameraPoses[index];

			glm::mat4 surroundingSphereModelMatrix = p.ModelMatrix * testFrontToSphereCameraRotation;

			glm::mat4 scaledSurroundingSphereModelMatrix = glm::scale(surroundingSphereModelMatrix, glm::vec3(0.25f, 0.25f, 0.25f));

			glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(scaledSurroundingSphereModelMatrix));

			if (i == 0) {
				glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 0.0f, 0.0f);
			}
			else if (i == 1) {
				glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.0f, 1.0f, 0.0f);
			}
			else if (i == 2) {
				glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.0f, 0.0f, 1.0f);
			}

			cubeModel->Draw(meshShader);

			

			// ========================================================
			// draw rays representing the keypoints

			if (rayDisplayComboBox->selectedIndex() == 2) {	// "All"
				auto kp_for_each_side = sphereDataCache->GetCubemapKeypointsForSphericalCameraFrame(currentSurroundingSphereCameraFrames[i]);
				for (int sideIdx = 0; sideIdx < 6; sideIdx++) {
					auto kp_for_this_side = kp_for_each_side[sideIdx];

					for (auto & kp : kp_for_this_side) {
						glm::vec3 kpPosition = getKeypointOnSphereFromCubemapKeypoint(kp, cubeSidePixels, sideIdx);

						glm::mat4 rayLocalModelMatrix = glm::inverse(glm::lookAt(glm::vec3(0, 0, 0), kpPosition, glm::vec3(0, 1, 0)));

						glm::mat4 rayWorldModelMatrix = surroundingSphereModelMatrix * rayLocalModelMatrix;
						rayWorldModelMatrix = glm::scale(rayWorldModelMatrix, glm::vec3(0.01, 0.01, 30.0));

						glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(rayWorldModelMatrix));
						arrowModel->Draw(meshShader);
					}
				}
			}
			

			// ========================================================

			// draw each set of pairwise world points for the current triplet
			
			auto pairwiseWorldPoints = currentTripletData->GetPairwiseWorldPoints();
			for (int pairwiseIdx = 0; pairwiseIdx < 3; pairwiseIdx++) {
				auto pairwiseWorldPointSet = pairwiseWorldPoints[pairwiseIdx];

				if (printingInfo) {
					std::cout << "for pairwiseIdx = " << pairwiseIdx << ", num world points = " << pairwiseWorldPointSet.size() << std::endl;
				}

				switch (pairwiseIdx)
				{
				case 0:
					glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 1.0f, 0.0f);
					break;
				case 1:
					glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.0f, 1.0f, 1.0f);
					break;
				case 2:
					glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 0.0f, 1.0f);
					break;
				default:
					break;
				}

				for (glm::vec3 & worldPoint : pairwiseWorldPointSet) {
					glm::mat4 worldPointMat;
					worldPointMat = glm::translate(worldPointMat, worldPoint);
					worldPointMat = glm::scale(worldPointMat, glm::vec3(0.05f, 0.05f, 0.05f));
					glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(worldPointMat));

					sphereModel->Draw(meshShader);
				}
			}

			// ========================================================

			// draw the average world point (mean of the pairwise points)

			auto averageWorldPoints = currentTripletData->GetAverageWorldPoints();

			if (printingInfo) {
				std::cout << "for average world points, num world points = " << averageWorldPoints.size() << std::endl;
			}

			glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 1.0f, 1.0f);

			for (glm::vec3 & worldPoint : averageWorldPoints) {
				glm::mat4 worldPointMat;
				worldPointMat = glm::translate(worldPointMat, worldPoint);
				worldPointMat = glm::scale(worldPointMat, glm::vec3(0.05f, 0.05f, 0.05f));
				glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(worldPointMat));

				sphereModel->Draw(meshShader);
			}

			if (printingInfo) {
				// compute stats about distance between pairwise points and the average

				std::vector<float> distsFromAverage;

				size_t numAveragePoints = averageWorldPoints.size();

				float sumDists = 0.0f;

				float minDist = std::numeric_limits<float>::max();
				float maxDist = std::numeric_limits<float>::min();

				for (size_t idx = 0; idx < numAveragePoints; idx++) {
					for (size_t ij_idx = 0; ij_idx < 3; ij_idx++) {
						float ij_dist = glm::distance(pairwiseWorldPoints[ij_idx][idx], averageWorldPoints[idx]);

						distsFromAverage.push_back(ij_dist);

						sumDists += ij_dist;

						if (ij_dist < minDist) {
							minDist = ij_dist;
						}

						if (ij_dist > maxDist) {
							maxDist = ij_dist;
						}
					}
				}

				float meanDist = sumDists / distsFromAverage.size();

				float var = 0.0f;
				for (int i = 0; i < distsFromAverage.size(); i++) {
					var += (distsFromAverage[i] - meanDist) * (distsFromAverage[i] - meanDist);
				}
				var /= distsFromAverage.size();
				float sd = std::sqrt(var);

				std::cout << "=== dists between pairwise points and corresponding average point ===" << std::endl;
				std::cout << "mean: " << meanDist << std::endl;
				std::cout << "min: " << minDist << std::endl;
				std::cout << "max: " << maxDist << std::endl;
				std::cout << "std dev: " << sd << std::endl;
			}

			// ========================================================

			// draw the triangulation models

			if (showingHullModel) {

				auto hullModels = currentTripletData->GetHullModels();

				for (int panoIdx = 0; panoIdx < 3; panoIdx++) {
					auto & hullModel = hullModels[panoIdx];

					int spherePoseIndex = sphere_pose_indices[panoIdx];

					Pose & p = sphericalCameraPoses[spherePoseIndex];

					glm::mat4 hullModelMat;
					hullModelMat = glm::translate(hullModelMat, p.Position);
					hullModelMat = glm::translate(hullModelMat, glm::vec3(0, 2, 0));
					hullModelMat = glm::scale(hullModelMat, glm::vec3(0.25f, 0.25f, 0.25f));

					renderHullModel(hullModel, hullModelMat, thirdPersonCameraView, thirdPersonCameraProjection);
				}

				// draw the interpolated hull mesh

				auto interpolatedHullModel = currentTripletData->GetInterpolatedHullModel();
				glm::mat4 interpolatedHullModelMat;
				interpolatedHullModelMat = glm::translate(interpolatedHullModelMat, visualizationCamera.Position);
				interpolatedHullModelMat = glm::translate(interpolatedHullModelMat, glm::vec3(0, 2, 0));
				interpolatedHullModelMat = glm::scale(interpolatedHullModelMat, glm::vec3(0.25f, 0.25f, 0.25f));

				renderHullModel(interpolatedHullModel, interpolatedHullModelMat, thirdPersonCameraView, thirdPersonCameraProjection);


			}
			

			// ========================================================

			meshShader->use();
			drawAxes(surroundingSphereModelMatrix, 0.5f, glm::vec3(1.0f, 1.0f, 1.0f));


		}

		break;
	case Locate_type::OUTSIDE_AFFINE_HULL:
		// outside affine hull
		break;
	case Locate_type::OUTSIDE_CONVEX_HULL:
		// outside convex hull
		break;
	case Locate_type::VERTEX:
		std::cout << "vertex" << std::endl;
		break;
	default:
		break;
	}
}








void DrawPanorama(std::vector<Kernel::FT> barycentric_coords) {

	glm::vec3 visualizationCameraPosition = visualizationCamera.Position;

	glm::mat4 visualizationCameraProjection = glm::perspective(glm::radians(panoramaFOV), (float)screenWidth / (float)screenHeight, 0.1f, 100.0f);


	glm::mat4 visualizationCameraView = visualizationCamera.GetViewMatrix(visualizationCameraPosition);

	

	if (renderingSimulatedView) {
		DrawRoom(visualizationCameraProjection, visualizationCameraView);	// just draw the holo-room from the same virtual camera location
	}
	else {
		//=================================


		std::vector<glm::mat4> rotationMatForSurroundingPoses;
		std::vector<glm::mat4> modelMatForSurroundingPoses;

		if (insideCapturedArea) {
			for (int i = 0; i < 3; i++) {

				size_t poseIndexForThisVert = poseIndicesForTriangleVerts[i];
				auto p = sphericalCameraPoses[poseIndexForThisVert];

				//auto rotationMatForThisPose = p.RotationMatrix;
				auto rotationMatForThisPose = p.RotationMatrix * testFrontToSphereCameraRotation;

				rotationMatForSurroundingPoses.push_back(rotationMatForThisPose);


				auto modelMatForThisPose = p.ModelMatrix * testFrontToSphereCameraRotation;
				modelMatForSurroundingPoses.push_back(modelMatForThisPose);

			}
		}




		if (projectiveTextureMapping) {
			auto currentlyActiveShader = panoProjectiveTextureMappingShader;

			currentlyActiveShader->use();

			glm::mat4 visualizationSphereModel;	// should be identity matrix for projective texture mapping

			glUniformMatrix4fv(glGetUniformLocation(currentlyActiveShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(visualizationCameraProjection));
			glUniformMatrix4fv(glGetUniformLocation(currentlyActiveShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(visualizationCameraView));
			glUniformMatrix4fv(glGetUniformLocation(currentlyActiveShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(visualizationSphereModel));

			glUniform1i(glGetUniformLocation(currentlyActiveShader->Program, "insideCapturedArea"), insideCapturedArea ? 1 : 0);

			

			if (insideCapturedArea) {
				for (int i = 0; i < 3; i++) {
					std::string modelMatLoc = "surroundingSphereModelMatrices[" + std::to_string(i) + "]";

					glUniformMatrix4fv(glGetUniformLocation(currentlyActiveShader->Program, modelMatLoc.c_str()), 1, GL_FALSE, glm::value_ptr(modelMatForSurroundingPoses[i]));


					int textureUnit = i;
					glActiveTexture(GL_TEXTURE0 + textureUnit);
					glBindTexture(GL_TEXTURE_2D, texturesForTriangleVerts[i]->texture());

					std::string texUniformLoc = "surroundingSphereTex[" + std::to_string(i) + "]";

					glUniform1i(glGetUniformLocation(currentlyActiveShader->Program, texUniformLoc.c_str()), textureUnit);


					std::string barycentricCoordLoc = "barycentricCoord[" + std::to_string(i) + "]";

					if (printingInfo) {
						std::cout << "barycentric_coords " << barycentric_coords[0] << " " << barycentric_coords[1] << " " << barycentric_coords[2] << std::endl;
					}

					glUniform1f(glGetUniformLocation(currentlyActiveShader->Program, barycentricCoordLoc.c_str()), barycentric_coords[i]);

					glUniform1i(glGetUniformLocation(currentlyActiveShader->Program, "nearestNeighbor"), nearestNeighbor ? 1 : 0);
				}
			}

			testHoloRoom->Draw(currentlyActiveShader);
		}
		else {
			auto currentlyActiveShader = spherePanoShader;

			currentlyActiveShader->use();


			glm::mat4 visualizationSphereModel;
			visualizationSphereModel = glm::translate(visualizationSphereModel, visualizationCameraPosition);


			glUniformMatrix4fv(glGetUniformLocation(currentlyActiveShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(visualizationCameraProjection));
			glUniformMatrix4fv(glGetUniformLocation(currentlyActiveShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(visualizationCameraView));
			glUniformMatrix4fv(glGetUniformLocation(currentlyActiveShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(visualizationSphereModel));

			glUniform1i(glGetUniformLocation(currentlyActiveShader->Program, "insideCapturedArea"), insideCapturedArea ? 1 : 0);

			if (insideCapturedArea) {
				for (int i = 0; i < 3; i++) {
					std::string rotationMatLoc = "surroundingSphereRotation[" + std::to_string(i) + "]";

					glUniformMatrix4fv(glGetUniformLocation(currentlyActiveShader->Program, rotationMatLoc.c_str()), 1, GL_FALSE, glm::value_ptr(rotationMatForSurroundingPoses[i]));


					int textureUnit = i;
					glActiveTexture(GL_TEXTURE0 + textureUnit);
					glBindTexture(GL_TEXTURE_2D, texturesForTriangleVerts[i]->texture());

					std::string texUniformLoc = "surroundingSphereTex[" + std::to_string(i) + "]";

					glUniform1i(glGetUniformLocation(currentlyActiveShader->Program, texUniformLoc.c_str()), textureUnit);


					std::string barycentricCoordLoc = "barycentricCoord[" + std::to_string(i) + "]";

					if (printingInfo) {
						std::cout << "barycentric_coords " << barycentric_coords[0] << " " << barycentric_coords[1] << " " << barycentric_coords[2] << std::endl;
					}

					glUniform1f(glGetUniformLocation(currentlyActiveShader->Program, barycentricCoordLoc.c_str()), barycentric_coords[i]);

					glUniform1i(glGetUniformLocation(currentlyActiveShader->Program, "nearestNeighbor"), nearestNeighbor ? 1 : 0);
				}
			}

		 
			panoSphereModel->Draw(currentlyActiveShader);
		}

		

		
	}

}

/*
void bufferPanoSphereModelWithBarycentricData() {

	int texWidth = equirectangularFramebuffer->getWidth();
	int texHeight = equirectangularFramebuffer->getHeight();

	// for each of the verts in the pano-sphere, look up the corresponding pixel in the equirectangular pixels
	auto &panoSphereVertices = panoSphereModel->meshes[0].vertices;
	for (auto &panoSphereVertex : panoSphereVertices) {
		glm::vec2 panoSphereUV = spherePointToUV(panoSphereVertex.Position);
		panoSphereUV.x = glm::clamp(panoSphereUV.x, 0.0f, 1.0f);
		panoSphereUV.y = glm::clamp(panoSphereUV.y, 0.0f, 1.0f);

		if (morphingPano) {
			panoSphereUV.x = 1.0f - panoSphereUV.x;

			glm::ivec2 panoSpherePixel(glm::floor((texWidth - 1)*panoSphereUV.x), glm::floor((texHeight - 1)*panoSphereUV.y));
			panoSpherePixel.x = glm::clamp(panoSpherePixel.x, 0, texWidth - 1);
			panoSpherePixel.y = glm::clamp(panoSpherePixel.y, 0, texHeight - 1);

			size_t pixelIdx = ((texWidth * panoSpherePixel.y) + panoSpherePixel.x) * equirectangularImageFormat.NumChannels;

			auto r = equirectangularPixels[pixelIdx + 0];
			auto g = equirectangularPixels[pixelIdx + 1];
			auto b = equirectangularPixels[pixelIdx + 2];
			auto a = equirectangularPixels[pixelIdx + 3];

			unsigned int triangleId;
			float bary_alpha, bary_beta, bary_gamma;

			if (equirectangularImageFormat.Type == GL_FLOAT) {
				triangleId = (unsigned int)(255 * (a * 256 + b));

				bary_alpha = r;
				bary_beta = g;
				bary_gamma = 1.0f - bary_alpha - bary_beta;
			}
			else if (equirectangularImageFormat.Type == GL_UNSIGNED_BYTE) {
				triangleId = a * 256 + b;
				//if (a > 0) {
				//std::cout << "a: " << a << std::endl;
				//}

				bary_alpha = r / 255.0f;
				bary_beta = g / 255.0f;
				bary_gamma = 1.0f - bary_alpha - bary_beta;
			}


			for (int panoIdx = 0; panoIdx < 3; panoIdx++) {

				auto &hullModel = hullModels[panoIdx];
				auto &hullModelIndices = hullModel->meshes[0].indices;

				auto vertIdx0 = hullModelIndices[3 * triangleId + 0];
				auto vertIdx1 = hullModelIndices[3 * triangleId + 1];
				auto vertIdx2 = hullModelIndices[3 * triangleId + 2];

				auto &hullModelVertices = hullModel->meshes[0].vertices;

				auto &refVert0 = hullModelVertices[vertIdx0];
				auto &refVert1 = hullModelVertices[vertIdx1];
				auto &refVert2 = hullModelVertices[vertIdx2];

				glm::vec3 resultPos = glm::normalize(bary_alpha*refVert0.Position + bary_beta*refVert1.Position + bary_gamma*refVert2.Position);

				glm::vec3 resultPosUnrotated = corrected_inverse_sphere_rotation_matrices[panoIdx] * glm::vec4(resultPos, 0);

				//glm::vec2 resultUV = spherePointToUV(resultPos);
				glm::vec2 resultUV = spherePointToUV(resultPosUnrotated);

				switch (panoIdx)
				{
				case 0:
					panoSphereVertex.TexCoords0 = resultUV;
					break;
				case 1:
					panoSphereVertex.TexCoords1 = resultUV;
					break;
				case 2:
					panoSphereVertex.TexCoords2 = resultUV;
					break;
				default:
					break;
				}
			}
		}
		else {
			panoSphereVertex.TexCoords0 = panoSphereUV;
			panoSphereVertex.TexCoords1 = panoSphereUV;
			panoSphereVertex.TexCoords2 = panoSphereUV;
		}

	}
	panoSphereModel->meshes[0].rebufferVertices();
}
*/

void draw() {
	
	sphereDataCache->IncrementFrameNumber();
	tripletDataCache->IncrementFrameNumber();

	if (rotationAdjustmentDirty) {
		// rotation of the camera/hololens, representing ZXY euler angles in degrees 
		// (so z degrees about z axis, then x degrees about x axis, then y degrees about y axis, in that order)
		float testZRad = glm::radians(testSphereRotationZ);
		float testXRad = glm::radians(testSphereRotationX);
		float testYRad = glm::radians(testSphereRotationY);

		testFrontToSphereCameraRotation = glm::mat4();	// generated at runtime from user-set rotations
		testFrontToSphereCameraRotation = glm::rotate(testFrontToSphereCameraRotation, testZRad, glm::vec3(0.0f, 0.0f, 1.0f));
		testFrontToSphereCameraRotation = glm::rotate(testFrontToSphereCameraRotation, testXRad, glm::vec3(1.0f, 0.0f, 0.0f));
		testFrontToSphereCameraRotation = glm::rotate(testFrontToSphereCameraRotation, testYRad, glm::vec3(0.0f, 1.0f, 0.0f));

		// we set dirty=false at the end of the function, so other side effects can be recomputed
		tripletDataCache->Invalidate();
	}
	


	// first, determine which spherical panoramas surround our current (virtual camera) location

	poseIndicesForTriangleVerts.clear();
	texturesForTriangleVerts.clear();
	insideCapturedArea = false;

	
	std::vector<Kernel::FT> barycentric_coords;
	barycentric_coords.reserve(3);	// alpha, beta, gamma

									// find the triangle closest to the current visualization location and mark the vertices
	Point2D query_point(visualizationCamera.Position.x, visualizationCamera.Position.z);

	face_handle = sphericalCameraPath->cellCenterTriangulation.locate(query_point, lt, li);

	bool tripletHasChanged = false;

	switch (lt)
	{
	case Locate_type::EDGE:
		std::cout << "edge" << std::endl;
		break;
	case Locate_type::FACE:

		insideCapturedArea = true;

		{
			// determine the barycentric coordinates of the query point
			Triangle_coordinates triangle_coordinates(face_handle->vertex(0)->point(), face_handle->vertex(1)->point(), face_handle->vertex(2)->point());

			triangle_coordinates(query_point, std::inserter(barycentric_coords, barycentric_coords.end()));	// calculates the barycentric coords and saves them in the preallocated list
			
			// determine if we have changed which triplet we're in this frame
			for (int panoIdx = 0; panoIdx < 3; panoIdx++) {
				auto &vert_i = face_handle->vertex(panoIdx);
				size_t poseIndex = (size_t)vert_i->info();	// the pose index in our path that corresponds with this triangle vert

				sphere_pose_indices[panoIdx] = poseIndex;

				int sphereCameraFrame = GetSphericalCameraFrameIndexFromPose(sphericalCameraPoses[poseIndex]);

				if (sphereCameraFrame != currentSurroundingSphereCameraFrames[panoIdx]) {
					tripletHasChanged = true;
				}

				currentSurroundingSphereCameraFrames[panoIdx] = sphereCameraFrame;
			}
		}

		if (tripletHasChanged) {
			std::cout << "triplet changed, corresponding sides map is: " << std::endl;

			auto poseIndex0 = face_handle->vertex(0)->info();
			auto poseIndex1 = face_handle->vertex(1)->info();
			auto poseIndex2 = face_handle->vertex(2)->info();

			currentTripletData = tripletDataCache->GetTripletDataForTriplet(poseIndex0, poseIndex1, poseIndex2);

			auto maps = currentTripletData->GetCorrespondingFaceMaps();
			for (int row = 0; row < 6; row++) {
				std::cout << maps[row][0] << " " << maps[row][1] << " " << maps[row][2] << std::endl;
			}

			if (rotationAdjustmentDirty) {

				auto sphereA_pose = sphericalCameraPoses[poseIndex0];
				auto sphereB_pose = sphericalCameraPoses[poseIndex1];
				auto sphereC_pose = sphericalCameraPoses[poseIndex2];

				corrected_sphere_rotation_matrices.clear();
				corrected_sphere_rotation_matrices.push_back(sphereA_pose.RotationMatrix * testFrontToSphereCameraRotation);
				corrected_sphere_rotation_matrices.push_back(sphereB_pose.RotationMatrix * testFrontToSphereCameraRotation);
				corrected_sphere_rotation_matrices.push_back(sphereC_pose.RotationMatrix * testFrontToSphereCameraRotation);

				corrected_inverse_sphere_rotation_matrices.clear();
				corrected_inverse_sphere_rotation_matrices.push_back(glm::inverse(corrected_sphere_rotation_matrices[0]));
				corrected_inverse_sphere_rotation_matrices.push_back(glm::inverse(corrected_sphere_rotation_matrices[1]));
				corrected_inverse_sphere_rotation_matrices.push_back(glm::inverse(corrected_sphere_rotation_matrices[2]));

			}
		}

		for (int i = 0; i < 3; i++) {
			auto &vert_i = face_handle->vertex(i);
			size_t poseIndex = (size_t)vert_i->info();	// the pose index in our path that corresponds with this triangle vert

			int sphereCameraFrame = currentSurroundingSphereCameraFrames[i];

			std::shared_ptr<GLTexture> cachedSphericalTexture = sphereDataCache->GetSphericalTextureForSphericalCameraFrame(sphereCameraFrame);

			cv::Mat cachedCubemapImg = sphereDataCache->GetCubemapImgForSphericalCameraFrame(sphereCameraFrame);

			if (tripletHasChanged) {

				auto cubemapKp = sphereDataCache->GetCubemapKeypointsForSphericalCameraFrame(sphereCameraFrame);

				// draw the keypoints and show them
				for (int sideIdx = 0; sideIdx < 6; sideIdx++) {

					cubeSidePixels = cachedCubemapImg.rows;
					cv::Mat cubeSideImg = cachedCubemapImg(cv::Rect(cubeSidePixels*sideIdx, 0, cubeSidePixels, cubeSidePixels));

					cv::drawKeypoints(cubeSideImg, cubemapKp[sideIdx], cubeSideImg, cv::Scalar(255, 0, 0));
				}

				// only do imshow when the images are actually different
				std::string cubemap_winname = "cached_sphere_" + std::to_string(i);
				cv::namedWindow(cubemap_winname, cv::WINDOW_NORMAL);
				cv::imshow(cubemap_winname, cachedCubemapImg);
			}

			
			


			poseIndicesForTriangleVerts.push_back(poseIndex);
			texturesForTriangleVerts.push_back(cachedSphericalTexture);
		}
		break;
	case Locate_type::VERTEX:
		std::cout << "vertex" << std::endl;
		break;
	case Locate_type::OUTSIDE_AFFINE_HULL:
	case Locate_type::OUTSIDE_CONVEX_HULL:
		break;
	default:
		break;
	}

	//==================

	if (lt == Locate_type::FACE && insideCapturedArea) {

		// generate the interpolated mesh based on the current barycentric coords and the surrounding hull models

		auto interpolatedHullModel = currentTripletData->GetInterpolatedHullModel();
		auto keypointPositionsForEachSphere = currentTripletData->GetKeypointPositionsForEachSphere();

		std::vector<Vertex> &interpolatedHullVertices = interpolatedHullModel->meshes[0].vertices;
		int numVertices = interpolatedHullVertices.size();

		for (int i = 0; i < numVertices; i++) {
			auto & meshVertex = interpolatedHullVertices[i];

			auto originalPointId = meshVertex.PointId;
			auto posA = keypointPositionsForEachSphere[0][originalPointId];
			auto posB = keypointPositionsForEachSphere[1][originalPointId];
			auto posC = keypointPositionsForEachSphere[2][originalPointId];

			float alpha = barycentric_coords[0];
			float beta = barycentric_coords[1];
			float gamma = barycentric_coords[2];

			glm::vec3 resultPos = glm::normalize(alpha*posA + beta*posB + gamma*posC);
			glm::vec2 resultUV = spherePointToUV(resultPos);

			meshVertex.Position = resultPos;
			meshVertex.Normal = resultPos;
			meshVertex.TexCoords0 = resultUV;
			meshVertex.TexCoords1 = resultUV;
			meshVertex.TexCoords2 = resultUV;
		}
		interpolatedHullModel->meshes[0].rebufferVertices();
	}





















	//==================

	// take the current visualization camera position and render the encoded hull mesh, then read the data to a CPU buffer

	renderInterpolatedHullSphericalTexture(false);	// this is the render that actually writes the intended encoded data to the framebuffer

													// after rendering the interpolated hull spherical texture to the framebuffer, do a glreadpixels to get the data
	glBindFramebuffer(GL_FRAMEBUFFER, equirectangularFramebuffer->framebufferId());

	int texWidth = equirectangularFramebuffer->getWidth();
	int texHeight = equirectangularFramebuffer->getHeight();

	glReadPixels(0, 0, texWidth, texHeight, equirectangularImageFormat.Format, equirectangularImageFormat.Type, equirectangularPixels);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	//==================

	// given the data that has been saved into equirectangularPixels, update the pano-sphere model so its UV coords correspond with the right positions on the source textures
	//bufferPanoSphereModelWithBarycentricData();














	switch (visualizationComboBox->selectedIndex()) {
	case 0:	// third person
		DrawThirdPersonVisualization();
		break;
	case 1:	// encoded mesh

		renderInterpolatedHullSphericalTexture(true); // this is just an additional rendering to the screen to show what the spherical texture looks like.

		break;
	case 2:	// pano
		DrawPanorama(barycentric_coords);
		break;
	}

	

	//==================

	//==================

	//==================

	

	rotationAdjustmentDirty = false;
}






// Moves/alters the camera positions based on user input
void doMovement()
{
	Camera* camera = controllingVisualizationCamera ? &visualizationCamera : &thirdPersonCamera;

	bool forceHorizontalMotion = controllingVisualizationCamera;
	//bool forceHorizontalMotion = false;

	// Camera controls
	if (keys[GLFW_KEY_W])
		camera->ProcessKeyboard(FORWARD, deltaTime, forceHorizontalMotion);
	if (keys[GLFW_KEY_S])
		camera->ProcessKeyboard(BACKWARD, deltaTime, forceHorizontalMotion);
	if (keys[GLFW_KEY_A])
		camera->ProcessKeyboard(LEFT, deltaTime, forceHorizontalMotion);
	if (keys[GLFW_KEY_D])
		camera->ProcessKeyboard(RIGHT, deltaTime, forceHorizontalMotion);
	if (keys[GLFW_KEY_Q])
		camera->ProcessKeyboard(DOWN, deltaTime, forceHorizontalMotion);
	if (keys[GLFW_KEY_E])
		camera->ProcessKeyboard(UP, deltaTime, forceHorizontalMotion);

	if (controllingVisualizationCamera) {
		// make sure the visualization camera is always set at the default height
		camera->Position.y = initialHeadY;
	}
}










int main(int /* argc */, char ** /* argv */) {

	initScanData();

	glfwInit();

	glfwSetTime(0);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glfwWindowHint(GLFW_SAMPLES, 0);
	glfwWindowHint(GLFW_RED_BITS, 8);
	glfwWindowHint(GLFW_GREEN_BITS, 8);
	glfwWindowHint(GLFW_BLUE_BITS, 8);
	glfwWindowHint(GLFW_ALPHA_BITS, 8);
	glfwWindowHint(GLFW_STENCIL_BITS, 8);
	glfwWindowHint(GLFW_DEPTH_BITS, 24);
	glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

	// Create a GLFWwindow object
	window = glfwCreateWindow(screenWidth, screenHeight, "example3", nullptr, nullptr);
	if (window == nullptr) {
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

#if defined(NANOGUI_GLAD)
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
		throw std::runtime_error("Could not initialize GLAD!");
	glGetError(); // pull and ignore unhandled errors like GL_INVALID_ENUM
#endif

	glClearColor(0.2f, 0.25f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Create a nanogui screen and pass the glfw pointer to initialize
	screen = new Screen();
	screen->initialize(window, true);

	glfwGetFramebufferSize(window, &screenWidth, &screenHeight);
	glViewport(0, 0, screenWidth, screenHeight);
	glfwSwapInterval(0);
	glfwSwapBuffers(window);


	// Create nanogui gui
	bool enabled = true;
	FormHelper *gui = new FormHelper(screen);
	nanogui::ref<Window> nanoguiWindow = gui->addWindow(Eigen::Vector2i(10, 10), "Form helper example");

	/*
	gui->addGroup("Basic types");
	gui->addVariable("bool", bvar)->setTooltip("Test tooltip.");
	gui->addVariable("string", strval);

	gui->addVariable("int", ivar)->setSpinnable(true);
	gui->addVariable("float", fvar)->setTooltip("Test.");
	gui->addVariable("double", dvar)->setSpinnable(true);

	gui->addGroup("Complex types");
	gui->addVariable("Enumeration", enumval, enabled)->setItems({ "Item 1", "Item 2", "Item 3" });
	gui->addVariable("Color", colval);

	gui->addGroup("Other widgets");
	gui->addButton("A button", []() { std::cout << "Button pressed." << std::endl; })->setTooltip("Testing a much longer tooltip, that will wrap around to new lines multiple times.");;
	*/

	gui->addGroup("Synthetic viewpoint");

	gui->addVariable("Pano projective texture mapping", projectiveTextureMapping)->setTooltip("Pano projective texture mapping");
	

	gui->addVariable("morphingPano", morphingPano);

	
	gui->addVariable("panoramaFOV", panoramaFOV)->setSpinnable(true);
	gui->addVariable("renderingSimulatedView", renderingSimulatedView);

	gui->addVariable("printingInfo", printingInfo)->setTooltip("printingInfo");

	gui->addVariable("nearestNeighbor", nearestNeighbor)->setTooltip("render panorama as nearest neighbor");

	auto testSphereRotationXWidget = gui->addVariable("testSphereRotationX", testSphereRotationX);
	testSphereRotationXWidget->setSpinnable(true);
	testSphereRotationXWidget->setCallback([](float value) {
		testSphereRotationX = value;
		rotationAdjustmentDirty = true;
	});

	auto testSphereRotationYWidget = gui->addVariable("testSphereRotationY", testSphereRotationY);
	testSphereRotationYWidget->setSpinnable(true);
	testSphereRotationYWidget->setCallback([](float value) {
		testSphereRotationY = value;
		rotationAdjustmentDirty = true;
	});

	auto testSphereRotationZWidget = gui->addVariable("testSphereRotationZ", testSphereRotationZ);
	testSphereRotationZWidget->setSpinnable(true);
	testSphereRotationZWidget->setCallback([](float value) {
		testSphereRotationZ = value;
		rotationAdjustmentDirty = true;
	});

	gui->addGroup("Visualization");

	gui->addVariable("showSelectedSpherePoses", showSelectedSpherePoses);
	gui->addVariable("showFloorTiles", showFloorTiles);
	
	gui->addVariable("showingHullModel", showingHullModel);

	gui->addVariable("controllingVisualizationCamera", controllingVisualizationCamera);

	Widget* panel = new Widget(nanoguiWindow);
	panel->setLayout(new BoxLayout(Orientation::Horizontal,
		Alignment::Middle, 0, 20));

	rayDisplayComboBox = std::make_shared<nanogui::ComboBox>(panel);
	rayDisplayComboBox->setItems(rayDisplayOptions);

	visualizationComboBox = std::make_shared<nanogui::ComboBox>(panel);
	visualizationComboBox->setItems(visualizationOptions);






	cameraPathSlider = new Slider(panel);
	cameraPathSlider->setValue(0.0f);
	cameraPathSlider->setFixedWidth(160);


	gui->addWidget("Camera Path", panel);


	screen->setVisible(true);
	screen->performLayout();
	//nanoguiWindow->center();


	screen->setVisible(true);
	screen->performLayout();




	glfwSetCursorPosCallback(window,
		[](GLFWwindow *, double x, double y) {
		bool cursorPosConsumedByUI = screen->cursorPosCallbackEvent(x, y);
		if (!cursorPosConsumedByUI) {
			// use mouse movement for camera movement

			if (firstMouse)
			{
				lastX = x;
				lastY = y;
				firstMouse = false;
			}

			GLfloat xoffset = x - lastX;
			GLfloat yoffset = lastY - y;

			lastX = x;
			lastY = y;

			if (cameraMouseControlsActive) {
				if (controllingVisualizationCamera) {
					visualizationCamera.ProcessMouseMovement(xoffset, yoffset);
				}
				else {
					thirdPersonCamera.ProcessMouseMovement(xoffset, yoffset);
				}

			}
		}
	}
	);

	glfwSetMouseButtonCallback(window,
		[](GLFWwindow *, int button, int action, int modifiers) {
		bool mouseButtonConsumedByUI = screen->mouseButtonCallbackEvent(button, action, modifiers);
		if (!mouseButtonConsumedByUI) {

			if (button == GLFW_MOUSE_BUTTON_RIGHT) {
				if (action == GLFW_PRESS) {
					cameraMouseControlsActive = true;
				}
				else if (action == GLFW_RELEASE) {
					cameraMouseControlsActive = false;
				}
			}
		}
	}
	);

	glfwSetKeyCallback(window,
		[](GLFWwindow *, int key, int scancode, int action, int mods) {
		bool keyConsumedByUI = screen->keyCallbackEvent(key, scancode, action, mods);
		if (!keyConsumedByUI) {
			// treat the key input as camera controls

			if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
				glfwSetWindowShouldClose(window, GL_TRUE);

			if (action == GLFW_PRESS)
				keys[key] = true;
			else if (action == GLFW_RELEASE)
				keys[key] = false;
		}
	}
	);

	glfwSetCharCallback(window,
		[](GLFWwindow *, unsigned int codepoint) {
		screen->charCallbackEvent(codepoint);
	}
	);

	glfwSetDropCallback(window,
		[](GLFWwindow *, int count, const char **filenames) {
		screen->dropCallbackEvent(count, filenames);
	}
	);

	glfwSetScrollCallback(window,
		[](GLFWwindow *, double x, double y) {
		screen->scrollCallbackEvent(x, y);
	}
	);

	glfwSetFramebufferSizeCallback(window,
		[](GLFWwindow *, int width, int height) {
		screenWidth = width;
		screenHeight = height;
		screen->resizeCallbackEvent(screenWidth, screenHeight);

		glViewport(0, 0, screenWidth, screenHeight);
	}
	);

	initShader();

	// Game loop
	while (!glfwWindowShouldClose(window)) {
		// Set frame time
		GLfloat currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		// Check if any events have been activated (key pressed, mouse moved etc.) and call corresponding response functions
		glfwPollEvents();
		doMovement();

		// note that we have to enable the depth test per-frame; it looks like nanogui disables it in order to draw the UI
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);


		glClearColor(0.2f, 0.25f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		draw();

		// Draw nanogui
		screen->drawContents();
		screen->drawWidgets();

		glfwSwapBuffers(window);
	}

	// Terminate GLFW, clearing any resources allocated by GLFW.
	glfwTerminate();

	return 0;
}
