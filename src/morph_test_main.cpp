// GLFW
//
#if defined(NANOGUI_GLAD)
    #if defined(NANOGUI_SHARED) && !defined(GLAD_GLAPI_EXPORT)
        #define GLAD_GLAPI_EXPORT
    #endif

    #include <glad/glad.h>
#else
    #if defined(__APPLE__)
        #define GLFW_INCLUDE_GLCOREARB
    #else
        #define GL_GLEXT_PROTOTYPES
    #endif
#endif

#include <GLFW/glfw3.h>

#include "globals.hpp"
#include "utils.hpp"

#include <nanogui/nanogui.h>
#include <iostream>
#include <memory>

#include "common.hpp"

#include "shader.hpp"
#include "mesh.hpp"
#include "model.hpp"

#include "filesystem.h"

#include "camera.hpp"

#include "path.hpp"

#include <opencv2/opencv.hpp>

#include "gltexture.hpp"

#include "framebuffer.hpp"
#include "cubemap.hpp"

#include <stb_image.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/calib3d.hpp>



#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/point_generators_3.h>
#include <CGAL/algorithm.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Polyhedron_items_with_id_3.h> 
#include <CGAL/convex_hull_3.h>
#include <vector>
#include <CGAL/AABB_tree.h>
#include <CGAL/AABB_traits.h>
#include <CGAL/AABB_triangle_primitive.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel  Hull_K;
typedef CGAL::Polyhedron_3<Hull_K, CGAL::Polyhedron_items_with_id_3> Polyhedron_3;
typedef Hull_K::Segment_3                              Segment_3;
typedef Hull_K::Point_3                                Point_3;
typedef Hull_K::Ray_3                                Ray_3;
typedef Hull_K::Triangle_3                                Triangle_3;
typedef CGAL::Creator_uniform_3<double, Point_3>  PointCreator;
typedef std::list<Triangle_3>::iterator Hull_Iterator;
typedef CGAL::AABB_triangle_primitive<Hull_K, Hull_Iterator> Hull_Primitive;
typedef CGAL::AABB_traits<Hull_K, Hull_Primitive> Hull_AABB_triangle_traits;
typedef CGAL::AABB_tree<Hull_AABB_triangle_traits> Hull_Tree;


ImageFormat RGBA8(GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE, 4);
ImageFormat RGBA32(GL_RGBA32F, GL_RGBA, GL_FLOAT, 4);






#include "json.hpp"

using json = nlohmann::json;


using namespace nanogui;

int screenWidth = 800;
int screenHeight = 800;

enum test_enum {
    Item1 = 0,
    Item2,
    Item3
};

bool bvar = true;
int ivar = 12345678;
double dvar = 3.1415926;
float fvar = (float)dvar;
std::string strval = "A string";
test_enum enumval = Item2;
Color colval(0.5f, 0.5f, 0.7f, 1.f);

Screen *screen = nullptr;

std::shared_ptr<Shader> meshShader;
std::shared_ptr<Shader> pathShader;
std::shared_ptr<Shader> spherePanoShader;
std::shared_ptr<Shader> equirectangularShader;
std::shared_ptr<Shader> encodeBarycentricShader;
//std::shared_ptr<Mesh> testMesh;
std::shared_ptr<Model> frustumModel;
std::shared_ptr<Model> cubeModel;
std::shared_ptr<Model> quadModel;

std::shared_ptr<Model> fullscreenQuadModel;

std::shared_ptr<Model> arrowModel;

std::shared_ptr<Path> sphericalCameraPath;

std::shared_ptr<Model> panoSphereModel;

Camera hullVisualizationCamera(glm::vec3(0.0f, 0.0f,0.0f)); // camera for viewing the test hulls
Camera visualizationCamera(glm::vec3(0.0f, 0.0f, 0.0f)); // camera for viewing the panorama

glm::vec3 lightPos(1.0f, 2.0f, 3.0f);

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

GLFWwindow* window;
bool keys[1024];

GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;

bool cameraMouseControlsActive = false;

ScanSession currentScan(SCAN_SESSION_LABEL);

float directionWeight = 0.0f;

float panoramaFOV = 90.0f;

bool renderingSimulatedView = false;

bool renderingWireframe = false;

std::vector<cv::Mat> fullCubemapImgs;

bool previewingInterpolatedSphericalTexture = false;

int baryTriImageSize = 300;
cv::Mat triImage;
cv::Point triPointsUI[3] = { cv::Point(0, baryTriImageSize), cv::Point(baryTriImageSize, baryTriImageSize), cv::Point(baryTriImageSize / 2, 0) };

std::vector<std::shared_ptr<Model>> hullModels;
std::shared_ptr<Model> interpolatedHullModel;	// represents the convex hull that is being distorted as we move along the barycentric coords

// represents the sphere that is rendered in the panorama.
// the vertices of the sphere are always the same uniform grid pattern.
// but the three UV coords for the model are distinct and change based on the barycentric coords of the user position.
std::shared_ptr<Model> interpolatedPanoSphereModel; 


std::vector<std::vector<glm::vec3>> autoKeypointPositionsForEachSphere;
std::vector<std::vector<glm::vec3>> manualKeypointPositionsForEachSphere;

std::vector<std::vector<glm::vec3>> *currentKeypointPositionsForEachSphere = &autoKeypointPositionsForEachSphere;

bool viewingMeshes = false;

std::shared_ptr<Framebuffer> equirectangularFramebuffer;

Slider* cameraPathSlider;

using imagesDataType = vector<pair<GLTexture, GLTexture::handleType>>;

std::vector<std::shared_ptr<GLTexture>> sphereTextures;

int frameNumber = 0;

float barycentric_coords[3] = { 1.0f / 3.0f, 1.0f / 3.0f, 1.0f / 3.0f };

int sphereCameraFrames[3];

std::shared_ptr<Cubemap> interpolatedHullCubemap;
int cubemapRenderTextureSize = 512;

glm::mat4 cubemapProjectionMat;
std::vector<glm::mat4> openglCubemapViewMats;	// view matrices for each side of the cubemap, organized in the order used by OpenGL

std::vector<glm::mat4> importedCubemapViewMats; // view matrices for each side of the cubemap, organized in the order in the cubemap images I generate

int panoSphereNumSegments = 128;
int panoSphereNumRings = panoSphereNumSegments / 2;

bool morphingPano = true;

vector<string> textureOptions = { "Blend", "NN", "A", "B", "C" };
std::shared_ptr<nanogui::ComboBox> blendOptionsComboBox;

//ImageFormat equirectangularImageFormat = RGBA8;
ImageFormat equirectangularImageFormat = RGBA32;
ImageFormat cubemapImageFormat = RGBA32;
GLfloat *equirectangularPixels;
GLfloat *cubemapFacePixels;

std::vector<cv::MouseCallback> manualCorrespondenceMouseCallbacks;



struct PoseClosestTo
{
	bool operator () (const Pose & lhs, const Pose & rhs)
	{
		if (lhs.Timestamp < rhs.Timestamp) {
			return true;
		}
		else {
			return false;
		}
	}
};

// http://forums.codeguru.com/showthread.php?320298-Searching-a-list-for-quot-closest-quot-floating-point-value-using-STL
// does a binary search to find the closest pose in the path that is AFTER or EQUAL TO the requested timestamp
size_t findPoseIndexFromTimestamp(float timestamp) {
	std::vector<Pose> poses = sphericalCameraPoses;
	
	Pose x(sphericalCamera, timestamp, glm::vec3(), glm::quat(), glm::mat4(), 0);
	std::vector<Pose>::iterator it = std::lower_bound(poses.begin(), poses.end(), x, PoseClosestTo());

	if (it == poses.end()) 
	{
		// the last element is closest
		if (!poses.empty()) {
			--it;	// go to last element
		}
	}
	else if (it != poses.begin()) 
	{
		std::vector<Pose>::iterator it1 = it;
		--it1;

		float dist1 = fabs(x.Timestamp - it1->Timestamp);
		float dist = fabs(x.Timestamp - it->Timestamp);

		if (dist1 < dist) {
			--it;
		}
	}

	// closest is at "it"
	size_t poseIndex = (it - poses.begin());

	return poseIndex;
}



int getSphericalCameraFrameIndexFromPose(Pose p) {

	int locatableCameraSyncFrameIndex = scanData["hololens_sync_frame"] - 1;	// go from 1-indexed to 0-indexed
	float locatableCameraSyncFrameTimestamp = sphericalCameraPoses[locatableCameraSyncFrameIndex].Timestamp;

	int sphericalCameraSyncFrameIndex = scanData["spherical_sync_frame"] - 1;	// go from 1-indexed to 0-indexed
	double sphericalCameraFPS = scanData["spherical_fps"];

	float locatableCameraTimestamp = p.Timestamp;
	float secondsSinceCalibrationTime = (locatableCameraTimestamp - locatableCameraSyncFrameTimestamp);
	int sphericalCameraFrameIndex = (int)std::round(sphericalCameraSyncFrameIndex + (secondsSinceCalibrationTime * sphericalCameraFPS));
	return sphericalCameraFrameIndex;
}


Pose makeInterpolatedPose(Pose poseA, Pose poseB, float desiredTimestamp) {
	float t = (desiredTimestamp - poseA.Timestamp) / (poseB.Timestamp - poseA.Timestamp);
	//std::cout << "t: " << t << std::endl;

	glm::vec3 interpolatedPos = glm::mix(poseA.Position, poseB.Position, t);
	glm::quat interpolatedRotQuat = glm::slerp(poseA.RotationQuaternion, poseB.RotationQuaternion, t);
	glm::mat4 interpolatedRotMat(interpolatedRotQuat);

	float interpolatedPoseIndex = glm::mix(poseA.PoseIndex, poseB.PoseIndex, t);

	Pose interpolatedPose(sphericalCamera, desiredTimestamp, interpolatedPos, interpolatedRotQuat, interpolatedRotMat, interpolatedPoseIndex);
	return interpolatedPose;
}

Pose getInterpolatedPoseFromSphericalCameraFrameIndex(int sphericalCameraFrameIndex) {
	int locatableCameraSyncFrameIndex = scanData["hololens_sync_frame"] - 1;	// go from 1-indexed to 0-indexed
	float locatableCameraSyncFrameTimestamp = sphericalCameraPoses[locatableCameraSyncFrameIndex].Timestamp;

	int sphericalCameraSyncFrameIndex = scanData["spherical_sync_frame"] - 1;	// go from 1-indexed to 0-indexed
	double sphericalCameraFPS = scanData["spherical_fps"];

	float secondsSinceCalibrationTime = (float)(sphericalCameraFrameIndex - sphericalCameraSyncFrameIndex) / sphericalCameraFPS;

	float locatableCameraTimestamp = secondsSinceCalibrationTime + locatableCameraSyncFrameTimestamp;

	// need to find which poses are immediately before/after the desired pose's timestamp
	int poseBIndex = findPoseIndexFromTimestamp(locatableCameraTimestamp);

	int poseAIndex = std::max(poseBIndex - 1, 0);

	Pose poseA = sphericalCameraPoses[poseAIndex];
	Pose poseB = sphericalCameraPoses[poseBIndex];

	return makeInterpolatedPose(poseA, poseB, locatableCameraTimestamp);
}












bool triWindowDrag = false;
void BarycentricTriCallbackFunc(int event, int x, int y, int flags, void* userdata) {
	bool shouldUpdateCoords = false;

	if (event == CV_EVENT_LBUTTONDOWN && !triWindowDrag) {
		//std::cout << "left down " << x << " " << y << std::endl;
		triWindowDrag = true;
		shouldUpdateCoords = true;
	}

	if (event == CV_EVENT_MOUSEMOVE && triWindowDrag) {
		//std::cout << "left drag " << x << " " << y << std::endl;
		shouldUpdateCoords = true;
	}

	if (event == CV_EVENT_LBUTTONUP && triWindowDrag) {
		//std::cout << "left up " << x << " " << y << std::endl;
		triWindowDrag = false;
	}

	if (shouldUpdateCoords) {
		float x1 = (float)triPointsUI[0].x;
		float y1 = (float)triPointsUI[0].y;
		float x2 = (float)triPointsUI[1].x;
		float y2 = (float)triPointsUI[1].y;
		float x3 = (float)triPointsUI[2].x;
		float y3 = (float)triPointsUI[2].y;

		float detT = (y2 - y3) * (x1 - x3) + (x3 - x2) * (y1 - y3);
		float bary1 = ((y2 - y3)*(x - x3) + (x3 - x2)*(y - y3)) / detT;
		float bary2 = ((y3 - y1) * (x - x3) + (x1 - x3) * (y - y3)) / detT;
		float bary3 = 1.0f - bary1 - bary2;

		if ((bary1 >= 0.0f && bary1 <= 1.0f) && (bary2 >= 0.0f && bary2 <= 1.0f) && (bary3 >= 0.0f && bary3 <= 1.0f)) {
			barycentric_coords[0] = bary1;
			barycentric_coords[1] = bary2;
			barycentric_coords[2] = bary3;

			//std::cout << "barycentric_coords " << barycentric_coords[0] << " " << barycentric_coords[1] << " " << barycentric_coords[2] << std::endl;
		}
	}
}

std::string getFramePath(int frameIndex) {
	auto frameLabel = string_format("frame%05d", frameIndex + 1); // frame images are 1-indexed
	auto framePath = FileSystem::getPath(string_format("resources/scans/%s/frames/%s.jpg", currentScan.Label.c_str(), frameLabel.c_str()));
	return framePath;
}

std::string getCubemapPath(int frameIndex) {
	auto cubemapLabel = string_format("pano_frame%05d_cubemap", frameIndex + 1); // frame images are 1-indexed
	auto cubemapPath = FileSystem::getPath(string_format("resources/scans/%s/pano_frames/cubemaps/%s.jpg", currentScan.Label.c_str(), cubemapLabel.c_str()));
	return cubemapPath;
}



// http://stackoverflow.com/questions/29678510/convert-21-equirectangular-panorama-to-cube-map
// Define our six cube faces. 
// 0 - 3 are side faces, clockwise order
// 4 and 5 are top and bottom, respectively
float faceTransform[6][2] =
{
	{ 0, 0 },
	{ M_PI / 2, 0 },
	{ M_PI, 0 },
	{ -M_PI / 2, 0 },
	{ 0, -M_PI / 2 },
	{ 0, M_PI / 2 }
};

// Map a part of the equirectangular panorama (in) to a cube face
// (face). The ID of the face is given by faceId. The desired
// width and height are given by width and height. 
inline void createCubeMapFace(const cv::Mat &in, cv::Mat &face,
	int faceId = 0, const int width = -1,
	const int height = -1) {

	float inWidth = in.cols;
	float inHeight = in.rows;

	// Allocate map
	cv::Mat mapx(height, width, CV_32F);
	cv::Mat mapy(height, width, CV_32F);

	// Calculate adjacent (ak) and opposite (an) of the
	// triangle that is spanned from the sphere center 
	//to our cube face.
	const float an = sin(M_PI / 4);
	const float ak = cos(M_PI / 4);

	const float ftu = faceTransform[faceId][0];
	const float ftv = faceTransform[faceId][1];

	// For each point in the target image, 
	// calculate the corresponding source coordinates. 
	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++) {

			// Map face pixel coordinates to [-1, 1] on plane
			float nx = (float)y / (float)height - 0.5f;
			float ny = (float)x / (float)width - 0.5f;

			nx *= 2;
			ny *= 2;

			// Map [-1, 1] plane coords to [-an, an]
			// thats the coordinates in respect to a unit sphere 
			// that contains our box. 
			nx *= an;
			ny *= an;

			float u, v;

			// Project from plane to sphere surface.
			if (ftv == 0) {
				// Center faces
				u = atan2(nx, ak);
				v = atan2(ny * cos(u), ak);
				u += ftu;
			}
			else if (ftv > 0) {
				// Bottom face 
				float d = sqrt(nx * nx + ny * ny);
				v = M_PI / 2 - atan2(d, ak);
				u = atan2(ny, nx);
			}
			else {
				// Top face
				float d = sqrt(nx * nx + ny * ny);
				v = -M_PI / 2 + atan2(d, ak);
				u = atan2(-ny, nx);
			}

			// Map from angular coordinates to [-1, 1], respectively.
			u = u / (M_PI);
			v = v / (M_PI / 2);

			// Warp around, if our coordinates are out of bounds. 
			while (v < -1) {
				v += 2;
				u += 1;
			}
			while (v > 1) {
				v -= 2;
				u += 1;
			}

			while (u < -1) {
				u += 2;
			}
			while (u > 1) {
				u -= 2;
			}

			// Map from [-1, 1] to in texture space
			u = u / 2.0f + 0.5f;
			v = v / 2.0f + 0.5f;

			u = u * (inWidth - 1);
			v = v * (inHeight - 1);

			// Save the result for this pixel in map
			mapx.at<float>(x, y) = u;
			mapy.at<float>(x, y) = v;
		}
	}

	// Recreate output image if it has wrong size or type. 
	if (face.cols != width || face.rows != height ||
		face.type() != in.type()) {
		face = cv::Mat(width, height, in.type());
	}

	// Do actual resampling using OpenCV's remap
	cv::remap(in, face, mapx, mapy,
		CV_INTER_LINEAR, cv::BORDER_CONSTANT, cv::Scalar(0, 0, 0));
}

glm::mat3 cubemap_side_camera_matrix;



cv::Mat loadOrCreateCubemap(int sphereCameraFrame) {
	std::string cubemapPath = getCubemapPath(sphereCameraFrame);
	std::cout << "attempting to load cubemap from " << cubemapPath << std::endl;

	cv::Mat attemptedLoadCubemapImg = cv::imread(cubemapPath);
	if (attemptedLoadCubemapImg.data != NULL) {
		std::cout << "cubemap found" << std::endl;
		return attemptedLoadCubemapImg;
	}
	else {
		std::cout << "cubemap not found, generating" << std::endl;
		cv::Mat sphereCameraFrameImg = cv::imread(getPanoFramePath(sphereCameraFrame));

		int cubemapSize = sphereCameraFrameImg.cols / 4;
		std::cout << "cubemapSize " << cubemapSize << std::endl;

		std::string in_img_winname = "sphere_img_" + std::to_string(sphereCameraFrame);

		cv::namedWindow(in_img_winname, cv::WINDOW_NORMAL);
		cv::imshow(in_img_winname, sphereCameraFrameImg);

		cv::Mat allFacesImg(cubemapSize, cubemapSize * 6, sphereCameraFrameImg.type());
		
		for (int faceIdx = 0; faceIdx < 6; faceIdx++) {
			cv::Mat faceImg = allFacesImg(cv::Rect(faceIdx*cubemapSize, 0, cubemapSize, cubemapSize));
			createCubeMapFace(sphereCameraFrameImg, faceImg, faceIdx, cubemapSize, cubemapSize);
		}

		bool writeSuccess = cv::imwrite(cubemapPath, allFacesImg);
		if (writeSuccess) {
			std::cout << "successfully saved cubemap to " << cubemapPath << std::endl;
		}
		else {
			std::cout << "failed to save cubemap" << std::endl;
		}

		return allFacesImg;
	}
}


std::vector<glm::mat4> corrected_sphere_rotation_matrices;
std::vector<glm::mat4> corrected_inverse_sphere_rotation_matrices;

// K1 and K2 are the intrinsic camera matrices for each camera
void generateFundamentalMatBetweenHLPoses(Pose & pose1, Pose & pose2, glm::mat4 & viewMat1, glm::mat4 & viewMat2, glm::mat3 K1, glm::mat3 K2, cv::Mat* out_F) {

	std::cout << "K1: " << std::endl << glm::to_string(K1) << std::endl;
	std::cout << "K2: " << std::endl << glm::to_string(K2) << std::endl;

	std::cout << "testFrontToSphereCameraRotation: " << std::endl << glm::to_string(testFrontToSphereCameraRotation) << std::endl;

	glm::mat4 modelViewMat1 = viewMat1 * pose1.ModelMatrix * testFrontToSphereCameraRotation;
	
	glm::mat4 modelViewMat2 = viewMat2 * pose2.ModelMatrix * testFrontToSphereCameraRotation;

	std::cout << "modelViewMat1: " << std::endl << glm::to_string(modelViewMat1) << std::endl;

	std::cout << "modelViewMat2: " << std::endl << glm::to_string(modelViewMat2) << std::endl;

	glm::mat4 mat1To2 = ((modelViewMat2) * glm::inverse(modelViewMat1));

	std::cout << "mat1To2: " << std::endl << glm::to_string(mat1To2) << std::endl;

	glm::mat3 R(mat1To2);

	std::cout << "R: " << std::endl << glm::to_string(R) << std::endl;

	glm::vec3 t(mat1To2[3]);

	std::cout << "t: " << std::endl << glm::to_string(t) << std::endl;

	float t_cross_array[9] = {
		0, t.z, -t.y, -t.z, 0, t.x, t.y, -t.x, 0
	};
	glm::mat3 t_cross;
	memcpy(glm::value_ptr(t_cross), t_cross_array, sizeof(t_cross_array));

	std::cout << "t_cross: " << std::endl << glm::to_string(t_cross) << std::endl;

	glm::mat3 E = t_cross * R;

	std::cout << "E: " << std::endl << glm::to_string(E) << std::endl;

	glm::mat3 F = glm::inverse(glm::transpose(K2)) * E * glm::inverse(K1);

	std::cout << "F: " << std::endl << glm::to_string(F) << std::endl;

	if (out_F->cols != 3 || out_F->rows != 3 || out_F->type() != CV_32FC1) {
		(*out_F) = cv::Mat(3, 3, CV_32F);
	}
	memcpy(out_F->data, glm::value_ptr(F), 9 * sizeof(float));
	cv::transpose(*out_F, *out_F);
	std::cout << "out_F: " << std::endl << (*out_F) << std::endl;
}

void drawEpipolarLines(cv::Mat & img_1, cv::Mat & img_2, std::vector<cv::KeyPoint> commonKeypoints_1, std::vector<cv::KeyPoint> commonKeypoints_2, cv::Mat & F, cv::Scalar & color_1, cv::Scalar & color_2, int thickness) {
	if (commonKeypoints_1.size() > 0 && commonKeypoints_2.size() > 0) {
		std::vector<cv::Point2f> points_1;
		std::vector<cv::Point2f> points_2;

		for (auto & kp_1 : commonKeypoints_1) {
			points_1.push_back(kp_1.pt);
		}

		for (auto & kp_2 : commonKeypoints_2) {
			points_2.push_back(kp_2.pt);
		}

		cv::Mat outImg_1 = img_1;
		cv::Mat outImg_2 = img_2;

		std::vector<cv::Vec3f> epilines_1;
		std::vector<cv::Vec3f> epilines_2;
		cv::computeCorrespondEpilines(points_1, 1, F, epilines_1);
		cv::computeCorrespondEpilines(points_2, 2, F, epilines_2);

		for (size_t pointIdx = 0; pointIdx < points_1.size(); pointIdx++) {
			{
				{
					auto img = outImg_2;
					auto pt1 = cv::Point(0, -epilines_1[pointIdx][2] / epilines_1[pointIdx][1]);
					auto pt2 = cv::Point(img.cols, -(epilines_1[pointIdx][2] + epilines_1[pointIdx][0] * img.cols) / epilines_1[pointIdx][1]);
					auto color = color_1; // applying lines from img 1 onto img 2
					
					cv::line(img, pt1, pt2, color, thickness);

					cv::circle(img, points_2[pointIdx], 10, cv::Scalar(0, 255, 255), -1, CV_AA);
				}

				{
					auto img = outImg_1;
					auto pt1 = cv::Point(0, -epilines_2[pointIdx][2] / epilines_2[pointIdx][1]);
					auto pt2 = cv::Point(img.cols, -(epilines_2[pointIdx][2] + epilines_2[pointIdx][0] * img.cols) / epilines_2[pointIdx][1]);
					auto color = color_2; // applying lines from img 2 onto img 1
					
					cv::line(img, pt1, pt2, color, thickness);

					cv::circle(img, points_1[pointIdx], 10, cv::Scalar(0, 255, 255), -1, CV_AA);
				}
			}
		}
	}
}


std::vector<std::vector<glm::vec3>> GenerateKeypointPositionsForEachSphere() {

	auto orb = cv::ORB::create();
	orb->setMaxFeatures(4000);

	std::vector<std::vector<cv::Mat>> separatedCubemapImgs;
	std::vector<std::vector<cv::Mat>> cubemapDescs;
	std::vector<std::vector<vector<cv::KeyPoint>>> cubemapKeypoints;

	for (int panoIdx = 0; panoIdx < 3; panoIdx++) {

		// set up the camera matrix representing each cubemap side
		cubemap_side_camera_matrix[0][0] = (float)cubeSidePixels / 2.0f;
		cubemap_side_camera_matrix[1][1] = (float)cubeSidePixels / 2.0f;
		cubemap_side_camera_matrix[2][0] = (float)cubeSidePixels / 2.0f;
		cubemap_side_camera_matrix[2][1] = (float)cubeSidePixels / 2.0f;
		cubemap_side_camera_matrix[2][2] = 1.0f;

		std::vector<cv::Mat> cubesideImgsForThisPano;
		std::vector<cv::Mat> cubesideDescsForThisPano;
		std::vector<vector<cv::KeyPoint>> cubesideKeypointsForThisPano;

		cv::Mat allFacesImg = fullCubemapImgs[panoIdx];

		for (int sideIdx = 0; sideIdx < 6; sideIdx++) {
			cv::Mat cubeSideImg = allFacesImg(cv::Rect(cubeSidePixels*sideIdx, 0, cubeSidePixels, cubeSidePixels));

			cubesideImgsForThisPano.push_back(cubeSideImg);

			vector<cv::KeyPoint> kp;
			cv::Mat desc;	// descriptors
			orb->detectAndCompute(cubeSideImg, cv::noArray(), kp, desc);
			int numKeypoints = kp.size();

			//cv::Mat cubesideImgWithKeyPoints;
			//cv::drawKeypoints(cubeSideImg, kp, cubesideImgWithKeyPoints, cv::Scalar(255, 0, 0));

			//std::string cubemap_winname = "sphere_" + std::to_string(panoIdx) + "_side_" + std::to_string(sideIdx);

			//std::cout << "in " << cubemap_winname << ", found " << numKeypoints << " keypoints" << std::endl;

			//cv::namedWindow(cubemap_winname, cv::WINDOW_NORMAL);
			//cv::moveWindow(cubemap_winname, 20, 20);
			//cv::imshow(cubemap_winname, cubesideImgWithKeyPoints);

			cubesideDescsForThisPano.push_back(desc);
			cubesideKeypointsForThisPano.push_back(kp);
		}

		separatedCubemapImgs.push_back(cubesideImgsForThisPano);
		cubemapDescs.push_back(cubesideDescsForThisPano);
		cubemapKeypoints.push_back(cubesideKeypointsForThisPano);

		
	}

	std::vector<cv::Scalar> testColors;
	testColors.push_back(cv::Scalar(255, 0, 0));
	testColors.push_back(cv::Scalar(255, 165, 0));
	testColors.push_back(cv::Scalar(255, 255, 0));
	testColors.push_back(cv::Scalar(0, 255, 0));
	testColors.push_back(cv::Scalar(0, 0, 255));
	testColors.push_back(cv::Scalar(0, 255, 255));


	// now that we have keypoints for each of the cubemaps, find the matches common to all 3 of them

	double nn_match_ratio = 0.8f;
	auto matcher = cv::DescriptorMatcher::create(cv::DescriptorMatcher::BRUTEFORCE_HAMMING);


	// determine which sides of the cubemap are "corresponding" -- i.e. facing as close as possible the same direction
	int correspondingFaceMaps[6][3] = {
		{ 0, 0, 0 },	// forward
		{ 1, 1, 1 },	// right
		{ 2, 2, 2 },	// back
		{ 3, 3, 3 },	// left
		{ 4, 4, 4 },	// up
		{ 5, 5, 5 }		// down
	};

	std::vector<glm::vec4> forwardForEachCubeFace;
	forwardForEachCubeFace.push_back(glm::vec4(0, 0, -1, 0));
	forwardForEachCubeFace.push_back(glm::vec4(1, 0, 0, 0));
	forwardForEachCubeFace.push_back(glm::vec4(0, 0, 1, 0));
	forwardForEachCubeFace.push_back(glm::vec4(-1, 0, 0, 0));
	forwardForEachCubeFace.push_back(glm::vec4(0, 1, 0, 0));
	forwardForEachCubeFace.push_back(glm::vec4(0, -1, 0, 0));

	for (int faceIdx = 0; faceIdx < 6; faceIdx++) {
		auto sphereA_forwardForThisFace = corrected_sphere_rotation_matrices[0] * forwardForEachCubeFace[faceIdx];

		int sphereB_minIdx = 0;
		float sphereA_sphereB_max_dot = -1000.0f;
		int sphereC_minIdx = 0;
		float sphereA_sphereC_max_dot = -1000.0f;

		for (int correspondingFaceIdx = 0; correspondingFaceIdx < 6; correspondingFaceIdx++) {
			auto sphereB_forwardForThatFace = corrected_sphere_rotation_matrices[1] * forwardForEachCubeFace[correspondingFaceIdx];
			auto sphereC_forwardForThatFace = corrected_sphere_rotation_matrices[2] * forwardForEachCubeFace[correspondingFaceIdx];

			auto a_b_dot = glm::dot(sphereA_forwardForThisFace, sphereB_forwardForThatFace);
			auto a_c_dot = glm::dot(sphereA_forwardForThisFace, sphereC_forwardForThatFace);

			if (a_b_dot > sphereA_sphereB_max_dot) {
				sphereB_minIdx = correspondingFaceIdx;
				sphereA_sphereB_max_dot = a_b_dot;
			}

			if (a_c_dot > sphereA_sphereC_max_dot) {
				sphereC_minIdx = correspondingFaceIdx;
				sphereA_sphereC_max_dot = a_c_dot;
			}
		}

		correspondingFaceMaps[faceIdx][1] = sphereB_minIdx;
		correspondingFaceMaps[faceIdx][2] = sphereC_minIdx;
	}

	for (int row = 0; row < 6; row++) {
		std::cout << correspondingFaceMaps[row][0] << " " << correspondingFaceMaps[row][1] << " " << correspondingFaceMaps[row][2] << std::endl;
	}

	std::vector<std::vector<std::vector<cv::KeyPoint>>> commonKeypointsOnEachPanoForAllFaces;

	std::vector<std::vector<cv::KeyPoint>> commonKeypointsForAllFaces_A;
	std::vector<std::vector<cv::KeyPoint>> commonKeypointsForAllFaces_B;
	std::vector<std::vector<cv::KeyPoint>> commonKeypointsForAllFaces_C;

	for (int sphereA_sideIdx = 0; sphereA_sideIdx < 6; sphereA_sideIdx++) {
		int sphereB_sideIdx = correspondingFaceMaps[sphereA_sideIdx][1];
		int sphereC_sideIdx = correspondingFaceMaps[sphereA_sideIdx][2];

		std::vector<std::vector<cv::DMatch>> matches_a_b;
		std::vector<std::vector<cv::DMatch>> matches_b_c;
		std::vector<std::vector<cv::DMatch>> matches_c_a;

		cv::Mat desc_a = cubemapDescs[0][sphereA_sideIdx];
		cv::Mat desc_b = cubemapDescs[1][sphereB_sideIdx];
		cv::Mat desc_c = cubemapDescs[2][sphereC_sideIdx];

		vector<cv::KeyPoint> kp_a = cubemapKeypoints[0][sphereA_sideIdx];
		vector<cv::KeyPoint> kp_b = cubemapKeypoints[1][sphereB_sideIdx];
		vector<cv::KeyPoint> kp_c = cubemapKeypoints[2][sphereC_sideIdx];

		matcher->knnMatch(desc_a, desc_b, matches_a_b, 2);
		matcher->knnMatch(desc_b, desc_c, matches_b_c, 2);
		matcher->knnMatch(desc_c, desc_a, matches_c_a, 2);

		std::cout << "before filtering, " << matches_a_b.size() << " matches between A and B" << std::endl;
		std::cout << "before filtering, " << matches_b_c.size() << " matches between B and C" << std::endl;
		std::cout << "before filtering, " << matches_c_a.size() << " matches between C and A" << std::endl;

		std::vector<cv::DMatch> filtered_matches_a_b;
		std::vector<cv::DMatch> filtered_matches_b_c;
		std::vector<cv::DMatch> filtered_matches_c_a;

		double findFundamentalMatParam1 = 3.0;
		double findFundamentalMatParam2 = 0.99;
		auto findFundamentalMatMethod = CV_FM_RANSAC;

		//============

		for (unsigned i = 0; i < matches_a_b.size(); i++) {
			if (matches_a_b[i][0].distance < nn_match_ratio * matches_a_b[i][1].distance) {
				auto good_match = matches_a_b[i][0];
				filtered_matches_a_b.push_back(good_match);
			}
		}

		std::cout << "after initial filtering, " << filtered_matches_a_b.size() << " matches between A and B" << std::endl;

		// load in the initially filtered keypoints and try to find the fundamental matrix between them. use this to determine some inliers/outliers

		vector<cv::Point2f> filtered_keypoints_ab_a;
		vector<cv::Point2f> filtered_keypoints_ab_b;
		for (auto & match : filtered_matches_a_b) {
			auto a_index = match.queryIdx;
			auto b_index = match.trainIdx;
			filtered_keypoints_ab_a.push_back(kp_a[a_index].pt);
			filtered_keypoints_ab_b.push_back(kp_b[b_index].pt);
		}

		cv::Mat a_to_b_mask;
		cv::Mat a_to_b_F = cv::findFundamentalMat(filtered_keypoints_ab_a, filtered_keypoints_ab_b, a_to_b_mask, findFundamentalMatMethod, findFundamentalMatParam1, findFundamentalMatParam2);

		std::vector<cv::DMatch> outlier_filtered_matches_a_b;
		for (unsigned i = 0; i < filtered_matches_a_b.size(); i++) {
			bool mask_value = a_to_b_mask.at<bool>(i);

			if (mask_value == 1) {
				outlier_filtered_matches_a_b.push_back(filtered_matches_a_b[i]);
			}
		}
		filtered_matches_a_b = outlier_filtered_matches_a_b;

		std::cout << "after fundamental matrix filtering, " << filtered_matches_a_b.size() << " matches between A and B" << std::endl;

		//============

		for (unsigned i = 0; i < matches_b_c.size(); i++) {
			if (matches_b_c[i][0].distance < nn_match_ratio * matches_b_c[i][1].distance) {
				auto good_match = matches_b_c[i][0];
				filtered_matches_b_c.push_back(good_match);
			}
		}

		std::cout << "after initial filtering, " << filtered_matches_b_c.size() << " matches between B and C" << std::endl;

		vector<cv::Point2f> filtered_keypoints_bc_b;
		vector<cv::Point2f> filtered_keypoints_bc_c;
		for (auto & match : filtered_matches_b_c) {
			auto b_index = match.queryIdx;
			auto c_index = match.trainIdx;
			filtered_keypoints_bc_b.push_back(kp_b[b_index].pt);
			filtered_keypoints_bc_c.push_back(kp_c[c_index].pt);
		}

		cv::Mat b_to_c_mask;
		cv::Mat b_to_c_F = cv::findFundamentalMat(filtered_keypoints_bc_b, filtered_keypoints_bc_c, b_to_c_mask, findFundamentalMatMethod, findFundamentalMatParam1, findFundamentalMatParam2);


		std::vector<cv::DMatch> outlier_filtered_matches_b_c;
		for (unsigned i = 0; i < filtered_matches_b_c.size(); i++) {
			bool mask_value = b_to_c_mask.at<bool>(i);

			if (mask_value == 1) {
				outlier_filtered_matches_b_c.push_back(filtered_matches_b_c[i]);
			}
		}
		filtered_matches_b_c = outlier_filtered_matches_b_c;

		std::cout << "after fundamental matrix filtering, " << filtered_matches_b_c.size() << " matches between B and C" << std::endl;

		//============

		for (unsigned i = 0; i < matches_c_a.size(); i++) {
			if (matches_c_a[i][0].distance < nn_match_ratio * matches_c_a[i][1].distance) {
				auto good_match = matches_c_a[i][0];
				filtered_matches_c_a.push_back(good_match);
			}
		}

		std::cout << "after initial filtering, " << filtered_matches_c_a.size() << " matches between C and A" << std::endl;

		vector<cv::Point2f> filtered_keypoints_ca_c;
		vector<cv::Point2f> filtered_keypoints_ca_a;
		for (auto & match : filtered_matches_c_a) {
			auto c_index = match.queryIdx;
			auto a_index = match.trainIdx;
			filtered_keypoints_ca_c.push_back(kp_c[c_index].pt);
			filtered_keypoints_ca_a.push_back(kp_a[a_index].pt);
		}

		cv::Mat c_to_a_mask;
		cv::Mat c_to_a_F = cv::findFundamentalMat(filtered_keypoints_ca_c, filtered_keypoints_ca_a, c_to_a_mask, findFundamentalMatMethod, findFundamentalMatParam1, findFundamentalMatParam2);


		std::vector<cv::DMatch> outlier_filtered_matches_c_a;
		for (unsigned i = 0; i < filtered_matches_c_a.size(); i++) {
			bool mask_value = c_to_a_mask.at<bool>(i);

			if (mask_value == 1) {
				outlier_filtered_matches_c_a.push_back(filtered_matches_c_a[i]);
			}
		}
		filtered_matches_c_a = outlier_filtered_matches_c_a;

		std::cout << "after fundamental matrix filtering, " << filtered_matches_c_a.size() << " matches between C and A" << std::endl;

		//============

		std::unordered_map<int, int> indices_b_to_c;
		std::unordered_map<int, int> indices_b_to_a;

		for (unsigned i = 0; i < filtered_matches_a_b.size(); i++) {
			auto good_match = filtered_matches_a_b[i];
			auto a_index = good_match.queryIdx;
			auto b_index = good_match.trainIdx;

			indices_b_to_a.emplace(b_index, a_index);
		}

		for (unsigned i = 0; i < filtered_matches_b_c.size(); i++) {
			auto good_match = filtered_matches_b_c[i];
			auto b_index = good_match.queryIdx;
			auto c_index = good_match.trainIdx;

			indices_b_to_c.emplace(b_index, c_index);
		}



		std::vector<std::vector<int>> triplet_abc_indices;

		for (auto it : indices_b_to_a) {
			int b_index = it.first;
			int a_index = it.second;

			if (indices_b_to_c.find(b_index) != indices_b_to_c.end()) {
				// if the other list has a match

				int c_index = indices_b_to_c.at(b_index);

				vector<int> abc_indices = { a_index, b_index, c_index };
				triplet_abc_indices.push_back(abc_indices);
			}
		}

		int numTripletMatches = triplet_abc_indices.size();
		std::cout << "numTripletMatches: " << numTripletMatches << std::endl;


		std::vector<cv::KeyPoint> commonKeypoints_A;
		std::vector<cv::KeyPoint> commonKeypoints_B;
		std::vector<cv::KeyPoint> commonKeypoints_C;

		for (auto triplet : triplet_abc_indices) {
			int kpIdx_A = triplet[0];
			int kpIdx_B = triplet[1];
			int kpIdx_C = triplet[2];

			commonKeypoints_A.push_back(kp_a[kpIdx_A]);
			commonKeypoints_B.push_back(kp_b[kpIdx_B]);
			commonKeypoints_C.push_back(kp_c[kpIdx_C]);
		}

		cv::Mat thisFaceImg_A = separatedCubemapImgs[0][sphereA_sideIdx];
		cv::Mat thisFaceImg_B = separatedCubemapImgs[1][sphereB_sideIdx];
		cv::Mat thisFaceImg_C = separatedCubemapImgs[2][sphereC_sideIdx];

		cv::Scalar approx_line_color_A(0, 0, 128);
		cv::Scalar approx_line_color_B(0, 128, 0);
		cv::Scalar approx_line_color_C(128, 0, 0);
		int approxLineThickness = 1;

		commonKeypointsForAllFaces_A.push_back(commonKeypoints_A);
		commonKeypointsForAllFaces_B.push_back(commonKeypoints_B);
		commonKeypointsForAllFaces_C.push_back(commonKeypoints_C);
	}

	commonKeypointsOnEachPanoForAllFaces.push_back(commonKeypointsForAllFaces_A);
	commonKeypointsOnEachPanoForAllFaces.push_back(commonKeypointsForAllFaces_B);
	commonKeypointsOnEachPanoForAllFaces.push_back(commonKeypointsForAllFaces_C);

	// now that we have a set of common keypoints on each cubemap image, we must convert the keypoints into the XYZ cartesian coordinates that correspond to them.

	std::vector<std::vector<glm::vec3>> kpForEachSphere;

	for (int panoIdx = 0; panoIdx < 3; panoIdx++) {

		std::vector<glm::vec3> keypointPositionsForThisSphere;

		auto commonKeypointsOnEachPanoForCorrespondingFaces = commonKeypointsOnEachPanoForAllFaces[panoIdx];

		for (int sideIdx = 0; sideIdx < 6; sideIdx++) {
			auto commonKeypointsForOneSide = commonKeypointsOnEachPanoForCorrespondingFaces[sideIdx];
			int sideIdxForThisPano = correspondingFaceMaps[sideIdx][panoIdx];

			for (auto kp : commonKeypointsForOneSide) {

				glm::vec3 kpPosition = getKeypointOnSphereFromCubemapKeypoint(kp.pt.x, kp.pt.y, cubeSidePixels, sideIdxForThisPano);

				// apply the rotation matrix from the sphere pose

				auto kpPositionRotated = corrected_sphere_rotation_matrices[panoIdx] * glm::vec4(kpPosition, 0);

				//keypointPositionsForThisSphere.push_back(kpPosition);
				keypointPositionsForThisSphere.push_back(kpPositionRotated);
			}
		}

		kpForEachSphere.push_back(keypointPositionsForThisSphere);
	}


	return kpForEachSphere;
}



void GenerateHullsFromKeypointPositionsForEachSphere(std::vector<std::vector<glm::vec3>> kpForEachSphere) {
	// TODO: generate the models for each hull

	CGAL::Random_points_in_sphere_3<Point_3, PointCreator> gen(1.0f);


	// TODO: currently ignoring flipped triangles, assuming topology matches the first sphere.
	auto testHullModel = Model::CreateConvexHullFromPoints(kpForEachSphere[0]);

	auto referenceModelVertices = testHullModel->meshes[0].vertices;
	auto commonIndices = testHullModel->meshes[0].indices;

	hullModels.clear();

	for (int panoIdx = 0; panoIdx < 3; panoIdx++) {

		std::shared_ptr<Model> hullModel = std::make_shared<Model>();

		std::vector<Vertex> hullVertices;
		
		for (int vertIdx = 0; vertIdx < referenceModelVertices.size(); vertIdx++) {
			auto referenceModelVertex = referenceModelVertices[vertIdx];

			Vertex copyVertex = referenceModelVertex; // hopefully a copy by value and not reference?

			auto originalPointId = copyVertex.PointId;
			auto correspondingPosition = kpForEachSphere[panoIdx][originalPointId];

			if (vertIdx == 0) {
				std::cout << "panoIdx = " << panoIdx << ", example vert position: " << correspondingPosition.x << " " << correspondingPosition.y << " " << correspondingPosition.z << std::endl;
			}

			glm::vec2 uv = spherePointToUV(correspondingPosition);

			copyVertex.Position = correspondingPosition;
			copyVertex.Normal = correspondingPosition;

			hullVertices.push_back(copyVertex);
		}

		hullModel->meshes.push_back(Mesh(hullVertices, commonIndices));

		hullModels.push_back(hullModel);
	}



	// generate the interpolated hull model

	float alpha = barycentric_coords[0];
	float beta = barycentric_coords[1];
	float gamma = barycentric_coords[2];

	interpolatedHullModel = std::make_shared<Model>();

	std::vector<Vertex> interpolatedHullVertices;
	for (auto referenceModelVertex : referenceModelVertices) {
		Vertex copyVertex = referenceModelVertex; // hopefully a copy by value and not reference?

		auto originalPointId = copyVertex.PointId;
		auto posA = kpForEachSphere[0][originalPointId];
		auto posB = kpForEachSphere[1][originalPointId];
		auto posC = kpForEachSphere[2][originalPointId];

		glm::vec3 resultPos = glm::normalize(alpha*posA + beta*posB + gamma*posC);
		glm::vec2 resultUV = spherePointToUV(resultPos);

		copyVertex.Position = resultPos;
		copyVertex.Normal = resultPos;
		copyVertex.TexCoords0 = resultUV;
		copyVertex.TexCoords1 = resultUV;
		copyVertex.TexCoords2 = resultUV;

		interpolatedHullVertices.push_back(copyVertex);
	}
	interpolatedHullModel->meshes.push_back(Mesh(interpolatedHullVertices, commonIndices));
}

std::vector<int> manualClickedPanoIndices;

void AddManualCorrespondence(int panoIdx, int fullImgX, int fullImgY) {
	std::cout << "AddManualCorrespondence " << panoIdx << " " << fullImgX << " " << fullImgY << std::endl;

	int sideIdx = fullImgX / cubeSidePixels;	// int division

	int sideImgX = fullImgX % cubeSidePixels;
	int sideImgY = fullImgY % cubeSidePixels;

	std::cout << "sideIdx: " << sideIdx << std::endl;

	std::cout << "cubeSidePixels: " << cubeSidePixels << std::endl;

	glm::vec3 kpPosition = getKeypointOnSphereFromCubemapKeypoint(sideImgX, sideImgY, cubeSidePixels, sideIdx);

	// apply the rotation matrix from the sphere pose

	auto kpPositionRotated = corrected_sphere_rotation_matrices[panoIdx] * glm::vec4(kpPosition, 0);

	std::cout << "kpPositionRotated: " << kpPositionRotated.x << " " << kpPositionRotated.y << " " << kpPositionRotated.z << std::endl;

	manualKeypointPositionsForEachSphere[panoIdx].push_back(kpPositionRotated);
	manualClickedPanoIndices.push_back(panoIdx);
}


void ManualCorrespondenceCallback_A(int event, int x, int y, int flags, void* userdata) {
	if (event == CV_EVENT_LBUTTONUP) {
		std::cout << "ManualCorrespondenceCallback_A left up " << x << " " << y << std::endl;
		AddManualCorrespondence(0, x, y);
	}
}

void ManualCorrespondenceCallback_B(int event, int x, int y, int flags, void* userdata) {
	if (event == CV_EVENT_LBUTTONUP) {
		std::cout << "ManualCorrespondenceCallback_B left up " << x << " " << y << std::endl;
		AddManualCorrespondence(1, x, y);
	}
}

void ManualCorrespondenceCallback_C(int event, int x, int y, int flags, void* userdata) {
	if (event == CV_EVENT_LBUTTONUP) {
		std::cout << "ManualCorrespondenceCallback_C left up " << x << " " << y << std::endl;
		AddManualCorrespondence(2, x, y);
	}
}

void UndoLastManualCorrespondence() {
	if (manualClickedPanoIndices.size() > 0) {
		int lastClickedPanoIdx = manualClickedPanoIndices.back();

		manualKeypointPositionsForEachSphere[lastClickedPanoIdx].pop_back();
		manualClickedPanoIndices.pop_back();
	}
}

void ResetManualCorrespondences() {
	manualClickedPanoIndices.clear();
	manualKeypointPositionsForEachSphere.clear();
	for (int panoIdx = 0; panoIdx < 3; panoIdx++) {
		manualKeypointPositionsForEachSphere.push_back(std::vector<glm::vec3>());
	}
}

void initShader() {

	sphericalCameraPath = std::make_shared<Path>(sphericalCameraPoses, gridSizeMeters);

	ResetManualCorrespondences();

	manualCorrespondenceMouseCallbacks.push_back(ManualCorrespondenceCallback_A);
	manualCorrespondenceMouseCallbacks.push_back(ManualCorrespondenceCallback_B);
	manualCorrespondenceMouseCallbacks.push_back(ManualCorrespondenceCallback_C);

	string scanLabel = currentScan.Label;

	cubemapProjectionMat = glm::perspective(glm::radians(90.0f), 1.0f, 0.001f, 100.0f);	// 90 degree FOV, square image, all imagery is at most 1 unit away from the origin
	
	openglCubemapViewMats.push_back(glm::lookAt(glm::vec3(0,0,0), glm::vec3(1,0,0), glm::vec3(0,1,0))); // pos x = right
	openglCubemapViewMats.push_back(glm::lookAt(glm::vec3(0, 0, 0), glm::vec3(-1, 0, 0), glm::vec3(0, 1, 0))); // neg x = left
	openglCubemapViewMats.push_back(glm::lookAt(glm::vec3(0, 0, 0), glm::vec3(0, -1, 0), glm::vec3(0, 0, -1))); // pos y = up
	openglCubemapViewMats.push_back(glm::lookAt(glm::vec3(0, 0, 0), glm::vec3(0, 1, 0), glm::vec3(0, 0, 1))); // neg y = dowm
	openglCubemapViewMats.push_back(glm::lookAt(glm::vec3(0, 0, 0), glm::vec3(0, 0, -1), glm::vec3(0, 1, 0))); // pos z = back
	openglCubemapViewMats.push_back(glm::lookAt(glm::vec3(0, 0, 0), glm::vec3(0, 0, 1), glm::vec3(0, 1, 0))); // neg z = forward



	importedCubemapViewMats.push_back(glm::lookAt(glm::vec3(0, 0, 0), glm::vec3(0, 0, 1), glm::vec3(0, 1, 0))); // forward
	importedCubemapViewMats.push_back(glm::lookAt(glm::vec3(0, 0, 0), glm::vec3(1, 0, 0), glm::vec3(0, 1, 0))); // right
	importedCubemapViewMats.push_back(glm::lookAt(glm::vec3(0, 0, 0), glm::vec3(0, 0, -1), glm::vec3(0, 1, 0))); // back
	importedCubemapViewMats.push_back(glm::lookAt(glm::vec3(0, 0, 0), glm::vec3(-1, 0, 0), glm::vec3(0, 1, 0))); // left
	importedCubemapViewMats.push_back(glm::lookAt(glm::vec3(0, 0, 0), glm::vec3(0, -1, 0), glm::vec3(0, 0, -1))); // up
	importedCubemapViewMats.push_back(glm::lookAt(glm::vec3(0, 0, 0), glm::vec3(0, 1, 0), glm::vec3(0, 0, 1))); // dowm

	Point2D query_point(0, 0);
	Locate_type lt;
	int li;
	auto selectedFace = sphericalCameraPath->cellCenterTriangulation.locate(query_point, lt, li);

	size_t sphereA_poseIndex = selectedFace->vertex(0)->info();
	size_t sphereB_poseIndex = selectedFace->vertex(1)->info();
	size_t sphereC_poseIndex = selectedFace->vertex(2)->info();

	auto sphereA_pose = sphericalCameraPoses[sphereA_poseIndex];
	auto sphereB_pose = sphericalCameraPoses[sphereB_poseIndex];
	auto sphereC_pose = sphericalCameraPoses[sphereC_poseIndex];

	{
		

		float testSphereRotationX = 8.0f;
		float testSphereRotationY = 0.0f;
		float testSphereRotationZ = -1.0f;

		//float testSphereRotationX = 0.0f;
		//float testSphereRotationY = 0.0f;
		//float testSphereRotationZ = 0.0f;

		// rotation of the camera/hololens, representing ZXY euler angles in degrees 
		// (so z degrees about z axis, then x degrees about x axis, then y degrees about y axis, in that order)
		float testZRad = glm::radians(testSphereRotationZ);
		float testXRad = glm::radians(testSphereRotationX);
		float testYRad = glm::radians(testSphereRotationY);

		testFrontToSphereCameraRotation = glm::rotate(testFrontToSphereCameraRotation, testZRad, glm::vec3(0.0f, 0.0f, 1.0f));
		testFrontToSphereCameraRotation = glm::rotate(testFrontToSphereCameraRotation, testXRad, glm::vec3(1.0f, 0.0f, 0.0f));
		testFrontToSphereCameraRotation = glm::rotate(testFrontToSphereCameraRotation, testYRad, glm::vec3(0.0f, 1.0f, 0.0f));

		corrected_sphere_rotation_matrices.push_back(sphereA_pose.RotationMatrix * testFrontToSphereCameraRotation);
		corrected_sphere_rotation_matrices.push_back(sphereB_pose.RotationMatrix * testFrontToSphereCameraRotation);
		corrected_sphere_rotation_matrices.push_back(sphereC_pose.RotationMatrix * testFrontToSphereCameraRotation);

		corrected_inverse_sphere_rotation_matrices.push_back(glm::inverse(corrected_sphere_rotation_matrices[0]));
		corrected_inverse_sphere_rotation_matrices.push_back(glm::inverse(corrected_sphere_rotation_matrices[1]));
		corrected_inverse_sphere_rotation_matrices.push_back(glm::inverse(corrected_sphere_rotation_matrices[2]));
	}
	
	sphereCameraFrames[0] = getSphericalCameraFrameIndexFromPose(sphereA_pose);
	sphereCameraFrames[1] = getSphericalCameraFrameIndexFromPose(sphereB_pose);
	sphereCameraFrames[2] = getSphericalCameraFrameIndexFromPose(sphereC_pose);

	for (int i = 0; i < 3; i++) {
		sphereTextures.push_back(std::make_shared<GLTexture>());
		sphereTextures[i]->load(getPanoFramePath(sphereCameraFrames[i]));
	}

	std::cout << "generating cubemaps" << std::endl;

	
	
	fullCubemapImgs.clear();
	for (int panoIdx = 0; panoIdx < 3; panoIdx++) {
		int sphereCameraFrame = sphereCameraFrames[panoIdx];
		cv::Mat allFacesImg = loadOrCreateCubemap(sphereCameraFrame);
		cubeSidePixels = allFacesImg.rows;
		fullCubemapImgs.push_back(allFacesImg);
	}

	
	
	autoKeypointPositionsForEachSphere = GenerateKeypointPositionsForEachSphere();
	GenerateHullsFromKeypointPositionsForEachSphere(autoKeypointPositionsForEachSphere);




	// display cubemap images -- will be clickable for manual correspondences

	for (int panoIdx = 0; panoIdx < 3; panoIdx++) {

		std::string cubemap_winname = "sphere_" + std::to_string(panoIdx) + "_manual_correspondences";
		cv::namedWindow(cubemap_winname, cv::WINDOW_NORMAL);
		//cv::moveWindow(cubemap_winname, 20, 20);
		cv::imshow(cubemap_winname, fullCubemapImgs[panoIdx]);

		cv::setMouseCallback(cubemap_winname, manualCorrespondenceMouseCallbacks[panoIdx], NULL);
	}

	








	

	// we need GL_NEAREST because we don't want blending of the data that we are encoding in the cubemap
	interpolatedHullCubemap = std::make_shared<Cubemap>(cubemapRenderTextureSize, cubemapImageFormat, GL_NEAREST);

	// the equirectangular texture has the same resolution as the pano-sphere because each pixel will correspond to particular vertex on the sphere
	equirectangularFramebuffer = std::make_shared<Framebuffer>(panoSphereNumSegments, panoSphereNumRings, equirectangularImageFormat, GL_TEXTURE_2D, GL_NEAREST);
	equirectangularPixels = new GLfloat[equirectangularFramebuffer->getWidth() * equirectangularFramebuffer->getHeight() * equirectangularImageFormat.NumChannels];
	cubemapFacePixels = new GLfloat[interpolatedHullCubemap->GetCubemapSize() * interpolatedHullCubemap->GetCubemapSize() * cubemapImageFormat.NumChannels];


	

	meshShader = std::make_shared<Shader>("mesh.vert", "mesh.frag");
	encodeBarycentricShader = std::make_shared<Shader>("encode_barycentric.vert", "encode_barycentric.frag");
	pathShader = std::make_shared<Shader>("pathline.vert", "pathline.frag");
	spherePanoShader = std::make_shared<Shader>("spherepano.vert", "spherepano.frag");
	equirectangularShader = std::make_shared<Shader>("equirectangular.vert", "equirectangular.frag");

	cubeModel = std::make_shared<Model>(FileSystem::getPath("resources/meshes/cube.obj").c_str());
	quadModel = std::make_shared<Model>(FileSystem::getPath("resources/meshes/quad.obj").c_str());
	arrowModel = std::make_shared<Model>(FileSystem::getPath("resources/meshes/arrow.obj").c_str());


	fullscreenQuadModel = Model::CreateFullscreenQuad();

	panoSphereModel = Model::CreatePanoramaSphere(1.0f, panoSphereNumSegments, panoSphereNumRings);

	interpolatedPanoSphereModel = Model::CreatePanoramaSphere(1.0f, panoSphereNumSegments, panoSphereNumRings);

	GL_CHECK(std::cout << "done initing" << std::endl);
}

void renderSpherePano(glm::mat4 &visualizationCameraView, glm::mat4 &visualizationCameraProjection) {
	spherePanoShader->use();

	glUniformMatrix4fv(glGetUniformLocation(spherePanoShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(visualizationCameraProjection));
	glUniformMatrix4fv(glGetUniformLocation(spherePanoShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(visualizationCameraView));

	glm::mat4 visualizationSphereModel;
	glUniformMatrix4fv(glGetUniformLocation(spherePanoShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(visualizationSphereModel));

	glUniform1i(glGetUniformLocation(spherePanoShader->Program, "insideCapturedArea"), 1);

	float baryCoordsWithBlendOptions[3];
	float maxBary = std::max(std::max(barycentric_coords[0], barycentric_coords[1]), barycentric_coords[2]);

	switch (blendOptionsComboBox->selectedIndex())
	{
	case 0:	// blend
		baryCoordsWithBlendOptions[0] = barycentric_coords[0];
		baryCoordsWithBlendOptions[1] = barycentric_coords[1];
		baryCoordsWithBlendOptions[2] = barycentric_coords[2];
		break;
	case 1:	// nn

		baryCoordsWithBlendOptions[0] = barycentric_coords[0] >= maxBary ? 1.0f : 0.0f;
		baryCoordsWithBlendOptions[1] = barycentric_coords[1] >= maxBary ? 1.0f : 0.0f;
		baryCoordsWithBlendOptions[2] = barycentric_coords[2] >= maxBary ? 1.0f : 0.0f;

		{
			int numSelected = 0;
			for (int i = 0; i < 3; i++) {
				if (baryCoordsWithBlendOptions[i] > 0.0f) {
					numSelected++;
				}
			}
			if (numSelected > 1) {
				baryCoordsWithBlendOptions[0] = 1.0f;
				baryCoordsWithBlendOptions[1] = 0.0f;
				baryCoordsWithBlendOptions[2] = 0.0f;
			}
		}
		

		break;
	case 2:	// A

		baryCoordsWithBlendOptions[0] = 1.0f;
		baryCoordsWithBlendOptions[1] = 0.0f;
		baryCoordsWithBlendOptions[2] = 0.0f;

		break;
	case 3:	// B

		baryCoordsWithBlendOptions[0] = 0.0f;
		baryCoordsWithBlendOptions[1] = 1.0f;
		baryCoordsWithBlendOptions[2] = 0.0f;

		break;
	case 4:	// C

		baryCoordsWithBlendOptions[0] = 0.0f;
		baryCoordsWithBlendOptions[1] = 0.0f;
		baryCoordsWithBlendOptions[2] = 1.0f;

		break;
	default:
		break;
	}


	for (int i = 0; i < 3; i++) {

		int textureUnit = i;
		glActiveTexture(GL_TEXTURE0 + textureUnit);
		glBindTexture(GL_TEXTURE_2D, sphereTextures[i]->texture());

		std::string texUniformLoc = "surroundingSphereTex[" + std::to_string(i) + "]";
		glUniform1i(glGetUniformLocation(spherePanoShader->Program, texUniformLoc.c_str()), textureUnit);

		std::string barycentricCoordLoc = "barycentricCoord[" + std::to_string(i) + "]";
		glUniform1f(glGetUniformLocation(spherePanoShader->Program, barycentricCoordLoc.c_str()), baryCoordsWithBlendOptions[i]);
	}

	panoSphereModel->Draw(spherePanoShader);
}

void drawPano() {
	glm::mat4 visualizationCameraProjection = glm::perspective(glm::radians(panoramaFOV), (float)screenWidth / (float)screenHeight, 0.1f, 100.0f);

	glm::mat4 visualizationCameraView = visualizationCamera.GetViewMatrix();

	renderSpherePano(visualizationCameraView, visualizationCameraProjection);
}


void renderInterpolatedHullModel(glm::mat4 modelMat, glm::mat4 viewMat, glm::mat4 projectionMat) {

	encodeBarycentricShader->use();

	std::shared_ptr<Model> interpolatedModel = interpolatedHullModel;	// TODO: change the model being drawn

	glUniformMatrix4fv(glGetUniformLocation(encodeBarycentricShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(modelMat));
	glUniformMatrix4fv(glGetUniformLocation(encodeBarycentricShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(projectionMat));
	glUniformMatrix4fv(glGetUniformLocation(encodeBarycentricShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(viewMat));

	interpolatedModel->Draw(encodeBarycentricShader);
}

void drawHullVisualization() {

	glm::mat4 hullVisualizationCameraProjection = glm::perspective(hullVisualizationCamera.Zoom, (float)screenWidth / (float)screenHeight, 0.1f, 100.0f);
	glm::mat4 hullVisualizationCameraView = hullVisualizationCamera.GetViewMatrix();

	meshShader->use();
	
	if (renderingWireframe) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}

	glUniform3f(glGetUniformLocation(meshShader->Program, "lightPos"), hullVisualizationCamera.Position.x, hullVisualizationCamera.Position.y, hullVisualizationCamera.Position.z);

	glUniform3f(glGetUniformLocation(meshShader->Program, "lightColor"), 1.0f, 1.0f, 1.0f);

	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(hullVisualizationCameraProjection));
	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(hullVisualizationCameraView));

	float distanceScale = 5.0f;

	float interpolatedPosX = 0.0f;
	float interpolatedPosY = 0.0f;
	float interpolatedPosZ = 0.0f;

	for (int i = 0; i < 3; i++) {
		std::shared_ptr<Model> hullModel = hullModels[i];

		glm::mat4 hullModelMat;

		float posX = (float)triPointsUI[i].x / baryTriImageSize * distanceScale;
		float posY = 0.0f * distanceScale;
		float posZ = (float)triPointsUI[i].y / baryTriImageSize * distanceScale;

		interpolatedPosX += (posX * barycentric_coords[i]);
		interpolatedPosY += (posY * barycentric_coords[i]);
		interpolatedPosZ += (posZ * barycentric_coords[i]);

		glUniform1i(glGetUniformLocation(meshShader->Program, "texEnabled"), 1);

		hullModelMat = glm::translate(hullModelMat, glm::vec3(posX, posY, posZ));
		glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(hullModelMat));
		glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.5f, 0.5f, 1.0f);


		int textureUnit = i;
		glActiveTexture(GL_TEXTURE0 + textureUnit);
		glBindTexture(GL_TEXTURE_2D, sphereTextures[i]->texture());

		glUniform1i(glGetUniformLocation(meshShader->Program, "tex"), textureUnit);


		hullModel->Draw(meshShader);
	}

	// draw the interpolated mesh

	glm::mat4 interpolatedModelMat;
	interpolatedModelMat = glm::translate(interpolatedModelMat, glm::vec3(interpolatedPosX, interpolatedPosY, interpolatedPosZ));
	renderInterpolatedHullModel(interpolatedModelMat, hullVisualizationCameraView, hullVisualizationCameraProjection);

	


	

	if (renderingWireframe) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	
}

void renderInterpolatedHullSphericalTexture(bool preview) {
	// first, render the interpolated hull model from the inside, as a cubemap
	
	
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ZERO);
	glBlendEquation(GL_FUNC_ADD);
	

	glClearDepth(0.0f);
	glDepthFunc(GL_GREATER);	// Here we are specifically prioritizing fragments that are FURTHER from the camera.
								// This is so that if we have bad topology that occludes a bunch, it isn't as bad.

	interpolatedHullCubemap->preRender();

	glm::mat4 &cubeProjMat = cubemapProjectionMat;
	glm::mat4 cubeModelMat; // identity

	for (int faceIdx = 0; faceIdx < 6; faceIdx++) {
		interpolatedHullCubemap->PreFace(faceIdx);

		glm::mat4 &cubeViewMat = openglCubemapViewMats[faceIdx];

		//renderSpherePano(cubeViewMat, cubeProjMat);

		renderInterpolatedHullModel(cubeModelMat, cubeViewMat, cubeProjMat);

		interpolatedHullCubemap->PostFace(faceIdx);
	}

	interpolatedHullCubemap->postRender();

	glClearDepth(1.0f);
	glDepthFunc(GL_LESS);		// Restore the default depth function

								// =================

								// now that the cubemap is rendered, convert it to an equirectangular projection
	if (!preview) {
		equirectangularFramebuffer->preRender();
	}
	
	equirectangularShader->use();

	int textureUnit = 0;
	glActiveTexture(GL_TEXTURE0 + textureUnit);
	glBindTexture(GL_TEXTURE_CUBE_MAP, interpolatedHullCubemap->textureCubemap());

	glUniform1i(glGetUniformLocation(spherePanoShader->Program, "cubemap"), textureUnit);

	fullscreenQuadModel->Draw(equirectangularShader);

	if (!preview) {
		equirectangularFramebuffer->postRender();
	}
}




void draw() {
	frameNumber++;

	std::vector<Vertex> &interpolatedHullVertices = interpolatedHullModel->meshes[0].vertices;

	std::vector<std::vector<glm::vec3>> kpPositionsForEachSphere = *currentKeypointPositionsForEachSphere;

	int numVertices = interpolatedHullVertices.size();





	// recalculate the mesh shape
	float alpha = barycentric_coords[0];
	float beta = barycentric_coords[1];
	float gamma = barycentric_coords[2];

	

	

	for (int i = 0; i < numVertices; i++) {
		auto & meshVertex = interpolatedHullVertices[i];

		auto originalPointId = meshVertex.PointId;

		glm::vec3 posA = kpPositionsForEachSphere[0][originalPointId];
		glm::vec3 posB = kpPositionsForEachSphere[1][originalPointId];
		glm::vec3 posC = kpPositionsForEachSphere[2][originalPointId];

		glm::vec3 resultPos = glm::normalize(alpha*posA + beta*posB + gamma*posC);
		glm::vec2 resultUV = spherePointToUV(resultPos);

		meshVertex.Position = resultPos;
		meshVertex.Normal = resultPos;
		meshVertex.TexCoords0 = resultUV;
		meshVertex.TexCoords1 = resultUV;
		meshVertex.TexCoords2 = resultUV;
	}
	interpolatedHullModel->meshes[0].rebufferVertices();

	//==================

	// now that there is a new mesh shape, figure out the UV coordinates for each sphere that should be applied to the interpolatedspheremodel

	if (previewingInterpolatedSphericalTexture) {
		renderInterpolatedHullSphericalTexture(true); // this is just an additional rendering to the screen to show what the spherical texture looks like.
	}

	
	renderInterpolatedHullSphericalTexture(false);	// this is the render that actually writes the intended encoded data to the framebuffer

	// after rendering the interpolated hull spherical texture to the framebuffer, do a glreadpixels to get the data
	glBindFramebuffer(GL_FRAMEBUFFER, equirectangularFramebuffer->framebufferId());

	int texWidth = equirectangularFramebuffer->getWidth();
	int texHeight = equirectangularFramebuffer->getHeight();

	glReadPixels(0, 0, texWidth, texHeight, equirectangularImageFormat.Format, equirectangularImageFormat.Type, equirectangularPixels);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	

	

	// for each of the verts in the pano-sphere, look up the corresponding pixel in the equirectangular pixels
	auto &panoSphereVertices = panoSphereModel->meshes[0].vertices;
	for (auto &panoSphereVertex : panoSphereVertices) {
		glm::vec2 panoSphereUV = spherePointToUV(panoSphereVertex.Position);
		panoSphereUV.x = glm::clamp(panoSphereUV.x, 0.0f, 1.0f);
		panoSphereUV.y = glm::clamp(panoSphereUV.y, 0.0f, 1.0f);

		if (morphingPano) {
			panoSphereUV.x = 1.0f - panoSphereUV.x;

			glm::ivec2 panoSpherePixel(glm::floor((texWidth - 1)*panoSphereUV.x), glm::floor((texHeight - 1)*panoSphereUV.y));
			panoSpherePixel.x = glm::clamp(panoSpherePixel.x, 0, texWidth - 1);
			panoSpherePixel.y = glm::clamp(panoSpherePixel.y, 0, texHeight - 1);

			size_t pixelIdx = ((texWidth * panoSpherePixel.y) + panoSpherePixel.x) * equirectangularImageFormat.NumChannels;

			auto r = equirectangularPixels[pixelIdx + 0];
			auto g = equirectangularPixels[pixelIdx + 1];
			auto b = equirectangularPixels[pixelIdx + 2];
			auto a = equirectangularPixels[pixelIdx + 3];

			unsigned int triangleId;
			float bary_alpha, bary_beta, bary_gamma;

			if (equirectangularImageFormat.Type == GL_FLOAT) {
				triangleId = (unsigned int) (255 * (a * 256 + b));

				bary_alpha = r;
				bary_beta = g;
				bary_gamma = 1.0f - bary_alpha - bary_beta;
			}
			else if (equirectangularImageFormat.Type == GL_UNSIGNED_BYTE) {
				triangleId = a * 256 + b;
				//if (a > 0) {
				//std::cout << "a: " << a << std::endl;
				//}

				bary_alpha = r / 255.0f;
				bary_beta = g / 255.0f;
				bary_gamma = 1.0f - bary_alpha - bary_beta;
			}
			

			for (int panoIdx = 0; panoIdx < 3; panoIdx++) {

				auto &hullModel = hullModels[panoIdx];
				vector<GLuint> &hullModelIndices = hullModel->meshes[0].indices;

				if (hullModelIndices.size() > 3 * triangleId + 2) {
					auto vertIdx0 = hullModelIndices[3 * triangleId + 0];
					auto vertIdx1 = hullModelIndices[3 * triangleId + 1];
					auto vertIdx2 = hullModelIndices[3 * triangleId + 2];

					auto &hullModelVertices = hullModel->meshes[0].vertices;

					auto &refVert0 = hullModelVertices[vertIdx0];
					auto &refVert1 = hullModelVertices[vertIdx1];
					auto &refVert2 = hullModelVertices[vertIdx2];

					glm::vec3 resultPos = glm::normalize(bary_alpha*refVert0.Position + bary_beta*refVert1.Position + bary_gamma*refVert2.Position);

					glm::vec3 resultPosUnrotated = corrected_inverse_sphere_rotation_matrices[panoIdx] * glm::vec4(resultPos, 0);

					//glm::vec2 resultUV = spherePointToUV(resultPos);
					glm::vec2 resultUV = spherePointToUV(resultPosUnrotated);

					switch (panoIdx)
					{
					case 0:
						panoSphereVertex.TexCoords0 = resultUV;
						break;
					case 1:
						panoSphereVertex.TexCoords1 = resultUV;
						break;
					case 2:
						panoSphereVertex.TexCoords2 = resultUV;
						break;
					default:
						break;
					}
				}
			}
		}
		else {
			panoSphereVertex.TexCoords0 = panoSphereUV;
			panoSphereVertex.TexCoords1 = panoSphereUV;
			panoSphereVertex.TexCoords2 = panoSphereUV;
		}
		
	}
	panoSphereModel->meshes[0].rebufferVertices();
	
	
	//==================

	
	if (viewingMeshes) {
		drawHullVisualization();
	}
	else {
		drawPano();
	}

	

}




// Moves/alters the camera positions based on user input
void doMovement()
{
	if (viewingMeshes) {
		Camera* camera = &hullVisualizationCamera;

		bool forceHorizontalMotion = false;
		//bool forceHorizontalMotion = false;

		// Camera controls
		if (keys[GLFW_KEY_W])
			camera->ProcessKeyboard(FORWARD, deltaTime, forceHorizontalMotion);
		if (keys[GLFW_KEY_S])
			camera->ProcessKeyboard(BACKWARD, deltaTime, forceHorizontalMotion);
		if (keys[GLFW_KEY_A])
			camera->ProcessKeyboard(LEFT, deltaTime, forceHorizontalMotion);
		if (keys[GLFW_KEY_D])
			camera->ProcessKeyboard(RIGHT, deltaTime, forceHorizontalMotion);
		if (keys[GLFW_KEY_Q])
			camera->ProcessKeyboard(DOWN, deltaTime, forceHorizontalMotion);
		if (keys[GLFW_KEY_E])
			camera->ProcessKeyboard(UP, deltaTime, forceHorizontalMotion);
	}
	else {
		return; // no translation of the panorama camera
	}
	
}


// https://stackoverflow.com/questions/12774207/fastest-way-to-check-if-a-file-exist-using-standard-c-c11-c
inline bool does_file_exist(const std::string& name) {
	struct stat buffer;
	return (stat(name.c_str(), &buffer) == 0);
}

int main(int /* argc */, char ** /* argv */) {

	initScanData();

    glfwInit();

    glfwSetTime(0);

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    glfwWindowHint(GLFW_SAMPLES, 0);
    glfwWindowHint(GLFW_RED_BITS, 8);
    glfwWindowHint(GLFW_GREEN_BITS, 8);
    glfwWindowHint(GLFW_BLUE_BITS, 8);
    glfwWindowHint(GLFW_ALPHA_BITS, 8);
    glfwWindowHint(GLFW_STENCIL_BITS, 8);
    glfwWindowHint(GLFW_DEPTH_BITS, 24);
    glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

    // Create a GLFWwindow object
    window = glfwCreateWindow(screenWidth, screenHeight, "example3", nullptr, nullptr);
    if (window == nullptr) {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

#if defined(NANOGUI_GLAD)
    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress))
        throw std::runtime_error("Could not initialize GLAD!");
    glGetError(); // pull and ignore unhandled errors like GL_INVALID_ENUM
#endif

	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

    glClearColor(0.2f, 0.25f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Create a nanogui screen and pass the glfw pointer to initialize
    screen = new Screen();
    screen->initialize(window, true);

    glfwGetFramebufferSize(window, &screenWidth, &screenHeight);
    glViewport(0, 0, screenWidth, screenHeight);
    glfwSwapInterval(0);
    glfwSwapBuffers(window);


    // Create nanogui gui
    bool enabled = true;
    FormHelper *gui = new FormHelper(screen);
	nanogui::ref<Window> nanoguiWindow = gui->addWindow(Eigen::Vector2i(10, 10), "Form helper example");

	/*
    gui->addGroup("Basic types");
    gui->addVariable("bool", bvar)->setTooltip("Test tooltip.");
    gui->addVariable("string", strval);

    gui->addVariable("int", ivar)->setSpinnable(true);
    gui->addVariable("float", fvar)->setTooltip("Test.");
    gui->addVariable("double", dvar)->setSpinnable(true);

    gui->addGroup("Complex types");
    gui->addVariable("Enumeration", enumval, enabled)->setItems({ "Item 1", "Item 2", "Item 3" });
    gui->addVariable("Color", colval);

    gui->addGroup("Other widgets");
    gui->addButton("A button", []() { std::cout << "Button pressed." << std::endl; })->setTooltip("Testing a much longer tooltip, that will wrap around to new lines multiple times.");;
	*/

	gui->addGroup("Synthetic viewpoint");

	gui->addVariable("panoramaFOV", panoramaFOV)->setSpinnable(true);

	gui->addVariable("viewingMeshes", viewingMeshes);

	gui->addVariable("renderingWireframe", renderingWireframe);

	gui->addVariable("morphingPano", morphingPano);

	

	gui->addVariable("previewingInterpolatedSphericalTexture", previewingInterpolatedSphericalTexture);

	Widget* panel = new Widget(nanoguiWindow);

	panel->setLayout(new BoxLayout(Orientation::Horizontal,
		Alignment::Middle, 0, 20));

	blendOptionsComboBox = std::make_shared<nanogui::ComboBox>(panel);
	blendOptionsComboBox->setItems(textureOptions);

	gui->addWidget("Barycentric Coords", panel);

    screen->setVisible(true);
    screen->performLayout();
    //nanoguiWindow->center();

	screen->setVisible(true);
	screen->performLayout();

	triImage = cv::Mat::zeros(baryTriImageSize, baryTriImageSize, CV_8UC3);
	cv::Scalar lineColor(255, 0, 0);
	int lineThickness = 5;
	cv::line(triImage, triPointsUI[0], triPointsUI[1], lineColor, lineThickness);
	cv::line(triImage, triPointsUI[1], triPointsUI[2], lineColor, lineThickness);
	cv::line(triImage, triPointsUI[2], triPointsUI[0], lineColor, lineThickness);

	cv::namedWindow("Barycentric Coords", cv::WINDOW_AUTOSIZE);
	cv::imshow("Barycentric Coords", triImage);

	cv::setMouseCallback("Barycentric Coords", BarycentricTriCallbackFunc, NULL);

    glfwSetCursorPosCallback(window,
            [](GLFWwindow *, double x, double y) {
            bool cursorPosConsumedByUI = screen->cursorPosCallbackEvent(x, y);
			if (!cursorPosConsumedByUI) {
				// use mouse movement for camera movement

				if (firstMouse)
				{
					lastX = x;
					lastY = y;
					firstMouse = false;
				}

				GLfloat xoffset = x - lastX;
				GLfloat yoffset = lastY - y;

				lastX = x;
				lastY = y;

				if (cameraMouseControlsActive) {
					if (viewingMeshes) {
						hullVisualizationCamera.ProcessMouseMovement(xoffset, yoffset);
					}
					else {
						visualizationCamera.ProcessMouseMovement(xoffset, yoffset);
					}
				}
			}
        }
    );

    glfwSetMouseButtonCallback(window,
        [](GLFWwindow *, int button, int action, int modifiers) {
            bool mouseButtonConsumedByUI = screen->mouseButtonCallbackEvent(button, action, modifiers);
			if (!mouseButtonConsumedByUI) {

				if (button == GLFW_MOUSE_BUTTON_RIGHT) {
					if (action == GLFW_PRESS) {
						cameraMouseControlsActive = true;
					}
					else if (action == GLFW_RELEASE) {
						cameraMouseControlsActive = false;
					}
				}
			}
        }
    );

    glfwSetKeyCallback(window,
        [](GLFWwindow *, int key, int scancode, int action, int mods) {
            bool keyConsumedByUI = screen->keyCallbackEvent(key, scancode, action, mods);
			if (!keyConsumedByUI) {
				// treat the key input as camera controls

				if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
					glfwSetWindowShouldClose(window, GL_TRUE);
				
				if (action == GLFW_PRESS) {
					keys[key] = true;
				}
				else if (action == GLFW_RELEASE) {
					keys[key] = false;


					if (key == GLFW_KEY_U) {
						std::cout << "undo last correspondence added" << std::endl;
						UndoLastManualCorrespondence();
					}
					else if (key == GLFW_KEY_L) {
						std::cout << "load manual correspondences from file" << std::endl;

						std::string savedCorrespondencesPath = "manual_correspondences.yml";

						if (does_file_exist(savedCorrespondencesPath)) {
							cv::FileStorage manualCorrespondencesFile(savedCorrespondencesPath, cv::FileStorage::READ);

							manualKeypointPositionsForEachSphere.clear();

							std::string labels[3] = { "a","b","c" };

							for (int panoIdx = 0; panoIdx < 3; panoIdx++) {

								std::vector<cv::Point3f> pts;

								manualCorrespondencesFile[labels[panoIdx]] >> pts;

								std::vector<glm::vec3> vecs;

								for (cv::Point3f p : pts) {
									vecs.push_back(glm::vec3(p.x, p.y, p.z));
								}

								manualKeypointPositionsForEachSphere.push_back(vecs);

							}

							manualCorrespondencesFile.release();
						}

					}
					else if (key == GLFW_KEY_Z) {
						std::cout << "save manual correspondences to file" << std::endl;

						std::string savedCorrespondencesPath = "manual_correspondences.yml";

						cv::FileStorage manualCorrespondencesFile(savedCorrespondencesPath, cv::FileStorage::WRITE);

						std::string labels[3] = { "a","b","c" };

						for (int panoIdx = 0; panoIdx < 3; panoIdx++) {

							std::vector<cv::Point3f> pts;

							for (glm::vec3 v : manualKeypointPositionsForEachSphere[panoIdx]) {
								pts.push_back(cv::Point3f(v.x, v.y, v.z));
							}

							manualCorrespondencesFile << labels[panoIdx] << pts;
						}

						manualCorrespondencesFile.release();

						std::cout << "saved to " << savedCorrespondencesPath << std::endl;

					}
					else if (key == GLFW_KEY_H) {
						std::cout << "TODO: apply manual correspondences to generate convex hull" << std::endl;

						auto kpForEachSphere = manualKeypointPositionsForEachSphere;

						if (kpForEachSphere[0].size() == kpForEachSphere[1].size() && kpForEachSphere[0].size() == kpForEachSphere[2].size()) {

							if (kpForEachSphere[0].size() > 4) {
								GenerateHullsFromKeypointPositionsForEachSphere(manualKeypointPositionsForEachSphere);
								currentKeypointPositionsForEachSphere = &manualKeypointPositionsForEachSphere;
							}
							else {
								std::cout << "error in convex hull generation, need more than 4 points" << std::endl;
							}
						}
						else {
							std::cout << "error in convex hull generation, not the same number of points for each sphere" << std::endl;
						}

						
					}
					else if (key == GLFW_KEY_C) {
						std::cout << "clearing all manual correspondences" << std::endl;
						ResetManualCorrespondences();
					}





				}
			}
        }
    );

    glfwSetCharCallback(window,
        [](GLFWwindow *, unsigned int codepoint) {
            screen->charCallbackEvent(codepoint);
        }
    );

    glfwSetDropCallback(window,
        [](GLFWwindow *, int count, const char **filenames) {
            screen->dropCallbackEvent(count, filenames);
        }
    );

    glfwSetScrollCallback(window,
        [](GLFWwindow *, double x, double y) {
            screen->scrollCallbackEvent(x, y);
       }
    );

    glfwSetFramebufferSizeCallback(window,
        [](GLFWwindow *, int width, int height) {
			screenWidth = width;
			screenHeight = height;
            screen->resizeCallbackEvent(screenWidth, screenHeight);

			glViewport(0, 0, screenWidth, screenHeight);
        }
    );

	initShader();

    // Game loop
    while (!glfwWindowShouldClose(window)) {
		// Set frame time
		GLfloat currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

        // Check if any events have been activated (key pressed, mouse moved etc.) and call corresponding response functions
        glfwPollEvents();
		doMovement();

		// note that we have to enable the depth test per-frame; it looks like nanogui disables it in order to draw the UI
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);


        glClearColor(0.2f, 0.25f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		draw();

        // Draw nanogui
        screen->drawContents();
        screen->drawWidgets();

        glfwSwapBuffers(window);

		auto key = cv::waitKey(1);
    }

    // Terminate GLFW, clearing any resources allocated by GLFW.
    glfwTerminate();

    return 0;
}
