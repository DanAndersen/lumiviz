#pragma once
#include <memory>
#include "path.hpp"
#include "glm_includes.hpp"
#include <opencv2/opencv.hpp>
#include "json.hpp"
using json = nlohmann::json;


extern float gridSizeMeters;

extern std::unordered_map<int, Pose> frameNumbersToSpherePoses;

extern glm::mat4 testFrontToSphereCameraRotation;

extern json scanData;

extern float nn_match_ratio;

extern int cubeSidePixels;

extern cv::Ptr<cv::DescriptorMatcher> matcher;

extern CameraParameters sphericalCamera;

json ReadScanData(const std::string &scandataPath);

void initScanData(std::string scanSessionLabel);