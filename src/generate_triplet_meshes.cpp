#include <iostream>
#include "globals.hpp"
#include <opencv2/opencv.hpp>
#include "filesystem.h"
#include "yaml-cpp/yaml.h"
#include "utils.hpp"
#include <unordered_set>
#include <unordered_map>
#include <regex>
#include <fstream>

#if defined(NANOGUI_GLAD)
#if defined(NANOGUI_SHARED) && !defined(GLAD_GLAPI_EXPORT)
#define GLAD_GLAPI_EXPORT
#endif

#include <glad/glad.h>
#else
#if defined(__APPLE__)
#define GLFW_INCLUDE_GLCOREARB
#else
#define GL_GLEXT_PROTOTYPES
#endif
#endif

#include <GLFW/glfw3.h>

#include <nanogui/nanogui.h>
using namespace nanogui;

#include <openvr.h>

#include "camera.hpp"
#include "model.hpp"
#include "gltexture.hpp"
#include "cubemap.hpp"
#include "framebuffer.hpp"

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <CGAL/Barycentric_coordinates_2/Triangle_coordinates_2.h>








//std::string SCAN_SESSION_LABEL = "20170927140341";	// lab
//std::string SCAN_SESSION_LABEL = "20171118004408";	// apartment
//std::string SCAN_SESSION_LABEL = "20180114180049";	// LWSN corner

//std::string SCAN_SESSION_LABEL = "20180212180805";	// user scan of lab: chris
//std::string SCAN_SESSION_LABEL = "20180213164009";	// user scan of lab: chengyuan
std::string SCAN_SESSION_LABEL = "20180214121658";	// user scan of lab: raine
//std::string SCAN_SESSION_LABEL = "20180214161721";	// user scan of lab: andres
//std::string SCAN_SESSION_LABEL = "20180215173737";	// user scan of lab: xiaowei
//std::string SCAN_SESSION_LABEL = "20180220151103";	// user scan of lab: evan
//std::string SCAN_SESSION_LABEL = "20180222122208";	// user scan of lab: gen
//std::string SCAN_SESSION_LABEL = "20180222133102";	// user scan of lab: edgar
//std::string SCAN_SESSION_LABEL = "20180222161203";	// user scan of lab: naveen
//std::string SCAN_SESSION_LABEL = "20180223111019";	// user scan of lab: juan-andres


//std::string SCAN_SESSION_LABEL = "20180222153716";	// voicu office 0.5
//std::string SCAN_SESSION_LABEL = "20180222153948";	// voicu office 0.25
//std::string SCAN_SESSION_LABEL = "20180222154249";	// voicu office 0.125














bool printingFPS = false;
bool renderThirdFromVisCamera = false;

typedef CGAL::Exact_predicates_inexact_constructions_kernel            Kernel;
typedef CGAL::Triangulation_vertex_base_with_info_2<unsigned int, Kernel> Vb;
typedef CGAL::Triangulation_data_structure_2<Vb>                       Tds;
typedef CGAL::Delaunay_triangulation_2<Kernel, Tds>                    Delaunay;
typedef Kernel::Point_2                                                Point2D;
typedef CGAL::Triangulation_2<Kernel, Tds>::Locate_type				   Locate_type;
typedef CGAL::Barycentric_coordinates::Triangle_coordinates_2<Kernel>  Triangle_coordinates;

//std::shared_ptr<Path> rawSpherePath;
std::shared_ptr<Path> refined1DPath;
std::unordered_map<int, int> fnsTo1DPoseIndices; // maps frame numbers to the pose indices *in refined1DPath*. These are not the same pose indices in the full raw path.
std::unordered_map<int, int> poseIndices1DToFns; // the reverse

std::vector<int> selected_1D_frame_numbers;

#pragma region triangulation lookup
Delaunay cellCenterTriangulation;
bool insideCapturedArea = false;
std::vector<float> bary;
#pragma endregion triangulation lookup

ImageFormat RGBA8(GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE, 4);
ImageFormat RGBA32(GL_RGBA32F, GL_RGBA, GL_FLOAT, 4);

#pragma region imported cubemap and triplet data
std::vector<std::vector<int>> triangulationFrameNumbers;	// 0-indexed
std::unordered_set<int> uniqueFrameNumbers;	// 0-indexed
std::unordered_map<int, glm::vec3> frameNumbersToCubemapCameraCenters;	// 0-indexed
std::unordered_map<int, glm::mat4> frameNumbersToCameraModelMatrices;	// 0-indexed
std::unordered_map<int, glm::mat4> frameNumbersToCameraRotationMatrices;	// 0-indexed
std::unordered_map<int, glm::mat4> frameNumbersToCameraInverseRotationMatrices;	// 0-indexed
std::unordered_map<int, std::shared_ptr<GLTexture>> _frameNumbersToPanoTextures;	// 0-indexed
std::unordered_map<string, std::shared_ptr<Model>> _setLabelsToHoloroomAndSparseHullModels;
std::unordered_map<string, std::shared_ptr<Model>> _setLabelsToSparseHullModels;

std::vector<int> currentFrameNumbers;
#pragma endregion imported cubemap and triplet data



bool VR_ENABLED = false;


#pragma region window controls
bool forceCameraOntoManifold = true;
bool showingTopDownMapInVR = false;
bool guiVisible = true;
bool visualizationCameraFollowsHMDOrientation = true;
bool drawPanoSphereWireframe = false;
bool gridTextureEnabled = false;
bool maskHeadTexture = true;
float maskHeadExtent = 0.75f;
bool showingCameraPoses = false;
float panoramaFOV = 60.0f;
bool controllingVisualizationCamera = false;
bool showingCurrentWorldHull = false;
bool projectCurrentWorldHull = false;
bool nearestNeighbor = false;
bool printingInfo = false;
bool drawRoom = true;
bool drawPath = true;
bool drawArrow = true;
bool drawWireframe = false;


bool drawHoloroomExtraPoints = false;

bool usingHoloroomAndSparseHulls = true; // if false, use the sparse ones instead

float thirdPersonNear = 0.1f;

bool renderingPathMorph = false;	// if true, render the panoramas based on the 1D path morph rather than the cell centers

// if true, the 1D morph will lock the rendering camera to the line segments of the 1D path. if false, the user can try to move off of the exact path.
// forcing onto the line will result in better image quality, but will make navigation more choppy when moving across path lines.
// free movement will allow gradual motion to another part of the path, but the morphing meshes lack topological consistency the further from the line the user travels.
bool forceCameraOntoPathLine = false;


bool drawAugmentedMapOverPanorama = false;




std::vector<std::string> visualizationModeOptions = { "Third Person", "Morph Pano", "VR View", "Top-Down Map" };
std::vector<std::string> hullVisualizationOptions = { "Wire Mesh", "Projective Texture Mapping", "Both" };
std::vector<std::string> snapCameraToSphereOptions = { "Free", "A", "B", "C" };
#pragma endregion window controls

#pragma region VR

std::shared_ptr<Framebuffer> topDownMapFramebuffer;


unsigned int m_uiCompanionWindowIndexSize;
GLuint m_unCompanionWindowVAO;
GLuint m_unCompanionWindowProgramID;

GLuint m_glCompanionWindowIDVertBuffer;
GLuint m_glCompanionWindowIDIndexBuffer;


struct PositionOnPath {
	int i;
	int j;		// j > i
	float t;	// between 0 and 1
};

PositionOnPath currentPositionOnPath;




static bool g_bPrintf = true;
bool m_bVblank = false;
bool m_bGlFinishHack = true;

struct FramebufferDesc
{
	GLuint m_nDepthBufferId;
	GLuint m_nRenderTextureId;
	GLuint m_nRenderFramebufferId;
	GLuint m_nResolveTextureId;
	GLuint m_nResolveFramebufferId;
};
FramebufferDesc leftEyeDesc;
FramebufferDesc rightEyeDesc;

bool vrInited;	// set in initVR()
vr::IVRSystem *m_pHMD = NULL;
vr::IVRRenderModels *m_pRenderModels = NULL;
std::string m_strDriver;
std::string m_strDisplay;

std::string m_strPoseClasses;                            // what classes we saw poses for this frame
char m_rDevClassChar[vr::k_unMaxTrackedDeviceCount];   // for each device, a character representing its class

vr::TrackedDevicePose_t m_rTrackedDevicePose[vr::k_unMaxTrackedDeviceCount];

int m_iValidPoseCount = 0;
int m_iValidPoseCount_Last = -1;

int m_iTrackedControllerCount = 0;
int m_iTrackedControllerCount_Last = -1;



bool baryGravityEnabled = true;
float baryGravityMin = 0.6f;
float baryGravityMax = 0.8f;

glm::mat4 leftEyeViewMat;
glm::mat4 rightEyeViewMat;


bool prepPanoForEachEye = false; // if true, we prepare the panoramas for each stereo eye. if false, we use a single prepared panorama and only adjust depth.

bool renderHoloRoomForMorph = false; // if true, we don't even try to render our triplet meshes and only render the rough HoloLens geometry.




//std::vector<float> rawCameraTimestamps; // for each original pose index, what was the corresponding timestamp (in HoloLens time, seconds since starting the app)


// the render target resolution for each eye
uint32_t m_nRenderWidth;
uint32_t m_nRenderHeight;

glm::mat4 m_rmat4DevicePose[vr::k_unMaxTrackedDeviceCount];
glm::mat4 m_mat4HMDPose;

glm::mat4 manual_forward_HMDPose;

glm::mat4 m_mat4eyePosLeft;
glm::mat4 m_mat4eyePosRight;
glm::mat4 m_mat4ProjectionLeft;
glm::mat4 m_mat4ProjectionRight;

float m_fNearClip = 0.01f;
float m_fFarClip = 100.0f;

#pragma endregion VR

#pragma region gui elements
std::shared_ptr<nanogui::ComboBox> visualizationModeComboBox;
std::shared_ptr<nanogui::ComboBox> hullVisualizationComboBox;
std::shared_ptr<nanogui::ComboBox> snapCameraToSphereComboBox;
Screen *screen = nullptr;
Camera thirdPersonCamera(glm::vec3(0.0f, 0.0f, 0.0f)); // camera for moving around the scene
Camera visualizationCamera(glm::vec3(0.0f, 0.0f, 0.0f)); // camera for viewing the scene from predetermined locations
int screenWidth = 800;
int screenHeight = 800;
GLFWwindow* window;
bool cameraMouseControlsActive = false;
GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;
bool keys[1024];
GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;
float initialHeadY = 0.0f;
#pragma endregion gui elements

#pragma region model variables
std::shared_ptr<Model> arrowModel;
std::shared_ptr<Model> cubeModel;
std::shared_ptr<Model> sphericalCameraFrustumModel;
std::shared_ptr<Model> fullscreenQuadModel;
std::shared_ptr<Model> panoSphereModel;
std::shared_ptr<Model> simpleSphereModel;
std::shared_ptr<Model> pathTriangulationModel;

std::shared_ptr<Model> testHoloRoom;

#pragma endregion model variables


glm::mat4 hmdViewMat;

// the location from which the morphing visualization is rendered. Not always the same as the visualization camera's position. 
// in the 1D morph, we allow the visualization camera to move freely in the 2D domain, but the render location is actually forced onto the nearest 1D line segment.
glm::vec3 renderPosition; 


#pragma region shader variables
std::shared_ptr<Shader> meshShader;
std::shared_ptr<Shader> panoProjectiveTextureMappingShader;
std::shared_ptr<Shader> pathShader;


#pragma endregion shader variables

PointCloud<float> cloud_selected_1D_positions;
std::shared_ptr<my_kd_tree_t> selected_1D_poses_kdTreeIndex;

struct VertexDataWindow
{
	glm::vec2 position;
	glm::vec2 texCoord;

	VertexDataWindow(const glm::vec2 & pos, const glm::vec2 tex) : position(pos), texCoord(tex) {	}
};

bool isRenderingStereo; // the current rendering job is for the stereo view

/*
int getSphericalCameraFrameIndexFromPoseIndex(int pose_index) {

	int locatableCameraSyncFrameIndex = scanData["hololens_sync_frame"] - 1;	// go from 1-indexed to 0-indexed
	float locatableCameraSyncFrameTimestamp = rawCameraTimestamps[locatableCameraSyncFrameIndex];

	int sphericalCameraSyncFrameIndex = scanData["spherical_sync_frame"] - 1;	// go from 1-indexed to 0-indexed
	double sphericalCameraFPS = scanData["spherical_fps"];

	float locatableCameraTimestamp = rawCameraTimestamps[pose_index];
	float secondsSinceCalibrationTime = (locatableCameraTimestamp - locatableCameraSyncFrameTimestamp);
	int sphericalCameraFrameIndex = (int)std::round(sphericalCameraSyncFrameIndex + (secondsSinceCalibrationTime * sphericalCameraFPS));
	return sphericalCameraFrameIndex;
}
*/

int numSourcePanos = 3; // 3 if doing 2D morphing. 2 if doing 1D morphing.

int baryTriImageSize = 300;
cv::Mat triImage;
cv::Point triPointsUI[3] = { cv::Point(0, baryTriImageSize), cv::Point(baryTriImageSize, baryTriImageSize), cv::Point(baryTriImageSize / 2, 0) };

std::shared_ptr<GLTexture> gridTexture;



bool triWindowDrag = false;
void BarycentricTriCallbackFunc(int event, int x, int y, int flags, void* userdata) {
	bool shouldUpdateCoords = false;

	if (event == CV_EVENT_LBUTTONDOWN && !triWindowDrag) {
		//std::cout << "left down " << x << " " << y << std::endl;
		triWindowDrag = true;
		shouldUpdateCoords = true;
	}

	if (event == CV_EVENT_MOUSEMOVE && triWindowDrag) {
		//std::cout << "left drag " << x << " " << y << std::endl;
		shouldUpdateCoords = true;
	}

	if (event == CV_EVENT_LBUTTONUP && triWindowDrag) {
		//std::cout << "left up " << x << " " << y << std::endl;
		triWindowDrag = false;
	}

	if (shouldUpdateCoords) {

		if (numSourcePanos == 3) {
			float x1 = (float)triPointsUI[0].x;
			float y1 = (float)triPointsUI[0].y;
			float x2 = (float)triPointsUI[1].x;
			float y2 = (float)triPointsUI[1].y;
			float x3 = (float)triPointsUI[2].x;
			float y3 = (float)triPointsUI[2].y;

			float detT = (y2 - y3) * (x1 - x3) + (x3 - x2) * (y1 - y3);
			float bary1 = ((y2 - y3)*(x - x3) + (x3 - x2)*(y - y3)) / detT;
			float bary2 = ((y3 - y1) * (x - x3) + (x1 - x3) * (y - y3)) / detT;
			float bary3 = 1.0f - bary1 - bary2;



			if ((bary1 >= 0.0f && bary1 <= 1.0f) && (bary2 >= 0.0f && bary2 <= 1.0f) && (bary3 >= 0.0f && bary3 <= 1.0f)) {

				std::vector<float> newBary;
				newBary.push_back(bary1);
				newBary.push_back(bary2);
				newBary.push_back(bary3);

				//std::cout << "barycentric_coords " << barycentric_coords[0] << " " << barycentric_coords[1] << " " << barycentric_coords[2] << std::endl;

				float snappedCameraX = 0.0f;
				float snappedCameraZ = 0.0f;

				for (int panoIdx = 0; panoIdx < numSourcePanos; panoIdx++) {
					glm::vec3 & camCenter = frameNumbersToCubemapCameraCenters[currentFrameNumbers[panoIdx]];

					snappedCameraX += newBary[panoIdx] * camCenter.x;
					snappedCameraZ += newBary[panoIdx] * camCenter.z;
				}

				visualizationCamera.Position.x = snappedCameraX;
				visualizationCamera.Position.z = snappedCameraZ;
			}
		}
		else {
			std::cout << "attempting to change bary coords when we don't have numSourcePanos = 3..." << std::endl;
		}
	}
}

inline std::string GetFrameNumberSetLabel(std::vector<int> frameNumbers)
{
	std::vector<int> sortedFrameNumbers(frameNumbers);
	std::sort(sortedFrameNumbers.begin(), sortedFrameNumbers.end());

	switch (sortedFrameNumbers.size())
	{
	case 3:
		return string_format("%d_%d_%d", sortedFrameNumbers[0], sortedFrameNumbers[1], sortedFrameNumbers[2]);
		break;
	case 2:
		return string_format("%d_%d", sortedFrameNumbers[0], sortedFrameNumbers[1]);
		break;
	default:
		std::cout << "invalid number of panos: " << sortedFrameNumbers.size() << std::endl;
		throw;
		break;
	}
}


std::shared_ptr<GLTexture> GetPanoTextureForFrameNumber(int frameNumber) {
	// 0-indexed frame number

	if (_frameNumbersToPanoTextures.find(frameNumber) == _frameNumbersToPanoTextures.end()) {
		//std::cout << "loading texture for frame " << frameNumber << std::endl;
		std::shared_ptr<GLTexture> panoTexture = std::make_shared<GLTexture>();
		auto loadedPixels = panoTexture->load(getPanoFramePath(SCAN_SESSION_LABEL, frameNumber));
		loadedPixels.reset();

		_frameNumbersToPanoTextures.emplace(frameNumber, panoTexture);
	}

	return _frameNumbersToPanoTextures[frameNumber];
}


// https://stackoverflow.com/questions/12774207/fastest-way-to-check-if-a-file-exist-using-standard-c-c11-c
inline bool does_file_exist(const std::string& name) {
	struct stat buffer;
	return (stat(name.c_str(), &buffer) == 0);
}

void LoadMeshesForPanoSourceSet(std::vector<int> frameNumbers) {

	std::unordered_map<std::string, shared_ptr<Model>> & currentModelSet = usingHoloroomAndSparseHulls ? _setLabelsToHoloroomAndSparseHullModels : _setLabelsToSparseHullModels;

	std::string panoSourceSetLabel = GetFrameNumberSetLabel(frameNumbers);

	std::cout << "loading meshes for pano source set " << panoSourceSetLabel << std::endl;
	
	std::string inMeshDataPath;

	if (frameNumbers.size() == 3) {
		
		
		std::string holoAndSparsetripletMeshDataPath = FileSystem::getPath(string_format("resources/scans/%s/triplet_meshes/holoroom_and_sparse/consistent_topology/triplet_%d_%d_%d.meshes", SCAN_SESSION_LABEL.c_str(),
			frameNumbers[0], frameNumbers[1], frameNumbers[2]));
		
		std::string sparseTripletMeshDataPath = FileSystem::getPath(string_format("resources/scans/%s/triplet_meshes/sparse/triplet_%d_%d_%d.meshes", SCAN_SESSION_LABEL.c_str(),
			frameNumbers[0], frameNumbers[1], frameNumbers[2]));
		
		if (usingHoloroomAndSparseHulls) {
			inMeshDataPath = holoAndSparsetripletMeshDataPath;
		}
		else {
			inMeshDataPath = sparseTripletMeshDataPath;
		}
	}
	else if (frameNumbers.size() == 2) {
		
		std::string holoAndSparsePairMeshDataPath = FileSystem::getPath(string_format("resources/scans/%s/path_meshes/holoroom_and_sparse/consistent_topology/pair_%d_%d.meshes", SCAN_SESSION_LABEL.c_str(),
			frameNumbers[0], frameNumbers[1]));

		std::string sparsePairMeshDataPath = FileSystem::getPath(string_format("resources/scans/%s/path_meshes/holoroom_and_sparse/consistent_topology/pair_%d_%d.meshes", SCAN_SESSION_LABEL.c_str(),
			frameNumbers[0], frameNumbers[1]));

		if (usingHoloroomAndSparseHulls) {
			inMeshDataPath = holoAndSparsePairMeshDataPath;
		}
		else {
			inMeshDataPath = sparsePairMeshDataPath;
		}
	}
	
	
	if (!does_file_exist(inMeshDataPath)) {
		std::cout << "error: can't find mesh data at " << inMeshDataPath << std::endl;
		throw;
	}
	
	
		
	
	std::ifstream tripletMesh_ifs;

	tripletMesh_ifs.open(inMeshDataPath, std::ifstream::in | std::ifstream::binary);

	int num_verts;

	tripletMesh_ifs >> num_verts;

	std::vector<glm::vec3> world_points;
	std::vector<Vertex> unprojectedWorldVerts;
	for (int i = 0; i < num_verts; i++) {
		float vert_x, vert_y, vert_z;
		tripletMesh_ifs >> vert_x >> vert_y >> vert_z;	// reading in the unprojected world verts

		glm::vec3 p(vert_x, vert_y, vert_z);
		world_points.push_back(p);
		Vertex v(p, p);
		unprojectedWorldVerts.push_back(v);
	}

	int num_triangles;

	tripletMesh_ifs >> num_triangles;

	std::vector<GLuint> hullIndices;					// the indices are common to all the meshes for this triplet
	for (int i = 0; i < num_triangles; i++) {
		GLuint idx0, idx1, idx2;
		tripletMesh_ifs >> idx0 >> idx1 >> idx2;
		hullIndices.push_back(idx0);
		hullIndices.push_back(idx1);
		hullIndices.push_back(idx2);
	}

	std::shared_ptr<Model> unprojectedWorldHullModel = std::make_shared<Model>();
	unprojectedWorldHullModel->meshes.push_back(Mesh(unprojectedWorldVerts, hullIndices));

	// next, augment the unprojected hull model so it includes barycentric data, etc.

	glm::vec3 centroid(0, 0, 0);
	for (auto & frameNumber : frameNumbers) {
		auto cam_center = frameNumbersToCubemapCameraCenters[frameNumber];

		centroid += cam_center;
	}
	centroid /= (float)frameNumbers.size();

	// this model represents the constant world-space coordinates
	std::shared_ptr<Model> augmentedWorldHullModel = Model::CreateAugmentedHullModelFromUnprojectedHullModel(unprojectedWorldHullModel, centroid);

	std::string label = GetFrameNumberSetLabel(frameNumbers);	// stay in 0-indexed here

	currentModelSet.emplace(label, augmentedWorldHullModel);

	tripletMesh_ifs.close();
}





std::shared_ptr<Model> GetUnprojectedWorldHullModelForSet(std::vector<int> frameNumbers) {
	std::string tripletLabel = GetFrameNumberSetLabel(frameNumbers);
	
	std::unordered_map<std::string, shared_ptr<Model>> & currentModelSet = usingHoloroomAndSparseHulls ? _setLabelsToHoloroomAndSparseHullModels : _setLabelsToSparseHullModels;
	
	if (currentModelSet.find(tripletLabel) == currentModelSet.end()) {
		LoadMeshesForPanoSourceSet(frameNumbers);
	}

	return currentModelSet[tripletLabel];
}

#pragma region drawing functions


void DrawMeshWithProjectiveTextureMapping(std::shared_ptr<Model> modelToRender, glm::mat4 & modelMat, glm::mat4 & viewMat, glm::mat4 & projMat) {
	auto currentlyActiveShader = panoProjectiveTextureMappingShader;
	currentlyActiveShader->use();
	
	glUniformMatrix4fv(glGetUniformLocation(currentlyActiveShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(projMat));
	glUniformMatrix4fv(glGetUniformLocation(currentlyActiveShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(viewMat));
	glUniformMatrix4fv(glGetUniformLocation(currentlyActiveShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(modelMat));

	glUniform1i(glGetUniformLocation(currentlyActiveShader->Program, "numSourcePanos"), numSourcePanos);

	glUniform1i(glGetUniformLocation(currentlyActiveShader->Program, "drawWireframe"), drawWireframe ? 1 : 0);

	glUniform1i(glGetUniformLocation(currentlyActiveShader->Program, "projectCurrentWorldHull"), projectCurrentWorldHull ? 1 : 0);

	
	if (projectCurrentWorldHull && currentFrameNumbers.size() > 0) {
		glm::vec3 centroid(0, 0, 0);
		for (auto & frameNumber : currentFrameNumbers) {
			auto cam_center = frameNumbersToCubemapCameraCenters[frameNumber];

			centroid += cam_center;
		}
		centroid /= (float)currentFrameNumbers.size();

		glUniform3f(glGetUniformLocation(currentlyActiveShader->Program, "tripletCentroid"), centroid.x, centroid.y, centroid.z);
	}
	



	for (int i = 0; i < numSourcePanos; i++) {

		int frameNumber = currentFrameNumbers[i];

		glm::mat4 & sphereModelMat = frameNumbersToCameraModelMatrices[frameNumber];
		std::shared_ptr<GLTexture> sphereTexture = GetPanoTextureForFrameNumber(frameNumber);

		std::string modelMatLoc = "surroundingSphereModelMatrices[" + std::to_string(i) + "]";

		glUniformMatrix4fv(glGetUniformLocation(currentlyActiveShader->Program, modelMatLoc.c_str()), 1, GL_FALSE, glm::value_ptr(sphereModelMat));


		int textureUnit = i;
		glActiveTexture(GL_TEXTURE0 + textureUnit);
		glBindTexture(GL_TEXTURE_2D, sphereTexture->texture());

		std::string texUniformLoc = "surroundingSphereTex[" + std::to_string(i) + "]";

		glUniform1i(glGetUniformLocation(currentlyActiveShader->Program, texUniformLoc.c_str()), textureUnit);


		std::string barycentricCoordLoc = "barycentricCoord[" + std::to_string(i) + "]";

		if (printingInfo) {
			std::cout << "barycentric_coords " << bary[0] << " " << bary[1] << " " << bary[2] << std::endl;
		}

		glUniform1f(glGetUniformLocation(currentlyActiveShader->Program, barycentricCoordLoc.c_str()), bary[i]);

		glUniform1i(glGetUniformLocation(currentlyActiveShader->Program, "nearestNeighbor"), nearestNeighbor ? 1 : 0);
	}


	modelToRender->Draw(currentlyActiveShader);
}

void DrawRoom(glm::mat4 & cameraProjection, glm::mat4 & cameraView) {
	meshShader->use();

	//==================

	// draw the room
	glDisable(GL_CULL_FACE);
	//glEnable(GL_CULL_FACE);
	//glCullFace(GL_FRONT);

	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(cameraProjection));
	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(cameraView));

	glm::mat4 modelIdentity;
	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(modelIdentity));
	
	DrawMeshWithProjectiveTextureMapping(testHoloRoom, modelIdentity, cameraView, cameraProjection);

	//testHoloRoom->Draw(meshShader);

	glDisable(GL_CULL_FACE);
	glCullFace(GL_BACK);
}



void DrawThirdPersonVisualization() {
	//==================
	// set up the third person camera
	glm::mat4 modelIdentity;
	glm::mat4 thirdPersonCameraProjection = glm::perspective(thirdPersonCamera.Zoom, (float)screenWidth / (float)screenHeight, thirdPersonNear, 100.0f);
	glm::mat4 thirdPersonCameraView = thirdPersonCamera.GetViewMatrix();
	if (renderThirdFromVisCamera) {
		thirdPersonCameraView = visualizationCamera.GetViewMatrix();
	}

	//==================
	// initialize common uniforms
	meshShader->use();

	glUniform3f(glGetUniformLocation(meshShader->Program, "lightPos"), thirdPersonCamera.Position.x, thirdPersonCamera.Position.y, thirdPersonCamera.Position.z);

	glUniform3f(glGetUniformLocation(meshShader->Program, "lightColor"), 1.0f, 1.0f, 1.0f);
	glUniform1i(glGetUniformLocation(meshShader->Program, "lightingEnabled"), true);
	glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.75f, 0.75f, 0.75f);

	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(thirdPersonCameraProjection));
	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(thirdPersonCameraView));

	//==================

	// draw where the visualization camera is
	if (drawArrow) {
		if (!renderThirdFromVisCamera) {
			glm::mat4 visualizationCameraModelMatrix = glm::inverse(visualizationCamera.GetViewMatrix());

			glm::mat4 visualizationCameraScaledMatrix = glm::scale(visualizationCameraModelMatrix, glm::vec3(0.5, 0.5, 0.5));

			glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(visualizationCameraScaledMatrix));
			glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 1.0f, 0.5f);
			arrowModel->Draw(meshShader);
		}
	}
	
	

	//==================

	// draw the raw imported cameras
	if (showingCameraPoses) {
		glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 1.0f, 0.5f);

		for (auto & entry : frameNumbersToCameraModelMatrices) {
			glm::mat4 & raw_cam_model = entry.second;

			glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(raw_cam_model));
			sphericalCameraFrustumModel->Draw(meshShader);
		}
	}
	
	//==================
	
	if (drawRoom) {
		DrawRoom(thirdPersonCameraProjection, thirdPersonCameraView);
	}

	//==================

	if (showingCurrentWorldHull) {

		glm::mat4 identityModel;

		auto currentWorldHull = GetUnprojectedWorldHullModelForSet(currentFrameNumbers);

		bool drawWireMesh = false;
		bool drawProjTexMapMesh = false;


		switch (hullVisualizationComboBox->selectedIndex()) {
		case 0:	// wire-mesh
			
			drawWireMesh = true;

			break;
		case 1:	// projective texture mapping

			drawProjTexMapMesh = true;

			//glm::mat4 modelMat = glm::translate(identityModel, renderPosition);
			//DrawMeshWithProjectiveTextureMapping(cubeModel, modelMat, thirdPersonCameraView, thirdPersonCameraProjection);
			break;
		case 2: // wire and projective texture mapping
			drawWireMesh = true;
			drawProjTexMapMesh = true;
		}

		if (drawWireMesh) {
			//glDisable(GL_DEPTH_TEST);
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

			// draw the world mesh for the current triplet
			glUniform1i(glGetUniformLocation(meshShader->Program, "lightingEnabled"), false);
			glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(identityModel));
			glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 1.0f, 1.0f);
			currentWorldHull->Draw(meshShader);

			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			//glEnable(GL_DEPTH_TEST);
		}

		if (drawProjTexMapMesh) {
			DrawMeshWithProjectiveTextureMapping(currentWorldHull, identityModel, thirdPersonCameraView, thirdPersonCameraProjection);
		}
	}

	glUniform1i(glGetUniformLocation(meshShader->Program, "lightingEnabled"), true);

	
	if (m_pHMD != NULL) {
		// show the current pose of the HMD and the eyes

		glm::vec3 leftEyeWorldPosition = glm::column(glm::inverse(leftEyeViewMat), 3);
		glm::vec3 rightEyeWorldPosition = glm::column(glm::inverse(rightEyeViewMat), 3);

		meshShader->use();

		// draw left eye
		glm::mat4 leftEyeModel = glm::inverse(leftEyeViewMat);
		leftEyeModel = glm::scale(leftEyeModel, glm::vec3(0.1f, 0.1f, 1.0f));
		glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(leftEyeModel));
		glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 0.0f, 0.0f);
		arrowModel->Draw(meshShader);

		// draw right eye
		glm::mat4 rightEyeModel = glm::inverse(rightEyeViewMat);
		rightEyeModel = glm::scale(rightEyeModel, glm::vec3(0.1f, 0.1f, 1.0f));
		glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(rightEyeModel));
		glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.0f, 1.0f, 0.0f);
		arrowModel->Draw(meshShader);

		// draw left eye cube
		glm::mat4 leftEyeCubeModel;
		leftEyeCubeModel = glm::translate(leftEyeCubeModel, leftEyeWorldPosition);
		leftEyeCubeModel = glm::scale(leftEyeCubeModel, glm::vec3(0.1f, 0.1f, 0.1f));
		glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(leftEyeCubeModel));
		glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 1.0f, 1.0f);
		cubeModel->Draw(meshShader);

		// draw right eye cube
		glm::mat4 rightEyeCubeModel;
		rightEyeCubeModel = glm::translate(rightEyeCubeModel, rightEyeWorldPosition);
		rightEyeCubeModel = glm::scale(rightEyeCubeModel, glm::vec3(0.1f, 0.1f, 0.1f));
		glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(rightEyeCubeModel));
		glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 1.0f, 1.0f);
		cubeModel->Draw(meshShader);

	}
	
	// draw the path

	if (drawPath) {
		pathShader->use();

		glUniformMatrix4fv(glGetUniformLocation(pathShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(thirdPersonCameraProjection));
		glUniformMatrix4fv(glGetUniformLocation(pathShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(thirdPersonCameraView));
		glUniformMatrix4fv(glGetUniformLocation(pathShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(modelIdentity));

		glUniform3f(glGetUniformLocation(pathShader->Program, "lineColor"), 1.0f, 1.0f, 1.0f);

		refined1DPath->Draw(pathShader, 0, refined1DPath->poses.size() - 1);
	}
	
	
	if (drawHoloroomExtraPoints) {
		// draw a sphere located at each point where there is a vertex that is in the holo+sparse hull but not in the sparse-only hull

		// save the original flag value
		bool usingHoloroomAndSparseHulls_original = usingHoloroomAndSparseHulls;

		// set to sparse only, get that model
		usingHoloroomAndSparseHulls = false;
		auto & sparseOnlyHullVertexVector = GetUnprojectedWorldHullModelForSet(currentFrameNumbers)->meshes[0].vertices;
		usingHoloroomAndSparseHulls = true;
		auto & holoAndSparseHullVertexVector = GetUnprojectedWorldHullModelForSet(currentFrameNumbers)->meshes[0].vertices;

		// restore the original flag value
		usingHoloroomAndSparseHulls = usingHoloroomAndSparseHulls_original;


		// now use the requested models to find the difference set
		std::unordered_set<glm::vec3> sparseOnlyVertexSet;
		for (auto & v : sparseOnlyHullVertexVector) {
			
			sparseOnlyVertexSet.insert(v.Position);
		}
		
		std::unordered_set<glm::vec3> holoAndSparseVertexSet;
		for (auto & v : holoAndSparseHullVertexVector) {
			
			holoAndSparseVertexSet.insert(v.Position);
		}
		
		std::unordered_set<glm::vec3> onlyNewVertexSet;

		for (auto & holoAndSparseVert : holoAndSparseVertexSet) {
			float min_dist_to_closest_sparse_vert = std::numeric_limits<double>::infinity();

			for (auto & sparseVert : sparseOnlyVertexSet) {
				float dist = glm::distance(holoAndSparseVert, sparseVert);
				min_dist_to_closest_sparse_vert = std::min(min_dist_to_closest_sparse_vert, dist);
			}

			if (min_dist_to_closest_sparse_vert > 0.1f) {
				onlyNewVertexSet.insert(holoAndSparseVert);
			}
		}

		//std::cout << "num verts in sparseOnlyVertexSet: " << sparseOnlyVertexSet.size() << std::endl;
		//std::cout << "num verts in holoAndSparseVertexSet: " << holoAndSparseVertexSet.size() << std::endl;
		//std::cout << "num verts in onlyNewVertexSet: " << onlyNewVertexSet.size() << std::endl;

		meshShader->use();
		glDisable(GL_DEPTH_TEST);
		glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 0.0f, 0.0f);
		glUniform1i(glGetUniformLocation(meshShader->Program, "lightingEnabled"), false);
		for (auto & p : onlyNewVertexSet) {
			glm::mat4 newVertModelMat;
			newVertModelMat = glm::translate(newVertModelMat, p);
			newVertModelMat = glm::scale(newVertModelMat, glm::vec3(0.1f, 0.1f, 0.1f));

			glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(newVertModelMat));

			simpleSphereModel->Draw(meshShader);
		}
		glEnable(GL_DEPTH_TEST);

		

		
		
		
		

	}

}


void DrawMapVisualization(glm::mat4 viewMat, glm::mat4 projectionMat) {

	glm::mat4 modelIdentity;

	// initialize common uniforms
	meshShader->use();

	glUniform1i(glGetUniformLocation(meshShader->Program, "lightingEnabled"), false);

	glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.75f, 0.75f, 0.75f);

	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(projectionMat));
	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(viewMat));

	//==================

	if (isRenderingStereo) {
		// render left eye then right eye

		glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 1.0f, 0.5f);

		glm::mat4 leftEyeScaledModelMatrix = glm::scale(glm::inverse(leftEyeViewMat), glm::vec3(0.05, 0.05, 0.05));;
		glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(leftEyeScaledModelMatrix));
		
		simpleSphereModel->Draw(meshShader);

		glm::mat4 rightEyeScaledModelMatrix = glm::scale(glm::inverse(rightEyeViewMat), glm::vec3(0.05, 0.05, 0.05));;
		glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(rightEyeScaledModelMatrix));
	}

	// draw the current render location

	glm::mat4 renderPositionModelMatrix;
	renderPositionModelMatrix = glm::translate(renderPositionModelMatrix, renderPosition);

	glm::mat4 renderPositionScaledMatrix = glm::scale(renderPositionModelMatrix, glm::vec3(0.1, 0.1, 0.1));

	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(renderPositionScaledMatrix));
	glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 1.0f, 0.5f);
	simpleSphereModel->Draw(meshShader);
	


	if (!renderingPathMorph) {
		// draw the 2D triangulation poses
		
		for (auto & frameNumber : uniqueFrameNumbers) {
			

			bool isCurrentFrameNumber = false;
			for (int i = 0; i < numSourcePanos; i++) {
				if (currentFrameNumbers[i] == frameNumber) {
					isCurrentFrameNumber = true;
					break;
				}
			}

			if (isCurrentFrameNumber) {
				glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 1.0f, 1.0f);
			}
			else {
				glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.5f, 0.5f, 0.5f);
			}

			glm::mat4 & raw_cam_model = frameNumbersToCameraModelMatrices[frameNumber];
			glm::mat4 scaledModel = glm::scale(raw_cam_model, glm::vec3(0.0625, 0.0625, 0.0625));

			glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(scaledModel));
			simpleSphereModel->Draw(meshShader);
		}

		// =========

		
	}

	//==================

	

	// ==========

	if (renderingPathMorph) {

		pathShader->use();

		glUniformMatrix4fv(glGetUniformLocation(pathShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(projectionMat));
		glUniformMatrix4fv(glGetUniformLocation(pathShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(viewMat));
		glUniformMatrix4fv(glGetUniformLocation(pathShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(modelIdentity));

		glUniform3f(glGetUniformLocation(pathShader->Program, "lineColor"), 1.0f, 1.0f, 1.0f);

		refined1DPath->Draw(pathShader, 0, refined1DPath->poses.size() - 1);


		// draw the selected raw poses for the 1D poses

		meshShader->use();
		for (auto & fn : selected_1D_frame_numbers) {

			int pidx_1d = fnsTo1DPoseIndices[fn];

			if (pidx_1d == currentPositionOnPath.i) {
				glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.0f, 1.0f, 0.0f);
			}
			else if (pidx_1d == currentPositionOnPath.j) {
				glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.0f, 1.0f, 0.0f);
			}
			else {
				glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.0f, 0.5f, 0.0f);
			}

			Pose & pose = refined1DPath->poses[pidx_1d];

			glm::mat4 scaledModel = glm::scale(pose.ModelMatrix, glm::vec3(0.0625, 0.0625, 0.0625));

			glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(scaledModel));
			simpleSphereModel->Draw(meshShader);
		}

		
		{
			// place a small marker at the point between the two poses based on the 't' value
			glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 0.0f, 0.0f);

			glm::mat4 markerModel;
			glm::vec3 a = refined1DPath->poses[currentPositionOnPath.i].Position;
			glm::vec3 b = refined1DPath->poses[currentPositionOnPath.j].Position;
			markerModel = glm::translate(markerModel, a + (b - a)*currentPositionOnPath.t);
			markerModel = glm::scale(markerModel, glm::vec3(0.05f, 1.0f, 0.05f));

			glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(markerModel));
			simpleSphereModel->Draw(meshShader);
		}
		

		/*
		{
			// place marker based on "render position"
			glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.5f, 0.0f, 0.0f);

			glm::mat4 markerModel;
			markerModel = glm::translate(markerModel, renderPosition);
			markerModel = glm::scale(markerModel, glm::vec3(0.075f, 1.0f, 0.075f));

			glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(markerModel));
			simpleSphereModel->Draw(meshShader);
		}
		*/

	}
}



void drawPanoSphere(std::shared_ptr<Model> modelToDraw, glm::mat4 modelMat, glm::mat4 viewMat, glm::mat4 projectionMat) {
	
	if (numSourcePanos == 0) {
		return;
	}

	auto currentlyActiveShader = panoProjectiveTextureMappingShader;

	currentlyActiveShader->use();

	glUniform1i(glGetUniformLocation(currentlyActiveShader->Program, "numSourcePanos"), numSourcePanos);

	glUniformMatrix4fv(glGetUniformLocation(currentlyActiveShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(projectionMat));
	glUniformMatrix4fv(glGetUniformLocation(currentlyActiveShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(viewMat));
	glUniformMatrix4fv(glGetUniformLocation(currentlyActiveShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(modelMat));

	glUniform1i(glGetUniformLocation(currentlyActiveShader->Program, "nearestNeighbor"), nearestNeighbor ? 1 : 0);

	glUniform1f(glGetUniformLocation(currentlyActiveShader->Program, "maskHeadExtent"), maskHeadExtent);

	glUniform1i(glGetUniformLocation(currentlyActiveShader->Program, "maskHeadTexture"), maskHeadTexture ? 1 : 0);

	glUniform1i(glGetUniformLocation(currentlyActiveShader->Program, "drawWireframe"), drawWireframe ? 1 : 0);

	std::vector<float> renderBaryCoordinates;
	std::vector<std::shared_ptr<GLTexture>> sphereTextures;
	for (int i = 0; i < numSourcePanos; i++) {
		int frameNumber = currentFrameNumbers[i];
		std::shared_ptr<GLTexture> sphereTexture = GetPanoTextureForFrameNumber(frameNumber);
		sphereTextures.push_back(sphereTexture);

		renderBaryCoordinates.push_back(bary[i]);
	}

	if (baryGravityEnabled) {
		// don't use the raw barycentric coordinates for rendering. instead, use baryGravity min/max to adjust the values so we can adjust the transition

		int maxBaryIndex = 0;
		float maxBaryValue = 0.0f;

		for (int i = 0; i < numSourcePanos; i++) {
			if (bary[i] > maxBaryValue) {
				maxBaryIndex = i;
				maxBaryValue = bary[i];
			}
		}

		// now we know which of the bary coordinates is the max

		// these are which bary coordinates are not the max
		std::vector<int> nonmaxBaryIndexValues;
		for (int i = 1; i < numSourcePanos; i++) {
			int nonmaxBaryIndex = (maxBaryIndex + i) % numSourcePanos;
			nonmaxBaryIndexValues.push_back(nonmaxBaryIndex);
		}

		if (maxBaryValue >= baryGravityMax) {
			renderBaryCoordinates[maxBaryIndex] = 1.0f;
			for (int nonmaxBaryIndex : nonmaxBaryIndexValues) {
				renderBaryCoordinates[nonmaxBaryIndex] = 0.0f;
			}
		}
		else if (maxBaryValue >= baryGravityMin) {
			float f = (maxBaryValue - baryGravityMin) / (baryGravityMax - baryGravityMin);

			renderBaryCoordinates[maxBaryIndex] = (bary[maxBaryIndex] + (1.0f - bary[maxBaryIndex]) * f);
			for (int nonmaxBaryIndex : nonmaxBaryIndexValues) {
				renderBaryCoordinates[nonmaxBaryIndex] = (bary[nonmaxBaryIndex] * (1.0f - f));
			}
		}
		else {
			renderBaryCoordinates[maxBaryIndex] = bary[maxBaryIndex];
			for (int nonmaxBaryIndex : nonmaxBaryIndexValues) {
				renderBaryCoordinates[nonmaxBaryIndex] = bary[nonmaxBaryIndex];
			}
		}
	}

	if (insideCapturedArea) {
		for (int i = 0; i < numSourcePanos; i++) {
			int textureUnit = i;
			glActiveTexture(GL_TEXTURE0 + textureUnit);

			GLuint tex;
			if (gridTextureEnabled) {
				tex = gridTexture->texture();
			}
			else {
				tex = sphereTextures[i]->texture();
			}
			glBindTexture(GL_TEXTURE_2D, tex);

			std::string texUniformLoc = "surroundingSphereTex[" + std::to_string(i) + "]";

			glUniform1i(glGetUniformLocation(currentlyActiveShader->Program, texUniformLoc.c_str()), textureUnit);


			std::string barycentricCoordLoc = "barycentricCoord[" + std::to_string(i) + "]";

			if (printingInfo) {
				std::cout << "barycentric_coords " << bary[0] << " " << bary[1] << " " << bary[2] << std::endl;
			}

			glUniform1f(glGetUniformLocation(currentlyActiveShader->Program, barycentricCoordLoc.c_str()), renderBaryCoordinates[i]);
		}

		glUniform1i(glGetUniformLocation(currentlyActiveShader->Program, "nearestNeighbor"), nearestNeighbor ? 1 : 0);
	}


	for (int i = 0; i < numSourcePanos; i++) {
		int frameNumber = currentFrameNumbers[i];
		glm::mat4 & sphereModelMat = frameNumbersToCameraModelMatrices[frameNumber];

		std::string modelMatLoc = "surroundingSphereModelMatrices[" + std::to_string(i) + "]";

		glUniformMatrix4fv(glGetUniformLocation(currentlyActiveShader->Program, modelMatLoc.c_str()), 1, GL_FALSE, glm::value_ptr(sphereModelMat));
	}


	modelToDraw->Draw(currentlyActiveShader);

	if (drawPanoSphereWireframe) {
		currentlyActiveShader = meshShader;
		currentlyActiveShader->use();
		glUniformMatrix4fv(glGetUniformLocation(currentlyActiveShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(projectionMat));
		glUniformMatrix4fv(glGetUniformLocation(currentlyActiveShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(viewMat));
		glUniformMatrix4fv(glGetUniformLocation(currentlyActiveShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(modelMat));

		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		glUniform3f(glGetUniformLocation(currentlyActiveShader->Program, "objectColor"), 1.0f, 1.0f, 1.0f);
		modelToDraw->Draw(currentlyActiveShader);

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	

	// now, if drawAugmentedMapOverPanorama is enabled, draw the positions of the various nodes on top of the panorama image
	if (drawAugmentedMapOverPanorama) {
		glClear(GL_DEPTH_BUFFER_BIT); // clear the depth buffer

		glm::mat4 m;
		m = glm::translate(m, glm::vec3(0.0f, -2.0f, 0.0f));
		glm::mat4 augmentedMapViewMat = visualizationCamera.GetViewMatrix() * m;

		DrawMapVisualization(augmentedMapViewMat, projectionMat);
	}
}

void RenderCompanionWindow() {

	glDisable(GL_DEPTH_TEST);
	glViewport(0, 0, screenWidth, screenHeight);

	glBindVertexArray(m_unCompanionWindowVAO);
	glUseProgram(m_unCompanionWindowProgramID);

	// render left eye (first half of index array )
	glBindTexture(GL_TEXTURE_2D, leftEyeDesc.m_nResolveTextureId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glDrawElements(GL_TRIANGLES, m_uiCompanionWindowIndexSize / 2, GL_UNSIGNED_SHORT, 0);

	// render right eye (second half of index array )
	glBindTexture(GL_TEXTURE_2D, rightEyeDesc.m_nResolveTextureId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glDrawElements(GL_TRIANGLES, m_uiCompanionWindowIndexSize / 2, GL_UNSIGNED_SHORT, (const void *)(uintptr_t)(m_uiCompanionWindowIndexSize));

	glBindVertexArray(0);
	glUseProgram(0);

}








void DrawTopDownMap(bool vrView) {

	

	float cameraExtents = 1.0f;
	float zNear = -2.0f;
	float zFar = 2.0f;

	glm::mat4 topdownCameraProjection = glm::ortho(-cameraExtents, cameraExtents, -cameraExtents, cameraExtents, zNear, zFar);


	glm::vec3 cameraForwardDir;
	if (vrView) {
		cameraForwardDir = - glm::column(glm::inverse(hmdViewMat), 2);
	}
	else {
		cameraForwardDir = visualizationCamera.Front;
	}

	// project onto XZ plane
	cameraForwardDir.y = 0.0f;
	cameraForwardDir = glm::normalize(cameraForwardDir);

	glm::mat4 topdownCameraView = glm::lookAt(renderPosition + glm::vec3(0, 1, 0), renderPosition, cameraForwardDir);

	DrawMapVisualization(topdownCameraView, topdownCameraProjection);
}









void draw() {
	isRenderingStereo = false;

	glViewport(0, 0, screenWidth, screenHeight);

	switch (visualizationModeComboBox->selectedIndex()) {
	case 0:	// third person
		DrawThirdPersonVisualization();
		break;
	case 1:	// morph pano
		{
		glm::mat4 visualizationCameraProjection = glm::perspective(glm::radians(panoramaFOV), (float)screenWidth / (float)screenHeight, 0.1f, 100.0f);
		glm::vec3 renderCameraPosition = renderPosition;
		glm::mat4 visualizationCameraView = visualizationCamera.GetViewMatrix(renderCameraPosition);
		glm::mat4 visualizationSphereModel;
		visualizationSphereModel = glm::translate(visualizationSphereModel, renderCameraPosition);

		std::shared_ptr<Model> modelToRender;
		if (renderHoloRoomForMorph) {
			modelToRender = testHoloRoom;
		}
		else {
			modelToRender = GetUnprojectedWorldHullModelForSet(currentFrameNumbers);
		}

		glm::mat4 modelIdentity;
		drawPanoSphere(modelToRender, modelIdentity, visualizationCameraView, visualizationCameraProjection);

		}
		break;
	case 2:	// VR companion view

		if (m_pHMD) {
			RenderCompanionWindow();
		}
		

		break;
	case 3: // top-down map

		DrawTopDownMap(false);

		break;
	default:
		std::cout << "unexpected value for visualizationModeComboBox" << std::endl;
		break;
	}

	
}
#pragma endregion drawing functions

void ImportAllData() {
	std::cout << "importing ALL data... please wait, this will take a long time" << std::endl;
	for (auto & frameNumber : uniqueFrameNumbers) {
		GetPanoTextureForFrameNumber(frameNumber);
	}

	for (auto & tripletFrameNumbers : triangulationFrameNumbers) {
		GetUnprojectedWorldHullModelForSet(tripletFrameNumbers);
	}
	std::cout << "full data import complete!" << std::endl;
}

#pragma region boilerplate
// Moves/alters the camera positions based on user input
void doMovement()
{
	Camera* camera = controllingVisualizationCamera ? &visualizationCamera : &thirdPersonCamera;

	bool forceHorizontalMotion = controllingVisualizationCamera;
	//bool forceHorizontalMotion = false;

	// Camera controls
	if (keys[GLFW_KEY_W])
		camera->ProcessKeyboard(FORWARD, deltaTime, forceHorizontalMotion);
	if (keys[GLFW_KEY_S])
		camera->ProcessKeyboard(BACKWARD, deltaTime, forceHorizontalMotion);
	if (keys[GLFW_KEY_A])
		camera->ProcessKeyboard(LEFT, deltaTime, forceHorizontalMotion);
	if (keys[GLFW_KEY_D])
		camera->ProcessKeyboard(RIGHT, deltaTime, forceHorizontalMotion);
	if (keys[GLFW_KEY_Q])
		camera->ProcessKeyboard(DOWN, deltaTime, forceHorizontalMotion);
	if (keys[GLFW_KEY_E])
		camera->ProcessKeyboard(UP, deltaTime, forceHorizontalMotion);

	if (controllingVisualizationCamera) {
		// make sure the visualization camera is always set at the default height
		//camera->Position.y = initialHeadY;
	}

	if (forceCameraOntoManifold) {
		// force the visualization camera to be stuck on the 2D manifold defined by the surrounding triplet
		if (insideCapturedArea) {
			// only do the motion if the camera is actually inside the captured region

			float interpolatedCameraHeight = 0.0f;
			for (int panoIdx = 0; panoIdx < numSourcePanos; panoIdx++) {
				glm::vec3 & camCenter = frameNumbersToCubemapCameraCenters[currentFrameNumbers[panoIdx]];

				interpolatedCameraHeight += bary[panoIdx] * camCenter.y;
			}
			visualizationCamera.Position.y = interpolatedCameraHeight;

		}
	}
	

	if (snapCameraToSphereComboBox->selectedIndex() == 0) {
		// free motion of camera in horizontal plane
	}
	else {
		// update the XZ coordinates of the visualization camera to sit on top of one of the sphere locations

		float epsilon = 0.0000001f;	// don't put it exactly on the sphere location, but very close to it

		std::vector<float> newBary;

		switch (snapCameraToSphereComboBox->selectedIndex()) {
		case 1:	// A

			newBary.push_back(1.0f - epsilon);
			newBary.push_back(epsilon / 2.0f);
			newBary.push_back(epsilon / 2.0f);

			break;
		case 2:	// B

			newBary.push_back(epsilon / 2.0f);
			newBary.push_back(1.0f - epsilon);
			newBary.push_back(epsilon / 2.0f);

			break;
		case 3:	// C

			newBary.push_back(epsilon / 2.0f);
			newBary.push_back(epsilon / 2.0f);
			newBary.push_back(1.0f - epsilon);

			break;
		default:
			std::cout << "ERROR: unexpected value for snapCameraToSphereComboBox: " << snapCameraToSphereComboBox->selectedIndex() << std::endl;
			break;
		}

		float snappedCameraX = 0.0f;
		float snappedCameraZ = 0.0f;

		for (int panoIdx = 0; panoIdx < numSourcePanos; panoIdx++) {
			glm::vec3 & camCenter = frameNumbersToCubemapCameraCenters[currentFrameNumbers[panoIdx]];

			snappedCameraX += newBary[panoIdx] * camCenter.x;
			snappedCameraZ += newBary[panoIdx] * camCenter.z;
		}

		visualizationCamera.Position.x = snappedCameraX;
		visualizationCamera.Position.z = snappedCameraZ;

	}

	
}

glm::mat4 GetHMDMatrixProjectionEye(vr::Hmd_Eye nEye)
{
	if (!m_pHMD)
		return glm::mat4();

	vr::HmdMatrix44_t mat = m_pHMD->GetProjectionMatrix(nEye, m_fNearClip, m_fFarClip);

	return glm::mat4(
		mat.m[0][0], mat.m[1][0], mat.m[2][0], mat.m[3][0],
		mat.m[0][1], mat.m[1][1], mat.m[2][1], mat.m[3][1],
		mat.m[0][2], mat.m[1][2], mat.m[2][2], mat.m[3][2],
		mat.m[0][3], mat.m[1][3], mat.m[2][3], mat.m[3][3]
	);
}

glm::mat4 GetHMDMatrixPoseEye(vr::Hmd_Eye nEye)
{
	if (!m_pHMD)
		return glm::mat4();

	vr::HmdMatrix34_t matEyeRight = m_pHMD->GetEyeToHeadTransform(nEye);
	glm::mat4 matrixObj(
		matEyeRight.m[0][0], matEyeRight.m[1][0], matEyeRight.m[2][0], 0.0,
		matEyeRight.m[0][1], matEyeRight.m[1][1], matEyeRight.m[2][1], 0.0,
		matEyeRight.m[0][2], matEyeRight.m[1][2], matEyeRight.m[2][2], 0.0,
		matEyeRight.m[0][3], matEyeRight.m[1][3], matEyeRight.m[2][3], 1.0f
	);

	return glm::inverse(matrixObj);
}

void resetHMDForward() {
	std::cout << "Resetting HMD forward center pose" << std::endl;
	manual_forward_HMDPose = glm::inverse(m_mat4HMDPose);

	// force the manual_forward_HMDPose to have Y pointing straight up... only rotation should be about the Y axis (yaw)

	// the goal is to turn

	//     | r_xx r_xy r_xz t_x |
	//     | r_yx r_yy r_yz t_y |
	// M = | r_zx r_zy r_zz t_z |
	//     | 0    0    0    1   |

	// into 

	//     | c(t)  0    s(t) t_x |
	//     | 0     1    0    t_y |
	// M = | -s(t) 0    c(t) t_z |
	//     | 0     0    0    1   |

	auto M = manual_forward_HMDPose;

	double xLen = sqrt(M[0][0] * M[0][0] + M[0][2] * M[0][2]); // Singularity if either of these
	double zLen = sqrt(M[2][0] * M[2][0] + M[2][2] * M[2][2]); //  is equal to zero.

	M[0][0] /= xLen; M[0][1] = 0; M[0][2] /= xLen;  // Set the x column
	M[1][0] = 0;     M[1][1] = 1; M[1][2] = 0;      // set the y column
	M[2][0] /= zLen; M[2][1] = 0; M[2][2] /= zLen;  // set the z column
													//Don't change the translation column
	manual_forward_HMDPose = M;
}

int initWindow() {
	glfwInit();

	glfwSetTime(0);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glfwWindowHint(GLFW_SAMPLES, 0);
	glfwWindowHint(GLFW_RED_BITS, 8);
	glfwWindowHint(GLFW_GREEN_BITS, 8);
	glfwWindowHint(GLFW_BLUE_BITS, 8);
	glfwWindowHint(GLFW_ALPHA_BITS, 8);
	glfwWindowHint(GLFW_STENCIL_BITS, 8);
	glfwWindowHint(GLFW_DEPTH_BITS, 24);
	glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

	// Create a GLFWwindow object
	window = glfwCreateWindow(screenWidth, screenHeight, "Visualization", nullptr, nullptr);
	if (window == nullptr) {
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

#if defined(NANOGUI_GLAD)
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
		throw std::runtime_error("Could not initialize GLAD!");
	glGetError(); // pull and ignore unhandled errors like GL_INVALID_ENUM
#endif

	glClearColor(0.2f, 0.25f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Create a nanogui screen and pass the glfw pointer to initialize
	screen = new Screen();
	screen->initialize(window, true);

	glfwGetFramebufferSize(window, &screenWidth, &screenHeight);
	glViewport(0, 0, screenWidth, screenHeight);
	glfwSwapInterval(0);
	glfwSwapBuffers(window);

	// Create nanogui gui
	bool enabled = true;
	FormHelper *gui = new FormHelper(screen);
	nanogui::ref<Window> nanoguiWindow = gui->addWindow(Eigen::Vector2i(10, 10), "Form helper example");

	
	
	gui->addVariable("thirdPersonNear", thirdPersonNear)->setSpinnable(true);
	gui->addVariable("usingHoloroomAndSparseHulls", usingHoloroomAndSparseHulls);
	gui->addVariable("drawHoloroomExtraPoints", drawHoloroomExtraPoints);	
	gui->addVariable("drawWireframe", drawWireframe);
	gui->addVariable("drawPath", drawPath);
	gui->addVariable("drawRoom", drawRoom);
	gui->addVariable("drawArrow", drawArrow);
	gui->addVariable("drawAugmentedMapOverPanorama", drawAugmentedMapOverPanorama);
	gui->addVariable("renderingPathMorph", renderingPathMorph);
	gui->addVariable("forceCameraOntoPathLine", forceCameraOntoPathLine);
	gui->addVariable("renderThirdFromVisCamera", renderThirdFromVisCamera);
	gui->addVariable("printingFPS", printingFPS);
	gui->addVariable("showingCameraPoses", showingCameraPoses);
	gui->addVariable("controllingVisualizationCamera", controllingVisualizationCamera);
	gui->addVariable("showingCurrentWorldHull", showingCurrentWorldHull);
	gui->addVariable("projectCurrentWorldHull", projectCurrentWorldHull);
	gui->addVariable("nearestNeighbor", nearestNeighbor)->setTooltip("render panorama as nearest neighbor");
	gui->addVariable("printingInfo", printingInfo)->setTooltip("printingInfo");
	gui->addVariable("panoramaFOV", panoramaFOV)->setSpinnable(true);
	gui->addVariable("maskHeadTexture", maskHeadTexture);
	gui->addVariable("maskHeadExtent", maskHeadExtent)->setSpinnable(true);
	gui->addVariable("gridTextureEnabled", gridTextureEnabled);
	gui->addVariable("drawPanoSphereWireframe", drawPanoSphereWireframe);
	gui->addVariable("visualizationCameraFollowsHMDOrientation", visualizationCameraFollowsHMDOrientation);
	gui->addVariable("forceCameraOntoManifold", forceCameraOntoManifold);
	gui->addVariable("baryGravityEnabled", baryGravityEnabled);
	gui->addVariable("baryGravityMin", baryGravityMin);
	gui->addVariable("baryGravityMax", baryGravityMax);
	
	gui->addVariable("prepPanoForEachEye", prepPanoForEachEye);
	
	gui->addVariable("renderHoloRoomForMorph", renderHoloRoomForMorph);
	gui->addVariable("third person camera speed", thirdPersonCamera.MovementSpeed);
	gui->addVariable("viz camera speed", visualizationCamera.MovementSpeed);
	
	
	
	{
		Widget* panel = new Widget(nanoguiWindow);
		panel->setLayout(new BoxLayout(Orientation::Horizontal,
			Alignment::Middle, 0, 20));

		visualizationModeComboBox = std::make_shared<nanogui::ComboBox>(panel);
		visualizationModeComboBox->setItems(visualizationModeOptions);

		gui->addWidget("Visualization Mode", panel);
	}

	{
		Widget* panel = new Widget(nanoguiWindow);
		panel->setLayout(new BoxLayout(Orientation::Horizontal,
			Alignment::Middle, 0, 20));

		hullVisualizationComboBox = std::make_shared<nanogui::ComboBox>(panel);
		hullVisualizationComboBox->setItems(hullVisualizationOptions);

		gui->addWidget("Hull Rendering", panel);
	}

	{
		Widget* panel = new Widget(nanoguiWindow);
		panel->setLayout(new BoxLayout(Orientation::Horizontal,
			Alignment::Middle, 0, 20));

		snapCameraToSphereComboBox = std::make_shared<nanogui::ComboBox>(panel);
		snapCameraToSphereComboBox->setItems(snapCameraToSphereOptions);

		gui->addWidget("Snap camera to sphere", panel);
	}

	{
		Widget* panel = new Widget(nanoguiWindow);
		panel->setLayout(new BoxLayout(Orientation::Horizontal,
			Alignment::Middle, 0, 20));


		nanogui::Button *b = new nanogui::Button(panel, "Import ALL data");
		b->setCallback([] {
			ImportAllData();
		});

		gui->addWidget("Import", panel);


	}


	if (VR_ENABLED) {
		{
			Widget* panel = new Widget(nanoguiWindow);
			panel->setLayout(new BoxLayout(Orientation::Horizontal,
				Alignment::Middle, 0, 20));


			nanogui::Button *b = new nanogui::Button(panel, "Reset HMD forward pose");
			b->setCallback([] {
				resetHMDForward();
			});

			gui->addWidget("HMD Pose", panel);
		}
	}
	

	

	screen->setVisible(true);
	screen->performLayout();



	triImage = cv::Mat::zeros(baryTriImageSize, baryTriImageSize, CV_8UC3);
	cv::Scalar lineColor(255, 0, 0);
	int lineThickness = 5;
	cv::line(triImage, triPointsUI[0], triPointsUI[1], lineColor, lineThickness);
	cv::line(triImage, triPointsUI[1], triPointsUI[2], lineColor, lineThickness);
	cv::line(triImage, triPointsUI[2], triPointsUI[0], lineColor, lineThickness);

	cv::namedWindow("Barycentric Coords", cv::WINDOW_AUTOSIZE);
	cv::imshow("Barycentric Coords", triImage);

	cv::setMouseCallback("Barycentric Coords", BarycentricTriCallbackFunc, NULL);

	glfwSetCursorPosCallback(window,
		[](GLFWwindow *, double x, double y) {
		bool cursorPosConsumedByUI = screen->cursorPosCallbackEvent(x, y);
		if (!cursorPosConsumedByUI) {
			// use mouse movement for camera movement

			if (firstMouse)
			{
				lastX = x;
				lastY = y;
				firstMouse = false;
			}

			GLfloat xoffset = x - lastX;
			GLfloat yoffset = lastY - y;

			lastX = x;
			lastY = y;

			if (cameraMouseControlsActive) {
				if (controllingVisualizationCamera) {
					visualizationCamera.ProcessMouseMovement(xoffset, yoffset);
				}
				else {
					thirdPersonCamera.ProcessMouseMovement(xoffset, yoffset);
				}

			}
		}
	}
	);

	glfwSetMouseButtonCallback(window,
		[](GLFWwindow *, int button, int action, int modifiers) {
		bool mouseButtonConsumedByUI = screen->mouseButtonCallbackEvent(button, action, modifiers);
		if (!mouseButtonConsumedByUI) {

			if (button == GLFW_MOUSE_BUTTON_RIGHT) {
				if (action == GLFW_PRESS) {
					cameraMouseControlsActive = true;
				}
				else if (action == GLFW_RELEASE) {
					cameraMouseControlsActive = false;
				}
			}
		}
	}
	);

	glfwSetKeyCallback(window,
		[](GLFWwindow *, int key, int scancode, int action, int mods) {
		bool keyConsumedByUI = screen->keyCallbackEvent(key, scancode, action, mods);
		if (!keyConsumedByUI) {
			// treat the key input as camera controls

			if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
				glfwSetWindowShouldClose(window, GL_TRUE);

			if (action == GLFW_PRESS)
				keys[key] = true;
			else if (action == GLFW_RELEASE) {
				keys[key] = false;

				if (key == GLFW_KEY_G) {
					std::cout << "Toggling visibility of GUI -- press G to toggle" << std::endl;
					guiVisible = !guiVisible;
				}

				if (key == GLFW_KEY_M) {
					std::cout << "toggling visibility of top-down map in VR" << std::endl;
					showingTopDownMapInVR = !showingTopDownMapInVR;
				}

				if (key == GLFW_KEY_1) {
					std::cout << "setting to 1D path morph" << std::endl;
					renderingPathMorph = true;
				}

				if (key == GLFW_KEY_2) {
					std::cout << "setting to 2D morph" << std::endl;
					renderingPathMorph = false;
				}

				if (key == GLFW_KEY_V) {
					std::cout << "toggling keys to control visualization position" << std::endl;
					controllingVisualizationCamera = !controllingVisualizationCamera;
				}

				if (key == GLFW_KEY_P) {
					std::cout << "toggling prepPanoForEachEye" << std::endl;
					prepPanoForEachEye = !prepPanoForEachEye;
				}

				if (key == GLFW_KEY_H) {
					std::cout << "toggling renderHoloRoomForMorph" << std::endl;
					renderHoloRoomForMorph = !renderHoloRoomForMorph;
				}

				if (key == GLFW_KEY_R) {
					resetHMDForward();
				}
			}
		}
	}
	);

	glfwSetCharCallback(window,
		[](GLFWwindow *, unsigned int codepoint) {
		screen->charCallbackEvent(codepoint);
	}
	);

	glfwSetDropCallback(window,
		[](GLFWwindow *, int count, const char **filenames) {
		screen->dropCallbackEvent(count, filenames);
	}
	);

	glfwSetScrollCallback(window,
		[](GLFWwindow *, double x, double y) {
		screen->scrollCallbackEvent(x, y);
	}
	);

	glfwSetFramebufferSizeCallback(window,
		[](GLFWwindow *, int width, int height) {
		screenWidth = width;
		screenHeight = height;
		screen->resizeCallbackEvent(screenWidth, screenHeight);

		glViewport(0, 0, screenWidth, screenHeight);
	}
	);
}
#pragma endregion boilerplate



void RenderScene(vr::Hmd_Eye nEye, glm::mat4 & viewMat, glm::vec3 & panoSphereCenterPosition)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);


	glm::mat4 projectionMatForThisEye;
	if (nEye == vr::Hmd_Eye::Eye_Left) {
		projectionMatForThisEye = m_mat4ProjectionLeft;
	}
	else if (nEye == vr::Hmd_Eye::Eye_Right) {
		projectionMatForThisEye = m_mat4ProjectionRight;
	}

	glm::mat4 modelMatForThisEye;
	// render each eye as if they are translated away from the center of a sphere of some radius in space
	modelMatForThisEye = glm::translate(modelMatForThisEye, panoSphereCenterPosition);
	
	
	glm::mat4 modelIdentity;


	glm::mat4 viewMatForThisEye = viewMat;

	std::shared_ptr<Model> modelToRender;
	if (renderHoloRoomForMorph) {
		modelToRender = testHoloRoom;
	}
	else {
		modelToRender = GetUnprojectedWorldHullModelForSet(currentFrameNumbers);
	}
	drawPanoSphere(modelToRender, modelIdentity, viewMatForThisEye, projectionMatForThisEye);


	





	if (showingTopDownMapInVR) {
		
		//glClear(GL_DEPTH_BUFFER_BIT);

		meshShader->use();

		glm::mat4 mapModelMat = glm::inverse(viewMatForThisEye);
		mapModelMat = glm::translate(mapModelMat, glm::vec3(0.0f, -0.3f, -0.8f));
		mapModelMat = glm::rotate(mapModelMat, -glm::half_pi<float>(), glm::vec3(0, 0, 1));
		mapModelMat = glm::scale(mapModelMat, glm::vec3(0.25f, 0.25f, 0.0001f));

		int textureUnit = 0;
		glActiveTexture(GL_TEXTURE0 + textureUnit);
		glBindTexture(GL_TEXTURE_2D, topDownMapFramebuffer->texture2D());

		glUniform1i(glGetUniformLocation(meshShader->Program, "tex"), textureUnit);

		glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(mapModelMat));
		glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(viewMatForThisEye));
		glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(projectionMatForThisEye));
		glUniform1i(glGetUniformLocation(meshShader->Program, "texEnabled"), 1);
		glUniform1i(glGetUniformLocation(meshShader->Program, "noWraparound"), 1);
		glUniform1i(glGetUniformLocation(meshShader->Program, "lightingEnabled"), 0);
		//glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 1.0f, 1.0f);

		cubeModel->Draw(meshShader);

		glUniform1i(glGetUniformLocation(meshShader->Program, "texEnabled"), 0);
		glUniform1i(glGetUniformLocation(meshShader->Program, "noWraparound"), 0);


	}
}




void preparePanoramaCellCenterTriangulation(glm::vec3 viewPosition) {

	renderPosition = viewPosition;

	numSourcePanos = 0;

	insideCapturedArea = false;

	std::vector<Kernel::FT> unsorted_barycentric_coords;
	unsorted_barycentric_coords.reserve(3);	// alpha, beta, gamma


	Locate_type lt;
	int li;
	CGAL::internal::CC_iterator<CGAL::Compact_container<CGAL::Triangulation_ds_face_base_2<Tds>>, false> face_handle;

	// do the triangulation lookup
	Point2D query_point(viewPosition.x, viewPosition.z);
	face_handle = cellCenterTriangulation.locate(query_point, lt, li);

	switch (lt)
	{
	case Locate_type::EDGE:
		std::cout << "edge" << std::endl;
		break;
	case Locate_type::FACE:

		insideCapturedArea = true;

		numSourcePanos = 3;

		{
			// determine the barycentric coordinates of the query point
			Triangle_coordinates triangle_coordinates(face_handle->vertex(0)->point(), face_handle->vertex(1)->point(), face_handle->vertex(2)->point());

			triangle_coordinates(query_point, std::inserter(unsorted_barycentric_coords, unsorted_barycentric_coords.end()));	// calculates the barycentric coords and saves them in the preallocated list

			auto unsorted_frameNumber0 = face_handle->vertex(0)->info();	// 0-indexed
			auto unsorted_frameNumber1 = face_handle->vertex(1)->info();
			auto unsorted_frameNumber2 = face_handle->vertex(2)->info();

			std::vector<std::pair<unsigned int, double>> sortedFrameNumbersToBaryCoords;
			sortedFrameNumbersToBaryCoords.push_back(std::make_pair(unsorted_frameNumber0, unsorted_barycentric_coords[0]));
			sortedFrameNumbersToBaryCoords.push_back(std::make_pair(unsorted_frameNumber1, unsorted_barycentric_coords[1]));
			sortedFrameNumbersToBaryCoords.push_back(std::make_pair(unsorted_frameNumber2, unsorted_barycentric_coords[2]));
			std::sort(sortedFrameNumbersToBaryCoords.begin(), sortedFrameNumbersToBaryCoords.end()); // we refer to triplets by their frame numbers in ascending order

			currentFrameNumbers.clear();
			bary.clear();

			for (auto & pair : sortedFrameNumbersToBaryCoords) {
				currentFrameNumbers.push_back(pair.first);
				bary.push_back(pair.second);
			}

		}

		break;
	case Locate_type::VERTEX:
		std::cout << "vertex" << std::endl;
		break;
	case Locate_type::OUTSIDE_AFFINE_HULL:
	case Locate_type::OUTSIDE_CONVEX_HULL:
		break;
	default:
		break;
	}
}


PositionOnPath findNearestPositionOnPath(glm::vec3 queryPosition)
{
	
	float query_pt[3] = { queryPosition.x, queryPosition.y, queryPosition.z };

	const size_t num_results = 1;
	size_t ret_index;
	float out_dist_sqr;
	nanoflann::KNNResultSet<float> resultSet(num_results);
	resultSet.init(&ret_index, &out_dist_sqr);
	selected_1D_poses_kdTreeIndex->findNeighbors(resultSet, &query_pt[0], nanoflann::SearchParams(10));

	// ret_index is the index in the list of selected 1D pose indices, not the pose index itself

	int pidx_1d = ret_index;

	// next, we need to determine which line segment is closer: (pidx, pidx+1) or (pidx-1, pidx)

	bool at_path_beginning = (ret_index == 0);
	bool at_path_end = (ret_index == selected_1D_frame_numbers.size() - 1);
	
	bool could_be_prev_segment = !at_path_beginning; // (pidx-1, pidx) could possibly be the right segment
	bool could_be_next_segment = !at_path_end; // (pidx, pidx+1) could possible be the right segment

	float prev_dist = std::numeric_limits<float>::max();
	float next_dist = std::numeric_limits<float>::max();

	float prev_t = 0.0f;
	float next_t = 0.0f;

	if (could_be_prev_segment) {
		
		// calculate distance between query position and prev segment. get the "t" value and distance to the closest point.

		int pidx_1d_a = ret_index - 1;
		int pidx_1d_b = ret_index;

		glm::vec3 a = refined1DPath->poses[pidx_1d_a].Position;
		glm::vec3 b = refined1DPath->poses[pidx_1d_b].Position;

		glm::vec3 n = b - a;

		glm::vec3 p = queryPosition;

		auto t = glm::dot(p - a, b - a) / glm::dot(b - a, b - a);
		t = glm::clamp(t, 0.0f, 1.0f);

		auto p_prime = a + t * (b - a);

		auto dist = glm::length(p_prime - p);

		prev_dist = dist;
		prev_t = t;
	}

	if (could_be_next_segment) {
		// calculate distance between query position and prev segment. get the "t" value and distance to the closest point.

		int pidx_1d_a = ret_index;
		int pidx_1d_b = ret_index + 1;

		glm::vec3 a = refined1DPath->poses[pidx_1d_a].Position;
		glm::vec3 b = refined1DPath->poses[pidx_1d_b].Position;

		glm::vec3 n = b - a;

		glm::vec3 p = queryPosition;

		auto t = glm::dot(p - a, b - a) / glm::dot(b - a, b - a);
		t = glm::clamp(t, 0.0f, 1.0f);

		auto p_prime = a + t * (b - a);

		auto dist = glm::length(p_prime - p);

		next_dist = dist;
		next_t = t;
	}

	if (prev_dist < next_dist) {
		PositionOnPath ret;
		ret.i = ret_index - 1;
		ret.j = ret_index;
		ret.t = prev_t;

		//std::cout << "chose prev" << std::endl;

		return ret;
	}
	else {
		PositionOnPath ret;
		ret.i = ret_index;
		ret.j = ret_index + 1;
		ret.t = next_t;

		//std::cout << "chose next" << std::endl;

		return ret;
	}
}




void preparePanoramaPathMorph(glm::vec3 viewPosition) {

	numSourcePanos = 2;

	insideCapturedArea = true;	// always "inside" the area when doing 1d path morph

	auto newPositionOnPath = findNearestPositionOnPath(viewPosition);

	int selectedFrameNumber_i = poseIndices1DToFns[newPositionOnPath.i];
	int selectedFrameNumber_j = poseIndices1DToFns[newPositionOnPath.j];

	if (newPositionOnPath.i != currentPositionOnPath.i || newPositionOnPath.j != currentPositionOnPath.j) {
		// we've changed which line segment we're on; load in new data

	}

	currentPositionOnPath = newPositionOnPath;

	currentFrameNumbers.clear();
	//currentFrameNumbers.push_back(selectedFrameNumber_i - 1); // had to do this because of the different encoding of the 1D path frame numbers
	//currentFrameNumbers.push_back(selectedFrameNumber_j - 1);
	currentFrameNumbers.push_back(selectedFrameNumber_i); // had to do this because of the different encoding of the 1D path frame numbers
	currentFrameNumbers.push_back(selectedFrameNumber_j);

	bary.clear();
	bary.push_back(1.0f - currentPositionOnPath.t);
	bary.push_back(currentPositionOnPath.t);

	if (forceCameraOntoPathLine && !isRenderingStereo) {
		glm::vec3 a = frameNumbersToCubemapCameraCenters[currentFrameNumbers[0]];
		glm::vec3 b = frameNumbersToCubemapCameraCenters[currentFrameNumbers[1]];

		renderPosition = a + (b - a)*currentPositionOnPath.t;
	}
	else {
		renderPosition = viewPosition;
	}
	
}






void preparePanorama(glm::vec3 viewPosition) {

	glClearColor(0.2f, 0.25f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (renderingPathMorph) {
		// set up the panoramas for the 1D morph along a path
		preparePanoramaPathMorph(viewPosition);
	}
	else {
		// set up the panoramas for the normal triangulation-based 2D morph
		preparePanoramaCellCenterTriangulation(viewPosition);
	}
}

void RenderStereoTargets()
{
	if (prepPanoForEachEye) {
		glm::vec3 leftEyeWorldPosition = glm::column(glm::inverse(leftEyeViewMat), 3);
		preparePanorama(leftEyeWorldPosition);
	}
	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glEnable(GL_MULTISAMPLE);

	
	// Left Eye
	glBindFramebuffer(GL_FRAMEBUFFER, leftEyeDesc.m_nRenderFramebufferId);
	glViewport(0, 0, m_nRenderWidth, m_nRenderHeight);
	RenderScene(vr::Eye_Left, leftEyeViewMat, renderPosition);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glDisable(GL_MULTISAMPLE);
	
	glBindFramebuffer(GL_READ_FRAMEBUFFER, leftEyeDesc.m_nRenderFramebufferId);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, leftEyeDesc.m_nResolveFramebufferId);

	glBlitFramebuffer(0, 0, m_nRenderWidth, m_nRenderHeight, 0, 0, m_nRenderWidth, m_nRenderHeight,
		GL_COLOR_BUFFER_BIT,
		GL_LINEAR);

	glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	
	glEnable(GL_MULTISAMPLE);

	if (prepPanoForEachEye) {
		glm::vec3 rightEyeWorldPosition = glm::column(glm::inverse(rightEyeViewMat), 3);
		preparePanorama(rightEyeWorldPosition);
	}
	

	// Right Eye
	glBindFramebuffer(GL_FRAMEBUFFER, rightEyeDesc.m_nRenderFramebufferId);
	glViewport(0, 0, m_nRenderWidth, m_nRenderHeight);
	RenderScene(vr::Eye_Right, rightEyeViewMat, renderPosition);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glDisable(GL_MULTISAMPLE);

	glBindFramebuffer(GL_READ_FRAMEBUFFER, rightEyeDesc.m_nRenderFramebufferId);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, rightEyeDesc.m_nResolveFramebufferId);

	glBlitFramebuffer(0, 0, m_nRenderWidth, m_nRenderHeight, 0, 0, m_nRenderWidth, m_nRenderHeight,
		GL_COLOR_BUFFER_BIT,
		GL_LINEAR);

	glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	
}

//-----------------------------------------------------------------------------
// Purpose: Outputs a set of optional arguments to debugging output, using
//          the printf format setting specified in fmt*.
//-----------------------------------------------------------------------------
void dprintf(const char *fmt, ...)
{
	va_list args;
	char buffer[2048];

	va_start(args, fmt);
	vsprintf_s(buffer, fmt, args);
	va_end(args);

	if (g_bPrintf)
		printf("%s", buffer);

	OutputDebugStringA(buffer);
}

//-----------------------------------------------------------------------------
// Purpose: Processes a single VR event
//-----------------------------------------------------------------------------
void ProcessVREvent(const vr::VREvent_t & event)
{
	switch (event.eventType)
	{
	case vr::VREvent_TrackedDeviceActivated:
	{
		dprintf("Device %u attached.", event.trackedDeviceIndex);
	}
	break;
	case vr::VREvent_TrackedDeviceDeactivated:
	{
		dprintf("Device %u detached.\n", event.trackedDeviceIndex);
	}
	break;
	case vr::VREvent_TrackedDeviceUpdated:
	{
		dprintf("Device %u updated.\n", event.trackedDeviceIndex);
	}
	break;
	}
}


void HandleVRInput() {
	if (vrInited) {

		// Process SteamVR events
		vr::VREvent_t event;
		while (m_pHMD->PollNextEvent(&event, sizeof(event)))
		{
			ProcessVREvent(event);
		}

		// Process SteamVR controller state
		for (vr::TrackedDeviceIndex_t unDevice = 0; unDevice < vr::k_unMaxTrackedDeviceCount; unDevice++)
		{
			vr::VRControllerState_t state;
			if (m_pHMD->GetControllerState(unDevice, &state, sizeof(state)))
			{
				// can do something with the controller state here
			}
		}

	}
}

glm::mat4 ConvertSteamVRMatrixToMat4(const vr::HmdMatrix34_t &matPose)
{
	glm::mat4 matrixObj(
		matPose.m[0][0], matPose.m[1][0], matPose.m[2][0], 0.0,
		matPose.m[0][1], matPose.m[1][1], matPose.m[2][1], 0.0,
		matPose.m[0][2], matPose.m[1][2], matPose.m[2][2], 0.0,
		matPose.m[0][3], matPose.m[1][3], matPose.m[2][3], 1.0f
	);
	return matrixObj;
}

void UpdateHMDMatrixPose() {
	if (!m_pHMD) {
		return;
	}

	vr::VRCompositor()->WaitGetPoses(m_rTrackedDevicePose, vr::k_unMaxTrackedDeviceCount, NULL, 0);

	m_iValidPoseCount = 0;
	m_strPoseClasses = "";
	for (int nDevice = 0; nDevice < vr::k_unMaxTrackedDeviceCount; ++nDevice)
	{
		if (m_rTrackedDevicePose[nDevice].bPoseIsValid)
		{
			m_iValidPoseCount++;
			m_rmat4DevicePose[nDevice] = ConvertSteamVRMatrixToMat4(m_rTrackedDevicePose[nDevice].mDeviceToAbsoluteTracking);
			if (m_rDevClassChar[nDevice] == 0)
			{
				switch (m_pHMD->GetTrackedDeviceClass(nDevice))
				{
				case vr::TrackedDeviceClass_Controller:        m_rDevClassChar[nDevice] = 'C'; break;
				case vr::TrackedDeviceClass_HMD:               m_rDevClassChar[nDevice] = 'H'; break;
				case vr::TrackedDeviceClass_Invalid:           m_rDevClassChar[nDevice] = 'I'; break;
				case vr::TrackedDeviceClass_GenericTracker:    m_rDevClassChar[nDevice] = 'G'; break;
				case vr::TrackedDeviceClass_TrackingReference: m_rDevClassChar[nDevice] = 'T'; break;
				default:                                       m_rDevClassChar[nDevice] = '?'; break;
				}
			}
			m_strPoseClasses += m_rDevClassChar[nDevice];
		}
	}

	if (m_rTrackedDevicePose[vr::k_unTrackedDeviceIndex_Hmd].bPoseIsValid)
	{
		m_mat4HMDPose = m_rmat4DevicePose[vr::k_unTrackedDeviceIndex_Hmd];
		m_mat4HMDPose = glm::inverse(m_mat4HMDPose);

	}
}

#include <deque>
int numSamplesForFPS = 100;
std::deque<float> deltaTimes;



void gameloop() {
	// Game loop
	while (!glfwWindowShouldClose(window)) {
		// Set frame time
		GLfloat currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		if (deltaTimes.size() >= numSamplesForFPS) {
			deltaTimes.pop_front();
		}
		deltaTimes.push_back(deltaTime);

		if (printingFPS) {
			float dTimeSum = 0.0f;
			for (auto & dTime : deltaTimes) {
				dTimeSum += dTime;
			}
			float dTimeMean = dTimeSum / deltaTimes.size();

			std::cout << "dTimeMean = " << dTimeMean << std::endl;

		}

		// Check if any events have been activated (key pressed, mouse moved etc.) and call corresponding response functions
		glfwPollEvents();
		doMovement();


		HandleVRInput();

		// note that we have to enable the depth test per-frame; it looks like nanogui disables it in order to draw the UI
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);

		preparePanorama(visualizationCamera.Position);
		draw();

		if (guiVisible) {
			// Draw nanogui
			screen->drawContents();
			screen->drawWidgets();
		}
		

		glfwSwapBuffers(window);





		if (m_pHMD) {

			auto start = std::chrono::system_clock::now();


			m_mat4ProjectionLeft = GetHMDMatrixProjectionEye(vr::Eye_Left);
			m_mat4ProjectionRight = GetHMDMatrixProjectionEye(vr::Eye_Right);
			m_mat4eyePosLeft = GetHMDMatrixPoseEye(vr::Eye_Left);
			m_mat4eyePosRight = GetHMDMatrixPoseEye(vr::Eye_Right);





			hmdViewMat = m_mat4HMDPose * manual_forward_HMDPose;

			hmdViewMat = glm::translate(hmdViewMat, -renderPosition);

			glm::vec3 hmdRenderPosition = glm::column(glm::inverse(hmdViewMat), 3);

			leftEyeViewMat = m_mat4eyePosLeft * hmdViewMat;
			rightEyeViewMat = m_mat4eyePosRight * hmdViewMat;

			preparePanorama(hmdRenderPosition);



			isRenderingStereo = true;
			// render the top-down map to texture
			topDownMapFramebuffer->preRender();
			glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
			DrawTopDownMap(true);
			topDownMapFramebuffer->postRender();

			RenderStereoTargets();

			// submit the eye textures to the compositor
			vr::Texture_t leftEyeTexture = { (void*)(uintptr_t)leftEyeDesc.m_nResolveTextureId, vr::TextureType_OpenGL, vr::ColorSpace_Gamma };
			vr::VRCompositor()->Submit(vr::Eye_Left, &leftEyeTexture);
			vr::Texture_t rightEyeTexture = { (void*)(uintptr_t)rightEyeDesc.m_nResolveTextureId, vr::TextureType_OpenGL, vr::ColorSpace_Gamma };
			vr::VRCompositor()->Submit(vr::Eye_Right, &rightEyeTexture);
			isRenderingStereo = false;

			auto end = std::chrono::system_clock::now();

			std::chrono::duration<double> elapsed_seconds = end - start;

			if (printingFPS) {
				std::cout << "HMD rendering time elapsed: " << elapsed_seconds.count() << "s" << std::endl;
			}

		}


		if (m_bVblank && m_bGlFinishHack)
		{
			//$ HACKHACK. From gpuview profiling, it looks like there is a bug where two renders and a present
			// happen right before and after the vsync causing all kinds of jittering issues. This glFinish()
			// appears to clear that up. Temporary fix while I try to get nvidia to investigate this problem.
			// 1/29/2014 mikesart
			glFinish();
		}

		// Flush and wait for swap.
		if (m_bVblank)
		{
			glFlush();
			glFinish();
		}

		// Spew out the controller and pose count whenever they change.
		if (m_iTrackedControllerCount != m_iTrackedControllerCount_Last || m_iValidPoseCount != m_iValidPoseCount_Last)
		{
			m_iValidPoseCount_Last = m_iValidPoseCount;
			m_iTrackedControllerCount_Last = m_iTrackedControllerCount;

			dprintf("PoseCount:%d(%s) Controllers:%d\n", m_iValidPoseCount, m_strPoseClasses.c_str(), m_iTrackedControllerCount);
		}

		

		if (m_pHMD) {
			UpdateHMDMatrixPose();

			// make the visualization camera yaw follow the head orientation
			
			glm::vec3 hmdForwardDir = glm::column(hmdViewMat, 2);

			// project onto XZ plane
			hmdForwardDir.y = 0.0f;
			hmdForwardDir = glm::normalize(hmdForwardDir);

			float yawRadians = glm::atan(-hmdForwardDir.z, hmdForwardDir.x);
			float yawDegrees = glm::degrees(yawRadians);

			visualizationCamera.Yaw = yawDegrees;
			visualizationCamera.updateCameraVectors();
		}




		

		
	}
}





GLuint CompileGLShader(const char *pchShaderName, const char *pchVertexShader, const char *pchFragmentShader)
{
	GLuint unProgramID = glCreateProgram();

	GLuint nSceneVertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(nSceneVertexShader, 1, &pchVertexShader, NULL);
	glCompileShader(nSceneVertexShader);

	GLint vShaderCompiled = GL_FALSE;
	glGetShaderiv(nSceneVertexShader, GL_COMPILE_STATUS, &vShaderCompiled);
	if (vShaderCompiled != GL_TRUE)
	{
		dprintf("%s - Unable to compile vertex shader %d!\n", pchShaderName, nSceneVertexShader);
		glDeleteProgram(unProgramID);
		glDeleteShader(nSceneVertexShader);
		return 0;
	}
	glAttachShader(unProgramID, nSceneVertexShader);
	glDeleteShader(nSceneVertexShader); // the program hangs onto this once it's attached

	GLuint  nSceneFragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(nSceneFragmentShader, 1, &pchFragmentShader, NULL);
	glCompileShader(nSceneFragmentShader);

	GLint fShaderCompiled = GL_FALSE;
	glGetShaderiv(nSceneFragmentShader, GL_COMPILE_STATUS, &fShaderCompiled);
	if (fShaderCompiled != GL_TRUE)
	{
		dprintf("%s - Unable to compile fragment shader %d!\n", pchShaderName, nSceneFragmentShader);
		glDeleteProgram(unProgramID);
		glDeleteShader(nSceneFragmentShader);
		return 0;
	}

	glAttachShader(unProgramID, nSceneFragmentShader);
	glDeleteShader(nSceneFragmentShader); // the program hangs onto this once it's attached

	glLinkProgram(unProgramID);

	GLint programSuccess = GL_TRUE;
	glGetProgramiv(unProgramID, GL_LINK_STATUS, &programSuccess);
	if (programSuccess != GL_TRUE)
	{
		dprintf("%s - Error linking program %d!\n", pchShaderName, unProgramID);
		glDeleteProgram(unProgramID);
		return 0;
	}

	glUseProgram(unProgramID);
	glUseProgram(0);

	return unProgramID;
}





#pragma region visualizationsetup
void initShaders() {

	m_unCompanionWindowProgramID = CompileGLShader(
		"CompanionWindow",

		// vertex shader
		"#version 410 core\n"
		"layout(location = 0) in vec4 position;\n"
		"layout(location = 1) in vec2 v2UVIn;\n"
		"noperspective out vec2 v2UV;\n"
		"void main()\n"
		"{\n"
		"	v2UV = v2UVIn;\n"
		"	v2UV.y = 1.0 - v2UV.y;\n"
		"   v2UV /= vec2(2.0, 2.0);\n"
		"   v2UV += vec2(0.25, 0.25);\n"
		"	gl_Position = position;\n"
		"}\n",

		// fragment shader
		"#version 410 core\n"
		"uniform sampler2D mytexture;\n"
		"noperspective in vec2 v2UV;\n"
		"out vec4 outputColor;\n"
		"void main()\n"
		"{\n"
		"		outputColor = texture(mytexture, v2UV);\n"
		"}\n"
	);






	gridTexture = std::make_shared<GLTexture>("gridTexture");
	gridTexture->load(FileSystem::getPath("resources/panos/grid.png"));

	meshShader = std::make_shared<Shader>("mesh.vert", "mesh.frag");
	panoProjectiveTextureMappingShader = std::make_shared<Shader>("pano_projective_texture_mapping.vert", "pano_projective_texture_mapping.frag");
	pathShader = std::make_shared<Shader>("pathline.vert", "pathline.frag");
}


std::vector<float> loadPoseTimestamps(string const & filepath) {
	std::vector<float> timestamps;

	std::ifstream ifs;

	ifs.open(filepath, std::ifstream::in | std::ifstream::binary);

	std::string timestamp_str;
	std::string m00_str, m01_str, m02_str, m03_str;
	std::string m10_str, m11_str, m12_str, m13_str;
	std::string m20_str, m21_str, m22_str, m23_str;
	std::string m30_str, m31_str, m32_str, m33_str;

	while (getline(ifs, timestamp_str, ',')) {

		glm::mat4 m;

		getline(ifs, m00_str, ',');
		getline(ifs, m01_str, ',');
		getline(ifs, m02_str, ',');
		getline(ifs, m03_str, ',');

		getline(ifs, m10_str, ',');
		getline(ifs, m11_str, ',');
		getline(ifs, m12_str, ',');
		getline(ifs, m13_str, ',');

		getline(ifs, m20_str, ',');
		getline(ifs, m21_str, ',');
		getline(ifs, m22_str, ',');
		getline(ifs, m23_str, ',');

		getline(ifs, m30_str, ',');
		getline(ifs, m31_str, ',');
		getline(ifs, m32_str, ',');
		getline(ifs, m33_str);


		std::stringstream ss(timestamp_str);

		float timestamp;
		ss >> timestamp;

		timestamps.push_back(timestamp);
	}

	ifs.close();

	return timestamps;
}


void initModels() {
	arrowModel = std::make_shared<Model>(FileSystem::getPath("resources/meshes/arrow.obj").c_str());
	cubeModel = std::make_shared<Model>(FileSystem::getPath("resources/meshes/cube.obj").c_str());
	sphericalCameraFrustumModel = Model::CreateFrustum(90.0f, 1.0f, 0.25f);
	fullscreenQuadModel = Model::CreateFullscreenQuad();
	
	simpleSphereModel = Model::CreatePanoramaSphere(0.5f, 16, 8);

	std::string roomFilepath = string_format("resources/scans/%s/%s_mesh.room", SCAN_SESSION_LABEL.c_str(), SCAN_SESSION_LABEL.c_str());
	//std::string roomFilepath = string_format("resources/scans/%s/%s_mesh_refined.room", SCAN_SESSION_LABEL.c_str(), SCAN_SESSION_LABEL.c_str());
	cout << "roomFilepath: " << roomFilepath << endl;

	testHoloRoom = Model::LoadHoloRoom(FileSystem::getPath(roomFilepath).c_str());

	//std::string spherePosePath = string_format("resources/scans/%s/pose_sphere.csv", SCAN_SESSION_LABEL.c_str());
	//rawSpherePath = Path::LoadSpherePath(FileSystem::getPath(spherePosePath).c_str());











	//=============

	// load in the raw pose timestamps
	//rawCameraTimestamps = loadPoseTimestamps(FileSystem::getPath(spherePosePath).c_str());

	
	




	//=============

	// load in the selected 1D poses

	std::ifstream ifs;

	std::string selectedFrameNumbersPath = string_format("resources/scans/%s/selected_1d_frame_numbers.csv", SCAN_SESSION_LABEL.c_str());

	ifs.open(FileSystem::getPath(selectedFrameNumbersPath).c_str(), std::ifstream::in | std::ifstream::binary);


	std::vector<Pose> refined1DPoses;

	std::string fn_str;
	while (getline(ifs, fn_str)) {
		std::stringstream ss(fn_str);

		int fn;
		ss >> fn;


		fnsTo1DPoseIndices.emplace(fn, selected_1D_frame_numbers.size());
		poseIndices1DToFns.emplace(selected_1D_frame_numbers.size(), fn);
		selected_1D_frame_numbers.push_back(fn);
		
		Pose p = frameNumbersToSpherePoses[fn];
		refined1DPoses.push_back(p);
	}

	ifs.close();

	refined1DPath = std::make_shared<Path>(refined1DPoses);

	//=============

	std::cout << "generating kd-tree for selected 1D poses..." << std::endl;
	// Generate a kD-tree for the selected 1D poses
	size_t N_selected_1D_fns = selected_1D_frame_numbers.size();

	cloud_selected_1D_positions.pts.resize(N_selected_1D_fns);
	for (size_t i = 0; i < N_selected_1D_fns; i++) {
		int fn = selected_1D_frame_numbers[i];
		int pidx_1d = fnsTo1DPoseIndices[fn];
		Pose & pose = refined1DPath->poses[pidx_1d];
		cloud_selected_1D_positions.pts[i].x = pose.Position.x;
		cloud_selected_1D_positions.pts[i].y = pose.Position.y;
		cloud_selected_1D_positions.pts[i].z = pose.Position.z;
	}
	
	selected_1D_poses_kdTreeIndex = std::make_shared<my_kd_tree_t>(3 /* dim */, cloud_selected_1D_positions, nanoflann::KDTreeSingleIndexAdaptorParams(10));
	selected_1D_poses_kdTreeIndex->buildIndex();
	std::cout << "kd tree generated" << std::endl;





	//=============
}
#pragma endregion visualizationsetup


void initBuffers() {
	topDownMapFramebuffer = std::make_shared<Framebuffer>(256, 256, RGBA8);
}

void loadCubemapData() {

	// load in triangulation data, get refs to each triplet both by pose index and by frame numbe
	std::string refinedSphereFrameNumberTriangulationPath = FileSystem::getPath(string_format("resources/scans/%s/sphere_pose_triangulation_refined_frame_numbers.yaml", SCAN_SESSION_LABEL.c_str()));

	YAML::Node refinedSphereFrameNumberTriangulation = YAML::LoadFile(refinedSphereFrameNumberTriangulationPath);

	_frameNumbersToPanoTextures.clear();
	triangulationFrameNumbers.clear();
	uniqueFrameNumbers.clear();

	auto triangulationSphereFrameNumbersNode = refinedSphereFrameNumberTriangulation["triangulation_indices"];
	for (int i = 0; i < triangulationSphereFrameNumbersNode.size(); i++) {
		auto tripletSphereFrameNumbersNode = triangulationSphereFrameNumbersNode[i];

		std::vector<int> tripletFrameNumbers;
		for (int j = 0; j < 3; j++) {
			//auto frameNumber = tripletSphereFrameNumbersNode[j].as<int>() - 1;	// go from 1-indexed to 0-indexed
			auto frameNumber = tripletSphereFrameNumbersNode[j].as<int>(); // assume the 2D morph frame numbers are 0-indexed
			tripletFrameNumbers.push_back(frameNumber);
			uniqueFrameNumbers.insert(frameNumber);
		}
		std::sort(tripletFrameNumbers.begin(), tripletFrameNumbers.end());

		triangulationFrameNumbers.push_back(tripletFrameNumbers);
	}

	std::vector<std::pair<Point2D, size_t>> triangulation_points;


	for (auto pair : frameNumbersToSpherePoses) {
		int frame_number = pair.first;
		auto & sphere_pose = pair.second;

		//int zero_indexed_frame_number = frame_number - 1;	// TODO: is this right???
		int zero_indexed_frame_number = frame_number;	// TODO: is this right??

		auto cam_center = sphere_pose.Position;

		frameNumbersToCubemapCameraCenters.emplace(zero_indexed_frame_number, cam_center);

		frameNumbersToCameraRotationMatrices.emplace(zero_indexed_frame_number, sphere_pose.RotationMatrix);
		frameNumbersToCameraInverseRotationMatrices.emplace(zero_indexed_frame_number, sphere_pose.InverseRotationMatrix);

		frameNumbersToCameraModelMatrices.emplace(zero_indexed_frame_number, sphere_pose.ModelMatrix);

		// add to triangulation ONLY if the frame number is in the triangulation
		if (uniqueFrameNumbers.find(zero_indexed_frame_number) != uniqueFrameNumbers.end()) {
			triangulation_points.push_back(std::make_pair(Point2D(cam_center.x, cam_center.z), zero_indexed_frame_number));
		}
	}

	// set up triangulation to determine what triplet we are currently in
	cellCenterTriangulation.insert(triangulation_points.begin(), triangulation_points.end());

	std::vector<Vertex> pathTriangulationVertices;
	std::vector<GLuint> pathTriangulationIndices;

	GLuint currentIndex = 0;
	for (auto & tripletFrameNumbers : triangulationFrameNumbers) {

		for (auto & frameNumber : tripletFrameNumbers) {

			glm::vec3 cameraPos = frameNumbersToCubemapCameraCenters[frameNumber];

			pathTriangulationVertices.push_back(Vertex(cameraPos, glm::vec3(0,1,0)));


			pathTriangulationIndices.push_back(currentIndex);
			currentIndex++;
		}
	}

	pathTriangulationModel = std::make_shared<Model>();
	pathTriangulationModel->meshes.push_back(Mesh(pathTriangulationVertices, pathTriangulationIndices));




	std::cout << "done loading cubemap data" << std::endl;
}










//-----------------------------------------------------------------------------
// Purpose: Helper to get a string from a tracked device property and turn it
//			into a std::string
//-----------------------------------------------------------------------------
std::string GetTrackedDeviceString(vr::IVRSystem *pHmd, vr::TrackedDeviceIndex_t unDevice, vr::TrackedDeviceProperty prop, vr::TrackedPropertyError *peError = NULL)
{
	uint32_t unRequiredBufferLen = pHmd->GetStringTrackedDeviceProperty(unDevice, prop, NULL, 0, peError);
	if (unRequiredBufferLen == 0)
		return "";

	char *pchBuffer = new char[unRequiredBufferLen];
	unRequiredBufferLen = pHmd->GetStringTrackedDeviceProperty(unDevice, prop, pchBuffer, unRequiredBufferLen, peError);
	std::string sResult = pchBuffer;
	delete[] pchBuffer;
	return sResult;
}



bool initVR() {
	// Loading the SteamVR Runtime
	vr::EVRInitError eError = vr::VRInitError_None;
	m_pHMD = vr::VR_Init(&eError, vr::VRApplication_Scene);

	if (eError != vr::VRInitError_None)
	{
		m_pHMD = NULL;
		char buf[1024];
		sprintf_s(buf, sizeof(buf), "Unable to init VR runtime: %s", vr::VR_GetVRInitErrorAsEnglishDescription(eError));
		return false;
	}


	m_pRenderModels = (vr::IVRRenderModels *)vr::VR_GetGenericInterface(vr::IVRRenderModels_Version, &eError);
	if (!m_pRenderModels)
	{
		m_pHMD = NULL;
		vr::VR_Shutdown();

		char buf[1024];
		sprintf_s(buf, sizeof(buf), "Unable to get render model interface: %s", vr::VR_GetVRInitErrorAsEnglishDescription(eError));
		return false;
	}

	m_strDriver = "No Driver";
	m_strDisplay = "No Display";

	m_strDriver = GetTrackedDeviceString(m_pHMD, vr::k_unTrackedDeviceIndex_Hmd, vr::Prop_TrackingSystemName_String);
	m_strDisplay = GetTrackedDeviceString(m_pHMD, vr::k_unTrackedDeviceIndex_Hmd, vr::Prop_SerialNumber_String);

	std::cout << "VR Driver: " << m_strDriver << std::endl;
	std::cout << "VR Display: " << m_strDisplay << std::endl;


	// init the VR compositor

	if (!vr::VRCompositor())
	{
		printf("Compositor initialization failed. See log file for details\n");
		return false;
	}

	vr::VRCompositor()->SetTrackingSpace(vr::ETrackingUniverseOrigin::TrackingUniverseSeated);

	m_mat4ProjectionLeft = GetHMDMatrixProjectionEye(vr::Eye_Left);
	m_mat4ProjectionRight = GetHMDMatrixProjectionEye(vr::Eye_Right);
	m_mat4eyePosLeft = GetHMDMatrixPoseEye(vr::Eye_Left);
	m_mat4eyePosRight = GetHMDMatrixPoseEye(vr::Eye_Right);



	return true;
}

//-----------------------------------------------------------------------------
// Purpose: Creates a frame buffer. Returns true if the buffer was set up.
//          Returns false if the setup failed.
//-----------------------------------------------------------------------------
bool CreateFrameBuffer(int nWidth, int nHeight, FramebufferDesc &framebufferDesc)
{
	glGenFramebuffers(1, &framebufferDesc.m_nRenderFramebufferId);
	glBindFramebuffer(GL_FRAMEBUFFER, framebufferDesc.m_nRenderFramebufferId);

	glGenRenderbuffers(1, &framebufferDesc.m_nDepthBufferId);
	glBindRenderbuffer(GL_RENDERBUFFER, framebufferDesc.m_nDepthBufferId);
	glRenderbufferStorageMultisample(GL_RENDERBUFFER, 4, GL_DEPTH_COMPONENT, nWidth, nHeight);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, framebufferDesc.m_nDepthBufferId);

	glGenTextures(1, &framebufferDesc.m_nRenderTextureId);
	glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, framebufferDesc.m_nRenderTextureId);
	glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 4, GL_RGBA8, nWidth, nHeight, true);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, framebufferDesc.m_nRenderTextureId, 0);

	glGenFramebuffers(1, &framebufferDesc.m_nResolveFramebufferId);
	glBindFramebuffer(GL_FRAMEBUFFER, framebufferDesc.m_nResolveFramebufferId);

	glGenTextures(1, &framebufferDesc.m_nResolveTextureId);
	glBindTexture(GL_TEXTURE_2D, framebufferDesc.m_nResolveTextureId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, nWidth, nHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, framebufferDesc.m_nResolveTextureId, 0);

	// check FBO status
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE)
	{
		return false;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return true;
}

bool initStereo() {
	// set up stereo render targets

	if (!m_pHMD) {
		return false;
	}

	m_pHMD->GetRecommendedRenderTargetSize(&m_nRenderWidth, &m_nRenderHeight );

	CreateFrameBuffer(m_nRenderWidth, m_nRenderHeight, leftEyeDesc);
	CreateFrameBuffer(m_nRenderWidth, m_nRenderHeight, rightEyeDesc);

	return true;
}

void vrShutdown() {

	if (m_pHMD)
	{
		std::cout << "shutting down VR" << std::endl;
		vr::VR_Shutdown();
		m_pHMD = NULL;
	}
}

void initCompanionWindow() {

	if (!m_pHMD)
		return;

	std::vector<VertexDataWindow> vVerts;

	// left eye verts
	vVerts.push_back(VertexDataWindow(glm::vec2(-1, -1), glm::vec2(0, 1)));
	vVerts.push_back(VertexDataWindow(glm::vec2(0, -1), glm::vec2(1, 1)));
	vVerts.push_back(VertexDataWindow(glm::vec2(-1, 1), glm::vec2(0, 0)));
	vVerts.push_back(VertexDataWindow(glm::vec2(0, 1), glm::vec2(1, 0)));

	// right eye verts
	vVerts.push_back(VertexDataWindow(glm::vec2(0, -1), glm::vec2(0, 1)));
	vVerts.push_back(VertexDataWindow(glm::vec2(1, -1), glm::vec2(1, 1)));
	vVerts.push_back(VertexDataWindow(glm::vec2(0, 1), glm::vec2(0, 0)));
	vVerts.push_back(VertexDataWindow(glm::vec2(1, 1), glm::vec2(1, 0)));

	GLushort vIndices[] = { 0, 1, 3,   0, 3, 2,   4, 5, 7,   4, 7, 6 };
	m_uiCompanionWindowIndexSize = _countof(vIndices);

	glGenVertexArrays(1, &m_unCompanionWindowVAO);
	glBindVertexArray(m_unCompanionWindowVAO);

	glGenBuffers(1, &m_glCompanionWindowIDVertBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, m_glCompanionWindowIDVertBuffer);
	glBufferData(GL_ARRAY_BUFFER, vVerts.size() * sizeof(VertexDataWindow), &vVerts[0], GL_STATIC_DRAW);

	glGenBuffers(1, &m_glCompanionWindowIDIndexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_glCompanionWindowIDIndexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_uiCompanionWindowIndexSize * sizeof(GLushort), &vIndices[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(VertexDataWindow), (void *)offsetof(VertexDataWindow, position));

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(VertexDataWindow), (void *)offsetof(VertexDataWindow, texCoord));

	glBindVertexArray(0);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

}

int main(int /* argc */, char ** /* argv */) {

	initScanData(SCAN_SESSION_LABEL);

	initWindow();

	if (VR_ENABLED) {
		vrInited = initVR();
	}
	

	initCompanionWindow();

	initBuffers();

	initShaders();

	initModels();

	loadCubemapData();

	initStereo();

	gameloop();

	// Terminate GLFW, clearing any resources allocated by GLFW.
	glfwTerminate();


	if (VR_ENABLED) {
		vrShutdown();
	}


	return 0;
}