#include "utils.hpp"
#include <Eigen/Geometry>
#include "mesh.hpp"
#include "filesystem.h"

// given a point on the surface of a sphere, determine the corresponding UV coordinate
glm::vec2 spherePointToUV(glm::vec3 pt) {
	float latRadians = glm::asin(pt.y);
	float lonRadians = glm::atan(-pt.x, -pt.z);
	if (lonRadians >= M_PI) {
		lonRadians -= (2.0 * M_PI);
	}
	if (lonRadians <= -M_PI) {
		lonRadians += (2.0 * M_PI);
	}

	float u = 1.0 - (1.0 / (2.0 * M_PI) * lonRadians + 0.5);
	float v = 1.0 - (1.0 / (M_PI)* latRadians + 0.5);

	return glm::vec2(u, v);
}

// assumes that keypoints were all generated on an image that is only a single face (not the entire cubemap image)
glm::vec3 getKeypointOnSphereFromCubemapKeypoint(float x, float y, int cubeSidePixels, int cubeFaceNumber) {
	float imgX = x;
	float imgY = y;

	float imgXLocal = imgX;
	float imgYLocal = imgY;

	// maps [0,width] to [-1,+1]
	float horizontalLocation = (2.0f / cubeSidePixels) * imgXLocal - 1.0f;
	float verticalLocation = -((2.0f / cubeSidePixels) * imgYLocal - 1.0f); // flipping so positive points up instead of down

	glm::vec3 kpPositionUnnormalized;

	switch (cubeFaceNumber)
	{
	case 0:	// front
		kpPositionUnnormalized.x = horizontalLocation;
		kpPositionUnnormalized.y = verticalLocation;
		kpPositionUnnormalized.z = -1.0f;
		break;
	case 1:	// right
		kpPositionUnnormalized.x = 1.0f;
		kpPositionUnnormalized.y = verticalLocation;
		kpPositionUnnormalized.z = horizontalLocation;
		break;
	case 2:	// back
		kpPositionUnnormalized.x = -horizontalLocation;
		kpPositionUnnormalized.y = verticalLocation;
		kpPositionUnnormalized.z = 1.0f;
		break;
	case 3:	// left
		kpPositionUnnormalized.x = -1.0f;
		kpPositionUnnormalized.y = verticalLocation;
		kpPositionUnnormalized.z = -horizontalLocation;
		break;
	case 4:	// up
		kpPositionUnnormalized.x = verticalLocation;
		kpPositionUnnormalized.y = 1.0f;
		kpPositionUnnormalized.z = -horizontalLocation;
		break;
	case 5:	// down
		kpPositionUnnormalized.x = -verticalLocation;
		kpPositionUnnormalized.y = -1.0f;
		kpPositionUnnormalized.z = -horizontalLocation;
		break;
	default:
		std::cout << "ERROR: invalid cube face number for keypoint " << x << " " << y << std::endl;
		break;
	}

	glm::vec3 kpPosition = glm::normalize(kpPositionUnnormalized);	// map from cube to sphere

	return kpPosition;
}


bool LoadNVM(ifstream& in, vector<CameraD>& camera_data, vector<Point3D>& point_data,
	vector<MeasurementPoint2D>& measurements, vector<int>& ptidx, vector<int>& camidx,
	vector<string>& names, vector<int>& ptc, vector<float>& fixedK, vector<int>& featureidx)
{
	int rotation_parameter_num = 4;
	bool format_r9t = false;
	string token;
	if (in.peek() == 'N')
	{
		in >> token; //file header
		if (strstr(token.c_str(), "R9T"))
		{
			rotation_parameter_num = 9;    //rotation as 3x3 matrix
			format_r9t = true;
		}

		// NOTE: Assume FixedK!

		string fixedKLabel;
		float fx, fy, cx, cy, dist;
		in >> fixedKLabel;
		in >> fx >> fy >> cx >> cy >> dist;
		fixedK.push_back(fx);
		fixedK.push_back(fy);
		fixedK.push_back(cx);
		fixedK.push_back(cy);
		fixedK.push_back(dist);

	}

	int ncam = 0, npoint = 0, nproj = 0;
	// read # of cameras
	in >> ncam;  if (ncam <= 1) return false;

	//read the camera parameters
	camera_data.resize(ncam); // allocate the camera data
	names.resize(ncam);
	for (int i = 0; i < ncam; ++i)
	{
		double f, q[9], c[3], d[2];
		in >> token >> f;
		for (int j = 0; j < rotation_parameter_num; ++j) in >> q[j];
		in >> c[0] >> c[1] >> c[2] >> d[0] >> d[1];

		camera_data[i].SetFocalLength(f);
		if (format_r9t)
		{
			camera_data[i].SetMatrixRotation(q);
			camera_data[i].SetTranslation(c);
		}
		else
		{
			//older format for compability
			camera_data[i].SetQuaternionRotation(q);        //quaternion from the file
			camera_data[i].SetCameraCenterAfterRotation(c); //camera center from the file
		}
		camera_data[i].SetNormalizedMeasurementDistortion(d[0]);
		names[i] = token;
	}

	//////////////////////////////////////
	in >> npoint;   if (npoint <= 0) return false;

	//read image projections and 3D points.
	point_data.resize(npoint);
	for (int i = 0; i < npoint; ++i)
	{
		float pt[3]; int cc[3], npj;
		in >> pt[0] >> pt[1] >> pt[2]
			>> cc[0] >> cc[1] >> cc[2] >> npj;
		for (int j = 0; j < npj; ++j)
		{
			int cidx, fidx; float imx, imy;
			in >> cidx >> fidx >> imx >> imy;

			camidx.push_back(cidx);    //camera index
			featureidx.push_back(fidx);	// feature index
			ptidx.push_back(i);        //point index

									   //add a measurment to the vector
			measurements.push_back(MeasurementPoint2D(imx, imy));
			nproj++;
		}
		point_data[i].SetPoint(pt);
		ptc.insert(ptc.end(), cc, cc + 3);
	}
	///////////////////////////////////////////////////////////////////////////////
	std::cout << ncam << " cameras; " << npoint << " 3D points; " << nproj << " projections\n";

	return true;
}


void SaveNVM(const char* filename, vector<CameraD>& camera_data, vector<Point3D>& point_data,
	vector<MeasurementPoint2D>& measurements, vector<int>& ptidx, vector<int>& camidx,
	vector<string>& names, vector<int>& ptc, vector<float>& fixedK, vector<int>& featureidx)
{
	std::cout << "Saving model to " << filename << "...\n";
	ofstream out(filename);

	out << "NVM_V3 FixedK " << fixedK[0] << ' ' << fixedK[1] << ' ' << fixedK[2] << ' ' << fixedK[3] << ' ' << fixedK[4] << "\n";
	out << "\n";
	out << camera_data.size() << '\n' << std::setprecision(12);
	if (names.size() < camera_data.size()) names.resize(camera_data.size(), string("unknown"));
	if (ptc.size() < 3 * point_data.size()) ptc.resize(point_data.size() * 3, 0);

	////////////////////////////////////
	for (size_t i = 0; i < camera_data.size(); ++i)
	{
		CameraD& cam = camera_data[i];
		out << names[i] << ' ' << cam.GetFocalLength() << ' ';

		double q[4];
		cam.GetQuaternionRotation(q);
		for (int j = 0; j < 4; ++j) out << q[j] << ' ';

		double camCenter[3];
		cam.GetCameraCenter(camCenter);

		for (int j = 0; j < 3; ++j) out << camCenter[j] << ' ';

		out << cam.GetNormalizedMeasurementDistortion() << " 0\n";
	}

	out << point_data.size() << '\n';

	for (size_t i = 0, j = 0; i < point_data.size(); ++i)
	{
		Point3D& pt = point_data[i];
		int * pc = &ptc[i * 3];
		out << pt.xyz[0] << ' ' << pt.xyz[1] << ' ' << pt.xyz[2] << ' '
			<< pc[0] << ' ' << pc[1] << ' ' << pc[2] << ' ';

		size_t je = j;
		while (je < ptidx.size() && ptidx[je] == (int)i) je++;

		out << (je - j) << ' ';

		for (; j < je; ++j)    out << camidx[j] << ' ' << " " << featureidx[j] << " " << measurements[j].x << ' ' << measurements[j].y << ' ';

		out << '\n';
	}
}

std::string getPanoFramePath(std::string scanSessionLabel, int frameIndex) {
	//auto frameLabel = string_format("pano_frame%05d", frameIndex + 1); // frame images are 1-indexed
	auto frameLabel = string_format("pano_frame%05d", frameIndex + 1); // frame images are 1-indexed
	auto framePath = FileSystem::getPath(string_format("resources/scans/%s/pano_frames/%s.jpg", scanSessionLabel.c_str(), frameLabel.c_str()));
	return framePath;
}





float EPS = 0.00000001f;

// http://paulbourke.net/geometry/pointlineplane/lineline.c
/*
Calculate the line segment PaPb that is the shortest route between
two lines P1P2 and P3P4. Calculate also the values of mua and mub where
Pa = P1 + mua (P2 - P1)
Pb = P3 + mub (P4 - P3)
Return FALSE if no solution exists.
*/
bool LineLineIntersect(
	glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec3 p4, glm::vec3 *pa, glm::vec3 *pb,
	double *mua, double *mub)
{
	glm::vec3 p13, p43, p21;
	double d1343, d4321, d1321, d4343, d2121;
	double numer, denom;

	p13.x = p1.x - p3.x;
	p13.y = p1.y - p3.y;
	p13.z = p1.z - p3.z;
	p43.x = p4.x - p3.x;
	p43.y = p4.y - p3.y;
	p43.z = p4.z - p3.z;
	if (abs(p43.x) < EPS && abs(p43.y) < EPS && abs(p43.z) < EPS) {
		return false;
	}
	p21.x = p2.x - p1.x;
	p21.y = p2.y - p1.y;
	p21.z = p2.z - p1.z;
	if (abs(p21.x) < EPS && abs(p21.y) < EPS && abs(p21.z) < EPS) {
		return false;
	}

	d1343 = p13.x * p43.x + p13.y * p43.y + p13.z * p43.z;
	d4321 = p43.x * p21.x + p43.y * p21.y + p43.z * p21.z;
	d1321 = p13.x * p21.x + p13.y * p21.y + p13.z * p21.z;
	d4343 = p43.x * p43.x + p43.y * p43.y + p43.z * p43.z;
	d2121 = p21.x * p21.x + p21.y * p21.y + p21.z * p21.z;

	denom = d2121 * d4343 - d4321 * d4321;
	if (abs(denom) < EPS) {
		return false;
	}
	numer = d1343 * d4321 - d1321 * d4343;

	*mua = numer / denom;
	*mub = (d1343 + d4321 * (*mua)) / d4343;

	pa->x = p1.x + *mua * p21.x;
	pa->y = p1.y + *mua * p21.y;
	pa->z = p1.z + *mua * p21.z;
	pb->x = p3.x + *mub * p43.x;
	pb->y = p3.y + *mub * p43.y;
	pb->z = p3.z + *mub * p43.z;

	return true;
}