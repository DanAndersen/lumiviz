#pragma once

#include "glm_includes.hpp"

#include <iostream>
#include <iomanip>

void CheckOpenGLError(const char* stmt, const char* fname, int line);

#ifdef _DEBUG
#define GL_CHECK(stmt) do { \
            stmt; \
            CheckOpenGLError(#stmt, __FILE__, __LINE__); \
        } while (0)
#else
#define GL_CHECK(stmt) stmt
#endif


#include <string>
#include <memory>
#include <vector>


#include "cgal_includes.hpp"


struct ImageFormat {
	GLint InternalFormat;
	GLenum Format;
	GLenum Type;
	int NumChannels;

	ImageFormat(GLint internalFormat, GLenum format, GLenum type, int numChannels) :
		InternalFormat(internalFormat),
		Format(format),
		Type(type),
		NumChannels(numChannels)
	{}
};


struct Vertex {
	glm::vec3 Position;
	glm::vec3 Normal;
	glm::uint PointId;
	glm::vec2 TexCoords0;
	glm::vec2 TexCoords1;
	glm::vec2 TexCoords2;
	glm::uint TriangleId;	// optional value representing what triangle this vertex belongs to (assumes unshared verts across faces)
	glm::vec3 BarycentricCoords; // optional value representing the barycentric coords of this particular vertex relative to some triangle (possible values are (1,0,0), (0,1,0) or (0,0,1))

	Vertex(glm::vec3 position) :
		Position(position),
		Normal(0.0f, 0.0f, 0.0f),
		PointId(0),
		TexCoords0(0.0f, 0.0f),
		TexCoords1(0.0f, 0.0f),
		TexCoords2(0.0f, 0.0f),
		TriangleId(0),
		BarycentricCoords(0.0f, 0.0f, 0.0f)
	{}

	Vertex(glm::vec3 position, glm::vec3 normal) :
		Position(position),
		Normal(normal),
		PointId(0),
		TexCoords0(0.0f, 0.0f),
		TexCoords1(0.0f, 0.0f),
		TexCoords2(0.0f, 0.0f),
		TriangleId(0),
		BarycentricCoords(0.0f, 0.0f, 0.0f)
	{}

	Vertex(glm::vec3 position, glm::vec3 normal, glm::vec2 texCoords) :
		Position(position),
		Normal(normal),
		PointId(0),
		TexCoords0(texCoords),
		TexCoords1(texCoords),
		TexCoords2(texCoords),
		TriangleId(0),
		BarycentricCoords(0.0f, 0.0f, 0.0f)
	{}

	Vertex(glm::vec3 position, glm::vec3 normal, glm::vec2 texCoords0, glm::vec2 texCoords1, glm::vec2 texCoords2) :
		Position(position),
		Normal(normal),
		PointId(0),
		TexCoords0(texCoords0),
		TexCoords1(texCoords1),
		TexCoords2(texCoords2),
		TriangleId(0),
		BarycentricCoords(0.0f, 0.0f, 0.0f)
	{}
};







template <class T>
inline void hash_combine(std::size_t & s, const T & v)
{
	std::hash<T> h;
	s ^= h(v) + 0x9e3779b9 + (s << 6) + (s >> 2);
}


namespace std
{
	template <>
	struct hash<Vertex>
	{
		std::size_t operator()(const Vertex& c) const
		{
			std::size_t res = 0;
			hash_combine(res, c.BarycentricCoords.x);
			hash_combine(res, c.BarycentricCoords.y);
			hash_combine(res, c.BarycentricCoords.z);
			hash_combine(res, c.Normal.x);
			hash_combine(res, c.Normal.y);
			hash_combine(res, c.Normal.z);
			hash_combine(res, c.PointId);
			hash_combine(res, c.Position.x);
			hash_combine(res, c.Position.y);
			hash_combine(res, c.Position.z);
			hash_combine(res, c.TexCoords0.x);
			hash_combine(res, c.TexCoords0.y);
			hash_combine(res, c.TexCoords1.x);
			hash_combine(res, c.TexCoords1.y);
			hash_combine(res, c.TexCoords2.x);
			hash_combine(res, c.TexCoords2.y);
			hash_combine(res, c.TriangleId);
			return res;
		}
	};
}




struct CameraProfile {
	int Width;
	int Height;
	float HfovDegrees;

	CameraProfile(int width, int height, float hfovDegrees) :
		Width(width),
		Height(height),
		HfovDegrees(hfovDegrees)
	{

	}
};

struct ScanSession {
	std::string Label;
	
	ScanSession(std::string label) :
		Label(label)
	{

	}
};




// setting up hash function for ivec2: http://stackoverflow.com/questions/18098178/how-do-i-use-unordered-set
namespace std {
	template<>
	struct hash<glm::ivec2>
	{
		size_t operator()(glm::ivec2 const & p) const noexcept
		{
			return (
				(51 + std::hash<int>()(p.x)) * 51
				+ std::hash<int>()(p.y)
				);
		}
	};
}


// This is an exampleof a custom data set class
template <typename T>
struct PointCloud
{
	struct Point
	{
		T  x, y, z;
	};

	std::vector<Point>  pts;

	// Must return the number of data points
	inline size_t kdtree_get_point_count() const { return pts.size(); }

	// Returns the distance between the vector "p1[0:size-1]" and the data point with index "idx_p2" stored in the class:
	inline T kdtree_distance(const T *p1, const size_t idx_p2, size_t /*size*/) const
	{
		const T d0 = p1[0] - pts[idx_p2].x;
		const T d1 = p1[1] - pts[idx_p2].y;
		const T d2 = p1[2] - pts[idx_p2].z;
		return d0*d0 + d1*d1 + d2*d2;
	}

	// Returns the dim'th component of the idx'th point in the class:
	// Since this is inlined and the "dim" argument is typically an immediate value, the
	//  "if/else's" are actually solved at compile time.
	inline T kdtree_get_pt(const size_t idx, int dim) const
	{
		if (dim == 0) return pts[idx].x;
		else if (dim == 1) return pts[idx].y;
		else return pts[idx].z;
	}

	// Optional bounding-box computation: return false to default to a standard bbox computation loop.
	//   Return true if the BBOX was already computed by the class and returned in "bb" so it can be avoided to redo it again.
	//   Look at bb.size() to find out the expected dimensionality (e.g. 2 or 3 for point clouds)
	template <class BBOX>
	bool kdtree_get_bbox(BBOX& /* bb */) const { return false; }

};

template<typename ... Args>
std::string string_format(const std::string& format, Args ... args)
{
	size_t size = std::snprintf(nullptr, 0, format.c_str(), args ...) + 1; // Extra space for '\0'
	std::unique_ptr<char[]> buf(new char[size]);
	std::snprintf(buf.get(), size, format.c_str(), args ...);
	return std::string(buf.get(), buf.get() + size - 1); // We don't want the '\0' inside
}

class Model;

