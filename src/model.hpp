#pragma once

#include "common.hpp"
#include "shader.hpp"
#include "mesh.hpp"

#include <assimp/Importer.hpp>      
#include <assimp/scene.h>           
#include <assimp/postprocess.h>

#include <vector>
#include <string>
#include <list>

#include <CGAL/Simple_cartesian.h>
#include <CGAL/AABB_tree.h>
#include <CGAL/AABB_traits.h>
#include <CGAL/Surface_mesh.h>
#include <CGAL/AABB_face_graph_triangle_primitive.h>
#include <CGAL/Polygon_mesh_processing/compute_normal.h>
#include <CGAL/Polygon_mesh_processing/orientation.h>




typedef CGAL::Surface_mesh<K_Point> K_Mesh;
typedef K_Mesh::Vertex_index K_vertex_descriptor;
typedef K_Mesh::Face_index K_face_descriptor;

typedef CGAL::AABB_face_graph_triangle_primitive<K_Mesh> K_Primitive;
typedef CGAL::AABB_traits<K, K_Primitive> K_Traits;
typedef CGAL::AABB_tree<K_Traits> K_Tree;
typedef boost::optional<K_Tree::Intersection_and_primitive_id<K_Ray>::Type> K_Ray_intersection;


using namespace std;

class Model
{
public:
	Model(); // empty model
	Model(string const & path);
	Model(const aiScene * scene);
	void Draw(std::shared_ptr<Shader> shader);

	static std::shared_ptr<Model> CreatePatchedSphere(int numPointsPerEdge);

	static std::shared_ptr<Model> CreateFrustum(float hfovDegrees, float aspectRatio, float zMeters);

	static std::shared_ptr<Model> CreatePanoramaSphere(float radius, int numSegments, int numRings, bool invertNormals = false);

	static std::shared_ptr<Model> CreateConvexHullFromPoints(std::vector<glm::vec3> pts);

	static std::shared_ptr<Model> LoadHoloRoom(string const & filepath, bool flatteningRoom = false, bool removingCeilingAndFloor = false, float ceilingMinY = 0.0f, float floorMaxY = 0.0f);

	static std::shared_ptr<Model> CreateFullscreenQuad();

	static std::shared_ptr<Model> CreateAugmentedHullModelFromUnprojectedHullModel(std::shared_ptr<Model> unprojectedHullModel, glm::vec3 centerPoint);

	void GenerateAABBTree();

	K_Mesh kMesh;

	std::shared_ptr<K_Tree> aabb_tree;

	std::vector<K_vertex_descriptor> points;

	vector<Mesh> meshes;

private:
	

	void processNode(aiNode* node, const aiScene* scene);
	Mesh processMesh(aiMesh* mesh, const aiScene* scene);

	void init(const aiScene * scene);

	
};