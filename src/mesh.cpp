#include "mesh.hpp"

Mesh::Mesh(vector<Vertex> vertices, vector<GLuint> indices)
{
	this->vertices = vertices;
	this->indices = indices;

	this->setupMesh();
}

void Mesh::Draw(std::shared_ptr<Shader> shader)
{
	if (this->indices.size() > 0) {
		shader->use();

		glBindVertexArray(this->VAO);
		glDrawElements(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
	}
	else {
		//std::cout << "Skipping Draw() because no indices in mesh" << std::endl;
	}
}

void Mesh::setupMesh()
{
	if (this->indices.size() > 0) {
		glGenVertexArrays(1, &this->VAO);
		glGenBuffers(1, &this->VBO);
		glGenBuffers(1, &this->EBO);

		glBindVertexArray(this->VAO);

		glBindBuffer(GL_ARRAY_BUFFER, this->VBO);

		glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(Vertex), &this->vertices[0], GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(GLuint), &this->indices[0], GL_STATIC_DRAW);

		GLuint vertexAttribPosition = 0;
		GLuint vertexAttribNormal = 1;
		GLuint vertexAttribPointId = 2;
		GLuint vertexAttribTexCoord0 = 3;
		GLuint vertexAttribTexCoord1 = 4;
		GLuint vertexAttribTexCoord2 = 5;
		GLuint vertexAttribTriangleId = 6;
		GLuint vertexAttribBarycentricCoords = 7;

		// positions
		glEnableVertexAttribArray(vertexAttribPosition);
		glVertexAttribPointer(vertexAttribPosition, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, Position));

		// normals
		glEnableVertexAttribArray(vertexAttribNormal);
		glVertexAttribPointer(vertexAttribNormal, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, Normal));

		// point id
		glEnableVertexAttribArray(vertexAttribPointId);
		glVertexAttribIPointer(vertexAttribPointId, 1, GL_UNSIGNED_INT, sizeof(Vertex), (GLvoid*)offsetof(Vertex, PointId));

		// texcoords0
		glEnableVertexAttribArray(vertexAttribTexCoord0);
		glVertexAttribPointer(vertexAttribTexCoord0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, TexCoords0));

		// texcoords1
		glEnableVertexAttribArray(vertexAttribTexCoord1);
		glVertexAttribPointer(vertexAttribTexCoord1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, TexCoords1));

		// texcoords2
		glEnableVertexAttribArray(vertexAttribTexCoord2);
		glVertexAttribPointer(vertexAttribTexCoord2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, TexCoords2));

		// triangle id
		glEnableVertexAttribArray(vertexAttribTriangleId);
		glVertexAttribIPointer(vertexAttribTriangleId, 1, GL_UNSIGNED_INT, sizeof(Vertex), (GLvoid*)offsetof(Vertex, TriangleId));

		// barycentric coord
		glEnableVertexAttribArray(vertexAttribBarycentricCoords);
		glVertexAttribPointer(vertexAttribBarycentricCoords, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, BarycentricCoords));

		glBindVertexArray(0);
	}
	else {
		std::cout << "Skipping setupMesh() because no indices in mesh" << std::endl;
	}
}

void Mesh::rebufferVertices() {
	if (this->vertices.size() > 0) {
		glBindVertexArray(this->VAO);

		glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
		glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(Vertex), &this->vertices[0], GL_STATIC_DRAW);

		glBindVertexArray(0);
	}
	else {
		std::cout << "Skipping rebufferVertices() because no vertices in mesh" << std::endl;
	}
	
}

void Mesh::rebufferIndices() {
	if (this->indices.size() > 0) {
		glBindVertexArray(this->VAO);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(GLuint), &this->indices[0], GL_STATIC_DRAW);

		glBindVertexArray(0);
	}
	else {
		std::cout << "Skipping rebufferIndices() because no indices in mesh" << std::endl;
	}
	
}