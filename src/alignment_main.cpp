// GLFW
//
#if defined(NANOGUI_GLAD)
    #if defined(NANOGUI_SHARED) && !defined(GLAD_GLAPI_EXPORT)
        #define GLAD_GLAPI_EXPORT
    #endif

    #include <glad/glad.h>
#else
    #if defined(__APPLE__)
        #define GLFW_INCLUDE_GLCOREARB
    #else
        #define GL_GLEXT_PROTOTYPES
    #endif
#endif

#include <GLFW/glfw3.h>



#include <nanogui/nanogui.h>
#include <iostream>
#include <memory>

#include "common.hpp"

#include "shader.hpp"
#include "mesh.hpp"
#include "model.hpp"

#include "filesystem.h"

#include "camera.hpp"

#include "path.hpp"

#include <opencv2/opencv.hpp>
#include <glm/gtx/vector_angle.hpp>

#include "gltexture.hpp"

#include "framebuffer.hpp"

#include <stb_image.h>

using namespace nanogui;

int screenWidth = 800;
int screenHeight = 800;

enum test_enum {
    Item1 = 0,
    Item2,
    Item3
};

bool bvar = true;
int ivar = 12345678;
double dvar = 3.1415926;
float fvar = (float)dvar;
std::string strval = "A string";
test_enum enumval = Item2;
Color colval(0.5f, 0.5f, 0.7f, 1.f);

Screen *screen = nullptr;

std::shared_ptr<Shader> meshShader;
std::shared_ptr<Shader> pathShader;
std::shared_ptr<Shader> spherePanoShader;

std::shared_ptr<Shader> skyboxShader;

std::shared_ptr<Model> frustumModel;
std::shared_ptr<Model> cubeModel;
std::shared_ptr<Model> quadModel;
std::shared_ptr<Model> testHoloRoom;
std::shared_ptr<Model> testHoloRoomWalkableSpace;

std::shared_ptr<Model> arrowModel;

std::shared_ptr<Model> skyboxModel;

std::shared_ptr<Model> panoSphereModel;
std::shared_ptr<GLTexture> panoSphereTexture;

std::shared_ptr<Path> testPath;

Camera thirdPersonCamera(glm::vec3(0.0f, 0.0f, 0.0f)); // camera for moving around the scene
Camera visualizationCamera(glm::vec3(0.0f, 0.0f, 0.0f)); // camera for viewing the scene from predetermined locations
bool controllingVisualizationCamera = false;

glm::vec3 lightPos(1.0f, 2.0f, 3.0f);

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

GLFWwindow* window;
bool keys[1024];

GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;

bool showingSkybox = false;
float skyboxScale = 50.0f;

bool cameraMouseControlsActive = false;

CameraProfile locatableCameraProfile(896, 504, 48.0f);	// Low power / Low resolution mode for image processing tasks

float locatableCameraAspectRatio = (float)locatableCameraProfile.Width / (float)locatableCameraProfile.Height;
float locatableCameraFovYRadians = glm::radians(locatableCameraProfile.HfovDegrees / locatableCameraAspectRatio);

glm::mat4 locatableCameraProjection = glm::perspective(locatableCameraFovYRadians, locatableCameraAspectRatio, 0.1f, 100.0f);
glm::mat4 inverseLocatableCameraProjection = glm::inverse(locatableCameraProjection);

float directionWeight = 0.5f;

bool renderingSimulatedView = false;

//ScanSessionHLOnly currentScan("20170912150450", 388);	// lab, with per-frame pose
//ScanSessionHLOnly currentScan("20170918135247", 445);	// looking at chessboard, first
ScanSessionHLOnly currentScan("20170919102131", 696);	// looking at chessboard, second

//ScanSessionHLOnly currentScan("20170411154653", 13627);	// OLD recording (entire room)

LocatableCamera locatableCamera(locatableCameraProfile.HfovDegrees, locatableCameraAspectRatio, 0.01f, 100.0f);


std::shared_ptr<Framebuffer> simulatedViewFramebuffer;

bool usingTextureArray = true;
const int numTextureArrayLayers = 16;

Slider* cameraPathSlider;

int mCurrentForwardFrameIndex;
int mCurrentFrameIndices[numTextureArrayLayers];

int poseIndexOffset = 0;

bool printingInfo = false;

using imagesDataType = vector<pair<GLTexture, GLTexture::handleType>>;
imagesDataType mNearestForwardFrameImageData;
imagesDataType mNearestForwardThumbImageData;

nanogui::ref<ImageView> mNearestForwardThumbImageView;

float initialHeadY;

bool cellCenterLookup = true;

float poseTimestampOffset = 0.0f;

int frameNumber = 0;

std::vector<glm::vec2> normalizedDetectedImagePoints;





std::vector<glm::vec2> loadNormalizedDetectedImagePoints(string const & filepath) {
	
	std::vector<glm::vec2> pts;
	
	std::ifstream ifs;

	string x_str;
	string y_str;

	ifs.open(filepath, std::ifstream::in | std::ifstream::binary);

	if (!ifs.good()) {
		std::cout << "Note: no file found for normalized detected image points" << std::endl;
		return pts;	// return empty
	}

	while (getline(ifs, x_str, ',')) {
		getline(ifs, y_str);

		auto pt = glm::vec2(stof(x_str), stof(y_str));

		std::cout << pt.x << " " << pt.y << std::endl;

		pts.push_back(pt);
	}


	ifs.close();

	return pts;
}



void initShader() {

	simulatedViewFramebuffer = std::make_shared<Framebuffer>(locatableCameraProfile.Width, locatableCameraProfile.Height, GL_TEXTURE_2D, GL_LINEAR, usingTextureArray, numTextureArrayLayers);

	meshShader = std::make_shared<Shader>("mesh.vert", "mesh.frag");
	pathShader = std::make_shared<Shader>("pathline.vert", "pathline.frag");
	spherePanoShader = std::make_shared<Shader>("spherepano.vert", "spherepano.frag");

	skyboxShader = std::make_shared<Shader>("skybox.vert", "skybox.frag");

	cubeModel = std::make_shared<Model>(FileSystem::getPath("resources/meshes/cube.obj").c_str());
	quadModel = std::make_shared<Model>(FileSystem::getPath("resources/meshes/quad.obj").c_str());
	arrowModel = std::make_shared<Model>(FileSystem::getPath("resources/meshes/arrow.obj").c_str());

	panoSphereModel = Model::CreatePanoramaSphere(1.0f, 128, 64);
	panoSphereTexture = std::make_shared<GLTexture>("panoSphereTexture");
	panoSphereTexture->load(FileSystem::getPath("resources/panos/SAM_Sample_0002.jpg"));

	skyboxModel = std::make_shared<Model>(FileSystem::getPath("resources/meshes/cube.obj").c_str());

	frustumModel = Model::CreateFrustum(locatableCameraProfile.HfovDegrees, (float)locatableCameraProfile.Width / (float)locatableCameraProfile.Height, 0.25f);

	string scanLabel = currentScan.Label;



	std::string detectedPointFilepath = string_format("resources/scans/%s/%s_detected_point.csv", scanLabel.c_str(), scanLabel.c_str());
	normalizedDetectedImagePoints = loadNormalizedDetectedImagePoints(FileSystem::getPath(detectedPointFilepath).c_str());






	std::string roomFilepath = string_format("resources/scans/%s/%s_mesh.room", scanLabel.c_str(), scanLabel.c_str());
	cout << "roomFilepath: " << roomFilepath << endl;

	testHoloRoom = Model::LoadHoloRoom(FileSystem::getPath(roomFilepath).c_str());
	//testHoloRoom->GenerateAABBTree();

	std::string pathFilepath = string_format("resources/scans/%s/%s_pose.csv", scanLabel.c_str(), scanLabel.c_str());
	cout << "pathFilepath: " << pathFilepath << endl;

	testPath = Path::LoadHoloPath(locatableCamera, FileSystem::getPath(pathFilepath).c_str(), currentScan.NumHLFrames);

	initialHeadY = testPath->poses[0].Position.y;

	GL_CHECK(std::cout << "done initing" << std::endl);
}

std::string getFramePath(int frameIndex) {
	auto frameLabel = string_format("frame%05d", frameIndex + 1); // frame images are 1-indexed
	auto framePath = FileSystem::getPath(string_format("resources/scans/%s/frames/%s.jpg", currentScan.Label.c_str(), frameLabel.c_str()));
	return framePath;
}

std::string getThumbPath(int thumbIndex) {
	auto thumbLabel = string_format("thumb%05d", thumbIndex + 1); // frame images are 1-indexed
	auto thumbPath = FileSystem::getPath(string_format("resources/scans/%s/thumbs/%s.jpg", currentScan.Label.c_str(), thumbLabel.c_str()));
	return thumbPath;
}

std::string getPanoFramePath(int frameIndex) {
	auto frameLabel = string_format("pano_frame%05d", frameIndex + 1); // frame images are 1-indexed
	auto framePath = FileSystem::getPath(string_format("resources/scans/%s/pano_frames/%s.jpg", currentScan.Label.c_str(), frameLabel.c_str()));
	return framePath;
}

std::string getPanoThumbPath(int thumbIndex) {
	auto thumbLabel = string_format("pano_thumb%05d", thumbIndex + 1); // frame images are 1-indexed
	auto thumbPath = FileSystem::getPath(string_format("resources/scans/%s/pano_thumbs/%s.jpg", currentScan.Label.c_str(), thumbLabel.c_str()));
	return thumbPath;
}


void setCurrentFrameTexture() {

	auto forwardFrameLabel = string_format("frame%05d", mCurrentForwardFrameIndex + 1); // frame images are 1-indexed
	auto forwardThumbLabel = string_format("thumb%05d", mCurrentForwardFrameIndex + 1); // frame images are 1-indexed
	
	GLTexture forwardFrameTexture(forwardFrameLabel);
	GLTexture forwardThumbTexture(forwardThumbLabel);
	
	auto forwardFramePath = getFramePath(mCurrentForwardFrameIndex);
	auto forwardThumbPath = getThumbPath(mCurrentForwardFrameIndex);
	
	auto forwardFrameData = forwardFrameTexture.load(forwardFramePath);
	auto forwardThumbData = forwardThumbTexture.load(forwardThumbPath);
	
	mNearestForwardFrameImageData.emplace_back(std::move(forwardFrameTexture), std::move(forwardFrameData));
	mNearestForwardThumbImageData.emplace_back(std::move(forwardThumbTexture), std::move(forwardThumbData));
}



void drawAxes(glm::mat4x4 &model, float size, glm::vec3 centerObjColor) {
	meshShader->use();

	float longSize = 1.0f * size;
	float narrowSize = 0.1f * size;

	glm::mat4 xModelMatrix(model);
	xModelMatrix = glm::translate(xModelMatrix, glm::vec3(longSize / 2, 0, 0));
	xModelMatrix = glm::scale(xModelMatrix, glm::vec3(longSize, narrowSize, narrowSize));

	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(xModelMatrix));
	glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 0.0f, 0.0f);
	cubeModel->Draw(meshShader);

	glm::mat4 yModelMatrix(model);
	yModelMatrix = glm::translate(yModelMatrix, glm::vec3(0, longSize / 2, 0));
	yModelMatrix = glm::scale(yModelMatrix, glm::vec3(narrowSize, longSize, narrowSize));

	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(yModelMatrix));
	glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.0f, 1.0f, 0.0f);
	cubeModel->Draw(meshShader);

	glm::mat4 zModelMatrix(model);
	zModelMatrix = glm::translate(zModelMatrix, glm::vec3(0, 0, longSize / 2));
	zModelMatrix = glm::scale(zModelMatrix, glm::vec3(narrowSize, narrowSize, longSize));

	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(zModelMatrix));
	glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.0f, 0.0f, 1.0f);
	cubeModel->Draw(meshShader);

	glm::mat4 centerModelMatrix(model);
	centerModelMatrix = glm::scale(centerModelMatrix, glm::vec3(narrowSize * 2, narrowSize * 2, narrowSize * 2));
	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(centerModelMatrix));
	glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), centerObjColor.x, centerObjColor.y, centerObjColor.z);
	cubeModel->Draw(meshShader);
}




void drawRoom(glm::mat4x4 &view, glm::mat4x4 &proj) {

	meshShader->use();

	glUniform3f(glGetUniformLocation(meshShader->Program, "lightPos"), thirdPersonCamera.Position.x, thirdPersonCamera.Position.y, thirdPersonCamera.Position.z);

	glUniform3f(glGetUniformLocation(meshShader->Program, "lightColor"), 1.0f, 1.0f, 1.0f);

	glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.75f, 0.75f, 0.75f);

	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(proj));
	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(view));

	//==================

	// draw the room
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);

	glm::mat4 modelIdentity;
	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(modelIdentity));
	testHoloRoom->Draw(meshShader);

	glDisable(GL_CULL_FACE);
	glCullFace(GL_BACK);
}


int getLocatableCameraFrameIndexFromPose(Pose p) {
	return (int)round(p.PoseIndex);
}

// given a particular index in the pose list, and a desired timestamp offset...
// select an adjacent pose and do interpolation and return the generated pose
Pose getPoseFromIndex(size_t index, float timestampOffset) {
	//std::cout << "getPoseFromIndex " << index << " with offset " << timestampOffset << std::endl;
	if (timestampOffset == 0.0f) {
		return testPath->poses[index];	// no interpolation
	}

	if (index == 0 && timestampOffset < 0) {
		// trying to go before the beginning
		return testPath->poses[index];
	}

	if (index == testPath->poses.size() - 1 && timestampOffset > 0) {
		// trying to go after the end
		return testPath->poses[index];
	}

	Pose currentPose = testPath->poses[index];

	float desiredTimestamp = currentPose.Timestamp + timestampOffset;

	if (timestampOffset < 0) {
		//std::cout << "before" << std::endl;
		size_t prevPoseIndex = index - 1;

		bool prevPoseFound = false;
		while (!prevPoseFound) {
			if (prevPoseIndex < 0) {
				return testPath->poses[index];
			}

			if (testPath->poses[prevPoseIndex].Timestamp < desiredTimestamp) {
				prevPoseFound = true;
			}
			else {
				prevPoseIndex--;
			}
		}

		Pose prevPose = testPath->poses[prevPoseIndex];
		float t = (desiredTimestamp - prevPose.Timestamp) / (currentPose.Timestamp - prevPose.Timestamp);
		//std::cout << "t: " << t << std::endl;

		glm::vec3 interpolatedPos = glm::mix(prevPose.Position, currentPose.Position, t);
		glm::quat interpolatedRotQuat = glm::slerp(prevPose.RotationQuaternion, currentPose.RotationQuaternion, t);
		glm::mat4 interpolatedRotMat(interpolatedRotQuat);

		float interpolatedPoseIndex = glm::mix(prevPose.PoseIndex, currentPose.PoseIndex, t);

		Pose interpolatedPose(locatableCamera, desiredTimestamp, interpolatedPos, interpolatedRotQuat, interpolatedRotMat, interpolatedPoseIndex);
		return interpolatedPose;
	}
	else {
		//std::cout << "after" << std::endl;
		// timestamp > 0

		size_t nextPoseIndex = index + 1;

		bool nextPoseFound = false;
		while (!nextPoseFound) {
			if (nextPoseIndex > testPath->poses.size() - 1) {
				return testPath->poses[index];
			}

			if (testPath->poses[nextPoseIndex].Timestamp > desiredTimestamp) {
				nextPoseFound = true;
			}
			else {
				nextPoseIndex++;
			}
		}

		Pose nextPose = testPath->poses[nextPoseIndex];
		float t = (desiredTimestamp - currentPose.Timestamp) / (nextPose.Timestamp - currentPose.Timestamp);
		//std::cout << "t: " << t << std::endl;

		glm::vec3 interpolatedPos = glm::mix(currentPose.Position, nextPose.Position, t);
		glm::quat interpolatedRotQuat = glm::slerp(currentPose.RotationQuaternion, nextPose.RotationQuaternion, t);
		glm::mat4 interpolatedRotMat(interpolatedRotQuat);

		float interpolatedPoseIndex = glm::mix(currentPose.PoseIndex, nextPose.PoseIndex, t);

		Pose interpolatedPose(locatableCamera, desiredTimestamp, interpolatedPos, interpolatedRotQuat, interpolatedRotMat, interpolatedPoseIndex);
		return interpolatedPose;
	}
}

void draw() {
	frameNumber++;

	float cameraPathProgress = cameraPathSlider->value();
	int cameraPoseIndex = glm::clamp((int)(testPath->poses.size() * cameraPathProgress), 0, (int)(testPath->poses.size() - 1));
	auto cameraPose = testPath->poses[cameraPoseIndex];

	float cellCenterX = glm::round(visualizationCamera.Position.x / gridSizeMeters) * gridSizeMeters;
	float cellCenterZ = glm::round(visualizationCamera.Position.z / gridSizeMeters) * gridSizeMeters;

	glm::vec3 cellCenterPosition(cellCenterX, initialHeadY, cellCenterZ);

	glm::vec3 cellLookupPosition = cellCenterLookup ? cellCenterPosition : visualizationCamera.Position;

	size_t nearestForwardPoseIndex = testPath->findMostSimilarNeighborIndex(cellLookupPosition, visualizationCamera.Front, directionWeight, gridSizeMeters);
	Pose nearestForwardPose = getPoseFromIndex(nearestForwardPoseIndex, poseTimestampOffset);


	// find "numLayers" nearest poses, each pointed in a different direction from the cell lookup position. 
	std::vector<Pose> nearestPoses;

	float degreesPerViewLookup = 90.0f / numTextureArrayLayers;
	float forwardCameraYawDegrees = visualizationCamera.Yaw;
	for (int i = 0; i < numTextureArrayLayers; i++) {
		float yawDegrees = forwardCameraYawDegrees + ((i - numTextureArrayLayers / 2)*degreesPerViewLookup);
		float yawRadians = glm::radians(yawDegrees);
		float pitchRadians = 0.0f; // sampled locations are always horizontal

								   // calculate a new front vector
		glm::vec3 front;
		front.x = cos(yawRadians) * cos(pitchRadians);
		front.y = sin(pitchRadians);
		front.z = sin(yawRadians) * cos(pitchRadians);
		front = glm::normalize(front);

		size_t nearestPoseIndex = testPath->findMostSimilarNeighborIndex(cellLookupPosition, front, directionWeight, gridSizeMeters);
		nearestPoses.push_back(getPoseFromIndex(nearestPoseIndex, poseTimestampOffset));
	}

	//==================

	GLuint nearestForwardFrameTexture;
	GLuint nearestForwardThumbTexture;

	if (renderingSimulatedView) {
		
		glm::mat4 simulatedCameraProj = glm::perspective(locatableCameraFovYRadians, locatableCameraAspectRatio, 0.1f, 100.0f);

		// first just render a front view to use as preview
		simulatedViewFramebuffer->preRender(false);

		glm::mat4 nearestFrontPoseView = glm::inverse(nearestForwardPose.ModelMatrix);

		drawRoom(nearestFrontPoseView, simulatedCameraProj);

		simulatedViewFramebuffer->postRender();

		nearestForwardFrameTexture = simulatedViewFramebuffer->texture2D();
		nearestForwardThumbTexture = simulatedViewFramebuffer->texture2D();

		mNearestForwardThumbImageView->bindImage(nearestForwardThumbTexture);

		//----------

		if (usingTextureArray) {
			// now render the views for each of the poses
			for (int i = 0; i < numTextureArrayLayers; i++) {
				simulatedViewFramebuffer->preRender(true, i);
				glm::mat4 nearestPoseView = glm::inverse(nearestPoses[i].ModelMatrix);
				drawRoom(nearestPoseView, simulatedCameraProj);
				simulatedViewFramebuffer->postRender();
			}
		}

	}
	else {

		// find the camera frame that most closely matches it temporally
		int newForwardFrameIndex = getLocatableCameraFrameIndexFromPose(nearestForwardPose);

		if (newForwardFrameIndex != mCurrentForwardFrameIndex) {
			mCurrentForwardFrameIndex = newForwardFrameIndex;
			mNearestForwardThumbImageData.clear();
			mNearestForwardFrameImageData.clear();
			setCurrentFrameTexture();
		}

		nearestForwardFrameTexture = mNearestForwardFrameImageData[0].first.texture();
		nearestForwardThumbTexture = mNearestForwardThumbImageData[0].first.texture();

		mNearestForwardThumbImageView->bindImage(nearestForwardThumbTexture);

		if (usingTextureArray) {
			// load in each of the real-world frame textures into the texture array
			for (int i = 0; i < numTextureArrayLayers; i++) {
				int frameIndex = getLocatableCameraFrameIndexFromPose(nearestPoses[i]);

				if (mCurrentFrameIndices[i] != frameIndex) {
					mCurrentFrameIndices[i] = frameIndex;

					std::string framePath = getFramePath(mCurrentFrameIndices[i]);

					int w, h, n;
					int force_channels = 4;
					GLTexture::handleType textureData(stbi_load(framePath.c_str(), &w, &h, &n, force_channels), stbi_image_free);

					if (!textureData) {
						throw std::invalid_argument("Could not load texture data from file " + framePath);
					}

					GL_CHECK(glBindTexture(GL_TEXTURE_2D_ARRAY, simulatedViewFramebuffer->texture2DArray()));
					GL_CHECK(glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, i, w, h, 1, GL_RGBA, GL_UNSIGNED_BYTE, textureData.get()));
				}
			}
		}
	}

	//==================

	std::vector<glm::vec3> detectedRayStarts;
	std::vector<glm::vec3> detectedRayEnds;

	if (usingTextureArray) {
		// calculate the rays going to the tracked image point
		if (normalizedDetectedImagePoints.size() > 0) {

			int width = locatableCameraProfile.Width;
			int height = locatableCameraProfile.Height;

			for (int i = 0; i < numTextureArrayLayers; i++) {

				auto & nearestPose = nearestPoses[i];

				int poseIndex = (int)nearestPose.PoseIndex;

				if (poseIndex > 0 && poseIndex < normalizedDetectedImagePoints.size()) {
					// http://antongerdelan.net/opengl/raycasting.html
					glm::vec2 imagePoint = normalizedDetectedImagePoints[poseIndex];

					// if the point wasn't detected in the image, we marked it "-1,-1"
					if (imagePoint.x >= 0 && imagePoint.y >= 0) {
						// get the "mouse" coordinates by taking the normalized coords and putting them into the resolution of the locatable camera
						float mouse_x = imagePoint.x * width;
						float mouse_y = imagePoint.y * height;

						// get normalized device coordinates
						float x = (2.0f * mouse_x) / width - 1.0f;
						float y = 1.0f - (2.0f * mouse_y) / height;
						float z = 1.0f;
						glm::vec3 ray_nds(x, y, z);

						// get 4d homogeneous clip coordinates
						glm::vec4 ray_clip(ray_nds.x, ray_nds.y, -1.0f, 1.0f);

						// 4d eye (camera) coordinates
						glm::vec4 ray_eye = inverseLocatableCameraProjection * ray_clip;

						// now only unproject the xy part
						ray_eye = glm::vec4(ray_eye.x, ray_eye.y, -1.0, 0.0);

						// 4d world coordinates

						glm::mat4 viewMatrix = nearestPose.ModelMatrix;

						glm::vec3 ray_wor = viewMatrix * ray_eye;
						ray_wor = glm::normalize(ray_wor);


						detectedRayStarts.push_back(nearestPose.Position);
						detectedRayEnds.push_back(nearestPose.Position + ray_wor);
					}
				}
			}
		}
	}



	//==================

	glm::mat4 thirdPersonCameraProjection = glm::perspective(thirdPersonCamera.Zoom, (float)screenWidth / (float)screenHeight, 0.1f, 100.0f);
	glm::mat4 thirdPersonCameraView = thirdPersonCamera.GetViewMatrix();


	if (showingSkybox) {
		skyboxShader->use();

		glm::mat4 visualizationCameraProjection = glm::perspective(visualizationCamera.Zoom, (float)screenWidth / (float)screenHeight, 0.1f, 100.0f);

		glm::vec3 visualizationCameraPosition = visualizationCamera.Position;
		//glm::vec3 visualizationCameraPosition = nearestForwardPose.Position;

		glm::mat4 visualizationCameraView = visualizationCamera.GetViewMatrix(visualizationCameraPosition);

		glUniformMatrix4fv(glGetUniformLocation(skyboxShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(visualizationCameraProjection));
		glUniformMatrix4fv(glGetUniformLocation(skyboxShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(visualizationCameraView));

		glUniform1i(glGetUniformLocation(skyboxShader->Program, "renderingSimulatedView"), renderingSimulatedView ? 1 : 0);


		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		glm::mat4 skyboxModelMatrix(1.0f);
		skyboxModelMatrix = glm::translate(skyboxModelMatrix, visualizationCameraPosition);
		skyboxModelMatrix = glm::scale(skyboxModelMatrix, glm::vec3(skyboxScale, skyboxScale, skyboxScale));
		glUniformMatrix4fv(glGetUniformLocation(skyboxShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(skyboxModelMatrix));

		
		glm::mat4 frontLocatableCameraWorldToLocal = glm::inverse(nearestForwardPose.ModelMatrix);

		glUniformMatrix4fv(glGetUniformLocation(skyboxShader->Program, "locatableCameraProjection"), 1, GL_FALSE, glm::value_ptr(locatableCameraProjection));
		glUniformMatrix4fv(glGetUniformLocation(skyboxShader->Program, "locatableCameraWorldToLocal"), 1, GL_FALSE, glm::value_ptr(frontLocatableCameraWorldToLocal));

		if (usingTextureArray) {
			int textureUnit = 1;	// this is because we can't have the sampler2d and the sampler2darray set to the same unit: https://www.opengl.org/discussion_boards/archive/index.php/t-183706.html
			glActiveTexture(GL_TEXTURE0 + textureUnit);
			glBindTexture(GL_TEXTURE_2D_ARRAY, simulatedViewFramebuffer->texture2DArray());
			glUniform1i(glGetUniformLocation(skyboxShader->Program, "usingTexArray"), 1);
			glUniform1i(glGetUniformLocation(skyboxShader->Program, "texArray"), textureUnit);

			for (int i = 0; i < numTextureArrayLayers; i++) {
				glm::mat4 locatableCameraWorldToLocal = glm::inverse(nearestPoses[i].ModelMatrix);

				std::string uniformName = "locatableCameraWorldToLocalArray[" + std::to_string(i) + "]";

				glUniformMatrix4fv(glGetUniformLocation(skyboxShader->Program, uniformName.c_str()), 1, GL_FALSE, glm::value_ptr(locatableCameraWorldToLocal));
			}
		}
		else {
			int textureUnit = 0;
			glActiveTexture(GL_TEXTURE0 + textureUnit);
			glBindTexture(GL_TEXTURE_2D, nearestForwardFrameTexture);
			glUniform1i(glGetUniformLocation(skyboxShader->Program, "usingTexArray"), 0);
			glUniform1i(glGetUniformLocation(skyboxShader->Program, "tex"), textureUnit);
		}

		skyboxModel->Draw(skyboxShader);

		glDisable(GL_CULL_FACE);
		glCullFace(GL_BACK);


	}
	else {
		meshShader->use();

		glUniform3f(glGetUniformLocation(meshShader->Program, "lightPos"), thirdPersonCamera.Position.x, thirdPersonCamera.Position.y, thirdPersonCamera.Position.z);

		glUniform3f(glGetUniformLocation(meshShader->Program, "lightColor"), 1.0f, 1.0f, 1.0f);
		glUniform1i(glGetUniformLocation(meshShader->Program, "lightingEnabled"), true);
		glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.75f, 0.75f, 0.75f);

		glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(thirdPersonCameraProjection));
		glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(thirdPersonCameraView));

		//==================

		// draw the room
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		glm::mat4 modelIdentity;
		glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(modelIdentity));
		testHoloRoom->Draw(meshShader);

		glDisable(GL_CULL_FACE);
		glCullFace(GL_BACK);

		//==================

		// draw the frustum in the place that matches the current pose

		glm::mat4 frustumModelMatrix = testPath->poses[cameraPoseIndex].ModelMatrix;

		glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 1.0f, 0.0f);
		glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(frustumModelMatrix));
		frustumModel->Draw(meshShader);

		//==================

		// draw where the visualization camera is

		glm::mat4 visualizationCameraModelMatrix = glm::inverse(visualizationCamera.GetViewMatrix());

		glm::mat4 visualizationCameraScaledMatrix = glm::scale(visualizationCameraModelMatrix, glm::vec3(0.5, 0.5, 0.5));

		glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(visualizationCameraScaledMatrix));
		glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 1.0f, 0.5f);
		arrowModel->Draw(meshShader);

		//==================

		// find the nearest location on the path to the current (virtual) camera and place a marker object there

		glm::mat4 nearestForwardPoseModelMatrix = nearestForwardPose.ModelMatrix;

		glm::mat4 nearestForwardScaledMatrix = glm::scale(nearestForwardPoseModelMatrix, glm::vec3(0.25, 0.25, 0.25));

		glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(nearestForwardScaledMatrix));
		glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.5f, 0.5f, 1.0f);
		arrowModel->Draw(meshShader);

		// draw each of the other suggested poses too
		for (int i = 0; i < numTextureArrayLayers; i++) {
			glm::mat4 nearestPoseModelMatrix = nearestPoses[i].ModelMatrix;
			nearestPoseModelMatrix = glm::scale(nearestPoseModelMatrix, glm::vec3(0.1, 0.1, 0.1));

			glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(nearestPoseModelMatrix));
			glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.25f, 0.25f, 0.75f);
			arrowModel->Draw(meshShader);
		}

		//==================

		if (detectedRayStarts.size() > 0) {

			glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 0.25f, 0.0f);

			for (int detectedRayIdx = 0; detectedRayIdx < detectedRayStarts.size(); detectedRayIdx++) {

				glm::mat4 rayModelMatrix = glm::inverse(glm::lookAt(detectedRayStarts[detectedRayIdx], detectedRayEnds[detectedRayIdx], glm::vec3(0, 1, 0)));
				rayModelMatrix = glm::scale(rayModelMatrix, glm::vec3(0.01, 0.01, 3.0));

				glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(rayModelMatrix));
				arrowModel->Draw(meshShader);
			}
		}



		//==================

		// draw the path

		pathShader->use();

		glUniformMatrix4fv(glGetUniformLocation(pathShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(thirdPersonCameraProjection));
		glUniformMatrix4fv(glGetUniformLocation(pathShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(thirdPersonCameraView));
		glUniformMatrix4fv(glGetUniformLocation(pathShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(modelIdentity));

		float endTimestamp = testPath->poses.back().Timestamp;
		glUniform1f(glGetUniformLocation(pathShader->Program, "endTimestamp"), endTimestamp);

		testPath->Draw(pathShader, 0, cameraPoseIndex);





		


	}




	
}






// Moves/alters the camera positions based on user input
void doMovement()
{
	Camera* camera = controllingVisualizationCamera ? &visualizationCamera : &thirdPersonCamera;

	bool forceHorizontalMotion = controllingVisualizationCamera;
	//bool forceHorizontalMotion = false;

	// Camera controls
	if (keys[GLFW_KEY_W])
		camera->ProcessKeyboard(FORWARD, deltaTime, forceHorizontalMotion);
	if (keys[GLFW_KEY_S])
		camera->ProcessKeyboard(BACKWARD, deltaTime, forceHorizontalMotion);
	if (keys[GLFW_KEY_A])
		camera->ProcessKeyboard(LEFT, deltaTime, forceHorizontalMotion);
	if (keys[GLFW_KEY_D])
		camera->ProcessKeyboard(RIGHT, deltaTime, forceHorizontalMotion);
	if (keys[GLFW_KEY_Q])
		camera->ProcessKeyboard(DOWN, deltaTime, forceHorizontalMotion);
	if (keys[GLFW_KEY_E])
		camera->ProcessKeyboard(UP, deltaTime, forceHorizontalMotion);

	if (controllingVisualizationCamera) {
		// make sure the visualization camera is always set at the default height
		camera->Position.y = initialHeadY;
	}
}










int main(int /* argc */, char ** /* argv */) {

    glfwInit();

    glfwSetTime(0);

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    glfwWindowHint(GLFW_SAMPLES, 0);
    glfwWindowHint(GLFW_RED_BITS, 8);
    glfwWindowHint(GLFW_GREEN_BITS, 8);
    glfwWindowHint(GLFW_BLUE_BITS, 8);
    glfwWindowHint(GLFW_ALPHA_BITS, 8);
    glfwWindowHint(GLFW_STENCIL_BITS, 8);
    glfwWindowHint(GLFW_DEPTH_BITS, 24);
    glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

    // Create a GLFWwindow object
    window = glfwCreateWindow(screenWidth, screenHeight, "example3", nullptr, nullptr);
    if (window == nullptr) {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

#if defined(NANOGUI_GLAD)
    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress))
        throw std::runtime_error("Could not initialize GLAD!");
    glGetError(); // pull and ignore unhandled errors like GL_INVALID_ENUM
#endif

    glClearColor(0.2f, 0.25f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Create a nanogui screen and pass the glfw pointer to initialize
    screen = new Screen();
    screen->initialize(window, true);

    glfwGetFramebufferSize(window, &screenWidth, &screenHeight);
    glViewport(0, 0, screenWidth, screenHeight);
    glfwSwapInterval(0);
    glfwSwapBuffers(window);


    // Create nanogui gui
    bool enabled = true;
    FormHelper *gui = new FormHelper(screen);
	nanogui::ref<Window> nanoguiWindow = gui->addWindow(Eigen::Vector2i(10, 10), "Form helper example");

	/*
    gui->addGroup("Basic types");
    gui->addVariable("bool", bvar)->setTooltip("Test tooltip.");
    gui->addVariable("string", strval);

    gui->addVariable("int", ivar)->setSpinnable(true);
    gui->addVariable("float", fvar)->setTooltip("Test.");
    gui->addVariable("double", dvar)->setSpinnable(true);

    gui->addGroup("Complex types");
    gui->addVariable("Enumeration", enumval, enabled)->setItems({ "Item 1", "Item 2", "Item 3" });
    gui->addVariable("Color", colval);

    gui->addGroup("Other widgets");
    gui->addButton("A button", []() { std::cout << "Button pressed." << std::endl; })->setTooltip("Testing a much longer tooltip, that will wrap around to new lines multiple times.");;
	*/

	gui->addGroup("Synthetic viewpoint");
	gui->addVariable("Showing Skybox", showingSkybox)->setTooltip("Showing Skybox");

	gui->addVariable("cellCenterLookup", cellCenterLookup)->setTooltip("cellCenterLookup");
	
	gui->addVariable("poseIndexOffset", poseIndexOffset)->setSpinnable(true);

	gui->addVariable("controllingVisualizationCamera", controllingVisualizationCamera);
	
	gui->addVariable("poseTimestampOffset", poseTimestampOffset);

	gui->addVariable("directionWeight", directionWeight);

	gui->addVariable("renderingSimulatedView", renderingSimulatedView);

	gui->addVariable("skyboxScale", skyboxScale)->setSpinnable(true);

	gui->addVariable("printingInfo", printingInfo)->setTooltip("printingInfo");

	
	gui->addGroup("Visualization");

	Widget* panel = new Widget(nanoguiWindow);
	panel->setLayout(new BoxLayout(Orientation::Horizontal,
		Alignment::Middle, 0, 20));

	cameraPathSlider = new Slider(panel);
	cameraPathSlider->setValue(0.0f);
	cameraPathSlider->setFixedWidth(160);


	gui->addWidget("Camera Path", panel);


    screen->setVisible(true);
    screen->performLayout();
    //nanoguiWindow->center();



	nanogui::ref<Window> nearestThumbWindow = gui->addWindow(Eigen::Vector2i(700, 10), "Nearest Frame");
	nearestThumbWindow->setLayout(new GroupLayout());

	mCurrentForwardFrameIndex = 0;
	setCurrentFrameTexture();


	mNearestForwardThumbImageView = new ImageView(nearestThumbWindow, mNearestForwardThumbImageData[0].first.texture());
	

	screen->setVisible(true);
	screen->performLayout();




    glfwSetCursorPosCallback(window,
            [](GLFWwindow *, double x, double y) {
            bool cursorPosConsumedByUI = screen->cursorPosCallbackEvent(x, y);
			if (!cursorPosConsumedByUI) {
				// use mouse movement for camera movement

				if (firstMouse)
				{
					lastX = x;
					lastY = y;
					firstMouse = false;
				}

				GLfloat xoffset = x - lastX;
				GLfloat yoffset = lastY - y;

				lastX = x;
				lastY = y;

				if (cameraMouseControlsActive) {
					if (controllingVisualizationCamera) {
						visualizationCamera.ProcessMouseMovement(xoffset, yoffset);
					}
					else {
						thirdPersonCamera.ProcessMouseMovement(xoffset, yoffset);
					}
					
				}
			}
        }
    );

    glfwSetMouseButtonCallback(window,
        [](GLFWwindow *, int button, int action, int modifiers) {
            bool mouseButtonConsumedByUI = screen->mouseButtonCallbackEvent(button, action, modifiers);
			if (!mouseButtonConsumedByUI) {

				if (button == GLFW_MOUSE_BUTTON_RIGHT) {
					if (action == GLFW_PRESS) {
						cameraMouseControlsActive = true;
					}
					else if (action == GLFW_RELEASE) {
						cameraMouseControlsActive = false;
					}
				}
			}
        }
    );

    glfwSetKeyCallback(window,
        [](GLFWwindow *, int key, int scancode, int action, int mods) {
            bool keyConsumedByUI = screen->keyCallbackEvent(key, scancode, action, mods);
			if (!keyConsumedByUI) {
				// treat the key input as camera controls

				if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
					glfwSetWindowShouldClose(window, GL_TRUE);

				if (action == GLFW_PRESS)
					keys[key] = true;
				else if (action == GLFW_RELEASE)
					keys[key] = false;
			}
        }
    );

    glfwSetCharCallback(window,
        [](GLFWwindow *, unsigned int codepoint) {
            screen->charCallbackEvent(codepoint);
        }
    );

    glfwSetDropCallback(window,
        [](GLFWwindow *, int count, const char **filenames) {
            screen->dropCallbackEvent(count, filenames);
        }
    );

    glfwSetScrollCallback(window,
        [](GLFWwindow *, double x, double y) {
            screen->scrollCallbackEvent(x, y);
       }
    );

    glfwSetFramebufferSizeCallback(window,
        [](GLFWwindow *, int width, int height) {
			screenWidth = width;
			screenHeight = height;
            screen->resizeCallbackEvent(screenWidth, screenHeight);

			glViewport(0, 0, screenWidth, screenHeight);
        }
    );

	initShader();

    // Game loop
    while (!glfwWindowShouldClose(window)) {
		// Set frame time
		GLfloat currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

        // Check if any events have been activated (key pressed, mouse moved etc.) and call corresponding response functions
        glfwPollEvents();
		doMovement();

		// note that we have to enable the depth test per-frame; it looks like nanogui disables it in order to draw the UI
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);


        glClearColor(0.2f, 0.25f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		draw();

        // Draw nanogui
        screen->drawContents();
        screen->drawWidgets();

        glfwSwapBuffers(window);
    }

    // Terminate GLFW, clearing any resources allocated by GLFW.
    glfwTerminate();

    return 0;
}
