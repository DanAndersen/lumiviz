#pragma once

#include "common.hpp"

class Framebuffer {
public:
	Framebuffer(int w, int h, ImageFormat imageFormat, GLenum target = GL_TEXTURE_2D, GLenum filter = GL_LINEAR, bool textureArray = false, int numLayers = 1);

	GLuint texture2D();
	GLuint texture2DArray();
	GLuint framebufferId();

	void preRender(bool usingTextureArray = false, int layerIdx = 0);

	void postRender();

	bool isUsingTextureArray();
	int getNumLayers();
	int getWidth();
	int getHeight();
private:
	GLuint framebufferName = 0;
	GLuint renderedTexture2D;
	GLuint renderedTexture2DArray;
	GLuint depthRenderbuffer;
	int width;
	int height;

	GLint previousViewport[4];
	bool usingTextureArray;
	int numLayers;
};