#pragma once

#include "common.hpp"

class Cubemap {
public:
	Cubemap(int size, ImageFormat imageFormat, GLenum filter);

	void preRender();
	void postRender();

	void PreFace(int faceIdx);
	void PostFace(int faceIdx);

	GLuint textureCubemap();

	GLuint framebufferName = 0;

	GLuint renderedTextureCubemap;

	int GetCubemapSize();

private:
	int size;
	
	
	GLuint depthRenderbuffer;

	GLint previousViewport[4];
};