#pragma once

#include "common.hpp"
#include "shader.hpp"

#include <vector>

using namespace std;

class Mesh {
public:
	vector<Vertex> vertices;
	vector<GLuint> indices;

	Mesh(vector<Vertex> vertices, vector<GLuint> indices);

	void Draw(std::shared_ptr<Shader> shader);

	void rebufferVertices();
	void rebufferIndices();

private:

	GLuint VAO, VBO, EBO;

	void setupMesh();
};