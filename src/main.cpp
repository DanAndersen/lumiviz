// GLFW
//
#if defined(NANOGUI_GLAD)
    #if defined(NANOGUI_SHARED) && !defined(GLAD_GLAPI_EXPORT)
        #define GLAD_GLAPI_EXPORT
    #endif

    #include <glad/glad.h>
#else
    #if defined(__APPLE__)
        #define GLFW_INCLUDE_GLCOREARB
    #else
        #define GL_GLEXT_PROTOTYPES
    #endif
#endif

#include <GLFW/glfw3.h>



#include <nanogui/nanogui.h>
#include <iostream>
#include <memory>

#include "common.hpp"

#include "shader.hpp"
#include "mesh.hpp"
#include "model.hpp"

#include "filesystem.h"

#include "camera.hpp"

#include "path.hpp"

#include <opencv2/opencv.hpp>

#include "gltexture.hpp"

#include "framebuffer.hpp"

#include <stb_image.h>

using namespace nanogui;

int screenWidth = 800;
int screenHeight = 800;

enum test_enum {
    Item1 = 0,
    Item2,
    Item3
};

bool bvar = true;
int ivar = 12345678;
double dvar = 3.1415926;
float fvar = (float)dvar;
std::string strval = "A string";
test_enum enumval = Item2;
Color colval(0.5f, 0.5f, 0.7f, 1.f);

Screen *screen = nullptr;

std::shared_ptr<Shader> meshShader;
std::shared_ptr<Shader> pathShader;
std::shared_ptr<Shader> spherePanoShader;
//std::shared_ptr<Mesh> testMesh;
std::shared_ptr<Model> frustumModel;
std::shared_ptr<Model> cubeModel;
std::shared_ptr<Model> quadModel;
std::shared_ptr<Model> testHoloRoom;
std::shared_ptr<Model> testHoloRoomWalkableSpace;

std::shared_ptr<Model> arrowModel;

std::shared_ptr<Model> panoSphereModel;
std::shared_ptr<GLTexture> panoSphereTexture;

std::shared_ptr<Path> testPath;
std::shared_ptr<ScoreGrid> scoreGrid;
std::shared_ptr<TravelGrid> travelGrid;

Camera thirdPersonCamera(glm::vec3(0.0f, 0.0f, 3.0f)); // camera for moving around the scene
Camera visualizationCamera(glm::vec3(0.0f, 0.0f, 3.0f)); // camera for viewing the scene from predetermined locations
bool controllingVisualizationCamera = false;

glm::vec3 lightPos(1.0f, 2.0f, 3.0f);

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

GLFWwindow* window;
bool keys[1024];

GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;

bool cameraMouseControlsActive = false;

float locatableCameraHfovDegrees = 67.0f;
//int locatableCameraWidth = 1344;
//int locatableCameraHeight = 756;

int locatableCameraWidth = 1408;
int locatableCameraHeight = 792;

float locatableCameraAspectRatio = (float)locatableCameraWidth / (float)locatableCameraHeight;
float locatableCameraFovYRadians = glm::radians(locatableCameraHfovDegrees / locatableCameraAspectRatio);

float directionWeight = 0.0f;

float panoramaFOV = 60.0f;

bool renderingSimulatedView = false;

//ScanSession currentScan("20170411154653", 13627, 13646, 202, 86); // first recording with 360 camera, no calibration
ScanSession currentScan("20170413142827", 4148, 4158, 233, 126); // recording with 360 camera, included chessboard imagery -- better calibration

// http://askubuntu.com/questions/110264/how-to-find-frames-per-second-of-any-video-file
float sphereCameraFPS = 30000.f / 1001.f;
float locatableCameraFPS = 14985.f / 499.f;

LocatableCamera locatableCamera(locatableCameraHfovDegrees, locatableCameraAspectRatio, 0.01f, 100.0f);


std::shared_ptr<Framebuffer> simulatedViewFramebuffer;

bool showingPano = false;

Slider* cameraPathSlider;

int mCurrentForwardFrameIndex;
int mCurrentSphericalFrameIndex;

int poseIndexOffset = 0;

bool printingInfo = false;

using imagesDataType = vector<pair<GLTexture, GLTexture::handleType>>;
imagesDataType mNearestForwardFrameImageData;
imagesDataType mNearestForwardThumbImageData;
imagesDataType mNearestSphericalFrameImageData;
imagesDataType mNearestSphericalThumbImageData;

nanogui::ref<ImageView> mNearestForwardThumbImageView;

float initialHeadY;

bool cellCenterLookup = true;

float poseTimestampOffset = 0.0f;

// maps from [index of sphere camera frame] to [index in sphere cache that contains actively loaded GL texture for that frame]
std::unordered_map<int, int> sphereFrameToActiveSphereTextures;
std::unordered_map<int, int> activeSphereTexturesToFrames;	// inverse mapping
const int numTexturesInSphereCache = 128;
std::vector<std::shared_ptr<GLTexture>> sphereCache;
std::vector<int> sphereCacheLastFrameUsed; // for each texture in the sphere cache, tracks the frame number when it was last used

int frameNumber = 0;

glm::mat4 frontToSphereCameraRotation;
glm::vec3 frontToSphereCameraTranslation;




float testSphereRotationX = 0.0f;
float testSphereRotationY = 0.0f;
float testSphereRotationZ = 0.0f;


void initShader() {

	/*
	float testR[16] = {
		0.99017101f, -0.01183255f, 0.13936054f, 0.0f,
		0.02711746f, 0.99374865f, -0.10829711f, 0.0f,
		-0.13720791f, 0.11101177f, 0.98430197f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	};
	*/

	float testR[16] = {
		1,0,0,0,
		0,1,0,0,
		0,0,1,0,
		0,0,0,1
	};

	memcpy(glm::value_ptr(frontToSphereCameraRotation), testR, sizeof(testR));

	float testT[3] = { -0.03666035f, 0.15327981f, 0.20412343f };
	//float testT[3] = { 0,0,0 };
	memcpy(glm::value_ptr(frontToSphereCameraTranslation), testT, sizeof(testT));


	for (int i = 0; i < numTexturesInSphereCache; i++) {
		sphereCache.push_back(std::make_shared<GLTexture>());
		sphereCacheLastFrameUsed.push_back(-1);
	}

	simulatedViewFramebuffer = std::make_shared<Framebuffer>(locatableCameraWidth, locatableCameraHeight);

	meshShader = std::make_shared<Shader>("test.vert", "test.frag");
	pathShader = std::make_shared<Shader>("pathline.vert", "pathline.frag");
	spherePanoShader = std::make_shared<Shader>("spherepano.vert", "spherepano.frag");

	cubeModel = std::make_shared<Model>(FileSystem::getPath("resources/meshes/cube.obj").c_str());
	quadModel = std::make_shared<Model>(FileSystem::getPath("resources/meshes/quad.obj").c_str());
	arrowModel = std::make_shared<Model>(FileSystem::getPath("resources/meshes/arrow.obj").c_str());

	panoSphereModel = Model::CreatePanoramaSphere(1.0f, 128, 64);
	panoSphereTexture = std::make_shared<GLTexture>("panoSphereTexture");
	panoSphereTexture->load(FileSystem::getPath("resources/panos/SAM_Sample_0002.jpg"));

	frustumModel = Model::CreateFrustum(locatableCameraHfovDegrees, (float)locatableCameraWidth / (float)locatableCameraHeight, 0.25f);

	string scanLabel = currentScan.Label;

	std::string roomFilepath = string_format("resources/scans/%s/%s_mesh.room", scanLabel.c_str(), scanLabel.c_str());
	cout << "roomFilepath: " << roomFilepath << endl;

	testHoloRoom = Model::LoadHoloRoom(FileSystem::getPath(roomFilepath).c_str());
	//testHoloRoom->GenerateAABBTree();

	std::string pathFilepath = string_format("resources/scans/%s/%s_pose.csv", scanLabel.c_str(), scanLabel.c_str());
	cout << "pathFilepath: " << pathFilepath << endl;

	testPath = Path::LoadHoloPath(locatableCamera, FileSystem::getPath(pathFilepath).c_str(), frontToSphereCameraRotation, frontToSphereCameraTranslation);

	initialHeadY = testPath->poses[0].Position.y;

	GL_CHECK(std::cout << "done initing" << std::endl);
}

std::string getFramePath(int frameIndex) {
	auto frameLabel = string_format("frame%05d", frameIndex + 1); // frame images are 1-indexed
	auto framePath = FileSystem::getPath(string_format("resources/scans/%s/frames/%s.jpg", currentScan.Label.c_str(), frameLabel.c_str()));
	return framePath;
}

std::string getThumbPath(int thumbIndex) {
	auto thumbLabel = string_format("thumb%05d", thumbIndex + 1); // frame images are 1-indexed
	auto thumbPath = FileSystem::getPath(string_format("resources/scans/%s/thumbs/%s.jpg", currentScan.Label.c_str(), thumbLabel.c_str()));
	return thumbPath;
}

std::string getPanoFramePath(int frameIndex) {
	auto frameLabel = string_format("pano_frame%05d", frameIndex + 1); // frame images are 1-indexed
	auto framePath = FileSystem::getPath(string_format("resources/scans/%s/pano_frames/%s.jpg", currentScan.Label.c_str(), frameLabel.c_str()));
	return framePath;
}

std::string getPanoThumbPath(int thumbIndex) {
	auto thumbLabel = string_format("pano_thumb%05d", thumbIndex + 1); // frame images are 1-indexed
	auto thumbPath = FileSystem::getPath(string_format("resources/scans/%s/pano_thumbs/%s.jpg", currentScan.Label.c_str(), thumbLabel.c_str()));
	return thumbPath;
}


void setCurrentFrameTexture() {

	auto forwardFrameLabel = string_format("frame%05d", mCurrentForwardFrameIndex + 1); // frame images are 1-indexed
	auto forwardThumbLabel = string_format("thumb%05d", mCurrentForwardFrameIndex + 1); // frame images are 1-indexed
	auto sphericalFrameLabel = string_format("pano_frame%05d", mCurrentSphericalFrameIndex + 1); // frame images are 1-indexed
	auto sphericalThumbLabel = string_format("pano_thumb%05d", mCurrentSphericalFrameIndex + 1); // frame images are 1-indexed

	GLTexture forwardFrameTexture(forwardFrameLabel);
	GLTexture forwardThumbTexture(forwardThumbLabel);
	GLTexture sphericalFrameTexture(sphericalFrameLabel);
	GLTexture sphericalThumbTexture(sphericalThumbLabel);

	auto forwardFramePath = getFramePath(mCurrentForwardFrameIndex);
	auto forwardThumbPath = getThumbPath(mCurrentForwardFrameIndex);
	auto sphericalFramePath = getPanoFramePath(mCurrentSphericalFrameIndex);
	auto sphericalThumbPath = getPanoThumbPath(mCurrentSphericalFrameIndex);

	auto forwardFrameData = forwardFrameTexture.load(forwardFramePath);
	auto forwardThumbData = forwardThumbTexture.load(forwardThumbPath);
	auto sphericalFrameData = sphericalFrameTexture.load(sphericalFramePath);
	auto sphericalThumbData = sphericalThumbTexture.load(sphericalThumbPath);

	mNearestForwardFrameImageData.emplace_back(std::move(forwardFrameTexture), std::move(forwardFrameData));
	mNearestForwardThumbImageData.emplace_back(std::move(forwardThumbTexture), std::move(forwardThumbData));
	mNearestSphericalFrameImageData.emplace_back(std::move(sphericalFrameTexture), std::move(sphericalFrameData));
	mNearestSphericalThumbImageData.emplace_back(std::move(sphericalThumbTexture), std::move(sphericalThumbData));
}



void drawAxes(glm::mat4x4 &model, float size, glm::vec3 centerObjColor) {
	meshShader->use();

	float longSize = 1.0f * size;
	float narrowSize = 0.1f * size;

	glm::mat4 xModelMatrix(model);
	xModelMatrix = glm::translate(xModelMatrix, glm::vec3(longSize / 2, 0, 0));
	xModelMatrix = glm::scale(xModelMatrix, glm::vec3(longSize, narrowSize, narrowSize));

	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(xModelMatrix));
	glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 0.0f, 0.0f);
	cubeModel->Draw(meshShader);

	glm::mat4 yModelMatrix(model);
	yModelMatrix = glm::translate(yModelMatrix, glm::vec3(0, longSize / 2, 0));
	yModelMatrix = glm::scale(yModelMatrix, glm::vec3(narrowSize, longSize, narrowSize));

	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(yModelMatrix));
	glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.0f, 1.0f, 0.0f);
	cubeModel->Draw(meshShader);

	glm::mat4 zModelMatrix(model);
	zModelMatrix = glm::translate(zModelMatrix, glm::vec3(0, 0, longSize / 2));
	zModelMatrix = glm::scale(zModelMatrix, glm::vec3(narrowSize, narrowSize, longSize));

	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(zModelMatrix));
	glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.0f, 0.0f, 1.0f);
	cubeModel->Draw(meshShader);

	glm::mat4 centerModelMatrix(model);
	centerModelMatrix = glm::scale(centerModelMatrix, glm::vec3(narrowSize * 2, narrowSize * 2, narrowSize * 2));
	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(centerModelMatrix));
	glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), centerObjColor.x, centerObjColor.y, centerObjColor.z);
	cubeModel->Draw(meshShader);
}




void drawRoom(glm::mat4x4 &view, glm::mat4x4 &proj) {

	meshShader->use();

	glUniform3f(glGetUniformLocation(meshShader->Program, "lightPos"), thirdPersonCamera.Position.x, thirdPersonCamera.Position.y, thirdPersonCamera.Position.z);

	glUniform3f(glGetUniformLocation(meshShader->Program, "lightColor"), 1.0f, 1.0f, 1.0f);

	glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.75f, 0.75f, 0.75f);

	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(proj));
	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(view));

	//==================

	// draw the room
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);

	glm::mat4 modelIdentity;
	glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(modelIdentity));
	testHoloRoom->Draw(meshShader);

	glDisable(GL_CULL_FACE);
	glCullFace(GL_BACK);
}


int getLocatableCameraFrameIndexFromPose(Pose p) {
	float poseTimestamp = p.Timestamp;
	float secondsSinceCameraStart = poseTimestamp - testPath->poses[0].Timestamp;
	float framesSinceCameraStart = secondsSinceCameraStart * locatableCameraFPS;
	int locatableCameraFrameIndex = (int)round(framesSinceCameraStart);
	locatableCameraFrameIndex = locatableCameraFrameIndex + poseIndexOffset;
	return locatableCameraFrameIndex;
}

int getSphericalCameraFrameIndexFromPose(Pose p) {
	int locatableCameraFrameIndex = getLocatableCameraFrameIndexFromPose(p);
	float secondsSinceCalibrationTime = (locatableCameraFrameIndex - currentScan.CalibrationFrameHL) / locatableCameraFPS;
	int sphericalFramesSinceCalibration = (int)std::round(secondsSinceCalibrationTime * sphereCameraFPS);
	int sphericalCameraFrameIndex = currentScan.CalibrationFrame360 + sphericalFramesSinceCalibration;
	return sphericalCameraFrameIndex;
}


// given a particular index in the pose list, and a desired timestamp offset...
// select an adjacent pose and do interpolation and return the generated pose
Pose getPoseFromIndex(size_t index, float timestampOffset) {
	//std::cout << "getPoseFromIndex " << index << " with offset " << timestampOffset << std::endl;
	if (timestampOffset == 0.0f) {
		return testPath->poses[index];	// no interpolation
	}

	if (index == 0 && timestampOffset < 0) {
		// trying to go before the beginning
		return testPath->poses[index];
	}

	if (index == testPath->poses.size() - 1 && timestampOffset > 0) {
		// trying to go after the end
		return testPath->poses[index];
	}

	Pose currentPose = testPath->poses[index];

	float desiredTimestamp = currentPose.Timestamp + timestampOffset;

	if (timestampOffset < 0) {
		//std::cout << "before" << std::endl;
		size_t prevPoseIndex = index - 1;

		bool prevPoseFound = false;
		while (!prevPoseFound) {
			if (prevPoseIndex < 0) {
				return testPath->poses[index];
			}

			if (testPath->poses[prevPoseIndex].Timestamp < desiredTimestamp) {
				prevPoseFound = true;
			}
			else {
				prevPoseIndex--;
			}
		}

		Pose prevPose = testPath->poses[prevPoseIndex];
		float t = (desiredTimestamp - prevPose.Timestamp) / (currentPose.Timestamp - prevPose.Timestamp);
		//std::cout << "t: " << t << std::endl;

		glm::vec3 interpolatedPos = glm::mix(prevPose.Position, currentPose.Position, t);
		glm::quat interpolatedRotQuat = glm::slerp(prevPose.RotationQuaternion, currentPose.RotationQuaternion, t);
		glm::mat4 interpolatedRotMat(interpolatedRotQuat);

		Pose interpolatedPose(locatableCamera, desiredTimestamp, interpolatedPos, interpolatedRotQuat, interpolatedRotMat, frontToSphereCameraRotation, frontToSphereCameraTranslation);
		return interpolatedPose;
	}
	else {
		//std::cout << "after" << std::endl;
		// timestamp > 0

		size_t nextPoseIndex = index + 1;

		bool nextPoseFound = false;
		while (!nextPoseFound) {
			if (nextPoseIndex > testPath->poses.size() - 1) {
				return testPath->poses[index];
			}

			if (testPath->poses[nextPoseIndex].Timestamp > desiredTimestamp) {
				nextPoseFound = true;
			}
			else {
				nextPoseIndex++;
			}
		}

		Pose nextPose = testPath->poses[nextPoseIndex];
		float t = (desiredTimestamp - currentPose.Timestamp) / (nextPose.Timestamp - currentPose.Timestamp);
		//std::cout << "t: " << t << std::endl;

		glm::vec3 interpolatedPos = glm::mix(currentPose.Position, nextPose.Position, t);
		glm::quat interpolatedRotQuat = glm::slerp(currentPose.RotationQuaternion, nextPose.RotationQuaternion, t);
		glm::mat4 interpolatedRotMat(interpolatedRotQuat);

		Pose interpolatedPose(locatableCamera, desiredTimestamp, interpolatedPos, interpolatedRotQuat, interpolatedRotMat, frontToSphereCameraRotation, frontToSphereCameraTranslation);
		return interpolatedPose;
	}
}

void draw() {
	frameNumber++;

	float cameraPathProgress = cameraPathSlider->value();
	int cameraPoseIndex = glm::clamp((int)(testPath->poses.size() * cameraPathProgress), 0, (int)(testPath->poses.size() - 1));
	auto cameraPose = testPath->poses[cameraPoseIndex];

	float cellCenterX = glm::round(visualizationCamera.Position.x / gridSizeMeters) * gridSizeMeters;
	float cellCenterZ = glm::round(visualizationCamera.Position.z / gridSizeMeters) * gridSizeMeters;

	glm::vec3 cellCenterPosition(cellCenterX, initialHeadY, cellCenterZ);

	glm::vec3 cellLookupPosition = cellCenterLookup ? cellCenterPosition : visualizationCamera.Position;

	size_t nearestForwardPoseIndex = testPath->findMostSimilarNeighborIndex(cellLookupPosition, visualizationCamera.Front, directionWeight, gridSizeMeters);
	Pose nearestForwardPose = getPoseFromIndex(nearestForwardPoseIndex, poseTimestampOffset);

	//==================

	//GLuint nearestForwardFrameTexture;
	//GLuint nearestForwardThumbTexture;
	//GLuint nearestSphericalFrameTexture;
	//GLuint nearestSphericalThumbTexture;

	// find the camera frame that most closely matches it temporally
	int newForwardFrameIndex = getLocatableCameraFrameIndexFromPose(nearestForwardPose);
	int newSphericalFrameIndex = getSphericalCameraFrameIndexFromPose(nearestForwardPose);
	

	if (newForwardFrameIndex != mCurrentForwardFrameIndex || newSphericalFrameIndex != mCurrentSphericalFrameIndex) {
		mCurrentForwardFrameIndex = newForwardFrameIndex;
		mCurrentSphericalFrameIndex = newSphericalFrameIndex;
	}

	//==================

	glm::mat4 thirdPersonCameraProjection = glm::perspective(thirdPersonCamera.Zoom, (float)screenWidth / (float)screenHeight, 0.1f, 100.0f);
	glm::mat4 thirdPersonCameraView = thirdPersonCamera.GetViewMatrix();
	
	


	glm::mat4 hololensModelMatrix = nearestForwardPose.ModelMatrix;	// model matrix of the HL device's pose (TODO: assuming HL camera is at HL origin)

	glm::mat4 sphereCameraModelMatrix = hololensModelMatrix;


	glm::vec3 visualizationCameraPosition = visualizationCamera.Position;
	//glm::vec3 visualizationCameraPosition = nearestForwardPose.Position;

	//glm::vec3 visualizationCameraPosition(sphereCameraModelMatrix * glm::vec4(0, 0, 0, 1));



	std::vector<size_t> poseIndicesForTriangleVerts;
	std::vector<std::shared_ptr<GLTexture>> texturesForTriangleVerts;


	bool insideCapturedArea = false;


	std::vector<Kernel::FT> barycentric_coords;
	barycentric_coords.reserve(3);	// alpha, beta, gamma

	// find the triangle closest to the current visualization location and mark the vertices
	Point2D query_point(visualizationCamera.Position.x, visualizationCamera.Position.z);

	Locate_type lt;
	int li;
	auto face_handle = testPath->cellCenterTriangulation.locate(query_point, lt, li);

	switch (lt)
	{
	case Locate_type::EDGE:
		std::cout << "edge" << std::endl;
		break;
	case Locate_type::FACE:

		insideCapturedArea = true;

		{
			// determine the barycentric coordinates of the query point
			Triangle_coordinates triangle_coordinates(face_handle->vertex(0)->point(), face_handle->vertex(1)->point(), face_handle->vertex(2)->point());

			triangle_coordinates(query_point, barycentric_coords);	// calculates the barycentric coords and saves them in the preallocated list
		}
		

		for (int i = 0; i < 3; i++) {

			auto &vert_i = face_handle->vertex(i);

			size_t poseIndex = (size_t) vert_i->info();	// the pose index in our path that corresponds with this triangle vert

			int sphereCameraFrame = getSphericalCameraFrameIndexFromPose(testPath->poses[poseIndex]);

			if (printingInfo) {
				std::cout << "for vert " << i << ", sphereCameraFrame = " << sphereCameraFrame << std::endl;
			}

			if (sphereFrameToActiveSphereTextures.find(sphereCameraFrame) == sphereFrameToActiveSphereTextures.end()) {
				std::cout << "need to load in gl texture for sphere camera frame " << sphereCameraFrame << std::endl;
				// active GL texture not found for that frame... load it in.

				int sphereCacheIndexToReplace = std::distance(sphereCacheLastFrameUsed.begin(), std::min_element(sphereCacheLastFrameUsed.begin(), sphereCacheLastFrameUsed.end()));
				std::cout << "need to load in gl texture for sphere camera frame " << sphereCameraFrame << std::endl;

				std::cout << "loading..." << std::endl;
				sphereCache[sphereCacheIndexToReplace]->load(getPanoFramePath(sphereCameraFrame));
				std::cout << "loaded, placed in cache index " << sphereCacheIndexToReplace << std::endl;

				auto updatedTexture = sphereCache[sphereCacheIndexToReplace];

				sphereFrameToActiveSphereTextures[sphereCameraFrame] = sphereCacheIndexToReplace;
				std::cout << "updated record: frame " << sphereCameraFrame << " --> texture " << sphereCacheIndexToReplace << std::endl;

				if (activeSphereTexturesToFrames.find(sphereCacheIndexToReplace) != activeSphereTexturesToFrames.end()) {
					int oldFrameForThisTexture = activeSphereTexturesToFrames[sphereCacheIndexToReplace];	// the sphere frame that used to be stored at this texture location

					std::cout << "this cache index (" << sphereCacheIndexToReplace << ") was previously used for frame " << oldFrameForThisTexture << std::endl;
					std::cout << "erasing this record" << std::endl;

					sphereFrameToActiveSphereTextures.erase(oldFrameForThisTexture);
				}
				
				activeSphereTexturesToFrames[sphereCacheIndexToReplace] = sphereCameraFrame;
				std::cout << "updated record: texture " << sphereCacheIndexToReplace << " --> frame " << sphereCameraFrame << std::endl;
			}

			// at this point, assume that the texture for this frame has been loaded correctly

			int sphereCacheIndex = sphereFrameToActiveSphereTextures[sphereCameraFrame];
			sphereCacheLastFrameUsed[sphereCacheIndex] = frameNumber;



			poseIndicesForTriangleVerts.push_back(poseIndex);
			texturesForTriangleVerts.push_back(sphereCache[sphereCacheIndex]);
		}

		

		break;
	case Locate_type::VERTEX:
		std::cout << "vertex" << std::endl;
		break;
	case Locate_type::OUTSIDE_AFFINE_HULL:
	case Locate_type::OUTSIDE_CONVEX_HULL:
		break;
	default:
		break;
	}




	if (showingPano) {

		glm::mat4 visualizationCameraProjection = glm::perspective(glm::radians(panoramaFOV), (float)screenWidth / (float)screenHeight, 0.1f, 100.0f);
		

		glm::mat4 visualizationCameraView = visualizationCamera.GetViewMatrix(visualizationCameraPosition);
		
		glm::mat4 visualizationSphereModel;
		visualizationSphereModel = glm::translate(visualizationSphereModel, visualizationCameraPosition);

		if (renderingSimulatedView) {

			meshShader->use();

			//==================

			// draw the room
			glEnable(GL_CULL_FACE);
			glCullFace(GL_FRONT);

			glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(visualizationCameraProjection));
			glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(visualizationCameraView));

			glm::mat4 modelIdentity;
			glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(modelIdentity));
			testHoloRoom->Draw(meshShader);

			glDisable(GL_CULL_FACE);
			glCullFace(GL_BACK);
		}
		else {
			spherePanoShader->use();

			glUniformMatrix4fv(glGetUniformLocation(spherePanoShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(visualizationCameraProjection));
			glUniformMatrix4fv(glGetUniformLocation(spherePanoShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(visualizationCameraView));


			glUniformMatrix4fv(glGetUniformLocation(spherePanoShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(visualizationSphereModel));

			//int textureUnit = 0;
			//glActiveTexture(GL_TEXTURE0 + textureUnit);
			//glBindTexture(GL_TEXTURE_2D, nearestSphericalFrameTexture);
			//glUniform1i(glGetUniformLocation(spherePanoShader->Program, "tex"), textureUnit);

			glUniform1i(glGetUniformLocation(spherePanoShader->Program, "insideCapturedArea"), insideCapturedArea ? 1 : 0);


			// rotation of the camera/hololens, representing ZXY euler angles in degrees 
			// (so z degrees about z axis, then x degrees about x axis, then y degrees about y axis, in that order)
			float testZRad = glm::radians(testSphereRotationZ);
			float testXRad = glm::radians(testSphereRotationX);
			float testYRad = glm::radians(testSphereRotationY);

			glm::mat4 testFrontToSphereCameraRotation;	// generated at runtime from user-set rotations
			testFrontToSphereCameraRotation = glm::rotate(testFrontToSphereCameraRotation, testZRad, glm::vec3(0.0f, 0.0f, 1.0f));
			testFrontToSphereCameraRotation = glm::rotate(testFrontToSphereCameraRotation, testXRad, glm::vec3(1.0f, 0.0f, 0.0f));
			testFrontToSphereCameraRotation = glm::rotate(testFrontToSphereCameraRotation, testYRad, glm::vec3(0.0f, 1.0f, 0.0f));


			if (insideCapturedArea) {
				for (int i = 0; i < 3; i++) {

					size_t poseIndexForThisVert = poseIndicesForTriangleVerts[i];
					auto p = testPath->poses[poseIndexForThisVert];

					auto rotationMatForThisPose = p.RotationMatrix * testFrontToSphereCameraRotation;
					//auto rotationMatForThisPose = p.SphereRotationMatrix;

					if (printingInfo) {
						std::cout << "for vert " << i << ", rotationMatForThisPose: " << std::endl;
						for (int x = 0; x < 4; x++) {
							for (int y = 0; y < 4; y++) {
								std::cout << rotationMatForThisPose[x][y] << "\t";
							}
							std::cout << std::endl;
						}
					}

					std::string rotationMatLoc = "surroundingSphereRotation[" + std::to_string(i) + "]";

					glUniformMatrix4fv(glGetUniformLocation(spherePanoShader->Program, rotationMatLoc.c_str()), 1, GL_FALSE, glm::value_ptr(rotationMatForThisPose));


					int textureUnit = i;
					glActiveTexture(GL_TEXTURE0 + textureUnit);
					glBindTexture(GL_TEXTURE_2D, texturesForTriangleVerts[i]->texture());

					std::string texUniformLoc = "surroundingSphereTex[" + std::to_string(i) + "]";

					glUniform1i(glGetUniformLocation(spherePanoShader->Program, texUniformLoc.c_str()), textureUnit);


					std::string barycentricCoordLoc = "barycentricCoord[" + std::to_string(i) + "]";
					glUniform1f(glGetUniformLocation(spherePanoShader->Program, barycentricCoordLoc.c_str()), barycentric_coords[i]);
				}
			}
			

			panoSphereModel->Draw(spherePanoShader);
		}
	} 
	else {
		meshShader->use();

		glUniform3f(glGetUniformLocation(meshShader->Program, "lightPos"), thirdPersonCamera.Position.x, thirdPersonCamera.Position.y, thirdPersonCamera.Position.z);

		glUniform3f(glGetUniformLocation(meshShader->Program, "lightColor"), 1.0f, 1.0f, 1.0f);

		glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.75f, 0.75f, 0.75f);

		glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(thirdPersonCameraProjection));
		glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(thirdPersonCameraView));

		//==================

		// draw the room
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		glm::mat4 modelIdentity;
		glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(modelIdentity));
		testHoloRoom->Draw(meshShader);

		glDisable(GL_CULL_FACE);
		glCullFace(GL_BACK);

		//==================

		// draw the frustum in the place that matches the current pose

		glm::mat4 frustumModelMatrix = testPath->poses[cameraPoseIndex].ModelMatrix;

		glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 1.0f, 0.0f);
		glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(frustumModelMatrix));
		frustumModel->Draw(meshShader);


		drawAxes(frustumModelMatrix, 0.5f, glm::vec3(1.0f, 1.0f, 0.5f));

		//==================

		// draw where the visualization camera is

		glm::mat4 visualizationCameraModelMatrix = glm::inverse(visualizationCamera.GetViewMatrix());
		visualizationCameraModelMatrix = glm::scale(visualizationCameraModelMatrix, glm::vec3(0.25f, 0.25f, 0.25f));

		glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(visualizationCameraModelMatrix));
		glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 1.0f, 0.5f);
		arrowModel->Draw(meshShader);

		//==================

		// find the nearest location on the path to the current (virtual) camera and place a marker object there

		glm::mat4 nearestForwardPoseModelMatrix(hololensModelMatrix);
		nearestForwardPoseModelMatrix = glm::scale(nearestForwardPoseModelMatrix, glm::vec3(0.25f, 0.25f, 0.25f));

		glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(nearestForwardPoseModelMatrix));
		glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.5f, 0.5f, 1.0f);
		arrowModel->Draw(meshShader);

		drawAxes(hololensModelMatrix, 0.25f, glm::vec3(0.5f, 0.5f, 1.0f));

		//==================

		// draw the test offset to the 360 camera
		glm::mat4 offsetModelMatrix(sphereCameraModelMatrix);
		offsetModelMatrix = glm::scale(offsetModelMatrix, glm::vec3(0.25f, 0.25f, 0.25f));

		glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(offsetModelMatrix));
		glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 0.75f, 0.5f, 1.0f);
		arrowModel->Draw(meshShader);

		drawAxes(sphereCameraModelMatrix, 0.25f, glm::vec3(0.75f, 0.5f, 1.0f));

		//==================

		// draw the closest cell center points
		for (auto &closestPoseEntry : testPath->closestPoseIndices) {
			auto &cell_id = closestPoseEntry.first;
			auto &index = closestPoseEntry.second;

			auto &p = testPath->poses[index];

			glm::mat4 cellCenterModelMatrix;
			cellCenterModelMatrix = glm::translate(cellCenterModelMatrix, p.SpherePosition);
			cellCenterModelMatrix = glm::scale(cellCenterModelMatrix, glm::vec3(0.1f, 1.0f, 0.1f));

			glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(cellCenterModelMatrix));
			glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 1.0f, 0.0f);
			cubeModel->Draw(meshShader);


			glm::mat4 frontPoseModelMatrix = p.ModelMatrix;
			frontPoseModelMatrix = glm::scale(frontPoseModelMatrix, glm::vec3(0.25f, 0.25f, 0.25f));
			glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(frontPoseModelMatrix));
			glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 0.0f, 1.0f);
			cubeModel->Draw(meshShader);

			glm::mat4 spherePoseModelMatrix = p.SphereModelMatrix;
			spherePoseModelMatrix = glm::scale(spherePoseModelMatrix, glm::vec3(0.15f, 0.5f, 0.15f));
			glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(spherePoseModelMatrix));
			glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 1.0f, 1.0f);
			cubeModel->Draw(meshShader);
		}

		switch (lt)
		{
		case Locate_type::EDGE:
			std::cout << "edge" << std::endl;
			break;
		case Locate_type::FACE:
			
			for (int i = 0; i < 3; i++) {
				auto v = face_handle->vertex(i);
				auto index = v->info();

				auto &p = testPath->poses[index];

				glm::mat4 cellCenterModelMatrix;
				cellCenterModelMatrix = glm::translate(cellCenterModelMatrix, p.SpherePosition);
				cellCenterModelMatrix = glm::scale(cellCenterModelMatrix, glm::vec3(0.25f, 0.25f, 0.25f));

				glUniformMatrix4fv(glGetUniformLocation(meshShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(cellCenterModelMatrix));
				glUniform3f(glGetUniformLocation(meshShader->Program, "objectColor"), 1.0f, 0.0f, 0.0f);
				cubeModel->Draw(meshShader);
			}

			break;
		case Locate_type::OUTSIDE_AFFINE_HULL:
			// outside affine hull
			break;
		case Locate_type::OUTSIDE_CONVEX_HULL:
			// outside convex hull
			break;
		case Locate_type::VERTEX:
			std::cout << "vertex" << std::endl;
			break;
		default:
			break;
		}


		//==================

		// draw the path

		pathShader->use();

		glUniformMatrix4fv(glGetUniformLocation(pathShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(thirdPersonCameraProjection));
		glUniformMatrix4fv(glGetUniformLocation(pathShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(thirdPersonCameraView));
		glUniformMatrix4fv(glGetUniformLocation(pathShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(modelIdentity));

		float endTimestamp = testPath->poses.back().Timestamp;
		glUniform1f(glGetUniformLocation(pathShader->Program, "endTimestamp"), endTimestamp);

		testPath->Draw(pathShader, 0, cameraPoseIndex);

		//==================

		// draw test sphere

		spherePanoShader->use();

		glUniformMatrix4fv(glGetUniformLocation(spherePanoShader->Program, "projection"), 1, GL_FALSE, glm::value_ptr(thirdPersonCameraProjection));
		glUniformMatrix4fv(glGetUniformLocation(spherePanoShader->Program, "view"), 1, GL_FALSE, glm::value_ptr(thirdPersonCameraView));

		// move the sphere to the nearest pose
		glm::mat4 panoSphereModelMatrix(sphereCameraModelMatrix);
		panoSphereModelMatrix = glm::scale(panoSphereModelMatrix, glm::vec3(0.05f, 0.05f, 0.05f));
		
		glUniformMatrix4fv(glGetUniformLocation(spherePanoShader->Program, "model"), 1, GL_FALSE, glm::value_ptr(panoSphereModelMatrix));

		//int textureUnit = 0;
		//glActiveTexture(GL_TEXTURE0 + textureUnit);
		//glBindTexture(GL_TEXTURE_2D, nearestSphericalFrameTexture);
		//glUniform1i(glGetUniformLocation(spherePanoShader->Program, "tex"), textureUnit);

		glUniform1i(glGetUniformLocation(spherePanoShader->Program, "insideCapturedArea"), insideCapturedArea ? 1 : 0);

		if (insideCapturedArea) {
			for (int i = 0; i < 3; i++) {

				size_t poseIndexForThisVert = poseIndicesForTriangleVerts[i];
				auto p = testPath->poses[poseIndexForThisVert];

				auto rotationMatForThisPose = p.RotationMatrix;

				std::string rotationMatLoc = "surroundingSphereRotation[" + std::to_string(i) + "]";

				glUniformMatrix4fv(glGetUniformLocation(spherePanoShader->Program, rotationMatLoc.c_str()), 1, GL_FALSE, glm::value_ptr(rotationMatForThisPose));


				int textureUnit = i;
				glActiveTexture(GL_TEXTURE0 + textureUnit);
				glBindTexture(GL_TEXTURE_2D, texturesForTriangleVerts[i]->texture());

				std::string texUniformLoc = "surroundingSphereTex[" + std::to_string(i) + "]";

				glUniform1i(glGetUniformLocation(spherePanoShader->Program, texUniformLoc.c_str()), textureUnit);
			}
		}

		panoSphereModel->Draw(spherePanoShader);
	}
	

	

	
}






// Moves/alters the camera positions based on user input
void doMovement()
{
	Camera* camera = controllingVisualizationCamera ? &visualizationCamera : &thirdPersonCamera;

	bool forceHorizontalMotion = controllingVisualizationCamera;
	//bool forceHorizontalMotion = false;

	// Camera controls
	if (keys[GLFW_KEY_W])
		camera->ProcessKeyboard(FORWARD, deltaTime, forceHorizontalMotion);
	if (keys[GLFW_KEY_S])
		camera->ProcessKeyboard(BACKWARD, deltaTime, forceHorizontalMotion);
	if (keys[GLFW_KEY_A])
		camera->ProcessKeyboard(LEFT, deltaTime, forceHorizontalMotion);
	if (keys[GLFW_KEY_D])
		camera->ProcessKeyboard(RIGHT, deltaTime, forceHorizontalMotion);
	if (keys[GLFW_KEY_Q])
		camera->ProcessKeyboard(DOWN, deltaTime, forceHorizontalMotion);
	if (keys[GLFW_KEY_E])
		camera->ProcessKeyboard(UP, deltaTime, forceHorizontalMotion);

	if (controllingVisualizationCamera) {
		// make sure the visualization camera is always set at the default height
		camera->Position.y = initialHeadY;
	}
}










int main(int /* argc */, char ** /* argv */) {

    glfwInit();

    glfwSetTime(0);

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    glfwWindowHint(GLFW_SAMPLES, 0);
    glfwWindowHint(GLFW_RED_BITS, 8);
    glfwWindowHint(GLFW_GREEN_BITS, 8);
    glfwWindowHint(GLFW_BLUE_BITS, 8);
    glfwWindowHint(GLFW_ALPHA_BITS, 8);
    glfwWindowHint(GLFW_STENCIL_BITS, 8);
    glfwWindowHint(GLFW_DEPTH_BITS, 24);
    glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

    // Create a GLFWwindow object
    window = glfwCreateWindow(screenWidth, screenHeight, "example3", nullptr, nullptr);
    if (window == nullptr) {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

#if defined(NANOGUI_GLAD)
    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress))
        throw std::runtime_error("Could not initialize GLAD!");
    glGetError(); // pull and ignore unhandled errors like GL_INVALID_ENUM
#endif

    glClearColor(0.2f, 0.25f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Create a nanogui screen and pass the glfw pointer to initialize
    screen = new Screen();
    screen->initialize(window, true);

    glfwGetFramebufferSize(window, &screenWidth, &screenHeight);
    glViewport(0, 0, screenWidth, screenHeight);
    glfwSwapInterval(0);
    glfwSwapBuffers(window);


    // Create nanogui gui
    bool enabled = true;
    FormHelper *gui = new FormHelper(screen);
	nanogui::ref<Window> nanoguiWindow = gui->addWindow(Eigen::Vector2i(10, 10), "Form helper example");

	/*
    gui->addGroup("Basic types");
    gui->addVariable("bool", bvar)->setTooltip("Test tooltip.");
    gui->addVariable("string", strval);

    gui->addVariable("int", ivar)->setSpinnable(true);
    gui->addVariable("float", fvar)->setTooltip("Test.");
    gui->addVariable("double", dvar)->setSpinnable(true);

    gui->addGroup("Complex types");
    gui->addVariable("Enumeration", enumval, enabled)->setItems({ "Item 1", "Item 2", "Item 3" });
    gui->addVariable("Color", colval);

    gui->addGroup("Other widgets");
    gui->addButton("A button", []() { std::cout << "Button pressed." << std::endl; })->setTooltip("Testing a much longer tooltip, that will wrap around to new lines multiple times.");;
	*/

	gui->addGroup("Synthetic viewpoint");
	gui->addVariable("Showing Panorama", showingPano)->setTooltip("Showing Panorama");

	gui->addVariable("cellCenterLookup", cellCenterLookup)->setTooltip("cellCenterLookup");
	
	gui->addVariable("poseIndexOffset", poseIndexOffset)->setSpinnable(true);

	gui->addVariable("controllingVisualizationCamera", controllingVisualizationCamera);
	
	gui->addVariable("poseTimestampOffset", poseTimestampOffset);

	gui->addVariable("panoramaFOV", panoramaFOV)->setSpinnable(true);

	gui->addVariable("directionWeight", directionWeight);

	gui->addVariable("renderingSimulatedView", renderingSimulatedView);


	gui->addVariable("testSphereRotationZ", testSphereRotationZ)->setSpinnable(true);
	gui->addVariable("testSphereRotationX", testSphereRotationX)->setSpinnable(true);
	gui->addVariable("testSphereRotationY", testSphereRotationY)->setSpinnable(true);
	

	gui->addVariable("printingInfo", printingInfo)->setTooltip("printingInfo");

	
	gui->addGroup("Visualization");

	Widget* panel = new Widget(nanoguiWindow);
	panel->setLayout(new BoxLayout(Orientation::Horizontal,
		Alignment::Middle, 0, 20));

	cameraPathSlider = new Slider(panel);
	cameraPathSlider->setValue(0.0f);
	cameraPathSlider->setFixedWidth(160);


	gui->addWidget("Camera Path", panel);


    screen->setVisible(true);
    screen->performLayout();
    //nanoguiWindow->center();



	nanogui::ref<Window> nearestThumbWindow = gui->addWindow(Eigen::Vector2i(700, 10), "Nearest Frame");
	nearestThumbWindow->setLayout(new GroupLayout());

	mCurrentForwardFrameIndex = 0;
	mCurrentSphericalFrameIndex = 0;
	setCurrentFrameTexture();


	mNearestForwardThumbImageView = new ImageView(nearestThumbWindow, mNearestForwardThumbImageData[0].first.texture());
	

	screen->setVisible(true);
	screen->performLayout();




    glfwSetCursorPosCallback(window,
            [](GLFWwindow *, double x, double y) {
            bool cursorPosConsumedByUI = screen->cursorPosCallbackEvent(x, y);
			if (!cursorPosConsumedByUI) {
				// use mouse movement for camera movement

				if (firstMouse)
				{
					lastX = x;
					lastY = y;
					firstMouse = false;
				}

				GLfloat xoffset = x - lastX;
				GLfloat yoffset = lastY - y;

				lastX = x;
				lastY = y;

				if (cameraMouseControlsActive) {
					if (controllingVisualizationCamera) {
						visualizationCamera.ProcessMouseMovement(xoffset, yoffset);
					}
					else {
						thirdPersonCamera.ProcessMouseMovement(xoffset, yoffset);
					}
					
				}
			}
        }
    );

    glfwSetMouseButtonCallback(window,
        [](GLFWwindow *, int button, int action, int modifiers) {
            bool mouseButtonConsumedByUI = screen->mouseButtonCallbackEvent(button, action, modifiers);
			if (!mouseButtonConsumedByUI) {

				if (button == GLFW_MOUSE_BUTTON_RIGHT) {
					if (action == GLFW_PRESS) {
						cameraMouseControlsActive = true;
					}
					else if (action == GLFW_RELEASE) {
						cameraMouseControlsActive = false;
					}
				}
			}
        }
    );

    glfwSetKeyCallback(window,
        [](GLFWwindow *, int key, int scancode, int action, int mods) {
            bool keyConsumedByUI = screen->keyCallbackEvent(key, scancode, action, mods);
			if (!keyConsumedByUI) {
				// treat the key input as camera controls

				if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
					glfwSetWindowShouldClose(window, GL_TRUE);

				if (action == GLFW_PRESS)
					keys[key] = true;
				else if (action == GLFW_RELEASE)
					keys[key] = false;
			}
        }
    );

    glfwSetCharCallback(window,
        [](GLFWwindow *, unsigned int codepoint) {
            screen->charCallbackEvent(codepoint);
        }
    );

    glfwSetDropCallback(window,
        [](GLFWwindow *, int count, const char **filenames) {
            screen->dropCallbackEvent(count, filenames);
        }
    );

    glfwSetScrollCallback(window,
        [](GLFWwindow *, double x, double y) {
            screen->scrollCallbackEvent(x, y);
       }
    );

    glfwSetFramebufferSizeCallback(window,
        [](GLFWwindow *, int width, int height) {
			screenWidth = width;
			screenHeight = height;
            screen->resizeCallbackEvent(screenWidth, screenHeight);

			glViewport(0, 0, screenWidth, screenHeight);
        }
    );

	initShader();

    // Game loop
    while (!glfwWindowShouldClose(window)) {
		// Set frame time
		GLfloat currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

        // Check if any events have been activated (key pressed, mouse moved etc.) and call corresponding response functions
        glfwPollEvents();
		doMovement();

		// note that we have to enable the depth test per-frame; it looks like nanogui disables it in order to draw the UI
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);


        glClearColor(0.2f, 0.25f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		draw();

        // Draw nanogui
        screen->drawContents();
        screen->drawWidgets();

        glfwSwapBuffers(window);
    }

    // Terminate GLFW, clearing any resources allocated by GLFW.
    glfwTerminate();

    return 0;
}
