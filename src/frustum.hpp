#pragma once

#include "glm_includes.hpp"
#include "cgal_includes.hpp"
#include "camera_parameters.hpp"

struct Frustum {
	// represents a world-space frustum (set of 6 planes) for some camera Pose, 
	// defining which region of space is potentially covered by a particular camera image

	K_Plane Planes[6];

	Frustum(CameraParameters camera, glm::mat4 modelMatrix)
	{
		auto hfovDegrees = camera.HfovDegrees;
		auto aspectRatio = camera.AspectRatio;
		auto nearZMeters = camera.NearZMeters;
		auto farZMeters = camera.FarZMeters;

		float hfovRadians = glm::radians(hfovDegrees);
		float vfovRadians = hfovRadians / aspectRatio;

		float nearXMeters = nearZMeters * glm::tan(hfovRadians / 2.0f);
		float nearYMeters = nearZMeters * glm::tan(vfovRadians / 2.0f);

		float farXMeters = farZMeters * glm::tan(hfovRadians / 2.0f);
		float farYMeters = farZMeters * glm::tan(vfovRadians / 2.0f);

		glm::vec3 near_bottom_left = modelMatrix * glm::vec4(-nearXMeters, -nearYMeters, -nearZMeters, 1.0f);
		glm::vec3 near_bottom_right = modelMatrix * glm::vec4(nearXMeters, -nearYMeters, -nearZMeters, 1.0f);
		glm::vec3 near_top_left = modelMatrix * glm::vec4(-nearXMeters, nearYMeters, -nearZMeters, 1.0f);
		glm::vec3 near_top_right = modelMatrix * glm::vec4(nearXMeters, nearYMeters, -nearZMeters, 1.0f);

		glm::vec3 far_bottom_left = modelMatrix * glm::vec4(-farXMeters, -farYMeters, -farZMeters, 1.0f);
		glm::vec3 far_bottom_right = modelMatrix * glm::vec4(farXMeters, -farYMeters, -farZMeters, 1.0f);
		glm::vec3 far_top_left = modelMatrix * glm::vec4(-farXMeters, farYMeters, -farZMeters, 1.0f);
		glm::vec3 far_top_right = modelMatrix * glm::vec4(farXMeters, farYMeters, -farZMeters, 1.0f);

		K_Point nbl(near_bottom_left.x, near_bottom_left.y, near_bottom_left.z);
		K_Point nbr(near_bottom_right.x, near_bottom_right.y, near_bottom_right.z);
		K_Point ntl(near_top_left.x, near_top_left.y, near_top_left.z);
		K_Point ntr(near_top_right.x, near_top_right.y, near_top_right.z);

		K_Point fbl(far_bottom_left.x, far_bottom_left.y, far_bottom_left.z);
		K_Point fbr(far_bottom_right.x, far_bottom_right.y, far_bottom_right.z);
		K_Point ftl(far_top_left.x, far_top_left.y, far_top_left.z);
		K_Point ftr(far_top_right.x, far_top_right.y, far_top_right.z);

		Planes[0] = K_Plane(nbl, nbr, ntr); // near
		Planes[1] = K_Plane(fbr, fbl, ftl); // far
		Planes[2] = K_Plane(fbl, nbl, ntl); // left
		Planes[3] = K_Plane(nbr, fbr, ftr); // right
		Planes[4] = K_Plane(nbr, nbl, fbl); // bottom
		Planes[5] = K_Plane(ntr, ftr, ftl); // top
	}
};