#pragma once

#include "gltexture.hpp"
#include <unordered_map>
#include <string>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/calib3d.hpp>


class SphereDataCache {
public:
	SphereDataCache(std::string scanLabel, int maxCapacity, int maxNumFeatures);

	std::shared_ptr<GLTexture> GetSphericalTextureForSphericalCameraFrame(int sphereCameraFrame);
	cv::Mat GetCubemapImgForSphericalCameraFrame(int sphereCameraFrame);

	std::vector<std::vector<cv::KeyPoint>> GetCubemapKeypointsForSphericalCameraFrame(int sphereCameraFrame);
	std::vector<cv::Mat> GetCubemapDescriptorsForSphericalCameraFrame(int sphereCameraFrame);

	void IncrementFrameNumber();
private:

	void CacheSphereDataIfNeededForSphericalCameraFrame(int sphereCameraFrame);

	std::string GetPanoFramePath(int frameIndex);
	std::string GetPanoThumbPath(int frameIndex);
	std::string GetCubemapPath(int frameIndex);

	cv::Ptr<cv::ORB> orb;

	void LoadOrGenerateKeypoints(int sphereCameraFrame, std::vector<std::vector<cv::KeyPoint>> & kp, std::vector<cv::Mat> & desc, cv::Mat & cubemapImg);

	int sphereDataCacheCapacity;
	std::string scanLabel;
	int maxNumFeatures; // maxNumFeatures is the number of features our feature detector is using for the max number of features

	int renderedFrameNumber;	// incremented each gameloop

	// maps from [index of sphere camera frame] to [index in sphere cache that contains actively loaded data for that frame]
	std::unordered_map<int, int> sphereFrameToActiveSphereData;
	std::unordered_map<int, int> activeSphereDataToFrames;	// inverse mapping

	std::vector<std::shared_ptr<GLTexture>> cachedSphericalTextures;	// spherical textures actively loaded into GPU
	std::vector<cv::Mat> cachedCubemapImgs;

	std::vector<std::vector<std::vector<cv::KeyPoint>>> cachedCubemapKeypoints;	// each entry is a vector of size 6, each containing a vector<KeyPoint>
	std::vector<std::vector<cv::Mat>> cachedCubemapDescriptors;	// each entry is a vector of size 6, each containing a Mat representing the descriptors

	std::vector<int> lastFrameUsed; // for each entry in the sphere cache, tracks the frame number when it was last used
};