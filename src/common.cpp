#include "common.hpp"
#include "model.hpp"

void CheckOpenGLError(const char* stmt, const char* fname, int line)
{
	bool anyError = false;
	GLenum err;
	do {
		err = glGetError();
		if (err != GL_NO_ERROR)
		{
			std::cout << "OpenGL error " << std::hex << std::setw(8) << err << std::dec << ", at " << fname << ":" << line << " - for " << stmt << std::endl;
			anyError = true;
		}
	} while (err != GL_NO_ERROR);

	if (anyError) {
		abort();
	}
}
