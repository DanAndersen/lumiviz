#include "model.hpp"
#include "assimp/cimport.h"

#include <unordered_map>
#include <iostream>
#include <fstream>
#include "utils.hpp"

Model::Model(string const & path)
{
	Assimp::Importer import;
	const aiScene* scene = import.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_GenNormals);

	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
		cout << "ERROR::ASSIMP::" << import.GetErrorString() << endl;
		return;
	}

	init(scene);
}

Model::Model()
{
}

Model::Model(const aiScene * scene)
{
	init(scene);
}

void Model::Draw(std::shared_ptr<Shader> shader)
{
	for (GLuint i = 0; i < this->meshes.size(); i++) {
		this->meshes[i].Draw(shader);
	}
}


std::shared_ptr<Model> Model::CreateAugmentedHullModelFromUnprojectedHullModel(std::shared_ptr<Model> unprojectedHullModel, glm::vec3 centerPoint) {
	// given some initially-generated model that has verts and indices, and where verts are shared across triangles...

	// generate a new model that instead duplicates vertices such that verts are not shared between multiple triangles, 
	// and so that we can encode barycentric coordinate data into the vertices.
	// the new vertices also contain a TriangleId value that refers to which triangle they were part of in the original model.

	auto & originalMesh = unprojectedHullModel->meshes[0];
	vector<GLuint> & originalMeshIndices = originalMesh.indices;
	vector<Vertex> & originalMeshVertices = originalMesh.vertices;
	int numTrianglesInOriginalMesh = originalMeshIndices.size() / 3;


	vector<GLuint> augmentedMeshIndices;
	vector<Vertex> augmentedMeshVertices;


	for (int originalTriangleId = 0; originalTriangleId < numTrianglesInOriginalMesh; originalTriangleId++) {

		for (int indexOfPointInTriangle = 0; indexOfPointInTriangle < 3; indexOfPointInTriangle++) {

			GLuint originalVertexIndex = originalMeshIndices[originalTriangleId * 3 + indexOfPointInTriangle];
			Vertex & originalVertex = originalMeshVertices[originalVertexIndex];

			
			glm::vec3 pos = originalVertex.Position;

			glm::vec3 normal = glm::normalize(pos - centerPoint);



			Vertex duplicatedVert(pos, normal);
			duplicatedVert.TriangleId = originalTriangleId;
			duplicatedVert.PointId = originalVertexIndex;

			switch (indexOfPointInTriangle) {
			case 0:
				duplicatedVert.BarycentricCoords = glm::vec3(1, 0, 0);
				break;
			case 1:
				duplicatedVert.BarycentricCoords = glm::vec3(0, 1, 0);
				break;
			case 2:
				duplicatedVert.BarycentricCoords = glm::vec3(0, 0, 1);
				break;
			default:
				std::cout << "ERROR: unanticipated indexOfPointInTriangle " << indexOfPointInTriangle << std::endl;
				break;
			}

			augmentedMeshVertices.push_back(duplicatedVert);
			GLuint indexOfNewDuplicatedVert = augmentedMeshVertices.size() - 1;

			augmentedMeshIndices.push_back(indexOfNewDuplicatedVert);

		}

	}
	
	std::shared_ptr<Model> augmentedHullModel = std::make_shared<Model>();
	augmentedHullModel->meshes.push_back(Mesh(augmentedMeshVertices, augmentedMeshIndices));

	return augmentedHullModel;
}







std::shared_ptr<Model> Model::CreateConvexHullFromPoints(std::vector<glm::vec3> pts) {
	std::vector<Point_3> hullPoints;
	std::vector<glm::vec2> uvs;

	for (auto pos : pts) {
		auto pt = Point_3(pos.x, pos.y, pos.z);
		hullPoints.push_back(pt);

		glm::vec2 uv = spherePointToUV(pos);
		uvs.push_back(uv);
	}

	Polyhedron_3 hullPoly;
	CGAL::convex_hull_3(hullPoints.begin(), hullPoints.end(), hullPoly);

	std::unordered_map<size_t, size_t> hullIndicesToMyIndices;	// maps the order of verts from the convex-hull-provided polyhedron, to the verts I used as input.
																// that's because the order of the verts is important, for consistency with the other spheres.

	std::size_t vertexId = 0;
	for (auto vertIt = hullPoly.vertices_begin(); vertIt != hullPoly.vertices_end(); ++vertIt) {

		auto pt = vertIt->point();
		glm::vec3 vertexPos(pt.x(), pt.y(), pt.z());
		glm::vec3 vertexNorm(pt.x(), pt.y(), pt.z());

		auto foundPoint = std::find(hullPoints.begin(), hullPoints.end(), pt);
		if (foundPoint != hullPoints.end()) {
			size_t foundIndexValue = (foundPoint - hullPoints.begin());
			hullIndicesToMyIndices[vertexId] = foundIndexValue;
			//std::cout << "found point " << pt << " in hullpoints, index = " << foundIndexValue << ", value = " << *foundPoint << std::endl;
		}

		vertIt->id() = vertexId;
		vertexId++;
	}

	std::vector<Vertex> hullVertices;	// making each triangle have unique verts rather than sharing verts, so we can have face-specific vert data
	vector<GLuint> hullIndices;

	GLuint triangleId = 0;

	for (auto facetIt = hullPoly.facets_begin(); facetIt != hullPoly.facets_end(); ++facetIt) {

		size_t indexOfPointInTriangle = 0;

		auto circ = facetIt->facet_begin();
		do {

			GLuint unduplicatedPointId = hullIndicesToMyIndices[circ->vertex()->id()];

			glm::vec3 &pos = pts[unduplicatedPointId];
			glm::vec2 &uv = uvs[unduplicatedPointId];

			Vertex duplicatedVert(pos, pos, uv);
			duplicatedVert.TriangleId = triangleId;	// setting the triangle ID
			duplicatedVert.PointId = unduplicatedPointId;

			switch (indexOfPointInTriangle)
			{
			case 0:
				duplicatedVert.BarycentricCoords = glm::vec3(1, 0, 0);
				break;
			case 1:
				duplicatedVert.BarycentricCoords = glm::vec3(0, 1, 0);
				break;
			case 2:
				duplicatedVert.BarycentricCoords = glm::vec3(0, 0, 1);
				break;
			default:
				std::cout << "ERROR: unanticipated indexOfPointInTriangle " << indexOfPointInTriangle << std::endl;
				break;
			}

			hullVertices.push_back(duplicatedVert);

			GLuint indexOfNewDuplicatedVert = hullVertices.size() - 1;

			hullIndices.push_back(indexOfNewDuplicatedVert);

			indexOfPointInTriangle++;
		} while (++circ != facetIt->facet_begin());

		triangleId++;
	}

	std::shared_ptr<Model> hullModel = std::make_shared<Model>();

	hullModel->meshes.push_back(Mesh(hullVertices, hullIndices));

	return hullModel;
}


glm::vec3 NormalFromTriangle(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3) {
	glm::vec3 norm = glm::normalize(glm::cross(p2 - p1, p3 - p1));
	return norm;
}

std::shared_ptr<Model> Model::CreateFrustum(float hfovDegrees, float aspectRatio, float zMeters) {
	cout << "creating a model for a frustum with hfov = " << hfovDegrees << " deg" << endl;

	float hfovRadians = glm::radians(hfovDegrees);
	float vfovRadians = hfovRadians / aspectRatio;

	float xMeters = zMeters * glm::tan(hfovRadians / 2.0f);
	float yMeters = zMeters * glm::tan(vfovRadians / 2.0f);

	glm::vec3 origin(0.0f, 0.0f, 0.0f);

	glm::vec3 bl(-xMeters, -yMeters, -zMeters);
	glm::vec3 br(xMeters, -yMeters, -zMeters);
	glm::vec3 tl(-xMeters, yMeters, -zMeters);
	glm::vec3 tr(xMeters, yMeters, -zMeters);
	
	glm::vec3 leftNorm = NormalFromTriangle(origin, tl, bl);
	glm::vec3 rightNorm = NormalFromTriangle(origin, br, tr);
	glm::vec3 topNorm = NormalFromTriangle(origin, tr, tl);
	glm::vec3 bottomNorm = NormalFromTriangle(origin, bl, br);
	glm::vec3 backNorm = NormalFromTriangle(br, bl, tl);

	Vertex origin_left(origin, leftNorm);
	Vertex origin_right(origin, rightNorm);
	Vertex origin_top(origin, topNorm);
	Vertex origin_bottom(origin, bottomNorm);

	Vertex tl_left(tl, leftNorm);
	Vertex tl_top(tl, topNorm);
	Vertex tl_back(tl, backNorm);

	Vertex tr_right(tr, rightNorm);
	Vertex tr_top(tr, topNorm);
	Vertex tr_back(tr, backNorm);

	Vertex bl_left(bl, leftNorm);
	Vertex bl_bottom(bl, bottomNorm);
	Vertex bl_back(bl, backNorm);

	Vertex br_right(br, rightNorm);
	Vertex br_bottom(br, bottomNorm);
	Vertex br_back(br, backNorm);

	vector<Vertex> vertices = { origin_left, tl_left, bl_left,
	origin_top,tr_top, tl_top,
	origin_right,br_right, tr_right,
	origin_bottom,bl_bottom, br_bottom,
	br_back, bl_back, tl_back,
	tl_back,tr_back,br_back };

	vector<GLuint> indices;
	for (GLuint i = 0; i < vertices.size(); i++) {
		indices.push_back(i);
	}

	std::shared_ptr<Model> model = std::make_shared<Model>();

	model->meshes.push_back(Mesh(vertices, indices));

	return model;
}

// defines a patched sphere (a subdivided cube with all points normalized to the unit sphere
std::shared_ptr<Model> Model::CreatePatchedSphere(int numPointsPerEdge)
{
	if (numPointsPerEdge < 2) {
		numPointsPerEdge = 2;
	}

	int numPointsPerFace = numPointsPerEdge*numPointsPerEdge;

	vector<Vertex> vertices;
	vector<GLuint> indices;

	for (int faceIdx = 0; faceIdx < 6; faceIdx++) {

		GLuint baseIdxForThisFace = faceIdx * numPointsPerFace;

		for (int verticalIdx = 0; verticalIdx < numPointsPerEdge; verticalIdx++) {

			float verticalValue = (2.0f / (numPointsPerEdge-1)) * verticalIdx - 1.0f;			// [0, n] --> [-1,1]

			float v = 1.0f - (float)verticalIdx / (numPointsPerEdge - 1);

			for (int horizontalIdx = 0; horizontalIdx < numPointsPerEdge; horizontalIdx++) {

				float horizontalValue = (2.0f / (numPointsPerEdge - 1)) * horizontalIdx - 1.0f;	// [0, n] --> [-1,1]

				float u = 1.0f - (float)horizontalIdx / (numPointsPerEdge - 1);

				glm::vec3 pos;

				switch (faceIdx)
				{
				case 0:	// pos x
					pos = glm::vec3(1.0f, verticalValue, horizontalValue);
					break;
				case 1:	// neg x
					pos = glm::vec3(-1.0f, verticalValue, -horizontalValue);
					break;
				case 2:	// pos y
					pos = glm::vec3(horizontalValue, -1.0f, -verticalValue);
					break;
				case 3:	// neg y
					pos = glm::vec3(horizontalValue, 1.0f, verticalValue);
					break;
				case 4: // pos z
					pos = glm::vec3(horizontalValue, verticalValue, -1.0f);
					break;
				case 5:	// neg z
					pos = glm::vec3(-horizontalValue, verticalValue, 1.0f);
					break;
				default:
					break;
				}

				pos = glm::normalize(pos);
				glm::vec3 norm = glm::normalize(-pos);
				glm::vec2 texCoords(u, v);

				vertices.push_back(Vertex(pos, norm, texCoords, texCoords, texCoords));

				if (verticalIdx < (numPointsPerEdge - 1) && horizontalIdx < (numPointsPerEdge - 1)) {
					GLuint lowerLeft = baseIdxForThisFace + (numPointsPerEdge*verticalIdx) + horizontalIdx;
					GLuint lowerRight = lowerLeft + 1;
					GLuint upperLeft = lowerLeft + numPointsPerEdge;
					GLuint upperRight = upperLeft + 1;

					indices.push_back(lowerLeft);
					indices.push_back(lowerRight);
					indices.push_back(upperRight);

					indices.push_back(lowerLeft);
					indices.push_back(upperRight);
					indices.push_back(upperLeft);
				}
			}
		}

		
	}
	
	std::shared_ptr<Model> model = std::make_shared<Model>();

	model->meshes.push_back(Mesh(vertices, indices));

	return model;
}

// defines a panorama sphere to display equirectangular textures onto.
// Y is up, -Z is forward, X is to the right.
// triangles face inward to the center of the sphere
// numSegments: number of verts along each parallel.
// numRings: number of parallels (number of verts along each meridian)
std::shared_ptr<Model> Model::CreatePanoramaSphere(float radius, int numSegments, int numRings, bool invertNormals)
{
	if (numSegments < 3) {
		numSegments = 3;
	}

	if (numRings < 3) {
		numRings = 3;
	}

	float pi = glm::pi<float>();

	float radiansPerRing = pi / numRings;
	float radiansPerSegment = 2.0f * glm::pi<float>() / numSegments;

	vector<Vertex> vertices;
	for (int latIdx = 0; latIdx <= numRings; latIdx++) {
		float latitudeRadians = (-pi / 2.0f) + (latIdx * radiansPerRing);
		float y = radius * glm::sin(latitudeRadians);

		float v = 1.0f - (float)latIdx / numRings;

		for (int lonIdx = 0; lonIdx < numSegments; lonIdx++) {
			float longitudeRadians = (-pi)+(lonIdx * radiansPerSegment);

			float x = (radius * glm::cos(latitudeRadians)) * (-glm::sin(longitudeRadians));
			float z = (radius * glm::cos(latitudeRadians)) * (-glm::cos(longitudeRadians));

			float u = 1.0f - (float)lonIdx / numSegments;

			glm::vec3 pos(x, y, z);
			
			float lengthPos = glm::length(pos);
			if (lengthPos < 0.5f * radius) {
				std::cout << "LENGTH: " << lengthPos << std::endl;
			}
			

			glm::vec3 norm = glm::normalize(-pos);
			if (invertNormals) {
				norm = norm * -1.0f;
			}
			glm::vec2 texCoords(u, v);

			vertices.push_back(Vertex(pos, norm, texCoords, texCoords, texCoords));
		}
	}

	int numVertsPerRing = numSegments;

	vector<GLuint> indices;
	for (int latIdx = 0; latIdx < numRings; latIdx++) {
		for (int lonIdx = 0; lonIdx < numSegments; lonIdx++) {

			// as viewed from inside:
			GLuint br = (numVertsPerRing * latIdx) + lonIdx;
			GLuint bl = br + 1;
			GLuint tr = br + numVertsPerRing;
			GLuint tl = tr + 1;

			indices.push_back(br);
			indices.push_back(tr);
			indices.push_back(tl);

			indices.push_back(br);
			indices.push_back(tl);
			indices.push_back(bl);
		}
	}

	std::shared_ptr<Model> model = std::make_shared<Model>();

	model->meshes.push_back(Mesh(vertices, indices));

	return model;
}

std::shared_ptr<Model> Model::LoadHoloRoom(string const & path, bool flatteningRoom, bool removingCeilingAndFloor, float ceilingMinY, float floorMaxY)
{
	// loading in a model in the format defined by HoloToolkit-Unity's SimpleMeshSerializer:
	// https://github.com/Microsoft/HoloToolkit-Unity/blob/77235f06cd6fd269d74813d1cc1c0baed0c5e8da/Assets/HoloToolkit/SpatialMapping/Scripts/RemoteMapping/SimpleMeshSerializer.cs

	// This class saves minimal mesh data (vertices and triangle indices) in the following format:
	//    File header: vertex count (32 bit integer), triangle count (32 bit integer)
	//    Vertex list: vertex.x, vertex.y, vertex.z (all 32 bit float)
	//    Triangle index list: 32 bit integers

	cout << "attempting to load in holo-room from " << path << endl;

	std::ifstream ifs;

	ifs.open(path, std::ifstream::in | std::ifstream::binary);
	if (!ifs) {
		cout << "Error opening file for holo-room" << endl;
		return nullptr;
	}

	std::shared_ptr<Model> model = std::make_shared<Model>();

	cout << "loading meshes for this model..." << endl;

	while (ifs.good()) {
		int vertexCount;
		int triangleIndexCount;

		ifs.read((char*)&vertexCount, sizeof(vertexCount));
		ifs.read((char*)&triangleIndexCount, sizeof(triangleIndexCount));

		cout << "loading mesh " << model->meshes.size() << " (vertexCount = " << vertexCount << ") ..." << endl;
		//cout << "got vertexCount = " << vertexCount << ", triangleIndexCount = " << triangleIndexCount << endl;

		vector<glm::vec3> vertexPositions;
		vertexPositions.resize(vertexCount);

		ifs.read((char*)vertexPositions.data(), vertexCount * sizeof(glm::vec3));



		

		int actualTriangleIndexCount = triangleIndexCount; // if we choose not to bring in some triangles, we will change this value

		vector<GLuint> indices;
		if (removingCeilingAndFloor) {

			int originalTriangleCount = triangleIndexCount / 3;
			//std::cout << "originalTriangleCount: " << originalTriangleCount << std::endl;

			// go through each index and determine if the associated triangles are within the correct bounds (below the "ceiling" and above the "floor").
			// Otherwise, skip that entire triangle.

			GLuint triIdxA;
			GLuint triIdxB;
			GLuint triIdxC;

			glm::vec3 triPosA;
			glm::vec3 triPosB;
			glm::vec3 triPosC;

			for (int originalTriangleIdx = 0; originalTriangleIdx < originalTriangleCount; originalTriangleIdx++) {
				ifs.read((char*)&triIdxA, sizeof(triIdxA));
				ifs.read((char*)&triIdxB, sizeof(triIdxB));
				ifs.read((char*)&triIdxC, sizeof(triIdxC));

				//std::cout << triIdxA << " " << triIdxB << " " << triIdxC << std::endl;

				triPosA = vertexPositions[triIdxA];
				triPosB = vertexPositions[triIdxB];
				triPosC = vertexPositions[triIdxC];

				bool inRangeA = triPosA.y <= ceilingMinY && triPosA.y >= floorMaxY;
				bool inRangeB = triPosB.y <= ceilingMinY && triPosB.y >= floorMaxY;
				bool inRangeC = triPosC.y <= ceilingMinY && triPosC.y >= floorMaxY;

				if (inRangeA && inRangeB && inRangeC) {
					indices.push_back(triIdxA);
					indices.push_back(triIdxB);
					indices.push_back(triIdxC);
				}
			}

			actualTriangleIndexCount = indices.size();
		}
		else {
			indices.resize(triangleIndexCount);
			ifs.read((char*)indices.data(), triangleIndexCount * sizeof(GLuint));
		}

		// now that the original vertex data has been read in, we can adjust it
		for (int i = 0; i < vertexCount; i++) {
			vertexPositions[i].z *= -1.0f; // because Unity uses a left-handed coord system, we want to flip this axis so it is in right-handed coords

			if (flatteningRoom) {
				// set all the vertical dimensions to 0 (do this after we have sliced off the ceiling/floor
				vertexPositions[i].y = 0.0f;
			}
		}
		

		//ifs >> numVertices >> numTriangles;

		
		

		// next, generate normals (follow procedure in GenFaceNormalsProcess in assimp)
		vector<glm::vec3> vertexNormals;
		vertexNormals.resize(vertexCount);

		int triangleCount = actualTriangleIndexCount / 3;

		if (flatteningRoom) {
			// if we're flattening everything to 2D, then we can just say the normals are all pointing up

			for (int i = 0; i < triangleCount; i++) {
				vertexNormals[indices[3 * i + 0]] = glm::vec3(0.0f, 1.0f, 0.0f);
				vertexNormals[indices[3 * i + 1]] = glm::vec3(0.0f, 1.0f, 0.0f);
				vertexNormals[indices[3 * i + 2]] = glm::vec3(0.0f, 1.0f, 0.0f);
			}
 		}
		else {
			for (int i = 0; i < triangleCount; i++) {
				GLuint i1 = indices[3 * i + 0];
				GLuint i2 = indices[3 * i + 1];
				GLuint i3 = indices[3 * i + 2];

				glm::vec3 p1 = vertexPositions[i1];
				glm::vec3 p2 = vertexPositions[i2];
				glm::vec3 p3 = vertexPositions[i3];

				glm::vec3 norm = -NormalFromTriangle(p1, p2, p3);	// because Unity has a left-handed coord system, we need to change the normal directions

				vertexNormals[indices[3 * i + 0]] = norm;
				vertexNormals[indices[3 * i + 1]] = norm;
				vertexNormals[indices[3 * i + 2]] = norm;
			}
		}
		

		vector<Vertex> vertices;
		for (int i = 0; i < vertexCount; i++) {
			Vertex v(vertexPositions[i], vertexNormals[i]);
			vertices.push_back(v);
		}

		model->meshes.push_back(Mesh(vertices, indices));
	}

	cout << "loading meshes done." << endl;

	ifs.close();

	return model;
}

std::shared_ptr<Model> Model::CreateFullscreenQuad()
{
	std::cout << "creating a fullscreen quad" << std::endl;

	
	glm::vec3 bl(-1, -1, 0);
	glm::vec3 br(1, -1, 0);
	glm::vec3 tl(-1, 1, 0);
	glm::vec3 tr(1, 1, 0);

	glm::vec2 bl_uv(0, 0);
	glm::vec2 br_uv(1, 0);
	glm::vec2 tl_uv(0, 1);
	glm::vec2 tr_uv(1, 1);

	vector<Vertex> vertices = { Vertex(bl,bl,bl_uv), Vertex(br,br,br_uv), Vertex(tl,tl,tl_uv), Vertex(tr,tr,tr_uv) };

	vector<GLuint> indices{ 
		0, 1, 3, 
		0, 2, 3 };

	std::shared_ptr<Model> model = std::make_shared<Model>();

	model->meshes.push_back(Mesh(vertices, indices));

	return model;
}

void Model::GenerateAABBTree()
{
	std::cout << "generating AABB tree..." << std::endl;

	points.clear();

	int meshIdx = 0;

	kMesh.clear();

	size_t vertStartIndex = 0;

	for (auto &mesh : this->meshes) {

		std::cout << "adding mesh " << meshIdx << std::endl;

		for (auto &vert : mesh.vertices) {

			K_vertex_descriptor v = kMesh.add_vertex(K_Point(vert.Position.x, vert.Position.y, vert.Position.z));

			points.push_back(v);
		}

		size_t numIndices = mesh.indices.size();

		for (auto i = 0; i < numIndices; i += 3) {

			size_t p1_index = vertStartIndex + mesh.indices[i + 0];
			size_t p2_index = vertStartIndex + mesh.indices[i + 1];
			size_t p3_index = vertStartIndex + mesh.indices[i + 2];

			K_vertex_descriptor p1 = points[p1_index];
			K_vertex_descriptor p2 = points[p2_index];
			K_vertex_descriptor p3 = points[p3_index];
			
			//std::cout << p1 << " " << p2 << " " << p3 << std::endl;
			kMesh.add_face(p1, p2, p3);

		}

		vertStartIndex += mesh.vertices.size();

		meshIdx++;
	}

	std::cout << "points: " << points.size() << std::endl;

	std::cout << "making tree object" << std::endl;
	this->aabb_tree = std::make_shared<K_Tree>(faces(kMesh).first, faces(kMesh).second, kMesh);
	std::cout << "tree object made" << std::endl;

}

void Model::init(const aiScene* scene)
{
	this->processNode(scene->mRootNode, scene);
}

void Model::processNode(aiNode * node, const aiScene * scene)
{
	for (GLuint i = 0; i < node->mNumMeshes; i++) {
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
		this->meshes.push_back(this->processMesh(mesh, scene));
	}

	for (GLuint i = 0; i < node->mNumChildren; i++) {
		this->processNode(node->mChildren[i], scene);
	}
}

Mesh Model::processMesh(aiMesh * mesh, const aiScene * scene)
{
	vector<Vertex> vertices;
	vector<GLuint> indices;

	// TODO: skipping texture import

	for (GLuint i = 0; i < mesh->mNumVertices; i++) {
		
		glm::vec3 pos;
		pos.x = mesh->mVertices[i].x;
		pos.y = mesh->mVertices[i].y;
		pos.z = mesh->mVertices[i].z;
		
		glm::vec3 normal;
		normal.x = mesh->mNormals[i].x;
		normal.y = mesh->mNormals[i].y;
		normal.z = mesh->mNormals[i].z;
		
		glm::vec2 uv(0.0f, 0.0f);

		if (mesh->mTextureCoords[0]) {
			uv.x = mesh->mTextureCoords[0][i].x;
			uv.y = mesh->mTextureCoords[0][i].y;
		}

		vertices.push_back(Vertex(pos, normal, uv));
	}

	for (GLuint i = 0; i < mesh->mNumFaces; i++) {
		aiFace face = mesh->mFaces[i];
		for (GLuint j = 0; j < face.mNumIndices; j++) {
			indices.push_back(face.mIndices[j]);
		}
	}

	// TODO: process material

	return Mesh(vertices, indices);
}

