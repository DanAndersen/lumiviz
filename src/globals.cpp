#include "globals.hpp"
#include "filesystem.h"

float gridSizeMeters = 0.5f;

std::unordered_map<int, Pose> frameNumbersToSpherePoses;

glm::mat4 testFrontToSphereCameraRotation;

json scanData;

float nn_match_ratio = 0.8f;

int cubeSidePixels = 960;

cv::Ptr<cv::DescriptorMatcher> matcher = cv::DescriptorMatcher::create(cv::DescriptorMatcher::BRUTEFORCE_HAMMING);




CameraParameters sphericalCamera(90.0f, 1.0f, 0.01f, 100.0f);	// We assume the "spherical camera" is represented as the front face of a cubemap

json ReadScanData(const std::string &scandataPath) {
	std::cout << "reading scan data..." << std::endl;
	std::ifstream ifs(scandataPath);

	json j;
	ifs >> j;

	std::cout << "hololens_sync_frame: " << j["hololens_sync_frame"] << std::endl;
	std::cout << "spherical_fps: " << j["spherical_fps"] << std::endl;
	std::cout << "spherical_sync_frame: " << j["spherical_sync_frame"] << std::endl;

	return j;
}

void initScanData(std::string scanSessionLabel) {

	scanData = ReadScanData(FileSystem::getPath(string_format("resources/scans/%s/scandata.json", scanSessionLabel.c_str())).c_str());

	std::string sphericalFilepath = string_format("resources/scans/%s/fn_to_pose_refined.csv", scanSessionLabel.c_str());
	//std::string sphericalFilepath = string_format("resources/scans/%s/fn_to_pose_initial.csv", scanSessionLabel.c_str());
	cout << "sphericalFilepath: " << sphericalFilepath << endl;

	frameNumbersToSpherePoses = Pose::LoadSpherePoses(sphericalCamera, FileSystem::getPath(sphericalFilepath).c_str());
}